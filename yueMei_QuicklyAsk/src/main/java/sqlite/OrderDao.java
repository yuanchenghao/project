package sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * 消息表管理类
 * Created by 裴成浩 on 2018/4/25.
 */

public class OrderDao {

    private String TAG = "OrderDao";
    private final Context mContext;
    private final SQLiteDatabase db;

    public OrderDao(Context context) {
        this.mContext = context;
        OrderDBHelper dbHelper = new OrderDBHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    /**
     * 增加数据
     *
     * @param messageId：修改数据的id
     * @param number：消息数
     */
    public void insertData(int messageId, int number) {
        if (checkHave(messageId)) {
            Log.e(TAG, "messageId === " + messageId);
            Log.e(TAG, "number === " + number);
            String sql = "insert into " + OrderDBHelper.MESSAGE_NUMBER + "(messageId,number) values(" + messageId + "," + number + ")";
            Log.e(TAG, "insertData ----->>>> " + sql);
            db.execSQL(sql);
        }
    }


    /**
     * 根据id删除一条数据
     *
     * @param messageId:删除数据的id
     */
    public void deleteData(int messageId) {
        String sql = "delete from " + OrderDBHelper.MESSAGE_NUMBER + " where messageId = " + messageId;
        Log.e(TAG, "deleteData ----->>>> " + sql);
        db.execSQL(sql);
    }

    /**
     * 修改一条数据
     *
     * @param messageId
     * @param number
     */
    public void correctData(int messageId, int number) {
        String sql = "update " + OrderDBHelper.MESSAGE_NUMBER + " set number = " + number + " where messageId = " + messageId;
        Log.e(TAG, "correctData ----->>>> " + sql);
        db.execSQL(sql);
    }

    /**
     * 查询数据
     *
     * @param messageId
     */
    public int checkData(int messageId) {
        int retNumber = 0;
        Cursor cursor = db.rawQuery("select * from " + OrderDBHelper.MESSAGE_NUMBER + " where messageId = " + messageId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();   //移动到首位
            int id = cursor.getInt(cursor.getColumnIndex("messageId"));
            int number = cursor.getInt(cursor.getColumnIndex("number"));

            Log.e(TAG, "id == " + id);
            Log.e(TAG, "number == " + number);
            retNumber = number;
        }
        cursor.close();
        return retNumber;
    }

    /**
     * 查询当前id是否存在
     *
     * @param messageId
     * @return
     */
    private boolean checkHave(int messageId) {
        Cursor cursor = db.rawQuery("select * from " + OrderDBHelper.MESSAGE_NUMBER + " where messageId = " + messageId, null);

        return cursor.getCount() > 0;
    }

    /**
     * 关闭资源防止内存溢出
     */
    public void closeResource() {
        db.close();
    }
}