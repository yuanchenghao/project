package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.controller.adapter.DiaryListAdapter;
import com.module.doctor.controller.api.DocDeRijiListApi;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 医生页全部日记列表
 *
 * @author 裴成浩
 */
public class DocDeRijiListActivity extends YMBaseActivity {

    private final String TAG = "DocDeRijiListActivity";
    private DocDeRijiListActivity mContext;


    @BindView(R.id.my_collect_list_refresh)
    SmartRefreshLayout listRefresh;
    @BindView(R.id.my_collect_list_view)
    ListView mListView;
    @BindView(R.id.my_collect_list_refresh_more)
    YMLoadMore mListRefreshMore;
    private int mCurPage = 1;
    @BindView(R.id.collect_posts_top)
    CommonTopBar mTop;// 返回
    @BindView(R.id.my_collect_post_tv_nodata)
    LinearLayout nodataTv;

    private DiaryListAdapter bbsListAdapter;
    private String docid;
    private HashMap<String, Object> docDeRijiListMap = new HashMap<>();
    private DocDeRijiListApi docDeRijiListApi;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_my_collect_posts;
    }

    @Override
    protected void initView() {
        mContext = DocDeRijiListActivity.this;
        Intent it = getIntent();
        docid = it.getStringExtra("docid");

        mTop.setCenterText("相关日记");
    }

    @Override
    protected void initData() {
        docDeRijiListApi = new DocDeRijiListApi();
        initList();
        lodBBsListData550();
    }

    private void initList() {

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //下拉刷新
        listRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mCurPage = 1;
                bbsListAdapter = null;
                lodBBsListData550();
            }
        });

        //上拉加载更多
        listRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                lodBBsListData550();
            }
        });

        //点击事件
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {
                if (bbsListAdapter != null) {
                    BBsListData550 bBsListData550 = bbsListAdapter.getmHotIssues().get(pos);
                    String url = bBsListData550.getUrl();
                    if (url.length() > 0) {
                        String qid = bBsListData550.getQ_id();
                        String appmurl = bBsListData550.getAppmurl();
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                        Log.e(TAG, "url=" + url + "/qid=" + qid);
                    }
                }
            }
        });
    }

    private void lodBBsListData550() {
        docDeRijiListMap.put("id", docid);
        docDeRijiListMap.put("page", mCurPage + "");
        docDeRijiListApi.getCallBack(mContext, docDeRijiListMap, new BaseCallBackListener<List<BBsListData550>>() {
            @Override
            public void onSuccess(List<BBsListData550> docListDatas) {
                nodataTv.setVisibility(View.GONE);
                mCurPage++;

                Log.e(TAG, "docListDatas == " + docListDatas.size());

                if (bbsListAdapter == null) {
                    listRefresh.finishRefresh();
                    bbsListAdapter = new DiaryListAdapter(mContext, docListDatas, "");
                    mListView.setAdapter(bbsListAdapter);
                } else {
                    if (docListDatas.size() < 20) {
                        listRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        listRefresh.finishLoadMore();
                    }
                    bbsListAdapter.add(docListDatas);
                    bbsListAdapter.notifyDataSetChanged();
                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
