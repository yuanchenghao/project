package com.module.doctor.controller.other;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.MapHospitalWebActivity;
import com.module.commonview.module.api.AutoSendApi;
import com.module.commonview.module.api.AutoSendApi2;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosCommentActivity;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.controller.activity.WantBeautifulActivity548;
import com.module.doctor.model.bean.HosShareData;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/1/9.
 */

public class HosDetailWebViewClient implements BaseWebViewClientCallback {

    private String hosid;
    private Intent intent;
    private HosDetailActivity mActivity;
    private String TAG = "HosDetailWebViewClient";
    private WebView mWebView;

    public HosDetailWebViewClient(WebView webView, HosDetailActivity activity, String hosid) {
        this.mActivity = activity;
        this.hosid = hosid;
        intent = new Intent();
        this.mWebView = webView;
    }

    @Override
    public void otherJump(String urlStr) throws JSONException {
        Log.e(TAG, "urlStr == " + urlStr);
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws JSONException {
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);

        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页

                String id1 = obj.getString("id");
                intent.putExtra("docId", id1);
                intent.putExtra("docName", "");
                intent.putExtra("partId", "");
                intent.putExtra("isAutoSend", "1");
                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                mActivity.startActivity(intent);
                break;
            case "3":   // 跳到预约
                String po3 = obj.getString("po");
                String hosid3 = obj.getString("hosid");
                String docid3 = obj.getString("docid");
                String shareid3 = "0";

                intent.setClass(mActivity, WantBeautifulActivity548.class);
                intent.putExtra("po", po3);
                intent.putExtra("hosid", hosid3);
                intent.putExtra("docid", docid3);
                intent.putExtra("shareid", shareid3);
                intent.putExtra("cityId", "0");
                intent.putExtra("partId", "0");
                intent.putExtra("partId_two", "0");
                mActivity.startActivity(intent);
                break;

            case "6":// 问答详情

                String link6 = obj.getString("link");
                String qid6 = obj.getString("id");

                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link6);
                intent.putExtra("qid", qid6);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);

                break;
            case "531":  // 医院介绍
                String link531 = obj.getString("link");
                Log.e(TAG, "link531 == " + link531);
                String url = FinalConstant.baseUrl + FinalConstant.VER + "/" + link531;

                String[] split = link531.split("/");

                HashMap<String, Object> hashMap = new HashMap<>();
                for (int i = 3; i < split.length; i += 2) {
                    if (i + 1 <= split.length) {
                        hashMap.put(split[i], split[i + 1]);
                    }
                }
                mActivity.LodUrl1(url, hashMap);

                break;
            case "534": //医院评论

                String link534 = obj.getString("link");
                Utils.tongjiApp(mActivity, "hospital_comment", "hospital", hosid, "13");
                Intent it = new Intent();
                it.putExtra("url", link534);
                it.setClass(mActivity, HosCommentActivity.class);
                mActivity.startActivity(it);

                break;

            case "541": // 医院位置

                String hosid541 = obj.getString("hosid");

                Utils.tongjiApp(mActivity, "hospital_address", "hospital", hosid541, "13");
                intent.setClass(mActivity, MapHospitalWebActivity.class);
                intent.putExtra("hosid", hosid541);
                mActivity.startActivity(intent);

                break;

            case "535": //打电话
                showDialog();
                break;
            case "6521": //搜本院
                String searchkey = obj.getString("searchkey");
                String sarch = "";
                try {
                    sarch = URLDecoder.decode(searchkey, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String id = obj.getString("id");

                Intent it6521 = new Intent(mActivity, SearchAllActivity668.class);
                it6521.putExtra(SearchAllActivity668.KEYS, sarch);
                it6521.putExtra(SearchAllActivity668.HOSPITAL_ID, id);
                it6521.putExtra(SearchAllActivity668.TYPE, "5");
                mActivity.startActivity(it6521);

                break;
            case "6523": //医院分享
                String sharedata = obj.getString("sharedata");
                String decode = URLDecoder.decode(sharedata);
                Log.e(TAG, "decode==" + decode);
                try {
                    HosShareData hosShareData = JSONUtil.TransformSingleBean(decode, HosShareData.class);

                    String mHos_name = hosShareData.getHos_name();
                    String mHos_address = hosShareData.getAddress();
                    String shareUrl = hosShareData.getUrl();
                    String shareImgUrl = hosShareData.getImg();
                    ShareWechat mWechat = hosShareData.getWechat();

                    Log.e(TAG, "mHos_name == " + mHos_name);
                    Log.e(TAG, "mHos_address == " + mHos_address);
                    Log.e(TAG, "shareUrl == " + shareUrl);
                    Log.e(TAG, "shareImgUrl == " + shareImgUrl);
                    Log.e(TAG, "mWechat == " + mWechat);

                    BaseShareView baseShareView = new BaseShareView(mActivity);
                    baseShareView.setShareContent("111").ShareAction(mWechat);
                    baseShareView.getShareBoardlistener()
                            .setSinaText(mHos_name + "," + mHos_address + "," + shareUrl + "@悦美整形APP")
                            .setSinaThumb(new UMImage(mActivity, shareImgUrl))
                            .setSmsText(mHos_name + "，" + mHos_name + "，" + shareUrl)
                            .setTencentUrl(shareUrl)
                            .setTencentTitle(mHos_name)
                            .setTencentThumb(new UMImage(mActivity, shareImgUrl))
                            .setTencentDescription(mHos_address)
                            .setTencentText(mHos_address)
                            .getUmShareListener()
                            .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                                @Override
                                public void onShareResultClick(SHARE_MEDIA platform) {
                                    if (!platform.equals(SHARE_MEDIA.SMS)) {
                                        Toast.makeText(mActivity, " 分享成功啦", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                                }
                            });

                } catch (Exception e) {
                    Log.e(TAG, "e == " + e.toString());
                    e.printStackTrace();
                }
                break;
            case "6753":
                if (Utils.isLoginAndBind(mActivity)) {
                    HashMap<String, Object> parms = new HashMap<>();
                    if (!obj.isNull("obj_type")) {
                        String obj_type = obj.getString("obj_type");
                        parms.put("type", obj_type);
                    }
                    if (!obj.isNull("obj_id")) {
                        String obj_id = obj.getString("obj_id");
                        parms.put("objid", obj_id);
                    }
                    new FocusAndCancelApi().getCallBack(mActivity, parms, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            if ("1".equals(serverData.code)) {
                                try {
                                    FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                                    switch (focusAndCancelData.getIs_following()) {
                                        case "0":          //未关注
                                            mWebView.loadUrl("javascript:changeText(\"关注\")");
                                            break;
                                        case "1":          //已关注
                                            mWebView.loadUrl("javascript:changeText(\"已关注\")");
                                            HashMap<String, Object> hashMap = new HashMap<>();
                                            hashMap.put("hos_id", hosid);
                                            hashMap.put("pos", "9");
                                            new AutoSendApi().getCallBack(mActivity, hashMap, new BaseCallBackListener<ServerData>() {
                                                @Override
                                                public void onSuccess(ServerData s) {
                                                    if ("1".equals(s.code)) {
                                                        Log.e(TAG, s.message);
                                                    }
                                                }
                                            });
                                            HashMap<String, Object> hashMap2 = new HashMap<>();
                                            hashMap2.put("obj_type", "9");
                                            hashMap2.put("obj_id", hosid);
                                            hashMap2.put("hos_id", hosid);
                                            new AutoSendApi2().getCallBack(mActivity, hashMap2, new BaseCallBackListener<ServerData>() {
                                                @Override
                                                public void onSuccess(ServerData s) {
                                                    if ("1".equals(s.code)) {
                                                        Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                                    }
                                                }
                                            });
                                            break;
                                        case "2":          //互相关注
                                            mWebView.loadUrl("javascript:changeText(\"互相关注\")");
                                            HashMap<String, Object> hashMap1 = new HashMap<>();
                                            hashMap1.put("hos_id", hosid);
                                            hashMap1.put("pos", "9");
                                            new AutoSendApi().getCallBack(mActivity, hashMap1, new BaseCallBackListener<ServerData>() {
                                                @Override
                                                public void onSuccess(ServerData s) {
                                                    if ("1".equals(s.code)) {
                                                        Log.e(TAG, s.message);
                                                    }
                                                }
                                            });
                                            HashMap<String, Object> hashMap3 = new HashMap<>();
                                            hashMap3.put("obj_type", "9");
                                            hashMap3.put("obj_id", hosid);
                                            hashMap3.put("hos_id", hosid);
                                            new AutoSendApi2().getCallBack(mActivity, hashMap3, new BaseCallBackListener<ServerData>() {
                                                @Override
                                                public void onSuccess(ServerData s) {
                                                    if ("1".equals(s.code)) {
                                                        Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                                    }
                                                }
                                            });
                                            break;
                                    }

                                    Toast.makeText(mActivity, serverData.message, Toast.LENGTH_SHORT).show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
                break;

        }
    }

    private void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage("     拨打400-056-7118？");
        builder.setPositiveButton("拨打", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                try {
                    ViewInject.toast("正在拨打中·····");
                    Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:4000567118"));
                    if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mActivity.startActivity(it);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        builder.create().show();
    }
}
