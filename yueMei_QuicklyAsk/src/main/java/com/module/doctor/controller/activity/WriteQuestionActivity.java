package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.bigkoo.pickerview.OptionsPickerView;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.commonview.view.CommonTopBar;
import com.module.community.controller.activity.SwitchPhotoPublicIfActivity;
import com.module.doctor.model.api.PartDataApi;
import com.module.doctor.model.bean.PartAskData;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.activity.SelectVideoActivity;
import com.module.my.controller.activity.SetCoverActivity;
import com.module.my.controller.activity.SubmitSuccess1Activity;
import com.module.my.controller.activity.VideoPlayerActivity;
import com.module.my.controller.adapter.ImageUploadAdapter2;
import com.module.my.model.api.PostTextQueApi;
import com.module.other.netWork.netWork.QiNiuConfigration;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.ProvinceBean;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.entity.WriteVideoResult;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.MyUploadImage;
import com.quicklyask.util.Utils;
import com.quicklyask.util.VideoUploadUpyun;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.ProcessImageView;
import com.quicklyask.view.WritePicPopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.xinlan.imageeditlibrary.editimage.EditImageActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.common.util.DensityUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 写提问帖
 *
 * @author Rubin
 */
public class WriteQuestionActivity extends BaseActivity {

    private final String TAG = "WriteQuestionActivity";

    private Activity mContext;

    private GridView gridview;
    private ImageUploadAdapter2 adapter;
    private float dp;

    @BindView(id = R.id.write_que_content_et)
    private EditText postContent;

    @BindView(id = R.id.set_switch_photo_rly)
    private RelativeLayout ifPhotoBt;
    @BindView(id = R.id.set_photo_public_tv)
    private TextView ifPhotoTv;

    @BindView(id = R.id.write_question_tips_photo_rly)
    private RelativeLayout tipsPhoto;

    private String userid = "0";
    private String cateid = "0";

    private String contentStr = "";
    private String visibleStr = "1";

    private boolean isCanNext = false;
    private boolean notClick = false;

    private String uid;

    @BindView(id = R.id.part_ask_select_tv)
    private TextView part_ask_tv;

    private String shengID = "";
    private String cityID = "";

    //视频封面
    @BindView(id = R.id.iv_note_cover)
    private ImageView noteCover;
    @BindView(id = R.id.fl_editor_cover)
    private FrameLayout editorCover;
    @BindView(id = R.id.tv_cover_title)
    private TextView titleCover;

    //视频上传列表
    @BindView(id = R.id.ll_note_video)
    private LinearLayout noteVideo;
    @BindView(id = R.id.rl_note_video)
    private RelativeLayout replaceVideo;
    @BindView(id = R.id.piv_note_video)
    private ProcessImageView thumVideo;
    @BindView(id = R.id.rl_note_picture)
    private RelativeLayout pictureVideo;
    @BindView(id = R.id.rl_video_del)
    private RelativeLayout delVideo;

    @BindView(id = R.id.select_part_rly_bt)
    private RelativeLayout selectPartRly;
    @BindView(id = R.id.select_public_simi_rly_bt)
    private RelativeLayout selectPublicRly;
    @BindView(id = R.id.select_ifpublic_iv)
    private ImageView ifpublicIv;
    @BindView(id = R.id.write_question_top)
    private CommonTopBar mTop;
    private boolean ifPublic = true;

    private List<PartAskData> partAsk1 = new ArrayList<>();

    AskTipsPopupwindows askTipsPop;

    public ArrayList<String> mResults = new ArrayList<>();
    private static final int REQUEST_CODE = 732;
    public static final int ACTION_REQUEST_EDITIMAGE = 9;
    private static final int PHOTO_IF_PUBLIC = 4;
    private static final int VIDEO_REQUEST_CODE = 76;
    private static final int EDITOR_COVER = 79;

    @BindView(id = R.id.tuppppppppppp_fly)
    private FrameLayout tuPicFly;

    @BindView(id = R.id.ll_bottom_container)
    private LinearLayout llBottomContainer;

    private WritePicPopWindow wpicPop;

    OptionsPickerView pvCityOptions;
    private ArrayList<ProvinceBean> optionsCityItems = new ArrayList<>();
    private ArrayList<ArrayList<String>> optionsCityItems2 = new ArrayList<>();
    public HashMap<String, ProcessImageView> processImages;
    private PageJumpManager pageJumpManager;
    private LoadingProgress mDialog;
    private int[] mImageWidthHeight;
    private String mKey;

    private File errorFile = null;
    private String selectNum = "";
    private WriteVideoResult videoResult;
    private String imgCoverPath = "";        //视频封面的本地存储路径
    private int coverImgWidth;
    private int coverImgHeight;
    private String videoDuration = "";
    private boolean videoCoverState = true;     //视频封面是否上传成功
    private JSONObject videoCoverUrl;       //视频封面上传后的网络存储路径
    private String mImgCover = "";          //封面图
    private String mZhipaths;   //到贴纸页传的路径


    @SuppressLint("NewApi")
    @Override
    public void setRootView() {
        setContentView(R.layout.acty_write_question_new);
    }

    Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = WriteQuestionActivity.this;

        mDialog = new LoadingProgress(mContext);
        pageJumpManager = new PageJumpManager(mContext);

        Init();
        initPartData();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //关闭按钮
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (postContent.getText().toString().length() > 0 || mResults.size() > 0) {
                    showDialogExitEdit();
                } else {
                    finish();
                }
            }
        });

        ifPhotoBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent addIntent = new Intent();
                addIntent.putExtra("type", visibleStr);
                addIntent.setClass(mContext, SwitchPhotoPublicIfActivity.class);
                startActivityForResult(addIntent, PHOTO_IF_PUBLIC);
            }
        });

        mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
        mTop.getTv_right().setClickable(false);

        postContent.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                initBtIfClick();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                initBtIfClick();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {

                initBtIfClick();
            }

        });

        postContent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notClick) {
                    Toast.makeText(WriteQuestionActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                }
            }
        });


        selectPublicRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (ifPublic) {
                    ifpublicIv.setBackgroundResource(R.drawable.ask_hidden_yes);
                    ifPublic = false;
                    visibleStr = "1";
                } else {
                    ifpublicIv.setBackgroundResource(R.drawable.ask_hidden_no);
                    ifPublic = true;
                    visibleStr = "0";
                }
            }
        });

        selectPartRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


                View view1 = getWindow().peekDecorView();
                Log.e("TAG", "view1 == " + view1);
                if (view1 != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view1.getWindowToken(), 0);
                }
                Log.e("TAG", "pvCityOptions == " + pvCityOptions);
                if (null != pvCityOptions) {
                    pvCityOptions.show();
                }

            }
        });

        //编辑封面
        editorCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectNum != null && selectNum.length() > 0) {
                    if (videoCoverState) {
                        Intent intent = new Intent(mContext, SetCoverActivity.class);
                        intent.putExtra("selectNum", selectNum);            //本地视频存储路径
                        intent.putExtra("imgCoverPath", imgCoverPath);      //本地封面存储路径
                        startActivityForResult(intent, EDITOR_COVER);
                    } else {
                        uploadCoverImage(imgCoverPath);      //上传失败后重新上传
                    }

                } else {
                    Toast.makeText(mContext, "请先选择视频", Toast.LENGTH_SHORT).show();
                }
            }
        });

        String asti = Cfg.loadStr(mContext, "asktips", "");
        if (asti.length() > 0) {

        } else {
            popAsktips();
        }

    }

    /**
     * 编辑内容是否获取焦点
     *
     * @param focusable
     */
    private void setContentSelected(boolean focusable) {
        postContent.setFocusable(focusable);
        postContent.setFocusableInTouchMode(focusable);
        postContent.requestFocus();
    }

    void popAsktips() {

        Cfg.saveStr(mContext, "asktips", "1");

        new CountDownTimer(150, 100) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                askTipsPop = new AskTipsPopupwindows(WriteQuestionActivity.this, selectPartRly);
                askTipsPop.setAnimationStyle(R.style.PopupAnimation1);
//                askTipsPop.showAsDropDown(selectPartRly, 0, -240);
                if (Build.VERSION.SDK_INT < 24) {
                    askTipsPop.showAsDropDown(selectPartRly, 0, -240);
                } else {
                    // 适配 android 7.0
                    int[] location = new int[2];
                    selectPartRly.getLocationOnScreen(location);
                    int x = location[0];
                    int y = location[1];
                    Log.e("xxxxxx", "x : " + x + ", y : " + y);
                    askTipsPop.showAtLocation(selectPartRly, Gravity.NO_GRAVITY, 0, y - selectPartRly.getHeight() - 20);
                }


                new CountDownTimer(4000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        askTipsPop.dismiss();
                    }
                }.start();
            }
        }.start();
    }

    void initBtIfClick() {
        String textStr = postContent.getText().toString();
        Log.e(TAG, "textStr === " + textStr);
        if (TextUtils.isEmpty(textStr)) {
            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
            mTop.getTv_right().setClickable(false);
        } else {
            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
            mTop.getTv_right().setClickable(true);
        }
    }

    void initPartData() {
        new PartDataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<List<PartAskData>>() {
            @Override
            public void onSuccess(List<PartAskData> partAskData) {
                partAsk1 = partAskData;
                Log.d(TAG, "===>" + partAsk1.size() + partAsk1.toString());
                pvCityOptions = new OptionsPickerView(mContext);

                int it1 = 0;
                int it2 = 0;

                for (int i = 0; i < partAsk1.size(); i++) {
                    optionsCityItems.add(new ProvinceBean(i, partAsk1.get(i).getName(), "", ""));
                    ArrayList<String> options2Items_00 = new ArrayList<>();
                    for (int j = 0; j < partAsk1.get(i).getList().size(); j++) {
                        options2Items_00.add(partAsk1.get(i).getList().get(j).getName());
                    }
                    optionsCityItems2.add(options2Items_00);

                    if (cateid.length() > 1) {
                        if (partAsk1.get(i).getName().equals(cateid)) {
                            shengID = partAsk1.get(i).get_id();
                            it1 = i;
                            for (int j = 0; j < partAsk1.get(i).getList().size(); j++) {
                                options2Items_00.add(partAsk1.get(i).getList().get(j).getName());
                            }
                        }
                    }
                }
                Log.d(TAG, "optionsCityItems" + optionsCityItems.toString());
                Log.d(TAG, "optionsCityItems2" + optionsCityItems2.toString());
                pvCityOptions.setPicker(optionsCityItems, optionsCityItems2, true);

                pvCityOptions.setTitle("请选择部位");
                pvCityOptions.setCyclic(false, false, true);

                pvCityOptions.setSelectOptions(it1, it2, 1);
                pvCityOptions.setCancelable(true);
                pvCityOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3) {
                        //返回的分别是三个级别的选中位置
                        String tx1 = optionsCityItems.get(options1).getPickerViewText();
                        String tx2 = optionsCityItems2.get(options1).get(option2);
                        part_ask_tv.setText(tx1 + "," + tx2);

                        shengID = partAsk1.get(options1).get_id();
                        cityID = partAsk1.get(options1).getList().get(option2).get_id();

                        cateid = shengID + "," + cityID;
                    }
                });
            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        uid = Utils.getUid();
    }

    public void Init() {
        Intent it = getIntent();
        if (it != null) {
            userid = it.getStringExtra("userid");
            cateid = it.getStringExtra("cateid");
            Log.e(TAG, "cateid === " + cateid);
        }

        dp = getResources().getDimension(R.dimen.dp);
        gridview = findViewById(R.id.noScrollgridview2);
        gridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridviewInit();

        /**
         * 提交
         */
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (notClick) {
                    Toast.makeText(WriteQuestionActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    uid = Utils.getUid();
                    if (Utils.isLogin()) {
                        if (Utils.isBind()) {
                            String content = postContent.getText().toString().trim();
                            contentStr = content;

                            Matcher matcher = emoji.matcher(contentStr);

                            if (matcher.find()) {
                                Toast.makeText(WriteQuestionActivity.this, "提交失败，请去掉非悦美表情或其他特殊字符后在尝试提交", Toast.LENGTH_SHORT).show();
                            } else {
                                if (content.length() > 0 && !"".equals(content)) {
                                    if (content.length() > 15) {
                                        mTop.getTv_right().setClickable(false);
                                        mDialog.startLoading();
                                        postTextQue();
                                    } else {
                                        ViewInject.toast("亲，内容至少要大于15个字哟！");
                                    }
                                } else {
                                    ViewInject.toast("内容不能为空！");
                                }
                            }
                        } else {
                            Utils.jumpBindingPhone(mContext);

                        }

                    } else {
                        Utils.jumpLogin(mContext);
                    }
                }
            }
        });
    }

    Button cancelBt;
    Button trueBt;

    void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        cancelBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
        trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
                finish();
            }
        });

    }

    public static final int START_S_C = 1;      //上传开始
    public static final int JIN_DU = 2;         //上传进度更逊
    public static final int WANCHENG_S_C = 3;   //上传完成（成功）
    public static final int SHIBAI_S_C = 4;     //上传失败
    public static final int WANCHENG_C_X = 5;     //重新上传成功（包括贴纸修改上传、上传失败重新上传）
    public static final int SHIBAI_C_X = 6;     //重新上传失败（包括贴纸修改上传、上传失败重新上传）
    public static final int VIDEO_PROGRESS = 7;     //视频上传进度
    public static final int VIDEO_SUCCESS = 8;     //视频上传成功
    public static final int VIDEO_FAILURE = 9;     //视频上传失败

    HashMap<String, Object> mSameData = new HashMap<>();            //图片上传完成后返回的图片地址集合。key是本地存储路径，vle是服务器连接
    HashMap<String, String> mErrorImg = new HashMap<>();            //图片上传失败后保存的一个集合。key：本地存储路径，vle：本地存储路径
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case START_S_C:             //开始上传（包括图片返回上传、贴纸修改的上传、重新上传）
                    String lianXu = (String) msg.obj;

                    if ("0".equals(lianXu)) {

                        postFileQue(msg.arg1, true);

                    } else if ("1".equals(lianXu)) {

                        postFileQue(msg.arg1, false);

                    }

                    break;
                case JIN_DU:                                                                //上传进度
                    int progress = msg.arg1;
                    ProcessImageView mImgs = (ProcessImageView) msg.obj;
                    mImgs.startHua(ProcessImageView.SAHNG_CHUAN_ZHONG, progress);             //开始画图

                    break;
                case WANCHENG_S_C:  //完成上传（成功）
                    int pos = msg.arg1;
                    JSONObject jsonObject = setJson();
                    mSameData.put(mResults.get(pos), jsonObject);            //设置要上传的集合值
                    ProcessImageView imgView = processImages.get(mResults.get(pos));
                    imgView.startHua(ProcessImageView.WANC_HENG, 100);      //设置图片UI

                    if (pos != mResults.size() - 1) {     //不是最后一个,继续上传
                        mySendMessage(pos + 1, "0", START_S_C);
                    } else {
                        notClick = false;           //设置按钮可以点击
                        setContentSelected(true);
                    }
                    adapter.notifyDataSetChanged();
                    break;

                case SHIBAI_S_C:  //上传失败
                    int posSb = msg.arg1;
                    mErrorImg.put(mResults.get(posSb), mResults.get(posSb));      //上传失败后设置失败图片的路径

                    Log.e("TAG", "mErrorImg主 == " + mErrorImg.size());
                    ProcessImageView imgViewSb = processImages.get(mResults.get(posSb));
                    imgViewSb.startHua(ProcessImageView.SHI_BAI, 0);             //设置图片UI

                    if (posSb != mResults.size() - 1) {     //不是最后一个,继续上传
                        mySendMessage(posSb + 1, "0", START_S_C);
                    } else {
                        notClick = false;       //设置按钮可以点击
                        setContentSelected(true);
                    }

                    adapter.notifyDataSetChanged();
                    break;

                case WANCHENG_C_X:                 //重新上传后成功（包括贴纸返回，图片重新上传）
                    int pos1 = msg.arg1;
                    JSONObject jsonObject1 = setJson();
                    mSameData.put(mResults.get(pos1), jsonObject1);            //设置要上传的集合值

                    if (mErrorImg.get(mResults.get(pos1)) != null) {
                        mErrorImg.remove(mResults.get(pos1));               //如果这个是上传失败重新上传的图片，那么删除失败集合的数据
                    }

                    ProcessImageView imgView1 = processImages.get(mResults.get(pos1));
                    imgView1.startHua(ProcessImageView.WANC_HENG, 100);              //设置图片UI

                    notClick = false;
                    setContentSelected(true);

                    adapter.notifyDataSetChanged();
                    break;

                case SHIBAI_C_X:               //重新上传后失败（包括贴纸返回，图片重新上传）
                    int posSb1 = msg.arg1;
                    mErrorImg.put(mResults.get(posSb1), mResults.get(posSb1));      //上传失败后设置失败图片的路径

                    ProcessImageView imgViewSb1 = processImages.get(mResults.get(posSb1));
                    imgViewSb1.startHua(ProcessImageView.SHI_BAI, 0);             //设置图片UI

                    notClick = false;
                    setContentSelected(true);
                    break;
                case VIDEO_PROGRESS:               //视频上传进度

                    int prog = msg.arg1;
                    Log.e(TAG, "prog == " + prog);
                    thumVideo.startHua(ProcessImageView.SAHNG_CHUAN_ZHONG, prog, true);
                    if (prog == 100) {
                        notClick = false;
                        setContentSelected(true);
                    }

                    break;
                case VIDEO_SUCCESS:                 //视频上传成功

                    videoResult = (WriteVideoResult) msg.obj;

                    thumVideo.startHua(ProcessImageView.WANC_HENG, 100, true);

                    errorFile = null;

                    notClick = false;
                    setContentSelected(true);
                    break;

                case VIDEO_FAILURE:                 //视频上传失败

                    errorFile = (File) msg.obj;
                    Log.e("GGG", "errorFile == " + errorFile);
                    thumVideo.startHua(ProcessImageView.SHI_BAI, 0, true);

                    notClick = false;
                    setContentSelected(true);
                    break;
            }
        }
    };

    private JSONObject setJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("width", mImageWidthHeight[0]);
            jsonObject.put("height", mImageWidthHeight[1]);
            jsonObject.put("img", mKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * 初始化图片列表形式
     */
    public void gridviewInit() {
        adapter = new ImageUploadAdapter2(this, mResults, mSameData, mErrorImg, mImgCover);

        setGridViewSize();

        gridview.setAdapter(adapter);
        llBottomContainer.setBackgroundColor(Color.parseColor("#EEF1EFF5"));
        //删除回调
        adapter.setOnItemDeleteClickListener(new ImageUploadAdapter2.onItemDeleteListener() {
            @Override
            public void onDeleteClick(int i) {
                if (notClick) {
                    Toast.makeText(WriteQuestionActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (mErrorImg.get(mResults.get(i)) != null) {
                        mErrorImg.remove(mResults.get(i));
                    }

                    if (mSameData.get(mResults.get(i)) != null) {
                        mSameData.remove(mResults.get(i));
                    }

                    mResults.remove(i);

                    if (mResults.size() > 0) {
                        mImgCover = mResults.get(0);
                    } else {
                        mImgCover = "";
                    }

                    gridviewInit();
                }
            }
        });

        gridview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, final int pos, long arg3) {
                if (notClick) {
                    Toast.makeText(WriteQuestionActivity.this, "图片正在上传，请稍候再试...", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.hideSoftKeyboard(mContext);
                    //版本判断
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(WriteQuestionActivity.this).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {        //判断权限是否开启
                                toXIangce(pos);
                            }

                            @Override
                            public void onDenied(List<String> permissions) {
//									Toast.makeText(WriteQuestionActivity.this, "没有权限", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        toXIangce(pos);
                    }
                }
            }
        });
    }

    /**
     * 设置gritView的宽高
     */
    private void setGridViewSize() {

        int size = 0;
        if (mResults.size() < 9) {
            size = mResults.size() + 1;
        } else {
            size = mResults.size();
        }

        if (mResults.size() == 0) {
            tipsPhoto.setVisibility(View.VISIBLE);
        } else {
            tipsPhoto.setVisibility(View.GONE);
        }

        LayoutParams params = gridview.getLayoutParams();
        int sizeType = size > 4 ? size > 8 ? 3 : 2 : 1;
        int height = (sizeType * (int) (dp * 9.4f)) + (sizeType == 3 ? 12 : sizeType == 3 ? 30 : sizeType == 2 ? 20 : 10);
        params.height = height;                             //设置高度
        gridview.setLayoutParams(params);
        gridview.setColumnWidth((int) (dp * 9.4f));         //设置列宽
    }


    void toXIangce(int pos) {
        if (pos == mResults.size()) {           //视频
            if (mResults.size() > 0) {
                showDialogExitEdit3("一篇日记内容只能选择视频或图片一种形式", "我知道了");
            } else {
                if (errorFile != null) {                              //如果是上传失败的
                    gridviewVideoInit(selectNum);       //视频上传
                } else {
                    Intent intent = new Intent(mContext, SelectVideoActivity.class);
                    intent.putExtra("selectNum", selectNum);
                    startActivityForResult(intent, VIDEO_REQUEST_CODE);
                    overridePendingTransition(R.anim.activity_open, 0);
                }
            }

        } else if (pos == mResults.size() + 1) {
            String sdcardState = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {

                pageJumpManager.jumpToImagesSelectorActivity(REQUEST_CODE, 9, 50000, true, mResults);

            } else {
                Toast.makeText(getApplicationContext(), "sdcard已拔出，不能选择照片", Toast.LENGTH_SHORT).show();
            }

        } else {

            mZhipaths = mResults.get(pos);

            if (mErrorImg.get(mZhipaths) != null) {          //是上传失败的图片
                Log.e("TAG", "是上传失败的图片");
                mySendMessage(pos, "1", START_S_C);        //发送不连续上传的消息

            } else {
                Intent it = new Intent(WriteQuestionActivity.this, EditImageActivity.class);
                it.putExtra(EditImageActivity.FILE_PATH, mZhipaths);
                it.putExtra(EditImageActivity.EXTRA_COVER, "1");
                File outputFile = FileUtils.getEmptyFile("yuemei" + System.currentTimeMillis() + ".jpg");
                it.putExtra(EditImageActivity.EXTRA_OUTPUT, outputFile.getAbsolutePath());
                it.putExtra("pos", pos + "");
                if (mImgCover.equals(mZhipaths)) {
                    it.putExtra("isCover", true);
                } else {
                    it.putExtra("isCover", false);
                }
                startActivityForResult(it, ACTION_REQUEST_EDITIMAGE);
            }
        }
        initBtIfClick();
    }

    /**
     * 视频的适配器
     *
     * @param path:视频本地路径
     */
    private void gridviewVideoInit(final String path) {

        tipsPhoto.setVisibility(View.GONE);
        setContentSelected(false);
        notClick = true;

        Glide.with(mContext).load(Uri.fromFile(new File(path))).into(thumVideo);

        thumVideo.startHua(ProcessImageView.WANC_HENG, 0);

        //上传视频到又拍云
        VideoUploadUpyun.getVideoUploadUpyun(mContext, mHandler).uploadVideo(selectNum);

        //上传视频封面到悦美服务器（获取默认第一帧的图片）
        savePicture(path);

        //重新选择视频
        replaceVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "视频上传中请稍后再试...", Toast.LENGTH_SHORT).show();
                } else {
                    if (errorFile != null && errorFile.length() > 0) {
                        setContentSelected(false);
                        notClick = true;
                        VideoUploadUpyun.getVideoUploadUpyun(mContext, mHandler).uploadVideo(selectNum);  //重新上传
                    } else {
                        Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                        intent.putExtra("selectNum", selectNum);
                        Log.e(TAG, "videoDuration === " + videoDuration);
                        intent.putExtra("duration", videoDuration);
                        startActivity(intent);
                    }
                }
            }
        });

        //图片点击按钮
        pictureVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "视频上传中请稍后再试...", Toast.LENGTH_SHORT).show();
                } else {
                    showDialogExitEdit3("一篇日记内容只能选择视频或图片一种形式", "我知道了");
                }
            }
        });

        //删除当前图片
        delVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notClick) {
                    Toast.makeText(mContext, "视频上传中请稍后再试...", Toast.LENGTH_SHORT).show();
                } else {
                    videoResult = null;
                    tipsPhoto.setVisibility(View.VISIBLE);
                    selectNum = "";
                    errorFile = null;
                    noteVideo.setVisibility(View.GONE);
                    gridview.setVisibility(View.VISIBLE);
                    editorCover.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 保存默认封面图图片
     *
     * @param path:视频路径
     * @return 第一帧图片保存路径
     */
    private void savePicture(String path) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        Bitmap bitmap = retriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);    //获取第一帧的数据

        int imgWidth = bitmap.getWidth();
        int imgHeight = bitmap.getHeight();

        //术后封面图宽高比设置
        coverImgHeight = DensityUtil.dip2px(99);
        coverImgWidth = imgWidth * coverImgHeight / imgHeight;

        ViewGroup.LayoutParams l = noteCover.getLayoutParams();
        l.width = coverImgWidth;
        l.height = coverImgHeight;
        noteCover.setLayoutParams(l);

        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);   // 建立这个路径的文件或文件夹
        if (!path1.exists()) {      // 如果不存在，就建立文件夹
            path1.mkdirs();
        }

        String pathPic = pathS + "/yuemei_" + System.currentTimeMillis() + ".jpg";

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pathPic);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();

            imgCoverPath = pathPic;

//            Picasso.with(mContext)                               //视频封面
//                    .load(new File(imgCoverPath)).into(noteCover);
            Glide.with(mContext)                               //视频封面
                    .load(new File(imgCoverPath)).into(noteCover);

            uploadCoverImage(imgCoverPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传视频封面图
     */
    private void uploadCoverImage(String path) {
        String qiniutoken = Cfg.loadStr(mContext, FinalConstant.QINIUTOKEN, "");
        titleCover.setText("上传封面中");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        File file = new File(path);
        mImageWidthHeight = FileUtils.getImageWidthHeight(path);
        mKey = QiNuConfig.getKey();
        uploadManager.put(file, mKey, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {
                if (info.isOK()) {
                    Log.e(TAG, "Upload Success");
                    JSONObject jsonObject = setJson();
                    titleCover.setText("编辑视频封面");
                    videoCoverState = true;
                    videoCoverUrl = jsonObject;
                } else {
                    try {
                        if (response != null) {
                            String error = response.getString("error");
                            if ("expired token".equals(error)) {
                                new QiNiuTokenApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        if ("1".equals(serverData.code)) {
                                            try {
                                                QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                Cfg.saveStr(mContext, FinalConstant.QINIUTOKEN, qiNiuBean.getQiniu_token());
                                                uploadCoverImage(imgCoverPath);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else {
                                Log.e(TAG, "Upload Fail");
                                titleCover.setText("上传失败，点击重试");
                                videoCoverState = false;
                            }
                        } else {
                            Log.e(TAG, "Upload Fail");
                            titleCover.setText("上传失败，点击重试");
                            videoCoverState = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                Log.e(TAG, "key==" + key + "info==" + info + "response==" + response);
            }
        }, null);

    }


    //dialog提示
    void showDialogExitEdit3(String title, String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(title);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(content);
        cancelBt88.setTextColor(Color.parseColor("#ffa5cc"));
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PHOTO_IF_PUBLIC:
                if (data != null) {
                    String type = data.getStringExtra("type");
                    if (type.equals("1")) {
                        ifPhotoTv.setText("私密");
                        visibleStr = "1";
                    } else {
                        ifPhotoTv.setText("公开");
                        visibleStr = "0";
                    }
                }
                break;
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);

                        if (mResults != null && mResults.size() > 0) {
                            if (mImgCover == null || "".equals(mImgCover)) {
                                mImgCover = mResults.get(0);
                            }
                        }

                        gridviewInit();

                        pictureReorder();

                        if (mResults.size() > 0) {
                            new CountDownTimer(350, 100) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    if (Utils.isValidContext(mContext)) {

                                        String ispop = Cfg.loadStr(mContext, "poppop", "");
                                        if (ispop.equals("1")) {

                                        } else {
                                            wpicPop = new WritePicPopWindow(mContext);
//                                            wpicPop.showAsDropDown(tuPicFly, 0, -300);
                                            if (Build.VERSION.SDK_INT < 24) {
                                                wpicPop.showAsDropDown(tuPicFly, 0, -300);
                                            } else {
                                                // 适配 android 7.0
                                                int[] location = new int[2];
                                                tuPicFly.getLocationOnScreen(location);
                                                int x = location[0];
                                                int y = location[1];
                                                Log.e("xxxxxx", "x : " + x + ", y : " + y);
                                                wpicPop.showAtLocation(tuPicFly, Gravity.NO_GRAVITY, 0, y - tuPicFly.getHeight() / 2 - 20);
                                            }

                                            Cfg.saveStr(mContext, "poppop", "1");
                                        }
                                    }
                                }
                            }.start();
                        }
                    }
                }
                break;
            case ACTION_REQUEST_EDITIMAGE:  //添加贴纸后的回调
                if (null != data) {
                    String newFilePath = data.getStringExtra("save_file_path");
                    String poss = data.getStringExtra("pos");
                    String dele = data.getStringExtra("dele");
                    boolean isCover = data.getBooleanExtra("isCover", false);               //当前这个

                    if (dele.equals("0")) {
                        mResults.set(Integer.parseInt(poss), newFilePath);
                        if (isCover) {
                            mImgCover = newFilePath;
                        } else {
                            if (mImgCover.equals(newFilePath)) {
                                mImgCover = mResults.get(0);
                            }
                        }

                        gridviewInit();
                        if (!mZhipaths.equals(newFilePath)) {
                            mSameData.remove(mResults.get(Integer.parseInt(poss)));         //删除成功后的图片集合(不管是删除还是修改都是要删除的)
                            mySendMessage(Integer.parseInt(poss), "1", START_S_C);
                        }
                    } else {
                        mSameData.remove(mResults.get(Integer.parseInt(poss)));         //删除成功后的图片集合(不管是删除还是修改都是要删除的)
                        mResults.remove(Integer.parseInt(poss));                        //删除集合中的数据

                        Log.e(TAG, "mResults == " + mResults.size());
                        Log.e(TAG, "mResults == " + mResults);
                        if (isCover && mResults.size() > 0) {
                            mImgCover = mResults.get(0);
                        }

                        gridviewInit();
                    }
                }
                break;

            case VIDEO_REQUEST_CODE:                //视频的回调

                String selectType = "";
                if (data != null) {
                    selectType = data.getStringExtra("selectNum");
                }

                if (selectType != null && selectType.length() > 0) {
                    videoDuration = data.getStringExtra("duration");
                    Log.e(TAG, "videoDuration111 == " + videoDuration);
                    selectNum = selectType;
                    editorCover.setVisibility(View.VISIBLE);

                    noteVideo.setVisibility(View.VISIBLE);
                    gridview.setVisibility(View.GONE);

                    gridviewVideoInit(selectNum);       //视频上传
                }

                break;

            case EDITOR_COVER:              //编辑视频封面
                imgCoverPath = data.getStringExtra("imgPath");

                ViewGroup.LayoutParams l = noteCover.getLayoutParams();
                l.width = coverImgWidth;
                l.height = coverImgHeight;
                noteCover.setLayoutParams(l);

                Glide.with(mContext).load(imgCoverPath).into(noteCover);

                uploadCoverImage(imgCoverPath);      //自己选择的封面上传
                break;
        }
    }

    /**
     * 图片重新排序
     */
    private void pictureReorder() {
        int pos = 0;
        if (mSameData.size() == 0 && mErrorImg.size() == 0) {     //如果是第一次添加图片
            mySendMessage(pos, "0", START_S_C);
        } else {
            for (int i = 0; i < mResults.size(); i++) {
                if (mSameData.get(mResults.get(i)) == null && mErrorImg.get(mResults.get(i)) == null) {       //说明是新增的图片
                    pos = i;
                    break;
                }
            }
            mySendMessage(pos, "0", START_S_C);
        }

    }

    /**
     * 发送图片上传的消息
     *
     * @param arg：第几个位置
     * @param lianXu：是否连续上传：0连续上传，1不连续上传
     * @param state：状态
     */
    private void mySendMessage(int arg, String lianXu, int state) {
        notClick = true;
        setContentSelected(false);
        Message msg = Message.obtain();
        msg.arg1 = arg;
        msg.obj = lianXu;
        msg.what = state;
        mHandler.sendMessage(msg);
    }

    /**
     * 上传图片地址和文字
     */
    private void postFileQue(int pos, boolean isLianXu) {
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + pos + System.currentTimeMillis() + ".jpg");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults.get(pos), desPath);
        // 压缩图片

        processImages = adapter.getProcessImage();
        ProcessImageView image = processImages.get(mResults.get(pos));
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
        mKey = QiNuConfig.getKey();
        MyUploadImage.getMyUploadImage(WriteQuestionActivity.this, pos, mHandler, desPath, image, isLianXu).uploadImage(mKey);
    }

    /**
     * 上传图片文件
     */
    private void postTextQue() {
        mTop.getTv_right().setEnabled(false);

        ArrayList<Object> typeData = new ArrayList<>();

        for (int i = 0; i < mResults.size(); i++) {
            if (mSameData.get(mResults.get(i)) != null) {
                typeData.add(mSameData.get(mResults.get(i)));
            }
        }

        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", Utils.getUid());
        maps.put("cateid", cateid + ",1090");
        maps.put("content", contentStr);
        maps.put("visibility", visibleStr);
        maps.put("image", typeData.toString());
        String cityss = Cfg.loadStr(mContext, "city_dingwei", "");
        if (cityss.length() > 0) {
            maps.put("city", cityss);
        }
        new PostTextQueApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                mTop.getTv_right().setEnabled(true);
                if ("1".equals(serverData.code)) {
                    WriteResultData data = JSONUtil.TransformWriteResult(serverData.data);
                    ViewInject.toast(serverData.message);
                    MyToast.makeImgToast(mContext, getResources().getDrawable(R.drawable.tips_submit_success2x), 1000).show();

                    String ifoneLogin = data.getOnelogin();
                    String url = data.getAppmurl();
                    String qid = data.get_id();
                    String appmurl = data.getAppmurl();
                    String isFirst2 = Cfg.loadStr(mContext, FinalConstant.ISFIRST2, "");
                    if (isFirst2.equals("1")) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                        finish();
                    } else {
                        if (ifoneLogin.equals("1")) {// 第一次登录发帖
                            Intent it = new Intent();
                            it.setClass(mContext, SubmitSuccess1Activity.class);
                            it.putExtra("url", url);
                            it.putExtra("qid", qid);
                            startActivity(it);
                            Cfg.saveStr(mContext, FinalConstant.ISFIRST2, "1");
                            finish();
                        } else {
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                            finish();
                        }
                    }
                    sumitHttpCode("8");
                } else {
                    ViewInject.toast(serverData.message);

                    StatService.onEvent(WriteQuestionActivity.this, "017", "提问帖", 1);
                }
            }
        });

    }


    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        sumitHttpAip.getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, 1000).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mContext, jifenNu, 1000).show();
                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mContext, jyNu, 1000).show();
                            }
                        }
                    }
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (postContent.getText().toString().length() > 0 || mResults.size() > 0) {
                showDialogExitEdit();
            } else {
                finish();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
        String textStr = postContent.getText().toString();
        if (textStr.length() != 0 && !isCanNext) {
            isCanNext = true;
            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.title_red_new));
            mTop.getTv_right().setClickable(true);
        }
        if (textStr.length() == 0) {
            isCanNext = false;
            mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
            mTop.getTv_right().setClickable(false);
        }
    }

    /**
     * @author Rubin
     */
    public class AskTipsPopupwindows extends PopupWindow {

        public AskTipsPopupwindows(Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.pop_ask_tips, null);

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            // showAsDropDown(v, 0, 0);
            update();

            LinearLayout wanmeiLy = view.findViewById(R.id.pop_home_wanto_mei_ly);

            // 点击 之外 消失
            view.setOnTouchListener(new OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.pop_home_wanto_mei_ly).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y > height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        if (askTipsPop != null) {
            askTipsPop.dismiss();
        }
        super.onDestroy();
    }
}
