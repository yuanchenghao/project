package com.module.doctor.controller.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.community.controller.adapter.CommunityPostAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.model.api.DoctorDetailBBsApi;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 医生说页面
 */
public class DoctorSaidActivity extends YMBaseActivity {

    @BindView(R.id.doctor_said_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.doctor_said_list)
    RecyclerView mList;
    private DoctorDetailBBsApi mDoctorDetailApi;
    private int mPage = 1;
    private String mDocId;
    private CommunityPostAdapter communityPostAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_doctor_said;
    }

    @Override
    protected void initView() {
        mDocId = getIntent().getStringExtra("id");

        //刷新加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                communityPostAdapter = null;
                loadingData();
            }
        });
    }

    @Override
    protected void initData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mList.setLayoutManager(linearLayoutManager);

        mDoctorDetailApi = new DoctorDetailBBsApi();
        loadingData();
    }


    private void loadingData() {

        mDoctorDetailApi.addData("id", mDocId);
        mDoctorDetailApi.addData("page", mPage + "");
        mDoctorDetailApi.getCallBack(mContext, mDoctorDetailApi.getHashMap(), new BaseCallBackListener<ArrayList<BBsListData550>>() {
            @Override
            public void onSuccess(ArrayList<BBsListData550> data) {
                mPage++;

                mRefresh.finishRefresh();
                if (data.size() == 0) {
                    mRefresh.finishLoadMoreWithNoMoreData();
                } else {
                    mRefresh.finishLoadMore();
                }

                if (communityPostAdapter == null) {
                    communityPostAdapter = new CommunityPostAdapter(mContext, data);
                    mList.setAdapter(communityPostAdapter);

                    communityPostAdapter.setOnItemCallBackListener(new CommunityPostAdapter.ItemCallBackListener() {
                        @Override
                        public void onItemClick(View v, int pos) {
                            List<BBsListData550> bBsListData550s = communityPostAdapter.getmData();
                            String url = bBsListData550s.get(pos).getAppmurl();
                            if (!TextUtils.isEmpty(url)) {
                                if ("404".equals(communityPostAdapter.getmData().get(pos).getAskorshare())) {
                                    mFunctionManager.showShort("该帖子已被删除");
                                } else {
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                                }
                            }
                        }
                    });

                } else {
                    communityPostAdapter.addData(data);
                }
            }
        });
    }
}
