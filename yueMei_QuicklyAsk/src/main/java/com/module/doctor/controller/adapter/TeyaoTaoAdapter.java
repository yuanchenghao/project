package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dwb on 17/2/14.
 */

public class TeyaoTaoAdapter extends BaseAdapter {

    private final String TAG = "TeyaoTaoAdapter";

    private List<HomeTaoData> mDoctorData = new ArrayList<HomeTaoData>();
    private Context mContext;
    private LayoutInflater inflater;
    private HomeTaoData doctorData;
    ViewHolder viewHolder;


    public TeyaoTaoAdapter(Context mContext, List<HomeTaoData> mDoctorData) {
        this.mContext = mContext;
        this.mDoctorData = mDoctorData;
        inflater = LayoutInflater.from(mContext);

    }

    static class ViewHolder {
        public TextView mTipsNumTv;
        public TextView mTitleTv;
        public TextView mSubTitleTv;
        public TextView mJiaTv;
        public TextView mJiaQiTv;
        public TextView mGuiGeTv;
        public TextView mGuiGeTv1;
        public LinearLayout mPlusVibility;
        public TextView mPlusPrice;
    }

    @Override
    public int getCount() {
        return mDoctorData.size();
    }

    @Override
    public Object getItem(int position) {
        return mDoctorData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({ "NewApi", "InlinedApi" })
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_teyao_doc_tao, null);
            viewHolder = new ViewHolder();

            viewHolder.mTipsNumTv= convertView.findViewById(R.id.teyao_tipsnum_tv);
            viewHolder.mTitleTv= convertView.findViewById(R.id.teyao_tao_title_tv);
            viewHolder.mSubTitleTv= convertView.findViewById(R.id.teyao_tao_subtitle_tv);
            viewHolder.mJiaTv= convertView.findViewById(R.id.teyao_tao_jia_tv);
            viewHolder.mJiaQiTv= convertView.findViewById(R.id.teyao_tao_jiaqi_tv);
            viewHolder.mGuiGeTv= convertView.findViewById(R.id.teyao_tao_guige_tv);
            viewHolder.mGuiGeTv1= convertView.findViewById(R.id.teyao_tao_guige_tv1);
            viewHolder.mPlusVibility= convertView.findViewById(R.id.teyao_doc_plus_vibility);
            viewHolder.mPlusPrice= convertView.findViewById(R.id.teyao_doc_plus_price2);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        doctorData = mDoctorData.get(position);

        if(null!=doctorData.getRate()&&doctorData.getRate().length()>0){
            viewHolder.mTipsNumTv.setVisibility(View.VISIBLE);
            viewHolder.mTipsNumTv.setText(doctorData.getRate());
        }else {
            viewHolder.mTipsNumTv.setVisibility(View.GONE);
        }

        viewHolder.mTitleTv.setText(doctorData.getTitle());
        viewHolder.mSubTitleTv.setText(doctorData.getSubtitle());
        viewHolder.mJiaTv.setText("￥"+ doctorData.getPrice_discount());
        String price = doctorData.getPrice_discount() + doctorData.getFeeScale();
        if(price.length()>10){
            viewHolder.mGuiGeTv1.setVisibility(View.GONE);
            viewHolder.mGuiGeTv.setVisibility(View.VISIBLE);
            viewHolder.mGuiGeTv.setText(doctorData.getFeeScale());
        }else {
            viewHolder.mGuiGeTv1.setVisibility(View.VISIBLE);
            viewHolder.mGuiGeTv.setVisibility(View.GONE);
            viewHolder.mGuiGeTv1.setText(doctorData.getFeeScale());
        }
        String member_price = doctorData.getMember_price();
        int i = Integer.parseInt(member_price);
        if (i >= 0){
            viewHolder.mPlusVibility.setVisibility(View.VISIBLE);
            viewHolder.mPlusPrice.setText("¥"+member_price);
        }else {
            viewHolder.mPlusVibility.setVisibility(View.GONE);
        }
        String qi=doctorData.getPrice_range_max();
        if(Integer.parseInt(qi)>0){
            viewHolder.mJiaQiTv.setVisibility(View.VISIBLE);
        }else {
            viewHolder.mJiaQiTv.setVisibility(View.GONE);
        }

        convertView.setBackgroundResource(R.color.white);
        return convertView;
    }

    public void add(List<HomeTaoData> infos) {
        mDoctorData.addAll(infos);
    }
}