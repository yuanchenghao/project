package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.HospitalTop;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import org.xutils.image.ImageOptions;

import java.util.ArrayList;
import java.util.List;

public class DoctorListAdpter extends BaseAdapter {

    private final String TAG = "DoctorListAdpter";

    private List<DocListData> mDoctorData = new ArrayList<>();
    private Activity mContext;
    private LayoutInflater inflater;
    private DocListData doctorData;
    ViewHolder viewHolder;

    private Animation ain;
    ImageOptions imageOptions;

    public DoctorListAdpter(Activity mContext, List<DocListData> mDoctorData) {
        this.mContext = mContext;
        this.mDoctorData = mDoctorData;
        inflater = LayoutInflater.from(mContext);

        ain = AnimationUtils.loadAnimation(mContext, R.anim.push_in);
        imageOptions = new ImageOptions.Builder().setCircular(true).setAnimation(ain).setConfig(Bitmap.Config.ARGB_8888).setPlaceholderScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();
    }

    static class ViewHolder {
        public ImageView docHeadPic;
        public ImageView docYueIv;
        public ImageView docPeiIv;
        public TextView docNameTV;
        public TextView docHosptialTV;
        public LinearLayout docHosptialTagContainer;
        public TextView docHosptialTagName;
        public TextView docHosptialLevel;
        public TextView docTitlelTV;
        public RatingBar ratBar;
        public TextView mscoreTv;
        public TextView myudingTv;
        public ImageView mYuemeiIv;

        public TextView docTao1NameTv;
        public TextView docTao2NameTv;
        public ImageView docTao1Iv;
        public ImageView docTao2Iv;
        public TextView docTao1PriceTv;
        public TextView docTao2PriceTv;
        public TextView docMoreTaoTv;
        public LinearLayout docTao1Content;
        public LinearLayout docTao2Content;
        public LinearLayout docPlusVibility;
        public LinearLayout docPlusVibility2;
        public TextView docPlusPrice;
        public TextView docPlusPrice2;

        public int flag;

    }

    @Override
    public int getCount() {
        return mDoctorData.size();
    }

    @Override
    public Object getItem(int position) {
        return mDoctorData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"NewApi", "InlinedApi"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        doctorData = mDoctorData.get(position);

        if (convertView == null || ((ViewHolder) convertView.getTag()).flag != position) {

            convertView = inflater.inflate(R.layout.doc_list_item_view1, null);
            viewHolder = new ViewHolder();
            viewHolder.flag = position;

            viewHolder.docHeadPic = convertView.findViewById(R.id.doc_list_head_image_iv);
            viewHolder.docYueIv = convertView.findViewById(R.id.doc_list_item_cooperation_iv);
            viewHolder.docPeiIv = convertView.findViewById(R.id.doc_list_item_pei_iv);
            viewHolder.docNameTV = convertView.findViewById(R.id.doc_list_item_name_tv);
            viewHolder.docTitlelTV = convertView.findViewById(R.id.doc_list_item_title);
            viewHolder.docHosptialTV = convertView.findViewById(R.id.doc_list_item_hspital_name);
            viewHolder.docHosptialTagContainer = convertView.findViewById(R.id.doc_list_item_tagcontianer);
            viewHolder.docHosptialTagName = convertView.findViewById(R.id.doc_list_item_tagtitle);
            viewHolder.docHosptialLevel = convertView.findViewById(R.id.doc_list_item_level);


            viewHolder.docTao1NameTv = convertView.findViewById(R.id.doc_list_tao1_name_tv);
            viewHolder.docTao2NameTv = convertView.findViewById(R.id.doc_list_tao2_name_tv);
            viewHolder.docTao1PriceTv = convertView.findViewById(R.id.doc_list_tao1_jg_tv);
            viewHolder.docTao2PriceTv = convertView.findViewById(R.id.doc_list_tao2_jg_tv);
//            viewHolder.docMoreTaoTv = convertView.findViewById(R.id.doc_list_more_tao_tv);

            viewHolder.docTao1Content = convertView.findViewById(R.id.doc_list_tao1_ly);
            viewHolder.docTao2Content = convertView.findViewById(R.id.doc_list_tao2_ly);

            viewHolder.docPlusVibility = convertView.findViewById(R.id.doc_list_plus_vibiliyt);
            viewHolder.docPlusVibility2 = convertView.findViewById(R.id.doc_list_plus_vibiliyt2);
            viewHolder.docPlusPrice = convertView.findViewById(R.id.doc_plus_price);
            viewHolder.docPlusPrice2 = convertView.findViewById(R.id.doc_plus_price2);

            viewHolder.docTao1Iv = convertView.findViewById(R.id.doc_list_tao_iv1);
            viewHolder.docTao2Iv = convertView.findViewById(R.id.doc_list_tao_iv2);

            viewHolder.mYuemeiIv = convertView.findViewById(R.id.doc_yuemei_renzheng_iv);

            viewHolder.ratBar = convertView.findViewById(R.id.room_ratingbar);
            viewHolder.mscoreTv = convertView.findViewById(R.id.comment_score_list_tv);
            viewHolder.myudingTv = convertView.findViewById(R.id.comment_num_hos_list_tv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Glide.with(mContext)
                .load(doctorData.getImg())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ic_headpic_img)
                .error(R.drawable.ic_headpic_img)
                .into(viewHolder.docHeadPic);


        viewHolder.docTitlelTV.setText(doctorData.getTitle());

        viewHolder.docNameTV.setText(doctorData.getUsername());
        if (null != doctorData.getHspital_name() && doctorData.getHspital_name().length() > 0) {
            viewHolder.docHosptialTV.setVisibility(View.VISIBLE);
            viewHolder.docHosptialTV.setText(doctorData.getHspital_name());
        } else {
            viewHolder.docHosptialTV.setVisibility(View.GONE);
        }
        HospitalTop hospitalTop = doctorData.getHospital_top();
        if (hospitalTop != null && !TextUtils.isEmpty(hospitalTop.getDesc())){
            viewHolder.docHosptialTagContainer.setVisibility(View.VISIBLE);
            viewHolder.docHosptialLevel.setText("NO."+hospitalTop.getLevel());
            viewHolder.docHosptialTagName.setText(hospitalTop.getDesc());
        }else {
            viewHolder.docHosptialTagContainer.setVisibility(View.GONE);
        }


        if (doctorData.getComment_bili() != null && doctorData.getComment_bili().length() > 0) {
            String startNum = doctorData.getComment_bili();

            if (startNum.length() > 0) {
                int num = Integer.parseInt(startNum);
                viewHolder.ratBar.setMax(100);
                viewHolder.ratBar.setProgress(num);

                if (num > 0) {
                    viewHolder.mscoreTv.setText(doctorData.getComment_score());
                } else {
                    viewHolder.mscoreTv.setText("暂无评价");
                }
            }
        }

        if (null != doctorData.getSku_order_num() && !doctorData.getSku_order_num().equals("0")) {
            viewHolder.myudingTv.setVisibility(View.VISIBLE);
            viewHolder.myudingTv.setText(doctorData.getSku_order_num() + "人预订");
        } else {
            viewHolder.myudingTv.setVisibility(View.GONE);
        }


        if (null != doctorData.getTalent() && !doctorData.getTalent().equals("0")) {
            viewHolder.mYuemeiIv.setVisibility(View.VISIBLE);

            if (doctorData.getTalent().equals("1")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("2")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("3")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("4")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("5")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.renzheng_list);
            } else if (doctorData.getTalent().equals("6")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.teyao);
            } else if (doctorData.getTalent().equals("7")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.renzheng_list);
            }

        } else {
            viewHolder.mYuemeiIv.setVisibility(View.GONE);
        }


        int taosize = doctorData.getTao().size();
        if (taosize > 0) {

            if (taosize == 1) {
                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.GONE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                viewHolder.docTao1PriceTv.setText("￥" + doctorData.getTao().get(0).getPrice_discount());
                String member_price = doctorData.getTao().get(0).getMember_price();
                int i = Integer.parseInt(member_price);
                if (i >= 0){
                    viewHolder.docPlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docPlusPrice.setText("¥"+member_price);
                }else {
                    viewHolder.docPlusVibility.setVisibility(View.GONE);
                }
                String iscu = doctorData.getTao().get(0).getDacu66_id();
                if (iscu.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao);
                }

//				viewHolder.docTao1Content.setOnClickListener(new MyAdapterListener(position,0));

            } else if (taosize >= 2) {

                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.VISIBLE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                viewHolder.docTao1PriceTv.setText("￥" + doctorData.getTao().get(0).getPrice_discount());
                viewHolder.docTao2NameTv.setText(doctorData.getTao().get(1).getTitle());
                viewHolder.docTao2PriceTv.setText("￥" + doctorData.getTao().get(1).getPrice_discount());

                String member_price = doctorData.getTao().get(0).getMember_price();
                String member_price2 = doctorData.getTao().get(1).getMember_price();
                int i = Integer.parseInt(member_price);
                int j = Integer.parseInt(member_price2);
                if (i >= 0){
                    viewHolder.docPlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docPlusPrice.setText("¥"+member_price);
                }else {
                    viewHolder.docPlusVibility.setVisibility(View.GONE);
                }
                if (j >= 0){
                    viewHolder.docPlusVibility2.setVisibility(View.VISIBLE);
                    viewHolder.docPlusPrice2.setText("¥"+member_price2);
                }else {
                    viewHolder.docPlusVibility2.setVisibility(View.GONE);
                }
                String iscu1 = doctorData.getTao().get(0).getDacu66_id();
                if (iscu1.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }

                String iscu2 = doctorData.getTao().get(1).getDacu66_id();
                if (iscu2.equals("1")) {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }
            }

            String isMore = doctorData.getLook();
//            if (isMore.length() > 0) {
//
//                viewHolder.docMoreTaoTv.setVisibility(View.VISIBLE);
//                viewHolder.docMoreTaoTv.setText(isMore);
//
//            } else {
//                viewHolder.docMoreTaoTv.setVisibility(View.GONE);
//            }


//			viewHolder.docTao1Content.setOnClickListener(new MyAdapterListener(position,0));
//			viewHolder.docTao2Content.setOnClickListener(new MyAdapterListener(position,1));

        } else {
            viewHolder.docTao1Content.setVisibility(View.GONE);
            viewHolder.docTao2Content.setVisibility(View.GONE);
//            viewHolder.docMoreTaoTv.setVisibility(View.GONE);
        }


        convertView.setBackgroundResource(R.color.white);
        return convertView;
    }


    class MyAdapterListener implements View.OnClickListener {

        private int position;
        private int posi;

        public MyAdapterListener(int pos, int pos_i) {
            position = pos;
            posi = pos_i;
        }

        @Override
        public void onClick(View v) {

            Intent it1 = new Intent();
            it1.putExtra("id", mDoctorData.get(position).getTao().get(posi).getTao_id());
            it1.putExtra("source", "0");
            it1.putExtra("objid", "0");
            it1.setClass(mContext, TaoDetailActivity.class);
            mContext.startActivity(it1);
        }
    }


    public void add(List<DocListData> infos) {
        mDoctorData.addAll(infos);
    }
}
