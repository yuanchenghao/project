/**
 * 
 */
package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.controller.api.MyPersonCenterApi;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.model.api.DoctorDetailBBsApi;
import com.module.home.view.LoadingProgress;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * 医生的帖子
 *
 * @author dwb
 * 
 */
public class DocBbsListActivity extends BaseActivity {

	private final String TAG = "DocBbsListActivity";
	private DocBbsListActivity mContext;
	// List
	@BindView(id = R.id.my_bbs_list_view)
	private DropDownListView homeList;
	private int mCurPage = 1;
	private Handler mHandler;

	private List<BBsListData550> lvBBslistData = new ArrayList<BBsListData550>();
	private List<BBsListData550> lvBBslistMoreData = new ArrayList<BBsListData550>();
	private BBsListAdapter bbsListAdapter;

	private String uid;
	@BindView(id = R.id.my_bbs_back)
	private RelativeLayout back;
	@BindView(id = R.id.my_post_tv_nodata)
	private LinearLayout nodataTv;

	@BindView(id = R.id.mybbs_list_rly)
	private RelativeLayout listRly;

	private String docid;
	private String docname;

	@BindView(id = R.id.doc_bbs_list_title_tv)
	private TextView titleTv;
	private LoadingProgress mDialog;
	private DoctorDetailBBsApi docBbListApi;
	private HashMap<String, Object> docBbListMap = new HashMap<>();

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_doc_bbslit);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = DocBbsListActivity.this;
		Intent it = getIntent();
		docid = it.getStringExtra("docid");
		docname = it.getStringExtra("docname");
		titleTv.setText(docname + "的帖子");
		mDialog = new LoadingProgress(mContext);

		docBbListApi = new DoctorDetailBBsApi();

	}

	public void onResume() {
		super.onResume();

		uid = Utils.getUid();

		initList();
		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						DocBbsListActivity.this.finish();
					}
				});
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	void initList() {

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				onBackPressed();
			}
		});

		mHandler = getHandler();

		lvBBslistData = null;
		lvBBslistMoreData = null;
		mCurPage = 1;
		mDialog.startLoading();
		lodBBsListData550(true);
		homeList.setHasMore(true);

		homeList.setOnBottomListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lodBBsListData550(false);
			}
		});

		homeList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpter, View v, int pos,
					long arg3) {
				// ViewInject.toast(pos + "" + "不要着急，会有的");

				if(null!=lvBBslistData) {
					String url = lvBBslistData.get(pos).getUrl();
					String qid = lvBBslistData.get(pos).getQ_id();
					Intent it2 = new Intent();
					it2.putExtra("url", url);
					it2.putExtra("qid", qid);
					it2.setClass(mContext, DiariesAndPostsActivity.class);
					startActivity(it2);
				}
			}
		});
	}

	void lodBBsListData550(final boolean isDonwn) {

		docBbListMap.put("id",docid);
		docBbListMap.put("page",mCurPage+"");
		docBbListApi.getCallBack(mContext, docBbListMap, new BaseCallBackListener<List<BBsListData550>>() {
			@Override
			public void onSuccess(List<BBsListData550> bBsListData550s) {
				Message msg = null;

				if (isDonwn) {
					if (mCurPage == 1) {

						lvBBslistData = bBsListData550s;

						msg = mHandler.obtainMessage(1);
						msg.sendToTarget();
					}
				} else {
					mCurPage++;
					lvBBslistMoreData = bBsListData550s;
					msg = mHandler.obtainMessage(2);
					msg.sendToTarget();
				}
			}
		});
	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (null!=lvBBslistData &&lvBBslistData.size()>0) {

						nodataTv.setVisibility(View.GONE);
						mDialog.stopLoading();
						bbsListAdapter = new BBsListAdapter(mContext,
								lvBBslistData);
						homeList.setAdapter(bbsListAdapter);

						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"MM-dd HH:mm:ss");

						homeList.onDropDownComplete(getString(R.string.update_at)
								+ dateFormat.format(new Date()));
						homeList.onBottomComplete();
					} else {
						mDialog.stopLoading();
						// ViewInject.toast("您还没有发表帖子");
						nodataTv.setVisibility(View.VISIBLE);
					}
					break;
				case 2:
					if (null!=lvBBslistMoreData &&lvBBslistMoreData.size()>0) {
						bbsListAdapter.add(lvBBslistMoreData);
						bbsListAdapter.notifyDataSetChanged();
						homeList.onBottomComplete();
					} else {
						homeList.setHasMore(false);
						homeList.setShowFooterWhenNoMore(true);
						homeList.onBottomComplete();
					}
					break;
				}

			}
		};
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

}
