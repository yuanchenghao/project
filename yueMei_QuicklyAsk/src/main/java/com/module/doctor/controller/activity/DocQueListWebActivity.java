package com.module.doctor.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.view.ElasticScrollView.OnRefreshListener;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

public class DocQueListWebActivity extends BaseActivity {

	private final String TAG = "DocQueListWebActivity";

	@BindView(id = R.id.doc_que_list_web_det_scrollview1, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.doc_que_list_linearlayout, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.doc_que_list_web_back, click = true)
	private RelativeLayout back;// 返回

	@BindView(id = R.id.title_tv)
	private TextView titleTv;
	private String type;

	private WebView docDetWeb;

	private DocQueListWebActivity mContex;

	private String link;
	public JSONObject obj_http;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_acty_doc_quelist);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = DocQueListWebActivity.this;

		Intent it0 = getIntent();
		link = it0.getStringExtra("link");
		type = it0.getStringExtra("type");

		if (type.equals("1")) {
			titleTv.setText("问题列表");
		} else {
			titleTv.setText("医生服务");
		}

		scollwebView.GetLinearLayout(contentWeb);

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();
		scollwebView.setonRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				webReload();
			}
		});
		LodUrl(link);

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						DocQueListWebActivity.this.finish();
					}
				});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}


	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 * 
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		Log.e(TAG, urlStr);

		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 问答详情
				String link = obj.getString("link");
				String qid = obj.getString("id");

				Intent it = new Intent();
				it.setClass(mContex, DiariesAndPostsActivity.class);
				it.putExtra("url", link);
				it.putExtra("qid", qid);
				startActivity(it);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 加载web
	 */
	public void LodUrl(String link) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(link);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.doc_que_list_web_back:
			// finish();
			onBackPressed();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
