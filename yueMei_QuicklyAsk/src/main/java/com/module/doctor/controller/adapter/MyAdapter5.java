/**
 * 
 */
package com.module.doctor.controller.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.doctor.controller.activity.TabDocListFragActivity;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.quicklyask.activity.R;

import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class MyAdapter5 extends BaseAdapter {

	private final String TAG = "MyAdapter5";

	private List<TaoPopItemIvData> mTaoPopItemData = new ArrayList<TaoPopItemIvData>();
	private Context mContext;
	private LayoutInflater inflater;
	private TaoPopItemIvData TaoPopItemData;
	ViewHolder viewHolder;

	ImageOptions imageOptions;

	public MyAdapter5(Context mContext, List<TaoPopItemIvData> mTaoPopItemData) {
		this.mContext = mContext;
		this.mTaoPopItemData = mTaoPopItemData;
		inflater = LayoutInflater.from(mContext);


		imageOptions = new ImageOptions.Builder()
				.setConfig(Bitmap.Config.RGB_565)
				.setUseMemCache(true)
				.setLoadingDrawableId(R.drawable.radius_gray80)
				.build();
	}

	static class ViewHolder {
		public TextView mPart1NameTV;
		public ImageView mIv;
	}

	@Override
	public int getCount() {
		return mTaoPopItemData.size();
	}

	@Override
	public Object getItem(int position) {
		return mTaoPopItemData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.listview_item1, null);
			viewHolder = new ViewHolder();

			viewHolder.mPart1NameTV = convertView
					.findViewById(R.id.tv);
			viewHolder.mIv = convertView.findViewById(R.id.iv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (position == TabDocListFragActivity.mPosition) {
			convertView.setBackgroundResource(R.color.white);
			viewHolder.mPart1NameTV.setTextColor(mContext.getResources()
					.getColor(R.color.red_title));
		} else {
			convertView.setBackgroundColor(Color.parseColor("#eeeeee"));
			viewHolder.mPart1NameTV.setTextColor(Color.parseColor("#333333"));
		}

		TaoPopItemData = mTaoPopItemData.get(position);

			x.image().bind(viewHolder.mIv, TaoPopItemData.getImg(), imageOptions);
		viewHolder.mPart1NameTV.setText(TaoPopItemData.getName());

		return convertView;
	}

	public void add(List<TaoPopItemIvData> infos) {
		mTaoPopItemData.addAll(infos);
	}
}
