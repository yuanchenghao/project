package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.community.model.bean.BBsListData550;
import com.module.home.model.bean.TaoBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日记流适配器
 * Created by 裴成浩 on 17/08/07.
 */
public class DiaryListAdapter extends BaseAdapter {

    private final String TAG = "DiaryListAdapter";
    private Map<Integer, Boolean> map = new HashMap<>();
    private List<BBsListData550> mHotIssues;
    private Activity mActivity;
    private LayoutInflater inflater;
    private ViewHolder viewHolder;
    private int windowsWight;
    private String mFlag;

    public DiaryListAdapter(Activity activity, List<BBsListData550> mHotIssues, String flag) {
        this.mActivity = activity;
        this.mHotIssues = mHotIssues;
        this.mFlag = flag;
        inflater = LayoutInflater.from(mActivity);
        windowsWight = Cfg.loadInt(mActivity, FinalConstant.WINDOWS_W, 0);
    }


    static class ViewHolder {
        public LinearLayout mBBsHeadHeadpic;
        public ImageView mBBsHeadPic;
        public TextView mBBsName;
        public TextView mBBsTime;
        public TextView mBBsTitle;

        public LinearLayout mFans;
        public ImageView mImgageFans;
        public TextView mCenterFans;

        public FrameLayout mSingalLy;
        ImageView mSingImg;
        ImageView mSigngaVideo;

        public RelativeLayout mDuozhangLy;
        public ImageView mBBsIv1;
        public ImageView mBBsIv2;

        public int flag;
        LinearLayout mSkuPlus1Visorgone;
        LinearLayout mSkuPlus2Visorgone;
        TextView mSkuPlus1Price;
        TextView mSkuPlus2Price;
        public TextView mBBsPicRuleIv1;
        public TextView mBBsPicRuleIv2;
        public TextView mSkuOrding;
        public TextView mTag;
        public LinearLayout tagLy;
        public RelativeLayout mSkuClick;
        public TextView mSkuName;
        public TextView mSkuPrice;
        public RelativeLayout mSkuClick2;
        public TextView mSkuName2;
        public TextView mSkuPrice2;
        public TextView mSkuPlusPrice;
        public View mFanxianLine;
        public LinearLayout mFanxianContent;
        public TextView mFanxian;
        public ImageView mIdentification1;
//        public ImageView mIdentification2;
    }

    @Override
    public int getCount() {
        return mHotIssues.size();
    }

    @Override
    public Object getItem(int position) {
        return mHotIssues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"NewApi", "InlinedApi", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final BBsListData550 hotIsData = mHotIssues.get(position);
        Log.d("DiaryListAdapter", hotIsData.toString());

        if (convertView == null || ((ViewHolder) convertView.getTag()).flag != position) {
            convertView = inflater.inflate(R.layout.item_home_diaryother, null);
            viewHolder = new ViewHolder();
            viewHolder.flag = position;

            viewHolder.mBBsHeadHeadpic = convertView.findViewById(R.id.diary_list_headpic_ly);
            viewHolder.mBBsHeadPic = convertView.findViewById(R.id.bbs_list_head_image_iv);
            viewHolder.mBBsName = convertView.findViewById(R.id.bbs_list_name_tv);
            viewHolder.mBBsTime = convertView.findViewById(R.id.bbs_list_time_tv);
            viewHolder.mBBsTitle = convertView.findViewById(R.id.bbs_list_title_tv);

            viewHolder.mFans = convertView.findViewById(R.id.diary_fans);
            viewHolder.mImgageFans = convertView.findViewById(R.id.diary_imgage_fans);
            viewHolder.mCenterFans = convertView.findViewById(R.id.diary_center_fans);

            viewHolder.mDuozhangLy = convertView.findViewById(R.id.bbs_list_duozhang_ly);
            viewHolder.mBBsIv1 = convertView.findViewById(R.id.bbs_list_duozhang_iv1);
            viewHolder.mBBsIv2 = convertView.findViewById(R.id.bbs_list_duozhang_iv2);


            viewHolder.mSingalLy = convertView.findViewById(R.id.bbs_list_pic_danzhang_ly);
            viewHolder.mSingImg = convertView.findViewById(R.id.bbs_list_pic_img);
            viewHolder.mSigngaVideo = convertView.findViewById(R.id.bbs_list_video);

            viewHolder.mSkuOrding = convertView.findViewById(R.id.bbs_list_sku_ording);
            viewHolder.mBBsPicRuleIv1 = convertView.findViewById(R.id.bbs_list_picrule1);
            viewHolder.mBBsPicRuleIv2 = convertView.findViewById(R.id.bbs_list_picrule2);

            viewHolder.mSkuPlus1Visorgone = convertView.findViewById(R.id.sku_plus1_visorgone);
            viewHolder.mSkuPlus2Visorgone = convertView.findViewById(R.id.sku_plus2_visorgone);
            viewHolder.mSkuPlus1Price = convertView.findViewById(R.id.sku_plus_price);
            viewHolder.mSkuPlus2Price = convertView.findViewById(R.id.sku_plus_price2);
            viewHolder.mTag = convertView.findViewById(R.id.bbs_list_tag_tv);
            viewHolder.tagLy = convertView.findViewById(R.id.tag_ly_550);
            viewHolder.mSkuClick = convertView.findViewById(R.id.bbs_item_sku_click);
            viewHolder.mSkuName = convertView.findViewById(R.id.bbs_list_sku_name);
            viewHolder.mSkuPrice = convertView.findViewById(R.id.bbs_list_sku_price);
            viewHolder.mSkuClick2 = convertView.findViewById(R.id.bbs_item_sku_click2);
            viewHolder.mSkuName2 = convertView.findViewById(R.id.bbs_list_sku_name2);
            viewHolder.mSkuPrice2 = convertView.findViewById(R.id.bbs_list_sku_price2);
            viewHolder.mSkuPlusPrice = convertView.findViewById(R.id.plus_price);

            viewHolder.mIdentification1 = convertView.findViewById(R.id.iv_video_identification1);
//            viewHolder.mIdentification2 = convertView.findViewById(R.id.iv_video_identification2);
            viewHolder.mFanxianLine = convertView.findViewById(R.id.diary_fanxian_line);
            viewHolder.mFanxianContent = convertView.findViewById(R.id.diary_fanxian_content);
            viewHolder.mFanxian = convertView.findViewById(R.id.diary_fanxian);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if ("1".equals(mFlag)) {
            viewHolder.mBBsHeadHeadpic.setVisibility(View.GONE);
        }

        Glide.with(mActivity).load(hotIsData.getUser_img()).transform(new GlideCircleTransform(mActivity)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mBBsHeadPic);

        viewHolder.mBBsName.setText(hotIsData.getUser_name());
        viewHolder.mBBsTime.setText(hotIsData.getTime()+"   共更新"+hotIsData.getShareNum()+"篇");
        viewHolder.mBBsTitle.setText(hotIsData.getTitle());

        if ("1".equals(hotIsData.getCashback().getIs_cashback())) {
            viewHolder.mFanxianLine.setVisibility(View.VISIBLE);
            viewHolder.mFanxianContent.setVisibility(View.VISIBLE);
            viewHolder.mFanxian.setText(hotIsData.getCashback().getCashback_complete());
        } else {
            viewHolder.mFanxianLine.setVisibility(View.GONE);
            viewHolder.mFanxianContent.setVisibility(View.GONE);
        }

        viewHolder.mFanxianContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/postinfo/id/939555/";
                Intent intent = new Intent();
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                intent.putExtra("url", link);
                intent.putExtra("qid", "939555");
                mActivity.startActivity(intent);
            }
        });

        String picRule = hotIsData.getPicRule();
        if (picRule.equals("1")) {
            viewHolder.mBBsPicRuleIv1.setVisibility(View.VISIBLE);
            viewHolder.mBBsPicRuleIv2.setVisibility(View.VISIBLE);
            if ("0".equals(hotIsData.getAfter_day())) {
                viewHolder.mBBsPicRuleIv2.setPadding(Utils.dip2px(mActivity, 13), 0, Utils.dip2px(mActivity, 12), 0);
                viewHolder.mBBsPicRuleIv2.setText("After");
            } else {
                viewHolder.mBBsPicRuleIv2.setPadding(Utils.dip2px(mActivity, 8), 0, Utils.dip2px(mActivity, 7), 0);
                viewHolder.mBBsPicRuleIv2.setText("After" + hotIsData.getAfter_day() + "天");
            }
        } else {
            viewHolder.mBBsPicRuleIv1.setVisibility(View.GONE);
            viewHolder.mBBsPicRuleIv2.setVisibility(View.GONE);
        }



        // 标签
        if (hotIsData.getTao() != null) {
            //关联SKU，
            TaoBean taoBean = hotIsData.getTao();
            if (!"0".equals(hotIsData.getTao().getId())) {

                String totalAppoint = taoBean.getTotalAppoint();
                String member_price = taoBean.getMember_price();
                int i = Integer.parseInt(member_price);
                if (!"0".equals(totalAppoint)) {
                    //且SKU有预订数
                    viewHolder.tagLy.setVisibility(View.GONE);
                    viewHolder.mSkuClick.setVisibility(View.VISIBLE);
                    viewHolder.mSkuClick2.setVisibility(View.GONE);
                    viewHolder.mSkuName.setText(taoBean.getTitle());
                    viewHolder.mSkuPrice.setText(taoBean.getPrice());
                    viewHolder.mSkuOrding.setText(totalAppoint + "人已预订");
                    if (i >= 0) {
                        viewHolder.mSkuPlus1Visorgone.setVisibility(View.VISIBLE);
                        viewHolder.mSkuPlus1Price.setText(member_price);
                    } else {
                        viewHolder.mSkuPlus1Visorgone.setVisibility(View.GONE);
                    }
                    viewHolder.mSkuClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }

                            Intent intent = new Intent(mActivity, TaoDetailActivity.class);
                            intent.putExtra("id", hotIsData.getTao().getId());
                            intent.putExtra("source", "0");
                            intent.putExtra("objid", "0");
                            mActivity.startActivity(intent);
                        }
                    });

                } else {
                    viewHolder.mSkuClick.setVisibility(View.GONE);
                    viewHolder.mSkuClick2.setVisibility(View.VISIBLE);
                    viewHolder.mSkuName2.setText(taoBean.getTitle());
                    viewHolder.mSkuPrice2.setText(taoBean.getPrice());
                    if (i >= 0) {
                        viewHolder.mSkuPlus2Visorgone.setVisibility(View.VISIBLE);
                        viewHolder.mSkuPlus2Price.setText(member_price);
                    } else {
                        viewHolder.mSkuPlus2Visorgone.setVisibility(View.GONE);
                    }
                    viewHolder.mSkuClick2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mActivity, TaoDetailActivity.class);
                            intent.putExtra("id", hotIsData.getTao().getId());
                            intent.putExtra("source", "0");
                            intent.putExtra("objid", "0");
                            mActivity.startActivity(intent);
                        }
                    });
                }

            } else {
                viewHolder.mSkuClick.setVisibility(View.GONE);
                viewHolder.mSkuClick2.setVisibility(View.GONE);
                //         标签
                if (null != hotIsData.getTag()) {
                    if (hotIsData.getTag().size() > 0) {
                        viewHolder.tagLy.setVisibility(View.VISIBLE);
                        int tagsize = hotIsData.getTag().size();
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < tagsize; i++) {
                            stringBuilder.append("#" + hotIsData.getTag().get(i).getName());
                        }
                        viewHolder.mTag.setText(stringBuilder);
                    } else {
                        viewHolder.tagLy.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.tagLy.setVisibility(View.GONE);
                }

            }
        }


        // 图片
        if (null != hotIsData.getPic()) {

            if (hotIsData.getPic().size() > 0) {
                if (hotIsData.getPic().size() == 1) {//单张图片的时候

                    viewHolder.mDuozhangLy.setVisibility(View.GONE);
                    viewHolder.mSingalLy.setVisibility(View.VISIBLE);
                    viewHolder.tagLy.setVisibility(View.GONE);
                    viewHolder.mSkuClick.setVisibility(View.GONE);
                    viewHolder.mSkuClick2.setVisibility(View.GONE);

                    ViewGroup.LayoutParams params = viewHolder.mSingalLy.getLayoutParams();
                    params.height = ((windowsWight - 40) * 340 / 710);
                    viewHolder.mSingalLy.setLayoutParams(params);

                    //视频播放按钮
                    if ("1".equals(hotIsData.getIs_video())) {
//                        String url="https://player.yuemei.com/video/6ea2/201906/5bb2ac1e7d68adf4766d63788ca4924c.mp4";
//                        String imgUrl="https://p11.yuemei.com/bms/img/20190729/190729143945_15833d.jpg";
                        viewHolder.mSigngaVideo.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mSigngaVideo.setVisibility(View.GONE);

                    }
                    try {
                    Glide.with(mActivity).load(hotIsData.getPic().get(0).getImg())
                            .transform(new GlideRoundTransform(mActivity, Utils.dip2px(3)))
                            .placeholder(R.drawable.home_other_placeholder)
                            .error(R.drawable.home_other_placeholder)
                            .into(viewHolder.mSingImg);
                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }


                } else if (hotIsData.getPic().size() > 1) {//两张对比图

                    viewHolder.mDuozhangLy.setVisibility(View.VISIBLE);
                    viewHolder.mSingalLy.setVisibility(View.GONE);

                    ViewGroup.LayoutParams params = viewHolder.mDuozhangLy.getLayoutParams();
                    params.height = ((windowsWight - 40) / 2);
                    viewHolder.mDuozhangLy.setLayoutParams(params);


                    if ("1".equals(hotIsData.getIs_video())) {
                        viewHolder.mIdentification1.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mIdentification1.setVisibility(View.GONE);
                    }

                    try {
                        String img1 = hotIsData.getPic().get(0).getImg();
                        String img2 = hotIsData.getPic().get(1).getImg();

                        Log.e(TAG, "img1 == " + img1);
                        Log.e(TAG, "img2 == " + img2);
                        if (!TextUtils.isEmpty(img1)) {
                            Glide.with(mActivity).load(img1).transform(new GlideRoundTransform(mActivity, Utils.dip2px(3))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mBBsIv1);
                        }

                        if (!TextUtils.isEmpty(img2)) {
                            Glide.with(mActivity).load(img2).transform(new GlideRoundTransform(mActivity, Utils.dip2px(3))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mBBsIv2);
                        }


                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }
                }
            } else {
                viewHolder.mDuozhangLy.setVisibility(View.GONE);
                viewHolder.mSingalLy.setVisibility(View.GONE);
            }

        } else {
            viewHolder.mDuozhangLy.setVisibility(View.GONE);
            viewHolder.mSingalLy.setVisibility(View.GONE);
        }

        final String isFollowUser = hotIsData.getIs_follow_user();

        switch (isFollowUser) {
            case "0":          //未关注
                setFocusView(R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                break;
            case "1":          //已关注
                setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                break;
            case "2":          //互相关注
                setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                break;
        }

        viewHolder.mFans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        switch (isFollowUser) {
                            case "0":          //未关注
                                FocusAndCancel(hotIsData.getUser_id(), isFollowUser, position);
                                break;
                            case "1":          //已关注
                            case "2":          //互相关注
                                showDialogExitEdit(hotIsData.getUser_id(), isFollowUser, position);
                                break;
                        }
                    } else {
                        Utils.jumpBindingPhone(mActivity);

                    }


                } else {
                    Utils.jumpLogin(mActivity);
                }

            }
        });

        //跳转到个人中心页
        viewHolder.mBBsHeadHeadpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(hotIsData.getUser_id()) && onItemPersonClickListener != null) {
                    onItemPersonClickListener.onItemPersonClick(hotIsData.getUser_id(), position);
                }
            }
        });

        return convertView;
    }

    private void showDialogExitEdit(final String id, final String mFolowing, final int position) {
        final EditExitDialog editDialog = new EditExitDialog(mActivity, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel(id, mFolowing, position);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    /**
     * 关注和取消关注
     */
    private void FocusAndCancel(String id, final String mFolowing, final int position) {
        Log.e(TAG, "id === " + id);
        Log.e(TAG, "mFolowing === " + mFolowing);
        Log.e(TAG, "position === " + position);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new FocusAndCancelApi().getCallBack(mActivity, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (focusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mHotIssues.get(position).setIs_follow_user("0");
                                setFocusView(R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                                break;
                            case "1":          //已关注
                                mHotIssues.get(position).setIs_follow_user("1");
                                setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                                break;
                            case "2":          //互相关注
                                mHotIssues.get(position).setIs_follow_user("2");
                                setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                                break;
                        }

                        notifyDataSetChanged();
                        Toast.makeText(mActivity, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void setEachFollowing(int mTempPos, String folowing) {
        switch (folowing) {
            case "0":          //未关注
                mHotIssues.get(mTempPos).setIs_follow_user("0");
                break;
            case "1":          //已关注
                mHotIssues.get(mTempPos).setIs_follow_user("1");
                break;
            case "2":          //互相关注
                mHotIssues.get(mTempPos).setIs_follow_user("2");
                break;
        }
        notifyDataSetChanged();
    }

    /**
     * 修改关注样式
     *
     * @param drawable1
     * @param drawable
     * @param color
     * @param text
     */
    private void setFocusView(int drawable1, int drawable, String color, String text) {

        viewHolder.mImgageFans.setBackground(ContextCompat.getDrawable(mActivity, drawable1));
        viewHolder.mFans.setBackground(ContextCompat.getDrawable(mActivity, drawable));
        viewHolder.mCenterFans.setTextColor(Color.parseColor(color));
        viewHolder.mCenterFans.setText(text);

//        ViewGroup.LayoutParams layoutParams1 = viewHolder.mFans.getLayoutParams();
//        switch (text.length()) {
//            case 2:
//                layoutParams1.width = Utils.dip2px(mActivity, 68);
//                break;
//            case 3:
//                layoutParams1.width = Utils.dip2px(mActivity, 78);
//                break;
//            case 4:
//                layoutParams1.width = Utils.dip2px(mActivity, 90);
//                break;
//        }
//
//        viewHolder.mFans.setLayoutParams(layoutParams1);
    }


    public void add(List<BBsListData550> infos) {
        mHotIssues.addAll(infos);
    }

    //跳转到个人中心页面
    private OnItemPersonClickListener onItemPersonClickListener;

    public interface OnItemPersonClickListener {
        void onItemPersonClick(String id, int pos);
    }

    public void setOnItemPersonClickListener(OnItemPersonClickListener onItemPersonClickListener) {
        this.onItemPersonClickListener = onItemPersonClickListener;
    }

    public List<BBsListData550> getmHotIssues() {
        return mHotIssues;
    }
}
