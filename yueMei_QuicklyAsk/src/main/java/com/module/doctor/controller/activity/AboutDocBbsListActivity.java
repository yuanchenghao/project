/**
 *
 */
package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.controller.api.DocDeRijiListApi;
import com.module.home.view.LoadingProgress;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.DropDownListView;
import com.quicklyask.view.DropDownListView.OnDropDownListener;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 提及到该医生的列表
 *
 * @author Robin
 */
public class AboutDocBbsListActivity extends BaseActivity {

    private final String TAG = "AboutDocBbsListActivity";
    private AboutDocBbsListActivity mContext;
    // List
    @BindView(id = R.id.my_bbs_list_view)
    private DropDownListView homeList;
    private int mCurPage = 1;
    private Handler mHandler;
    private List<BBsListData550> lvBBslistData = new ArrayList<>();
    private List<BBsListData550> lvBBslistMoreData = new ArrayList<>();
    private BBsListAdapter bbsListAdapter;

    private String uid;

    @BindView(id = R.id.about_tow_top)
    private CommonTopBar mTop;

    @BindView(id = R.id.my_post_tv_nodata)
    private LinearLayout nodataTv;

    @BindView(id = R.id.mybbs_list_rly)
    private RelativeLayout listRly;

    private String docid;
    private String docname;
    private LoadingProgress progress;
    private HashMap<String, Object> docDeRijiListMap = new HashMap<>();
    private DocDeRijiListApi docDeRijiListApi;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_about_doc_bbslist);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = AboutDocBbsListActivity.this;
        mTop.setCenterText("关于他的日记");

        Intent it = getIntent();
        docid = it.getStringExtra("docid");
        docname = it.getStringExtra("docname");

        progress = new LoadingProgress(mContext);
        docDeRijiListApi = new DocDeRijiListApi();
    }

    public void onResume() {
        super.onResume();

        uid = Utils.getUid();

        initList();
        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        AboutDocBbsListActivity.this.finish();
                    }
                });
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    void initList() {

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mHandler = getHandler();
        lvBBslistData = null;
        lvBBslistMoreData = null;
        mCurPage = 1;

        progress.startLoading();

        lodBBsListData550(true);
        homeList.setHasMore(true);

        // homeList.setDropDownStyle(false);

        homeList.setOnDropDownListener(new OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvBBslistData = null;
                lvBBslistMoreData = null;
                mCurPage = 1;
                progress.startLoading();
                lodBBsListData550(true);
                homeList.setHasMore(true);
            }
        });

        homeList.setOnBottomListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                lodBBsListData550(false);
            }
        });

        homeList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {
                // ViewInject.toast(pos + "" + "不要着急，会有的");
                if (null != lvBBslistData && lvBBslistData.size() > 0) {
                    if (lvBBslistData.size() < 20) {
                        String url = lvBBslistData.get(pos).getUrl();
                        String qid = lvBBslistData.get(pos).getQ_id();
                        String appmurl = lvBBslistData.get(pos).getAppmurl();
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                    } else {
                        String url = lvBBslistData.get(pos - 1).getUrl();
                        String qid = lvBBslistData.get(pos - 1).getQ_id();
                        String appmurl = lvBBslistData.get(pos - 1).getAppmurl();
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                    }
                }
            }
        });
    }

    void lodBBsListData550(final boolean isDonwn) {

        docDeRijiListMap.put("id", docid);
        docDeRijiListMap.put("page", mCurPage + "");
        docDeRijiListApi.getCallBack(mContext, docDeRijiListMap, new BaseCallBackListener<List<BBsListData550>>() {
            @Override
            public void onSuccess(List<BBsListData550> docListDatas) {
                Message msg = null;
				if (isDonwn) {
					if (mCurPage == 1) {
						lvBBslistData = docListDatas;
						msg = mHandler.obtainMessage(1);
						msg.sendToTarget();
					}
				} else {
					mCurPage++;
					lvBBslistMoreData = docListDatas;
					msg = mHandler.obtainMessage(2);
					msg.sendToTarget();
				}
            }
        });

    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (null != lvBBslistData && lvBBslistData.size() > 0) {
                            nodataTv.setVisibility(View.GONE);
                            if (lvBBslistData.size() < 20) {
                                homeList.setDropDownStyle(false);
                                homeList.setOnBottomStyle(false);
                            } else {
                                homeList.setDropDownStyle(true);
                                homeList.setOnBottomStyle(true);
                            }
                            progress.stopLoading();
                            bbsListAdapter = new BBsListAdapter(mContext,
                                    lvBBslistData);
                            homeList.setAdapter(bbsListAdapter);

                            SimpleDateFormat dateFormat = new SimpleDateFormat(
                                    "MM-dd HH:mm:ss");
                            homeList.onDropDownComplete(getString(R.string.update_at)
                                    + dateFormat.format(new Date()));
                            homeList.onBottomComplete();
                        } else {
                            progress.stopLoading();
                            // ViewInject.toast("您还没有发表帖子");
                            nodataTv.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        if (null != lvBBslistMoreData
                                && lvBBslistMoreData.size() > 0) {
                            bbsListAdapter.add(lvBBslistMoreData);
                            bbsListAdapter.notifyDataSetChanged();
                            homeList.onBottomComplete();
                        } else {
                            homeList.setHasMore(false);
                            homeList.setShowFooterWhenNoMore(true);
                            homeList.onBottomComplete();
                        }
                        break;
                }
            }
        };
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

}
