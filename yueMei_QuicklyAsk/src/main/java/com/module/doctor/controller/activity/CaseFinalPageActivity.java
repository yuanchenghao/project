package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alan.imagebrowser.gestureimage.GestureImageView;
import com.alan.imagebrowser.gestureimage.MyViewPager;
import com.baidu.mobstat.StatService;
import com.module.doctor.model.bean.CaseFinalData;
import com.module.doctor.model.bean.CaseFinalPic;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 案例最终页
 * 
 * @author Rubin
 * 
 */
public class CaseFinalPageActivity extends BaseActivity {

	private final String TAG = "CaseFinalPageActivity";

	private List<CaseFinalPic> casePic = new ArrayList<CaseFinalPic>();

	private RelativeLayout docNameRly;
	private RelativeLayout docYuyueRly;
	private TextView detaTv;
	private TextView docNameTv;
	private TextView docYuyueTv;

	private RelativeLayout back;

	private Context mContext;
	private String id = "0";
	private String hosid = "0";
	private String docid = "0";
	// -------------------------------
	private String[] imageArray;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private int position;
	private GestureImageView[] mImageViews;
	private MyViewPager viewPager;
	private TextView page;
	private int count;
	private CaseFinalData caseFinaldata;
	private String caseFinalJson;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_case_finalpage);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = CaseFinalPageActivity.this;
		Intent it = getIntent();

		if (it != null) {
			caseFinalJson = it.getStringExtra("casefinaljson");
			id = it.getStringExtra("id");
			hosid = it.getStringExtra("hosid");
			caseFinaldata = JSONUtil.TransformCaseFinalData(caseFinalJson);//
			// Log.e(TAG, "caseFinalJson==" + caseFinalJson);
			position = 0;
			casePic = caseFinaldata.getPic();
			count = casePic.size();
			imageArray = new String[count];
			for (int i = 0; i < count; i++) {
				imageArray[i] = casePic.get(i).getImg();
			}

			detaTv = findViewById(R.id.case_final_detail_tv);
			// pageTv = (TextView) findViewById(R.id.case_final_page_tv);
			docNameTv = findViewById(R.id.case_final_docname_tv);
			docYuyueTv = findViewById(R.id.case_final_docyuyue_tv);

			docNameRly = findViewById(R.id.case_final_docname_rly);
			docNameRly.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					finish();
				}
			});

			back = findViewById(R.id.case_final_back);
			back.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					finish();
				}
			});

			docYuyueRly = findViewById(R.id.case_final_doyuyue_rly);
			docYuyueRly.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent it = new Intent();
					it.putExtra("po", "2250");
					it.putExtra("hosid", hosid);
					it.putExtra("docid", docid);
					it.putExtra("shareid", id);
					it.putExtra("cityId", "0");
					it.putExtra("partId", "0");
					it.putExtra("partId_two", "0");
					it.setClass(mContext, WantBeautifulActivity548.class);
					startActivity(it);
				}
			});

			String sexInt = caseFinaldata.getSex();
			String ageStr = caseFinaldata.getAge();
			String jiaStr = caseFinaldata.getFee();

			docid = caseFinaldata.getDoctor_id();
			String sexStr = "";
			if (sexInt.equals("1")) {
				sexStr = "男";
			} else {
				sexStr = "女";
			}
			detaTv.setText(sexStr + "，" + ageStr + jiaStr);

			String codeIfStr = caseFinaldata.getCooperation();
			if (codeIfStr.equals("1")) {
				docYuyueTv.setText("预约（未开通）");
				docYuyueRly.setClickable(false);
				docYuyueRly.setPressed(true);
			}
			String docName = caseFinaldata.getDname();
			docNameTv.setText("专家：" + docName);
		}

		// getPicData();

		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this).threadPriority(Thread.NORM_PRIORITY - 2)
				.threadPoolSize(3).memoryCacheSize(getMemoryCacheSize(this))
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO).build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(false).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		initViews();
	}

//	void getPicData() {
//		KJHttp kjh = new KJHttp();
//		kjh.get(FinalConstant.CASEFINAL + id + "/" + Utils.getTokenStr(),
//				new StringCallBack() {
//
//					@Override
//					public void onSuccess(String json) {
//
//						// Log.e(TAG, "json=" + json);
//
//						CaseFinal case1 = new CaseFinal();
//						case1 = JSONUtil.TransformCaseFinalData(json);// 最终页数据
//						casePic = case1.getData().getPic();
//
//						position = 0;
//						count = casePic.size();
//						String urlpic = "";
//
//						for (int i = 0; i < count; i++) {
//							if (count == 1) {
//								urlpic = casePic.get(i).toString();
//							} else {
//								urlpic = casePic.get(i).toString() + ",";
//							}
//						}
//
//						imageArray = urlpic.split(",");
//
//					}
//
//				});
//	}

	private void initViews() {
		// Log.e(TAG, "initViews");
		// Log.e(TAG, "page==" + position + "==count==" + count);
		page = findViewById(R.id.case_final_page_tv);
		if (count <= 1) {
			page.setVisibility(View.GONE);
		} else {
			page.setVisibility(View.VISIBLE);
			page.setText((position + 1) + "/" + count);
		}

		viewPager = findViewById(R.id.viewpager_casefinal);
		viewPager.setPageMargin(20);
		viewPager.setAdapter(new ImagePagerAdapter(getWebImageViews()));
		viewPager.setCurrentItem(position);
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			public void onPageSelected(int arg0) {
				page.setText((arg0 + 1) + "/" + count);
				mImageViews[arg0].reset();
			}

			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	@SuppressLint("NewApi")
	private static int getMemoryCacheSize(Context context) {
		int memoryCacheSize;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
			int memClass = ((ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE))
					.getMemoryClass();
			memoryCacheSize = (memClass / 8) * 1024 * 1024; // 1/8 of app memory
															// limit
		} else {
			memoryCacheSize = 2 * 1024 * 1024;
		}
		return memoryCacheSize;
	}

	@SuppressLint("InflateParams")
	private List<View> getWebImageViews() {
		mImageViews = new GestureImageView[count];
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		List<View> views = new ArrayList<View>();
		for (int i = 0; i < count; i++) {
			View view = layoutInflater.inflate(R.layout.web_image_item, null);
			final GestureImageView imageView = view
					.findViewById(R.id.image);
			final ProgressBar progressBar = view
					.findViewById(R.id.loading);
			mImageViews[i] = imageView;

			imageLoader.displayImage(imageArray[i],imageView,options,new SimpleImageLoadingListener(){

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					progressBar.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					progressBar.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					progressBar.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					progressBar.setVisibility(View.GONE);
				}
			});

			imageView.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					finish();
				}
			});
			views.add(view);
		}
		viewPager.setGestureImages(mImageViews);
		return views;
	}

	@Override
	protected void onDestroy() {
		if (mImageViews != null) {
			mImageViews = null;
		}
		imageLoader.clearMemoryCache();
		super.onDestroy();
	}

	private class ImagePagerAdapter extends PagerAdapter {
		private List<View> views;

		public ImagePagerAdapter(List<View> views) {
			this.views = views;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return views.size();
		}

		@Override
		public Object instantiateItem(View view, int position) {
			((ViewPager) view).addView(views.get(position), 0);
			return views.get(position);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == (arg1);
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
