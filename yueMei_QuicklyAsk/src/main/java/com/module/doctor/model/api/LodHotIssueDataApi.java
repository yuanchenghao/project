package com.module.doctor.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * 搜索
 * Created by Administrator on 2018/3/14.
 */

public class LodHotIssueDataApi implements BaseCallBackApi {

    private final HashMap<String, Object> mHashMap;

    public LodHotIssueDataApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.HOME, "index613", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e("LodHotIssueDataApi","LodHotIssueDataApi");
                listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
