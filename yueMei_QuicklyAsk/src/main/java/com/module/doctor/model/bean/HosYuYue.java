/**
 * 
 */
package com.module.doctor.model.bean;

/**
 * @author lenovo17
 * 
 */
public class HosYuYue {

	private String code;
	private String message;
	private HosYuYueData data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public HosYuYueData getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(HosYuYueData data) {
		this.data = data;
	}

}
