/**
 * 
 */
package com.module.doctor.model.bean;

/**
 * @author Robin
 * 
 */
public class HosYuYueData {

	private String hosid;
	private String po;
	private String cooperation;
	private String hosname;
	private String tel;
	private String is_rongyun;
	private String hos_userid;
	private String searchkey;

	public String getHosid() {
		return hosid;
	}

	public void setHosid(String hosid) {
		this.hosid = hosid;
	}

	public String getPo() {
		return po;
	}

	public void setPo(String po) {
		this.po = po;
	}

	public String getCooperation() {
		return cooperation;
	}

	public void setCooperation(String cooperation) {
		this.cooperation = cooperation;
	}

	public String getHosname() {
		return hosname;
	}

	public void setHosname(String hosname) {
		this.hosname = hosname;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getIs_rongyun() {
		return is_rongyun;
	}

	public void setIs_rongyun(String is_rongyun) {
		this.is_rongyun = is_rongyun;
	}

	public String getHos_userid() {
		return hos_userid;
	}

	public void setHos_userid(String hos_userid) {
		this.hos_userid = hos_userid;
	}

	public String getSearchkey() {
		return searchkey;
	}

	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	@Override
	public String toString() {
		return "HosYuYueData{" +
				"hosid='" + hosid + '\'' +
				", po='" + po + '\'' +
				", cooperation='" + cooperation + '\'' +
				", hosname='" + hosname + '\'' +
				", tel='" + tel + '\'' +
				", is_rongyun='" + is_rongyun + '\'' +
				", hos_userid='" + hos_userid + '\'' +
				", searchkey='" + searchkey + '\'' +
				'}';
	}
}
