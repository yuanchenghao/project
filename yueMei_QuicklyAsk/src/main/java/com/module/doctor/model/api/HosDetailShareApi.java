package com.module.doctor.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.bean.HosShareData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/25.
 */

public class HosDetailShareApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, final Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.HOSPITAL, "hosshare", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)){
                    try {
                        HosShareData hosShareData = JSONUtil.TransformSingleBean(mData.data, HosShareData.class);
                        listener.onSuccess(hosShareData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
