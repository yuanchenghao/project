/**
 * 
 */
package com.module.doctor.model.bean;

import com.module.community.model.bean.BBsListData550;

import java.util.List;

/**
 * @author Robin
 * 
 */
public class DocHeadData {

	private String user_id;
	private String img;
	private String doctor_name;
	private String title;
	private List<String> hospital_name;
	private String hospital_id;
	private String desc;
	private String desc_img;
	private String ardent;
	private String attention;
	private String taoNum;
	private String shareNum;
	private String postNum;
	private String coverp;
	private String cooperation;
	private String po;
	private String sixin;
	private String tel;

	private String is_rongyun;
	private String hos_userid;

	private String big_img;
	private String persign;
	private String work_year;
	private List<BBsListData550> postlist;
	private List<BBsListData550> sharelist;


	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getDoctor_name() {
		return doctor_name;
	}

	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getHospital_name() {
		return hospital_name;
	}

	public void setHospital_name(List<String> hospital_name) {
		this.hospital_name = hospital_name;
	}

	public String getHospital_id() {
		return hospital_id;
	}

	public void setHospital_id(String hospital_id) {
		this.hospital_id = hospital_id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDesc_img() {
		return desc_img;
	}

	public void setDesc_img(String desc_img) {
		this.desc_img = desc_img;
	}

	public String getArdent() {
		return ardent;
	}

	public void setArdent(String ardent) {
		this.ardent = ardent;
	}

	public String getAttention() {
		return attention;
	}

	public void setAttention(String attention) {
		this.attention = attention;
	}

	public String getTaoNum() {
		return taoNum;
	}

	public void setTaoNum(String taoNum) {
		this.taoNum = taoNum;
	}

	public String getShareNum() {
		return shareNum;
	}

	public void setShareNum(String shareNum) {
		this.shareNum = shareNum;
	}

	public String getPostNum() {
		return postNum;
	}

	public void setPostNum(String postNum) {
		this.postNum = postNum;
	}

	public String getCoverp() {
		return coverp;
	}

	public void setCoverp(String coverp) {
		this.coverp = coverp;
	}

	public String getCooperation() {
		return cooperation;
	}

	public void setCooperation(String cooperation) {
		this.cooperation = cooperation;
	}

	public String getPo() {
		return po;
	}

	public void setPo(String po) {
		this.po = po;
	}

	public String getSixin() {
		return sixin;
	}

	public void setSixin(String sixin) {
		this.sixin = sixin;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getIs_rongyun() {
		return is_rongyun;
	}

	public void setIs_rongyun(String is_rongyun) {
		this.is_rongyun = is_rongyun;
	}

	public String getHos_userid() {
		return hos_userid;
	}

	public void setHos_userid(String hos_userid) {
		this.hos_userid = hos_userid;
	}

	public String getBig_img() {
		return big_img;
	}

	public void setBig_img(String big_img) {
		this.big_img = big_img;
	}

	public String getPersign() {
		return persign;
	}

	public void setPersign(String persign) {
		this.persign = persign;
	}

	public String getWork_year() {
		return work_year;
	}

	public void setWork_year(String work_year) {
		this.work_year = work_year;
	}

	public List<BBsListData550> getPostlist() {
		return postlist;
	}

	public void setPostlist(List<BBsListData550> postlist) {
		this.postlist = postlist;
	}

	public List<BBsListData550> getSharelist() {
		return sharelist;
	}

	public void setSharelist(List<BBsListData550> sharelist) {
		this.sharelist = sharelist;
	}
}
