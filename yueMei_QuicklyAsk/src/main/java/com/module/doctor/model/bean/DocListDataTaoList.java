package com.module.doctor.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class DocListDataTaoList implements Parcelable {

	private String title;
	private String price_discount;
	private String dacu66_id;//
	private String tao_id;
	private String member_price;

	protected DocListDataTaoList(Parcel in) {
		title = in.readString();
		price_discount = in.readString();
		dacu66_id = in.readString();
		tao_id = in.readString();
		member_price = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(price_discount);
		dest.writeString(dacu66_id);
		dest.writeString(tao_id);
		dest.writeString(member_price);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<DocListDataTaoList> CREATOR = new Creator<DocListDataTaoList>() {
		@Override
		public DocListDataTaoList createFromParcel(Parcel in) {
			return new DocListDataTaoList(in);
		}

		@Override
		public DocListDataTaoList[] newArray(int size) {
			return new DocListDataTaoList[size];
		}
	};

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrice_discount() {
		return price_discount;
	}

	public void setPrice_discount(String price_discount) {
		this.price_discount = price_discount;
	}

	public String getDacu66_id() {
		return dacu66_id;
	}

	public void setDacu66_id(String dacu66_id) {
		this.dacu66_id = dacu66_id;
	}

	public String getTao_id() {
		return tao_id;
	}

	public void setTao_id(String tao_id) {
		this.tao_id = tao_id;
	}

	public String getMember_price() {
		return member_price;
	}

	public void setMember_price(String member_price) {
		this.member_price = member_price;
	}
}
