/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.module.doctor.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.adapter.DoctorListAdpter;
import com.module.doctor.controller.api.DoctorListApi;
import com.module.doctor.model.bean.DocListData;
import com.module.home.view.LoadingProgress;
import com.quicklyask.activity.R;
import com.quicklyask.view.DropDownListView;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DoctorListFragment extends ListFragment {

	private final String TAG = "DoctorListFragment";

	private int position;
	private String[] part2Id;
	private String partIds;
	private Activity mCotext;

	private LinearLayout docText;
	// List
	private DropDownListView mlist;
	private int mCurPage = 1;
	private Handler mHandler;

	private List<DocListData> lvHotIssueData = new ArrayList<DocListData>();
	private List<DocListData> lvHotIssueMoreData = new ArrayList<DocListData>();
	private DoctorListAdpter hotAdpter;
	private String id = " ";
	private String mCity = "";
	private RelativeLayout back;

	private View headViewLine;
	private LinearLayout nodataTv;
	private LoadingProgress mDialog;
	private HashMap<String, Object> lodHotIssueDatMap = new HashMap<>();
	private DoctorListApi mDctorListApi;

	public static DoctorListFragment newInstance(int position, String[] partId,
			String city) {
		DoctorListFragment f = new DoctorListFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		b.putStringArray("part2Id", partId);
		b.putString("city", city);
		f.setArguments(b);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Log.d(TAG, "onCreateView");
		View v = inflater.inflate(R.layout.fragment_doctor_list, container,
				false);
		nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata1);
		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDctorListApi = new DoctorListApi();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
		super.onActivityCreated(savedInstanceState);
		mCotext = getActivity();
		mDialog = new LoadingProgress(mCotext);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Log.e(TAG, "onStart");
		position = getArguments().getInt("position");
		part2Id = getArguments().getStringArray("part2Id");
		mCity = getArguments().getString("city");

		partIds = part2Id[position];
		id = partIds;
		mlist = (DropDownListView) getListView();

		initList();
	}

	void initList() {
		@SuppressLint("RestrictedApi") LayoutInflater mInflater = getLayoutInflater(null);
		if (headViewLine == null) {
			// 表头
			headViewLine = mInflater.inflate(R.layout.head_line, null);
			mlist.addHeaderView(headViewLine);
		}

		mHandler = getHandler();
		mDialog.startLoading();
		lodHotIssueData(true);


		mlist.setOnBottomListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lodHotIssueData(false);
			}
		});

		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpter, View v, int pos,
					long arg3) {

				String docId = lvHotIssueData.get(pos - 1).getUser_id();
				String docName = lvHotIssueData.get(pos - 1).getUsername();
				Intent it0 = new Intent();
				it0.putExtra("docId", docId);
				it0.putExtra("docName", docName);
				it0.putExtra("partId", partIds);
				it0.setClass(getActivity(), DoctorDetailsActivity592.class);
				startActivity(it0);

			}
		});
	}

//	void lodHotIssueData(final boolean isDonwn) {
//
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				// Log.d(TAG, "id=====" + id);
//				Message msg = null;
//				if (isDonwn) {
//					if (mCurPage == 1) {
//						lvHotIssueData = HttpData.loadDocListData1(id,
//								mCurPage, mCity, "1", "0");
//						// Log.e(TAG, "lvHotIssueData==" + lvHotIssueData);
//
//						msg = mHandler.obtainMessage(1);
//						msg.sendToTarget();
//					}
//				} else {
//					mCurPage++;
//					lvHotIssueMoreData = HttpData.loadDocListData1(id,
//							mCurPage, mCity, "1", "0");
//					msg = mHandler.obtainMessage(2);
//					msg.sendToTarget();
//				}
//			}
//		}).start();
//
//	}

	/**
	 * 列表数据联网请求
	 *
	 * @param isDonwn
	 */
	private void lodHotIssueData(final boolean isDonwn) {

		lodHotIssueDatMap.put("cateid",id);
		lodHotIssueDatMap.put("page",mCurPage+"");
		lodHotIssueDatMap.put("city",mCity);
		lodHotIssueDatMap.put("sort","1");
		lodHotIssueDatMap.put("kind","0");

		mDctorListApi.getCallBack(mCotext, lodHotIssueDatMap, new BaseCallBackListener<ArrayList<DocListData>>() {
			@Override
			public void onSuccess(ArrayList<DocListData> docListData) {
				Message msg;
				if (isDonwn) {
					if (mCurPage == 1) {
						lvHotIssueData = docListData;

						msg = mHandler.obtainMessage(1);
						msg.sendToTarget();
					}
				} else {
					mCurPage++;
					lvHotIssueMoreData = docListData;
					msg = mHandler.obtainMessage(2);
					msg.sendToTarget();
				}
			}
		});
	}


	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					// Log.e(TAG, "lvHotIssueData==" + lvHotIssueData);

					if (null != lvHotIssueData  && lvHotIssueData.size() > 0) {

						mDialog.stopLoading();

						if (null != getActivity()) {
							hotAdpter = new DoctorListAdpter(getActivity(), lvHotIssueData);
							setListAdapter(hotAdpter);
						}


						mlist.onBottomComplete();
					} else {
						mDialog.stopLoading();
						nodataTv.setVisibility(View.VISIBLE);
						mlist.setVisibility(View.GONE);
						// ViewInject.toast("您还没有收藏的专家");
					}
					break;
				case 2:
					if (null!=lvHotIssueMoreData &&lvHotIssueMoreData.size()>0) {
						hotAdpter.add(lvHotIssueMoreData);
						hotAdpter.notifyDataSetChanged();
						mlist.onBottomComplete();
					} else {
						mlist.setHasMore(false);
						mlist.setShowFooterWhenNoMore(true);
						mlist.onBottomComplete();
					}
					break;
				}

			}
		};
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MainScreen"); // 统计页面
		StatService.onResume(getActivity());
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MainScreen");
		StatService.onPause(getActivity());
	}
}