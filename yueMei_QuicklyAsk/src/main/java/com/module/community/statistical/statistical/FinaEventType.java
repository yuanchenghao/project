package com.module.community.statistical.statistical;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * 统计类型
 * Created by 裴成浩 on 2019/3/22
 */
public class FinaEventType {
    //保存所有注册的数据
    private HashMap<String, ActivityTypeData> mEventType = new LinkedHashMap<>();

    private static final String TaoDetailActivity = "com.module.commonview.activity.TaoDetailActivity";

    private static final String DoctorDetailsActivity592 = "com.module.doctor.controller.activity.DoctorDetailsActivity592";

    private static final String HosDetailActivity = "com.module.doctor.controller.activity.HosDetailActivity";

    private static final String ProjectDetailsActivity = "com.module.home.controller.activity.ProjectDetailsActivity";

    private static final String TaoActivity623 = "com.module.taodetail.controller.activity.TaoActivity623";

    private static final String CommunityActivity = "com.module.community.controller.activity.CommunityActivity";

    private static final String HomePersonalActivity550 = "com.module.my.controller.activity.HomePersonalActivity550";

    private static final String HomeDiarySXActivity = "com.module.other.activity.HomeDiarySXActivity";

    private static final String MoreQuanZiActivity = "com.module.home.controller.activity.MoreQuanZiActivity";

    private static final String VideoListActivity = "com.module.community.controller.activity.VideoListActivity";

    private static final String SearchAllActivity668 = "com.module.home.controller.activity.SearchAllActivity668";

    private static final String ProjectDetailActivity638 = "com.module.commonview.activity.ProjectDetailActivity638";

    private static final String ChatBaseActivity = "com.module.commonview.activity.ChatBaseActivity";

    private static final String DiariesAndPostsActivity = "com.module.commonview.activity.DiariesAndPostsActivity";


    private static volatile FinaEventType finaEventType;

    private FinaEventType() {
    }

    public static FinaEventType getInstance() {
        if (finaEventType == null) {
            synchronized (FinaEventType.class) {
                if (finaEventType == null) {
                    finaEventType = new FinaEventType();
                }
            }
        }
        return finaEventType;
    }


    /**
     * 把数据存起来
     */
    void configInterface() {
        //淘整型页面
        mEventType.put(TaoDetailActivity, new ActivityTypeData("2"));
        //医生详情页面
        mEventType.put(DoctorDetailsActivity592, new ActivityTypeData("3"));
        //医院详情页面
        mEventType.put(HosDetailActivity, new ActivityTypeData("4"));
        //频道页
        mEventType.put(ProjectDetailsActivity, new ActivityTypeData("96"));
        //陶整形页面
        mEventType.put(TaoActivity623, new ActivityTypeData("49"));
        //社区首页
        mEventType.put(CommunityActivity, new ActivityTypeData("25"));
        //我的页面
        mEventType.put(HomePersonalActivity550, new ActivityTypeData("62"));
        //日记精选
        mEventType.put(HomeDiarySXActivity, new ActivityTypeData("18"));
        //热门话题列表
        mEventType.put(MoreQuanZiActivity, new ActivityTypeData("117"));
        //视频流页面
        mEventType.put(VideoListActivity, new ActivityTypeData("136"));
        //搜索页面
        mEventType.put(SearchAllActivity668, new ActivityTypeData("39"));
        //项目淘列表
        mEventType.put(ProjectDetailActivity638, new ActivityTypeData("20"));
        //私信页面
        mEventType.put(ChatBaseActivity, new ActivityTypeData("63"));
        //日记本详情页面
        mEventType.put(DiariesAndPostsActivity, new ActivityTypeData("45"));
    }


    /**
     * 获取Type列表
     *
     * @return
     */
    public HashMap<String, ActivityTypeData> getmEventType() {
        return mEventType;
    }
}
