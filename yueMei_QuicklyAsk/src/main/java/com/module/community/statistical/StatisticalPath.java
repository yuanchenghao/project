package com.module.community.statistical;

import android.text.TextUtils;
import android.util.Log;

import com.module.MyApplication;
import com.module.community.model.bean.StatisticalPathData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;

import org.aspectj.lang.annotation.Aspect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 需要埋点的接口获取
 * Created by 裴成浩 on 2018/10/9.
 */
@Aspect
public class StatisticalPath {
    private String TAG = "StatisticalPath";
    private ArrayList<StatisticalPathData> pathData = new ArrayList<>();

    private StatisticalPath() {

    }

    private static StatisticalPath statisticalPath = null;

    public synchronized static StatisticalPath getInstance() {
        if (statisticalPath == null) {
            statisticalPath = new StatisticalPath();
        }
        return statisticalPath;
    }

    /**
     * 获取统计路径
     */
    public void getStatistical() {
        String messageArr = Cfg.loadStr(MyApplication.getContext(), FinalConstant.MESSAGE_ARR, "");
        if (!TextUtils.isEmpty(messageArr)) {
            pathData = JSONUtil.jsonToArrayList(messageArr, StatisticalPathData.class);
            Log.e(TAG, "pathData111 === " + pathData.size());
//            if (onRequestCallback != null) {
//                onRequestCallback.onCallBack();
//            }
        } else {
            //获取要统计的路径
            NetWork.getInstance().call(FinalConstant1.MESSAGE, "gettjArr", new HashMap<String, Object>(), new ServerCallback() {
                @Override
                public void onServerCallback(ServerData mData) {
                    Cfg.saveStr(MyApplication.getContext(), FinalConstant.MESSAGE_ARR, mData.data);
                    Log.e(TAG, "mData === " + mData.toString());
                    Log.e(TAG, "pathData222 === " + pathData.size());
                    if ("1".equals(mData.code)) {
                        pathData = JSONUtil.jsonToArrayList(mData.data, StatisticalPathData.class);

//                        if (onRequestCallback != null) {
//                            onRequestCallback.onCallBack();
//                        }
                    }
                }
            });
        }
    }

    /**
     * 获取要统计的接口
     *
     * @return
     */
    public List<StatisticalPathData> getPathData() {
        return pathData;
    }


//    private OnRequestCallback onRequestCallback;
//
//    public interface OnRequestCallback {
//        void onCallBack();
//    }
//
//    public void setOnRequestCallback(OnRequestCallback onRequestCallback) {
//        this.onRequestCallback = onRequestCallback;
//    }
}
