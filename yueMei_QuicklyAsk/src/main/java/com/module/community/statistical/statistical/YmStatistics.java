package com.module.community.statistical.statistical;

import android.text.TextUtils;
import android.util.Log;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.module.MyApplication;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyask.activity.BuildConfig;
import com.quicklyask.util.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 悦美自己的统计
 * Created by 裴成浩 on 2019/3/22
 */
public class YmStatistics {
    private static String TAG = "YmStatistics";
    private static StringBuilder tongjiUrl;

    private static volatile YmStatistics ymStatistics;

    private YmStatistics() {
        //拼接统计连接
        tongjiUrl = new StringBuilder(FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_EMPTY + FinalConstant1.SYMBOL2 + FinalConstant1.SYMBOL3);
    }

    public static YmStatistics getInstance() {
        if (ymStatistics == null) {
            synchronized (YmStatistics.class) {
                if (ymStatistics == null) {
                    ymStatistics = new YmStatistics();
                }
            }
        }
        return ymStatistics;
    }

    /**
     * 初始化 统计
     */
    public void initStatistics() {
        //拼接写死参数
        tongjiUrl.append("source").append(FinalConstant1.SYMBOL4).append("1").append(FinalConstant1.SYMBOL5);
        tongjiUrl.append("event").append(FinalConstant1.SYMBOL4).append("1").append(FinalConstant1.SYMBOL5);
        tongjiUrl.append(FinalConstant1.SESSIONID).append(FinalConstant1.SYMBOL4).append(SignUtils.getSessionid()).append(FinalConstant1.SYMBOL5);
        tongjiUrl.append("utm_source").append(FinalConstant1.SYMBOL4).append(FinalConstant1.YUEMEI_MARKET).append(FinalConstant1.SYMBOL5);                      //渠道
        tongjiUrl.append("ym_onlyk").append(FinalConstant1.SYMBOL4).append(Utils.getImei()).append(FinalConstant1.SYMBOL5);                                     //唯一标识
        tongjiUrl.append("from").append(FinalConstant1.SYMBOL4).append("4").append(FinalConstant1.SYMBOL5);                                                     //写死android是4
        tongjiUrl.append("v").append(FinalConstant1.SYMBOL4).append(BuildConfig.VERSION_NAME).append(FinalConstant1.SYMBOL5);


        Log.e(TAG, "tongjiUrl111 == " + tongjiUrl);

        //获取统计activity对应的type
        FinaEventType.getInstance().configInterface();
    }

    public void tongjiApp(EventParamData eventParamData) {
        tongjiApp(eventParamData, null, null);
    }

    public void tongjiApp(HashMap<String, String> otherMaps) {
        tongjiApp(null, otherMaps);
    }

    public void tongjiApp(EventParamData eventParamData, ActivityTypeData typeData) {
        tongjiApp(eventParamData, null, typeData);
    }

    public void tongjiApp(EventParamData eventParamData, HashMap<String, String> otherMaps) {
        tongjiApp(eventParamData, otherMaps, null);
    }

    public void tongjiApp(EventParamData eventParamData, HashMap<String, String> otherMaps, ActivityTypeData typeData) {

        try {

            StringBuilder tongjiParam = new StringBuilder();

            //拼接会变化页面对应的参数
            if (typeData == null) {
                typeData = getTypeString();
            }
            String[] TypeDatas = getFiledName(typeData);
            for (int i = 0; i < TypeDatas.length; i++) {
                String attribute = TypeDatas[i];
                if (otherMaps == null || !otherMaps.containsKey(attribute)) {
                    Object fieldValueByName = getFieldValueByName(attribute, typeData);
                    if (fieldValueByName != null) {
                        tongjiParam.append(attribute).append(FinalConstant1.SYMBOL4).append(fieldValueByName).append(FinalConstant1.SYMBOL5);
                    }
                }
            }

//            Log.e(TAG, "urls222 = " + tongjiParam.toString());

            //获取公共参数对象中所有属性名，并拼接参数
            if (eventParamData != null) {
                String[] filedName = getFiledName(eventParamData);

                for (int i = 0; i < filedName.length; i++) {
                    String attribute = filedName[i];
                    if (otherMaps == null || !otherMaps.containsKey(attribute)) {
                        Object fieldValueByName = getFieldValueByName(attribute, eventParamData);
                        if (fieldValueByName != null) {
                            tongjiParam.append(attribute).append(FinalConstant1.SYMBOL4).append(fieldValueByName).append(FinalConstant1.SYMBOL5);
                        }
                    }
                }
            }
//            Log.e(TAG, "urls444 = " + tongjiParam.toString());

            //拼接会变化的参数
            tongjiParam.append("user_id").append(FinalConstant1.SYMBOL4).append(Utils.getUid()).append(FinalConstant1.SYMBOL5);                                       //用户id
            tongjiParam.append("others").append(FinalConstant1.SYMBOL4).append(Utils.getCity()).append(FinalConstant1.SYMBOL5);                                       //定位城市
            tongjiParam.append("refresh").append(FinalConstant1.SYMBOL4).append(System.currentTimeMillis());                                                            //时间戳

//            Log.e(TAG, "urls555 = " + tongjiParam.toString());
            //把非公共的参数拼接
            if (otherMaps != null) {
//                Log.e(TAG, "urls6666 = " + otherMaps.toString());
                for (Map.Entry<String,String> entry : otherMaps.entrySet()) {
                    if (!TextUtils.isEmpty(entry.getValue())){
                        tongjiParam.append(FinalConstant1.SYMBOL5).append(entry.getKey())
                                .append(FinalConstant1.SYMBOL4).append(entry.getValue());
                    }
                }
            }

//            Log.e(TAG, "urls333 = " + tongjiParam.toString());
            Log.e(TAG, "最后请求的链接 = " + tongjiUrl.toString() + tongjiParam.toString());

            networkStatistics(tongjiParam);

        } catch (Exception e) {
            Log.e(TAG, "e === " + e.toString());
            e.printStackTrace();
        }
    }


    /**
     * 联网请求统计
     *
     * @param tongjiParam
     */
    private void networkStatistics(StringBuilder tongjiParam) {

        StringBuilder url = new StringBuilder(tongjiUrl.toString() + tongjiParam.toString());

//        Log.e(TAG, "url11 === " + url.toString());
        Map<String, Object> urlParams = getUrlParams(url.toString());
//        Log.e(TAG, "urlParams === " + urlParams.toString());
        String sign = SignUtils.getSign(urlParams);

//        Log.e(TAG, "sign === " + sign);

        url.append(FinalConstant1.SYMBOL5).append("sign").append(FinalConstant1.SYMBOL4).append(sign);

//        Log.e(TAG, "url222 === " + url.toString());

        OkGo.get(url.toString()).execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {
                Log.e(TAG, "统计成功");
            }
        });
    }


    /**
     * 获取属性名数组
     *
     * @param data :对象
     * @return ：属性名称数组
     */
    private String[] getFiledName(Object data) {
        Field[] fields = data.getClass().getDeclaredFields();
        String[] fieldNames = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldNames[i] = fields[i].getName();
        }
        return fieldNames;
    }

    /**
     * 根据属性名获取属性值
     *
     * @param fieldName  : 属性名
     * @param data：实体类对象
     * @return ：属性值
     */
    private Object getFieldValueByName(String fieldName, Object data) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = data.getClass().getMethod(getter);
            return method.invoke(data);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取当前activity对应的参数
     *
     * @return :如果没有注册返回默认值
     */
    private ActivityTypeData getTypeString() {
        ActivityTypeData type = new ActivityTypeData("0");                 //默认值id:0,type:0
        for (Map.Entry<String, ActivityTypeData> entry : FinaEventType.getInstance().getmEventType().entrySet()) {
            if (MyApplication.activityAddress.contains(entry.getKey())) {
                type = entry.getValue();
                break;
            }
        }
        return type;
    }

    /**
     * 获取链接所有参数
     *
     * @param params
     * @return
     */
    private Map<String, Object> getUrlParams(String params) {
        Map<String, Object> map = new HashMap<>();
        params = params.replace("?", ";");
        if (!params.contains(";")) {
            return map;
        }
        if (params.split(";").length > 0) {
            String[] arr = params.split(";")[1].split("&");
            for (String s : arr) {
                String key = s.split("=")[0];
                String value = s.split("=")[1];
                map.put(key, value);
            }
            return map;

        } else {
            return map;
        }
    }

}
