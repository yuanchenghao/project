package com.module.community.other;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.quicklyask.util.Utils;

public class StaggeredItemDecoretion extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        //判断左右列，方法2（推荐）
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof StaggeredGridLayoutManager){
            StaggeredGridLayoutManager.LayoutParams params = ((StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams());
            if (params.getSpanIndex() != GridLayoutManager.LayoutParams.INVALID_SPAN_ID) {
                //getSpanIndex方法不管控件高度如何，始终都是左右左右返回index
                if (params.getSpanIndex() % 2 == 0) {
                    //左列
                    outRect.left = Utils.dip2px(5);
                    outRect.right = Utils.dip2px((int) 2.5);
                } else {
                    //右列
                    outRect.left = Utils.dip2px((int) 2.5);
                    outRect.right = Utils.dip2px(5);
                }
            }
        }

    }
}
