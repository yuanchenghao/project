package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2018/10/11.
 */
public class StatisticalData {
    private String url;
    private String params;

    public StatisticalData(String url, String params) {
        this.url = url;
        this.params = params;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}
