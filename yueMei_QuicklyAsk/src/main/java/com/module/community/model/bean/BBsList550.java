/**
 * 
 */
package com.module.community.model.bean;

import java.util.List;

/**
 * 帖子列表数据
 * 
 * @author Robin
 * 
 */
public class BBsList550 {

	private String code;
	private String message;

	private List<BBsListData550> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<BBsListData550> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<BBsListData550> data) {
		this.data = data;
	}


}
