package com.module.community.model.bean;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/7/10.
 */
public class HomeCommunityTagTag implements Parcelable {
    private int partId;
    private String name;
    private boolean isSelected;     //判断是否选中


    protected HomeCommunityTagTag(Parcel in) {
        partId = in.readInt();
        name = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(partId);
        dest.writeString(name);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeCommunityTagTag> CREATOR = new Creator<HomeCommunityTagTag>() {
        @Override
        public HomeCommunityTagTag createFromParcel(Parcel in) {
            return new HomeCommunityTagTag(in);
        }

        @Override
        public HomeCommunityTagTag[] newArray(int size) {
            return new HomeCommunityTagTag[size];
        }
    };

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
