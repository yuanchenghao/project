package com.module.community.model.bean;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/7/13.
 */
public class CommunityStaggeredData {

    private ArrayList<CommunityStaggeredListData> data;
//    private CommunityStaggeredMetroData metro;
//    private ArrayList<ArrayList<CommunityStaggeredNoticeData>> notice;
    private ArrayList<CommunityBannerData> banner;
    private ArrayList<CommunityBannerData> katwo;
    private ArrayList<CommunityKaFive> kafive;
    private ArrayList<CommunityKaFive> changebeautiful;
    private int requestTime;

    public ArrayList<CommunityStaggeredListData> getData() {
        return data;
    }

    public void setData(ArrayList<CommunityStaggeredListData> data) {
        this.data = data;
    }

//    public CommunityStaggeredMetroData getMetro() {
//        return metro;
//    }
//
//    public void setMetro(CommunityStaggeredMetroData metro) {
//        this.metro = metro;
//    }
//
//    public ArrayList<ArrayList<CommunityStaggeredNoticeData>> getNotice() {
//        return notice;
//    }
//
//    public void setNotice(ArrayList<ArrayList<CommunityStaggeredNoticeData>> notice) {
//        this.notice = notice;
//    }

    public int getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(int requestTime) {
        this.requestTime = requestTime;
    }

    public ArrayList<CommunityBannerData> getBanner() {
        return banner;
    }

    public void setBanner(ArrayList<CommunityBannerData> banner) {
        this.banner = banner;
    }

    public ArrayList<CommunityBannerData> getKatwo() {
        return katwo;
    }

    public void setKatwo(ArrayList<CommunityBannerData> katwo) {
        this.katwo = katwo;
    }

    public ArrayList<CommunityKaFive> getKafive() {
        return kafive;
    }

    public void setKafive(ArrayList<CommunityKaFive> kafive) {
        this.kafive = kafive;
    }

    public ArrayList<CommunityKaFive> getChangebeautiful() {
        return changebeautiful;
    }

    public void setChangebeautiful(ArrayList<CommunityKaFive> changebeautiful) {
        this.changebeautiful = changebeautiful;
    }
}
