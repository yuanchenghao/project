package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/7/16
 */
public class OcpaActionsTraceData {
    private String trace;

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }
}
