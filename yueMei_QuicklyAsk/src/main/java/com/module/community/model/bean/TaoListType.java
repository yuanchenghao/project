package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/9/3
 */
public enum TaoListType {
    TOP,
    DATA,
    DOCTOR_LIST,
    FLASH_SALE, //秒杀
    LEADER_BOARD, //月销量排行
    BIG_PROMOTION, //大促
    RECOMMEND, //推荐数据
    LIKE,
    NOT_MORE
}
