package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class DoctorData implements Parcelable {
    private String doc_name;						//医生姓名
    private String doc_img;						//	医生头像
    private String book_num	;				//	预约
    private String diary_num;			//	案例
    private String hospital_name;			//	机构名称

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.doc_name);
        dest.writeString(this.doc_img);
        dest.writeString(this.book_num);
        dest.writeString(this.diary_num);
        dest.writeString(this.hospital_name);
    }

    public DoctorData() {
    }

    protected DoctorData(Parcel in) {
        this.doc_name = in.readString();
        this.doc_img = in.readString();
        this.book_num = in.readString();
        this.diary_num = in.readString();
        this.hospital_name = in.readString();
    }

    public static final Parcelable.Creator<DoctorData> CREATOR = new Parcelable.Creator<DoctorData>() {
        @Override
        public DoctorData createFromParcel(Parcel source) {
            return new DoctorData(source);
        }

        @Override
        public DoctorData[] newArray(int size) {
            return new DoctorData[size];
        }
    };

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getDoc_img() {
        return doc_img;
    }

    public void setDoc_img(String doc_img) {
        this.doc_img = doc_img;
    }

    public String getBook_num() {
        return book_num;
    }

    public void setBook_num(String book_num) {
        this.book_num = book_num;
    }

    public String getDiary_num() {
        return diary_num;
    }

    public void setDiary_num(String diary_num) {
        this.diary_num = diary_num;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }
}
