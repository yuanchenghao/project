package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/9/13.
 */
public class ShareDetailPictorialString implements Parcelable{
    private String img;

    protected ShareDetailPictorialString(Parcel in) {
        img = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShareDetailPictorialString> CREATOR = new Creator<ShareDetailPictorialString>() {
        @Override
        public ShareDetailPictorialString createFromParcel(Parcel in) {
            return new ShareDetailPictorialString(in);
        }

        @Override
        public ShareDetailPictorialString[] newArray(int size) {
            return new ShareDetailPictorialString[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
