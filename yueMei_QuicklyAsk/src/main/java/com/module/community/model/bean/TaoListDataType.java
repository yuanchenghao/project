package com.module.community.model.bean;

import com.module.home.model.bean.SearchTao;
import com.module.taodetail.model.bean.HomeTaoData;


/**
 * Created by 裴成浩 on 2019/9/3
 */
public class TaoListDataType {
    private TaoListDataEnum type;                       //列表、推荐type
    private HomeTaoData tao;                  //数据列表
    private SearchTao searchTao;

    public TaoListDataEnum getType() {
        return type;
    }

    public void setType(TaoListDataEnum type) {
        this.type = type;
    }

    public HomeTaoData getTao() {
        return tao;
    }

    public void setTao(HomeTaoData tao) {
        this.tao = tao;
    }

    public SearchTao getSearchTao() {
        return searchTao;
    }

    public void setSearchTao(SearchTao searchTao) {
        this.searchTao = searchTao;
    }
}
