package com.module.community.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/10/24.
 */

public class PartListApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.TAO, "parts", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.d("PartListApi",mData.toString());
                Log.d("PartListApi", mData.data);
                if ("1".equals(mData.code)){
                    try {
                        List<TaoPopItemIvData> taoPopItemIvDatas = JSONUtil.TransformProjectPop(mData.data);
                        listener.onSuccess(taoPopItemIvDatas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
