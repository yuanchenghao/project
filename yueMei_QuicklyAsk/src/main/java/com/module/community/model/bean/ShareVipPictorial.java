package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/9/13.
 */
public class ShareVipPictorial implements Parcelable{

    private String title;
    private String content;
    private String liulan;
    private String user_img;
    private String user_name;
    private String sun_img;
    private List<ShareDetailPictorialString> img;

    protected ShareVipPictorial(Parcel in) {
        title = in.readString();
        content = in.readString();
        liulan = in.readString();
        user_img = in.readString();
        user_name = in.readString();
        sun_img = in.readString();
        img = in.createTypedArrayList(ShareDetailPictorialString.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(liulan);
        dest.writeString(user_img);
        dest.writeString(user_name);
        dest.writeString(sun_img);
        dest.writeTypedList(img);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShareVipPictorial> CREATOR = new Creator<ShareVipPictorial>() {
        @Override
        public ShareVipPictorial createFromParcel(Parcel in) {
            return new ShareVipPictorial(in);
        }

        @Override
        public ShareVipPictorial[] newArray(int size) {
            return new ShareVipPictorial[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLiulan() {
        return liulan;
    }

    public void setLiulan(String liulan) {
        this.liulan = liulan;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getSun_img() {
        return sun_img;
    }

    public void setSun_img(String sun_img) {
        this.sun_img = sun_img;
    }

    public List<ShareDetailPictorialString> getImg() {
        return img;
    }

    public void setImg(List<ShareDetailPictorialString> img) {
        this.img = img;
    }
}
