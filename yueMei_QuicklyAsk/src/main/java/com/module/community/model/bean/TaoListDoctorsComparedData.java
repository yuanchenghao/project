package com.module.community.model.bean;

import com.module.home.model.bean.SearchResultDoctorList;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/3
 */
public class TaoListDoctorsComparedData {
    private boolean isSelected = false;
    private List<SearchResultDoctorList> list;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public List<SearchResultDoctorList> getList() {
        return list;
    }

    public void setList(List<SearchResultDoctorList> list) {
        this.list = list;
    }
}
