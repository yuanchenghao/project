package com.module.community.model.bean;

public class BBsButtonData {

	private String user_id;
	private String askorshare;
	private String p_id;
	private String prevPage;
	private String nextPage;
	private String isShow;
	private String moban;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAskorshare() {
		return askorshare;
	}

	public void setAskorshare(String askorshare) {
		this.askorshare = askorshare;
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public String getPrevPage() {
		return prevPage;
	}

	public void setPrevPage(String prevPage) {
		this.prevPage = prevPage;
	}

	public String getNextPage() {
		return nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}

	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	public String getMoban() {
		return moban;
	}

	public void setMoban(String moban) {
		this.moban = moban;
	}
}
