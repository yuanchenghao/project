package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/7/16
 */
public class OcpaActionsUserIdData {
    private String hash_imei;

    public String getHash_imei() {
        return hash_imei;
    }

    public void setHash_imei(String hash_imei) {
        this.hash_imei = hash_imei;
    }
}
