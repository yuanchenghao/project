package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/7/16
 */
public class OcpaActionsData {
    private int action_time;
    private OcpaActionsUserIdData user_id;
    private String action_type;

    public int getAction_time() {
        return action_time;
    }

    public void setAction_time(int action_time) {
        this.action_time = action_time;
    }

    public OcpaActionsUserIdData getUser_id() {
        return user_id;
    }

    public void setUser_id(OcpaActionsUserIdData user_id) {
        this.user_id = user_id;
    }

    public String getAction_type() {
        return action_type;
    }

    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

}
