package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class HospitalData implements Parcelable {
    private String hos_name;						//医院名称
    private String hos_img;						//	医院logo
    private String rateScale	;				//	服务百分比
    private String book_num;			//	预约
    private String diary_num;			//	案例
    private String distance;			//	距离
    private String business_district;			//	商圈
    private String hospital_type;			//	医院类型
    private String big_promotion_icon;			//	大促标识

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.hos_name);
        dest.writeString(this.hos_img);
        dest.writeString(this.rateScale);
        dest.writeString(this.book_num);
        dest.writeString(this.diary_num);
        dest.writeString(this.distance);
        dest.writeString(this.business_district);
        dest.writeString(this.hospital_type);
        dest.writeString(this.big_promotion_icon);
    }

    public HospitalData() {
    }

    protected HospitalData(Parcel in) {
        this.hos_name = in.readString();
        this.hos_img = in.readString();
        this.rateScale = in.readString();
        this.book_num = in.readString();
        this.diary_num = in.readString();
        this.distance = in.readString();
        this.business_district = in.readString();
        this.hospital_type = in.readString();
        this.big_promotion_icon = in.readString();
    }

    public static final Parcelable.Creator<HospitalData> CREATOR = new Parcelable.Creator<HospitalData>() {
        @Override
        public HospitalData createFromParcel(Parcel source) {
            return new HospitalData(source);
        }

        @Override
        public HospitalData[] newArray(int size) {
            return new HospitalData[size];
        }
    };

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getHos_img() {
        return hos_img;
    }

    public void setHos_img(String hos_img) {
        this.hos_img = hos_img;
    }

    public String getRateScale() {
        return rateScale;
    }

    public void setRateScale(String rateScale) {
        this.rateScale = rateScale;
    }

    public String getBook_num() {
        return book_num;
    }

    public void setBook_num(String book_num) {
        this.book_num = book_num;
    }

    public String getDiary_num() {
        return diary_num;
    }

    public void setDiary_num(String diary_num) {
        this.diary_num = diary_num;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBusiness_district() {
        return business_district;
    }

    public void setBusiness_district(String business_district) {
        this.business_district = business_district;
    }

    public String getHospital_type() {
        return hospital_type;
    }

    public void setHospital_type(String hospital_type) {
        this.hospital_type = hospital_type;
    }

    public String getBig_promotion_icon() {
        return big_promotion_icon;
    }

    public void setBig_promotion_icon(String big_promotion_icon) {
        this.big_promotion_icon = big_promotion_icon;
    }
}
