package com.module.community.model.bean;

/**
 * 统计路径数据
 * Created by 裴成浩 on 2018/10/9.
 */
public class StatisticalPathData {
    private String controller;          //控制器
    private String action;              //方法名

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getControllerAction() {
        return controller + "/" + action;
    }
}
