package com.module.community.model.bean;

public class VideoBean {

    /**
     * url : /201904/d9c12fa75f33a0c798f6768ef83a677a.mp4
     * cover : upload/forum/image/20190426/190426112203_b99f45.jpg
     * video_time : 0
     * sign :
     * cover_width : 1280
     * cover_height : 720
     */

    private String url;
    private String cover;
    private String video_time;
    private String sign;
    private String cover_width;
    private String cover_height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getVideo_time() {
        return video_time;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getCover_width() {
        return cover_width;
    }

    public void setCover_width(String cover_width) {
        this.cover_width = cover_width;
    }

    public String getCover_height() {
        return cover_height;
    }

    public void setCover_height(String cover_height) {
        this.cover_height = cover_height;
    }
}
