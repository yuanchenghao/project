/**
 * 
 */
package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author lenovo17
 * 
 */
public class BBsListTag implements Parcelable {

	private String id;
	private String url_name;
	private String level;
	private String p_id;
	private String top_id;
	private String status;
	private String rel_id;
	private String pc_url;
	private String m_url;
	private String name;

	protected BBsListTag(Parcel in) {
		id = in.readString();
		url_name = in.readString();
		level = in.readString();
		p_id = in.readString();
		top_id = in.readString();
		status = in.readString();
		rel_id = in.readString();
		pc_url = in.readString();
		m_url = in.readString();
		name = in.readString();
	}

	public static final Creator<BBsListTag> CREATOR = new Creator<BBsListTag>() {
		@Override
		public BBsListTag createFromParcel(Parcel in) {
			return new BBsListTag(in);
		}

		@Override
		public BBsListTag[] newArray(int size) {
			return new BBsListTag[size];
		}
	};

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl_name() {
		return url_name;
	}

	public void setUrl_name(String url_name) {
		this.url_name = url_name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public String getTop_id() {
		return top_id;
	}

	public void setTop_id(String top_id) {
		this.top_id = top_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPc_url() {
		return pc_url;
	}

	public void setPc_url(String pc_url) {
		this.pc_url = pc_url;
	}

	public String getM_url() {
		return m_url;
	}

	public void setM_url(String m_url) {
		this.m_url = m_url;
	}

	public String getRel_id() {
		return rel_id;
	}

	public void setRel_id(String rel_id) {
		this.rel_id = rel_id;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(url_name);
		dest.writeString(level);
		dest.writeString(p_id);
		dest.writeString(top_id);
		dest.writeString(status);
		dest.writeString(rel_id);
		dest.writeString(pc_url);
		dest.writeString(m_url);
		dest.writeString(name);
	}
}
