package com.module.community.model.bean;

public class BBsButton {
	private String code;
	private String message;
	private BBsButtonData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BBsButtonData getData() {
		return data;
	}

	public void setData(BBsButtonData data) {
		this.data = data;
	}

}
