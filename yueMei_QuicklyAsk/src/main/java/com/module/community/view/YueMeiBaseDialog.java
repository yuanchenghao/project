package com.module.community.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * 单利的Doalog双弹窗
 * Created by 裴成浩 on 2019/7/4
 */
public class YueMeiBaseDialog extends Dialog {

    private BtnClickListener mBtnClickListener;
    /**
     * 单例
     */
    private static volatile YueMeiBaseDialog yueMeiBaseDialog;

    private YueMeiBaseDialog(Context context) {
        super(context);
    }

    public static YueMeiBaseDialog getInstance(Context context) {
        if (yueMeiBaseDialog == null) {
            synchronized (YueMeiBaseDialog.class) {
                if (yueMeiBaseDialog == null) {
                    yueMeiBaseDialog = new YueMeiBaseDialog(context);
                }
            }
        }
        return yueMeiBaseDialog;
    }

    /**
     * 设置布局
     */
    public void initView(String message) {
        initView(message, "取消", "确认");
    }

    public void initView(String message, String leftMsg, String rightMsg) {
        // 指定布局
        this.setContentView(R.layout.yueme_dialog);

        // 根据id在布局中找到控件对象
        TextView mTip = findViewById(R.id.yuemei_tip);
        Button mLeftBtn = findViewById(R.id.yuemei_left);
        Button mRightBtn = findViewById(R.id.yuemei_right);

        //设置内容
        mTip.setText(message);
        mLeftBtn.setText(leftMsg);
        mRightBtn.setText(rightMsg);

        //左边点击事件
        mLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtnClickListener != null) {
                    mBtnClickListener.leftBtnClick();
                }
            }
        });

        //右边点击事件
        mRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtnClickListener != null) {
                    mBtnClickListener.rightBtnClick();
                }
            }
        });

        setCanceledOnTouchOutside(false);
    }

    public interface BtnClickListener {
        void leftBtnClick();

        void rightBtnClick();
    }

    public void setBtnClickListener(BtnClickListener btnClickListener) {
        mBtnClickListener = btnClickListener;
    }
}
