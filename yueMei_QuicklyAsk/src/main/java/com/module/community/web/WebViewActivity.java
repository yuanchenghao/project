package com.module.community.web;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.base.refresh.refresh.RefreshListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import butterknife.BindView;

import static com.qmuiteam.qmui.util.QMUIDisplayHelper.getStatusBarHeight;

/**
 * 封装的加载H5页面
 * Created by 裴成浩 on 2019/6/11
 */
public class WebViewActivity extends YMBaseActivity implements WebChromeClientImpl.OnWebChromeListener {

    @BindView(R.id.activity_web_view)
    LinearLayout activityWebView;
    @BindView(R.id.web_view_container)
    FrameLayout mWebViewContainer;
    @BindView(R.id.web_view_refresh_container)
    MyPullRefresh mRefreshWebViewContainer;

    private String TAG = "WebViewActivity";
    public static final String WEB_DATA = "WebData";
    private static final String JS_NAME = "Android";
    private WebViewInitImpl mWebViewInitializer;
    public WebView mWebView;
    private boolean isLogin = false;          //是否登录

    /**
     * 打开页面传递过来的参数
     */
    public WebData mWebData;
    private CommonTopBar mTopTitle;

    @Override
    protected int getLayoutId() {
        // 获取上个页面传递过来的数据
        mWebData = getIntent().getParcelableExtra(WEB_DATA);

        //主题设置
        int themeResid = mWebData.getThemeResid();
        if (themeResid > 0) {
            setTheme(themeResid);
        }
        isLogin = Utils.isLogin();


        return R.layout.activity_web_view;
    }

    @Override
    protected void initView() {
        // WebView初始化对象
        mWebViewInitializer = new WebViewInitImpl(WebViewActivity.this);
        // WebView初始化
        initWebView();


    }

    @Override
    protected void initData() {
        mWebViewInitializer.setOnWebChromeListener(this);
        if (mWebViewContainer.getChildCount() > 0) {
            mWebViewContainer.removeAllViews();
        }

        Log.e(TAG, "mWebData.isShowRefresh() == " + mWebData.isShowRefresh());
        Log.e(TAG, "mWebData.isShowTopbar() == " + mWebData.isShowTopbar());
        if (!mWebData.isShowTopbar()){
            setStatusBarView(WebViewActivity.this);
        }


        if (mWebData.isShowRefresh()) {
            mRefreshWebViewContainer.setVisibility(View.VISIBLE);
            mWebViewContainer.setVisibility(View.GONE);
            mRefreshWebViewContainer.addView(mWebView);
            mRefreshWebViewContainer.setRefreshListener(new RefreshListener() {
                @Override
                public void onRefresh() {
                    loadUrl();
                }
            });
        } else {
            mRefreshWebViewContainer.setVisibility(View.GONE);
            mWebViewContainer.setVisibility(View.VISIBLE);
            mWebViewContainer.addView(mWebView);
        }

//        if (mWebData.isRemoveUpper()){
//            finish();
//        }

        loadUrl();

    }

    @SuppressLint({"JavascriptInterface", "AddJavascriptInterface"})
    private void initWebView() {
        //是否需要标题
        Log.e(TAG, "mWebData.isShowTitle() == " + mWebData.isShowTitle());
        if (mWebData.isShowTitle()) {
            mTopTitle = new CommonTopBar(mContext);
            int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTopTitle.getLayoutParams();
            if (layoutParams != null){
                layoutParams.topMargin = statusbarHeight;
                mTopTitle.setLayoutParams(layoutParams);
            }
            activityWebView.addView(mTopTitle, 0);
        }

        if (mWebView != null) {
            mWebView.removeAllViews();
        } else {
            mWebView = mWebViewInitializer.initWebView(new WebView(this));
            mWebView.setWebViewClient(mWebViewInitializer.initWebViewClient());
            mWebView.setWebChromeClient(mWebViewInitializer.initWebChromeClient());
            //注入JS交互
            mWebView.addJavascriptInterface(new JsCallAndroid(), JS_NAME);
        }
    }

    // 标题回调
    @Override
    public void onReceivedTitle(WebView view, String title) {
        Log.e(TAG, "title == " + title);
        if (mTopTitle != null) {
            if (mWebData != null && !TextUtils.isEmpty(mWebData.title)) {
                mTopTitle.setCenterText(mWebData.title);
            } else {
                if (title.startsWith("http")) {
                    mTopTitle.setCenterText("悦美");
                } else {
                    mTopTitle.setCenterText(title);
                }
            }
        }
    }

    // 页面加载进度回调
    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if (mDialog == null) {
            return;
        }
        if (newProgress == 100) {
            mDialog.dismiss();

            //刷新关闭
            if (mRefreshWebViewContainer != null && mWebData.isShowRefresh()) {
                mRefreshWebViewContainer.finishRefresh();
            }
        } else {
//            if (!mDialog.isShowing()) {
//                mDialog.show();
//            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onRestart() {
        super.onRestart();
        //是否刷新页面
        if (isLogin != Utils.isLogin()) {
            loadUrl();
            isLogin = Utils.isLogin();
        } else {
            //调用js方法
            mWebView.evaluateJavascript(AndroidCallJs.htmlBackReFresh, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    if ("true".equals(value)) {
                        loadUrl();
                    }
                }
            });
        }
    }
    @Override
    public void onPause() {
        if (mWebView != null) {
            mWebView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (mWebView != null) {
            mWebView.onResume();
        }
        super.onResume();
    }


    @Override
    public void onDestroy() {
        if (mWebViewContainer != null && mWebView != null) {
            mWebView = null;
        }
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mWebView != null && (keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack(); // 浏览网页历史记录 goBack()和goForward()
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 处理相机相册选择
        if (mWebViewInitializer != null) {
            mWebViewInitializer.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * 加载URL
     */
    private void loadUrl() {
        Log.e(TAG, "mWebView == " + mWebView);
        if (mWebView != null && mWebData != null) {
            // 跳转并进行页面加载
            WebUtil.getInstance().loadPage(mWebView, mWebData.url);
        }
    }


    /**
     * 添加View到状态栏，在沉浸式状态下不侵入状态栏
     */
    public void setStatusBarView(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 生成一个状态栏大小的矩形
            View StatusView = createStatusView(activity);
            // 添加statusView到布局中
            ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();
            decorView.addView(StatusView);
            // 设置根布局的参数
            ViewGroup rootView = (ViewGroup) (((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0));
            rootView.setFitsSystemWindows(true);
        }
    }

    private View createStatusView(Activity activity) {
        int statusBarHeight = getStatusBarHeight(activity);
        View view = new View(activity);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, statusBarHeight);
        view.setLayoutParams(params);
        view.setBackgroundColor(Color.TRANSPARENT);
        return view;
    }



}
