package com.module.community.web;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * WebActivity传递参数的Bean
 * Created by 裴成浩 on 2019/6/11
 */
public class WebData implements Parcelable {
    public String url;                              //连接
    public String title;                            //标题头
    private boolean isShowTitle = true;             //是否显示标题
    private boolean isShowRefresh = true;           //是否有刷新
    private boolean isShowTopbar = false;            //状态栏是否侵入
    private boolean isRemoveUpper = false;
    private int themeResid = -1;                    //公共webView主题，如果是负数。使用默认的

    public WebData(String url) {
        this.url = url;
    }

    public WebData(String url, String title) {
        this.url = url;
        this.title = title;
    }


    protected WebData(Parcel in) {
        url = in.readString();
        title = in.readString();
        isShowTitle = in.readByte() != 0;
        isShowRefresh = in.readByte() != 0;
        themeResid = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(title);
        dest.writeByte((byte) (isShowTitle ? 1 : 0));
        dest.writeByte((byte) (isShowRefresh ? 1 : 0));
        dest.writeInt(themeResid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WebData> CREATOR = new Creator<WebData>() {
        @Override
        public WebData createFromParcel(Parcel in) {
            return new WebData(in);
        }

        @Override
        public WebData[] newArray(int size) {
            return new WebData[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isShowTitle() {
        return isShowTitle;
    }

    public void setShowTitle(boolean showTitle) {
        isShowTitle = showTitle;
    }

    public boolean isShowRefresh() {
        return isShowRefresh;
    }

    public void setShowRefresh(boolean showRefresh) {
        isShowRefresh = showRefresh;
    }

    public void setShowTopbar(boolean showTopbar) {
        isShowTopbar = showTopbar;
    }

    public boolean isRemoveUpper() {
        return isRemoveUpper;
    }

    public void setRemoveUpper(boolean removeUpper) {
        isRemoveUpper = removeUpper;
    }

    public boolean isShowTopbar() {
        return isShowTopbar;
    }

    public int getThemeResid() {
        return themeResid;
    }

    public void setThemeResid(int themeResid) {
        this.themeResid = themeResid;
    }
}
