package com.module.community.web;

import android.content.Context;
import android.content.Intent;

import com.module.other.other.RequestAndResultCode;


/**
 * web页面回调
 * Created by 裴成浩 on 2019/8/31
 */
public class WebViewActivityResult {

    private static volatile WebViewActivityResult webViewActivityResult;
    private String TAG = "WebViewActivityResult";

    private WebViewActivityResult() {
    }

    public static WebViewActivityResult getInstance() {
        if (webViewActivityResult == null) {
            synchronized (WebViewActivityResult.class) {
                if (webViewActivityResult == null) {
                    webViewActivityResult = new WebViewActivityResult();
                }
            }
        }
        return webViewActivityResult;
    }


    /**
     * web页面回调
     *
     * @param context
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        if (context instanceof WebViewActivity) {
             if(requestCode == RequestAndResultCode.WEB_VIEW_REQUEST_CODE){
                 switch (resultCode) {
                     case RequestAndResultCode.CITY_RESULT_CODE :
                         WebViewActivity webViewActivity = (WebViewActivity) context;
                         WebUtil.getInstance().loadPage(webViewActivity.mWebView, webViewActivity.mWebData.url);
                         break;
                 }
             }
        }
    }
}
