package com.module.community.controller.other;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * 不支持手势滑动的ScrollView
 * Created by 裴成浩 on 2018/7/19.
 */
public class CommunityScrollView extends ScrollView {
    public CommunityScrollView(Context context) {
        this(context, null);
    }

    public CommunityScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommunityScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }
}
