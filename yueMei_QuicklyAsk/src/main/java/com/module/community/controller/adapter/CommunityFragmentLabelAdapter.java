package com.module.community.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.community.model.bean.HomeCommunityTagTag;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/7/11.
 */
public class CommunityFragmentLabelAdapter extends RecyclerView.Adapter<CommunityFragmentLabelAdapter.ViewHolder> {

    private Context mContext;
    private String TAG = "CommunityFragmentLabelAdapter";
    private List<HomeCommunityTagTag> mData;
    private LayoutInflater mInflater;

    public CommunityFragmentLabelAdapter(Context context, ArrayList<HomeCommunityTagTag> data) {
        this.mContext = context;
        this.mData = data;

        Log.e(TAG, "mData == " + data.size());
        //设置第一个为默认选中的
        if (mData.size() > 0) {
            this.mData.get(0).setSelected(true);
        }

        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_community_fragment_label, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.mLabel.setText(mData.get(position).getName());

        //判断是否选中
        Log.e(TAG, "mData.get(position).isSelected() == " + mData.get(position).isSelected());
        if (mData.get(position).isSelected()) {
            holder.mLabel.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_tuoyuan_fdf1f31));
            holder.mLabel.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
        } else {
            holder.mLabel.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_tuiyuan_f6f6f6));
            holder.mLabel.setTextColor(Utils.getLocalColor(mContext, R.color._33));
        }

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.mLabel.getLayoutParams();
        if (position == 0) {
            layoutParams.leftMargin = Utils.dip2px(10);
            layoutParams.rightMargin = Utils.dip2px(5);

        } else if (position == mData.size() - 1) {
            layoutParams.leftMargin = Utils.dip2px(5);
            layoutParams.rightMargin = Utils.dip2px(10);
        } else {
            layoutParams.leftMargin = Utils.dip2px(5);
            layoutParams.rightMargin = Utils.dip2px(5);
        }

        holder.mLabel.setLayoutParams(layoutParams);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mLabel;

        public ViewHolder(View itemView) {
            super(itemView);

            mLabel = itemView.findViewById(R.id.community_label_fragment);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mData.get(getLayoutPosition()).isSelected()) {         //如果点击的是未选中的
                        //改为未选中
                        int selected = findSelected();
                        mData.get(selected).setSelected(false);
                        notifyItemChanged(selected);

                        //改为选中
                        Log.e(TAG, "getLayoutPosition() == " + getLayoutPosition());
                        mData.get(getLayoutPosition()).setSelected(true);
                        notifyItemChanged(getLayoutPosition());

                        if (itemCallBackListener != null) {
                            itemCallBackListener.onItemClick(v, getLayoutPosition());
                        }
                    }
                }
            });
        }
    }

    public int findSelected() {
        //查找当前选中的，改为未选中
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).isSelected()) {
                return i;
            }
        }
        return 0;
    }

    public List<HomeCommunityTagTag> getmData() {
        return mData;
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}