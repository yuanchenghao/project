package com.module.community.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.view.FunctionManager;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.community.model.bean.TaoListData;
import com.module.community.model.bean.TaoListDataEnum;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.model.bean.TaoListDoctors;
import com.module.community.model.bean.TaoListDoctorsCompared;
import com.module.community.model.bean.TaoListDoctorsComparedData;
import com.module.community.model.bean.TaoListDoctorsEnum;
import com.module.community.model.bean.TaoListType;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.home.controller.adapter.SearhLikeRecyclerAdapter;
import com.module.home.model.bean.ComparedBean;
import com.module.home.model.bean.DcotorComparData;
import com.module.home.model.bean.SearchActivityData;
import com.module.home.model.bean.SearchResultDoctor;
import com.module.home.model.bean.SearchResultDoctorControlParams;
import com.module.home.model.bean.SearchResultDoctorList;
import com.module.home.model.bean.SearchResultLike;
import com.module.home.model.bean.SearchResultTaoData;
import com.module.home.model.bean.SearchResultTaoData2;
import com.module.home.model.bean.SearchResultsTaoTag;
import com.module.home.model.bean.SearchTao;
import com.module.home.model.bean.SearchTaolistData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.module.taodetail.model.bean.PromotionNoticeTextData;
import com.module.taodetail.model.bean.SkuLabelLevel;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.ItemTimerTextView;

import org.apache.commons.collections.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/5/24
 */
public class TaoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "TaoListAdapter";
    private Activity mContext;

    private FunctionManager mFunctionManager;
    private LayoutInflater mLayoutInflater;

    private final int DATA = 1;
    private final int DOCTOR_LIST = 2;
    private final int LIKE = 3;
    private final int NOT_MORE = 4;
    private final int FLASH_SALE = 5;//秒杀
    private final int LEADER_BOARD = 6;//月销量排行
    private final int BIG_PROMOTION = 7;//大促
    private final int RECOMMEND = 8;//推荐

    private List<TaoListData> mTaoDatas;
    private boolean isHeadView = false;
    private boolean mIsHeightLigh = false;

    /**
     * 其他页页面
     *
     * @param context
     * @param data
     */
    public TaoListAdapter(Activity context, List<HomeTaoData> data) {
        this(context, data, null);
    }

    /**
     * 搜索词高亮
     * @param context
     * @param data
     * @param isHeightLigh 默认false  true为高亮
     */
    public TaoListAdapter(Activity context, List<HomeTaoData> data,boolean isHeightLigh) {
        this.mIsHeightLigh = isHeightLigh;
        SearchResultTaoData taoData = new SearchResultTaoData();
        taoData.setList(data);
        taoData.setComparedConsultative(null);
        initData(context, taoData, null);
    }

    /**
     * 频道页面
     *
     * @param context
     * @param data
     * @param doctors
     */
    public TaoListAdapter(Activity context, List<HomeTaoData> data, SearchResultDoctor doctors) {
        SearchResultTaoData taoData = new SearchResultTaoData();
        taoData.setList(data);
        taoData.setComparedConsultative(doctors);
        initData(context, taoData, null);
    }

    /**
     * 搜索页面多类型
     * @param context
     * @param data
     * @param isHeightLigh 默认false  true为高亮
     */
    public TaoListAdapter(Activity context, SearchResultTaoData2 data,boolean isHeightLigh) {
        this.mIsHeightLigh = isHeightLigh;
        initData(context, null, data);
    }


    @Override
    public int getItemViewType(int position) {
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListType type = taoListData.getType();
        if (type != null) {
            switch (type) {
                case DATA:
                    return DATA;
                case DOCTOR_LIST:
                    return DOCTOR_LIST;
                case BIG_PROMOTION:
                    return BIG_PROMOTION;
                case FLASH_SALE:
                    return FLASH_SALE;
                case LEADER_BOARD:
                    return LEADER_BOARD;
                case RECOMMEND:
                    return RECOMMEND;
                case LIKE:
                    return LIKE;
                case NOT_MORE:
                    return NOT_MORE;
            }
        }

        return DATA;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case LIKE:
                return new LikeViewHolder(mLayoutInflater.inflate(R.layout.item_search_tao_like_view, parent, false));
            case DOCTOR_LIST:
                return new DoctorViewHolder(mLayoutInflater.inflate(R.layout.item_search_tao_doctor_view, parent, false));
            case NOT_MORE:
                return new NotMoreViewHolder(mLayoutInflater.inflate(R.layout.list_searh_not_more_view, parent, false));
            case DATA:
                return new TaoViewHolder(mLayoutInflater.inflate(R.layout.item_tao_list_view, parent, false));
            case BIG_PROMOTION:
            case FLASH_SALE:
            case LEADER_BOARD:
            case RECOMMEND:
                return new TopViewHolder(mLayoutInflater.inflate(R.layout.item_search_top_view, parent, false));
            default:
                return new TaoViewHolder(mLayoutInflater.inflate(R.layout.item_tao_list_view, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TaoViewHolder) {
            setDataView((TaoViewHolder) holder, position);
        } else if (holder instanceof LikeViewHolder) {
            setLikeView((LikeViewHolder) holder, position);
        } else if (holder instanceof DoctorViewHolder) {
            setDoctorView((DoctorViewHolder) holder, position);
        } else if (holder instanceof NotMoreViewHolder) {
            setNotMoreView((NotMoreViewHolder) holder, position);
        } else if (holder instanceof TopViewHolder) {
            setTopView((TopViewHolder) holder, position);
        }


    }


    @Override
    public int getItemCount() {
        return mTaoDatas.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 淘数据
     *
     * @param viewHolder
     * @param pos
     */
    @SuppressLint("SetTextI18n")
    private void setDataView(final TaoViewHolder viewHolder, int pos) {
        HomeTaoData taoData = null;
        TaoListData taoListData = mTaoDatas.get(pos);
        TaoListType type = taoListData.getType();

        if (type == TaoListType.DATA) {
            viewHolder.itemView.setTag(type);
            TaoListDataType taoList = taoListData.getTaoList();
            HomeTaoData tao = taoList.getTao();
            if (null != tao) {
                taoData = tao;
            } else {
                SearchTao searchTao = taoList.getSearchTao();
                if (null != searchTao) {
                    taoData = searchTao.getTao();
                }
            }

            if (taoData != null) {
                //设置海报的显示隐藏
                if ("1".equals(taoData.getShixiao())) {
                    mFunctionManager.setRoundImageSrc(viewHolder.mImage, R.drawable.sall_null_2x, Utils.dip2px(5));
                } else {
                    mFunctionManager.setRoundImageSrc(viewHolder.mImage, taoData.getImg(), Utils.dip2px(5));
                }

                //视频标签显示隐藏
                if ("1".equals(taoData.getIs_have_video())) {
                    viewHolder.mImageVideoTag.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.mImageVideoTag.setVisibility(View.GONE);
                }

                //搜索词高亮
                if (mIsHeightLigh){
                    String highlightTitle = taoData.getHighlight_title();
                    if (!TextUtils.isEmpty(highlightTitle)){
                        try {
                            Log.e(TAG,"highlightTitleOrgin =="+highlightTitle);
                            String htmlTitle = URLDecoder.decode(highlightTitle.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");
                            Log.e(TAG,"highlightTitle =="+htmlTitle);
                            //设置标题
                            viewHolder.mTitle.setText(Html.fromHtml(htmlTitle));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    Log.e(TAG, "下标 == " + pos + "，标题是 == <" + taoData.getTitle() + "> " + taoData.getSubtitle());
                    viewHolder.mTitle.setText("< "+ taoData.getTitle() + ">" + taoData.getSubtitle());
                }



                //设置医生
                viewHolder.mDocName.setText(taoData.getDoc_name());

                //设置医院
                if (!TextUtils.isEmpty(taoData.getDoc_name())) {
                    viewHolder.mHosName.setText("，" + taoData.getHos_name());
                } else {
                    viewHolder.mHosName.setText(taoData.getHos_name());
                }

                //设置预定人数
                viewHolder.mRate.setText(taoData.getRate());

                //设置位置信息
                String business_district = taoData.getBusiness_district();
                String distance = taoData.getDistance();
                if (!TextUtils.isEmpty(business_district) && !TextUtils.isEmpty(distance)) {
                    business_district = business_district.trim();
                    distance = distance.trim();
                    viewHolder.mAddress.setText(business_district + distance);
                } else {
                    if (!TextUtils.isEmpty(business_district)) {
                        viewHolder.mAddress.setText(business_district);
                    } else if (!TextUtils.isEmpty(distance)) {
                        viewHolder.mAddress.setText(distance);
                    }
                }

                //设置劵后是否显示
                String coupon_type = taoData.getCoupon_type();
                if (!"0".equals(coupon_type)) {
                    viewHolder.mPriceTag.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.mPriceTag.setVisibility(View.GONE);
                }

                //设置价格
                String price = taoData.getPrice_discount();
                viewHolder.mPriceNum.setText(price);
                viewHolder.mPriceFeescalev.setText(taoData.getFeeScale());
                //设置plus会员价
                String member_price = taoData.getMember_price();
                if (!TextUtils.isEmpty(member_price)) {
                    if (Integer.parseInt(member_price) >= 0) {
                        viewHolder.mPricePlus.setVisibility(View.VISIBLE);
                        viewHolder.mPricePlusPrice.setText("¥" + member_price);
                    } else {
                        viewHolder.mPricePlus.setVisibility(View.GONE);
                    }
                }


                //设置奖牌
                SkuLabelLevel hospital_top = taoData.getHospital_top();
                if (hospital_top != null) {
                    if (!TextUtils.isEmpty(hospital_top.getDesc())) {
                        viewHolder.hocTopTagContainer.setVisibility(View.VISIBLE);
                        viewHolder.mTopLeval.setText("NO."+hospital_top.getLevel());
                        viewHolder.mTopTag.setText(hospital_top.getDesc());
                    } else {
                        viewHolder.hocTopTagContainer.setVisibility(View.GONE);
                    }
                }


                //设置标签
                List<Promotion> promotion = taoData.getPromotion();
                if (CollectionUtils.isNotEmpty(promotion)) {
                    viewHolder.mFlowLayout.setVisibility(View.VISIBLE);
                    setTagView(viewHolder.mFlowLayout, promotion);
                } else {
                    viewHolder.mFlowLayout.setVisibility(View.GONE);
                }


                //设置大促显示隐藏
                String saleData = taoData.getImg66();
                if (!TextUtils.isEmpty(saleData)) {
                    Log.e(TAG, "taoData.getImg66() == " + taoData.getImg66());
                    viewHolder.mSales.setVisibility(View.VISIBLE);
                    viewHolder.mBack.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(taoData.getImg66())
                            .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                            .placeholder(R.drawable.home_other_placeholder)
                            .error(R.drawable.home_other_placeholder)
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    int intrinsicWidth = resource.getIntrinsicWidth();
                                    int intrinsicHeight = resource.getIntrinsicHeight();

                                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.mSales.getLayoutParams();

                                    Log.e(TAG, "intrinsicWidth == " + intrinsicWidth);
                                    Log.e(TAG, "intrinsicHeight == " + intrinsicHeight);
                                    layoutParams.width = intrinsicWidth;
                                    layoutParams.height = intrinsicHeight;
                                    viewHolder.mSales.setLayoutParams(layoutParams);

                                    viewHolder.mSales.setImageDrawable(resource);
                                }
                            });

                } else {
                    viewHolder.mSales.setVisibility(View.GONE);
                    //返现显示隐藏设置
                    if (!TextUtils.isEmpty(price) && Integer.parseInt(price) > 1000) {
                        String isFanxian = taoData.getIs_fanxian();
                        if ("0".equals(isFanxian)) {
                            viewHolder.mBack.setVisibility(View.GONE);
                        } else {
                            viewHolder.mBack.setVisibility(View.VISIBLE);
                        }
                    } else {
                        viewHolder.mBack.setVisibility(View.GONE);
                    }
                }


                //底部标签设置
                ArrayList<SearchResultsTaoTag> bottomList = new ArrayList<>();

                //大促是否存在
                PromotionNoticeTextData promotionNoticeText = taoData.getPromotionNoticeText();
                if (promotionNoticeText != null) {
                    bottomList.add(new SearchResultsTaoTag(promotionNoticeText.getPromotionNotice(), promotionNoticeText.getPromotionType(), R.drawable.shape_ff6e59));
                }

                //分期是否存在
                String repayment = taoData.getRepayment();
                if (!TextUtils.isEmpty(repayment)) {
                    bottomList.add(new SearchResultsTaoTag(repayment, "分期", R.drawable.shape_ff94ab));
                }

                //保险是否存在
                String baoxian = taoData.getBaoxian();
                if (!TextUtils.isEmpty(baoxian)) {
                    bottomList.add(new SearchResultsTaoTag(baoxian, "保险", R.drawable.shape_66cccc));
                }

                //红包是否存在
                String hosRedPacket = taoData.getHos_red_packet();
                if (!TextUtils.isEmpty(hosRedPacket)) {
                    bottomList.add(new SearchResultsTaoTag(hosRedPacket, "红包", R.drawable.shape_fb5e79));
                }

                //设置底部数据
                setBottomTag(viewHolder.mBottom, bottomList);
            }
        }
    }

    private void setTopView(TopViewHolder holder, int position) {
        SearchActivityData searchActivityData = null;
        TaoListData taoListData = mTaoDatas.get(position);
        SearchTao searchTao = taoListData.getTaoList().getSearchTao();

        TaoListType taolistType = searchTao.getTaolistType();
        holder.itemView.setTag(taolistType);
        switch (taolistType) {
            case BIG_PROMOTION:
                holder.mItemTopBg.setBackgroundResource(R.drawable.promotion_commonbg);
                holder.mItemIcon.setBackgroundResource(R.drawable.promotion_gift);
                searchActivityData = searchTao.getBig_promotion();
                break;
            case FLASH_SALE:
                holder.mItemTopBg.setBackgroundResource(R.drawable.flash_sale_commbg);
                holder.mItemIcon.setBackgroundResource(R.drawable.flash_sale_clock);
                searchActivityData = searchTao.getFlash_sale();
                break;
            case LEADER_BOARD:
                holder.mItemTopBg.setBackgroundResource(R.drawable.item_yellow);
                holder.mItemIcon.setBackgroundResource(R.drawable.item_king);
                searchActivityData = searchTao.getLeader_board();
                break;
            case RECOMMEND:
                holder.mItemTopBg.setBackgroundResource(R.drawable.recommend_comoonbg);
                holder.mItemIcon.setBackgroundResource(R.drawable.recommend_topimg);
                searchActivityData = searchTao.getRecommend();
                break;
        }
        if (searchActivityData != null) {
            holder.mItemTitle.setText(searchActivityData.getTitle());
            String end_time = searchActivityData.getEnd_time();
            holder.mItemContainer.removeAllViews();
            if (!TextUtils.isEmpty(end_time) && !"0".equals(end_time)) {
                Date date = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentTime = simpleDateFormat.format(date);
                String endTimeAct = Utils.stampToPhpDate(end_time);
                long[] timeSubAct = Utils.getTimeSub(currentTime, endTimeAct);
                long[] timesAct = {timeSubAct[0], timeSubAct[1], timeSubAct[2], timeSubAct[3]};
                ItemTimerTextView timerTextView = new ItemTimerTextView(mContext);
                timerTextView.setTimes(timesAct);
                if (!timerTextView.isRun()) {
                    timerTextView.beginRun();
                }
                holder.mItemContainer.addView(timerTextView);
            } else {
                TextView textView = new TextView(mContext);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textView.setLayoutParams(layoutParams);
                textView.setTextColor(ContextCompat.getColor(mContext, R.color._33));
                textView.setTextSize(12);
                textView.setText(searchActivityData.getSubtitle());
                holder.mItemContainer.addView(textView);
            }
            final List<SearchTaolistData> taoList = searchActivityData.getTaoList();
            if (taolistType == TaoListType.LEADER_BOARD) {
                if (taoList.size() < 14) {
                    int imgs[] = new int[]{R.drawable.item_one, R.drawable.item_two, R.drawable.item_three
                            , R.drawable.item_four, R.drawable.item_five, R.drawable.item_six, R.drawable.item_seven
                            , R.drawable.item_eight, R.drawable.item_nine, R.drawable.item_ten, R.drawable.item_eleven
                            , R.drawable.item_twelve, R.drawable.item_thideteen};
                    for (int i = 0; i < taoList.size(); i++) {
                        SearchTaolistData searchTaolistData = taoList.get(i);
                        searchTaolistData.setImg_resouce(imgs[i]);
                    }
                }
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
            ItemAdapter itemAdapter = new ItemAdapter(R.layout.item_search_top_item, taoList);
            holder.mItemTopList.setLayoutManager(linearLayoutManager);
            holder.mItemTopList.setAdapter(itemAdapter);
            //单测流  -  大促专题SKU列表点击【附加参数（big_promotion.taoList.event_params）】
            itemAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    HashMap<String, String> event_params = taoList.get(position).getEvent_params();
                    event_params.put("show_style", "692_oneside");
                    YmStatistics.getInstance().tongjiApp(event_params);
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(taoList.get(position).getApp_url());
                }
            });
            //单测流  -  大促专题点击【附加参数（big_promotion.event_params）】
            final SearchActivityData finalSearchActivityData = searchActivityData;
            holder.mItemTopBg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> event_params = finalSearchActivityData.getEvent_params();
                    event_params.put("show_style", "692_oneside");
                    YmStatistics.getInstance().tongjiApp(event_params);
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(finalSearchActivityData.getJumpUrl());
                }
            });
        }

    }

    /**
     * 猜你喜欢
     *
     * @param holder
     * @param position
     */
    private void setLikeView(LikeViewHolder holder, final int position) {

        TaoListData taoListData = mTaoDatas.get(position);
        TaoListType type = taoListData.getType();
        if (type == TaoListType.LIKE) {
            List<SearchResultLike> mLikeLists = taoListData.getLikeList();
            if (mLikeLists != null) {
                //设置适配器
                ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 3);
                gridLayoutManager.setScrollEnable(false);
                SearhLikeRecyclerAdapter sortScreenItemAdapter = new SearhLikeRecyclerAdapter(mContext, mLikeLists);
                holder.mRecycler.setLayoutManager(gridLayoutManager);
                holder.mRecycler.setAdapter(sortScreenItemAdapter);

                sortScreenItemAdapter.setOnEventClickListener(new SearhLikeRecyclerAdapter.OnEventClickListener() {
                    @Override
                    public void onItemClick(SearchResultLike data) {
                        if (onLikeClickListener != null) {
                            onLikeClickListener.onLikeClick(position, data);
                        }
                    }
                });
            }
        }
    }

    /***
     * 医院位
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    private void setDoctorView(DoctorViewHolder holder, int position) {
        TaoListData taoListData = mTaoDatas.get(position);
        TaoListType type = taoListData.getType();

        if (type == TaoListType.DOCTOR_LIST) {
            holder.itemView.setTag(type);
            TaoListDoctors mDoctors = taoListData.getDoctors();
            if (mDoctors != null) {
                holder.mTitle.setText(mDoctors.getComparedTitle());
                List<TaoListDoctorsComparedData> doctorsList = mDoctors.getCompared().getDoctorsList();
                TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(getDoctorSelectPos(doctorsList));
                List<SearchResultDoctorList> comparedDoctors = taoListDoctorsComparedData.getList();
                SearchResultDoctorList doctor1 = comparedDoctors.get(0);
                SearchResultDoctorList doctor2 = comparedDoctors.get(1);
                //头像1
                mFunctionManager.setCircleImageSrc(holder.mPortrait1, doctor1.getDoctorsImg());
                //医生标签1
                holder.mTag1.setText(doctor1.getDoctorsTag());
                setTagBackground(holder.mTag1, doctor1.getDoctorsTagID());
                //医生名称1
                holder.mName1.setText(doctor1.getDoctorsName());
                //医生职称1
                holder.mLevel1.setText(doctor1.getDoctorsTitle());
                //医生所在医院1
                holder.mHospital1.setText(doctor1.getHospitalName());
                //医生口碑1
                holder.mScore1.setText("口碑：" + doctor1.getDiary_pf());
                //医生预定数1
                holder.mBooking1.setText(doctor1.getSku_order_num() + "人预订");

                //头像2
                mFunctionManager.setCircleImageSrc(holder.mPortrait2, doctor2.getDoctorsImg());
                //医生标签2
                holder.mTag2.setText(doctor2.getDoctorsTag());
                setTagBackground(holder.mTag2, doctor2.getDoctorsTagID());
                //医生名称2
                holder.mName2.setText(doctor2.getDoctorsName());
                //医生职称2
                holder.mLevel2.setText(doctor2.getDoctorsTitle());
                //医生所在医院2
                holder.mHospital2.setText(doctor2.getHospitalName());
                //医生口碑2
                holder.mScore2.setText("口碑：" + doctor2.getDiary_pf());
                //医生预定数2
                holder.mBooking2.setText(doctor2.getSku_order_num() + "人预订");
            }
        }
    }


    /**
     * 设置标签背景
     *
     * @param tag
     * @param tag_id
     */
    private void setTagBackground(TextView tag, String tag_id) {
        switch (tag_id) {
            case "1":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag1);
                break;
            case "2":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag2);
                break;
            case "3":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag3);
                break;
            case "4":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag4);
                break;
            case "5":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag5);
                break;
            case "6":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag6);
                break;
            case "7":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag7);
                break;
            case "8":
                tag.setBackgroundResource(R.drawable.shape_tao_list_tag8);
                break;
        }
    }

    /**
     * 没有更多
     *
     * @param holder
     * @param position
     */
    private void setNotMoreView(NotMoreViewHolder holder, int position) {

    }

    /**
     * 底部标签设置
     *
     * @param bottom
     * @param lists
     */
    private void setBottomTag(LinearLayout bottom, ArrayList<SearchResultsTaoTag> lists) {
        if (bottom.getChildCount() != lists.size()) {
            bottom.removeAllViews();
            for (SearchResultsTaoTag data : lists) {
                View view = View.inflate(mContext, R.layout.item_search_results_tao_bottom, null);
                TextView tag = view.findViewById(R.id.tao_list_bottom_tag);
                TextView content = view.findViewById(R.id.tao_list_bottom_content);
                tag.setText(data.getTagText());
                tag.setBackgroundResource(data.getResource());
                content.setText(data.getContent());

                ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.topMargin = Utils.dip2px(8);
                bottom.addView(view, lp);
            }
        }
    }

    /**
     * 初始化数据
     */
    private void initData(Activity context, SearchResultTaoData data, SearchResultTaoData2 searchData) {
        this.mContext = context;
        mFunctionManager = new FunctionManager(mContext);
        mLayoutInflater = LayoutInflater.from(mContext);
        mTaoDatas = setListData(data, searchData);
    }

    /**
     * 设置列表数据
     */
    private List<TaoListData> setListData(SearchResultTaoData data, SearchResultTaoData2 searchData) {

        if (data != null) {
            return setTaoListData(data);
        }
        if (searchData != null) {
            return setSearctTaoListData(searchData);
        }

        return null;
    }

    private List<TaoListData> setSearctTaoListData(SearchResultTaoData2 data) {
        //调整后的数据
        List<TaoListData> lists = new ArrayList<>();

        List<SearchTao> mTaoDatas = data.getList();

        List<SearchTao> mRecomendList = data.getRecomend_list();
        List<SearchResultLike> mLikeLists = data.getParts();
        String mRecomendTips = data.getRecomend_tips();

        //要插入的数据
        @SuppressLint("UseSparseArrays")
        Map<Integer, TaoListData> insertData = new HashMap<>();
        //主数据设置
        setTaoSearchData(mTaoDatas, TaoListDataEnum.LIST, lists);

        //设置对比咨询数据
        int firstInsert = 0;
        for (SearchTao mTaoData : mTaoDatas) {
            DcotorComparData compared = mTaoData.getCompared();
            if (compared != null) {
                ComparedBean comparedBean = compared.getCompared();
                String showSkuListPosition = comparedBean.getShowSkuListPosition();
                int showSkuListPositionA = Integer.parseInt(showSkuListPosition);

                TaoListDoctorsCompared taoListDoctorsCompared = new TaoListDoctorsCompared();
                if (firstInsert == 0) {
                    taoListDoctorsCompared.setDoctorsEnum(TaoListDoctorsEnum.TOP);
                    firstInsert++;
                } else {
                    taoListDoctorsCompared.setDoctorsEnum(TaoListDoctorsEnum.BOTTOM);
                }

                taoListDoctorsCompared.setShowSkuListPosition(comparedBean.getShowSkuListPosition());
                taoListDoctorsCompared.setChangeOneEventParams(comparedBean.getChangeOneEventParams());
                taoListDoctorsCompared.setExposureEventParams(comparedBean.getExposureEventParams());

                List<TaoListDoctorsComparedData> taoListDoctorsComparedData = new ArrayList<>();

                List<List<SearchResultDoctorList>> doctorsList = comparedBean.getDoctorsList();
                for (int i = 0; i < doctorsList.size(); i++) {
                    List<SearchResultDoctorList> searchResultDoctorLists = doctorsList.get(i);
                    TaoListDoctorsComparedData taoListDoctorsComparedData1 = new TaoListDoctorsComparedData();
                    taoListDoctorsComparedData1.setList(searchResultDoctorLists);
                    if (i == 0) {
                        taoListDoctorsComparedData1.setSelected(true);
                    } else {
                        taoListDoctorsComparedData1.setSelected(false);
                    }
                    taoListDoctorsComparedData.add(taoListDoctorsComparedData1);
                }

                taoListDoctorsCompared.setDoctorsList(taoListDoctorsComparedData);


                TaoListDoctors taoListDoctors = new TaoListDoctors();
                taoListDoctors.setComparedTitle(compared.getComparedTitle());
                taoListDoctors.setCompared(taoListDoctorsCompared);
                if (showSkuListPositionA < lists.size()) {
                    lists.get(showSkuListPositionA).setDoctors(taoListDoctors);
                } else {
                    lists.get(lists.size() - 1).setDoctors(taoListDoctors);
                }

            }
        }

        //猜你喜欢数据
        if (mLikeLists != null && mLikeLists.size() > 0) {
            TaoListData taoListData = new TaoListData();
            taoListData.setLikeList(mLikeLists);
            taoListData.setType(TaoListType.LIKE);
            insertData.put(11, taoListData);
        }

        //排序
        List<Integer> keys = new ArrayList<>(insertData.keySet());
        Collections.sort(keys);                 //正序排序
        Collections.reverse(keys);              //反转之后变成倒序 排序
        for (Integer key : keys) {
            Log.e(TAG, "key == " + key);
        }

        //没有更多数据提示
        if (!TextUtils.isEmpty(mRecomendTips) && mRecomendList != null && mRecomendList.size() > 0) {
            TaoListData taoListData = new TaoListData();
            taoListData.setRecomendTips(mRecomendTips);
            taoListData.setType(TaoListType.NOT_MORE);
            lists.add(taoListData);
        }

        //推荐数据设置
        setTaoSearchData(mRecomendList, TaoListDataEnum.RECOMMENDED, lists);

        return lists;
    }


    private List<TaoListData> setTaoListData(SearchResultTaoData data) {
        //调整后的数据
        List<TaoListData> lists = new ArrayList<>();
        List<HomeTaoData> mTaoDatas = data.getList();
        List<HomeTaoData> mRecomendList = data.getRecomend_list();
        SearchResultDoctor mDoctors = data.getComparedConsultative();
        List<SearchResultLike> mLikeLists = data.getParts();
        String mRecomendTips = data.getRecomend_tips();

        //要插入的数据
        @SuppressLint("UseSparseArrays")
        Map<Integer, TaoListData> insertData = new HashMap<>();
        //主数据设置

        setTaoData(mTaoDatas, TaoListDataEnum.LIST, lists);

        //医生数据插入
        if (mDoctors != null) {
            String comparedTitle = mDoctors.getComparedTitle();
            ComparedBean comparedA = mDoctors.getComparedA();
            ComparedBean comparedB = mDoctors.getComparedB();

            //插入对比数据A
            if (comparedA != null) {
                String showSkuListPosition = comparedA.getShowSkuListPosition();
                if (!TextUtils.isEmpty(showSkuListPosition)) {
                    int showSkuListPositionA = Integer.parseInt(showSkuListPosition);
                    if (showSkuListPositionA < lists.size()) {
                        TaoListDoctorsCompared taoListDoctorsCompared = new TaoListDoctorsCompared();
                        taoListDoctorsCompared.setDoctorsEnum(TaoListDoctorsEnum.TOP);
                        taoListDoctorsCompared.setShowSkuListPosition(comparedA.getShowSkuListPosition());
                        taoListDoctorsCompared.setChangeOneEventParams(comparedA.getChangeOneEventParams());
                        taoListDoctorsCompared.setExposureEventParams(comparedA.getExposureEventParams());

                        List<TaoListDoctorsComparedData> taoListDoctorsComparedData = new ArrayList<>();
                        List<List<SearchResultDoctorList>> doctorsList = comparedA.getDoctorsList();
                        for (int i = 0; i < doctorsList.size(); i++) {
                            List<SearchResultDoctorList> searchResultDoctorLists = doctorsList.get(i);
                            TaoListDoctorsComparedData taoListDoctorsComparedData1 = new TaoListDoctorsComparedData();
                            taoListDoctorsComparedData1.setList(searchResultDoctorLists);
                            if (i == 0) {
                                taoListDoctorsComparedData1.setSelected(true);
                            } else {
                                taoListDoctorsComparedData1.setSelected(false);
                            }
                            taoListDoctorsComparedData.add(taoListDoctorsComparedData1);
                        }

                        taoListDoctorsCompared.setDoctorsList(taoListDoctorsComparedData);

                        TaoListDoctors taoListDoctors = new TaoListDoctors();
                        taoListDoctors.setComparedTitle(comparedTitle);
                        taoListDoctors.setCompared(taoListDoctorsCompared);
                        TaoListData taoListData = new TaoListData();
                        taoListData.setType(TaoListType.DOCTOR_LIST);
                        taoListData.setDoctors(taoListDoctors);
                        insertData.put(showSkuListPositionA, taoListData);
                    }
                }
            }

            //插入对比数据2
            if (comparedB != null) {
                String showSkuListPosition = comparedB.getShowSkuListPosition();
                if (!TextUtils.isEmpty(showSkuListPosition)) {
                    int showSkuListPositionB = Integer.parseInt(showSkuListPosition);
                    if (showSkuListPositionB < lists.size()) {
                        TaoListDoctorsCompared taoListDoctorsCompared = new TaoListDoctorsCompared();
                        taoListDoctorsCompared.setDoctorsEnum(TaoListDoctorsEnum.BOTTOM);
                        taoListDoctorsCompared.setShowSkuListPosition(comparedB.getShowSkuListPosition());
                        taoListDoctorsCompared.setChangeOneEventParams(comparedB.getChangeOneEventParams());
                        taoListDoctorsCompared.setExposureEventParams(comparedB.getExposureEventParams());

                        List<TaoListDoctorsComparedData> taoListDoctorsComparedData = new ArrayList<>();
                        List<List<SearchResultDoctorList>> doctorsList = comparedB.getDoctorsList();
                        for (int i = 0; i < doctorsList.size(); i++) {
                            List<SearchResultDoctorList> searchResultDoctorLists = doctorsList.get(i);
                            TaoListDoctorsComparedData taoListDoctorsComparedData1 = new TaoListDoctorsComparedData();
                            taoListDoctorsComparedData1.setList(searchResultDoctorLists);
                            if (i == 0) {
                                taoListDoctorsComparedData1.setSelected(true);
                            } else {
                                taoListDoctorsComparedData1.setSelected(false);
                            }
                            taoListDoctorsComparedData.add(taoListDoctorsComparedData1);
                        }

                        taoListDoctorsCompared.setDoctorsList(taoListDoctorsComparedData);

                        TaoListDoctors taoListDoctors = new TaoListDoctors();
                        taoListDoctors.setComparedTitle(comparedTitle);
                        taoListDoctors.setCompared(taoListDoctorsCompared);
                        TaoListData taoListData = new TaoListData();
                        taoListData.setType(TaoListType.DOCTOR_LIST);
                        taoListData.setDoctors(taoListDoctors);
                        insertData.put(showSkuListPositionB, taoListData);
                    }
                }
            }
        }


        //猜你喜欢数据
        if (mLikeLists != null && mLikeLists.size() > 0) {
            TaoListData taoListData = new TaoListData();
            taoListData.setLikeList(mLikeLists);
            taoListData.setType(TaoListType.LIKE);
            insertData.put(11, taoListData);
        }

        //排序
        List<Integer> keys = new ArrayList<>(insertData.keySet());
        Collections.sort(keys);                 //正序排序
        Collections.reverse(keys);              //反转之后变成倒序 排序
        for (Integer key : keys) {
            Log.e(TAG, "key == " + key);
        }

        //插入数据
        for (int i = keys.size() - 1; i >= 0; i--) {
            Integer integer = keys.get(i);
            if (integer < lists.size()) {
                TaoListData taoListData = insertData.get(integer);
                lists.add(integer, taoListData);
            }
        }

        //没有更多数据提示
        if (!TextUtils.isEmpty(mRecomendTips) && mRecomendList != null && mRecomendList.size() > 0) {
            TaoListData taoListData = new TaoListData();
            taoListData.setRecomendTips(mRecomendTips);
            taoListData.setType(TaoListType.NOT_MORE);
            lists.add(taoListData);
        }

        //推荐数据设置
        setTaoData(mRecomendList, TaoListDataEnum.RECOMMENDED, lists);
        return lists;
    }

    /**
     * 获取数据列表
     *
     * @return
     */
    public List<HomeTaoData> getData() {
        List<HomeTaoData> datas = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            if (data.getType() == TaoListType.DATA) {
                datas.add(data.getTaoList().getTao());
            }
        }
        return datas;
    }

    public List<HomeTaoData> getSearchData() {
        List<HomeTaoData> datas = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            if (data.getType() == TaoListType.DATA) {
                datas.add(data.getTaoList().getSearchTao().getTao());
            }
        }
        return datas;
    }


    /**
     * 获取对比咨询选中的下标
     *
     * @return
     */
    public int getDoctorSelectPos(List<TaoListDoctorsComparedData> datas) {
        for (int i = 0; i < datas.size(); i++) {
            TaoListDoctorsComparedData data = datas.get(i);
            if (data.isSelected()) {
                return i;
            }
        }
        return 0;
    }

    /**
     * 获取医院数据
     *
     * @return
     */
    public List<TaoListDoctors> getComparChatData() {
        List<TaoListDoctors> doctors = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            if (data.getType() == TaoListType.DOCTOR_LIST) {
                doctors.add(data.getDoctors());
            }
        }
        return doctors;
    }

    public List<SearchTao> getActiveData() {
        List<SearchTao> searchTaos = new ArrayList<>();
        for (TaoListData data : mTaoDatas) {
            searchTaos.add(data.getTaoList().getSearchTao());
        }
        return searchTaos;
    }

    /**
     * 设置医生选中下标
     */
    private void setDoctorSelectPos(TaoListDoctorsEnum doctorsEnum, int doctorSelectPos) {
        for (int i = 0; i < mTaoDatas.size(); i++) {
            TaoListData data = mTaoDatas.get(i);
            if (data.getType() == TaoListType.DOCTOR_LIST) {
                TaoListDoctorsCompared compared = data.getDoctors().getCompared();
                if (compared.getDoctorsEnum() == doctorsEnum) {
                    List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                    for (int i1 = 0; i1 < doctorsList.size(); i1++) {
                        TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(i1);
                        if (i1 == doctorSelectPos) {
                            taoListDoctorsComparedData.setSelected(true);
                        } else {
                            taoListDoctorsComparedData.setSelected(false);
                        }
                    }
                }
            }
        }
    }

    /**
     * 有头布局
     */
    public void isHeadView(boolean isHeadView) {
        this.isHeadView = isHeadView;
    }

    /**
     * 淘整型数据
     */
    public class TaoViewHolder extends RecyclerView.ViewHolder {
        ImageView mImage;
        ImageView mImageVideoTag;
        TextView mTitle;
        TextView mDocName;
        TextView mHosName;
        TextView mRate;
        TextView mAddress;
        TextView mPriceTag;
        TextView mPriceNum;
        TextView mPriceFeescalev;
        LinearLayout mPricePlus;
        TextView mPricePlusPrice;
        RelativeLayout mBack;
        LinearLayout hocTopTagContainer;
        TextView mTopTag;
        TextView mTopLeval;
        FlowLayout mFlowLayout;
        ImageView mSales;
        LinearLayout mBottom;

        public TaoViewHolder(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.tao_list_image);
            mImageVideoTag = itemView.findViewById(R.id.tao_list_image_video_tag);
            mTitle = itemView.findViewById(R.id.tao_list_title);
            mDocName = itemView.findViewById(R.id.tao_list_docname);
            mHosName = itemView.findViewById(R.id.tao_list_hosname);
            mRate = itemView.findViewById(R.id.tao_list_rate);
            mAddress = itemView.findViewById(R.id.tao_list_address);
            mPriceTag = itemView.findViewById(R.id.tao_list_price_tag);
            mPriceNum = itemView.findViewById(R.id.tao_list_price_num);
            mPriceFeescalev = itemView.findViewById(R.id.tao_list_price_feescalev);
            mPricePlus = itemView.findViewById(R.id.tao_list_price_plus);
            mPricePlusPrice = itemView.findViewById(R.id.tao_list_price_plus_price);
            mBack = itemView.findViewById(R.id.tao_list_back);
            hocTopTagContainer = itemView.findViewById(R.id.tao_list_top_tag_container);
            mTopTag = itemView.findViewById(R.id.tao_list_top_tag);
            mTopLeval = itemView.findViewById(R.id.doc_list_item_level);
            mFlowLayout = itemView.findViewById(R.id.tao_list_flowLayout);
            mSales = itemView.findViewById(R.id.tao_list_sales);
            mBottom = itemView.findViewById(R.id.tao_list_bottom);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int layoutPosition = getLayoutPosition();
                    Log.e(TAG, "layoutPosition == " + layoutPosition);
                    if (onItemClickListener != null) {
                        if (isHeadView) {
                            layoutPosition--;
                        }

                        if (layoutPosition >= 0) {
                            TaoListDataType taoList = mTaoDatas.get(layoutPosition).getTaoList();
                            if (taoList != null) {
                                onItemClickListener.onItemClick(taoList, layoutPosition);
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 猜你喜欢
     */
    public class LikeViewHolder extends RecyclerView.ViewHolder {
        RecyclerView mRecycler;

        public LikeViewHolder(@NonNull View itemView) {
            super(itemView);
            mRecycler = itemView.findViewById(R.id.item_search_like_recycler);
        }
    }

    /**
     * 医院位
     */
    public class DoctorViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;

        LinearLayout mDoctorClick1;
        ImageView mPortrait1;
        TextView mTag1;
        TextView mName1;
        TextView mLevel1;
        TextView mHospital1;
        TextView mScore1;
        TextView mBooking1;
        RelativeLayout mAsk1;
        LinearLayout mChange;

        LinearLayout mDoctorClick2;
        ImageView mPortrait2;
        TextView mTag2;
        TextView mName2;
        TextView mLevel2;
        TextView mHospital2;
        TextView mScore2;
        TextView mBooking2;
        RelativeLayout mAsk2;

        public DoctorViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_search_tao_doctor_title);

            mDoctorClick1 = itemView.findViewById(R.id.item_search_tao_doctor_click1);
            mPortrait1 = itemView.findViewById(R.id.item_search_tao_doctor_portrait1);
            mTag1 = itemView.findViewById(R.id.item_search_tao_doctor_tag1);
            mName1 = itemView.findViewById(R.id.item_search_tao_doctor_name1);
            mLevel1 = itemView.findViewById(R.id.item_search_tao_doctor_level1);
            mHospital1 = itemView.findViewById(R.id.item_search_tao_doctor_hospital1);
            mScore1 = itemView.findViewById(R.id.item_search_tao_doctor_score1);
            mBooking1 = itemView.findViewById(R.id.item_search_tao_doctor_booking1);
            mAsk1 = itemView.findViewById(R.id.item_search_tao_doctor_ask1);
            mChange = itemView.findViewById(R.id.item_search_tao_doctor_change);

            mDoctorClick2 = itemView.findViewById(R.id.item_search_tao_doctor_click2);
            mPortrait2 = itemView.findViewById(R.id.item_search_tao_doctor_portrait2);
            mTag2 = itemView.findViewById(R.id.item_search_tao_doctor_tag2);
            mName2 = itemView.findViewById(R.id.item_search_tao_doctor_name2);
            mLevel2 = itemView.findViewById(R.id.item_search_tao_doctor_level2);
            mHospital2 = itemView.findViewById(R.id.item_search_tao_doctor_hospital2);
            mScore2 = itemView.findViewById(R.id.item_search_tao_doctor_score2);
            mBooking2 = itemView.findViewById(R.id.item_search_tao_doctor_booking2);
            mAsk2 = itemView.findViewById(R.id.item_search_tao_doctor_ask2);

            //问医生
            mDoctorClick1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onDoctorChatlickListener != null) {
                            int layoutPosition = getLayoutPosition();
                            if (isHeadView) {
                                layoutPosition--;
                            }

                            TaoListData taoListData = mTaoDatas.get(layoutPosition);
                            if (taoListData.getType() == TaoListType.DOCTOR_LIST) {
                                TaoListDoctors doctors = taoListData.getDoctors();
                                if (doctors != null) {
                                    TaoListDoctorsCompared compared = doctors.getCompared();
                                    if (compared != null) {
                                        List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                                        int doctorSelectPos = getDoctorSelectPos(doctorsList);
                                        TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(doctorSelectPos);
                                        List<SearchResultDoctorList> list = taoListDoctorsComparedData.getList();
                                        SearchResultDoctorList searchResultDoctorList = list.get(0);
                                        SearchResultDoctorControlParams typeControlParams = searchResultDoctorList.getTypeControlParams();

                                        WebData webData = new WebData(searchResultDoctorList.getJumpUrl());
                                        webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                                        webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                                        WebUtil.getInstance().startWebActivity(mContext, webData);
                                        HashMap<String, String> event_params = searchResultDoctorList.getEvent_params();
                                        onDoctorChatlickListener.onDoctorChatClick(event_params);
                                    }

                                }

                            }
                        }
                    }
                }
            });

            //问医生2
            mDoctorClick2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onDoctorChatlickListener != null) {
                            int layoutPosition = getLayoutPosition();
                            if (isHeadView) {
                                layoutPosition--;
                            }
                            TaoListData taoListData = mTaoDatas.get(layoutPosition);
                            if (taoListData.getType() == TaoListType.DOCTOR_LIST) {
                                TaoListDoctors doctors = taoListData.getDoctors();
                                if (doctors != null) {
                                    TaoListDoctorsCompared compared = doctors.getCompared();
                                    if (compared != null) {
                                        List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                                        int doctorSelectPos = getDoctorSelectPos(doctorsList);
                                        TaoListDoctorsComparedData taoListDoctorsComparedData = doctorsList.get(doctorSelectPos);
                                        List<SearchResultDoctorList> list = taoListDoctorsComparedData.getList();
                                        SearchResultDoctorList searchResultDoctorList = list.get(1);
                                        SearchResultDoctorControlParams typeControlParams = searchResultDoctorList.getTypeControlParams();

                                        WebData webData = new WebData(searchResultDoctorList.getJumpUrl());
                                        webData.setShowTitle("0".equals(typeControlParams.getIsHide()));
                                        webData.setShowRefresh("1".equals(typeControlParams.getIsRefresh()));
                                        WebUtil.getInstance().startWebActivity(mContext, webData);
                                        HashMap<String, String> event_params = searchResultDoctorList.getEvent_params();
                                        onDoctorChatlickListener.onDoctorChatClick(event_params);
                                    }

                                }

                            }
                        }


                    }
                }
            });

            //换一换
            mChange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int layoutPosition = getLayoutPosition();
                    if (isHeadView) {
                        layoutPosition--;
                    }

                    TaoListData taoListData = mTaoDatas.get(layoutPosition);

                    if (taoListData.getType() == TaoListType.DOCTOR_LIST) {
                        TaoListDoctors doctors = taoListData.getDoctors();
                        if (doctors == null) {
                            mFunctionManager.showShort("没有更多医生");
                            return;
                        }
                        TaoListDoctorsCompared compared = doctors.getCompared();
                        if (compared == null) {
                            mFunctionManager.showShort("没有更多医生");
                            return;
                        }
                        List<TaoListDoctorsComparedData> doctorsList = compared.getDoctorsList();
                        int doctorSelectPos = getDoctorSelectPos(doctorsList);
                        doctorSelectPos++;
                        if (doctorSelectPos >= doctorsList.size()) {
                            doctorSelectPos = 0;
                            mFunctionManager.showShort("没有更多医生");
                        }

                        setDoctorSelectPos(compared.getDoctorsEnum(), doctorSelectPos);
                        notifyDataSetChanged();

                        if (onDoctorChatlickListener != null) {
                            onDoctorChatlickListener.onChangeClick(compared, doctorSelectPos);
                        }
                    }


                }
            });
        }
    }


    /**
     * 销量榜
     */
    public class TopViewHolder extends RecyclerView.ViewHolder {

        private final FrameLayout mItemTopBg;
        private final ImageView mItemIcon;
        private final TextView mItemTitle;
        private final LinearLayout mItemContainer;
        private final RecyclerView mItemTopList;

        public TopViewHolder(@NonNull View itemView) {
            super(itemView);
            mItemTopBg = itemView.findViewById(R.id.item_search_top_bg);
            mItemIcon = itemView.findViewById(R.id.item_search_top_icon);
            mItemTitle = itemView.findViewById(R.id.item_search_tio_title);
            mItemContainer = itemView.findViewById(R.id.subtitle_container);
            mItemTopList = itemView.findViewById(R.id.item_search_top_list);
        }
    }

    /**
     * 没有更多了，不妨看看这些项目吧~
     */
    public class NotMoreViewHolder extends RecyclerView.ViewHolder {

        public NotMoreViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<Promotion> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            mFlowLayout.setMaxLine(1);
            for (int i = 0; i < lists.size(); i++) {
                Promotion promotion = (Promotion) lists.get(i);
                String styleType = promotion.getStyle_type();//1正常  2最近浏览
                TextView textView = new TextView(mContext);
                textView.setText(promotion.getTitle());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                if ("2".equals(styleType)){
                    textView.setBackgroundResource(R.drawable.sku_list_nearlook);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.ff7c4f));
                }else {
                    textView.setBackgroundResource(R.drawable.shopping_cart_sku_tab_background);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                }
                mFlowLayout.addView(textView);


            }
        }
    }

    /**
     * 分页加载
     *
     * @param data
     */
    public void addData(List<HomeTaoData> data) {
        addData(data, null);
    }

    /**
     * 分页加载
     *
     * @param datas
     * @param recomendList
     */
    public void addData(List<HomeTaoData> datas, List<HomeTaoData> recomendList) {
        int size = mTaoDatas.size();
        setTaoData(datas, TaoListDataEnum.LIST, mTaoDatas);
        setTaoData(recomendList, TaoListDataEnum.RECOMMENDED, mTaoDatas);
        notifyItemRangeInserted(size + 1, mTaoDatas.size() - size);
    }

    /**
     * 分页加载
     *
     * @param datas
     * @param recomendList
     */
    public void addDataSearch(List<SearchTao> datas, List<SearchTao> recomendList) {
        int size = mTaoDatas.size();
        setTaoSearchData(datas, TaoListDataEnum.LIST, mTaoDatas);
        setTaoSearchData(recomendList, TaoListDataEnum.RECOMMENDED, mTaoDatas);
        notifyItemRangeInserted(size + 1, mTaoDatas.size() - size);
    }

    /**
     * 设置淘列表+推荐列表数据
     *
     * @param datas     ：要插入的数据
     * @param list      ：插入数据类型
     * @param mTaoDatas ：要插入到的父数据列表
     */
    private void setTaoData(List<HomeTaoData> datas, TaoListDataEnum list, List<TaoListData> mTaoDatas) {
        if (datas != null && datas.size() > 0) {
            for (HomeTaoData tao : datas) {
                TaoListDataType taoListDataType = new TaoListDataType();
                taoListDataType.setType(list);
                taoListDataType.setTao(tao);
                TaoListData taoListData = new TaoListData();
                taoListData.setType(TaoListType.DATA);
                taoListData.setTaoList(taoListDataType);
                mTaoDatas.add(taoListData);
            }
        }
    }

    /**
     * 设置淘列表+推荐列表数据
     *
     * @param datas     ：要插入的数据
     * @param list      ：插入数据类型
     * @param mTaoDatas ：要插入到的父数据列表
     */
    private void setTaoSearchData(List<SearchTao> datas, TaoListDataEnum list, List<TaoListData> mTaoDatas) {
        if (datas != null && datas.size() > 0) {
            for (SearchTao tao : datas) {
                TaoListDataType taoListDataType = new TaoListDataType();
                taoListDataType.setType(list);
                taoListDataType.setSearchTao(tao);
                tao.handleListType();
                TaoListData taoListData = new TaoListData();
                taoListData.setType(tao.getTaolistType());
                taoListData.setTaoList(taoListDataType);
                mTaoDatas.add(taoListData);
            }
        }
    }

    /**
     * 获取列表数据
     *
     * @return
     */
    public List<TaoListData> getTaoDatas() {
        return mTaoDatas;
    }

    /**
     * 刷新数据
     *
     * @param data
     */
    public void refreshData(SearchResultTaoData data, SearchResultTaoData2 searchData) {
        mTaoDatas = setListData(data, searchData);
        notifyDataSetChanged();
    }

    //猜你喜欢点击
    public interface OnLikeClickListener {
        void onLikeClick(int position, SearchResultLike data);
    }

    private OnLikeClickListener onLikeClickListener;

    public void setOnLikeClickListener(OnLikeClickListener onLikeClickListener) {
        this.onLikeClickListener = onLikeClickListener;
    }


    //item点击
    public interface OnItemClickListener {
        void onItemClick(TaoListDataType taoData, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //医生咨询
    public interface OnDoctorChatlickListener {
        void onDoctorChatClick(HashMap<String, String> event_params);

        /**
         * @param compared  : 换一换item位所有数据
         * @param changePos :换一换的pos
         */
        void onChangeClick(TaoListDoctorsCompared compared, int changePos);
    }

    private OnDoctorChatlickListener onDoctorChatlickListener;

    public void setOnDoctorChatlickListener(OnDoctorChatlickListener onDoctorChatlickListener) {
        this.onDoctorChatlickListener = onDoctorChatlickListener;
    }
}
