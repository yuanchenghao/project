package com.module.community.controller.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

import com.module.base.view.YMBaseWebViewFragment;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.model.bean.HomeCommunityTagData;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONObject;

import java.net.URLDecoder;

import butterknife.BindView;

/**
 * 社区webView模版
 * Created by 裴成浩 on 2018/7/10.
 */
public class CommunityWebViewFragment extends YMBaseWebViewFragment {

    @BindView(R.id.community_web_view)
    SmartRefreshLayout mRefreshWebView;
    @BindView(R.id.community_web_view_nested)
    NestedScrollView mWebViewNested;
    private BaseWebViewClientMessage clientManager;
    private HomeCommunityTagData mData;
    private String TAG = "CommunityWebViewFragment";

    public static CommunityWebViewFragment newInstance(HomeCommunityTagData data) {
        CommunityWebViewFragment fragment = new CommunityWebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mData = getArguments().getParcelable("data");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_web_community;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initView(View view) {
        clientManager = new BaseWebViewClientMessage(mContext);

        mWebViewNested.addView(mWebView);

        //下拉刷新
        mRefreshWebView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                initWebVeiw();
            }
        });

        /**
         * 公共跳转外的跳转
         */
        clientManager.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String url) {
                try {
                    showWebDetail(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mWebView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent ev) {

                ((WebView)v).requestDisallowInterceptTouchEvent(true);

                return false;
            }
        });

        initWebVeiw();
    }

    @Override
    protected void initData(View view) {

    }

    private void showWebDetail(String urlStr) {
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;

            if (obj.getString("type").equals("1")) {// 医生详情页
                try {
                    String id = obj.getString("id");
                    String docname = URLDecoder.decode(obj.getString("docname"), "utf-8");

                    Intent it = new Intent();
                    it.setClass(mContext, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", docname);
                    it.putExtra("partId", "");
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (obj.getString("type").equals("6")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("_id");

                Intent it = new Intent();
                it.setClass(mContext, DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                startActivity(it);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        Log.e(TAG, "url === " + url);
        if (url.startsWith("type")) {
            clientManager.showWebDetail(url);
        } else {
            WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
        }
        return true;
    }

    @Override
    protected void onYmProgressChanged(WebView view, int newProgress) {
        if (newProgress == 100) {
            mRefreshWebView.finishRefresh();
        }

        super.onYmProgressChanged(view, newProgress);
    }

    /**
     * 初始化
     */
    private void initWebVeiw() {
        loadUrl(mData.getUrl());
    }

}
