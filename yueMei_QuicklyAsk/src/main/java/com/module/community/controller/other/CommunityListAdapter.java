package com.module.community.controller.other;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.community.model.bean.CommunityKaFive;
import com.module.community.model.bean.CommunityPost;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.List;

public class CommunityListAdapter extends BaseQuickAdapter<CommunityKaFive,BaseViewHolder> {

    private ImageView imageView;

    public CommunityListAdapter(int layoutResId, @Nullable List<CommunityKaFive> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CommunityKaFive item) {
        FrameLayout postContainer=helper.getView(R.id.post_container);
        ViewGroup.LayoutParams layoutParams = postContainer.getLayoutParams();
        int windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
        layoutParams.width=windowsWight-Utils.dip2px(30);
        postContainer.setLayoutParams(layoutParams);
        imageView = helper.getView(R.id.community_item_img);
        Glide.with(mContext)
                .load(item.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(7), GlidePartRoundTransform.CornerType.ALL))
                .into(imageView);


        CommunityPost post = item.getPost();
        if (post != null){
            helper.setVisible(R.id.community_item_post,true);
            ImageView postImg=helper.getView(R.id.community_item_post_img);
            Glide.with(mContext)
                    .load(post.getUser_img())
                    .transform(new GlideCircleTransform(mContext))
                    .into(postImg);

            helper.setText(R.id.community_item_post_title,post.getUser_name())
                    .setText(R.id.community_item_post_subtitle,post.getDaren())
                    .setText(R.id.community_itme_post_content,post.getTitle());
        }else {
            helper.setVisible(R.id.community_item_post,false);
        }
    }
}
