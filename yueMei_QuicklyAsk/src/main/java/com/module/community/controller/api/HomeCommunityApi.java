package com.module.community.controller.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.HomeCommunityData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

import simplecache.ACache;

/**
 * 社区页面数据获取
 * Created by 裴成浩 on 2018/3/13.
 */

public class HomeCommunityApi implements BaseCallBackApi {
    private ACache mCache;
    private String TAG = "HomeCommunityApi";

    private HashMap<String, Object> hashMap;  //传值容器
    public static final String HOMEBBS_HEAD_JSON = "homebbs_head_json";

    public HomeCommunityApi(Context context) {
        hashMap = new HashMap<>();
        mCache = ACache.get(context);
    }

    @Override
    public void getCallBack(Context context, final Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.BBS, "home", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "serverData.data 111 = " + mData.data);
                mCache.put(HOMEBBS_HEAD_JSON, mData.data);
                if ("1".equals(mData.code)) {
                    try {
                        HomeCommunityData communityData = JSONUtil.TransformSingleBean(mData.data, HomeCommunityData.class);
                        listener.onSuccess(communityData);
                    } catch (Exception e) {
                        Log.e(TAG, "e === " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }

}
