package com.module.community.controller.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseFragment;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.adapter.CommunityFragmentLabelAdapter;
import com.module.community.controller.adapter.CommunityPostAdapter;
import com.module.community.controller.adapter.CommunityPostFocusAdapter;
import com.module.community.controller.api.CommunityFragmentApi;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.CommunityPostData;
import com.module.community.model.bean.HomeCommunityTagData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.my.controller.activity.QuanziDetailActivity;
import com.module.my.model.bean.MyFansData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 社区帖子列表模版
 * Created by 裴成浩 on 2018/7/10.
 */
public class CommunityPostFragment extends YMBaseFragment {
    @BindView(R.id.community_post_refresh)
    SmartRefreshLayout mPullRefresh;
    @BindView(R.id.community_post_label_recycler)
    RecyclerView mLabelRecycler;
    @BindView(R.id.community_post_list_recycler)
    LoadMoreRecyclerView mListView;

    private String TAG = "CommunityPostFragment";

    private HomeCommunityTagData mData;
    private CommunityFragmentApi postApi;
    private ArrayList<BBsListData550> postDatas = new ArrayList<>();
    private ArrayList<MyFansData> interestDatas = new ArrayList<>();
    private int mPage = 1;
    private CommunityPostAdapter communityPostAdapter;
    private CommunityPostFocusAdapter mPostFocusAdapter;
    private CommunityFragmentLabelAdapter mCommunityFragmentAdapter;
    private View headerView;

    public static CommunityPostFragment newInstance(HomeCommunityTagData data) {
        CommunityPostFragment fragment = new CommunityPostFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mData = getArguments().getParcelable("data");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_post_community;
    }

    @Override
    protected void initView(View view) {
        if (mData.getTag().size() > 0) {
            mLabelRecycler.setVisibility(View.VISIBLE);
            mCommunityFragmentAdapter = new CommunityFragmentLabelAdapter(mContext, mData.getTag());
            mLabelRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            ((DefaultItemAnimator) mLabelRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
            mLabelRecycler.setAdapter(mCommunityFragmentAdapter);

            mCommunityFragmentAdapter.setOnItemCallBackListener(new CommunityFragmentLabelAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    mPage = 1;
                    loadingData(true);
                }
            });
        } else {
            mLabelRecycler.setVisibility(View.GONE);
        }

        mPullRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                mLabelRecycler.setEnabled(false);
                loadingData(true);
            }
        });

        //加载更多
        mListView.setLoadMoreListener(new LoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e(TAG, "onLoadMore........");
                if (focunsOrPost()) {
                    loadingData(false);
                } else {
                    mListView.setNoMore(true);
                }
            }
        });
    }

    @Override
    protected void initData(View view) {
        postApi = new CommunityFragmentApi(mData.getController(), mData.getAction());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mListView.setLayoutManager(linearLayoutManager);

        loadingData(true);
    }

    /**
     * 加载数据
     */
    public void loadingData(final boolean isRefresh) {
        postApi.addData("page", mPage + "");
        postApi.addData("id", mData.getId() + "");
        if (mCommunityFragmentAdapter != null && mCommunityFragmentAdapter.getmData().size() > 0) {
            postApi.addData("partId", mCommunityFragmentAdapter.getmData().get(mCommunityFragmentAdapter.findSelected()).getPartId() + "");
        }
        postApi.getCallBack(mContext, postApi.getHashMap(), new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String json) {
                mPullRefresh.finishRefresh();
                mPage++;
                try {
                    if (mData.getId() == 1) {     //关注帖子列表
                        CommunityPostData data = JSONUtil.TransformSingleBean(json, CommunityPostData.class);
                        postDatas = data.getData();
                        interestDatas = data.getInterest();
                        Log.e(TAG, "data11 == " + postDatas.size());
                        Log.e(TAG, "interest11 == " + interestDatas.size());

                    } else {                      //普通帖子列表
                        postDatas = JSONUtil.jsonToArrayList(json, BBsListData550.class);
                        Log.e(TAG, "data222 == " + postDatas.size());
                    }

                    initViewData(isRefresh);
                } catch (Exception e) {
                    Log.e(TAG, "e == " + e.toString());
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 加载完成后的数据设置
     *
     * @param isRefresh
     */
    private void initViewData(boolean isRefresh) {
        if (mData.getId() == 1) {     //关注帖子列表

            if (focunsOrPost()) {                 //普通列表

                if (isRefresh && communityPostAdapter != null) {
                    communityPostAdapter.clearData();
                }

                initPostData();

                if (postDatas.size() < 20) {
                    mListView.setNoMore(true);
                } else {
                    mListView.loadMoreComplete();
                }

            } else {                            //关注列表

                if (isRefresh && mPostFocusAdapter != null) {
                    mPostFocusAdapter.clearData();
                }

                initFocusData();

                if (interestDatas.size() < 20) {
                    mListView.setNoMore(true);
                } else {
                    mListView.loadMoreComplete();
                }
            }

        } else {                      //普通帖子列表
            if (isRefresh && communityPostAdapter != null) {
                communityPostAdapter.clearData();
            }
            initPostData();

            if (postDatas.size() < 20) {
                mListView.setNoMore(true);
            } else {
                mListView.loadMoreComplete();
            }
        }
    }

    /**
     * 帖子列表初始化
     */
    private void initPostData() {
        if (communityPostAdapter == null) {

            communityPostAdapter = new CommunityPostAdapter(mContext, postDatas);
            mListView.setAdapter(communityPostAdapter);

            if (headerView != null) {
                mListView.removeHeaderView(headerView);
                headerView = null;
            }

            communityPostAdapter.setOnItemCallBackListener(new CommunityPostAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    if (pos != communityPostAdapter.getItemCount()) {
                        List<BBsListData550> bBsListData550s = communityPostAdapter.getmData();
                        String url = bBsListData550s.get(pos).getAppmurl();
                        if (url.length() > 0) {
                            if ("404".equals(communityPostAdapter.getmData().get(pos).getAskorshare())) {
                                mFunctionManager.showShort("该帖子已被删除");
                            } else {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBSHOMECLICK, "bbs|list_" + mData.getTongjiid() + "|" + (pos+1) + "|" + FinalConstant1.MARKET + "|" + FinalConstant1.DEVICE));
                                WebUrlTypeUtil.getInstance(getActivity()).urlToApp(url, "0", "0");
                            }
                        }
                    }
                }
            });
        } else {
            communityPostAdapter.addData(postDatas);
        }
    }

    /**
     * 关注列表初始化
     */
    @SuppressLint("SetTextI18n")
    private void initFocusData() {
        if (mPostFocusAdapter == null) {
            mPostFocusAdapter = new CommunityPostFocusAdapter(mContext, interestDatas);
            mListView.setAdapter(mPostFocusAdapter);

            if (headerView == null) {
                headerView = mInflater.inflate(R.layout.fragment_community_post_focus_header, null, false);
                mListView.addHeaderView(headerView);
            }
            mPostFocusAdapter.setOnItemCallBackListener(new CommunityPostFocusAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    List<MyFansData> fansDatas = mPostFocusAdapter.getmData();
                    if (pos != 0 && pos != fansDatas.size()) {
                        MyFansData myFansData = fansDatas.get(pos - 1);
                        Intent mIntent = new Intent();
                        switch (myFansData.getObj_type()) {
                            case "6":
                                mIntent.setClass(mContext, PersonCenterActivity641.class);
                                mIntent.putExtra("id", myFansData.getObj_id());
                                break;
                            case "1":
                                mIntent.setClass(mContext, DoctorDetailsActivity592.class);
                                mIntent.putExtra("docId", myFansData.getObj_id());
                                mIntent.putExtra("docName", myFansData.getName());
                                mIntent.putExtra("partId", "");
                                break;
                            case "3":
                                mIntent.setClass(mContext, HosDetailActivity.class);
                                mIntent.putExtra("hosid", myFansData.getObj_id());
                                break;
                            case "0":
                                mIntent.setClass(mContext, QuanziDetailActivity.class);
                                mIntent.putExtra("url_name", myFansData.getUrl());
                                break;
                        }
                        startActivityForResult(mIntent, 10);
                    }
                }
            });
        } else {
            mPostFocusAdapter.addData(interestDatas);
        }
    }

    /**
     * 判断是帖子列表还是关注列表
     *
     * @return ：true:帖子列表，flast:关注列表
     */
    private boolean focunsOrPost() {
        if (mData.getId() == 1) {     //关注帖子列表

            if (mPostFocusAdapter != null) {
                if (communityPostAdapter != null) {         //关注列表 ---> 帖子列表 或者是   帖子列表 --->关注列表

                    return postDatas.size() != 0 && interestDatas.size() == 0;

                } else {                                    //关注列表非第一次加载，可能会变为帖子列表
                    if (interestDatas.size() == 0 && postDatas.size() != 0) {
                        mPostFocusAdapter = null;
                        communityPostAdapter = null;
                        return true;

                    } else {

                        return false;
                    }
                }
            } else {
                if (communityPostAdapter != null) {         //帖子列表非第一次加载,可能会变为关注列表
                    if (postDatas.size() == 0 && interestDatas.size() != 0) {

                        mPostFocusAdapter = null;
                        communityPostAdapter = null;

                        return false;

                    } else {
                        return true;
                    }

                } else {                                    //第一次加载
                    return postDatas.size() != 0 && interestDatas.size() == 0;

                }
            }

        } else {
            return true;
        }
    }

}
