package com.module.community.controller.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 悦美达人
 * Created by dwb on 16/8/18.
 */
public class WebUrlTitleAndBtActivity extends BaseActivity {

    private final String TAG = "WebUrlTitleAndBtActivity";

    @BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
    private MyElasticScrollView scollwebView;
    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.wan_beautiful_web_back, click = true)
    private RelativeLayout back;// 返回
    @BindView(id = R.id.wan_beautifu_docname)
    private TextView titleBarTv;

    @BindView(id = R.id.tao_web_share_rly111, click = true)
    private RelativeLayout wantDaten;

    private WebView docDetWeb;

    private WebUrlTitleAndBtActivity mContex;

    public JSONObject obj_http;

    private String url;
    private String title;
    private BaseWebViewClientMessage baseWebViewClientMessage;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_web_title_bt);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = WebUrlTitleAndBtActivity.this;

        Intent it = getIntent();
        url = it.getStringExtra("link");
        title = it.getStringExtra("title");

        url = FinalConstant.baseUrl + FinalConstant.VER + url;

        titleBarTv.setText(title);
        scollwebView.GetLinearLayout(contentWeb);

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        initWebview();

        LodUrl1(url);
        scollwebView.setonRefreshListener(new MyElasticScrollView.OnRefreshListener1() {

            @Override
            public void onRefresh() {
                webReload();
            }
        });

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        WebUrlTitleAndBtActivity.this.finish();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public void initWebview() {
        docDetWeb = new WebView(mContex);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            docDetWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData(String str) {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();

        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.wan_beautiful_web_back:
                // finish();
                onBackPressed();
                break;
            case R.id.tao_web_share_rly111://达人帖
                    Intent it = new Intent();
                    String url = FinalConstant.baseUrl + FinalConstant.VER
                            + "/forum/postinfo/id/1265933/";
                    it.putExtra("url", url);
                    it.putExtra("qid", "1265933");
                    it.setClass(mContex, DiariesAndPostsActivity.class);
                    startActivity(it);
                break;
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}