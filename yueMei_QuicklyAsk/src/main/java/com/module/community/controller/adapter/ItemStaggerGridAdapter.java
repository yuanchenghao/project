package com.module.community.controller.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.SearchTaolistData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class ItemStaggerGridAdapter extends BaseQuickAdapter<SearchTaolistData, BaseViewHolder> {
    public ItemStaggerGridAdapter(int layoutResId, @Nullable List<SearchTaolistData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SearchTaolistData item) {
        Glide.with(mContext)
                .load(item.getDiaryImg())
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into((ImageView) helper.getView(R.id.staggered_gride_img));
        helper.setText(R.id.staggered_gride_title,item.getBilateral_title());

    }
}
