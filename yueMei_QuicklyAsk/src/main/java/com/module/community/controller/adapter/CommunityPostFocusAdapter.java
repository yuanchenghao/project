package com.module.community.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.my.model.bean.MyFansData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/7/16.
 */
public class CommunityPostFocusAdapter extends RecyclerView.Adapter<CommunityPostFocusAdapter.ViewHolder> {

    private Activity mContext;
    private LayoutInflater mInflater;
    private List<MyFansData> mFansDatas;
    private String TAG = "CommunityPostFocusAdapter";

    public CommunityPostFocusAdapter(Activity context, ArrayList<MyFansData> interestDatas) {
        this.mContext = context;
        this.mFansDatas = interestDatas;
        Log.e(TAG, "mFansDatas === " + mFansDatas.size());
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_my_focus, null);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        MyFansData fansData = mFansDatas.get(position);

        holder.ficusContainer.setPadding(Utils.dip2px(10), 0, Utils.dip2px(10), 0);

        Log.e(TAG, "fansData.getImg() === " + fansData.getImg());
        Glide.with(mContext).load(fansData.getImg()).transform(new GlideCircleTransform(mContext)).into(holder.ficusImg);

        holder.ficusName.setText(fansData.getName());
        holder.ficusDesc.setText(fansData.getDesc());

        switch (fansData.getV()) {
            case "0":          //无
                holder.ficusV.setVisibility(View.GONE);
                break;
            case "10":          //红色--官方
                holder.ficusV.setVisibility(View.VISIBLE);
                holder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.official_bottom));
                break;
            case "12":          //蓝色--认证
                holder.ficusV.setVisibility(View.VISIBLE);
                holder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.renzheng_bottom));
                break;
            case "13":          //蓝色--认证
                holder.ficusV.setVisibility(View.VISIBLE);
                holder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.renzheng_bottom));
                break;
            case "11":          //达人
                holder.ficusV.setVisibility(View.VISIBLE);
                holder.ficusV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.talent_bottom));
                break;
        }

        //关注样式
        final String eachFollowing = fansData.getEach_following();
        initFocus1(eachFollowing, holder);

        //关注点击事件
        holder.eachFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    initFocus2(eachFollowing, position, holder);
                }
            }
        });
    }

    private void initFocus1(String eachFollowing, ViewHolder holder) {
        switch (eachFollowing) {
            case "0":          //未关注
                setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注", holder);
                break;
            case "1":          //已关注
                setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.focus_plus_sign_yes, R.color._99, "已关注", holder);
                break;
            case "2":          //互相关注
                setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.each_focus, R.color._99, "互相关注", holder);
                break;
        }
    }

    private void initFocus2(String eachFollowing, int position, ViewHolder holder) {
        switch (eachFollowing) {
            case "0":          //未关注
                FocusAndCancel(position, holder);
                break;
            case "1":          //已关注
            case "2":          //互相关注
                showDialogExitEdit(position, holder);
                break;
        }
    }

    /**
     * 关注样式设置
     *
     * @param drawable1
     * @param drawable2
     * @param drawable3
     * @param text
     */
    private void setFocusView(int drawable1, int drawable2, int drawable3, String text, ViewHolder holder) {
        holder.eachFollowing.setBackground(ContextCompat.getDrawable(mContext, drawable1));
        holder.imgageFans.setBackground(ContextCompat.getDrawable(mContext, drawable2));
        holder.centerFans.setTextColor(ContextCompat.getColor(mContext, drawable3));
        holder.centerFans.setText(text);

        ViewGroup.LayoutParams layoutParams1 = holder.eachFollowing.getLayoutParams();
        switch (text.length()) {
            case 2:
                layoutParams1.width = Utils.dip2px(mContext, 68);
                break;
            case 3:
                layoutParams1.width = Utils.dip2px(mContext, 78);
                break;
            case 4:
                layoutParams1.width = Utils.dip2px(mContext, 90);
                break;
        }

        holder.eachFollowing.setLayoutParams(layoutParams1);
    }

    private void showDialogExitEdit(final int position, final ViewHolder holder) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel(position, holder);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    /**
     * 关注和取消关注
     *
     * @param position
     */
    private void FocusAndCancel(final int position, final ViewHolder holder) {
        HashMap<String, Object> hashMap = new HashMap<>();
        Log.e(TAG, "objid === " + mFansDatas.get(position).getObj_id());
        Log.e(TAG, "type === " + mFansDatas.get(position).getObj_type());
        hashMap.put("objid", mFansDatas.get(position).getObj_id());
        hashMap.put("type", mFansDatas.get(position).getObj_type());
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        Log.e(TAG, "focusAndCancelData.getIs_following() == " + focusAndCancelData.getIs_following());
                        switch (focusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注", holder);
                                break;
                            case "1":          //已关注
                                setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "已关注", holder);
                                break;
                            case "2":          //互相关注
                                setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "互相关注", holder);
                                break;
                        }
                        switch (focusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mFansDatas.get(position).setEach_following("0");
                                break;
                            case "1":          //已关注
                                mFansDatas.get(position).setEach_following("1");
                                break;
                            case "2":          //互相关注
                                mFansDatas.get(position).setEach_following("2");
                                break;
                        }
                        notifyDataSetChanged();
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mFansDatas.size();
    }

    public void clearData() {
        mFansDatas.clear();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ficusContainer;
        ImageView ficusImg;
        ImageView ficusV;
        TextView ficusName;
        TextView ficusDesc;
        LinearLayout eachFollowing;
        ImageView imgageFans;
        TextView centerFans;

        public ViewHolder(View itemView) {
            super(itemView);
            ficusContainer = itemView.findViewById(R.id.item_ficus_container);
            ficusImg = itemView.findViewById(R.id.item_ficus_img);
            ficusV = itemView.findViewById(R.id.item_ficus_v);
            ficusName = itemView.findViewById(R.id.item_ficus_name);
            ficusDesc = itemView.findViewById(R.id.item_ficus_desc);
            eachFollowing = itemView.findViewById(R.id.ficus_each_following);
            imgageFans = itemView.findViewById(R.id.following_imgage_fans);
            centerFans = itemView.findViewById(R.id.following_center_fans);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public List<MyFansData> getmData() {
        return mFansDatas;
    }

    public void addData(ArrayList<MyFansData> data) {
        mFansDatas.addAll(data);
        notifyDataSetChanged();
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}
