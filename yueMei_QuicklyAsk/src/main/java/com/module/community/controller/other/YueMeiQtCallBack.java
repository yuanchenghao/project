package com.module.community.controller.other;

import android.util.Log;

import com.sijla.callback.QtCallBack;

import org.json.JSONObject;

/**
 * Created by 裴成浩 on 2018/11/15.
 */
public class YueMeiQtCallBack implements QtCallBack {
    private static final String TAG = "YueMeiQtCallBack";

    @Override
    public void uploadCallBack(JSONObject jsonObject) {
        Log.e(TAG, "jsonObject == " + jsonObject.toString());
    }
}
