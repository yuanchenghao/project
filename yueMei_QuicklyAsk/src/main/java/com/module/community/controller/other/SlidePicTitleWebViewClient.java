package com.module.community.controller.other;

import android.content.Intent;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by 裴成浩 on 2018/1/22.
 */

public class SlidePicTitleWebViewClient implements BaseWebViewClientCallback {

    private final SlidePicTitieWebActivity mActivity;
    private final Intent intent;
    private String uid;
    private String TAG = "SlidePicTitleWebViewClient";

    public SlidePicTitleWebViewClient(SlidePicTitieWebActivity mActivity) {
        this.mActivity = mActivity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }
    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页
                String id = obj.getString("id");
                String docname = URLDecoder.decode(
                        obj.getString("docname"), "utf-8");

                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                intent.putExtra("docId", id);
                intent.putExtra("docName", docname);
                intent.putExtra("partId", "");
                mActivity.startActivity(intent);
                break;

            case "6":
                String link = obj.getString("link");
                String qid = obj.getString("id");
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                intent.putExtra("url", link);
                intent.putExtra("qid", qid);
                mActivity.startActivity(intent);
                break;
        }
    }
}
