package com.module.community.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.controller.other.SlidePicTitleWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 主页 头部 跑马灯图片的链接
 *
 * @author Rubin
 */
public class SlidePicTitieWebActivity extends BaseActivity {

    private final String TAG = "SlidePicTitieWebActivity";

    //	@BindView(id = R.id.slide_pic_web_det_scrollview1, click = true)
//	private MyElasticScrollView scollwebView;
    @BindView(id = R.id.slide_pic_linearlayout, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.wan_beautiful_web_back, click = true)
    private RelativeLayout back;// 返回
    @BindView(id = R.id.wan_beautifu_docname)
    private TextView titleBarTv;
    @BindView(id = R.id.tao_web_share_rly111, click = true)
    private RelativeLayout shareBt;// 分享

    private WebView bbsDetWeb;

    private SlidePicTitieWebActivity mContex;

    private String url;

    public Handler handler = new Handler();

    public JSONObject obj_http;

    @BindView(id = R.id.wuurl_ly)
    private RelativeLayout noUrlLy;
    @BindView(id = R.id.sao_wenzi_tv)
    private TextView noUrlTv;

    // 分享
    private String shareUrl = "";
    private String shareContent = "";
    private String shareTitle;
    private String sharePic;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private boolean login = true;

    @Override
    public void setRootView() {
        setContentView(R.layout.web_acty_slidepic);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = SlidePicTitieWebActivity.this;

        login = Utils.isLogin();

        Intent it0 = getIntent();
        url = it0.getStringExtra("url");
        sharePic = it0.getStringExtra("sharePic");
        shareTitle = it0.getStringExtra("shareTitle");

        if (shareTitle.equals("0")) {
            titleBarTv.setText("悦美");
        } else {
            titleBarTv.setText(shareTitle);
        }

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex, "7");
        baseWebViewClientMessage.setBaseWebViewClientCallback(new SlidePicTitleWebViewClient(mContex));

        initWebview();

        if (url.startsWith("http")) {
            LodUrl(url);
        } else {
            noUrlLy.setVisibility(View.VISIBLE);
            noUrlTv.setText(url);
        }
    }


    @SuppressWarnings("deprecation")
    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    public void initWebview() {
        bbsDetWeb = new WebView(mContex);
        bbsDetWeb.getSettings().setJavaScriptEnabled(true);
        bbsDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        bbsDetWeb.getSettings().setUseWideViewPort(true);
        bbsDetWeb.getSettings().supportMultipleWindows();
        bbsDetWeb.getSettings().setNeedInitialFocus(true);
        bbsDetWeb.getSettings().setAllowFileAccess(true);

        bbsDetWeb.setWebViewClient(baseWebViewClientMessage);
        bbsDetWeb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        contentWeb.addView(bbsDetWeb);
    }

    /**
     * 加载web
     */
    public void LodUrl(String url) {
        baseWebViewClientMessage.startLoading();

        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        bbsDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //如果之前是未登录的，现在是登录的
        if (!login && Utils.isLogin()) {
            if (url.startsWith("http")) {
                LodUrl(url);
            } else {
                noUrlLy.setVisibility(View.VISIBLE);
                noUrlTv.setText(url);
            }
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.wan_beautiful_web_back:
                onBackPressed();
                // finish();
                break;
            case R.id.tao_web_share_rly111:// 分享按钮

                if (null != sharePic && sharePic.length() > 0) {
                    setShare();
                }
                break;
        }
    }

    /**
     * 设置分享
     */
    private void setShare() {
        BaseShareView baseShareView = new BaseShareView(mContex);
        baseShareView.setShareContent(shareTitle).ShareAction();

        baseShareView.getShareBoardlistener().setSinaText(shareTitle + shareUrl + "分享自@悦美整形APP").setSinaThumb(new UMImage(mContex, sharePic)).setSmsText(shareTitle + "，" + shareUrl).setTencentUrl(shareUrl).setTencentTitle(shareTitle).setTencentThumb(new UMImage(mContex, R.drawable.ic_launcher)).setTencentDescription(shareContent).setTencentText(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {

                if (!platform.equals(SHARE_MEDIA.SMS)) {
                    Toast.makeText(mContex, " 分享成功啦", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
