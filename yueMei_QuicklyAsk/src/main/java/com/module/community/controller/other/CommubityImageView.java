package com.module.community.controller.other;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by 裴成浩 on 2018/9/18.
 */
public class CommubityImageView extends AppCompatImageView {
    private float lastX;
    private float lastY;

    public CommubityImageView(Context context) {
        this(context, null);
    }

    public CommubityImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommubityImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = x;
                lastY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                float offsetX = x - lastX;
                float offsetY = y - lastY;
                layout((int) (getLeft() + offsetX),(int)(getTop() + offsetY),(int)(getRight() + offsetX),(int)(getBottom() + offsetY));
                break;
        }

        return super.onTouchEvent(event);
    }
}
