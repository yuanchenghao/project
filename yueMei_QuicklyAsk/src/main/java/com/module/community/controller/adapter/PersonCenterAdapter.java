package com.module.community.controller.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.community.model.bean.BBsListData550;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * 个人中心也适配器
 * Created by 裴成浩 on 2018/5/3.
 */
public class PersonCenterAdapter extends RecyclerView.Adapter<PersonCenterAdapter.ViewHolder> {

    private final Context mContext;
    private final List<BBsListData550> bBsListData550s;
    private final LayoutInflater mInflater;

    public PersonCenterAdapter(Context context, List<BBsListData550> bBsListData550s) {
        this.mContext = context;
        this.bBsListData550s = bBsListData550s;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_ification_class, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
         bBsListData550s.get(position);
//        holder.tvTitle.setText(s);
    }

    @Override
    public int getItemCount() {
        return bBsListData550s.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView tvTitle;

        public ViewHolder(View itemView) {
            super (itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
}
