/**
 *
 */
package com.module.community.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.community.model.bean.BBsListData550;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.ViewInject;

import java.util.List;

/**
 * 社区帖子列表
 *
 * @author Robin
 */
public class BBsListAdapter extends BaseAdapter {

    private final String TAG = "BBsListAdapter";

    private List<BBsListData550> mHotIssues;
    private Context mContext;
    private LayoutInflater inflater;
    private BBsListData550 hotIsData;
    ViewHolder viewHolder;

    private Animation ain;

    private int windowsWight;

    public BBsListAdapter(Context mContext, List<BBsListData550> mHotIssues) {
        this.mContext = mContext;
        this.mHotIssues = mHotIssues;
        Log.e(TAG, "mHotIssues个数是 == " + mHotIssues.size());
        inflater = LayoutInflater.from(mContext);

        ain = AnimationUtils.loadAnimation(mContext, R.anim.push_in);
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);

    }
    static class ViewHolder {
        public ImageView mBBsHeadPic;

        public TextView mBBsName;
        public TextView mBBsTitle;

        public LinearLayout mSingalLy;
        public ImageView mSingalPic;
        public ImageView mSingalPic_;

        public LinearLayout mDuozhangLy;
        public ImageView mPic[] = new ImageView[9];

        public LinearLayout mTagLy;
        public TextView mTag;

        public TextView mPrice;
        public TextView mTime;
        public TextView mCommentNum;

        public ImageView mYuemeiIv;
        public ImageView mDocRenIv;

        public LinearLayout mTu2;
        public LinearLayout mTu3;
        public LinearLayout tagLy;

        public LinearLayout mCommentLy;

        public LinearLayout mSeeLy;

        public TextView mSeenum;
        public ImageView mJingIv;

        public LinearLayout mFxTipsLy;

        public RelativeLayout mPicLy;
        public RelativeLayout mPicLy_;


        public ImageView mTagVideo;
        public ImageView mTagVideo_;


        public int flag;
    }

    @Override
    public int getCount() {
        return mHotIssues.size();
    }

    @Override
    public Object getItem(int position) {
        return mHotIssues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"NewApi", "InlinedApi", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.e(TAG, "position == " +position);
        hotIsData = mHotIssues.get(position);

        if (convertView == null || ((ViewHolder) convertView.getTag()).flag != position) {

            convertView = inflater.inflate(R.layout.item_bbs_550, null);

            viewHolder = new ViewHolder();
            viewHolder.flag = position;

            viewHolder.mBBsHeadPic = convertView.findViewById(R.id.bbs_list_head_image_iv);
            viewHolder.mBBsName = convertView.findViewById(R.id.bbs_list_name_tv);
            viewHolder.mBBsTitle = convertView.findViewById(R.id.bbs_list_title_tv);

            viewHolder.mDuozhangLy = convertView.findViewById(R.id.bbs_list_duozhang_ly);
            viewHolder.mPic[0] = convertView.findViewById(R.id.bbs_list_pic1);
            viewHolder.mPic[1] = convertView.findViewById(R.id.bbs_list_pic2);
            viewHolder.mPic[2] = convertView.findViewById(R.id.bbs_list_pic3);
            viewHolder.mPic[3] = convertView.findViewById(R.id.bbs_list_pic4);
            viewHolder.mPic[4] = convertView.findViewById(R.id.bbs_list_pic5);
            viewHolder.mPic[5] = convertView.findViewById(R.id.bbs_list_pic6);
            viewHolder.mPic[6] = convertView.findViewById(R.id.bbs_list_pic7);
            viewHolder.mPic[7] = convertView.findViewById(R.id.bbs_list_pic8);
            viewHolder.mPic[8] = convertView.findViewById(R.id.bbs_list_pic9);

            viewHolder.mSingalLy = convertView.findViewById(R.id.bbs_list_pic_danzhang_ly);
            viewHolder.mSingalPic = convertView.findViewById(R.id.bbs_list_pic_danzhang);
            viewHolder.mSingalPic_ = convertView.findViewById(R.id.bbs_list_pic_danzhang_);

            viewHolder.mTagLy = convertView.findViewById(R.id.bbs_list_tag_ly);
            viewHolder.mTag = convertView.findViewById(R.id.bbs_list_tag_tv);

            viewHolder.mPrice = convertView.findViewById(R.id.bbs_list_price_tv);
            viewHolder.mTime = convertView.findViewById(R.id.bbs_list_time_tv);
            viewHolder.mCommentNum = convertView.findViewById(R.id.bbs_list_comment_num_tv);

            viewHolder.mYuemeiIv = convertView.findViewById(R.id.bbs_list_yuemei_daren_iv);

            viewHolder.mTu2 = convertView.findViewById(R.id.bbs_list_h_ly2);
            viewHolder.mTu3 = convertView.findViewById(R.id.bbs_list_h_ly3);

            viewHolder.tagLy = convertView.findViewById(R.id.tag_ly_550);

            viewHolder.mCommentLy = convertView.findViewById(R.id.bbs_list_comment_ly);

            viewHolder.mSeenum = convertView.findViewById(R.id.bbs_list_see_num_tv);
            viewHolder.mJingIv = convertView.findViewById(R.id.bbs_list_jinghua_iv);
            viewHolder.mDocRenIv = convertView.findViewById(R.id.bbs_list_doc_daren_iv);

            viewHolder.mFxTipsLy = convertView.findViewById(R.id.note_fanxian_tips_ly);

            viewHolder.mSeeLy = convertView.findViewById(R.id.bbs_list_see_ly);


            viewHolder.mPicLy = convertView.findViewById(R.id.bbs_list_danzhang);
            viewHolder.mPicLy_ = convertView.findViewById(R.id.bbs_list_danzhang_);

            viewHolder.mTagVideo = convertView.findViewById(R.id.bbs_pic_tag);
            viewHolder.mTagVideo_ = convertView.findViewById(R.id.bbs_pic_tag_);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        String userImg = hotIsData.getUser_img();
        Log.e(TAG, "userImg == " + userImg);
        if (userImg != null && !"".equals(userImg)) {
            Glide.with(mContext).load(hotIsData.getUser_img()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).bitmapTransform(new GlideCircleTransform(mContext)).into(viewHolder.mBBsHeadPic);

        }

        viewHolder.mBBsName.setText(hotIsData.getUser_name());
        viewHolder.mBBsTitle.setText(hotIsData.getTitle());

        viewHolder.mTime.setText(hotIsData.getTime());

        String isFx = hotIsData.getIs_fanxian();
        if (null != isFx && isFx.equals("1")) {
            viewHolder.mFxTipsLy.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mFxTipsLy.setVisibility(View.GONE);

        }

        if (null != hotIsData.getAnswer_num() && !hotIsData.getAnswer_num().equals("0")) {
            viewHolder.mCommentLy.setVisibility(View.VISIBLE);
            viewHolder.mCommentNum.setText(hotIsData.getAnswer_num());
        } else {
            viewHolder.mCommentLy.setVisibility(View.INVISIBLE);
        }

        if (null != hotIsData.getTalent() && !hotIsData.getTalent().equals("0")) {
            viewHolder.mYuemeiIv.setVisibility(View.VISIBLE);
            if ("10".equals(hotIsData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = viewHolder.mYuemeiIv.getLayoutParams();
                layoutParams.width = Utils.dip2px(46);
                layoutParams.height = Utils.dip2px(16);
                viewHolder.mYuemeiIv.setLayoutParams(layoutParams);
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.yuemei_officia_listl);
            } else if ("11".equals(hotIsData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = viewHolder.mYuemeiIv.getLayoutParams();
                layoutParams.width = Utils.dip2px(15);
                layoutParams.height = Utils.dip2px(15);
                viewHolder.mYuemeiIv.setLayoutParams(layoutParams);
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if ("12".equals(hotIsData.getTalent()) || "13".equals(hotIsData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = viewHolder.mYuemeiIv.getLayoutParams();
                layoutParams.width = Utils.dip2px(46);
                layoutParams.height = Utils.dip2px(16);
                viewHolder.mYuemeiIv.setLayoutParams(layoutParams);
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.renzheng_list);
            }

        } else {
            viewHolder.mYuemeiIv.setVisibility(View.GONE);
        }

        if (null != hotIsData.getView_num() && !hotIsData.getView_num().equals("0")) {
            viewHolder.mSeenum.setText(hotIsData.getView_num());
            viewHolder.mSeeLy.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mSeeLy.setVisibility(View.INVISIBLE);
        }

        if (null != hotIsData.getSet_tid()) {
            if (hotIsData.getSet_tid().equals("1")) {
                viewHolder.mJingIv.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mJingIv.setVisibility(View.GONE);
            }
        }

        // 标签
        if (null != hotIsData.getTag()) {
            if (hotIsData.getTag().size() > 0) {
                viewHolder.tagLy.setVisibility(View.VISIBLE);
                viewHolder.mTagLy.setVisibility(View.VISIBLE);
                int tagsize = hotIsData.getTag().size();
                String tagStr = "";
                for (int i = 0; i < tagsize; i++) {
                    tagStr = hotIsData.getTag().get(i).getName() + "  " + tagStr;
                }
                viewHolder.mTag.setText(tagStr);
            } else {
                viewHolder.mTagLy.setVisibility(View.GONE);
                viewHolder.tagLy.setVisibility(View.GONE);
            }
        } else {
            viewHolder.mTagLy.setVisibility(View.GONE);
            viewHolder.tagLy.setVisibility(View.GONE);
        }
        // 标签


        if (null != hotIsData.getPrice() && !hotIsData.getPrice().equals("0") && hotIsData.getPrice().length() > 0) {
            viewHolder.tagLy.setVisibility(View.VISIBLE);
            viewHolder.mPrice.setText("（￥" + hotIsData.getPrice() + "）");
        } else {
            viewHolder.mPrice.setVisibility(View.GONE);

            if (null != hotIsData.getTag()) {
                if (hotIsData.getTag().size() > 0) {
                    viewHolder.tagLy.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tagLy.setVisibility(View.GONE);
                }
            } else {
                viewHolder.tagLy.setVisibility(View.GONE);
            }

        }

        // 图片

        if (null != hotIsData.getPic()) {

            if (hotIsData.getPic().size() > 0) {
                if (hotIsData.getPic().size() == 1) {
                    viewHolder.mDuozhangLy.setVisibility(View.GONE);
                    viewHolder.mSingalLy.setVisibility(View.VISIBLE);

                    String ww = hotIsData.getPic().get(0).getWidth();
                    String hh = hotIsData.getPic().get(0).getHeight();

                    if (null != ww && null != hh && !ww.equals(hh)) {

                        int www = Integer.parseInt(ww);
                        int hhh = Integer.parseInt(hh);

                        viewHolder.mPicLy.setVisibility(View.GONE);
                        viewHolder.mPicLy_.setVisibility(View.VISIBLE);

                        ViewGroup.LayoutParams params = viewHolder.mSingalPic_.getLayoutParams();
                        params.height = ((windowsWight - 75) * hhh / www);
                        viewHolder.mSingalPic_.setLayoutParams(params);

                        try {

                            Glide.with(mContext).load(hotIsData.getPic().get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mSingalPic_);

                        } catch (OutOfMemoryError e) {
                            ViewInject.toast("内存不足");
                        }

                    } else {

                        viewHolder.mPicLy.setVisibility(View.VISIBLE);
                        viewHolder.mPicLy_.setVisibility(View.GONE);
                        try {
                            Glide.with(mContext).load(hotIsData.getPic().get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mSingalPic);

                        } catch (OutOfMemoryError e) {
                            ViewInject.toast("内存不足");
                        }

                    }

                    //判断有没有视频
                    if ("1".equals(hotIsData.getIs_video())) {
                        viewHolder.mTagVideo.setVisibility(View.VISIBLE);
                        viewHolder.mTagVideo_.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mTagVideo.setVisibility(View.GONE);
                        viewHolder.mTagVideo_.setVisibility(View.GONE);
                    }


                } else if (hotIsData.getPic().size() > 1) {

                    viewHolder.mDuozhangLy.setVisibility(View.VISIBLE);
                    viewHolder.mSingalLy.setVisibility(View.GONE);
                    int sizeI = hotIsData.getPic().size();

                    if (sizeI >= 9) {
                        sizeI = 9;
                    }

                    try {
                        for (int i = 0; i < sizeI; i++) {
                            viewHolder.mPic[i].setVisibility(View.VISIBLE);
                            Log.e(TAG, "hotIsData.getPic().get(i).getImg() === " + hotIsData.getPic().get(i).getImg());
                            Glide.with(mContext).load(hotIsData.getPic().get(i).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mPic[i]);
                        }
                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }

                    int sizeT = 9 - sizeI;
                    if (sizeT > 0) {
                        for (int i = 0; i < sizeT; i++) {
                            viewHolder.mPic[8 - i].setVisibility(View.GONE);
                        }
                    }
                }
            } else {
                viewHolder.mDuozhangLy.setVisibility(View.GONE);
                viewHolder.mSingalLy.setVisibility(View.GONE);
            }
            // 图片
            if (hotIsData.getPic().size() > 6) {
                viewHolder.mTu2.setVisibility(View.VISIBLE);
                viewHolder.mTu3.setVisibility(View.VISIBLE);
            } else if (3 < hotIsData.getPic().size() && hotIsData.getPic().size() <= 6) {
                viewHolder.mTu2.setVisibility(View.VISIBLE);
                viewHolder.mTu3.setVisibility(View.GONE);
            } else {
                viewHolder.mTu2.setVisibility(View.GONE);
                viewHolder.mTu3.setVisibility(View.GONE);
            }
        } else {
            viewHolder.mTu2.setVisibility(View.GONE);
            viewHolder.mTu3.setVisibility(View.GONE);
            viewHolder.mSingalLy.setVisibility(View.GONE);
        }
        return convertView;
    }

    public void add(List<BBsListData550> infos) {
        mHotIssues.addAll(infos);
    }

    public List<BBsListData550> getmHotIssues() {
        return mHotIssues;
    }

}