package com.module.other.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.module.OpenClickActivity;
import com.quicklyask.util.Utils;

import cn.jpush.android.api.JPushInterface;

public class JiGuangPushReceiver extends BroadcastReceiver {
    public static final String TAG="JiGuangPushReceiver";
    @Override
    public void onReceive(final Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.d(TAG, "onReceive - " + intent.getAction());

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
            Log.d(TAG, "[MyReceiver] 接收Registration Id : " + regId);
        }else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            Log.d(TAG, "收到了自定义消息。消息内容是：" + bundle.getString(JPushInterface.EXTRA_MESSAGE));
            // 自定义消息不会展示在通知栏，完全要开发者写代码去处理
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            Log.d(TAG, "收到了通知");
            // 在这里可以做些统计，或者做些其他工作
            //{"link":"https:\/\/m.yuemei.com\/tao_zt\/4894.html?u=9100078","type":"100000"}
            String string = bundle.getString(JPushInterface.EXTRA_EXTRA);
            Log.d(TAG,"extra==="+string);
//                BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(activity);
//                baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
//                    @Override
//                    public void otherJump(String urlStr) throws Exception {
//                        JSONObject jsonObject = new JSONObject(urlStr);
//                        String link = jsonObject.optString("link");
//                            WebUrlTypeUtil.getInstance(context).urlToApp(link, "0", "0");
//                        context.startActivity(new Intent());
//                    }
//                });
//                baseWebViewClientMessage.showWebDetail(string);

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            Log.d(TAG, "用户点击打开了通知");
            // 在这里可以自己写代码去定义用户点击后的行为
            String string = bundle.getString(JPushInterface.EXTRA_EXTRA);
            Log.d(TAG,"extra==="+string);
            Intent i = new Intent(context, OpenClickActivity.class);  //自定义打开的界面
            i.putExtra("extra",string);
            if (Utils.isAppRunning(context,"com.quicklyask.activity")){ //app存活
               i.putExtra("flag","1");
            }else {
                i.putExtra("flag","0");
            }
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } else {
            Log.d(TAG, "Unhandled intent - " + intent.getAction());
        }
    }
}
