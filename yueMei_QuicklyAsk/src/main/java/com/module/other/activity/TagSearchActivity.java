package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.api.LodProjectHotApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.home.controller.adapter.SearchBaikeAdapter;
import com.module.home.model.bean.BaikeSearchItemData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 标签搜索
 *
 * @author 裴成浩
 */
public class TagSearchActivity extends YMBaseActivity {

    @BindView(R.id.tag_search_top)
    CommonTopBar mTop;
    @BindView(R.id.input_edit)
    EditText inputEt;
    @BindView(R.id.search_cancel_or_rly)
    RelativeLayout cancelOrSearchBt;
    @BindView(R.id.search_cancel_or_tv)
    TextView cancleOrsearchTv;
    @BindView(R.id.ivDeleteText)
    ImageView searchIv;
    @BindView(R.id.pop_list_tao_project_list)
    ListView mlist;
    @BindView(R.id.my_collect_post_tv_nodata)
    LinearLayout nodataTv;

    private List<BaikeSearchItemData> lvGroupData = new ArrayList<>();

    private String ifCancel = "1";

    private Handler mHandler;
    private SearchBaikeAdapter mPart2Adapter;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_tag_search;
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
        setonListner();
    }

    private void setonListner() {
        inputEt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String inputStr = inputEt.getText().toString().trim();
                if (inputStr.length() > 0) {
                    inputEt.setCursorVisible(true);
                    cancleOrsearchTv.setText("搜索");
                    ifCancel = "0";
                } else {
                    inputEt.setCursorVisible(true);
                    cancleOrsearchTv.setText("取消");
                    ifCancel = "1";
                }
            }
        });

        inputEt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    if (inputEt.hasFocus()) {
                        cancleOrsearchTv.setText("取消");
                        ifCancel = "1";
                    }
                    searchIv.setVisibility(View.GONE);
                } else {
                    if (inputEt.hasFocus()) {
                        cancleOrsearchTv.setText("搜索");
                        ifCancel = "0";
                    }
                    searchIv.setVisibility(View.VISIBLE);

                    // 监听搜索键的行为
                    String key = inputEt.getText().toString().trim();
                    // 搜搜哦
                    lodBaikeSearchItemData(key);
                    mHandler = getHandler();
                    inputEt.clearFocus();
                    inputEt.setCursorVisible(false);
                }

            }
        });

        searchIv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                inputEt.setText("");
                cancleOrsearchTv.setText("取消");
                ifCancel = "1";
                inputEt.setCursorVisible(true);
            }
        });

        cancelOrSearchBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (ifCancel.equals("1")) {
                    finish();
                } else {
                    // 先隐藏键盘
                    Utils.hideSoftKeyboard(TagSearchActivity.this);

                    // 监听搜索键的行为
                    String key = inputEt.getText().toString().trim();
                    // 搜搜哦
                    lodBaikeSearchItemData(key);
                    mHandler = getHandler();
                    inputEt.clearFocus();
                    inputEt.setCursorVisible(false);
                    cancleOrsearchTv.setText("取消");
                    ifCancel = "1";
                }
            }
        });

        mlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                String id = lvGroupData.get(pos).get_id();
                String name = lvGroupData.get(pos).getName();

                Intent intent = new Intent();
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                setResult(1, intent);
                finish();
            }

        });
    }

    void lodBaikeSearchItemData(final String keyStr) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put("keyword", keyStr);
                maps.put("flag", "1");
                new LodProjectHotApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            lvGroupData = JSONUtil.jsonToArrayList(serverData.data, BaikeSearchItemData.class);
                            Message message = mHandler.obtainMessage(1);
                            message.sendToTarget();
                        }
                    }
                });
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint("NewApi")
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (lvGroupData != null && lvGroupData.size() > 0) {
                            mTop.setCenterText("请选择项目日记类型");
                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);
                            mPart2Adapter = new SearchBaikeAdapter(mContext,
                                    lvGroupData);
                            mlist.setAdapter(mPart2Adapter);
                        } else {
                            mTop.setCenterText("日记项目类型");
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                }

            }
        };
    }
}
