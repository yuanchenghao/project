/**
 * 
 */
package com.module.other.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.adapter.ProjectFourTreeAdapter;
import com.module.commonview.module.bean.ProjectFourTree;
import com.module.other.module.api.LoadFourTreeDataApI;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.ProjectFourTreeItemSelectListener;
import com.quicklyask.entity.ProjectTwoTree;
import com.quicklyask.util.JSONUtil;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目详情四级列表
 * 
 * @author Robin
 * 
 */
public class ProjectFourTreeListFragment extends ListFragment {

	public static final String TAG = "ProjectFourTreeListFragment";
	private String pid;
	private String _id;
	private String oneid;
	private int twoPos;
	private List<ProjectFourTree> lvfourTreeData = new ArrayList<ProjectFourTree>();
	private ListView mlist;
	private ProjectFourTreeAdapter fourTreeAdapter;
	private ProjectFourTree itemFourtree = new ProjectFourTree();
	public static int mmPosition = -1;

	// 提供给外的接口
	private ProjectFourTreeItemSelectListener menuViewOnSelectListener;

	private static ProjectFourTreeListFragment instance = null;

	// 单例模式
	public static ProjectFourTreeListFragment getInstance() {
		if (instance == null) {
			instance = new ProjectFourTreeListFragment();
		}
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.ffragment_list, null);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
		super.onActivityCreated(savedInstanceState);
		mlist = getListView();

		if(isAdded()) {
			twoPos = getArguments().getInt("pos");
			oneid = getArguments().getString("oneid");
			// pid = getArguments().getString("id");
			_id = getArguments().getString("_id");
		}

		loadFourTreeData();
	}

	public void setCascadingMenuViewOnSelectListener(
			ProjectFourTreeItemSelectListener onSelectListener) {
		menuViewOnSelectListener = onSelectListener;
	}

	void loadFourTreeData() {
		if (SystemTool.checkNet(getActivity())) {
			Map<String,Object> maps=new HashMap<>();
			maps.put("id",oneid);
			new LoadFourTreeDataApI().getCallBack(getActivity(), maps, new BaseCallBackListener<ServerData>() {
				@Override
				public void onSuccess(ServerData serverData) {
					if ("1".equals(serverData.code)){
						List<ProjectTwoTree> projectTwoTrees = JSONUtil.TransformProjectTree(serverData.data);

						lvfourTreeData = projectTwoTrees.get(twoPos).getList();

						String idd = "-2";

						for (int i = 0; i < lvfourTreeData.size(); i++) {

							idd = lvfourTreeData.get(i).get_id();

							if (_id.equals(idd)) {
								mmPosition = i;
								break;
							} else {
								mmPosition = -1;
							}
						}

						if (null != lvfourTreeData
								&& lvfourTreeData.size() > 0) {

							if (null != getActivity()) {
								fourTreeAdapter = new ProjectFourTreeAdapter(
										getActivity(), lvfourTreeData);

								mlist.setAdapter(fourTreeAdapter);
							}

							mlist.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(
										AdapterView<?> arg0, View arg1,
										int pos, long arg3) {

									mmPosition = pos;
									fourTreeAdapter = new ProjectFourTreeAdapter(
											getActivity(),
											lvfourTreeData);
									mlist.setAdapter(fourTreeAdapter);

									itemFourtree = lvfourTreeData.get(pos);
									if (menuViewOnSelectListener != null) {
										menuViewOnSelectListener.getValue(itemFourtree);
									}
								}

							});
						}
					}
				}
			});

		} else {
			ViewInject.toast("请检查您的网络");
		}



	}



	public void onResume() {
		super.onResume();
		StatService.onResume(getActivity());
		MobclickAgent.onPageStart("MainScreen"); // 统计页面
	}

	public void onPause() {
		super.onPause();
		StatService.onPause(getActivity());
		MobclickAgent.onPageEnd("MainScreen");
	}

	@Override
	public void onDetach() {
		super.onDetach();

		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
