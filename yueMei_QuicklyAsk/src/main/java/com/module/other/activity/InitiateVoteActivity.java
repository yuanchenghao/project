package com.module.other.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.event.VoteMsgEvent;
import com.module.my.controller.activity.WriteSuibianLiaoActivity647;
import com.module.other.adapter.InitiateVoteAdapter;
import com.module.other.fragment.InitiateVoteFragment;
import com.module.other.module.bean.SearchTaoDate;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 文 件 名: ProjectContrastActivity
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-18 19:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：发起投票页面
 */

public class InitiateVoteActivity extends YMBaseActivity {
    @BindView(R.id.topBar)
    CommonTopBar topBar;
    @BindView(R.id.ed_title)
    EditText edTitle;
    @BindView(R.id.tv_edit_num)
    TextView tvEditNum;
    @BindView(R.id.switch_if_multiple)
    Switch switchIfMultiple;
    @BindView(R.id.tl)
    CommonTabLayout tl;
    @BindView(R.id.fl_change)
    FrameLayout flChange;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.tv_vote)
    TextView tvVote;
    private String mFrom;
    private List<SearchTaoDate.TaoListBean> tao_list = new ArrayList<>();
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private InitiateVoteAdapter initiateVoteAdapter;
    private ArrayList<Fragment> mFragments;
    private String[] mTitles = {"购物车", "收藏", "足迹"};
    //提示框
    private YueMeiDialog yueMeiDialog;
    private DeleteDialogClickListener deleteDialogClickListener;
    public static ArrayList<String> selectList1;
    public static ArrayList<SearchTaoDate.TaoListBean> selectList2;
    public boolean isMultiple = false;
    //限制的最大字数
    public int num = 20;
    private String mVoteTitle;
    private String mIsMultiple;
    private ArrayList<SearchTaoDate.TaoListBean> rvList;


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if(yueMeiDialog != null){
            yueMeiDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        upUiSwitch();
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(VoteMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 1:
                selectList1 = (ArrayList<String>) msgEvent.getData();
                break;
            case 2:
                selectList2 = (ArrayList<SearchTaoDate.TaoListBean>) msgEvent.getData();
                tao_list = selectList2;
                initiateVoteAdapter.updata(selectList2);
                upUiSwitch();
                break;
//            case 5:
//                //添加
//                Intent intent = new Intent(InitiateVoteActivity.this, SearchProjectActivity.class);
//                intent.putExtra("from", "InitiateVoteActivity");
//                intent.putStringArrayListExtra("taoIdList", selectList1);
//                intent.putExtra("taoData", selectList2);
//                startActivityForResult(intent, 1000);
//                break;
            default:
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_initiate_vote;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) topBar.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        mFrom = getIntent().getStringExtra("from");

        //回传回来的值
        mVoteTitle = getIntent().getStringExtra("voteTitle");
        mIsMultiple = getIntent().getStringExtra("isMultiple");
        rvList = getIntent().getParcelableArrayListExtra("votedata");
        if (!TextUtils.isEmpty(mVoteTitle)) {
            edTitle.setText(mVoteTitle);
            tvEditNum.setText(mVoteTitle.length() + "/" + 20);
        }
        if (!TextUtils.isEmpty(mIsMultiple) && mIsMultiple.equals("2")) {
            //多选
            switchIfMultiple.setChecked(true);
            isMultiple = true;
        } else {
            //单选
            switchIfMultiple.setChecked(false);
            isMultiple = false;
        }

        selectList1 = new ArrayList<>();
        selectList2 = new ArrayList<>();

        if (rvList != null) {
            tao_list = rvList;
            selectList2 = rvList;
            for (int i = 0; i < rvList.size(); i++) {
                selectList1.add(rvList.get(i).getId().trim());
            }
        }


        yueMeiDialog = new YueMeiDialog(mContext, "差一步即可发起投票\n确认退出么？", "确定返回", "继续发起");
        deleteDialogClickListener = new DeleteDialogClickListener();
        yueMeiDialog.setBtnClickListener(deleteDialogClickListener);

        topBar.getIv_left().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //返回
                if (mFrom.equals("PostingAndNoteFragment")) {
                    Intent i = new Intent();
                    setResult(1003, i);
                    finish();
                } else {
                    yueMeiDialog.show();
                }
            }
        });
        topBar.getTv_right().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edTitle.getText().toString().trim())) {
                    MyToast.makeTextToast2(mContext, "请填写标题", 1000).show();
                    return;
                }
                if (tao_list != null && tao_list.size() < 2) {
                    MyToast.makeTextToast2(mContext, "最少输入两个投票项目", 1000).show();
                    return;
                } else {
                    if (!mFrom.equals("PostingAndNoteFragment")) {
                        Intent it3 = new Intent(mContext, WriteSuibianLiaoActivity647.class);
                        //不知道为啥传cateid
                        it3.putExtra("cateid", "0" + ",1090");
                        if (isMultiple) {
                            //多选
                            it3.putExtra("isMultiple", "2");
                        } else {
                            //单选
                            it3.putExtra("isMultiple", "1");
                        }
                        //editText内容
                        it3.putExtra("voteTitle", edTitle.getText().toString());
                        //选中的skuid ,分隔
                        it3.putExtra("sku_id", StringUtils.strip(selectList1.toString(), "[]").trim());
                        it3.putParcelableArrayListExtra("sku_data", selectList2);
                        startActivity(it3);
                        finish();
                    } else {
                        Intent intent = new Intent();
                        //不知道为啥传cateid
                        intent.putExtra("cateid", "0" + ",1090");
                        if (isMultiple) {
                            //多选
                            intent.putExtra("isMultiple", "2");
                        } else {
                            //单选
                            intent.putExtra("isMultiple", "1");
                        }
                        //editText内容
                        intent.putExtra("voteTitle", edTitle.getText().toString());
                        //选中的skuid ,分隔
                        intent.putExtra("sku_id", StringUtils.strip(selectList1.toString(), "[]").trim());
                        intent.putParcelableArrayListExtra("sku_data", selectList2);
                        setResult(1002, intent);
                        finish();
                    }
                }

            }
        });

        mFragments = new ArrayList<>();
        mFragments.add(InitiateVoteFragment.newInstance(2 + ""));
        mFragments.add(InitiateVoteFragment.newInstance(3 + ""));
        mFragments.add(InitiateVoteFragment.newInstance(1 + ""));
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], 0, 0));
        }
        tl.setTabData(mTabEntities, this, R.id.fl_change, mFragments);
        //0.45
        ViewGroup.LayoutParams params = tl.getLayoutParams();
        params.width = (int) (DensityUtil.getScreenWidth() * 0.45);
        params.height = DensityUtil.dip2px(30);
        tl.setLayoutParams(params);
        tl.setIndicatorCornerRadius((float) 1.25);
        tl.setCurrentTab(0);
        //指示器宽3 长20
        tl.setIndicatorWidth(20);
        tl.setIndicatorHeight(3);
        tl.getTitleView(0).setTextSize(16);
        tl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                tl.setCurrentTab(position);
                for (int i = 0; i < mTitles.length; i++) {
                    if (position == i) {
                        tl.getTitleView(position).setTextSize(16);
                    } else {
                        tl.getTitleView(i).setTextSize(14);
                    }
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        setAdapter();
        switchIfMultiple.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //选中时
                    isMultiple = true;
                } else {
                    //非选中时
                    isMultiple = false;
                }
            }
        });


        edTitle.addTextChangedListener(watcher);

    }

    TextWatcher watcher = new TextWatcher() {
        private CharSequence temp;
        private int selectionStart;
        private int selectionEnd;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //只要编辑框内容有变化就会调用该方法，s为编辑框变化后的内容
            temp = s;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //编辑框内容变化之前会调用该方法，s为编辑框内容变化之前的内容
        }

        @Override
        public void afterTextChanged(Editable s) {
            //编辑框内容变化之后会调用该方法，s为编辑框内容变化后的内容
//            tvEditNum.setText(edTitle.getText().toString().length() + "/" + 20);
//            int number = num - s.length();
            tvEditNum.setText(s.length() + "/" + 20);
            selectionStart = edTitle.getSelectionStart();
            selectionEnd = edTitle.getSelectionEnd();
            if (temp.length() > num) {
                s.delete(selectionStart - 1, selectionEnd);
                int tempSelection = selectionStart;
                edTitle.setText(s);
                edTitle.setSelection(tempSelection);//设置光标在最后
            }
        }
    };


    @Override
    protected void initData() {

    }

    private void setAdapter() {
        initiateVoteAdapter = new InitiateVoteAdapter(mContext, tao_list);
        YMLinearLayoutManager linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(initiateVoteAdapter);

        initiateVoteAdapter.setOnItemClickListener(new InitiateVoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (initiateVoteAdapter.getItemViewType(pos) == initiateVoteAdapter.ITEM3_TYPE_TAG) {
                    //详情页
//                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
//                    intent.putExtra("id", initiateVoteAdapter.getData().get(pos).get_id());
//                    intent.putExtra("source", "0");
//                    intent.putExtra("objid", "0");
//                    mContext.startActivity(intent);
                } else {
                    //添加
                    Intent intent = new Intent(InitiateVoteActivity.this, SearchProjectActivity.class);
                    intent.putExtra("from", "InitiateVoteActivity");
                    intent.putStringArrayListExtra("taoIdList", selectList1);
                    intent.putExtra("taoData", selectList2);
                    startActivityForResult(intent, 1000);
                }
            }
        });

        initiateVoteAdapter.setOnItemDeleteClickListener(new InitiateVoteAdapter.OnItemDeleteClickListener() {
            @Override
            public void onItemDeleteClick(View view, int pos) {
                selectList1.remove(pos);
                selectList2.remove(pos);
                initiateVoteAdapter.removeItem(pos);
                upUiSwitch();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 10010) {
            EventBus.getDefault().post(new VoteMsgEvent(1, data.getStringArrayListExtra("id")));
            EventBus.getDefault().post(new VoteMsgEvent(2, data.getStringArrayListExtra("taoData")));
//            SearchTaoDate.TaoListBean taoData = data.getParcelableExtra("sku");
//            selectList1.add(taoData.getId().trim());
//            EventBus.getDefault().post(new VoteMsgEvent(1, selectList1));
//            selectList2.add(taoData);
//            EventBus.getDefault().post(new VoteMsgEvent(2, selectList2));
        }
    }

    private void upUiSwitch() {
        if (selectList2 != null && selectList2.size() > 2) {
            switchIfMultiple.setEnabled(true);
            tvVote.setTextColor(Color.parseColor("#666666"));
        } else {
            if (isMultiple) {
                switchIfMultiple.setChecked(false);
            }
            switchIfMultiple.setEnabled(false);
            tvVote.setTextColor(Color.parseColor("#999999"));
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

    }

    /**
     * @param context
     */
    public static void invoke(Context context) {
        Intent intent = new Intent(context, InitiateVoteActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    class TabEntity implements CustomTabEntity {
        public String title;
        public int selectedIcon;
        public int unSelectedIcon;

        public TabEntity(String title, int selectedIcon, int unSelectedIcon) {
            this.title = title;
            this.selectedIcon = selectedIcon;
            this.unSelectedIcon = unSelectedIcon;
        }

        @Override
        public String getTabTitle() {
            return title;
        }

        @Override
        public int getTabSelectedIcon() {
            return selectedIcon;
        }

        @Override
        public int getTabUnselectedIcon() {
            return unSelectedIcon;
        }
    }

    private class DeleteDialogClickListener implements YueMeiDialog.BtnClickListener {

        @Override
        public void leftBtnClick() {
            Intent i = new Intent();
            setResult(1003, i);
            finish();
        }

        @Override
        public void rightBtnClick() {
            yueMeiDialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (mFrom.equals("PostingAndNoteFragment")) {
                Intent i = new Intent();
                setResult(1003, i);
                finish();
            } else {
                yueMeiDialog.show();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
