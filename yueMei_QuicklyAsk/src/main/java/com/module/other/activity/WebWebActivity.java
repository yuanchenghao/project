package com.module.other.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.web.WebViewUrlLoading;
import com.module.other.module.bean.SerializableMap;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.BaoxianPopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by dwb on 17/1/17.
 */
public class WebWebActivity extends Activity {
    private static String ua;
    private static Stack<WebWebActivity> activityStack;
    public static final String VERSION = "1.0.8";
    public static CreditsListener creditsListener;
    public static boolean IS_WAKEUP_LOGIN = false;
    public static String INDEX_URI = "/chome/index";

    private BaoxianPopWindow baoxianPop;
    private Map<String, String> singStr = new HashMap<>();
    private TextView mRightTitle;
    private String subtitle;
    private String TAG = "WebWebActivity";
    private Activity mContext;
    private PageJumpManager pageJumpManager;

    public interface CreditsListener {
        /**
         * 当点击分享按钮被点击
         *
         * @param shareUrl       分享的地址
         * @param shareThumbnail 分享的缩略图
         * @param shareTitle     分享的标题
         * @param shareSubtitle  分享的副标题
         */
        void onShareClick(WebView webView, String shareUrl, String shareThumbnail, String shareTitle, String shareSubtitle);

        /**
         * 当点击登录
         *
         * @param webView    用于登录成功后返回到当前的webview并刷新。
         * @param currentUrl 当前页面的url
         */
        void onLoginClick(WebView webView, String currentUrl);

        /**
         * 当点复制券码时
         *
         * @param mWebView mWebView对象。
         * @param code     复制的券码
         */
        void onCopyCode(WebView mWebView, String code);

        /**
         * 通知本地，刷新颜值币
         *
         * @param mWebView
         * @param credits
         */
        void onLocalRefresh(WebView mWebView, String credits);
    }

    protected String mUrl;
    protected String shareUrl;            //分享的url
    protected String shareThumbnail;    //分享的缩略图
    protected String shareTitle;        //分享的标题
    protected String shareSubtitle;        //分享的副标题
    protected Boolean ifRefresh = false;
    protected String navColor;
    protected String titleColor;
    protected Long shareColor;

    protected WebView mWebView;
    protected LinearLayout mLinearLayout;
    protected RelativeLayout mNavigationBar;
    protected TextView mTitle;
    protected ImageView mBackView;
    protected TextView mShare;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLinearLayout = new LinearLayout(this);
        mLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        setContentView(mLinearLayout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // 锁定竖屏显示

        mContext = WebWebActivity.this;
        pageJumpManager = new PageJumpManager(mContext);

        mUrl = getIntent().getStringExtra("url");
        if (mUrl == null) {
            throw new RuntimeException("url can't be blank");
        }

        //post请求
        Bundle bundle = getIntent().getExtras();
        SerializableMap serMap = (SerializableMap) bundle.get("serMap");
        if (serMap != null) {
            singStr = serMap.getMap();
        }

        // 管理匿名类栈，用于模拟原生应用的页面跳转。
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
        activityStack.push(this);

        subtitle = getIntent().getStringExtra("subtitle");
        // 配置导航条文本颜色
        titleColor = getIntent().getStringExtra("titleColor");
        // 配置导航栏背景颜色
        navColor = getIntent().getStringExtra("navColor");

        // 初始化页面
        initView();

        // api11以上的系统隐藏系统默认的ActionBar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        if (ua == null) {
            ua = mWebView.getSettings().getUserAgentString() + " yuemei/" + VERSION;
        }
        mWebView.getSettings().setUserAgentString(ua);

        BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                if (mTitle != null){
                    if (!TextUtils.isEmpty(title) && Utils.isChinese(title)) {
                        mTitle.setText(title);
                    } else {
                        mTitle.setText("悦美");
                    }
                }

            }
        });

        mWebView.setWebViewClient(baseWebViewClientMessage);

        Log.e(TAG, "mUrl === " + mUrl);
        if (singStr != null && singStr.size() != 0) {
            mWebView.loadUrl(mUrl, singStr);
        } else {
            mWebView.loadUrl(mUrl);
        }


    }

    protected void onBackClick() {
        Intent intent = new Intent();
        setResult(99, intent);
        finishActivity(this);
    }

    // 初始化页面
    protected void initView() {
        int height50dp = dip2px(this, 50);
        if (!mUrl.contains("chat.yuemei.com")){
            // 自定义导航栏
            initNavigationBar();
        }

        // 初始化WebView配置
        initWebView();

        mLinearLayout.addView(mWebView);

    }

    //自定义导航栏，包含 后退按钮，页面标题，分享按钮（默认隐藏）
    protected void initNavigationBar() {
        int dp200 = dip2px(this, 200);
        int dp50 = dip2px(this, 50);
        int dp20 = dip2px(this, 20);
        int dp15 = dip2px(this, 15);
        int dp10 = dip2px(this, 10);


        String titleColorTemp = "0xff" + titleColor.substring(1, titleColor.length());
        Long titlel = Long.parseLong(titleColorTemp.substring(2), 16);
        // 配置分享文案颜色,同title
        shareColor = titlel;
        String navColorTemp = "0xff" + navColor.substring(1, navColor.length());
        Long navl = Long.parseLong(navColorTemp.substring(2), 16);

        mNavigationBar = new RelativeLayout(this);
        mNavigationBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, dp50));
        mNavigationBar.setBackgroundColor(navl.intValue());

        mLinearLayout.addView(mNavigationBar);

        mTitle = new TextView(this);
        mTitle.setMaxWidth(dp200);
        mTitle.setLines(1);
        mTitle.setTextSize(20.0f);
        mNavigationBar.addView(mTitle);

        mTitle.setTextColor(titlel.intValue());
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mTitle.getLayoutParams();
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);

        mBackView = new ImageView(this);
        mBackView.setBackgroundResource(R.drawable.back_black);
        RelativeLayout.LayoutParams mBackLayout = new RelativeLayout.LayoutParams(dp20, dp20);
        mBackLayout.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        mBackLayout.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        mBackLayout.setMargins(dp15, 0, 0, 0);
        mNavigationBar.addView(mBackView, mBackLayout);
        mBackView.setPadding(50, 50, 50, 50);
        mBackView.setClickable(true);

        // 添加后退按钮监听事件
        mBackView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackClick();
            }
        });

        //在导航栏的右侧添加分享按钮（无分享信息的页面隐藏）
        mShare = new TextView(this);
        mShare.setLines(1);
        mShare.setTextSize(20.0f);
        mShare.setText("分享");
        mShare.setPadding(0, 0, dp10, 0);
        mShare.setTextColor(shareColor.intValue());
        mNavigationBar.addView(mShare);
        RelativeLayout.LayoutParams shareLp = (RelativeLayout.LayoutParams) mShare.getLayoutParams();
        shareLp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        shareLp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //设置为默认不显示
        mShare.setVisibility(View.INVISIBLE);
        mShare.setClickable(false);

        //右边标题
        mRightTitle = new TextView(this);
        mRightTitle.setLines(1);
        mRightTitle.setTextSize(16.0f);
        mRightTitle.setText(subtitle);
        mRightTitle.setPadding(0, 0, dp10, 0);
        mRightTitle.setTextColor(shareColor.intValue());
        mNavigationBar.addView(mRightTitle);
        RelativeLayout.LayoutParams shareLp1 = (RelativeLayout.LayoutParams) mRightTitle.getLayoutParams();
        shareLp1.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        shareLp1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //设置为默认不显示
        if (!"".equals(subtitle)) {
            mRightTitle.setVisibility(View.VISIBLE);
            mRightTitle.setClickable(true);
        } else {
            mRightTitle.setVisibility(View.INVISIBLE);
            mRightTitle.setClickable(false);
        }

        mNavigationBar.setVisibility(View.VISIBLE);

        // 添加分享按钮的监听事件
        if (mShare != null) {
            mShare.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (creditsListener != null) {
                        creditsListener.onShareClick(mWebView, shareUrl, shareThumbnail, shareTitle, shareSubtitle);
                    }
                }
            });
        }

        // 添加我要上精选按钮的监听事件
        if (mRightTitle != null) {
            mRightTitle.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    String url = "http://m.yuemei.com/p/1538457.html";
                    WebUrlTypeUtil.getInstance(WebWebActivity.this).urlToApp(url, "0", "0", mUrl);
                }
            });
        }
    }

    //初始化WebView配置
    protected void initWebView() {
        mWebView = new WebView(this);
        mWebView.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        mWebView.setVerticalScrollBarEnabled(false); //垂直滚动条不显示

        mWebView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mWebView.setLongClickable(true);
        mWebView.setScrollbarFadingEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.setDrawingCacheEnabled(true);

        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);    //设置webview支持javascript
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setUseWideViewPort(true);    //设置webview推荐使用的窗口，使html界面自适应屏幕
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportMultipleWindows(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {            //版本大于21默认加载视频是关闭的需要手动开启
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        // User settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }

        CookieManager.getInstance().setAcceptCookie(true);

    }




    /**
     * 拦截url请求，根据url结尾执行相应的动作。 （重要）
     * 用途：模仿原生应用体验，管理页面历史栈。
     *
     * @param view
     * @param url
     * @return
     */
    protected boolean shouldOverrideUrlByDuiba(WebView view, String url) {

        if (url.startsWith("type")) {
            try {
                ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
                parserWebUrl.parserPagrms(url);
                JSONObject obj = parserWebUrl.jsonObject;


                if (obj.getString("type").equals("5981")) {// 融云
                    if (!Utils.isLogin()) {
                        ViewInject.toast("请登录之后再试");
                    }
                } else if (obj.getString("type").equals("5983")) {// 私信
                    String kid = obj.getString("id");
                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                            .setDirectId(kid)
                            .setObjId("0")
                            .setObjType("0")
                            .setYmClass("0")
                            .setYmId("0")
                            .build();
                    pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                } else if (obj.getString("type").equals("6156")) {// 排行榜弹窗

                    String urlss = FinalConstant.PAIHANGBANG_GUIZE;

                    HashMap<String, Object> urlMap = new HashMap<>();

                    baoxianPop = new BaoxianPopWindow(WebWebActivity.this, urlss, urlMap);
                    baoxianPop.showAtLocation(mLinearLayout, Gravity.CENTER, 0, 0);

                } else if (obj.getString("type").equals("6022")) {// 领优惠券
                    String _id = obj.getString("id");

                    if (Utils.isLogin()) {
                        if (Utils.isBind()) {
                            daijinjuanLingqu(_id);
                        } else {
                            Utils.jumpBindingPhone(mContext);

                        }

                    } else {
                        Utils.jumpLogin(mContext);
                    }
                } else if (obj.getString("type").equals("6310")) {
                    finish();
                    return true;
                } else {
                    WebViewUrlLoading.getInstance().showWebDetail(mContext, url);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            if (url.contains("m.yuemei.com/haoyi/")) {
                view.loadUrl(url);
                return true;
            }

            Uri uri = Uri.parse(url);
            // 处理电话链接，启动本地通话应用。
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(intent);
                return true;
            }

            WebUrlTypeUtil.getInstance(WebWebActivity.this).urlToApp(url, "0", "0", mUrl);
            finish();

            if (url.equals("http://m.yuemei.com/")) {
                finish();
                return true;
            }

            if (url.equals("https://h5.youzan.com/v2/goods/35wph6h0q0bia")) {         //社区广告位web自动跳转
                finish();
                return true;
            }

            if (url.contains("sogou") || url.contains("baidu")) {
                finish();
                return true;
            }

        }

        return true;

    }

    void daijinjuanLingqu(String jusnid) {
        String uid = Utils.getUid();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("id", jusnid);
        new DaiJinJuanApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null) {
                    ViewInject.toast(serverData.message);

                    mWebView.reload();
                } else {
                    ViewInject.toast(serverData.message);
                }
            }

        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == 100) {
            if (intent.getStringExtra("url") != null) {
                this.mUrl = intent.getStringExtra("url");

                if (singStr.size() != 0 && singStr != null) {
                    mWebView.loadUrl(mUrl, singStr);
                } else {
                    mWebView.loadUrl(mUrl);
                }
                ifRefresh = false;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        if (ifRefresh) {
            this.mUrl = getIntent().getStringExtra("url");
            if (singStr.size() != 0 && singStr != null) {
                mWebView.loadUrl(mUrl, singStr);
            } else {
                mWebView.loadUrl(mUrl);
            }
            ifRefresh = false;
            //如果首页含有登录的入口，返回时需要同时刷新首页的话，
            // 需要把下面判断语句中的 && this.url.indexOf(INDEX_URI) > 0 去掉。
        } else if (IS_WAKEUP_LOGIN && this.mUrl.indexOf(INDEX_URI) > 0) {
            mWebView.reload();
            IS_WAKEUP_LOGIN = false;
        } else {
            // 返回页面时，如果页面含有onDBNewOpenBack()方法,则调用该js方法。
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mWebView.evaluateJavascript("if(window.onDBNewOpenBack){onDBNewOpenBack()}", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                    }
                });
            } else {
                mWebView.loadUrl("javascript:if(window.onDBNewOpenBack){onDBNewOpenBack()}");
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackClick();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    //--------------------------------------------以下为工具方法----------------------------------------------

    /**
     * 配置分享信息
     */
    protected void setShareInfo(String shareUrl, String shareThumbnail, String shareTitle, String shareSubtitle) {
        this.shareUrl = shareUrl;
        this.shareThumbnail = shareThumbnail;
        this.shareSubtitle = shareSubtitle;
        this.shareTitle = shareTitle;
    }

    /**
     * 结束除了最底部一个以外的所有Activity
     */
    public void finishUpActivity() {
        int size = activityStack.size();
        for (int i = 0; i < size - 1; i++) {
            activityStack.pop().finish();
        }
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
        }
    }

    /**
     * （已弃用此方法，改为使用静态变量IS_WAKEUP_LOGIN来做通过唤醒登录的判断，从而设置返回后刷新）
     * 设置栈内所有Activity为返回待刷新。
     * 作用：未登录用户唤起登录后，将所有栈内的Activity设置为onResume时刷新页面。
     */
//    public void setAllActivityDelayRefresh(){
//    	int size = activityStack.size();
//        for (int i = 0;i < size; i++) {
//        	if(activityStack.get(i)!=this){
//        		activityStack.get(i).delayRefresh = true;
//        	}
//        }
//    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

}

