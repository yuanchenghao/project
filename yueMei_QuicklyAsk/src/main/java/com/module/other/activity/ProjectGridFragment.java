package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.ProjectDetailActivity638;
import com.module.doctor.model.api.LodGroupDataApi;
import com.module.doctor.model.bean.GroupDiscData;
import com.module.home.view.LoadingProgress;
import com.module.other.adapter.ProjectAdpter;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 项目首页 右边列表
 * 
 * @author Rubin
 * 
 */
public class ProjectGridFragment extends Fragment {

	public static final String TAG = "ProjectGridFragment";
	private String pid="";
	private GridView mGrid;
	private List<GroupDiscData> lvGroupData = new ArrayList<GroupDiscData>();
	private Handler mHandler;
	private ProjectAdpter discAdapter;

	private static ProjectGridFragment instance = null;
	private LoadingProgress mDialog;

	// 单例模式
	public static ProjectGridFragment getInstance() {
		if (instance == null) {
			instance = new ProjectGridFragment();
		}
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_project_grid, null);
		mGrid = view.findViewById(R.id.project_grid_flg);
		// 得到数据
		if(isAdded()) {
			pid = getArguments().getString("id");
		}
		mHandler = getHandler();
		mDialog = new LoadingProgress(getActivity());
		mDialog.startLoading();
		lodGroupData();
		mGrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {

				if(pid.length()>0) {
					String id = lvGroupData.get(pos).get_id();
					String name = lvGroupData.get(pos).getCate_name();
					Intent it = new Intent(getActivity(), ProjectDetailActivity638.class);
					it.putExtra("id", id);
					it.putExtra("pid", pid);
					it.putExtra("name", name);
					startActivity(it);
				}
			}
		});
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	void lodGroupData() {

		new Thread(new Runnable() {
			@Override
			public void run() {
				HashMap<String,Object> maps=new HashMap<>();
				maps.put("id",pid);
				maps.put("type",3);
				new LodGroupDataApi().getCallBack(getActivity(), maps, new BaseCallBackListener<ServerData>() {
					@Override
					public void onSuccess(ServerData serverData) {
						if ("1".equals(serverData.code)){
							lvGroupData = JSONUtil.jsonToArrayList(serverData.data,GroupDiscData.class);
							Message message = mHandler.obtainMessage(1);
							message.sendToTarget();
						}
					}
				});
				//lvGroupData = HttpData.loadGroupDiscData(pid, "3");
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (lvGroupData.size() > 0 && lvGroupData != null) {

						mDialog.stopLoading();

						if (null != getActivity()) {
							discAdapter = new ProjectAdpter(getActivity(),
									lvGroupData);

							mGrid.setAdapter(discAdapter);
						}
					} else {
						mDialog.stopLoading();
					}
					break;
				}
			}
		};
	}

	public void onResume() {
		super.onResume();
		StatService.onResume(getActivity());
		MobclickAgent.onPageStart("MainScreen"); // 统计页面
	}

	public void onPause() {
		super.onPause();
		StatService.onPause(getActivity());
		MobclickAgent.onPageEnd("MainScreen");
	}
}
