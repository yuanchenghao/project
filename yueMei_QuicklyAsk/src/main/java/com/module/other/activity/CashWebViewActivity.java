package com.module.other.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.api.ShareDataApiZt;
import com.module.commonview.module.bean.TaoShareData;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.module.other.other.CashWebViewClient;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;


public class CashWebViewActivity extends BaseActivity {

    private final String TAG = "CashWebViewActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private RelativeLayout contentWeb;

    @BindView(id = R.id.wan_beautiful_web_back, click = true)
    private RelativeLayout back;// 返回
    @BindView(id = R.id.wan_beautifu_docname)
    private TextView titleBarTv;
    @BindView(id = R.id.tao_web_share_rly111, click = true)
    private RelativeLayout shareBt;// 分享

    @BindView(id = R.id.tao_web_refresh_rly111, click = true)
    private RelativeLayout refreshBt;// 刷新

    private WebView docDetWeb;

    private CashWebViewActivity mContext;

    private String url;
    private String urlStr;

    private String mTitle;

    public Handler handler = new Handler();

    public JSONObject obj_http;

    private String ztid = "";

    // 分享
    public String shareUrl = "";
    public String shareContent = "";
    public String shareImgUrl;
    public String shareWXImgUrl;
    public String shareTitle;

    @BindView(id = R.id.zhuanti_choujiang_rly)
    private RelativeLayout lingHongbaoRly;
    @BindView(id = R.id.choujiang_iv, click = true)
    private ImageView lingHongbaoIv;

    @BindView(id = R.id.all_ly)
    private LinearLayout allLinely;

    private PopupWindows hongbaoPop;

    private String uid;

    String curCity = "全国";

    private String login = "0";
    private PageJumpManager pageJumpManager;
    private String userAgent;
    private boolean installWeiXin;
    private boolean installQQ;
    private boolean installSina;
    private String shareContext = "一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~";
    private BaseWebViewClientMessage baseWebViewClientMessage;


    @Override
    public void setRootView() {
        setContentView(R.layout.activity_cash_web_view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = CashWebViewActivity.this;
        pageJumpManager = new PageJumpManager(mContext);
        uid = Utils.getUid();

        UMShareAPI umShareAPI = UMShareAPI.get(mContext);
        installWeiXin = umShareAPI.isInstall(mContext, SHARE_MEDIA.WEIXIN);
        installQQ = umShareAPI.isInstall(mContext, SHARE_MEDIA.QQ);
        installSina = umShareAPI.isInstall(mContext, SHARE_MEDIA.SINA);

        curCity = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");
        if (curCity.length() > 0) {

            if (curCity.equals("失败")) {
                curCity = "全国";
            }
        } else {
            curCity = "全国";
        }

        Intent it0 = getIntent();
        url = it0.getStringExtra("url");

        String substring = url.substring(url.length() - 1, url.length());
        if (!"/".equals(substring)) {
            url = url + "/";
        }


        mTitle = it0.getStringExtra("title");
        ztid = it0.getStringExtra("ztid");

        urlStr = url;
        setWebType();

        initWebview();

        LodUrl(urlStr);
        initShareData();


        hongbaoPop = new PopupWindows(mContext, allLinely);

        if (null != ztid) {

            if (ztid.equals("675")) {

                String hongbao = Cfg.loadStr(mContext, "hongbao", "");
                if (hongbao.length() > 0) {
                } else {
                    new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            hongbaoPop.showAtLocation(allLinely, Gravity.BOTTOM, 0,
                                    0);
                        }
                    }.start();
                }
            }
        }

    }

    private void setWebType() {
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
        baseWebViewClientMessage.setParameter5983(ztid, "1",shareTitle,shareWXImgUrl,"0","0");
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
            @Override
            public void typeOutside(WebView view, String url) {
                if (url.startsWith("type")) {
                    baseWebViewClientMessage.showWebDetail(url);
                } else if (url.startsWith("http://user.yuemei.com/home/taozt/")) {
                    docDetWeb.loadUrl(url);
                } else if (url.startsWith("https://user.yuemei.com/home/taozt/")) {
                    docDetWeb.loadUrl(url);
                } else {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "8", ztid,urlStr);
                }
            }
        });

        baseWebViewClientMessage.setBaseWebViewClientCallback(new CashWebViewClient(mContext));
    }

    public void onResume() {
        super.onResume();
        StatService.onResume(this);
        MobclickAgent.onResume(this);
        TCAgent.onResume(this);

        urlStr = url;
        if (Utils.isLogin()) {
            initShareData();
            LodUrl(urlStr);
        }
    }


    @SuppressLint("InlinedApi")
    public void initWebview() {
        docDetWeb = new WebView(mContext);

        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        docDetWeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        docDetWeb.loadData("", "text/html", "UTF-8");

        docDetWeb.setLongClickable(true);
        docDetWeb.setScrollbarFadingEnabled(true);
        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        docDetWeb.setDrawingCacheEnabled(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                String[] strs = message.split("\n");

                showDialogExitEdit2("确定", message, result, strs.length);

                return true;
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if ("0".equals(mTitle)) {
                    titleBarTv.setText(title);
                } else {
                    titleBarTv.setText(mTitle);
                }

            }

        });

        //设置 缓存模式
        WebSettings settings = docDetWeb.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);

        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setJavaScriptEnabled(true);                //支持js
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.supportMultipleWindows();
        settings.setNeedInitialFocus(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(
                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        userAgent = settings.getUserAgentString() + "/yuemei.com";
        settings.setUserAgentString(userAgent);

        contentWeb.addView(docDetWeb);
    }


    /**
     * 加载web
     */
    public void LodUrl(String url) {
        baseWebViewClientMessage.startLoading();

        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit2(String title, String content, final JsResult result, int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        result.confirm();       //关闭js的弹窗
        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);


        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContext, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

    }

    void initShareData() {

        uid = Utils.getUid();

        String shareurlS = "";
        if (!"0".equals(uid)) {
            shareurlS = FinalConstant.ZHUANTI_SHARE + ztid + "/uid/" + uid + "/city/" + curCity + "/" + Utils.getTokenStr();
        } else {
            shareurlS = FinalConstant.ZHUANTI_SHARE + ztid + "/city/" + curCity + "/" + Utils.getTokenStr();
        }
        Map<String,Object> maps=new HashMap<>();
        maps.put("id",ztid);
        if (Utils.isLogin()){
            maps.put("uid",uid);
        }
        maps.put("city",curCity);
        new ShareDataApiZt().getCallBack(mContext, maps, new BaseCallBackListener<TaoShareData>() {
            @Override
            public void onSuccess(TaoShareData taoShareData) {
                shareUrl = taoShareData.getUrl();
                shareContent = taoShareData.getContent();
                shareImgUrl = taoShareData.getImg_weibo();
                shareWXImgUrl = taoShareData.getImg_weix();
                shareTitle = taoShareData.getTitle();
            }

        });
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.wan_beautiful_web_back:
                // onBackPressed();
                finish();
                break;
            case R.id.tao_web_share_rly111:// 分享按钮
                if (null != shareContent && shareContent.length() > 0) {

                    setShare();
                }

                break;
            case R.id.tao_web_refresh_rly111:
                webReload();

                break;
            case R.id.choujiang_iv:// 抢红包
                lingHongbaoRly.setVisibility(View.GONE);
                hongbaoPop.showAtLocation(allLinely, Gravity.BOTTOM, 0, 0);
                break;
        }
    }

    /**
     * 设置分享
     */
    public void setShare() {
        BaseShareView baseShareView = new BaseShareView(mContext);
        baseShareView
                .setShareContent(shareContent)
                .ShareAction();

        baseShareView.getShareBoardlistener()
                .setSinaText("#整形#" + shareContent + shareUrl + "分享自@悦美整形APP")
                .setSinaThumb(new UMImage(mContext, shareImgUrl))
                .setSmsText(shareTitle + "，" + shareUrl)
                .setTencentUrl(shareUrl)
                .setTencentTitle(shareTitle)
                .setTencentThumb(new UMImage(mContext, TextUtils.isEmpty(shareWXImgUrl) ? shareImgUrl : shareWXImgUrl))
                .setTencentDescription(shareContent)
                .setTencentText(shareContent)
                .getUmShareListener()
                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                    @Override
                    public void onShareResultClick(SHARE_MEDIA platform) {

                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                            Toast.makeText(mContext, " 分享成功啦",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    ImageView hongbaoBgIv;
    RelativeLayout hongbaoBgRly;
    RelativeLayout hongbaoColseRly;
    Button hongbaoLingSahreBt;
    Button hongbaoShareBt;
    LinearLayout hongbaoZjly;

    /**
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(final Context mContextt, View parent) {

            final View view = View.inflate(mContextt,
                    R.layout.pop_qiang_hongbao, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContextt,
                    R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            // showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            hongbaoBgIv = view.findViewById(R.id.hongbao_bg_iv);
            hongbaoBgRly = view
                    .findViewById(R.id.hongbao_bg_rly);
            hongbaoColseRly = view
                    .findViewById(R.id.hongbao_close_cha_rly);
            hongbaoLingSahreBt = view
                    .findViewById(R.id.lingqu_share_bt);
            hongbaoShareBt = view
                    .findViewById(R.id.lingqu_share_bt_share);
            hongbaoZjly = view
                    .findViewById(R.id.hongbao_zhongjia_wen_ly);

            hongbaoColseRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                    lingHongbaoRly.setVisibility(View.VISIBLE);
                }
            });

            hongbaoBgRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                    lingHongbaoRly.setVisibility(View.VISIBLE);
                }
            });

            hongbaoLingSahreBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    uid = Utils.getUid();
                    if (Utils.isLogin()) {
                        if (Utils.isBind()){
                            hongbaoLingqu();
                        }else {
                            Utils.jumpBindingPhone(mContext);

                        }

                    } else {
                        Utils.jumpLogin(mContext);
                    }
                }
            });

            /**
             * 红包分享
             */
            hongbaoShareBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    setShare2();
                }
            });

        }
    }

    /**
     * 设置分享
     */
    private void setShare2() {
        BaseShareView baseShareView = new BaseShareView(mContext);
        baseShareView
                .setShareContent("【悦美】猴年大吉，抢千元红包")
                .ShareAction();

        baseShareView.getShareBoardlistener()
                .setSinaText(shareContext + shareUrl)
                .setSinaThumb(new UMImage(mContext, R.drawable.hongbao_weibo_share))
                .setSmsText(shareContext)
                .setTencentUrl(shareUrl)
                .setTencentTitle(shareTitle)
                .setTencentThumb(new UMImage(mContext, R.drawable.hongbao_weibo_share))
                .setTencentDescription(shareContent)
                .setTencentText(shareContext + shareUrl)
                .getUmShareListener()
                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                    @Override
                    public void onShareResultClick(SHARE_MEDIA platform) {

                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                            Toast.makeText(mContext, " 分享成功啦",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                    }
                });
    }

    void hongbaoLingqu() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("id", "461");
        new DaiJinJuanApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
               if ("1".equals(s.code)){
                   hongbaoLingSahreBt.setVisibility(View.GONE);
                   hongbaoShareBt.setVisibility(View.VISIBLE);
                   hongbaoZjly.setVisibility(View.VISIBLE);
                   hongbaoBgIv.setBackgroundResource(R.drawable.zhongjiang_2x);

                   Cfg.saveStr(mContext, "hongbao", "1");
               }else {
                   ViewInject.toast(s.message);
               }
            }

        });
    }



    public void onPause() {
        super.onPause();
        StatService.onPause(this);
        MobclickAgent.onPause(this);
        TCAgent.onPause(this);
    }

}
