/**
 *
 */
package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.ElasticScrollView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.net.URLDecoder;

/**
 * @author lenovo17
 */
public class YuemeiWebActivity extends BaseActivity {

    @BindView(id = R.id.slide_pic_web_det_scrollview1, click = true)
    private ElasticScrollView scollwebView;
    @BindView(id = R.id.slide_pic_linearlayout, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.yuemei_web_top)
    private CommonTopBar mTop;// 返回

    private WebView bbsDetWeb;

    private YuemeiWebActivity mContex;

    private String url;
    private String url_a;

    public JSONObject obj_http;

    private String uid;

    private static final int SHOW_TIME = 1000;
    private BaseWebViewClientMessage baseWebViewClientMessage;

    @Override
    public void setRootView() {
        setContentView(R.layout.web_acty_yuemei);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = YuemeiWebActivity.this;
        uid = Utils.getUid();

        mTop.setCenterText("悦美");

        Intent it0 = getIntent();
        url_a = it0.getStringExtra("url");

        scollwebView.GetLinearLayout(contentWeb);

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });
        initWebview();
    }

    public void onResume() {
        super.onResume();
        StatService.onResume(this);
        MobclickAgent.onResume(this);
        TCAgent.onResume(this);
        uid = Utils.getUid();
        url = FinalConstant.baseUrl + FinalConstant.VER + url_a;
        LodUrl(url);
    }

    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi"})
    public void initWebview() {
        bbsDetWeb = new WebView(mContex);
        bbsDetWeb.getSettings().setJavaScriptEnabled(true);
        bbsDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        bbsDetWeb.getSettings().setUseWideViewPort(true);
        bbsDetWeb.getSettings().supportMultipleWindows();
        bbsDetWeb.getSettings().setNeedInitialFocus(true);
        bbsDetWeb.setWebViewClient(baseWebViewClientMessage);
        bbsDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(bbsDetWeb);
    }

    protected void OnReceiveData(String str) {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (bbsDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            bbsDetWeb.reload();
        }
    }

    public void showWebDetail(String urlStr) {
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 医生详情页
                try {
                    String id = obj.getString("id");
                    String docname = URLDecoder.decode(
                            obj.getString("docname"), "utf-8");

                    Intent it = new Intent();
                    it.setClass(mContex, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", docname);
                    it.putExtra("partId", "");
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (obj.getString("type").equals("6")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");

                Intent it = new Intent();
                it.setClass(mContex, DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                startActivity(it);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * 加载web
     */
    public void LodUrl(String url) {
        baseWebViewClientMessage.startLoading();
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        bbsDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
