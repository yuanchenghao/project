package com.module.other.netWork.netWork;


import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.module.MyApplication;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.utils.StatisticalManage;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.view.RewardAlertToast;
import com.module.other.module.bean.Data;
import com.module.other.module.bean.RewardAlertData;
import com.module.other.module.bean.SessionidData;
import com.module.other.netWork.SignUtils;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 联网请求类
 * Created by 裴成浩 on 2017/9/30.
 */

public class NetWork {

    private long timeOffset = 0;//服务器时间和本地时间的差值
    private final static String TAG = "NetWork_Http";

    /**
     * 单例
     */
    private static volatile NetWork netWork;
    private final Gson mGson;

    private NetWork() {
        mGson = new Gson();
    }

    public static NetWork getInstance() {
        if (netWork == null) {
            synchronized (NetWork.class) {
                if (netWork == null) {
                    netWork = new NetWork();
                }
            }
        }
        return netWork;
    }

    //保存所有注册的数据
    private HashMap<String, BindData> mBindDatas = new HashMap<>();

    /**
     * 注册请求地址
     *
     * @param agreement:联网请求协议
     * @param addr:服务端地址
     * @param entry:服务端代码入口
     * @param ifaceName:接口名
     * @param ifaceType:接口类型（get or post）
     */
    public void regist(String agreement, String addr, String entry, String ifaceName, EnumInterfaceType ifaceType) {
        regist(agreement, addr, entry, ifaceName, ifaceType, null);
    }

    /**
     * 注册请求地址
     *
     * @param agreement:联网请求协议
     * @param addr:服务端地址
     * @param entry:服务端代码入口
     * @param ifaceName:接口名
     * @param ifaceType:接口类型（get or post）
     * @param recordClass:返回实体类型
     */
    public void regist(String agreement, String addr, String entry, String ifaceName, EnumInterfaceType ifaceType, Class<?> recordClass) {
        //初始化BindData
        BindData data = new BindData(Utils.getCity(), Utils.getUid(), Utils.getImei());
        data.setAgreement(agreement);
        data.setAddr(addr);
        data.setEntry(entry);
        data.setIfaceName(ifaceName);
        data.setIfaceType(ifaceType);
        if (recordClass != null) {
            data.setRecordClass(recordClass);
        }
        //把数据存起来
        mBindDatas.put(entry + ifaceName, data);
    }

    /**
     * 客户端调用接口
     *
     * @param entry:服务端代码入口
     * @param ifaceName:接口名
     * @param maps:参数
     * @param cb：回调
     */
    public void call(String entry, String ifaceName, Map<String, Object> maps, ServerCallback cb) {
        call(entry, ifaceName, maps, null, cb);
    }

    public void call(String entry, String ifaceName, Map<String, Object> maps, HttpParams uploadParams, ServerCallback cb) {
        boolean isChange = false;

        BindData bindData = mBindDatas.get(entry + ifaceName);
        //判断这个接口是否注册了
        if (null == bindData) {
            return;
        }
        //判断城市是否发生了变化
        if (!bindData.getCity().equals(Utils.getCity())) {
            bindData.setCity(Utils.getCity());
            isChange = true;
        }
        //判断uid是否发生了变化
        if (!bindData.getUid().equals(Utils.getUid())) {
            bindData.setUid(Utils.getUid());
            isChange = true;
        }

        //判断唯一标识是否发生了变化
        if (!bindData.getImei().equals(Utils.getImei())) {
            bindData.setImei(Utils.getImei());
            isChange = true;
        }

        //判断会话id是否超时
        SessionidData sessionidData = Utils.getSessionid();
        if (sessionidData != null) {
            Long interval = (System.currentTimeMillis() - sessionidData.getTime()) / (1000 * 60);
            if (interval > 30) {
                isChange = true;
            }
        }

        //如果是变化的
        if (isChange) {
            mBindDatas.put(entry + ifaceName, bindData);
        }

        //设置时间
        String time = (System.currentTimeMillis() / 1000) + "";
        maps.put(FinalConstant1.TIME, time);

        switch (bindData.getIfaceType()) {
            case GET:
                get(bindData, maps, cb);
                break;
            case POST:
                post(bindData, maps, cb);
                break;
            case CACHE_GET:
                cacheGet(bindData, maps, cb);
                break;
            case CACHE_POST:
                cachePost(bindData, maps, cb);
                break;
            case DOWNLOAD:
                download(bindData, maps, cb);
                break;
            case UPLOAD:
                upload(bindData, maps, uploadParams, cb);
                break;
        }
    }

    /**
     * get请求
     *
     * @param bindData
     * @param cb
     * @param maps
     */
    public void get(final BindData bindData, Map<String, Object> maps, final ServerCallback cb) {
        Map<String, String> httpParams = SignUtils.buildHttpParamMap(maps);
        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);
        CookieConfig.getInstance().setCookie(bindData.getAgreement(), bindData.getAddr(), bindData.getAddr());

        String url = getherUrl(bindData, httpParams);

        Log.e(TAG, "getherUrl --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> url === " + url);

        OkGo.get(url).headers(headers).execute(new StringCallback() {
            @Override
            public void onSuccess(String result, Call call, Response response) {
                Log.e(TAG, "get --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " == " + result);
                handleResponse(bindData, result, cb);
                if (bindData.getUrl().contains("cart/getcartnumber")) {
                    Cfg.saveStr(MyApplication.getContext(), FinalConstant.NO_NETWORK, "0");
                }
            }

            @Override
            public void onError(Call call, Response response, Exception e) {
                super.onError(call, response, e);
                Log.e(TAG, "get -->  " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> call == " + call);
                Log.e(TAG, "get --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> e == " + e.toString());

                handleResponse(bindData, setErrorResult(), cb);
                if (bindData.getUrl().contains("cart/getcartnumber")) {
                    Cfg.saveStr(MyApplication.getContext(), FinalConstant.NO_NETWORK, "1");
                }
            }

        });
    }

    /**
     * post请求
     *
     * @param bindData
     * @param maps
     * @param cb
     */
    public void post(final BindData bindData, Map<String, Object> maps, final ServerCallback cb) {
        YMHttpParams httpParams = SignUtils.buildHttpParam5(maps);
        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);
        CookieConfig.getInstance().setCookie(bindData.getAgreement(), bindData.getAddr(), bindData.getAddr());
        OkGo.post(bindData.getUrl()).cacheMode(CacheMode.DEFAULT).params(httpParams).headers(headers).execute(new StringCallback() {
            @Override
            public void onSuccess(String result, Call call, Response response) {
                Log.e(TAG, "post --> onSuccess ==" + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> result == " + result);
                handleResponse(bindData, result, cb);

            }

            @Override
            public void onError(Call call, Response response, Exception e) {
                super.onError(call, response, e);
                if (bindData.getUrl().contains("chat/send")){
                    if (null != mOnErrorCallBack){
                        mOnErrorCallBack.onErrorCallBack(call,response,e);
                    }
                }
                if (call != null) {
                    Log.e(TAG, "post --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> call === " + call.toString());
                }
                if (response != null) {
                    Log.e(TAG, "post --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> response === " + response.toString());
                }

                if (e != null) {
                    Log.e(TAG, "post --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> e === " + e.toString());
                }

                handleResponse(bindData, setErrorResult(), cb);
                if (bindData.getUrl().contains("homenew/home") || bindData.getUrl().contains("usernew/login")) {
                    Toast.makeText(MyApplication.getContext(), "网络连接异常", Toast.LENGTH_SHORT).show();
                }


            }
        });
        String newString = removeOtherChar(httpParams.toString());
        Log.e(TAG, "post ---> " + bindData.getUrl() + newString);
    }


    /**
     * 去掉[]等特殊字符
     * @param orginStr
     * @return
     */
    private String removeOtherChar(String orginStr){
        //可以在中括号内加上任何想要替换的字符
        String regEx="[\n`~!@#$%^&*()+=|':;',\\[\\]<>?~！@#￥%……&*（）——+|【】‘；：”“’。， 、？]";
        String aa = "";//这里是将特殊字符换为aa字符串,""代表直接去掉
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(orginStr);//这里把想要替换的字符串传进来
        return m.replaceAll(aa).trim();
    }


    /**
     * 带缓存的get请求
     *
     * @param bindData
     * @param maps
     * @param cb
     */
    private void cachePost(BindData bindData, Map<String, Object> maps, ServerCallback cb) {

    }

    /**
     * 带缓存的post请求
     *
     * @param bindData
     * @param maps
     * @param cb
     */
    private void cacheGet(BindData bindData, Map<String, Object> maps, ServerCallback cb) {

    }

    /**
     * 下载
     *
     * @param bindData
     * @param maps
     * @param cb
     */
    public void download(BindData bindData, Map<String, Object> maps, ServerCallback cb) {

    }

    /**
     * 上传
     *
     * @param bindData
     * @param maps
     * @param cb
     */
    public void upload(final BindData bindData, Map<String, Object> maps, HttpParams uploadParams, final ServerCallback cb) {
        //上传判断 获取参数
        if (uploadParams != null) {
            Map<String, String> map = SignUtils.buildHttpParamMap(maps);
            List<String> keys = new ArrayList<>(map.keySet());
            for (int i = 0; i < keys.size(); i++) {
                String key = keys.get(i);
                String value = map.get(key);
                uploadParams.put(key, value);
            }
        } else {
            uploadParams = SignUtils.buildHttpParam5(maps);
        }

        //把File文件加到参数中
        List<String> mapKeys = new ArrayList<>(maps.keySet());
        for (String key : mapKeys) {
            Object value = maps.get(key);
            if (value instanceof File) {
                uploadParams.put(key, (File) value);
            }
        }

        //加密头部
        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);

        CookieConfig.getInstance().setCookie(bindData.getAgreement(), bindData.getAddr(), bindData.getAddr());
        Log.e(TAG, "upload --> uploadParams == " + uploadParams.toString());
        Log.e(TAG, "upload --> url == " + bindData.getUrl());
        OkGo.post(bindData.getUrl()).cacheMode(CacheMode.DEFAULT).params(uploadParams).headers(headers).execute(new StringCallback() {
            @Override
            public void onSuccess(String result, Call call, Response response) {
                Log.e(TAG, "upload --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> result == " + result);
                handleResponse(bindData, result, cb);
            }

            @Override
            public void onError(Call call, Response response, Exception e) {
                super.onError(call, response, e);
                if (bindData.getUrl().contains("chat/send")){
                    if (null != mOnErrorCallBack){
                        mOnErrorCallBack.onErrorCallBack(call,response,e);
                    }
                }
                if (call != null) {
                    Log.e(TAG, "upload --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> call === " + call.toString());
                }
                if (response != null) {
                    Log.e(TAG, "upload --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> response === " + response.toString());
                }
                if (e != null) {
                    Log.e(TAG, "upload --> " + bindData.getEntry() + "/" + bindData.getIfaceName() + " ---> e === " + e.toString());
                }

                handleResponse(bindData, setErrorResult(), cb);
            }
        });
    }


    /**
     * 返回数据的解析
     *
     * @param bindData
     * @param result
     * @param cb
     */
    private void handleResponse(BindData bindData, String result, ServerCallback cb) {
        try {
            ServerData serverData = new ServerData();
            String code = JSONUtil.resolveJson(result, FinalConstant.CODE);
            Log.e(TAG, "code == " + code);
            String message = JSONUtil.resolveJson(result, FinalConstant.MESSAGE);
            if (null != code)
                switch (code) {
                    case "404":
                        JSONObject jsonObject2 = new JSONObject(result);
                        if (jsonObject2.isNull("data")) {
                            serverData.bindData = bindData;
                            serverData.code = code;
                            serverData.message = message;
                            cb.onServerCallback(serverData);
                        }
                        Toast.makeText(MyApplication.getContext(), "没有内容", Toast.LENGTH_SHORT).show();
                        break;
                    case "10001":
                        Toast.makeText(MyApplication.getContext(), "当前账号密码变动，请重新登录", Toast.LENGTH_SHORT).show();
                        Utils.clearUserData();
                        Intent it = new Intent();
                        it.setClass(MyApplication.getContext(), LoginActivity605.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        MyApplication.getContext().startActivity(it);
                        break;
                    case "20001":
                        JSONObject jsonObject1 = new JSONObject(result);
                        String mDataStr = jsonObject1.getString("data");
                        if (!jsonObject1.isNull("is_alert_message")) {
                            String is_alert_message = jsonObject1.getString("is_alert_message");
                            serverData.is_alert_message = is_alert_message;
                        }
                        Data data = JSONUtil.TransformSingleBean(mDataStr, Data.class);
                        RewardAlertData rewardAlertData = data.getRewardAlertData();
                        RewardAlertToast.show(rewardAlertData);
                        serverData.bindData = bindData;
                        serverData.code = "1";
                        serverData.data = mDataStr;
                        serverData.message = message;
                        serverData.isOtherCode = true;

                        Log.e(TAG, "handleResponse --> serverData.toString() === " + serverData.toString());
                        cb.onServerCallback(serverData);
                        break;
                    case "0":
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("Error_messages", message);
                        hashMap.put("url", bindData.getUrl());
                        StatisticalManage.getInstance().growingIO("Errors", hashMap);
                    case "1":
                    default:
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.isNull("data")) {
                            serverData.bindData = bindData;
                            serverData.code = code;
                            serverData.message = message;
                            cb.onServerCallback(serverData);
                        } else if (jsonObject.isNull("is_alert_message")) {
                            String mData = jsonObject.getString("data");
                            Log.e(TAG, "handleResponse --> mData === " + mData);
                            serverData.bindData = bindData;
                            serverData.code = code;
                            serverData.data = mData;
                            serverData.message = message;
                            Log.e(TAG, "handleResponse --> serverData.toString() === " + serverData.toString());
                            cb.onServerCallback(serverData);
                        } else {
                            String mData = jsonObject.getString("data");
                            String is_alert_message = jsonObject.getString("is_alert_message");
                            Log.e(TAG, "handleResponse --> mData === " + mData);
                            serverData.bindData = bindData;
                            serverData.code = code;
                            serverData.data = mData;
                            serverData.message = message;
                            serverData.is_alert_message = is_alert_message;
                            Log.e(TAG, "handleResponse --> serverData.toString() === " + serverData.toString());
                            cb.onServerCallback(serverData);
                        }
                        break;
                }


        } catch (JSONException e) {
            Log.e(TAG, "handleResponse --> e === " + e.toString());
            if (FinalConstant1.YUEMEI_DEBUG) {
                Toast.makeText(MyApplication.getContext(), "数据异常稍后再试", Toast.LENGTH_SHORT).show();
            }
            e.printStackTrace();
        }

    }

    /**
     * 拼接传过来的参数
     *
     * @param bindData
     * @param maps
     * @return
     */
    private String getherUrl(BindData bindData, Map<String, String> maps) {
        StringBuilder url = new StringBuilder(bindData.getUrl());

        switch (bindData.getAddr()) {
            case FinalConstant1.BASE_API_M_URL:
            case FinalConstant1.BASE_API_URL:

                //拼接传过来的参数
                Iterator<String> iter1 = maps.keySet().iterator();
                while (iter1.hasNext()) {
                    String key = iter1.next();
                    String value = maps.get(key);
                        url.append(key).append(FinalConstant1.SYMBOL4).append(value).append(FinalConstant1.SYMBOL5);
                }


                break;
            default:

                Iterator<String> iter2 = maps.keySet().iterator();
                while (iter2.hasNext()) {
                    String key = iter2.next();
                    String value = maps.get(key);
                        url.append(key).append(FinalConstant1.SYMBOL2).append(value).append(FinalConstant1.SYMBOL2);
                }
                break;
        }
        return url.toString();
    }

    /**
     * 设置请求失败的json
     */
    private String setErrorResult() {
        ErrorResult errorResult = new ErrorResult();
        errorResult.setCode("-1");
        errorResult.setMessage("联网请求失败");
        return mGson.toJson(errorResult);
    }

    /**
     * 获取本地时间
     *
     * @return
     */
    public long getLocalTime() {
        return System.currentTimeMillis();//毫秒，注意时间单位的统一。
    }

    /**
     * 获取服务器时间
     *
     * @return
     */
    public long getServerTime() {
        return getLocalTime() + timeOffset;
    }

    /**
     * 获取接口全连接信息
     *
     * @param key：接口名称 (addr+entry)
     * @return
     */
    public BindData getBindData(String key) {
        return mBindDatas.get(key);
    }




    OnErrorCallBack mOnErrorCallBack;

    public void setOnErrorCallBack(OnErrorCallBack onErrorCallBack) {
        mOnErrorCallBack = onErrorCallBack;
    }

    public interface OnErrorCallBack{
        void onErrorCallBack(Call call, Response response, Exception e);
    }


}

