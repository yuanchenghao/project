package com.module.other.netWork.netWork;

import java.util.Map;

/**
 * webView签名后返回的连接和头
 * Created by 裴成浩 on 2018/3/14.
 */

public class WebSignData {
    private String url;
    private Map<String,String> httpHeaders;

    public WebSignData(String url, Map<String, String> httpHeaders) {
        this.url = url;
        this.httpHeaders = httpHeaders;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHttpHeaders() {
        return httpHeaders;
    }

    public void setHttpHeaders(Map<String, String> httpHeaders) {
        this.httpHeaders = httpHeaders;
    }
}
