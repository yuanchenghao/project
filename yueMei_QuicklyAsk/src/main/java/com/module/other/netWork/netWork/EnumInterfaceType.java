package com.module.other.netWork.netWork;

/**
 * Created by 裴成浩 on 2017/9/30.
 */

public enum EnumInterfaceType {
    GET, CACHE_GET, POST, CACHE_POST, DOWNLOAD, UPLOAD
}
