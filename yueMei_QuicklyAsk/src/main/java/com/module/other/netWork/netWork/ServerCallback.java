package com.module.other.netWork.netWork;

/**
 * 客户端回调接口
 * Created by 裴成浩 on 2017/9/30.
 */

public interface ServerCallback {

    //status 表示网络请求状态，bindData表示当前请求相关参数，record表示返回数据
    void onServerCallback(ServerData mData);
}
