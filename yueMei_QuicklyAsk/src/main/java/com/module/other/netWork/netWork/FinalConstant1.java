package com.module.other.netWork.netWork;

import com.leon.channel.helper.ChannelReaderUtil;
import com.module.MyApplication;
import com.module.commonview.module.bean.CashBack;
import com.module.commonview.module.bean.ChatListBean;
import com.module.commonview.module.bean.DaiJinJuan;
import com.module.commonview.module.bean.IfTaoOrderData;
import com.module.commonview.module.bean.SpeltShareData;
import com.module.commonview.module.bean.TaoShareData;
import com.module.commonview.module.bean.YuDingData;
import com.module.community.model.bean.BBsButtonData;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.SearchAboutData;
import com.module.community.model.bean.ZhuanTi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.DocHeadData;
import com.module.doctor.model.bean.DocShareData;
import com.module.doctor.model.bean.HosShareData;
import com.module.doctor.model.bean.HosYuYueData;
import com.module.doctor.model.bean.MainCityData;
import com.module.doctor.model.bean.PartAskData;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.home.model.bean.Home623;
import com.module.home.model.bean.SaozfData;
import com.module.home.model.bean.SearchXSData;
import com.module.my.model.bean.GetCodeData;
import com.module.my.model.bean.KeFuData;
import com.module.my.model.bean.PostInfoData;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ShenHeData;
import com.module.my.model.bean.UserData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Advert;
import com.quicklyask.entity.JFJY1;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.NewsNumberData;
import com.quicklyask.entity.ProjectHome550;
import com.quicklyask.entity.Status1Data;
import com.quicklyask.entity.TaoData623;
import com.quicklyask.entity.TaoDetail591;
import com.quicklyask.entity.VersionJCData;
import com.quicklyask.util.Utils;

import static com.module.other.netWork.netWork.EnumInterfaceType.GET;
import static com.module.other.netWork.netWork.EnumInterfaceType.POST;
import static com.module.other.netWork.netWork.EnumInterfaceType.UPLOAD;

/**
 * 新换的网络接口类
 * Created by 裴成浩 on 2017/9/30.
 */

public class FinalConstant1 {

    // token
    public static final String YUEMEI_APP_KEY = "84ea4af0199361f0b15f1ac1a0d87e57";

    //悦美调试（正式包记得关闭）
    public static final boolean YUEMEI_DEBUG = false;

    //公共参数key
    public static final String CITY = "city";
    public static final String UID = "uid";
    public static final String APPKEY = "appkey";
    public static final String VER = "ver";
    public static final String DEVICE = "device";
    public static final String MARKET = "market";
    public static final String IMEI = "imei";
    public static final String TIME = "time";
    public static final String LONGITUDE = "lon";
    public static final String LATITUDE = "lat";
    public static final String SESSIONID = "ymsessionid";


    //公共参数value
    public static final String YUEMEI_VER = "V" + Utils.getVersionName();                                       //版本号
    public static final String YUEMEI_DEVICE = "android";                                                       //android还是ios（设备标识）
    private static final String MYAPP_MARKET = MyApplication.getContext().getString(R.string.marketv);          //自己写的渠道
    public static final String VASDOLLY_MARKET = ChannelReaderUtil.getChannel(MyApplication.getContext());
    public static final String YUEMEI_MARKET = VASDOLLY_MARKET == null ? MYAPP_MARKET : VASDOLLY_MARKET;          //市场渠道

    //协议
    public static final String HTTPS = "https";
    public static final String WSS = "wss";

    //符号
    public static final String SYMBOL1 = "://";
    public static final String SYMBOL2 = "/";
    public static final String SYMBOL3 = "?";
    public static final String SYMBOL4 = "=";
    public static final String SYMBOL5 = "&";

    //域名
    public static final String YUEMEI_DOMAIN_NAME = ".yuemei.com";
    public static final String BASE_URL = "sjapp" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_API_M_URL = "m" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_API_URL = "www" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_SEARCH_URL = "s" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_USER_URL = "user" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_NEWS_URL = "chat" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_SERVICE = "chats" + YUEMEI_DOMAIN_NAME;
    public static final String BASE_EMPTY = "empty" + YUEMEI_DOMAIN_NAME;

    //拼接好的主域名，带版本号，后边不带斜杠的 https://sjapp.yuemei.com/Vxxx
    public static final String BASE_VER_URL = HTTPS + SYMBOL1 + BASE_URL + SYMBOL2 + YUEMEI_VER;
    //拼接好的主域名，带版本号，后边带斜杠的 https://sjapp.yuemei.com/Vxxx/
    public static final String BASE_VER_URL_S = HTTPS + SYMBOL1 + BASE_URL + SYMBOL2 + YUEMEI_VER + SYMBOL2;

    //拼接好的主域名，不带版本号，后边不带斜杠的 https://sjapp.yuemei.com
    public static final String BASE_HTML_URL = HTTPS + SYMBOL1 + BASE_URL;
    //拼接好的主域名，不带版本号，后边带斜杠的 https://sjapp.yuemei.com/
    public static final String BASE_HTML_URL_S = HTTPS + SYMBOL1 + BASE_URL + SYMBOL2;

    //服务端地址
    public static final String API = "api";
    public static final String COUPNOS = "coupons";
    public static final String MESSAGE = "message";
    public static final String BOARD = "board";
    public static final String HOME_NEW = "homenew";
    public static final String FORUM = "forum";
    public static final String HOME = "home";
    public static final String SEARCH = "search";
    public static final String USER = "user";
    public static final String USERNEW = "usernew";
    public static final String INTEGRALTASK = "integraltask";
    public static final String ORDER = "order";
    public static final String SERVICE = "service";
    public static final String TAO = "tao";
    public static final String BBS = "bbs";
    public static final String HOSPITAL = "hospital";
    public static final String M_CITY = "city";
    public static final String BANKPAYNEW = "bankpaynew";
    public static final String CHAT = "chat";
    public static final String RONGYUN = "rongyun";
    public static final String HUAJIAO = "huajiao";
    public static final String DUIBA = "duiba";
    public static final String ACTIVE = "active";
    public static final String DOCTOR = "doctor";
    public static final String SHARE = "share";
    public static final String WALLET = "wallet";
    public static final String MEMBER = "member";
    public static final String CART = "cart";
    public static final String ALIPAY = "alipay";
    public static final String WXPAY = "wxpay";
    public static final String CHANNEL = "channel";
    public static final String BAIKE = "baike";
    public static final String COUPONS = "coupons";
    public static final String TAOPK= "taopk";
    public static final String AJAX= "ajax";


    public static void configInterface() {

        //首页数据 HomeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "home", POST, Home623.class);
        //新首页数据
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "homeIndex", POST);
        //淘详情页数据加载 LoadHomeDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "tao", POST, TaoData623.class);
        // 搜索文字显示接口 SearchShowDataApi
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "searchk", GET, SearchXSData.class);
        // 补填邀请码 AddInsCodeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, INTEGRALTASK, "writecode", GET, JFJY1.class);
        // 版本检测 VersionApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "versions", POST, VersionJCData.class);
        //导航页广告位 SplashApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "start", GET, Advert.class);
        // 项目首页 顶部接口 HomeProjectApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "parts", GET, ProjectHome550.class);
        // 消息数 MessagecountApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "messagecount", POST, NewsNumberData.class);
        // 收藏接口 post BaikeFourApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "collect", POST);
        // 获取用户数据接口 BBsDetailUserInfoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getuserinfo", POST, UserData.class);
        // 详情贴回复 SumbitHttpApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "replypost", POST);
        // 帖子详情页按钮处理接口 WebBottomApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "button", GET, BBsButtonData.class);
        // 判断帖子是否赞过接口 ZanApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getagreeport", GET, Status1Data.class);
        // 帖子点赞/举报接口更新接口 ZanOrJuBaoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "insertagree", POST);
        //返现返回数据 FanXianApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "cashback", GET, CashBack.class);
        // 颜值币领取接口 SumitHttpAip
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "integraltask", GET, JFJY1Data.class);
        // 删除帖子内容 /forum/delpostcontent/  DeleteBBsContentApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "delpostcontent", GET);
        // 删除帖子 或 回复 DeleteBBsApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "delpost", GET);
        // 收藏接口 post BaikeFourApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "collect", POST);
        // 取消收藏 CancelCollectApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "delcollect", POST);
        // 收藏判断接口 IsCollectApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "isjoin", GET);
        //帖子 图片接口 CheckPhotoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "postimglist", GET);
        // 领取代金劵 /id/优惠券id（变的时候告诉你）默认1/  DaiJinJuanApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, COUPNOS, "getcoupons", GET, DaiJinJuan.class);
        // 验证验证码 SecurityCodeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "setphoneverifycode", POST);
        // 淘整形是否支持下单 IsOrderApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "taobutton", GET, IfTaoOrderData.class);
        // 获取淘整形客服信息 KeFuApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, SERVICE, "getkfuinfo", GET, KeFuData.class);
        // 淘整形分享接口 ShareDataApiZt
        NetWork.getInstance().regist(HTTPS, BASE_USER_URL, SHARE, "ztshare", GET, TaoShareData.class);
        // 淘整形预定信息 YuDingApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "taoyuding", GET, YuDingData.class);
        //用户浏览SKU发送聊天消息接口 TagDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "sendrmsg", POST);
        // 淘整形详细页获取第一屏数据 TaoDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "tao", POST, TaoDetail591.class);
        // 添加手机号获取短信
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "setphone", POST);
        // 医生说下的专题 HomeTopDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "dochotzt", GET, ZhuanTi.class);
        //PartListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "parts", GET, TaoPopItemIvData.class);
        // 社区首页头部接口 HomeCommunityApi 未缓存
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "home", POST);
        // 案例最终页接口 CaseDetailApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "share", GET);
        //医生帖子列表DoctorDetailBBsApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "userpost", GET, BBsListData550.class);
        // 专家详情页接口 DoctorHeadMessageApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "userinfo", GET, DocHeadData.class);
        // 专家详情日记关联 RiJiListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "usershare", GET, BBsListData550.class);
        // 医生分享接口 ShareDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "docshare", GET, DocShareData.class);
        // 医生下的淘整形 TaoListDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "usertao", GET, HomeTaoData.class);
        // 医院详情页 预约 HosYuYueApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOSPITAL, "cooperation", GET, HosYuYueData.class);
        // 医院分享接口 HosDetailShareApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOSPITAL, "hosshare", GET, HosShareData.class);
        // 搜索联想词 SearchApi
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "searchkey", GET, SearchAboutData.class);
        // 城市列表 LoadCityApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME, "city", GET, MainCityData.class);
        //热门城市 HotCityApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, M_CITY, "hotcity", GET, CityDocDataitem.class);
        // 我想变美验证验证码 WantBeautifulSecurityCodeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "beautyverifycode", POST);
        //提问页 部位选择  PartDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "tagtree", GET, PartAskData.class);
        // 我想变美发送验证码 WantBeautySendSecurityCodeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "beautify", POST);
        // 验证验证码并打电话接口 CallAndSecurityCodeApi
        NetWork.getInstance().regist(HTTPS, BASE_USER_URL, USER, "codeverify", POST);
        // 获取扫描支付数据接口 SaoPayApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "saomapay", GET, SaozfData.class);
        //淘整形项目 筛选条件 FilterDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "getScreen", GET, ProjcetData.class);

        //搜索 LodHotIssueDataApi
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "index613", GET);

        // 补充更新获取数据接口  UpdateMypostDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "getpostinfo", GET, PostInfoData.class);
        // 吐槽接口 post     TuCaoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "suggest", GET);
        // 审核判断    ShenHeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "show", POST, ShenHeData.class);
        // 获取邀请码接口等  my/ShareDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getinvitation", GET, GetCodeData.class);
        // 手机号验证码验证登录 VerificationSecurityCodeApi
        //NetWork.getInstance().regist(HTTPS,BASE_URL,USER,"login",POST);
        // 注册接口 RegisterApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "reg", POST);
        // 发送验证码 SendSecurityApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "setpassverifycode", POST);
        //忘记密码 ForgetPasswordApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "setpass", POST);
        //选择日记本
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "diarylist", GET);
        // 第三方登录接口  LoginOtherHttpApi
        NetWork.getInstance().regist(HTTPS, BASE_USER_URL, HOME, "tposlogin", POST);
        //发送验证码 SendEMSApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "getcode", GET);
        // 登录接口 LoginApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "login", POST);
        // 获取城市列表 InitCityDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, M_CITY, "getarea", GET);
        // 取消订单 CancelOrderApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "cancel", GET);
        // 更改预约时间 ChageTimeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "changetime", GET);
        //修改用户发帖用户信息接口 GetPostingMessageApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "upuserfullname", POST);
        //圈子详情页头部信息 InitTopTitleApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "taginfo", GET);
        // 手机验证码提交 RegisterDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getphoneverifycode", POST);
        // 百度云推送绑定（解绑）接口 InitBindOpenApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "bdyunbbs", POST);
        // 打扰开关 DisturbApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "disturb", POST);
        //修改返现账号接口 SaveAiliMessageApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "updatealipay", POST);
        //签到页面接口 InitializeHttpApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, INTEGRALTASK, "signindata", GET);
        //视频播放器的数据接口 GetVideoInformationApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "videodata", POST);
        // 合作医生预约提交 YuyueSumbitApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "docorder", POST);
        // 申请退款接口 ApplyMoneyBackApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "applyrefund", GET);
        // 获取订单信息 InitOrderInfoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "getrefundorder", POST);
        //支付宝支付 PayALiPay
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, ALIPAY, "appindex.php", POST);
        // 微信支付 PayWeixinApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, WXPAY, "index599.php", POST);
        //银联支付 PayYinlianApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, BANKPAYNEW, "appindex599.php", POST);
        //花呗分期利率 InitHBFenqiApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "hbfeilv", GET);
        //
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "getorderinfo", GET);
        // 登录状态下提交验证码 CommitSecurityCodeInLoginApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "coderight", POST);
        // 获取保险信息 InsuranceMessageApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getinsure", GET);
        // 我的可用红包个数 InitYouHuiApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, COUPNOS, "usecoupons", POST);
        // 我的可用红包个数 InitYouHuiApi594
        NetWork.getInstance().regist(HTTPS, BASE_URL, COUPNOS, "couponsnum", POST);
        // 获取颜值币可抵金额 InitGetJifenApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "getpriceintegral", GET);
        // 登录情况下 获取用户手机号 GetUserPhoneNumberApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getuserphone", GET);
        // 淘整形下单的信息 InitOrderMessage
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "taoinfo", GET);
        // 扫描下单 请求接口 InitSaoOrderListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "docorder", POST);
        // 项目详情页的二级列表和四级列表 LoadTwoTreeListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "seltagtree", GET);
        // 扫描提交订单 SubmitPayOrderApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "docordersave", POST);
        // 图形验证码 SecuritycodeApi
        NetWork.getInstance().regist(HTTPS, BASE_USER_URL, USER, "getCaptcha", POST);
        // 限时淘 头部 InitHotCity
        NetWork.getInstance().regist(HTTPS, BASE_URL, M_CITY, "gethotcity", GET);
        // 淘整形项目 LoadCityDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "citys", GET);
        //选择日记标签 LoadLinkDataApi/other
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "hottag", GET);
        // 社区写日记标签选择接口 GetTagDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "diarytagtree", POST);
        //搜索接口 LoadResultTypeApi
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "index597", GET);
        //私信列表接口 XiaoxiChatListAPI
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "chatlist", POST, ChatListBean.class);
        //私信列表消息数修改i接口 XiaoxiChatUpdatereadApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "updateread", POST);
        //拼团分享数据获取 SpeltShareApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "groupshare", POST, SpeltShareData.class);
        //获取私信聊天数据ChatMessApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "getmessage", POST);
        //私信发送数据 ChatSendMessageApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "send", UPLOAD);
        //聊天界面数据 ChataInterfaceApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "index", POST);
        //分享接口
        NetWork.getInstance().regist(HTTPS, BASE_USER_URL, SHARE, "post", GET);
        // 城市列表 InitLoadCityApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, M_CITY, "city", GET);
        // 帖子分享接口
        NetWork.getInstance().regist(HTTPS, BASE_USER_URL, SHARE, "city", GET);
        // 手机号验证码验证登录 InitCode1Api
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "codelogin", POST);
        // 获取客服 融云信息 InitKefuApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, SERVICE, "kefu", POST);
        // 线下支付提醒 InitXianXiaPayApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "uppayid", GET);
        //获取融云组的信息 InitGroupRongIMApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, RONGYUN, "group.php", POST);
        // 项目详情页的二级列表和四级列表 LoadFourTreeDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "tagtree24", GET);
        // 获取rongyun Token GetRongYunTokenApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, RONGYUN, "gettoken.php", POST);
        //写日记弹层 NoteBookDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "alert", GET);
        // 写提问帖，写日记，随便聊聊 my/PostTextQueApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "share", POST);
        //淘详情页面统计 TongjiClickApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "taofunclickvisit", GET);
        // 统计安卓接口 GetInfoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "appreceive", POST);
        // 淘整形列表 LoadDataAapi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "taolist", POST);
        //定位开始 GetCityOneToHttpApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "position", POST);
        //加统计专题 ZTRecordHttpApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME, "ztrecord", GET);
        //花椒获取token GetHJTokenApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HUAJIAO, "huajiao.php", POST);
        //SendRongToYMApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, RONGYUN, "ym/route.php", POST);
        // 首页日记列表 home/LoadDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "sharelistnew", POST);
        //颜值币商城入口 登录 JiFenStroeApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, DUIBA, "duiba.php", GET);
        //日记筛选接口 other/LoadCityListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "sharecitypart", GET);
        // 报名接口LoadPartCityListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ACTIVE, "activitynew", GET);
        //全部项目接口 LoadProjectDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "boardtao", GET);
        //获取IMEI号，IESI号，手机型号 GetInfoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "appreceive", GET);
        //获取购物车数量 CartSkuNumberApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "getcartnumber", GET);
        //购物车颜值币获取
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "getyancoindeduction", GET);
        //医生列表DoctorListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "list", GET);
        //医院列表 HospitalListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOSPITAL, "list", GET);
        // 搜索医院淘整型和日记 LodHotDataApi
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "hossearch", GET);
        //限时淘 other/LodHotIssueDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "specical", GET);
        // 部位页 LodHotIssueDataApi2
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "index", GET);
        // 项目热门专题接口 home/LodHotIssueData
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "hotzt", GET);
        // 医生下的淘整形 doctor/LodHotIssueData
        NetWork.getInstance().regist(HTTPS, BASE_URL, USER, "usertao", GET);
        // 项目详情页 LodHomeTaoDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "boardtao", GET);
        // 搜索热词推荐 LodGroupDataApi
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "hotword", GET);
        // 首页、部位页、顶部部位接口 LodPart1DataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "parts", GET);
        // 讨论组列表一级默认接口 doctor/LodGroupDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "askorshare", GET);
        // 项目搜索 LodProjectHotApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "search", POST);
        // 医生列表顶部部位接口 home/LodPart1DataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME, "askorshare", GET);
        //社区页面列表HomeBBsApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "replylist", GET);
        //社区页面列表LoadHotDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "bbslist", GET);
        //限时淘 与 最新上架列表HotIssueApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "taolist", GET);
        //收藏的淘整形MyCollectTaoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mycollecttao", GET);
        //CollectBaikeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mycollectbaike", GET);
        //收藏的医院CollectHosApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mycollecthospital", GET);
        //收藏的医生CollectDocApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mycollectdoc", GET);
        //专家详情帖子关联DocDeBBsListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "userpost", GET);
        //专家详情日记关联DocDeRijiListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "usershare", GET);
        //我收藏的帖子CollectBBsApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mycollectpost", GET);
        //我的帖子接口MyPersonCenterApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mypost", POST);
        //小组下的帖子接口QuanziDetailApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "tagpost", GET);
        //大家都在聊QuanziDetailApi2
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "chat", GET);
        //医生说下的帖子DocTalkApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, DOCTOR, "doctorsay", GET);
        //社区日记精选 更多日记 LookRijiApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "postlist", GET);
        //增日记列表接口HomeDiarySXApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "sharelist", POST);
        //查看医院相册图片
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOSPITAL, "album", GET);
        //消费凭证/日记补充更新 ConsumVoucherApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "sharetwo", POST);
        //个人资料上传ModifyMyDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "setuser", UPLOAD);
        //判断是否关注IsFocuApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "isfollowing", POST);
        //关注取消关注FocusAndCancelApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "following", GET);
        //我的关注MyFocusApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "myfollowing", POST);
        //我的日记
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "mydiary", POST);
        //我的推荐
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "ucenterrectao", POST);
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "following", POST);
        //我的粉丝接口MyFansApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "followingme", POST);
        //日记本接口 DiaryListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "shareinfo", POST);
        //帖子详情接口 PostListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "postinfo", POST);
        //日记本列表接口DiaryListListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "diaryshare", POST);
        //日记本评论列表CommentsListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "postreplylist", POST);
        //日记本术后恢复相册
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "diaryimglist", POST);
        //恢复相册保存
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "upsharepic", POST);
        //极光推送绑定
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "jpushbind", POST);
        //极光推送解绑
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "jpushclose", POST);
        //日记大图
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "postimglist", POST);
        //图片浏览列表PhotoBrowsApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "postimglist", POST);
        //日记本列表美丽日记推荐列表DiaryRecommListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "otherdiary", POST);
        //获取七牛token QiNiuTokenApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "gettoken", POST);
        //钱包 YuemeiWalletApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, WALLET, "mywallet", POST);
        //提现 TixianApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, WALLET, "withdraw", POST);
        //提现获取验证码 TixianCodeApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, WALLET, "getcode", POST);
        //芝麻认证初始化
        NetWork.getInstance().regist(HTTPS, BASE_URL, WALLET, "realnameinit", POST);
        //芝麻认证认证成功接口
        NetWork.getInstance().regist(HTTPS, BASE_URL, WALLET, "realnameok", POST);
        //新增收货地址 SaveaddressApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "saveaddress", POST);
        //获取收货地址 GetaddressApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "getaddress", POST);
        //删除收货地址 DeladdressApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "deladdress", POST);
        //购买会员SaveorderiApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MEMBER, "saveorder", POST);
        //确认收货地址 MemberUsersureaddressApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MEMBER, "usersureaddress", POST);
        //私信领红包 ChatRedPacketApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "getcoupons", POST);
        //统计接口
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "gettjArr", POST);
        //上传统计接口
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "tongji", POST);
        //添加购物车  SkuAddcarApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "addtocart", POST);
        //获取购物车数据 ShoppingCartApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "getcart", POST);
        //删除购物车数据 ShoppingCartDeleteApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "delinvalidcart", POST);
        //购物车操作加减 ShoppingCartNumberApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "upcartnumber", POST);
        //购物车选中/不选中 ShoppingCartSelectedApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "selectedcart", POST);
        //购物车可使用订金券 ShoppingCartPreferentialApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "getcouponsbycart", POST);
        //下单页面获取数据MakeSureOrderApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "settlement", POST);
        //支付订单接口 PayOrderApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "saveorder", POST);
        //支付宝支付 PayALiPay
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, ALIPAY, "new/appindex.php", POST);
        // 微信支付 PayWeixinApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, WXPAY, "new/index.php", POST);
        //银联支付 PayYinlianApi
        NetWork.getInstance().regist(HTTPS, BASE_API_URL, BANKPAYNEW, "appindex599.php", POST);
        //花呗分期利率 InitHBFenqiApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, ORDER, "hbfeilv", GET);
        //重新加入购物车 AgainAddCarApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "orderaddtocart", POST);
        //计算退款金额 AmountRefundApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CART, "amountrefund", POST);
        //全部项目ChannelAllpartApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CHANNEL, "allpart", POST);
        //频道首页ProjectDetailsApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CHANNEL, "channel", POST);
        //频道首页标签获取ProjectDetailsTagApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CHANNEL, "getpartalllabel", POST);
        //私信页获取对比咨询医生信息 ChatBackApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, CHANNEL, "gettaorelationcomparedconsultative", POST);
        //选择问题类型接口TypeProblemApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "asktype", POST);
        //收集用户信息 CollectUserDataApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "collectuserdata", POST);
        //收集用户信息 TaoPushApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "settaopush", POST);
        //冷启动 ColdStartApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, HOME_NEW, "coldStart", POST);
        //百科四级详情页
        NetWork.getInstance().regist(HTTPS, BASE_URL, BAIKE, "otherList", POST);
        //视频播放数据VideoListApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "video", POST);
        //清楚消息
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "clearNoticeNum", POST);
        //自动私信
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "autosend", POST);
        //一键登录接口
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "phonelogin", POST);
        //看过的视频上送
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "lookVideo", POST);
        //提示语发送【执行发送我正在看之后请求】SendTipsApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "sendtips", POST);
        //获取淘列表活动数据返回
        NetWork.getInstance().regist(HTTPS, BASE_URL, BOARD, "getScreenBoard", POST);
        //Menu优惠券领取（多个）
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "receivecoupons", POST);
        //应用宝ocpa获取access_token
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "ocpa", POST);
        //自动私信
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "autosendtoymt", POST);
        //赞同收藏入库
        NetWork.getInstance().regist(HTTPS, BASE_URL, BBS, "agreeAndCollect", POST);
        //获取日记本同城其他日记列表
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "samecitydiarylist", POST);
        //获取支付成功页面弹层
        NetWork.getInstance().regist(HTTPS, BASE_URL, COUPONS, "getRecommendCouponsStatus", POST);
        //一键绑定
        NetWork.getInstance().regist(HTTPS, BASE_URL, USERNEW, "setloginphone", POST);
        //APP搜索入口词接口
        NetWork.getInstance().regist(HTTPS, BASE_SEARCH_URL, HOME, "searchEntry", POST);
        //搜索初始化面板
        NetWork.getInstance().regist(HTTPS, BASE_URL, SEARCH, "searchindex", POST);
        //搜索列表曝光接口
        NetWork.getInstance().regist(HTTPS, BASE_URL, MESSAGE, "searchtaolistshow", POST);
        //私信返回成功领奖数据
        NetWork.getInstance().regist(HTTPS, BASE_URL, CHAT, "chattips", POST);
        //获取频道页面的标签
        NetWork.getInstance().regist(HTTPS, BASE_URL, CHANNEL, "getPartAllLabel", POST);
        //我的对比页
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAOPK, "index", GET);
        //添加对比
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAOPK, "addtaopk", GET);
        //删除对比
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAOPK, "deltaopk", GET);
        //套整形筛选列表
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAOPK, "searchtao", GET);
        //websocket 推送正在输入消息给聊天对象 ChatOnFocusApi
        NetWork.getInstance().regist(HTTPS, BASE_NEWS_URL, CHAT, "onfocus", POST);
        //淘整形详情-用户足迹 UserBrowsetaoApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, TAO, "userbrowsetao", POST);
        //投票帖--投票动作
        NetWork.getInstance().regist(HTTPS, BASE_URL, FORUM, "newvotepost", GET);
        //HomeRefeshApi
        NetWork.getInstance().regist(HTTPS, BASE_URL, AJAX, "updateusershowcontrol", POST);
    }

}
