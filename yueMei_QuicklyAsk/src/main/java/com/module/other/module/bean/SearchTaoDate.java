package com.module.other.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.taodetail.model.bean.Promotion;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: SearchTaoDate
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-12 21:07
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class SearchTaoDate implements Parcelable {


    /**
     * tao_list : [{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/67427/","coupon_type":0,"is_show_member":"0","pay_dingjin":"320","number":"20","start_number":"1","pay_price_discount":"880","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220233654_62da14.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220233649_154ef7.jpg","title":"瘦脸针","subtitle":"衡力瘦脸针（热销近2万 无隐形消费）北医三院医学博士注射 超轻柔 超舒适","hos_name":"悦美好医医疗美容门诊部","doc_name":"张利民","price":"2800","price_discount":"880","price_range_max":"0","id":"67427","_id":"67427","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"19465人预订","rateNum":19465,"service":"5.0","feeScale":"/支(100单位)","is_fanxian":"1","hospital_id":"10647","doctor_id":"86345464","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/00/39/51_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/00/40/03_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/00/59/97_avatar_50_50.jpg"],"surgInfo":{"id":"329","title":"瘦脸","name":"瘦脸","alias":"","url_name":"faceslimm"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%98%A6%E8%84%B8%E9%92%88%E3%80%91%E8%A1%A1%E5%8A%9B%E7%98%A6%E8%84%B8%E9%92%88%EF%BC%88%E7%83%AD%E9%94%80%E8%BF%912%E4%B8%87%20%E6%97%A0%E9%9A%90%E5%BD%A2%E6%B6%88%E8%B4%B9%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E8%B6%85%E8%BD%BB%E6%9F%94%20%E8%B6%85%E8%88%92%E9%80%82%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":67427,"event_others":0,"event_pos":1,"id":0},"distance":"17.2km"},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/223888/","coupon_type":2,"is_show_member":"0","pay_dingjin":"1980","number":"10","start_number":"1","pay_price_discount":"9056","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1122/200_200/jt191122201430_675dae.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1122/jt191122201430_675dae.jpg","title":"吸脂塑形","subtitle":"30余年（双博士）团队14项专利多维立体吸脂减肥/专利面部吸脂瘦大腿吸脂瘦腰腹部","hos_name":"北京润美玉之光医疗美容门诊部","doc_name":"朱金成","price":"18000","price_discount":"9056","price_range_max":"0","id":"223888","_id":"223888","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"7130人预订","rateNum":7130,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"3354","doctor_id":"86373205","is_rongyun":"3","hos_userid":"85280507","business_district":"亚运村","hospital_top":{"level":0,"desc":"","sales":0},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/086/39/76/40_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/39/86/42_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/39/86/51_avatar_50_50.jpg"],"surgInfo":{"id":"641","title":"瘦腰腹","name":"瘦腰腹","alias":"瘦肚子、减啤酒肚","url_name":"abdominal"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E5%90%B8%E8%84%82%E5%A1%91%E5%BD%A2%E3%80%9130%E4%BD%99%E5%B9%B4%EF%BC%88%E5%8F%8C%E5%8D%9A%E5%A3%AB%EF%BC%89%E5%9B%A2%E9%98%9F14%E9%A1%B9%E4%B8%93%E5%88%A9%E5%A4%9A%E7%BB%B4%E7%AB%8B%E4%BD%93%E5%90%B8%E8%84%82%E5%87%8F%E8%82%A5%2F%E4%B8%93%E5%88%A9%E9%9D%A2%E9%83%A8%E5%90%B8%E8%84%82%E7%98%A6%E5%A4%A7%E8%85%BF%E5%90%B8%E8%84%82%E7%98%A6%E8%85%B0%E8%85%B9%E9%83%A8%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":223888,"event_others":0,"event_pos":2,"id":0},"distance":"12.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/104530/","coupon_type":0,"is_show_member":"1","pay_dingjin":"100","number":"20","start_number":"1","pay_price_discount":"388","member_price":"368","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0904/200_200/jt190904190518_ea9fad.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/0904/jt190904190518_ea9fad.jpg","title":"瘦脸针","subtitle":"私信送豪礼 瘦脸针 国产衡力瘦脸针超级特价超快瘦脸","hos_name":"北京蜜邦医疗美容诊所","doc_name":"夏毓琴","price":"1480","price_discount":"388","price_range_max":"0","id":"104530","_id":"104530","showprice":"1","specialPrice":"1","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"满10000减500","hos_red_packet":"满1000减50,满2000减100,满5000减300,满10000减500","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"1881人预订","rateNum":1881,"service":"5.0","feeScale":"/次","is_fanxian":"0","hospital_id":"12268","doctor_id":"88443439","is_rongyun":"3","hos_userid":"85652638","business_district":"亚运村","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://www.yuemei.com/images/weibo/noavatar2_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar1_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar3_50_50.jpg"],"surgInfo":{"id":"329","title":"瘦脸","name":"瘦脸","alias":"","url_name":"faceslimm"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%98%A6%E8%84%B8%E9%92%88%E3%80%91%E7%A7%81%E4%BF%A1%E9%80%81%E8%B1%AA%E7%A4%BC%20%E7%98%A6%E8%84%B8%E9%92%88%20%E5%9B%BD%E4%BA%A7%E8%A1%A1%E5%8A%9B%E7%98%A6%E8%84%B8%E9%92%88%E8%B6%85%E7%BA%A7%E7%89%B9%E4%BB%B7%E8%B6%85%E5%BF%AB%E7%98%A6%E8%84%B8%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":104530,"event_others":0,"event_pos":3,"id":0},"distance":"12.0km"},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/21517/","coupon_type":2,"is_show_member":"0","pay_dingjin":"2560","number":"20","start_number":"1","pay_price_discount":"11600","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191220232535_363.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232406_66bb06.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232355_bcf9e9.jpg","title":"鼻综合整形","subtitle":"鼻综合 硅胶假体+耳软骨综合隆鼻 翘挺自然 不惧揉捏 美若天生","hos_name":"悦美好医医疗美容门诊部","doc_name":"门智和","price":"48000","price_discount":"11600","price_range_max":"0","id":"21517","_id":"21517","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191220232426_170.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191220232519_891.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"4388人预订","rateNum":4388,"service":"4.9","feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"84987073","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":""},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/084/89/94/21_avatar_50_50.jpg","https://p21.yuemei.com/avatar/084/89/95/09_avatar_50_50.jpg","https://p21.yuemei.com/avatar/084/91/16/33_avatar_50_50.jpg"],"surgInfo":{"id":"8245","title":"鼻综合","name":"鼻综合","alias":"综合鼻整形","url_name":"bizonghe"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E9%BC%BB%E7%BB%BC%E5%90%88%E6%95%B4%E5%BD%A2%E3%80%91%E9%BC%BB%E7%BB%BC%E5%90%88%20%E7%A1%85%E8%83%B6%E5%81%87%E4%BD%93%2B%E8%80%B3%E8%BD%AF%E9%AA%A8%E7%BB%BC%E5%90%88%E9%9A%86%E9%BC%BB%20%E7%BF%98%E6%8C%BA%E8%87%AA%E7%84%B6%20%E4%B8%8D%E6%83%A7%E6%8F%89%E6%8D%8F%20%E7%BE%8E%E8%8B%A5%E5%A4%A9%E7%94%9F%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":21517,"event_others":0,"event_pos":4,"id":0},"distance":"17.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/41839/","coupon_type":0,"is_show_member":"1","pay_dingjin":"100","number":"20","start_number":"1","pay_price_discount":"488","member_price":"463","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1204/200_200/jt191204172753_9d0e0b.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1204/jt191204172753_9d0e0b.jpg","title":"注射微整","subtitle":"（陈小春来了）爱芙莱 1ml  爱芙莱尿酸 可拆验 足量原装","hos_name":"北京凯润婷医疗美容医院","doc_name":"赵洋","price":"2980","price_discount":"488","price_range_max":"0","id":"41839","_id":"41839","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"满10000减1000","hos_red_packet":"满100减20,满1000减100,满3000减200,满5000减400,满10000减1000","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"563人预订","rateNum":563,"service":"5.0","feeScale":"/次","is_fanxian":"0","hospital_id":"3345","doctor_id":"528213","is_rongyun":"3","hos_userid":"416885","business_district":"双井富力城","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/17/85/85_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/17/87/45_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/17/96/55_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B3%A8%E5%B0%84%E5%BE%AE%E6%95%B4%E3%80%91%EF%BC%88%E9%99%88%E5%B0%8F%E6%98%A5%E6%9D%A5%E4%BA%86%EF%BC%89%E7%88%B1%E8%8A%99%E8%8E%B1%201ml%20%20%E7%88%B1%E8%8A%99%E8%8E%B1%E5%B0%BF%E9%85%B8%20%E5%8F%AF%E6%8B%86%E9%AA%8C%20%E8%B6%B3%E9%87%8F%E5%8E%9F%E8%A3%85%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":41839,"event_others":0,"event_pos":5,"id":0},"distance":"2.1km"},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/60503/","coupon_type":2,"is_show_member":"0","pay_dingjin":"13600","number":"1","start_number":"1","pay_price_discount":"63000","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191220232535_363.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0122/200_200/jt190122153729_9f0e00.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/0122/jt190122153729_9f0e00.jpg","title":"自体脂肪隆胸","subtitle":"自体脂肪隆胸 煌家御用女医生团队 | 增大1-3罩杯以上 |  形体精雕S曲线","hos_name":"北京英煌医疗美容诊所","doc_name":"梁耀婵","price":"68800","price_discount":"63000","price_range_max":"0","id":"60503","_id":"60503","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191220232426_170.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191220232519_891.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"437人预订","rateNum":437,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"3453","doctor_id":"32443","is_rongyun":"3","hos_userid":"85279795","business_district":"西单","hospital_top":{"level":0,"desc":"","sales":0},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/31/33/43_avatar_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar3_50_50.jpg","https://p21.yuemei.com/avatar/085/31/79/59_avatar_50_50.jpg"],"surgInfo":{"id":"592","title":"隆胸","name":"隆胸","alias":"丰胸、丰乳、隆乳","url_name":"breastaug"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E8%87%AA%E4%BD%93%E8%84%82%E8%82%AA%E9%9A%86%E8%83%B8%E3%80%91%E8%87%AA%E4%BD%93%E8%84%82%E8%82%AA%E9%9A%86%E8%83%B8%20%E7%85%8C%E5%AE%B6%E5%BE%A1%E7%94%A8%E5%A5%B3%E5%8C%BB%E7%94%9F%E5%9B%A2%E9%98%9F%20%7C%20%E5%A2%9E%E5%A4%A71-3%E7%BD%A9%E6%9D%AF%E4%BB%A5%E4%B8%8A%20%7C%20%20%E5%BD%A2%E4%BD%93%E7%B2%BE%E9%9B%95S%E6%9B%B2%E7%BA%BF%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":60503,"event_others":0,"event_pos":6,"id":0},"distance":"9.1km"},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/235644/","coupon_type":2,"is_show_member":"0","pay_dingjin":"1360","number":"20","start_number":"1","pay_price_discount":"6150","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191220232535_363.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220235306_204368.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220235302_e367d4.jpg","title":"热拉提","subtitle":"热拉提全面部提升 除皱紧致提拉 悦美甄选皮肤专家 以色列原装进口热拉提","hos_name":"悦美好医医疗美容门诊部","doc_name":"徐莹莹","price":"12800","price_discount":"6150","price_range_max":"0","id":"235644","_id":"235644","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191220232426_170.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191220232519_891.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"6633人预订","rateNum":6633,"service":null,"feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"84819881","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/17/79/41_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/17/80/09_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/17/85/85_avatar_50_50.jpg"],"surgInfo":{"id":"8295","title":"紧致提升","name":"紧致提升","alias":"面部年轻化","url_name":"facefirming"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%83%AD%E6%8B%89%E6%8F%90%E3%80%91%E7%83%AD%E6%8B%89%E6%8F%90%E5%85%A8%E9%9D%A2%E9%83%A8%E6%8F%90%E5%8D%87%20%E9%99%A4%E7%9A%B1%E7%B4%A7%E8%87%B4%E6%8F%90%E6%8B%89%20%E6%82%A6%E7%BE%8E%E7%94%84%E9%80%89%E7%9A%AE%E8%82%A4%E4%B8%93%E5%AE%B6%20%E4%BB%A5%E8%89%B2%E5%88%97%E5%8E%9F%E8%A3%85%E8%BF%9B%E5%8F%A3%E7%83%AD%E6%8B%89%E6%8F%90%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":235644,"event_others":0,"event_pos":7,"id":0},"distance":"17.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/21291/","coupon_type":0,"is_show_member":"1","pay_dingjin":"140","number":"20","start_number":"1","pay_price_discount":"680","member_price":"646","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1204/200_200/jt191204155552_8ddc19.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1204/jt191204155552_8ddc19.jpg","title":"注射微整","subtitle":"（陈小春来了）月销10000+伊婉C   进口玻尿酸 可拆验 足量原装","hos_name":"北京凯润婷医疗美容医院","doc_name":"赵洋","price":"2980","price_discount":"680","price_range_max":"0","id":"21291","_id":"21291","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"满10000减1000","hos_red_packet":"满100减20,满1000减100,满3000减200,满5000减400,满10000减1000","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"11131人预订","rateNum":11131,"service":"4.8","feeScale":"/次","is_fanxian":"0","hospital_id":"3345","doctor_id":"528213","is_rongyun":"3","hos_userid":"416885","business_district":"双井富力城","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/60/93/57_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/61/31/13_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/61/35/72_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B3%A8%E5%B0%84%E5%BE%AE%E6%95%B4%E3%80%91%EF%BC%88%E9%99%88%E5%B0%8F%E6%98%A5%E6%9D%A5%E4%BA%86%EF%BC%89%E6%9C%88%E9%94%8010000%2B%E4%BC%8A%E5%A9%89C%20%20%20%E8%BF%9B%E5%8F%A3%E7%8E%BB%E5%B0%BF%E9%85%B8%20%E5%8F%AF%E6%8B%86%E9%AA%8C%20%E8%B6%B3%E9%87%8F%E5%8E%9F%E8%A3%85%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":21291,"event_others":0,"event_pos":8,"id":0},"distance":"2.1km"},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/213466/","coupon_type":2,"is_show_member":"0","pay_dingjin":"6560","number":"1","start_number":"1","pay_price_discount":"29999","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1118/200_200/jt191118185445_584695.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1118/jt191118185445_584695.jpg","title":"自体肋软骨隆鼻","subtitle":"★画美金字塔X+1鼻雕/原生半肋鼻综合@徐学东 订制混血范 高翘立挺 美若天生","hos_name":"北京画美医疗美容（原长虹整形）","doc_name":"徐学东","price":"68000","price_discount":"29999","price_range_max":"0","id":"213466","_id":"213466","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"502人预订","rateNum":502,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"3379","doctor_id":"499553","is_rongyun":"3","hos_userid":"85282559","business_district":"团结湖","hospital_top":{"level":0,"desc":""},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/000/68/16/89_avatar_50_50.jpg","https://p21.yuemei.com/avatar/000/70/10/41_avatar_50_50.jpg","https://p21.yuemei.com/avatar/000/70/45/75_avatar_50_50.jpg"],"surgInfo":{"id":"135","title":"隆鼻","name":"隆鼻","alias":"垫鼻子、隆鼻手术","url_name":"rhino"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E8%87%AA%E4%BD%93%E8%82%8B%E8%BD%AF%E9%AA%A8%E9%9A%86%E9%BC%BB%E3%80%91%E2%98%85%E7%94%BB%E7%BE%8E%E9%87%91%E5%AD%97%E5%A1%94X%2B1%E9%BC%BB%E9%9B%95%2F%E5%8E%9F%E7%94%9F%E5%8D%8A%E8%82%8B%E9%BC%BB%E7%BB%BC%E5%90%88%40%E5%BE%90%E5%AD%A6%E4%B8%9C%20%E8%AE%A2%E5%88%B6%E6%B7%B7%E8%A1%80%E8%8C%83%20%E9%AB%98%E7%BF%98%E7%AB%8B%E6%8C%BA%20%E7%BE%8E%E8%8B%A5%E5%A4%A9%E7%94%9F%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":213466,"event_others":0,"event_pos":9,"id":0},"distance":"4.7km"},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/19895/","coupon_type":2,"is_show_member":"0","pay_dingjin":"100","number":"2","start_number":"1","pay_price_discount":"239","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232254_ec199a.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232249_95f748.jpg","title":"除皱针","subtitle":"衡力除皱（热销13000+）北医三院医学博士注射 高纯度保证疗效 除皱性价比之王 现场可查验","hos_name":"悦美好医医疗美容门诊部","doc_name":"张利民","price":"1580","price_discount":"239","price_range_max":"0","id":"19895","_id":"19895","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"13521人预订","rateNum":13521,"service":"5.0","feeScale":"/部位","is_fanxian":"0","hospital_id":"10647","doctor_id":"86345464","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/086/04/38/77_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/04/69/10_avatar_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar3_50_50.jpg"],"surgInfo":{"id":"8863","title":"注射除皱","name":"注射除皱","alias":"","url_name":"roudusu"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E9%99%A4%E7%9A%B1%E9%92%88%E3%80%91%E8%A1%A1%E5%8A%9B%E9%99%A4%E7%9A%B1%EF%BC%88%E7%83%AD%E9%94%8013000%2B%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E9%AB%98%E7%BA%AF%E5%BA%A6%E4%BF%9D%E8%AF%81%E7%96%97%E6%95%88%20%E9%99%A4%E7%9A%B1%E6%80%A7%E4%BB%B7%E6%AF%94%E4%B9%8B%E7%8E%8B%20%E7%8E%B0%E5%9C%BA%E5%8F%AF%E6%9F%A5%E9%AA%8C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":19895,"event_others":0,"event_pos":10,"id":0},"distance":"17.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/68403/","coupon_type":2,"is_show_member":"0","pay_dingjin":"198","number":"20","start_number":"1","pay_price_discount":"128","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1204/200_200/jt191204163052_c91b2e.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1204/jt191204163052_c91b2e.jpg","title":"水光针","subtitle":"（陈小春来了）功效复合补水 足量3ml 欣菲聆水光针2ml+1mlVC+医用面膜","hos_name":"北京凯润婷医疗美容医院","doc_name":"赵洋","price":"3800","price_discount":"128","price_range_max":"0","id":"68403","_id":"68403","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"280人预订","rateNum":280,"service":"5.0","feeScale":"/次","is_fanxian":"0","hospital_id":"3345","doctor_id":"528213","is_rongyun":"3","hos_userid":"416885","business_district":"双井富力城","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://www.yuemei.com/images/weibo/noavatar1_50_50.jpg","https://p21.yuemei.com/avatar/086/23/71/79_avatar_50_50.jpg","https://p21.yuemei.com/avatar/086/24/14/21_avatar_50_50.jpg"],"surgInfo":{"id":"10615","title":"补水保湿","name":"补水保湿","alias":"补水保湿","url_name":"bsbs1"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B0%B4%E5%85%89%E9%92%88%E3%80%91%EF%BC%88%E9%99%88%E5%B0%8F%E6%98%A5%E6%9D%A5%E4%BA%86%EF%BC%89%E5%8A%9F%E6%95%88%E5%A4%8D%E5%90%88%E8%A1%A5%E6%B0%B4%20%E8%B6%B3%E9%87%8F3ml%20%E6%AC%A3%E8%8F%B2%E8%81%86%E6%B0%B4%E5%85%89%E9%92%882ml%2B1mlVC%2B%E5%8C%BB%E7%94%A8%E9%9D%A2%E8%86%9C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":68403,"event_others":0,"event_pos":11,"id":0},"distance":"2.1km"},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/67289/","coupon_type":2,"is_show_member":"0","pay_dingjin":"200","number":"1","start_number":"1","pay_price_discount":"480","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220233453_db36a8.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220233449_9ee6d7.jpg","title":"美国M22光子嫩肤","subtitle":"光子嫩肤（销量TOP1）M22王者之冠 美白嫩肤 祛黄祛暗 赠面膜","hos_name":"悦美好医医疗美容门诊部","doc_name":"","price":"3800","price_discount":"480","price_range_max":"0","id":"67289","_id":"67289","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"12134人预订","rateNum":12134,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/18/06/11_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/18/10/51_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/18/34/23_avatar_50_50.jpg"],"surgInfo":{"id":"542","title":"美白嫩肤","name":"美白嫩肤","alias":"美容美白、全身美白","url_name":"skinwhiten"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%BE%8E%E5%9B%BDM22%E5%85%89%E5%AD%90%E5%AB%A9%E8%82%A4%E3%80%91%E5%85%89%E5%AD%90%E5%AB%A9%E8%82%A4%EF%BC%88%E9%94%80%E9%87%8FTOP1%EF%BC%89M22%E7%8E%8B%E8%80%85%E4%B9%8B%E5%86%A0%20%E7%BE%8E%E7%99%BD%E5%AB%A9%E8%82%A4%20%E7%A5%9B%E9%BB%84%E7%A5%9B%E6%9A%97%20%E8%B5%A0%E9%9D%A2%E8%86%9C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":67289,"event_others":0,"event_pos":12,"id":0},"distance":"17.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/104059/","coupon_type":0,"is_show_member":"1","pay_dingjin":"140","number":"20","start_number":"1","pay_price_discount":"668","member_price":"634","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1217/200_200/jt191217112850_2d56dd.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1217/jt191217112850_2d56dd.jpg","title":"伊婉C玻尿酸","subtitle":"年终特惠 伊婉C玻尿酸1ml 正品保证进口韩国伊婉玻尿酸  强效塑型","hos_name":"北京蜜邦医疗美容诊所","doc_name":"夏毓琴","price":"1980","price_discount":"668","price_range_max":"0","id":"104059","_id":"104059","showprice":"1","specialPrice":"1","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"满10000减500","hos_red_packet":"满1000减50,满2000减100,满5000减300,满10000减500","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"2370人预订","rateNum":2370,"service":"5.0","feeScale":"/支(1ml)","is_fanxian":"0","hospital_id":"12268","doctor_id":"88443439","is_rongyun":"3","hos_userid":"85652638","business_district":"亚运村","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/56/33/88_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/58/41/24_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/58/41/81_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E4%BC%8A%E5%A9%89C%E7%8E%BB%E5%B0%BF%E9%85%B8%E3%80%91%E5%B9%B4%E7%BB%88%E7%89%B9%E6%83%A0%20%E4%BC%8A%E5%A9%89C%E7%8E%BB%E5%B0%BF%E9%85%B81ml%20%E6%AD%A3%E5%93%81%E4%BF%9D%E8%AF%81%E8%BF%9B%E5%8F%A3%E9%9F%A9%E5%9B%BD%E4%BC%8A%E5%A9%89%E7%8E%BB%E5%B0%BF%E9%85%B8%20%20%E5%BC%BA%E6%95%88%E5%A1%91%E5%9E%8B%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":104059,"event_others":0,"event_pos":13,"id":0},"distance":"12.0km"},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/67271/","coupon_type":2,"is_show_member":"0","pay_dingjin":"200","number":"1","start_number":"1","pay_price_discount":"480","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220233419_b7830e.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220233414_6ba1a1.jpg","title":"美白嫩肤","subtitle":"白瓷娃娃（销量TOP1）德国4D进口白瓷 美白嫩肤 提亮肤色 赠医用面膜","hos_name":"悦美好医医疗美容门诊部","doc_name":"","price":"3800","price_discount":"480","price_range_max":"0","id":"67271","_id":"67271","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"9964人预订","rateNum":9964,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/58/52/58_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/58/85/43_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/59/13/75_avatar_50_50.jpg"],"surgInfo":{"id":"542","title":"美白嫩肤","name":"美白嫩肤","alias":"美容美白、全身美白","url_name":"skinwhiten"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%BE%8E%E7%99%BD%E5%AB%A9%E8%82%A4%E3%80%91%E7%99%BD%E7%93%B7%E5%A8%83%E5%A8%83%EF%BC%88%E9%94%80%E9%87%8FTOP1%EF%BC%89%E5%BE%B7%E5%9B%BD4D%E8%BF%9B%E5%8F%A3%E7%99%BD%E7%93%B7%20%E7%BE%8E%E7%99%BD%E5%AB%A9%E8%82%A4%20%E6%8F%90%E4%BA%AE%E8%82%A4%E8%89%B2%20%E8%B5%A0%E5%8C%BB%E7%94%A8%E9%9D%A2%E8%86%9C%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":67271,"event_others":0,"event_pos":14,"id":0},"distance":"17.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/216823/","coupon_type":0,"is_show_member":"1","pay_dingjin":"99","number":"20","start_number":"1","pay_price_discount":"99","member_price":"94","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0708/200_200/jt190708172856_84f35b.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/0708/jt190708172856_84f35b.jpg","title":"激光点痣","subtitle":"激光点痣#美丽无需\u201c痣\u201d疑激光祛痣 无痛点痣 给你无暇美肌  专业医师团队","hos_name":"北京美莱医疗美容医院","doc_name":"戴琴","price":"199","price_discount":"99","price_range_max":"0","id":"216823","_id":"216823","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"410人预订","rateNum":410,"service":null,"feeScale":"/次","is_fanxian":"0","hospital_id":"12053","doctor_id":"88227922","is_rongyun":"3","hos_userid":"85382353","business_district":"朝外大街","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/56/32/98_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/56/33/52_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/56/33/88_avatar_50_50.jpg"],"surgInfo":{"id":"763","title":"去痣","name":"去痣","alias":"去痣","url_name":"nevus"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%BF%80%E5%85%89%E7%82%B9%E7%97%A3%E3%80%91%E6%BF%80%E5%85%89%E7%82%B9%E7%97%A3%23%E7%BE%8E%E4%B8%BD%E6%97%A0%E9%9C%80%E2%80%9C%E7%97%A3%E2%80%9D%E7%96%91%E6%BF%80%E5%85%89%E7%A5%9B%E7%97%A3%20%E6%97%A0%E7%97%9B%E7%82%B9%E7%97%A3%20%E7%BB%99%E4%BD%A0%E6%97%A0%E6%9A%87%E7%BE%8E%E8%82%8C%20%20%E4%B8%93%E4%B8%9A%E5%8C%BB%E5%B8%88%E5%9B%A2%E9%98%9F%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":216823,"event_others":0,"event_pos":15,"id":0},"distance":"5.0km"},{"sale_type":2,"appmurl":"https://m.yuemei.com/tao/216838/","coupon_type":2,"is_show_member":"0","pay_dingjin":"1090","number":"1","start_number":"1","pay_price_discount":"4940","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0901/200_200/jt190901143533_5061a8.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/0901/jt190901143533_5061a8.jpg","title":"吸脂瘦大腿","subtitle":"吸脂瘦身 水动力大腿环吸 拉长腿型 修饰腿型视觉身高180","hos_name":"北京英煌医疗美容诊所","doc_name":"袁杰","price":"19800","price_discount":"4940","price_range_max":"0","id":"216838","_id":"216838","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"2422人预订","rateNum":2422,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"3453","doctor_id":"87996571","is_rongyun":"3","hos_userid":"85279795","business_district":"西单","hospital_top":{"level":0,"desc":""},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/01/63/39_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/01/73/19_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/04/55/73_avatar_50_50.jpg"],"surgInfo":{"id":"691","title":"瘦大腿","name":"瘦大腿","alias":"减大腿","url_name":"thighfat"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E5%90%B8%E8%84%82%E7%98%A6%E5%A4%A7%E8%85%BF%E3%80%91%E5%90%B8%E8%84%82%E7%98%A6%E8%BA%AB%20%E6%B0%B4%E5%8A%A8%E5%8A%9B%E5%A4%A7%E8%85%BF%E7%8E%AF%E5%90%B8%20%E6%8B%89%E9%95%BF%E8%85%BF%E5%9E%8B%20%E4%BF%AE%E9%A5%B0%E8%85%BF%E5%9E%8B%E8%A7%86%E8%A7%89%E8%BA%AB%E9%AB%98180%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":216838,"event_others":0,"event_pos":16,"id":0},"distance":"9.1km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/37817/","coupon_type":0,"is_show_member":"0","pay_dingjin":"400","number":"20","start_number":"1","pay_price_discount":"790","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232709_d090a9.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232705_996e88.jpg","title":"注射微整","subtitle":"伊婉C玻尿酸（热销13000+）北医三院医学博士注射 轻柔舒适 填苹果肌/法令纹/面颊/额头","hos_name":"悦美好医医疗美容门诊部","doc_name":"张利民","price":"3980","price_discount":"790","price_range_max":"0","id":"37817","_id":"37817","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"13272人预订","rateNum":13272,"service":"4.9","feeScale":"/支(1ml)","is_fanxian":"1","hospital_id":"10647","doctor_id":"86345464","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/34/06/35_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/34/34/79_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/34/35/31_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E6%B3%A8%E5%B0%84%E5%BE%AE%E6%95%B4%E3%80%91%E4%BC%8A%E5%A9%89C%E7%8E%BB%E5%B0%BF%E9%85%B8%EF%BC%88%E7%83%AD%E9%94%8013000%2B%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E8%BD%BB%E6%9F%94%E8%88%92%E9%80%82%20%E5%A1%AB%E8%8B%B9%E6%9E%9C%E8%82%8C%2F%E6%B3%95%E4%BB%A4%E7%BA%B9%2F%E9%9D%A2%E9%A2%8A%2F%E9%A2%9D%E5%A4%B4%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":37817,"event_others":0,"event_pos":17,"id":0},"distance":"17.2km"},{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/64579/","coupon_type":0,"is_show_member":"0","pay_dingjin":"6560","number":"11","start_number":"1","pay_price_discount":"32800","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1030/200_200/jt191030105701_d76570.png","img360":"https://p24.yuemei.com/tao/360_360/2019/1030/jt191030105701_d76570.png","title":"胸部整形","subtitle":"一家专注做胸的医院 | 3000丰胸编号案例随意看 | 专家经验均在15年以上","hos_name":"北京英煌医疗美容诊所","doc_name":"项力源","price":"39800","price_discount":"32800","price_range_max":"0","id":"64579","_id":"64579","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"892人预订","rateNum":892,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"3453","doctor_id":"85347779","is_rongyun":"3","hos_userid":"85279795","business_district":"西单","hospital_top":{"level":0,"desc":"","sales":0},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/38/68/13_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/38/68/29_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/38/70/61_avatar_50_50.jpg"],"surgInfo":{"id":"592","title":"隆胸","name":"隆胸","alias":"丰胸、丰乳、隆乳","url_name":"breastaug"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E8%83%B8%E9%83%A8%E6%95%B4%E5%BD%A2%E3%80%91%E4%B8%80%E5%AE%B6%E4%B8%93%E6%B3%A8%E5%81%9A%E8%83%B8%E7%9A%84%E5%8C%BB%E9%99%A2%20%7C%203000%E4%B8%B0%E8%83%B8%E7%BC%96%E5%8F%B7%E6%A1%88%E4%BE%8B%E9%9A%8F%E6%84%8F%E7%9C%8B%20%7C%20%E4%B8%93%E5%AE%B6%E7%BB%8F%E9%AA%8C%E5%9D%87%E5%9C%A815%E5%B9%B4%E4%BB%A5%E4%B8%8A%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":64579,"event_others":0,"event_pos":18,"id":0},"distance":"9.1km"},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/171001/","coupon_type":2,"is_show_member":"0","pay_dingjin":"3960","number":"20","start_number":"1","pay_price_discount":"18000","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191220232535_363.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220234544_88fdeb.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220234540_475509.jpg","title":"半肋鼻综合","subtitle":"鼻综合整形 悦美甄选好医门智和 国产硅胶+肋鼻尖 3S美学个性化定制 真实自然 不惧揉捏","hos_name":"悦美好医医疗美容门诊部","doc_name":"门智和","price":"68000","price_discount":"18000","price_range_max":"0","id":"171001","_id":"171001","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191220232426_170.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191220232519_891.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"3904人预订","rateNum":3904,"service":"5.0","feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"84987073","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":"","sales":1},"is_have_video":"0","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/09/76/11_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/09/76/23_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/09/78/07_avatar_50_50.jpg"],"surgInfo":{"id":"135","title":"隆鼻","name":"隆鼻","alias":"垫鼻子、隆鼻手术","url_name":"rhino"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E5%8D%8A%E8%82%8B%E9%BC%BB%E7%BB%BC%E5%90%88%E3%80%91%E9%BC%BB%E7%BB%BC%E5%90%88%E6%95%B4%E5%BD%A2%20%E6%82%A6%E7%BE%8E%E7%94%84%E9%80%89%E5%A5%BD%E5%8C%BB%E9%97%A8%E6%99%BA%E5%92%8C%20%E5%9B%BD%E4%BA%A7%E7%A1%85%E8%83%B6%2B%E8%82%8B%E9%BC%BB%E5%B0%96%203S%E7%BE%8E%E5%AD%A6%E4%B8%AA%E6%80%A7%E5%8C%96%E5%AE%9A%E5%88%B6%20%E7%9C%9F%E5%AE%9E%E8%87%AA%E7%84%B6%20%E4%B8%8D%E6%83%A7%E6%8F%89%E6%8D%8F%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":171001,"event_others":0,"event_pos":19,"id":0},"distance":"17.2km"},{"sale_type":1,"appmurl":"https://m.yuemei.com/tao/29789/","coupon_type":2,"is_show_member":"0","pay_dingjin":"1960","number":"20","start_number":"1","pay_price_discount":"9000","member_price":"-1","m_list_logo":"https://p11.yuemei.com/taobigpromotion/20191220232535_363.png","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1220/200_200/191220232604_c38f62.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1220/191220232553_14a1ed.jpg","title":"自体脂肪丰胸","subtitle":"超纳米自体脂肪丰胸 悦美脂肪人气TOP1医生@门智和 吸脂+丰胸 5大脂肪升级技术","hos_name":"悦美好医医疗美容门诊部","doc_name":"门智和","price":"49800","price_discount":"9000","price_range_max":"0","id":"29789","_id":"29789","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"https://p11.yuemei.com/taobigpromotion/20191220232426_170.png","app_slide_logo":"https://p11.yuemei.com/taobigpromotion/20191220232519_891.png","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"6160人预订","rateNum":6160,"service":"4.9","feeScale":"/次","is_fanxian":"1","hospital_id":"10647","doctor_id":"84987073","is_rongyun":"3","hos_userid":"700893","business_district":"万柳","hospital_top":{"level":0,"desc":""},"is_have_video":"1","promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/01/55/67_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/01/56/25_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/01/63/15_avatar_50_50.jpg"],"surgInfo":{"id":"592","title":"隆胸","name":"隆胸","alias":"丰胸、丰乳、隆乳","url_name":"breastaug"},"highlight_title":"%3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E8%87%AA%E4%BD%93%E8%84%82%E8%82%AA%E4%B8%B0%E8%83%B8%E3%80%91%E8%B6%85%E7%BA%B3%E7%B1%B3%E8%87%AA%E4%BD%93%E8%84%82%E8%82%AA%E4%B8%B0%E8%83%B8%20%E6%82%A6%E7%BE%8E%E8%84%82%E8%82%AA%E4%BA%BA%E6%B0%94TOP1%E5%8C%BB%E7%94%9F%40%E9%97%A8%E6%99%BA%E5%92%8C%20%E5%90%B8%E8%84%82%2B%E4%B8%B0%E8%83%B8%205%E5%A4%A7%E8%84%82%E8%82%AA%E5%8D%87%E7%BA%A7%E6%8A%80%E6%9C%AF%3C%2Fspan%3E","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":29789,"event_others":0,"event_pos":20,"id":0},"distance":"17.2km"}]
     * last_request_time : 1577026736
     */

    private int last_request_time;
    private List<TaoListBean> tao_list;
    private String is_has_more;

    public String getIs_has_more() {
        return is_has_more;
    }

    public void setIs_has_more(String is_has_more) {
        this.is_has_more = is_has_more;
    }

    public int getLast_request_time() {
        return last_request_time;
    }

    public void setLast_request_time(int last_request_time) {
        this.last_request_time = last_request_time;
    }

    public List<TaoListBean> getTao_list() {
        return tao_list;
    }

    public void setTao_list(List<TaoListBean> tao_list) {
        this.tao_list = tao_list;
    }

    public static class TaoListBean implements Parcelable {
        /**
         * sale_type : 0
         * appmurl : https://m.yuemei.com/tao/67427/
         * coupon_type : 0
         * is_show_member : 0
         * pay_dingjin : 320
         * number : 20
         * start_number : 1
         * pay_price_discount : 880
         * member_price : -1
         * m_list_logo :
         * bmsid : 0
         * seckilling : 0
         * img : https://p24.yuemei.com/tao/2019/1220/200_200/191220233654_62da14.jpg
         * img360 : https://p24.yuemei.com/tao/360_360/2019/1220/191220233649_154ef7.jpg
         * title : 瘦脸针
         * subtitle : 衡力瘦脸针（热销近2万 无隐形消费）北医三院医学博士注射 超轻柔 超舒适
         * hos_name : 悦美好医医疗美容门诊部
         * doc_name : 张利民
         * price : 2800
         * price_discount : 880
         * price_range_max : 0
         * id : 67427
         * _id : 67427
         * showprice : 1
         * specialPrice : 0
         * show_hospital : 1
         * invitation : 0
         * lijian : 0
         * baoxian :
         * insure : {"is_insure":"0","insure_pay_money":"0","title":""}
         * img66 :
         * app_slide_logo :
         * repayment : 最高可享12期分期付款：花呗分期
         * bilateral_coupons :
         * hos_red_packet :
         * mingyi : 0
         * hot : 0
         * newp : 0
         * shixiao : 0
         * extension_user :
         * postStr :
         * depreciate :
         * rate : 19465人预订
         * rateNum : 19465
         * service : 5.0
         * feeScale : /支(100单位)
         * is_fanxian : 1
         * hospital_id : 10647
         * doctor_id : 86345464
         * is_rongyun : 3
         * hos_userid : 700893
         * business_district : 万柳
         * hospital_top : {"level":0,"desc":"","sales":1}
         * is_have_video : 1
         * promotion : []
         * userImg : ["https://p21.yuemei.com/avatar/085/00/39/51_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/00/40/03_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/00/59/97_avatar_50_50.jpg"]
         * surgInfo : {"id":"329","title":"瘦脸","name":"瘦脸","alias":"","url_name":"faceslimm"}
         * highlight_title : %3Cspan%20style%3D%22color%3A%20%23333333%22%3E%E3%80%90%E7%98%A6%E8%84%B8%E9%92%88%E3%80%91%E8%A1%A1%E5%8A%9B%E7%98%A6%E8%84%B8%E9%92%88%EF%BC%88%E7%83%AD%E9%94%80%E8%BF%912%E4%B8%87%20%E6%97%A0%E9%9A%90%E5%BD%A2%E6%B6%88%E8%B4%B9%EF%BC%89%E5%8C%97%E5%8C%BB%E4%B8%89%E9%99%A2%E5%8C%BB%E5%AD%A6%E5%8D%9A%E5%A3%AB%E6%B3%A8%E5%B0%84%20%E8%B6%85%E8%BD%BB%E6%9F%94%20%E8%B6%85%E8%88%92%E9%80%82%3C%2Fspan%3E
         * videoTaoTitle :
         * event_params : {"to_page_type":2,"to_page_id":67427,"event_others":0,"event_pos":1,"id":0}
         * distance : 17.2km
         */

        private int sale_type;
        private String appmurl;
        private int coupon_type;
        private String is_show_member;
        private String pay_dingjin;
        private String number;
        private String start_number;
        private String pay_price_discount;
        private String member_price;
        private String m_list_logo;
        private String bmsid;
        private String seckilling;
        private String img;
        private String img360;
        private String title;
        private String subtitle;
        private String hos_name;
        private String doc_name;
        private String price;
        private String price_discount;
        private String price_range_max;
        private String id;
        private String _id;
        private String showprice;
        private String specialPrice;
        private String show_hospital;
        private String invitation;
        private String lijian;
        private String baoxian;
        private InsureBean insure;
        private String img66;
        private String app_slide_logo;
        private String repayment;
        private String bilateral_coupons;
        private String hos_red_packet;
        private String mingyi;
        private String hot;
        private String newp;
        private String shixiao;
        private String extension_user;
        private String postStr;
        private String depreciate;
        private String rate;
        private int rateNum;
        private String service;
        private String feeScale;
        private String is_fanxian;
        private String hospital_id;
        private String doctor_id;
        private String is_rongyun;
        private String hos_userid;
        private String business_district;
        private HospitalTopBean hospital_top;
        private String is_have_video;
        private SurgInfoBean surgInfo;
        private String highlight_title;
        private String videoTaoTitle;
        private EventParamsBean event_params;
        private String distance;
        private List<?> promotion;
        private List<String> userImg;

        public int getSale_type() {
            return sale_type;
        }

        public void setSale_type(int sale_type) {
            this.sale_type = sale_type;
        }

        public String getAppmurl() {
            return appmurl;
        }

        public void setAppmurl(String appmurl) {
            this.appmurl = appmurl;
        }

        public int getCoupon_type() {
            return coupon_type;
        }

        public void setCoupon_type(int coupon_type) {
            this.coupon_type = coupon_type;
        }

        public String getIs_show_member() {
            return is_show_member;
        }

        public void setIs_show_member(String is_show_member) {
            this.is_show_member = is_show_member;
        }

        public String getPay_dingjin() {
            return pay_dingjin;
        }

        public void setPay_dingjin(String pay_dingjin) {
            this.pay_dingjin = pay_dingjin;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStart_number() {
            return start_number;
        }

        public void setStart_number(String start_number) {
            this.start_number = start_number;
        }

        public String getPay_price_discount() {
            return pay_price_discount;
        }

        public void setPay_price_discount(String pay_price_discount) {
            this.pay_price_discount = pay_price_discount;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }

        public String getM_list_logo() {
            return m_list_logo;
        }

        public void setM_list_logo(String m_list_logo) {
            this.m_list_logo = m_list_logo;
        }

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getSeckilling() {
            return seckilling;
        }

        public void setSeckilling(String seckilling) {
            this.seckilling = seckilling;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg360() {
            return img360;
        }

        public void setImg360(String img360) {
            this.img360 = img360;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getHos_name() {
            return hos_name;
        }

        public void setHos_name(String hos_name) {
            this.hos_name = hos_name;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice_range_max() {
            return price_range_max;
        }

        public void setPrice_range_max(String price_range_max) {
            this.price_range_max = price_range_max;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getShowprice() {
            return showprice;
        }

        public void setShowprice(String showprice) {
            this.showprice = showprice;
        }

        public String getSpecialPrice() {
            return specialPrice;
        }

        public void setSpecialPrice(String specialPrice) {
            this.specialPrice = specialPrice;
        }

        public String getShow_hospital() {
            return show_hospital;
        }

        public void setShow_hospital(String show_hospital) {
            this.show_hospital = show_hospital;
        }

        public String getInvitation() {
            return invitation;
        }

        public void setInvitation(String invitation) {
            this.invitation = invitation;
        }

        public String getLijian() {
            return lijian;
        }

        public void setLijian(String lijian) {
            this.lijian = lijian;
        }

        public String getBaoxian() {
            return baoxian;
        }

        public void setBaoxian(String baoxian) {
            this.baoxian = baoxian;
        }

        public InsureBean getInsure() {
            return insure;
        }

        public void setInsure(InsureBean insure) {
            this.insure = insure;
        }

        public String getImg66() {
            return img66;
        }

        public void setImg66(String img66) {
            this.img66 = img66;
        }

        public String getApp_slide_logo() {
            return app_slide_logo;
        }

        public void setApp_slide_logo(String app_slide_logo) {
            this.app_slide_logo = app_slide_logo;
        }

        public String getRepayment() {
            return repayment;
        }

        public void setRepayment(String repayment) {
            this.repayment = repayment;
        }

        public String getBilateral_coupons() {
            return bilateral_coupons;
        }

        public void setBilateral_coupons(String bilateral_coupons) {
            this.bilateral_coupons = bilateral_coupons;
        }

        public String getHos_red_packet() {
            return hos_red_packet;
        }

        public void setHos_red_packet(String hos_red_packet) {
            this.hos_red_packet = hos_red_packet;
        }

        public String getMingyi() {
            return mingyi;
        }

        public void setMingyi(String mingyi) {
            this.mingyi = mingyi;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getNewp() {
            return newp;
        }

        public void setNewp(String newp) {
            this.newp = newp;
        }

        public String getShixiao() {
            return shixiao;
        }

        public void setShixiao(String shixiao) {
            this.shixiao = shixiao;
        }

        public String getExtension_user() {
            return extension_user;
        }

        public void setExtension_user(String extension_user) {
            this.extension_user = extension_user;
        }

        public String getPostStr() {
            return postStr;
        }

        public void setPostStr(String postStr) {
            this.postStr = postStr;
        }

        public String getDepreciate() {
            return depreciate;
        }

        public void setDepreciate(String depreciate) {
            this.depreciate = depreciate;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public int getRateNum() {
            return rateNum;
        }

        public void setRateNum(int rateNum) {
            this.rateNum = rateNum;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getFeeScale() {
            return feeScale;
        }

        public void setFeeScale(String feeScale) {
            this.feeScale = feeScale;
        }

        public String getIs_fanxian() {
            return is_fanxian;
        }

        public void setIs_fanxian(String is_fanxian) {
            this.is_fanxian = is_fanxian;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getBusiness_district() {
            return business_district;
        }

        public void setBusiness_district(String business_district) {
            this.business_district = business_district;
        }

        public HospitalTopBean getHospital_top() {
            return hospital_top;
        }

        public void setHospital_top(HospitalTopBean hospital_top) {
            this.hospital_top = hospital_top;
        }

        public String getIs_have_video() {
            return is_have_video;
        }

        public void setIs_have_video(String is_have_video) {
            this.is_have_video = is_have_video;
        }

        public SurgInfoBean getSurgInfo() {
            return surgInfo;
        }

        public void setSurgInfo(SurgInfoBean surgInfo) {
            this.surgInfo = surgInfo;
        }

        public String getHighlight_title() {
            return highlight_title;
        }

        public void setHighlight_title(String highlight_title) {
            this.highlight_title = highlight_title;
        }

        public String getVideoTaoTitle() {
            return videoTaoTitle;
        }

        public void setVideoTaoTitle(String videoTaoTitle) {
            this.videoTaoTitle = videoTaoTitle;
        }

        public EventParamsBean getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBean event_params) {
            this.event_params = event_params;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public List<?> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<?> promotion) {
            this.promotion = promotion;
        }

        public List<String> getUserImg() {
            return userImg;
        }

        public void setUserImg(List<String> userImg) {
            this.userImg = userImg;
        }

        public static class InsureBean implements Parcelable {
            /**
             * is_insure : 0
             * insure_pay_money : 0
             * title :
             */

            private String is_insure;
            private String insure_pay_money;
            private String title;

            public String getIs_insure() {
                return is_insure;
            }

            public void setIs_insure(String is_insure) {
                this.is_insure = is_insure;
            }

            public String getInsure_pay_money() {
                return insure_pay_money;
            }

            public void setInsure_pay_money(String insure_pay_money) {
                this.insure_pay_money = insure_pay_money;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.is_insure);
                dest.writeString(this.insure_pay_money);
                dest.writeString(this.title);
            }

            public InsureBean() {
            }

            protected InsureBean(Parcel in) {
                this.is_insure = in.readString();
                this.insure_pay_money = in.readString();
                this.title = in.readString();
            }

            public static final Creator<InsureBean> CREATOR = new Creator<InsureBean>() {
                @Override
                public InsureBean createFromParcel(Parcel source) {
                    return new InsureBean(source);
                }

                @Override
                public InsureBean[] newArray(int size) {
                    return new InsureBean[size];
                }
            };
        }

        public static class HospitalTopBean implements Parcelable {
            /**
             * level : 0
             * desc :
             * sales : 1
             */

            private int level;
            private String desc;
            private int sales;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public int getSales() {
                return sales;
            }

            public void setSales(int sales) {
                this.sales = sales;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.level);
                dest.writeString(this.desc);
                dest.writeInt(this.sales);
            }

            public HospitalTopBean() {
            }

            protected HospitalTopBean(Parcel in) {
                this.level = in.readInt();
                this.desc = in.readString();
                this.sales = in.readInt();
            }

            public static final Creator<HospitalTopBean> CREATOR = new Creator<HospitalTopBean>() {
                @Override
                public HospitalTopBean createFromParcel(Parcel source) {
                    return new HospitalTopBean(source);
                }

                @Override
                public HospitalTopBean[] newArray(int size) {
                    return new HospitalTopBean[size];
                }
            };
        }

        public static class SurgInfoBean implements Parcelable {
            /**
             * id : 329
             * title : 瘦脸
             * name : 瘦脸
             * alias :
             * url_name : faceslimm
             */

            private String id;
            private String title;
            private String name;
            private String alias;
            private String url_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAlias() {
                return alias;
            }

            public void setAlias(String alias) {
                this.alias = alias;
            }

            public String getUrl_name() {
                return url_name;
            }

            public void setUrl_name(String url_name) {
                this.url_name = url_name;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.id);
                dest.writeString(this.title);
                dest.writeString(this.name);
                dest.writeString(this.alias);
                dest.writeString(this.url_name);
            }

            public SurgInfoBean() {
            }

            protected SurgInfoBean(Parcel in) {
                this.id = in.readString();
                this.title = in.readString();
                this.name = in.readString();
                this.alias = in.readString();
                this.url_name = in.readString();
            }

            public static final Creator<SurgInfoBean> CREATOR = new Creator<SurgInfoBean>() {
                @Override
                public SurgInfoBean createFromParcel(Parcel source) {
                    return new SurgInfoBean(source);
                }

                @Override
                public SurgInfoBean[] newArray(int size) {
                    return new SurgInfoBean[size];
                }
            };
        }

        public static class EventParamsBean implements Parcelable {
            /**
             * to_page_type : 2
             * to_page_id : 67427
             * event_others : 0
             * event_pos : 1
             * id : 0
             */

            private int to_page_type;
            private int to_page_id;
            private int event_others;
            private int event_pos;
            private int id;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }

            public int getEvent_others() {
                return event_others;
            }

            public void setEvent_others(int event_others) {
                this.event_others = event_others;
            }

            public int getEvent_pos() {
                return event_pos;
            }

            public void setEvent_pos(int event_pos) {
                this.event_pos = event_pos;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.to_page_type);
                dest.writeInt(this.to_page_id);
                dest.writeInt(this.event_others);
                dest.writeInt(this.event_pos);
                dest.writeInt(this.id);
            }

            public EventParamsBean() {
            }

            protected EventParamsBean(Parcel in) {
                this.to_page_type = in.readInt();
                this.to_page_id = in.readInt();
                this.event_others = in.readInt();
                this.event_pos = in.readInt();
                this.id = in.readInt();
            }

            public static final Creator<EventParamsBean> CREATOR = new Creator<EventParamsBean>() {
                @Override
                public EventParamsBean createFromParcel(Parcel source) {
                    return new EventParamsBean(source);
                }

                @Override
                public EventParamsBean[] newArray(int size) {
                    return new EventParamsBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.sale_type);
            dest.writeString(this.appmurl);
            dest.writeInt(this.coupon_type);
            dest.writeString(this.is_show_member);
            dest.writeString(this.pay_dingjin);
            dest.writeString(this.number);
            dest.writeString(this.start_number);
            dest.writeString(this.pay_price_discount);
            dest.writeString(this.member_price);
            dest.writeString(this.m_list_logo);
            dest.writeString(this.bmsid);
            dest.writeString(this.seckilling);
            dest.writeString(this.img);
            dest.writeString(this.img360);
            dest.writeString(this.title);
            dest.writeString(this.subtitle);
            dest.writeString(this.hos_name);
            dest.writeString(this.doc_name);
            dest.writeString(this.price);
            dest.writeString(this.price_discount);
            dest.writeString(this.price_range_max);
            dest.writeString(this.id);
            dest.writeString(this._id);
            dest.writeString(this.showprice);
            dest.writeString(this.specialPrice);
            dest.writeString(this.show_hospital);
            dest.writeString(this.invitation);
            dest.writeString(this.lijian);
            dest.writeString(this.baoxian);
            dest.writeParcelable(this.insure, flags);
            dest.writeString(this.img66);
            dest.writeString(this.app_slide_logo);
            dest.writeString(this.repayment);
            dest.writeString(this.bilateral_coupons);
            dest.writeString(this.hos_red_packet);
            dest.writeString(this.mingyi);
            dest.writeString(this.hot);
            dest.writeString(this.newp);
            dest.writeString(this.shixiao);
            dest.writeString(this.extension_user);
            dest.writeString(this.postStr);
            dest.writeString(this.depreciate);
            dest.writeString(this.rate);
            dest.writeInt(this.rateNum);
            dest.writeString(this.service);
            dest.writeString(this.feeScale);
            dest.writeString(this.is_fanxian);
            dest.writeString(this.hospital_id);
            dest.writeString(this.doctor_id);
            dest.writeString(this.is_rongyun);
            dest.writeString(this.hos_userid);
            dest.writeString(this.business_district);
            dest.writeParcelable(this.hospital_top, flags);
            dest.writeString(this.is_have_video);
            dest.writeParcelable(this.surgInfo, flags);
            dest.writeString(this.highlight_title);
            dest.writeString(this.videoTaoTitle);
            dest.writeParcelable(this.event_params, flags);
            dest.writeString(this.distance);
//            dest.writeTypedList(this.promotion);
            dest.writeStringList(this.userImg);
        }

        public TaoListBean() {
        }

        protected TaoListBean(Parcel in) {
            this.sale_type = in.readInt();
            this.appmurl = in.readString();
            this.coupon_type = in.readInt();
            this.is_show_member = in.readString();
            this.pay_dingjin = in.readString();
            this.number = in.readString();
            this.start_number = in.readString();
            this.pay_price_discount = in.readString();
            this.member_price = in.readString();
            this.m_list_logo = in.readString();
            this.bmsid = in.readString();
            this.seckilling = in.readString();
            this.img = in.readString();
            this.img360 = in.readString();
            this.title = in.readString();
            this.subtitle = in.readString();
            this.hos_name = in.readString();
            this.doc_name = in.readString();
            this.price = in.readString();
            this.price_discount = in.readString();
            this.price_range_max = in.readString();
            this.id = in.readString();
            this._id = in.readString();
            this.showprice = in.readString();
            this.specialPrice = in.readString();
            this.show_hospital = in.readString();
            this.invitation = in.readString();
            this.lijian = in.readString();
            this.baoxian = in.readString();
            this.insure = in.readParcelable(InsureBean.class.getClassLoader());
            this.img66 = in.readString();
            this.app_slide_logo = in.readString();
            this.repayment = in.readString();
            this.bilateral_coupons = in.readString();
            this.hos_red_packet = in.readString();
            this.mingyi = in.readString();
            this.hot = in.readString();
            this.newp = in.readString();
            this.shixiao = in.readString();
            this.extension_user = in.readString();
            this.postStr = in.readString();
            this.depreciate = in.readString();
            this.rate = in.readString();
            this.rateNum = in.readInt();
            this.service = in.readString();
            this.feeScale = in.readString();
            this.is_fanxian = in.readString();
            this.hospital_id = in.readString();
            this.doctor_id = in.readString();
            this.is_rongyun = in.readString();
            this.hos_userid = in.readString();
            this.business_district = in.readString();
            this.hospital_top = in.readParcelable(HospitalTopBean.class.getClassLoader());
            this.is_have_video = in.readString();
            this.surgInfo = in.readParcelable(SurgInfoBean.class.getClassLoader());
            this.highlight_title = in.readString();
            this.videoTaoTitle = in.readString();
            this.event_params = in.readParcelable(EventParamsBean.class.getClassLoader());
            this.distance = in.readString();
//            this.promotion = in.createTypedArrayList(Promotion.CREATOR);
            this.userImg = in.createStringArrayList();
        }

        public static final Creator<TaoListBean> CREATOR = new Creator<TaoListBean>() {
            @Override
            public TaoListBean createFromParcel(Parcel source) {
                return new TaoListBean(source);
            }

            @Override
            public TaoListBean[] newArray(int size) {
                return new TaoListBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.last_request_time);
        dest.writeList(this.tao_list);
    }

    public SearchTaoDate() {
    }

    protected SearchTaoDate(Parcel in) {
        this.last_request_time = in.readInt();
        this.tao_list = new ArrayList<TaoListBean>();
        in.readList(this.tao_list, TaoListBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<SearchTaoDate> CREATOR = new Parcelable.Creator<SearchTaoDate>() {
        @Override
        public SearchTaoDate createFromParcel(Parcel source) {
            return new SearchTaoDate(source);
        }

        @Override
        public SearchTaoDate[] newArray(int size) {
            return new SearchTaoDate[size];
        }
    };
}
