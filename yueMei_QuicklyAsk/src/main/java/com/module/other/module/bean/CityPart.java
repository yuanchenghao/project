package com.module.other.module.bean;

/**
 * Created by dwb on 16/10/10.
 */
public class CityPart {

    private String code;
    private String message;
    private CityPartData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CityPartData getData() {
        return data;
    }

    public void setData(CityPartData data) {
        this.data = data;
    }
}
