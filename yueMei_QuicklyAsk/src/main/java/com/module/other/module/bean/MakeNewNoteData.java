package com.module.other.module.bean;

/**
 * Created by dwb on 16/3/22.
 */
public class MakeNewNoteData {

    private String title;
    private String one_id;
    private String two_id;
    private String three_id;
    private String four_id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOne_id() {
        return one_id;
    }

    public void setOne_id(String one_id) {
        this.one_id = one_id;
    }

    public String getTwo_id() {
        return two_id;
    }

    public void setTwo_id(String two_id) {
        this.two_id = two_id;
    }

    public String getThree_id() {
        return three_id;
    }

    public void setThree_id(String three_id) {
        this.three_id = three_id;
    }

    public String getFour_id() {
        return four_id;
    }

    public void setFour_id(String four_id) {
        this.four_id = four_id;
    }

    @Override
    public String toString() {
        return "MakeNewNoteData{" +
                "title='" + title + '\'' +
                ", one_id='" + one_id + '\'' +
                ", two_id='" + two_id + '\'' +
                ", three_id='" + three_id + '\'' +
                ", four_id='" + four_id + '\'' +
                '}';
    }
}
