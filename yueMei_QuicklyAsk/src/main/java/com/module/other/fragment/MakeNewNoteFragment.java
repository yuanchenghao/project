package com.module.other.fragment;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.module.base.view.YMBaseFragment;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;

/**
 * 项目体验时间选择
 * Created by 裴成浩 on 2019/3/20
 */
public class MakeNewNoteFragment extends YMBaseFragment {

    //手术时间选择
    @BindView(R.id.note_fragment_time)
    LinearLayout experienceTime;
    @BindView(R.id.note_fragment_months)
    TextView experienceMonths;
    @BindView(R.id.note_fragment_day)
    TextView experienceDay;

    TimePickerView pvTime;

    private String TAG = "MakeNewNoteFragment";

    public static MakeNewNoteFragment newInstance() {
        return new MakeNewNoteFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_make_new_note;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView(View view) {
        //初始化手术时间
        experienceMonths.setText((Calendar.getInstance().get(Calendar.MONTH) + 1) + "");
        experienceDay.setText(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "");

        initTimeSelector();

        //手术时间选择
        experienceTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != pvTime) {
                    pvTime.show();
                }
            }
        });
    }

    @Override
    protected void initData(View view) {

    }

    /**
     * 初始化时间选择器
     */
    private void initTimeSelector() {
        //时间选择器
        pvTime = new TimePickerView(mContext, TimePickerView.Type.YEAR_MONTH_DAY);
        //控制时间范围
        Calendar calendar = Calendar.getInstance();
        Log.e(TAG, "calendar.get(Calendar.YEAR) == " + calendar.get(Calendar.YEAR));
        pvTime.setRange(calendar.get(Calendar.YEAR) - 27, calendar.get(Calendar.YEAR));//要在setTime 之前才有效果哦

        pvTime.setCyclic(false);
        pvTime.setCancelable(true);
        //时间选择后回调
        pvTime.setOnTimeSelectListener(new TimePickerView.OnTimeSelectListener() {

            @Override
            public void onTimeSelect(Date date) {
                String year = Utils.getYear(date);
                String month = Utils.getMonths(date);
                String day = Utils.getDay(date);

                experienceMonths.setText(month);
                experienceDay.setText(day);

                String surgeryDate = year + "-" + month + "-" + day;

                if (onTimeSelectListener != null) {
                    onTimeSelectListener.onTimeSelect(surgeryDate);
                }
            }
        });
    }

    //手术时间选择后的回调
    public OnTimeSelectListener onTimeSelectListener;

    public interface OnTimeSelectListener {
        void onTimeSelect(String data);
    }

    public void setOnTimeSelectListener(OnTimeSelectListener onTimeSelectListener) {
        this.onTimeSelectListener = onTimeSelectListener;
    }
}
