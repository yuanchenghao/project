package com.module.other.other;

import android.content.Intent;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.other.activity.CashWebViewActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by 裴成浩 on 2018/1/23.
 */

public class CashWebViewClient implements BaseWebViewClientCallback {

    private final CashWebViewActivity mActivity;
    private final Intent intent;
    private String uid;

    public CashWebViewClient(CashWebViewActivity mActivity) {
        this.mActivity = mActivity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }
    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页
                String id = obj.getString("id");
                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                intent.putExtra("docId", id);
                intent.putExtra("docName", "");
                intent.putExtra("partId", "");
                mActivity.startActivity(intent);

                break;
            case "6":   //问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link);
                intent.putExtra("qid", qid);
                mActivity.startActivity(intent);

                break;

            case "6313":
                mActivity.shareTitle = URLDecoder.decode(obj.getString("sharetitle"), "utf-8");
                mActivity.shareContent = URLDecoder.decode(obj.getString("sharecontent"), "utf-8");

                mActivity.shareImgUrl = obj.getString("shareimg");
                mActivity.shareUrl = obj.getString("shareurl");

                if (null != mActivity.shareContent && mActivity.shareContent.length() > 0) {
                    mActivity.setShare();

                }
                break;
        }
    }
}
