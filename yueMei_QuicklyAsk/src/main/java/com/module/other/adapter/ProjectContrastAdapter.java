package com.module.other.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.base.view.FunctionManager;
import com.module.commonview.view.NiceImageView;
import com.module.home.model.bean.SearchResultsTaoTag;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoPKBean;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;


public class ProjectContrastAdapter extends RecyclerView.Adapter<ProjectContrastAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    //    private TaoPKBean mTaopkBean;
    private List<TaoPKBean.TaoListBean> mTao;
    private List<String> mSelectDatas;
    private List<String> mSelectDatas_contrast;
    private List<String> mDeleteDatas;
    private String mDeleteFlag;
    //判断是哪个适配器
    private String mType;
    private FunctionManager mFunctionManager;
    private boolean mIsHeightLigh = false;

    public ProjectContrastAdapter(Context context, List<TaoPKBean.TaoListBean> tao_list, List<String> selectDatas, List<String> selectDatas_contrast, String type, String deleteFlag, List<String> deleteData) {
        this.mContext = context;
//        this.mTaopkBean = taoPK;
        this.mTao = tao_list;
        this.mSelectDatas = selectDatas;
        this.mSelectDatas_contrast = selectDatas_contrast;
        this.mType = type;
        this.mDeleteFlag = deleteFlag;
        this.mDeleteDatas = deleteData;
        inflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_project_contrast, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {


        if (mTao != null) {
            //设置海报的显示隐藏
            if ("1".equals(mTao.get(position).getShixiao())) {
                Glide.with(mContext).load(R.drawable.sall_null_2x)
                        .into(viewHolder.mImage);
//                mFunctionManager.setRoundImageSrc(viewHolder.mImage, R.drawable.sall_null_2x, Utils.dip2px(3));
            } else {
                Glide.with(mContext).load(mTao.get(position).getImg())
                        .asBitmap()
                        .into(viewHolder.mImage);
//                mFunctionManager.setRoundImageSrc(viewHolder.mImage, mTao.get(position).getImg(), Utils.dip2px(3));
            }

            //视频标签显示隐藏
            if ("1".equals(mTao.get(position).getIs_have_video())) {
                viewHolder.mImageVideoTag.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mImageVideoTag.setVisibility(View.GONE);
            }

            //搜索词高亮
            if (mIsHeightLigh) {
                String highlightTitle = mTao.get(position).getHighlight_title();
                if (!TextUtils.isEmpty(highlightTitle)) {
                    try {
                        String htmlTitle = URLDecoder.decode(highlightTitle.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");
                        //设置标题
                        viewHolder.mTitle.setText(Html.fromHtml(htmlTitle));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            } else {
//                Log.e(TAG, "下标 == " + pos + "，标题是 == <" + taoData.getTitle() + "> " + taoData.getSubtitle());
                viewHolder.mTitle.setText("< " + mTao.get(position).getTitle() + ">" + mTao.get(position).getSubtitle());
            }


            //设置医生
            viewHolder.mDocName.setText(mTao.get(position).getDoc_name());

            //设置医院
            if (!TextUtils.isEmpty(mTao.get(position).getDoc_name())) {
                viewHolder.mHosName.setText("，" + mTao.get(position).getHos_name());
            } else {
                viewHolder.mHosName.setText(mTao.get(position).getHos_name());
            }

            //设置预定人数
            viewHolder.mRate.setText(mTao.get(position).getRate());

            //设置位置信息
//            String business_district = mTao.get(position).getBusiness_district();
//            String distance = mTao.get(position).getDistance();
//            if (!TextUtils.isEmpty(business_district) && !TextUtils.isEmpty(distance)) {
//                business_district = business_district.trim();
//                distance = distance.trim();
//                viewHolder.mAddress.setText(business_district + distance);
//            } else {
//                if (!TextUtils.isEmpty(business_district)) {
//                    viewHolder.mAddress.setText(business_district);
//                } else if (!TextUtils.isEmpty(distance)) {
//                    viewHolder.mAddress.setText(distance);
//                }
//            }

            //设置劵后是否显示
            String coupon_type = mTao.get(position).getCoupon_type() + "";
            if (!"0".equals(coupon_type)) {
                viewHolder.mPriceTag.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mPriceTag.setVisibility(View.GONE);
            }

            //设置价格
            String price = mTao.get(position).getPrice_discount();
            viewHolder.mPriceNum.setText(price);
            viewHolder.mPriceFeescalev.setText(mTao.get(position).getFeeScale());
            //设置plus会员价
            String member_price = mTao.get(position).getMember_price();
            if (!TextUtils.isEmpty(member_price)) {
                if (Integer.parseInt(member_price) >= 0) {
                    viewHolder.mPricePlus.setVisibility(View.VISIBLE);
                    viewHolder.mPricePlusPrice.setText("¥" + member_price);
                } else {
                    viewHolder.mPricePlus.setVisibility(View.GONE);
                }
            }


            //设置奖牌
            TaoPKBean.TaoListBean.HospitalTopBean hospital_top = mTao.get(position).getHospital_top();
            if (hospital_top != null) {
                if (!TextUtils.isEmpty(hospital_top.getDesc())) {
                    viewHolder.hocTopTagContainer.setVisibility(View.VISIBLE);
                    viewHolder.mTopTag.setText(hospital_top.getDesc());
                } else {
                    viewHolder.hocTopTagContainer.setVisibility(View.GONE);
                }
            }


//            //设置标签
//            List<Promotion> promotion = mTao.get(position).getPromotion();
//            if (CollectionUtils.isNotEmpty(promotion)) {
//                viewHolder.mFlowLayout.setVisibility(View.VISIBLE);
//                setTagView(viewHolder.mFlowLayout, promotion);
//            } else {
//                viewHolder.mFlowLayout.setVisibility(View.GONE);
//            }


            //设置大促显示隐藏
            String saleData = mTao.get(position).getImg66();
            if (!TextUtils.isEmpty(saleData)) {
//                Log.e(TAG, "taoData.getImg66() == " + taoData.getImg66());
                viewHolder.mSales.setVisibility(View.VISIBLE);
                viewHolder.mBack.setVisibility(View.GONE);

                Glide.with(mContext)
                        .load(mTao.get(position).getImg66())
                        .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                int intrinsicWidth = resource.getIntrinsicWidth();
                                int intrinsicHeight = resource.getIntrinsicHeight();

                                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.mSales.getLayoutParams();

//                                Log.e(TAG, "intrinsicWidth == " + intrinsicWidth);
//                                Log.e(TAG, "intrinsicHeight == " + intrinsicHeight);
                                layoutParams.width = intrinsicWidth;
                                layoutParams.height = intrinsicHeight;
                                viewHolder.mSales.setLayoutParams(layoutParams);

                                viewHolder.mSales.setImageDrawable(resource);
                            }
                        });

            } else {
                viewHolder.mSales.setVisibility(View.GONE);
                //返现显示隐藏设置
                if (!TextUtils.isEmpty(price) && Integer.parseInt(price) > 1000) {
                    String isFanxian = mTao.get(position).getIs_fanxian();
                    if ("0".equals(isFanxian)) {
                        viewHolder.mBack.setVisibility(View.GONE);
                    } else {
                        viewHolder.mBack.setVisibility(View.VISIBLE);
                    }
                } else {
                    viewHolder.mBack.setVisibility(View.GONE);
                }
            }


            //底部标签设置
            ArrayList<SearchResultsTaoTag> bottomList = new ArrayList<>();

//            //大促是否存在
//            PromotionNoticeTextData promotionNoticeText = mTaoList.get(position).getPromotion();
//            if (promotionNoticeText != null) {
//                bottomList.add(new SearchResultsTaoTag(promotionNoticeText.getPromotionNotice(), promotionNoticeText.getPromotionType(), R.drawable.shape_ff6e59));
//            }

            //分期是否存在
            String repayment = mTao.get(position).getRepayment();
            if (!TextUtils.isEmpty(repayment)) {
                bottomList.add(new SearchResultsTaoTag(repayment, "分期", R.drawable.shape_ff94ab));
            }

            //保险是否存在
            String baoxian = mTao.get(position).getBaoxian();
            if (!TextUtils.isEmpty(baoxian)) {
                bottomList.add(new SearchResultsTaoTag(baoxian, "保险", R.drawable.shape_66cccc));
            }

            //红包是否存在
            String hosRedPacket = mTao.get(position).getHos_red_packet();
            if (!TextUtils.isEmpty(hosRedPacket)) {
                bottomList.add(new SearchResultsTaoTag(hosRedPacket, "红包", R.drawable.shape_fb5e79));
            }
            //设置底部数据
            setBottomTag(viewHolder.mBottom, bottomList);

            if (mDeleteFlag.equals("1") && mType.equals("0")) {
                viewHolder.iv_check.setImageResource(R.drawable.image_unselected_contrast);
                Log.i("delete", mDeleteDatas + "");
                Log.i("delete", mTao.get(position).getPk_id() + "");
                if (mDeleteDatas != null && !mDeleteDatas.isEmpty() && mDeleteDatas.contains(mTao.get(position).getPk_id() + "".trim())) {
                    viewHolder.iv_check.setImageResource(R.drawable.add_project_contrast);
                } else {
                    viewHolder.iv_check.setImageResource(R.drawable.image_unselected_contrast);
                }
            } else {
                if (mType.equals("0")) {
                    Log.i("mSelectDatas_contrast", mSelectDatas_contrast + "");
                    Log.i("mSelectDatas_contrast", mTao.get(position).getId());
                    if (mSelectDatas_contrast != null && !mSelectDatas_contrast.isEmpty() && mSelectDatas_contrast.contains(mTao.get(position).getId())) {
                        viewHolder.iv_check.setImageResource(R.drawable.add_project_contrast);
                    } else {
                        viewHolder.iv_check.setImageResource(R.drawable.image_unselected_contrast);
                    }
                } else {
                    if (mSelectDatas != null && !mSelectDatas.isEmpty() && mSelectDatas.contains(mTao.get(position).getId())) {
                        viewHolder.iv_check.setImageResource(R.drawable.add_project_contrast);
                    } else {
                        viewHolder.iv_check.setImageResource(R.drawable.image_unselected_contrast);
                    }
                }
            }
            viewHolder.iv_check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemCheckClick != null) {
                        onItemCheckClick.onItemCheckClick(viewHolder.iv_check, position);
                    }
                }
            });
            viewHolder.ll_root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, position);
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return mTao.size();
    }

    public void addList(List<TaoPKBean.TaoListBean> tao_list, List<String> selectDatas) {
//        Log.i("mTao", mTao + "");
//        Log.i("mTao", tao_list + "");
        this.mTao.addAll(tao_list);
//        this.mSelectDatas.addAll(selectDatas);
        this.mSelectDatas = selectDatas;
        notifyItemRangeChanged(mTao.size() - tao_list.size(), tao_list.size());
//        notifyDataSetChanged();
    }

    public void initDeleteList(List<String> deleteList, String deleteFlag) {
        this.mDeleteDatas = deleteList;
        this.mDeleteFlag = deleteFlag;
        notifyDataSetChanged();
    }

    public void upDataList(List<String> selectDatas_contrast, List<String> selectDatas) {
        this.mSelectDatas_contrast = selectDatas_contrast;
        this.mSelectDatas = selectDatas;
        notifyDataSetChanged();
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<Promotion> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            mFlowLayout.setMaxLine(1);
            for (int i = 0; i < lists.size(); i++) {
                Promotion promotion = lists.get(i);
                String styleType = promotion.getStyle_type();//1正常  2最近浏览
                TextView textView = new TextView(mContext);
                textView.setText(lists.get(i).getTitle());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                if ("2".equals(styleType)) {
                    textView.setBackgroundResource(R.drawable.sku_list_nearlook);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.ff7c4f));
                } else {
                    textView.setBackgroundResource(R.drawable.shopping_cart_sku_tab_background);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                }
                mFlowLayout.addView(textView);


            }
        }
    }

    /**
     * 底部标签设置
     *
     * @param bottom
     * @param lists
     */
    private void setBottomTag(LinearLayout bottom, ArrayList<SearchResultsTaoTag> lists) {
        if (bottom.getChildCount() != lists.size()) {
            bottom.removeAllViews();
            for (SearchResultsTaoTag data : lists) {
                View view = View.inflate(mContext, R.layout.item_search_results_tao_bottom, null);
                TextView tag = view.findViewById(R.id.tao_list_bottom_tag);
                TextView content = view.findViewById(R.id.tao_list_bottom_content);
                tag.setText(data.getTagText());
                tag.setBackgroundResource(data.getResource());
                content.setText(data.getContent());

                ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.topMargin = Utils.dip2px(8);
//                lp.bottomMargin = Utils.dip2px(8);
                bottom.addView(view, lp);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        NiceImageView mImage;
        ImageView mImageVideoTag;
        TextView mTitle;
        TextView mDocName;
        TextView mHosName;
        TextView mRate;
        TextView mAddress;
        TextView mPriceTag;
        TextView mPriceNum;
        TextView mPriceFeescalev;
        LinearLayout mPricePlus;
        TextView mPricePlusPrice;
        RelativeLayout mBack;
        LinearLayout hocTopTagContainer;
        TextView mTopTag;
        FlowLayout mFlowLayout;
        ImageView mSales;
        LinearLayout mBottom;
        ImageView iv_check;
        LinearLayout ll_root;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_root = itemView.findViewById(R.id.ll_root);
            mImage = itemView.findViewById(R.id.tao_list_image);
            mImageVideoTag = itemView.findViewById(R.id.tao_list_image_video_tag);
            mTitle = itemView.findViewById(R.id.tao_list_title);
            mDocName = itemView.findViewById(R.id.tao_list_docname);
            mHosName = itemView.findViewById(R.id.tao_list_hosname);
            mRate = itemView.findViewById(R.id.tao_list_rate);
            mAddress = itemView.findViewById(R.id.tao_list_address);
            mPriceTag = itemView.findViewById(R.id.tao_list_price_tag);
            mPriceNum = itemView.findViewById(R.id.tao_list_price_num);
            mPriceFeescalev = itemView.findViewById(R.id.tao_list_price_feescalev);
            mPricePlus = itemView.findViewById(R.id.tao_list_price_plus);
            mPricePlusPrice = itemView.findViewById(R.id.tao_list_price_plus_price);
            mBack = itemView.findViewById(R.id.tao_list_back);
            hocTopTagContainer = itemView.findViewById(R.id.tao_list_top_tag_container);
            mTopTag = itemView.findViewById(R.id.tao_list_top_tag);
            mFlowLayout = itemView.findViewById(R.id.tao_list_flowLayout);
            mSales = itemView.findViewById(R.id.tao_list_sales);
            mBottom = itemView.findViewById(R.id.tao_list_bottom);
            iv_check = itemView.findViewById(R.id.iv_check);
        }
    }

    public List<TaoPKBean.TaoListBean> getmDatas() {
        return mTao;
    }

    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * item的选中回调
     */
    public interface OnItemCheckClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemCheckClick(View view, int pos);
    }

    private OnItemCheckClickListener onItemCheckClick;

    public void setOnItemCheckClickListener(OnItemCheckClickListener onItemCheckClick) {
        this.onItemCheckClick = onItemCheckClick;
    }
}