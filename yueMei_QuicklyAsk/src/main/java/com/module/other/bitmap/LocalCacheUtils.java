package com.module.other.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLEncoder;

/**
 * 三级缓存之本地缓存
 * Created by 裴成浩 on 2018/2/5.
 */

public class LocalCacheUtils {
    private static final String CACHE_PATH= Environment.getExternalStorageDirectory().toString()+"/CacheYueMeiImage";
    private String TAG = "LocalCacheUtils";

    /**
     * 从本地读取图片
     * @param url
     */
    public Bitmap getBitmapFromLocal(String url){
        String fileName = null;//把图片的url当做文件名,并进行MD5加密
        try {
            fileName = URLEncoder.encode(url,"utf-8");
            File file=new File(CACHE_PATH,fileName);

            Log.e(TAG, "parentFile的存储路径是 === " + file.getPath());
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));

            return bitmap;
        } catch (Exception e) {
            Log.e(TAG, "e111111=== " + e.toString());
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 从网络获取图片后,保存至本地缓存
     * @param url
     * @param bitmap
     */
    public void setBitmapToLocal(String url,Bitmap bitmap){
        try {
            String fileName = URLEncoder.encode(url,"utf-8");//把图片的url当做文件名,并进行MD5加密
            File file=new File(CACHE_PATH,fileName);

            //通过得到文件的父文件,判断父文件是否存在
            File parentFile = file.getParentFile();
            if (!parentFile.exists()){
                parentFile.mkdirs();
            }

            Log.e(TAG, "parentFile的存储路径是 === " + parentFile.getPath());
            //把图片保存至本地
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,new FileOutputStream(file));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "e22222=== " + e.toString());
        }

    }
}