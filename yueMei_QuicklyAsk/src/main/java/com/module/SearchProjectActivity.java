package com.module;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.ArrayMap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.api.LodProjectHotApi;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.community.controller.adapter.SearchAboutWordsAdapter;
import com.module.community.model.bean.SearchAboutData;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.adpter.SearchProAdpter;
import com.quicklyask.entity.ProjectHot;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.kymjs.aframe.utils.SystemTool;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目搜索
 * 
 * @author Rubin
 * 
 */
public class SearchProjectActivity extends BaseActivity {

	@BindView(id = R.id.input_edit)
	private EditText inputEt;
	@BindView(id = R.id.search_cancel_or_rly)
	private RelativeLayout cancelOrSearchBt;
	@BindView(id = R.id.search_cancel_or_tv)
	private TextView cancleOrsearchTv;
	@BindView(id = R.id.ivDeleteText)
	private ImageView searchIv;
	@BindView(id = R.id.pop_list_tao_project_list)
	private ListView mlist;
	@BindView(id = R.id.my_collect_post_tv_nodata)
	private LinearLayout nodataTv;

	private List<ProjectHot> lvGroupData = new ArrayList<>();

	private String ifCancel = "1";

	private Handler mHandler;
	private SearchProjectActivity mContext;
	private SearchProAdpter mPart2Adapter;

	// 搜索相关联的词
	@BindView(id = R.id.load_search_aboutci_ly)
	private LinearLayout searchContentLy;
	private List<SearchAboutData> lvabwords = new ArrayList<>();
	@BindView(id = R.id.pop_sousuo_about_list)
	private ListView schAboulist;
	private SearchAboutWordsAdapter seaAbAdapter;

	private boolean ifdianji = false;
	private PageJumpManager pageJumpManager;
	private Map<String, String> projectDetailMap = new ArrayMap<>();
	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_search_project);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = SearchProjectActivity.this;
		pageJumpManager = new PageJumpManager(mContext);
		mDialog = new LoadingProgress(mContext);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setonListner();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	void setonListner() {
		inputEt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String inputStr = inputEt.getText().toString().trim();
				if (inputStr.length() > 0) {
					inputEt.setCursorVisible(true);
					cancleOrsearchTv.setText("搜索");
					ifCancel = "0";
				} else {
					inputEt.setCursorVisible(true);
					cancleOrsearchTv.setText("取消");
					ifCancel = "1";
				}
			}
		});

		inputEt.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void afterTextChanged(Editable s) {
				if (s.length() == 0) {
					if (inputEt.hasFocus()) {
						cancleOrsearchTv.setText("取消");
						ifCancel = "1";
					}
					searchIv.setVisibility(View.GONE);
				} else {
					if (inputEt.hasFocus()) {
						cancleOrsearchTv.setText("搜索");
						ifCancel = "0";
					}
					if (ifdianji) {
						searchContentLy.setVisibility(View.GONE);
					} else {
						// SearchaboutWords(inputEt.getText().toString());
					}
					searchIv.setVisibility(View.VISIBLE);
				}

			}
		});

		searchIv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				inputEt.setText("");
				cancleOrsearchTv.setText("取消");
				ifCancel = "1";
				inputEt.setCursorVisible(true);
			}
		});

		cancelOrSearchBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (ifCancel.equals("1")) {
					SystemTool.hideKeyBoard(SearchProjectActivity.this);
					finish();
				} else {
					// 先隐藏键盘
					Utils.hideSoftKeyboard(mContext);
					// 监听搜索键的行为
					String key = inputEt.getText().toString().trim();
					// 搜搜哦
					mDialog.startLoading();
					lodProjectHot(key);
					mHandler = getHandler();
					inputEt.clearFocus();
					inputEt.setCursorVisible(false);
					cancleOrsearchTv.setText("取消");
					ifCancel = "1";
					searchContentLy.setVisibility(View.GONE);
				}
			}
		});

		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {

				String oneid = lvGroupData.get(pos).getOneid();
				String onetitle = lvGroupData.get(pos).getOnetitle();
				String twoid = lvGroupData.get(pos).getTwoid();
				String twotitle = lvGroupData.get(pos).getTwotitle();
				String _id = lvGroupData.get(pos).get_id();
				String _title = lvGroupData.get(pos).getTitle();

				projectDetailMap.put("oneid", oneid);
				projectDetailMap.put("onetitle", onetitle);
				projectDetailMap.put("twoid", twoid);
				projectDetailMap.put("twotitle", twotitle);
				projectDetailMap.put("threeid", _id);
				projectDetailMap.put("threetitle", _title);
				projectDetailMap.put("medthod", "0");
				pageJumpManager.jumpToProjectDetailActivity550(projectDetailMap);

				finish();
			}

		});
	}

	void lodProjectHot(final String keyStr) {

		new Thread(new Runnable() {
			@Override
			public void run() {
				HashMap<String,Object> maps=new HashMap<>();
				maps.put("key", URLEncoder.encode(keyStr));
				maps.put("flag","0");
				new LodProjectHotApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
					@Override
					public void onSuccess(ServerData serverData) {
						if ("1".equals(serverData.code)){
							lvGroupData =JSONUtil.jsonToArrayList(serverData.data,ProjectHot.class);
							Message message = mHandler.obtainMessage(1);
							message.sendToTarget();
						}
					}
				});
				//lvGroupData = HttpData.loadSearchProjectData(keyStr);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint("NewApi")
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (lvGroupData != null && lvGroupData.size() > 0) {

						mDialog.stopLoading();
						nodataTv.setVisibility(View.GONE);
						mlist.setVisibility(View.VISIBLE);
						mPart2Adapter = new SearchProAdpter(mContext,
								lvGroupData);
						mlist.setAdapter(mPart2Adapter);

						ifdianji = false;
					} else {
						mDialog.stopLoading();
						nodataTv.setVisibility(View.VISIBLE);
						mlist.setVisibility(View.GONE);

						ifdianji = false;
					}
					break;
				}

			}
		};
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
