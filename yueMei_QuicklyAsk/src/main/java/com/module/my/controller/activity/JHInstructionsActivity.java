package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WriteNoteManager;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 精华帖说明
 * 
 * @author Rubin
 * 
 */
public class JHInstructionsActivity extends BaseActivity {

	private final String TAG = "JHInstructionsActivity";

	@BindView(id = R.id.jinghua_web_det_scrollview3, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.jinghua_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.jh_instructions_top)
	private CommonTopBar mTop;// 返回

	@BindView(id = R.id.bbs_detail_bottom_bt, click = true)
	private RelativeLayout wtiteBBs;

	private WebView docDetWeb;

	private JHInstructionsActivity mContext;

	public JSONObject obj_http;

	@BindView(id = R.id.all_content)
	private LinearLayout contentLy;
	private BaseWebViewClientMessage mBaseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_jh_instructions);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = JHInstructionsActivity.this;

		mTop.setCenterText("精华帖说明");

		scollwebView.GetLinearLayout(contentWeb);
		mBaseWebViewClientMessage = new BaseWebViewClientMessage(mContext);

		initWebview();

		LodUrl1(FinalConstant.JH_BBS + Utils.getTokenStr());

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						JHInstructionsActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContext);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.getSettings().supportMultipleWindows();
		docDetWeb.getSettings().setNeedInitialFocus(true);
		docDetWeb.setWebViewClient(mBaseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.bbs_detail_bottom_bt:

			if (Utils.isLogin()) {
				if (Utils.isBind()){
					WriteNoteManager.getInstance(mContext).ifAlert(null);
				}else {
					Utils.jumpBindingPhone(mContext);

				}

			} else {
				Utils.jumpLogin(mContext);
			}

			break;
		}
	}


	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
