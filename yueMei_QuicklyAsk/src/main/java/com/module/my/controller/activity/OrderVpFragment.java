package com.module.my.controller.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.BaseWebViewTel;
import com.module.my.controller.other.MyOrdersWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.qmuiteam.qmui.widget.pullRefreshLayout.QMUIPullRefreshLayout;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.util.HashMap;

/**
 * Created by Administrator on 2018/4/17.
 */

public class OrderVpFragment extends Fragment {
    public static final String TAG ="OrderVpFragment";
    private static final String KEY = "url";
    private WebView mWebView;
    private String mFlag;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private ViewGroup nowifiLy;
    private Button refreshBt;
    private MyPullRefresh mPullRefresh;
    private int page=1;
    private String mYmClass="";
    public OrderVpFragment() {
    }

    public static OrderVpFragment newInstance(String flag) {
        Bundle args = new Bundle();
        args.putString(KEY, flag);
        OrderVpFragment fragment = new OrderVpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mFlag = bundle.getString(KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_vp_item, container, false);

        RelativeLayout contentLy =view.findViewById(R.id.fragment_order_vp_content);

        nowifiLy = view.findViewById(R.id.order_no_wifi);
        refreshBt = view.findViewById(R.id.refresh_bt);
        mPullRefresh = view.findViewById(R.id.order_refresh);
        mWebView = view.findViewById(R.id.order_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().supportMultipleWindows();
        mWebView.getSettings().setNeedInitialFocus(true);

        baseWebViewClientMessage = new BaseWebViewClientMessage(getActivity());
        baseWebViewClientMessage.setView(contentLy);

        baseWebViewClientMessage.setParameter5983("0","32","32","0");
        baseWebViewClientMessage.setBaseWebViewClientCallback(new MyOrdersWebViewClient(getActivity()));
        baseWebViewClientMessage.setBaseWebViewTel(new BaseWebViewTel() {
            @Override
            public void tel(WebView view, String url) {
                telPhone(url);
            }
        });
        mWebView.setWebViewClient(baseWebViewClientMessage);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        LodUrl1(FinalConstant.MY_ORDER,mFlag,"");
        /**
         * 下拉刷新监听
         */

        mPullRefresh.setOnPullListener(new QMUIPullRefreshLayout.OnPullListener() {
            @Override
            public void onMoveTarget(int offset) {
                Log.e(TAG, "11111");
            }

            @Override
            public void onMoveRefreshView(int offset) {
                Log.e(TAG, "222222");
            }

            @Override
            public void onRefresh() {
                LodUrl1(FinalConstant.MY_ORDER,mFlag,page+"");
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SystemTool.checkNet(getActivity())) {
            nowifiLy.setVisibility(View.GONE);

        } else {
            nowifiLy.setVisibility(View.VISIBLE);
        }

        refreshBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                baseWebViewClientMessage.startLoading();
                new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (SystemTool.checkNet(getActivity())) {
                            nowifiLy.setVisibility(View.GONE);
                            baseWebViewClientMessage.stopLoading();
                            LodUrl1(FinalConstant.MY_ORDER,mFlag,"");

                        } else {
                            baseWebViewClientMessage.stopLoading();
                            nowifiLy.setVisibility(View.VISIBLE);
                            ViewInject.toast("网络连接失败");
                        }
                    }
                }.start();
            }
        });
    }

    public void LodUrl1(String url,String flag,String page) {
        baseWebViewClientMessage.startLoading();
        mPullRefresh.finishRefresh();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("flag",flag);
        if (!page.equals("")){
            hashMap.put("page",page);
        }
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url,hashMap);
        if (null != mWebView) {
            Log.d(TAG,"url:=="+addressAndHead.getUrl());
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }

    }
    public void webReload() {
        if (mWebView != null) {
            baseWebViewClientMessage.startLoading();
            mWebView.reload();
        }
    }
    private void telPhone(String url) {
        String aa = url.replace("tel:", "");
        ViewInject.toast("正在拨打中·····");
        Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + aa));
        try {
            startActivity(it);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
