package com.module.my.controller.activity;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.BaseWebViewReload;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;

import java.util.HashMap;

import butterknife.BindView;

public class WalletMingxiActivity extends YMBaseActivity {
    public static final String TAG="WalletMingxiActivity";
    @BindView(R.id.wallet_mingxi_title)
    CommonTopBar mWalletMingxiTitle;
    @BindView(R.id.wallet_webview)
    WebView mWalletWebview;
    private BaseWebViewClientMessage mBaseWebViewClientMessage;
    private String mUrl;
    private HashMap<String, Object> mSingStr = new HashMap<>();
    private String mJumpType;
    private String mTitle;
    private String mFlag;
    private String mSource;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_wallet_mingxi;
    }

    @Override
    protected void initView() {
        Intent intent = getIntent();
        mJumpType = intent.getStringExtra("jump_type");
        mTitle = intent.getStringExtra("title");
        mFlag = intent.getStringExtra("flag");
        mSource = intent.getStringExtra("source");
        mWalletMingxiTitle.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mWalletMingxiTitle.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                //客服跳转
                Intent it = new Intent();
                it.setClass(mContext, OnlineKefuWebActivity.class);
                it.putExtra("link", "/service/zxzx/");
                it.putExtra("title", "在线客服");
                startActivity(it);
            }
        });
    }

    @Override
    protected void initData() {
        mBaseWebViewClientMessage = new BaseWebViewClientMessage(mContext,"","",mWalletWebview);
        mBaseWebViewClientMessage.setBaseWebViewReload(new BaseWebViewReload() {
            @Override
            public void reload() {
                webReload();
            }
        });
        mWalletWebview.getSettings().setJavaScriptEnabled(true);
        mWalletWebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWalletWebview.getSettings().setUseWideViewPort(true);
        mWalletWebview.getSettings().supportMultipleWindows();
        mWalletWebview.getSettings().setNeedInitialFocus(true);
        mWalletWebview.getSettings().setAllowFileAccess(true); // 允许访问文件
        mWalletWebview.getSettings().setSupportZoom(true); // 支持缩放
        mWalletWebview.getSettings().setLoadWithOverviewMode(true);
        mWalletWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWalletWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        mWalletWebview.setWebViewClient(mBaseWebViewClientMessage);
        if ("1".equals(mJumpType)){
            mUrl= FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1 .YUEMEI_VER + FinalConstant1.SYMBOL2+"wallet/detaild/";
            mSingStr.put("flag","0"); //0全部 1收入 3支出
            mWalletMingxiTitle.setCenterText(mTitle);
        }else if ("2".equals(mJumpType)){
            mUrl= FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1 .YUEMEI_VER + FinalConstant1.SYMBOL2+"wallet/explain/";
            mSingStr.put("type","8");//8钱包
            mWalletMingxiTitle.setCenterText(mTitle);
        }else if ("3".equals(mJumpType)){
            mUrl= FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1 .YUEMEI_VER + FinalConstant1.SYMBOL2+"wallet/explain/";
            mSingStr.put("type","9");// 9提现
            mWalletMingxiTitle.setCenterText(mTitle);
        }else if ("4".equals(mJumpType)){ //提现协议
            mUrl= FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1 .YUEMEI_VER + FinalConstant1.SYMBOL2+"wallet/protocol/";
            mWalletMingxiTitle.setCenterText(mTitle);
        }else {
            mUrl= FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + FinalConstant1.SYMBOL2 + FinalConstant1 .YUEMEI_VER + FinalConstant1.SYMBOL2+"wallet/detaild/";
            mSingStr.put("flag",mFlag);
            mSingStr.put("source",mSource);
            mWalletMingxiTitle.setCenterText(mTitle);
        }
        loadAction();
    }


    public void webReload() {
        if (mWalletWebview != null) {
            mBaseWebViewClientMessage.startLoading();
            mWalletWebview.reload();
        }
    }

    public void loadAction() {
        mBaseWebViewClientMessage.startLoading();
        Log.e(TAG,"url111====" + mUrl);
        Log.e(TAG,"mSingStr====" + mSingStr);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(mUrl,mSingStr);
        Log.e(TAG,"url====" + addressAndHead.getUrl());
        Log.e(TAG,"httpHeaders====" + addressAndHead.getHttpHeaders());
        mWalletWebview.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
