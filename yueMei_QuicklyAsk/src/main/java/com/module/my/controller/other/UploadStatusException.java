package com.module.my.controller.other;

/**
 * 图片视频上传状态异常，下标出现负数抛此异常
 * Created by 裴成浩 on 2018/8/14.
 */
public class UploadStatusException extends Exception {

    private String msg;                         //异常说明

    public UploadStatusException() {
        super();
    }

    public UploadStatusException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
