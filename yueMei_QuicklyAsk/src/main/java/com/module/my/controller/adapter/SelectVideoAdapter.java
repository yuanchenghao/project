package com.module.my.controller.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.my.controller.activity.SelectVideoActivity;
import com.module.my.model.bean.BitmapEntity;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.io.File;
import java.util.List;

/**
 * Created by 裴成浩 on 2017/6/27.
 */

public class SelectVideoAdapter extends RecyclerView.Adapter<SelectVideoAdapter.ViewHolder> {

    private String TAG = "SelectVideoAdapter";
    private Context mContext;
    private List<BitmapEntity> mBit;
    private LayoutInflater inflater;
    private String mSelectPath;

    public SelectVideoAdapter(Context context, List<BitmapEntity> bit, String selectPath) {
        this.mContext = context;
        this.mBit = bit;
        Log.e(TAG, "mBit == " + mBit.size());
        this.mSelectPath = selectPath;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_select_video, parent, false);
        return new ViewHolder(itemView);        //把这个布局传到ViewHolder中
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        BitmapEntity entity = mBit.get(position);

        long mss = entity.getDuration();

        long minutes = (mss % (1000 * 60 * 60)) / (1000 * 60);
        long seconds = (mss % (1000 * 60)) / 1000;

        String minutes1 = (int) minutes < 10 ? "0" + minutes : minutes + "";
        String seconds1 = (int) seconds < 10 ? "0" + seconds : seconds + "";

        holder.tvShijian.setText(minutes1 + ":" + seconds1);
        /*****视频缩略图******/

        Glide.with(mContext)
                .load(Uri.fromFile(new File(entity.getPath())))
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                .into(holder.thumbnail);

        /******视频缩略图*****/

        Log.e("GGG", "mSelectPath === " + mSelectPath);
        Log.e("GGG", "entity.getPath() === " + entity.getPath());
        if (mSelectPath.equals(entity.getPath())) {

            ((SelectVideoActivity)mContext).mPos = position;

            holder.ivXuanzhong.setImageResource(R.drawable.image_selected);
            holder.ivMengceng.setVisibility(View.VISIBLE);
        } else {
            holder.ivXuanzhong.setImageResource(R.drawable.image_unselected);
            holder.ivMengceng.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mBit.size();
    }


    public void setmSelectPath(String mSelectPath) {
        this.mSelectPath = mSelectPath;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView thumbnail;
        private ImageView ivXuanzhong;
        private ImageView ivMengceng;
        private TextView tvShijian;

        public ViewHolder(View itemView) {
            super(itemView);
            thumbnail =  itemView.findViewById(R.id.iv_thumbnail);
            ivXuanzhong = itemView.findViewById(R.id.iv_xuanzhong);
            ivMengceng = itemView.findViewById(R.id.iv_mengceng);
            tvShijian = itemView.findViewById(R.id.tv_shijian);

            itemView. setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null){
                        onItemClickListener .onItemClick(v,getLayoutPosition());
                    }
                }
            });
        }
    }


    /**
     * item的监听器
     */
    public interface OnItemClickListener{
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener ;

    public void setOnItemClickListener (OnItemClickListener onItemClickListener){
        this .onItemClickListener = onItemClickListener ;
    }

}
