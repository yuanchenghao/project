package com.module.my.controller.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.quicklyask.activity.R;
import com.quicklyask.view.ProcessImageView;

import org.json.JSONObject;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2017/6/13.
 */

public class ImageUploadAdapter extends BaseAdapter {
    private final Context mContext;
    private final ImageOptions imageOptions;
    private final Animation ain;
    private HashMap<String, JSONObject> mSameData;
    private HashMap<String, String> mErrorImg;
    private LayoutInflater listContainer;

    private boolean shape;
    private ArrayList<String> mResults = new ArrayList<>();
    private HashMap<String, ProcessImageView> imgs = new HashMap();
    private String TAG = "ImageUploadAdapter";

    public ImageUploadAdapter(Context context, ArrayList<String> mResults, HashMap<String, JSONObject> mSameData, HashMap<String, String> mErrorImg) {
        this.mContext = context;
        this.mResults = mResults;
        this.mSameData = mSameData;
        this.mErrorImg = mErrorImg;
        listContainer = LayoutInflater.from(context);
        ain = AnimationUtils.loadAnimation(mContext, R.anim.push_in);
        imageOptions = new ImageOptions.Builder()
                .setConfig(Bitmap.Config.ARGB_8888)
                .setIgnoreGif(false)
                .setAnimation(ain)
                .setCrop(true)//是否对图片进行裁剪
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setUseMemCache(true)
                .setLoadingDrawableId(R.drawable.radius_gray80)
                .setFailureDrawableId(R.drawable.radius_gray80)
                .build();
    }

    public int getCount() {
        if (mResults.size() < 9) {
            return mResults.size() + 1;
        } else {
            return mResults.size();
        }
    }

    public Object getItem(int arg0) {

        return mResults.get(arg0);
    }

    public long getItemId(int arg0) {

        return arg0;
    }

    /**
     * ListView Item设置
     */
    public View getView(final int position, View convertView, ViewGroup parent) {
        // 自定义视图
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            // 获取list_item布局文件的视图
            convertView = listContainer.inflate(R.layout.item_published_grida, null);
            // 获取控件对象
            holder.image = convertView.findViewById(R.id.item_grida_image);
            holder.bt = convertView.findViewById(R.id.item_grida_bt);
            // 设置控件集到convertView
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == mResults.size()) {      //如果是最后一个 + 1 个
            holder.image.startHua(ProcessImageView.WANC_HENG, 100);      //设置图片UI

            holder.image.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.camera_gray3x));//设置成选择的图片

            holder.bt.setVisibility(View.GONE);         //取消按钮隐藏
            if (position == 9) {                        //如果是第10个，那么添加按钮可以隐藏了
                holder.image.setVisibility(View.GONE);
            }
        } else {            //不是最后一个添加本地图片
            String imgUrl = mResults.get(position);     //本地路径

            x.image().bind(holder.image, imgUrl, imageOptions);

            if (mSameData.get(imgUrl) != null && mSameData.get(imgUrl).length() > 0) {            //说明是已经上传过
                    Log.e(TAG, "2222222");
                    holder.image.startHua(ProcessImageView.WANC_HENG, 100);
            } else {                             //没有上传过的(上传失败/新增图片)
                if (mErrorImg.get(imgUrl) != null) {          //上传失败的图片
//                    if (mCover.equals(imgUrl)) {
//                        Log.e(TAG, "333333");
//                        holder.image.startHua(ProcessImageView.SHI_BAI, 0, false, true);
//                    } else {
                        Log.e(TAG, "4444444");
                        holder.image.startHua(ProcessImageView.SHI_BAI, 0);
//                    }
                } else {
//                    if (mCover.equals(imgUrl)) {
//                        Log.e(TAG, "555555");
//                        holder.image.startHua(ProcessImageView.DENG_DAI, 0, false, true);
//                    } else {
                        Log.e(TAG, "666666");
                        holder.image.startHua(ProcessImageView.DENG_DAI, 0);         //新增图片
//                    }
                }
            }
            imgs.put(mResults.get(position), holder.image);
        }

        holder.bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemDeleteListener.onDeleteClick(position);
            }
        });

        return convertView;
    }

    public boolean isShape() {
        return shape;
    }

    public void setShape(boolean shape) {
        this.shape = shape;
    }

    public HashMap<String, ProcessImageView> getProcessImage() {
        return imgs;
    }


    public class ViewHolder {
        public ProcessImageView image;
        public RelativeLayout bt;
    }

    /**
     * 删除按钮的监听接口
     */
    public interface onItemDeleteListener {
        void onDeleteClick(int i);
    }

    private onItemDeleteListener mOnItemDeleteListener;

    public void setOnItemDeleteClickListener(onItemDeleteListener mOnItemDeleteListener) {
        this.mOnItemDeleteListener = mOnItemDeleteListener;
    }
}
