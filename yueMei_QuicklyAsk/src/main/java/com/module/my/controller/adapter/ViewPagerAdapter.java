package com.module.my.controller.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.module.my.controller.activity.OrderVpFragment;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/4/17.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {



    private String[] titles;
    private ArrayList<OrderVpFragment> viewPagerFragments;

    public void setTitles(String[] titles) {
        this.titles = titles;
    }

    public void setViewPagerFragments(ArrayList<OrderVpFragment> viewPagerFragments) {
        this.viewPagerFragments = viewPagerFragments;
    }
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return viewPagerFragments.get(position);
    }

    @Override
    public int getCount() {
        return viewPagerFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }
}
