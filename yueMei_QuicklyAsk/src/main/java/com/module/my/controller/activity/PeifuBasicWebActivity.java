package com.module.my.controller.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 赔付依据
 */
public class PeifuBasicWebActivity extends BaseActivity {

	private final String TAG = "PeifuBasicWebActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.basic_web_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private PeifuBasicWebActivity mContex;

	public JSONObject obj_http;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_peifu_basic_web);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = PeifuBasicWebActivity.this;

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);

		mTop.setCenterText("赔付依据");

		scollwebView.GetLinearLayout(contentWeb);
		initWebview();

		LodUrl1(FinalConstant.PEIFU_BASIC);
		scollwebView.setonRefreshListener(new OnRefreshListener1() {

			@Override
			public void onRefresh() {
				webReload();
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						PeifuBasicWebActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}


	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}