package com.module.my.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.AmountRefundApi;
import com.module.commonview.module.bean.AmountRefundBean;
import com.module.my.view.orderpay.ApplyMoneyBackActivity;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.OrderInfoData;
import com.quicklyask.util.JSONUtil;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplyMoneyBackAdapter2 extends RecyclerView.Adapter<ApplyMoneyBackAdapter2.ViewHolder>
        implements View.OnClickListener {
    //数据源
    private List<OrderInfoData> list;
    private Context context;
    //是否显示单选框,默认false
    private boolean isshowBox = false;
    // 存储勾选框状态的map集合
    private Map<Integer, Boolean> map = new HashMap<>();
    //接口实例
    private RecyclerViewOnItemClickListener onItemClickListener;
    private List<String> mGoodsId=new ArrayList<>();
    private String mGoodsId1;
    public ApplyMoneyBackAdapter2(List<OrderInfoData> list, Context context) {
        this.list = list;
        this.context = context;
        initMap();
    }

    //初始化map集合,默认为不选中
    private void initMap() {
        for (int i = 0; i < list.size(); i++) {
            map.put(i, false);
        }
    }
    @NonNull
    @Override
    public ApplyMoneyBackAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View root = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.money_back_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(root);
        //为Item设置点击事件
        root.setOnClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ApplyMoneyBackAdapter2.ViewHolder holder, final int position) {
        final ApplyMoneyBackActivity moneyBackActivity= (ApplyMoneyBackActivity) context;
        holder.mTitle.setText(list.get(position).getTitle());
        holder.mStatus.setText(list.get(position).getStatus_title());
        holder.mCode.setText(list.get(position).getServer_id());
        holder.root.setTag(position);
        if ("1".equals(list.get(position).getIs_refund())){
            holder.mTip.setVisibility(View.GONE);
            holder.mCheck.setVisibility(View.VISIBLE);
            //设置Tag

            //设置checkBox改变监听
            holder.mCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    //用map集合保存
                    map.put(position, isChecked);
                    //获取你选中的item
                    mGoodsId.clear();
                    Map<Integer, Boolean> map = getMap();
                    for (int i = 0; i < map.size(); i++) {
                        if (map.get(i)) {
                            mGoodsId.add(list.get(i).getGoods_id());
                        }
                    }
                    HashMap<String,Object>maps=new HashMap<>();
                    mGoodsId1 = StringUtils.strip(mGoodsId.toString(), "[]");
                    if (TextUtils.isEmpty(mGoodsId1)){
                        return;
                    }
                    maps.put("goodsIDs", mGoodsId1);
                    Log.e("ApplyMoneyBackAdapter2","mGoodsId"+ mGoodsId1);
                    new AmountRefundApi().getCallBack(context, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData1) {
                            if ("1".equals(serverData1.code)){
                                try {
                                    AmountRefundBean amountRefundBean = JSONUtil.TransformSingleBean(serverData1.data, AmountRefundBean.class);
                                    float payTotalFee = Float.parseFloat(amountRefundBean.getPay_total_fee());
                                    float extract_balance_pay_money = Float.parseFloat(amountRefundBean.getExtract_balance_pay_money());
                                    float unextract_balance_pay_money = Float.parseFloat(amountRefundBean.getUnextract_balance_pay_money());
                                    float price=payTotalFee+extract_balance_pay_money+unextract_balance_pay_money;
                                    Log.e("ApplyMoneyBackAdapter2","pric---"+price);
                                    moneyBackActivity.mMoneybackPrice.setText(price+"元");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }else {
                                Toast.makeText(context,serverData1.message,Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });
            // 设置CheckBox的状态
            if (map.get(position) == null) {
                map.put(position, false);
            }
            holder.mCheck.setChecked(map.get(position));
        }else {
            holder.mTip.setVisibility(View.VISIBLE);
            holder.mCheck.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener != null) {
            //注意这里使用getTag方法获取数据

            onItemClickListener.onItemClickListener(v, (Integer) v.getTag());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTitle;
        private final TextView mStatus;
        private final TextView mCode;
        private final TextView mTip;
        private final CheckBox mCheck;
        private View root;

        public ViewHolder(@NonNull View root) {
            super(root);
            this.root = root;
            mTitle = root.findViewById(R.id.money_back_item_title);
            mStatus = root.findViewById(R.id.money_back_item_status);
            mCode = root.findViewById(R.id.money_back_item_code);
            mTip = root.findViewById(R.id.money_back_item_tip);
            mCheck = root.findViewById(R.id.money_back_item_check);
        }
    }

    //设置点击事件
    public void setRecyclerViewOnItemClickListener(ApplyMoneyBackAdapter2.RecyclerViewOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    //点击item选中CheckBox
    public void setSelectItem(int position) {
        //对当前状态取反
        if (map.get(position)) {
            map.put(position, false);
        } else {
            map.put(position, true);
        }
        notifyItemChanged(position);
    }

    //返回集合给MainActivity
    public Map<Integer, Boolean> getMap() {
        return map;
    }

    //接口回调设置点击事件
    public interface RecyclerViewOnItemClickListener {
        //点击事件
        void onItemClickListener(View view, int position);

    }
}
