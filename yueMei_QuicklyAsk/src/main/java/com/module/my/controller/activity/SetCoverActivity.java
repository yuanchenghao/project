package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quicklyask.activity.R;
import com.module.my.controller.adapter.SetCoverPageAdapter;
import com.module.my.controller.adapter.SetCoverRecAdapter;
import com.quicklyask.view.RounRecyclerView;

import org.apache.commons.lang.StringUtils;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * 视频封面选取页
 *
 * @author 裴成浩
 */
public class SetCoverActivity extends BaseActivity {

    @BindView(id = R.id.activity_set_cover)
    private LinearLayout actCover;
    @BindView(id = R.id.ll_cover_title)
    private LinearLayout coverTitle;
    @BindView(id = R.id.ll_cover_bottom)
    private LinearLayout coverBottom;
    @BindView(id = R.id.iv_cover_back)
    private ImageView ivBack;
    @BindView(id = R.id.tv_cover_determine)
    private TextView tvDetermine;
    @BindView(id = R.id.vp_set_cover)
    private ViewPager mPager;
    @BindView(id = R.id.rv_set_cover)
    private RounRecyclerView mRecycler;
    private String selectPath;

    private ArrayList<String> imgPaths = new ArrayList<>();
    private SetCoverRecAdapter mRecAdapter;

    private boolean isScaleType = false;
    long aaa = 1;
    public int typePos = 0;
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    setmRecycler();
                    setmPager();
                    break;
            }
        }
    };
    public String imgPath = "";
    private String TAG = "SetCoverActivity";


    @Override
    public void setRootView() {
        setContentView(R.layout.activity_set_cover);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        new Search_photo().start();
    }

    private void initView() {
        selectPath = getIntent().getStringExtra("selectNum");
        imgPath = getIntent().getStringExtra("imgCoverPath");

        //返回按钮
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("imgPath", imgPath);
                setResult(79, intent);
                finish();
            }
        });

        //确定按钮
        tvDetermine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("imgPath", imgPath);
                setResult(79, intent);
                finish();
            }
        });
    }

    /**
     * 设置RecyclerView的数据
     */
    private void setmRecycler() {
        //设置布局管理器
        mRecycler.setLayoutManager(new LinearLayoutManager(SetCoverActivity.this, LinearLayoutManager.HORIZONTAL, false));
        ((DefaultItemAnimator) mRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
        //设置适配器
        mRecAdapter = new SetCoverRecAdapter(SetCoverActivity.this, imgPaths, typePos);
        mRecycler.setAdapter(mRecAdapter);

        mRecycler.scrollToPosition(typePos);

        //item点击事件
        mRecAdapter.setOnItemClickListener(new SetCoverRecAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                mRecAdapter.notifyItemChanged(typePos);
                mRecAdapter.setTypePos(pos);
                mRecAdapter.notifyItemChanged(pos);
                Log.e("GGG", "pos == " + pos);
                if (typePos > pos) {                 //下一个
                    if (pos > 2) {
                        mRecycler.scrollToPosition(pos - 2 < 0 ? 0 : pos - 2);
                    }
                } else {                                 //上一个
                    if (pos > 2) {
                        mRecycler.scrollToPosition(pos + 3 > imgPaths.size() - 1 ? imgPaths.size() - 1 : pos + 3);
                    }
                }

                mPager.setCurrentItem(pos);

                imgPath = imgPaths.get(pos);
            }
        });
    }

    /**
     * 设置ViewPager的数据
     */
    private void setmPager() {
        SetCoverPageAdapter mPageAdapter = new SetCoverPageAdapter(SetCoverActivity.this, imgPaths, isScaleType);
        mPager.setAdapter(mPageAdapter);

        mPager.setCurrentItem(typePos);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mRecAdapter.notifyItemChanged(typePos);
                mRecAdapter.setTypePos(position);
                mRecAdapter.notifyItemChanged(position);
                Log.e("GGG", "position == " + position);
                if (typePos > position) {                 //下一个
                    if (position > 2) {
                        mRecycler.scrollToPosition(position - 2 < 0 ? 0 : position - 2);
                    }
                } else {                                 //上一个
                    if (position > 2) {
                        mRecycler.scrollToPosition(position + 3 > imgPaths.size() - 1 ? imgPaths.size() - 1 : position + 3);
                    }
                }

                imgPath = imgPaths.get(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    /**
     * 获取图片是一个耗时的操作，所以必须在分线程中，避免堵塞主线程
     */
    class Search_photo extends Thread {
        @Override
        public void run() {
            getPicture();
        }
    }

    /**
     * 获取视频的每一帧，并保存到本地
     */
    public void getPicture() {
        //将路径实例化为一个文件对象
        File file = new File(selectPath);
        //判断对象是否存在，不存在的话给出Toast提示
        if (file.exists()) {
            //提供统一的接口用于从一个输入媒体中取得帧和元数据
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(selectPath);
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

            //视频一共多少毫秒
            int seconds = Integer.valueOf(time);
            int interval = seconds / 10;                           //间隔多少秒截取一个张图
            ArrayList<Integer> interceptPoint = new ArrayList<>();      //截图时间点集合
            int times = 0;
            for (int i = 1; i < seconds; i += interval) {
                if (interceptPoint.size() < 10) {
                    interceptPoint.add(times += interval);
                }
            }
            String name = new File(selectPath).getName();
            name = StringUtils.substringBefore(name, ".");
            String pathS = Environment.getExternalStorageDirectory().toString()
                    + "/YueMeiImage/VideoThumbnail" + name;                           //创建视频缩略图的路径

            File path1 = new File(pathS);   // 建立这个路径的文件或文件夹
            if (!path1.exists()) {      // 如果不存在，就建立文件夹
                path1.mkdirs();
                savePicture(retriever, interceptPoint, pathS, seconds);
            } else {
                File[] files = path1.listFiles();
                int length = files.length;
                if (length != 10 && length != 0) {           //不是正好10张，或者不是一张也没有
                    for (int i = 0; i < files.length; i++) {
                        File file1 = files[i];
                        file1.delete();
                    }
                }

                if (length != 10) {                                         //如果不是一共正好10张，那么重新拆解视频
                    savePicture(retriever, interceptPoint, pathS, seconds);
                } else {
                    Bitmap bitmap1 = retriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                    int height = bitmap1.getHeight();
                    int width = bitmap1.getWidth();
                    isScaleType = height > width;
                    bitmap1.recycle();
                }
            }

            //获取图片路径保存到集合中
            File[] files = path1.listFiles();
            for (int i = 0; i < files.length; i++) {
                imgPaths.add(files[i].getPath());
            }

            mHandler.sendEmptyMessage(1);
        } else {
            Toast.makeText(SetCoverActivity.this, "视频文件不存在了", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 保存图片
     *
     * @param retriever
     * @param interceptPoint
     * @param pathS
     * @param seconds:视频总时长
     */
    private void savePicture(MediaMetadataRetriever retriever, ArrayList<Integer> interceptPoint, String pathS, int seconds) {
        Log.e(TAG, "interceptPoint == " + interceptPoint.toString());
        for (int i = 0; i < interceptPoint.size(); i++) {
            int time = interceptPoint.get(i) > seconds ? seconds : interceptPoint.get(i);
            Log.e(TAG, "seconds == " + seconds);
            Log.e(TAG, "time == " + time);
            Bitmap bitmap1 = retriever.getFrameAtTime(time * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);        //第一个参数只能是微秒而不是毫秒
//            Bitmap bitmap1 = retriever.getFrameAtTime(time, MediaMetadataRetriever.OPTION_NEXT_SYNC);
            if (bitmap1 != null) {


                int height = bitmap1.getHeight();
                int width = bitmap1.getWidth();

                Log.e(TAG, "bitmap1 == " + bitmap1);

                Bitmap bitmap;
                if (height > width) {
                    bitmap = Bitmap.createBitmap(bitmap1, 0, 88, width, height - 88 - 246);
                    isScaleType = true;
                } else {
                    bitmap = bitmap1;
                    isScaleType = false;
                }

                Log.e(TAG, "bitmap == " + bitmap);
                Log.e(TAG, "bitmap1 == " + bitmap1);
                String pathPic = pathS + "/yuemei_" + i + System.currentTimeMillis() + ".jpg";

                FileOutputStream fos;
                try {
                    fos = new FileOutputStream(pathPic);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                bitmap1.recycle();
                bitmap.recycle();
            }
        }
    }

    /**
     * 监听Back键按下事件
     * 注意:
     * 返回值表示:是否能完全处理该事件
     * 在此处返回false,所以会继续传播该事件.
     * 在具体项目中此处的返回值视情况而定.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            Intent intent = new Intent();
            intent.putExtra("imgPath", imgPath);
            setResult(79, intent);
            finish();

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }
}
