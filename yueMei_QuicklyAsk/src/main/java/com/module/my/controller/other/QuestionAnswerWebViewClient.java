package com.module.my.controller.other;

import android.content.Intent;
import android.util.Log;

import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.my.controller.activity.QuestionAnswerActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;

/**
 * Created by 裴成浩 on 2018/1/22.
 */

public class QuestionAnswerWebViewClient  implements BaseWebViewClientCallback {

    private final QuestionAnswerActivity mActivity;
    private final Intent intent;
    private String uid;
    private String TAG = "QuestionAnswer";

    public QuestionAnswerWebViewClient(QuestionAnswerActivity mActivity) {
        this.mActivity = mActivity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }
    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "6342":// 跳转到问题详情

                String question_id6342 = obj.getString("question_id");
                Log.e(TAG, "跳转到问题详情" + question_id6342);

                String url6342 = FinalConstant.baseUrl + FinalConstant.VER + "/taoask/info/";

                Log.e(TAG, "url == " + url6342);
                mActivity.pageJumpManager.jumpToQuestionDetailsActivity(url6342,"0",question_id6342);

                break;
            case "6343": // 当前页面跳转

                String link = obj.getString("link");
                String flag = obj.getString("flag");
                Log.e(TAG, "当前页面跳转" + link);
                String url = FinalConstant.baseUrl + FinalConstant.VER + link;
                mActivity.LodUrl(url,flag);

                break;
        }
    }

}
