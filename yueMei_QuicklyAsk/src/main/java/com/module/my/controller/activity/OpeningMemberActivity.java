package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.home.controller.activity.WebUrlTitleActivity;
import com.module.my.controller.other.OpeningMemberManage;
import com.module.my.model.api.SaveorderiApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.SaveorderiData;
import com.module.my.model.bean.ShenHeData;
import com.module.my.view.view.BannedClickRadioButton;
import com.quicklyask.activity.R;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 开通会员，续费会员页面
 *
 * @author 裴成浩
 */
public class OpeningMemberActivity extends YMBaseActivity {

    @BindView(R.id.opening_member_top)
    CommonTopBar mTop;                                              //标题
    @BindView(R.id.opening_member_title)
    TextView mTitle;                                              //开通服务标题
    @BindView(R.id.opening_member_model)
    TextView mModel;                                                //开通模式
    @BindView(R.id.opening_member_service_price)
    TextView mServicePrice;                                         //服务总价
    @BindView(R.id.opening_member_preferential_price_line)
    View mPreferentialPriceLine;                                //优惠金额上边分割线
    @BindView(R.id.opening_member_preferential_price_click)
    LinearLayout mPreferentialPriceClick;                                    //优惠金额容器
    @BindView(R.id.opening_member_preferential_price)
    TextView mPreferentialPrice;                                    //优惠金额
    //    @BindView(R.id.opening_member_wallet_balance)
//    TextView mWalletBalance;                                        //钱包余额支付金额
//    @BindView(R.id.opening_member_wallet_no_off)
//    SwitchButton mWalletNoOff;                                      //钱包余额支付开关
    @BindView(R.id.opening_member_remaining_pay)
    TextView mRemainingPay;                                         //还需要支付的金额
    @BindView(R.id.opening_member_zhifubao_click)
    LinearLayout zhifubaoLl;                                        //支付宝点击
    @BindView(R.id.opening_member_zhifubao_rb)
    BannedClickRadioButton mZhifubaoRb;                             //支付宝选中图
    @BindView(R.id.opening_member_weixin_click)
    LinearLayout weixinoLl;                                        //微信点击
    @BindView(R.id.opening_member_weixin_rb)
    BannedClickRadioButton mWeixinRb;                               //微信选中图
    @BindView(R.id.opening_member_yinlian_click)
    LinearLayout yinlianLl;                                        //银联点击
    @BindView(R.id.opening_member_yinlian_rb)
    BannedClickRadioButton mYinlianRb;                              //银行卡选中图

    @BindView(R.id.opening_member_submit)
    Button mSubmit;                                                 //开通/续费

    private String TAG = "OpeningMemberActivity";
    private SaveorderiApi saveorderiApi;

    private String dataService;                     //订单标题
    private String orderId;                         //订单id
    private String dataMoney;
    private OpeningMemberManage memberManage;
    public static final String PAY_STATE = "pay_state";
    private ShenHeApi shenHeApi;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_opening_member;
    }

    @Override
    protected void initView() {
        String flag = getIntent().getStringExtra("flag");
        Log.e(TAG, "flag ===" + flag);

        if ("renewals".equals(flag)) {
            mTop.setCenterText("续费PLUS会员");
            mSubmit.setText("续费");
        } else {
            mTop.setCenterText("开通PLUS会员");
            mSubmit.setText("开通");
        }

        //是否是第一次加载
        mFunctionManager.saveInt(PAY_STATE, 0);

//        //钱包余额支付
//        mWalletNoOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                Log.e(TAG, "isChecked == " + isChecked);
//                if (!isChecked) {            //打开
//                    Log.e(TAG, "打开");
//                }
//            }
//        });

        //


        memberManage = OpeningMemberManage.getInstance(mContext);

        //支付宝支付状态回调
        memberManage.setPayStatusCallback(new OpeningMemberManage.PayStatusCallback() {
            @Override
            public void PayStatus(final String code) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (TextUtils.equals(code, "9000")) {
                            mFunctionManager.showShort("订单支付成功");
                            onBackPressed();
                        } else {
                            if (TextUtils.equals(code, "8000")) {
                                mFunctionManager.showShort("正在处理中，支付结果未知（有可能已经支付成功），请查询商户订单列表中订单的支付状态");
                            } else if (TextUtils.equals(code, "4000")) {
                                mFunctionManager.showShort("订单支付失败");
                            } else if (TextUtils.equals(code, "5000")) {
                                mFunctionManager.showShort("重复请求");
                            } else if (TextUtils.equals(code, "6001")) {
                                mFunctionManager.showShort("中途取消了订单");
                            } else if (TextUtils.equals(code, "6002")) {
                                mFunctionManager.showShort("网络连接出错，请检查网络");
                            } else if (TextUtils.equals(code, "6004")) {
                                mFunctionManager.showShort("支付结果未知（有可能已经支付成功），请查询商户订单列表中订单的支付状态");
                            } else {
                                mFunctionManager.showShort("订单支付失败");
                            }
                        }

                        onBackPressed();
                    }
                });
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        //微信支付状态回调
        switch (mFunctionManager.loadInt(PAY_STATE, 0)) {
            case 1:
                mFunctionManager.showShort("支付成功");
                onBackPressed();
                break;
            case 2:
                mFunctionManager.showShort("支付失败");
                break;
            case 3:
                mFunctionManager.showShort("取消了支付");
                break;
        }
    }

    @Override
    protected void initData() {
        saveorderiApi = new SaveorderiApi();
        shenHeApi = new ShenHeApi();

        initShenHe();
        generateOrders();
    }

    /**
     * 初始化第三方支付
     */
    private void initShenHe() {
        shenHeApi.getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {
            @Override
            public void onSuccess(ShenHeData sdata) {

                String alipay = sdata.getAlipay();
                if ("1".equals(alipay)) {
                    zhifubaoLl.setVisibility(View.GONE);
                } else {
                    zhifubaoLl.setVisibility(View.VISIBLE);
                }
                String wxpay = sdata.getWxpay();
                if ("1".equals(wxpay)) {
                    weixinoLl.setVisibility(View.GONE);
                } else {
                    weixinoLl.setVisibility(View.VISIBLE);
                }

                String bankpay = sdata.getBankpay();
                if ("1".equals(bankpay)) {
                    yinlianLl.setVisibility(View.GONE);
                } else {
                    yinlianLl.setVisibility(View.VISIBLE);
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        //银联支付状态回调
        String str = data.getExtras().getString("pay_result");
        if (str.equalsIgnoreCase("success")) {
            mFunctionManager.showShort("支付成功");
            onBackPressed();
        } else if (str.equalsIgnoreCase("fail")) {
            mFunctionManager.showShort("支付失败");
        } else if (str.equalsIgnoreCase("cancel")) {
            mFunctionManager.showShort("取消了支付");
        }
    }

    /**
     * 支付宝点击
     */
    @OnClick(R.id.opening_member_zhifubao_click)
    public void onZhifubaoClicked() {
        chooseOrder(1);
    }

    /**
     * 微信点击
     */
    @OnClick(R.id.opening_member_weixin_click)
    public void onWeixinClicked() {
        chooseOrder(2);
    }

    /**
     * 银联点击
     */
    @OnClick(R.id.opening_member_yinlian_click)
    public void onYinlianClicked() {
        chooseOrder(3);
    }

    /**
     * 协议点击
     */
    @OnClick(R.id.opening_member_agreement)
    public void onAgreementClicked() {
        Intent intent = new Intent(mContext, WebUrlTitleActivity.class);
        intent.putExtra("link", "/member/protocol/");
        startActivity(intent);
    }


    /**
     * 提交按钮点击
     */
    @OnClick(R.id.opening_member_submit)
    public void onSubmitClicked() {
        if (!TextUtils.isEmpty(orderId)) {
            switch (orderPay()) {
                case 0:
                    mFunctionManager.showShort("请选择支付方式");
                    break;
                case 1:
                    memberManage.payALiPay(dataService, orderId, dataMoney);
                    break;
                case 2:
                    memberManage.payWeixin(dataService, orderId, dataMoney);
                    break;
                case 3:
                    memberManage.payYinlian(dataService, orderId, dataMoney);
                    break;
            }

//        //钱包余额支付判断
//        if (mWalletNoOff.isChecked()) {
//            Log.e(TAG, "关闭");
//        } else {
//            Log.e(TAG, "打开");
//        }
        }
    }

    /**
     * 生成订单
     */
    private void generateOrders() {
        saveorderiApi.getCallBack(mContext, saveorderiApi.getSaveorderiHashMap(), new BaseCallBackListener<SaveorderiData>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(SaveorderiData data) {

                //开通服务标题
                dataService = data.getService();
                mTitle.setText(dataService);
                //服务总价
                mServicePrice.setText("￥" + data.getPrice());
                //优惠金额
                if (!"0".equals(data.getMember_lijian_money())) {
                    mPreferentialPrice.setText("-￥" + data.getMember_lijian_money());
                    mPreferentialPriceLine.setVisibility(View.VISIBLE);
                    mPreferentialPriceClick.setVisibility(View.VISIBLE);
                } else {
                    mPreferentialPriceLine.setVisibility(View.GONE);
                    mPreferentialPriceClick.setVisibility(View.GONE);
                }

                //需支付
                dataMoney = data.getMoney();
                mRemainingPay.setText("￥" + dataMoney);

                orderId = data.getOrder_id();

            }
        });
    }

    /**
     * 选择下单方式
     *
     * @param type 1：支付宝，2：微信，3：银联
     */
    private void chooseOrder(int type) {
        switch (type) {
            case 1:
                mZhifubaoRb.setChecked(true);
                mWeixinRb.setChecked(false);
                mYinlianRb.setChecked(false);
                break;
            case 2:
                mZhifubaoRb.setChecked(false);
                mWeixinRb.setChecked(true);
                mYinlianRb.setChecked(false);
                break;
            case 3:
                mZhifubaoRb.setChecked(false);
                mWeixinRb.setChecked(false);
                mYinlianRb.setChecked(true);
                break;
        }
    }

    /**
     * 判断下单方式
     *
     * @return 0：未选中支付方式 1：支付宝，2：微信，3：银联
     */
    private int orderPay() {

        if (mZhifubaoRb.isChecked()) {
            return 1;
        } else if (mWeixinRb.isChecked()) {
            return 2;
        } else if (mYinlianRb.isChecked()) {
            return 3;
        } else {
            return 0;
        }
    }

}
