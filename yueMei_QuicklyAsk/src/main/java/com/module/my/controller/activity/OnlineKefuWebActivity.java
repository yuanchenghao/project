package com.module.my.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * Created by dwb on 17/1/10.
 */
public class OnlineKefuWebActivity extends BaseActivity {

    private final String TAG = "WebUrlTitleActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.basic_web_top)
    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private Activity mContex;

    public JSONObject obj_http;

    private String url;
    private String title;

    private int bTheight;
    private int windowsH;
    private int statusBarHeight;
    private int windowsW;

    @BindView(id = R.id.sildingFinishLayout)
    private RelativeLayout biaoView;

    @BindView(id = R.id.all_content)
    private BaseWebViewClientMessage baseWebViewClientMessage;


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_peifu_basic_web);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = OnlineKefuWebActivity.this;

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);

        windowsH = Cfg.loadInt(mContex, FinalConstant.WINDOWS_H, 0);
        windowsW = Cfg.loadInt(mContex, FinalConstant.WINDOWS_W, 0);

        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        biaoView.measure(w, h);
        bTheight =biaoView.getMeasuredHeight();

        Rect rectangle= new Rect();
        Window window= getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        statusBarHeight= rectangle.top;


        Intent it = getIntent();
        url = it.getStringExtra("link");
        title = it.getStringExtra("title");

        url = FinalConstant.baseUrl + FinalConstant.VER + url;

        mTop.setCenterText(title);

        initWebview();

        LodUrl1(url);
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) docDetWeb.getLayoutParams();
        linearParams.height = windowsH-bTheight-statusBarHeight*2-60;
        linearParams.weight = windowsW;
        docDetWeb.setLayoutParams(linearParams);

        contentWeb.addView(docDetWeb);
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();

        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

    }
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

}