package com.module.my.controller.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

/**
 * 我的粉丝适配器
 * Created by 裴成浩 on 2018/4/12.
 */

public class MyFocusPagerAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private List<Fragment> fragmentList;
    private List<String> titleList;

    public MyFocusPagerAdapter(Context mContext, FragmentManager fm, List<Fragment> mFragmentList, List<String> mPageTitleList) {
        super(fm);
        this.mContext = mContext;
        this.fragmentList = mFragmentList;
        this.titleList = mPageTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }
}
