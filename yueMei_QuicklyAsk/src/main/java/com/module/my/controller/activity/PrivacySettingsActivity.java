package com.module.my.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.module.community.web.WebUtil;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.view.YueMeiDialog;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import butterknife.BindView;

import static android.Manifest.permission.WRITE_CALENDAR;

/**
 * 文 件 名: PrivacySettingsActivity
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-18 19:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class PrivacySettingsActivity extends YMBaseActivity {
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.ll_location)
    LinearLayout ll_location;
    @BindView(R.id.ll_camera)
    LinearLayout ll_camera;
    @BindView(R.id.ll_album)
    LinearLayout ll_album;
    @BindView(R.id.ll_notice)
    LinearLayout ll_notice;
    @BindView(R.id.ll_call)
    LinearLayout ll_call;
    @BindView(R.id.ll_calendar)
    LinearLayout ll_calendar;
    @BindView(R.id.ll_audio)
    LinearLayout ll_audio;



    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.tv_camera)
    TextView tv_camera;
    @BindView(R.id.tv_album)
    TextView tv_album;
    @BindView(R.id.tv_notice)
    TextView tv_notice;
    @BindView(R.id.tv_call)
    TextView tv_call;
    @BindView(R.id.tv_calendar)
    TextView tv_calendar;
    @BindView(R.id.tv_audio)
    TextView tv_audio;

    @BindView(R.id.tv_location_rule)
    TextView tv_location_rule;
    @BindView(R.id.tv_camera_rule)
    TextView tv_camera_rule;
    @BindView(R.id.tv_album_rule)
    TextView tv_album_rule;
    @BindView(R.id.tv_notice_rule)
    TextView tv_notice_rule;
    @BindView(R.id.tv_call_rule)
    TextView tv_call_rule;
    @BindView(R.id.tv_calendar_rule)
    TextView tv_calendar_rule;
    @BindView(R.id.tv_audio_rule)
    TextView tv_audio_rule;




    private LocationManager lm;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_privacy_setting;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;
        setMultiOnClickListener(iv_back, ll_location, ll_camera, ll_album, ll_notice, tv_location_rule, tv_camera_rule, tv_album_rule, tv_notice_rule,tv_call_rule,ll_call,ll_audio,ll_calendar,tv_calendar_rule,tv_audio_rule);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        updataUi();
    }

    @SuppressLint("NewApi")
    private void updataUi() {
        if (lacksPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            tv_location.setText("去设置");
        } else {
            tv_location.setText("已开启");
        }
        if (lacksPermission(mContext, Manifest.permission.CAMERA)) {
            tv_camera.setText("去设置");
        } else {
            tv_camera.setText("已开启");
        }
        if (lacksPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            tv_album.setText("去设置");
        } else {
            tv_album.setText("已开启");
        }
        if (!NotificationsUtils.isNotificationEnabled(this)) {
            tv_notice.setText("去设置");
        } else {
            tv_notice.setText("已开启");
        }
        if (lacksPermission(mContext, Manifest.permission.CALL_PHONE)) {
            tv_call.setText("去设置");
        } else {
            tv_call.setText("已开启");
        }
        if (lacksPermission(mContext, Manifest.permission.READ_CALENDAR) && lacksPermission(mContext, WRITE_CALENDAR)) {
            tv_calendar.setText("去设置");
        } else {
            tv_calendar.setText("已开启");
        }
        if (lacksPermission(mContext, Manifest.permission.RECORD_AUDIO)) {
            tv_audio.setText("去设置");
        } else {
            tv_audio.setText("已开启");
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_location:
                if (tv_location.getText() != null && tv_location.getText().equals("去设置")) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            tv_location.setText("已开启");
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            tv_location.setText("去设置");
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.ll_camera:
                if (tv_camera.getText() != null && tv_camera.getText().equals("去设置")) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            tv_camera.setText("已开启");
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            tv_camera.setText("去设置");
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.ll_album:
                if (tv_album.getText() != null && tv_album.getText().equals("去设置")) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            tv_album.setText("已开启");
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            tv_album.setText("去设置");
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.ll_notice:
                if (!NotificationsUtils.isNotificationEnabled(this)) {
                    final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "检测到您没有打开通知权限，是否去打开?", "取消", "确定");
                    yueMeiDialog.setCanceledOnTouchOutside(false);
                    yueMeiDialog.show();
                    yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                        @Override
                        public void leftBtnClick() {
                            yueMeiDialog.dismiss();
                        }

                        @Override
                        public void rightBtnClick() {
                            yueMeiDialog.dismiss();
                            JumpSetting();
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.ll_call:
                if (tv_call.getText() != null && tv_call.getText().equals("去设置")) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            tv_call.setText("已开启");
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            tv_call.setText("去设置");
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.ll_calendar:
                if (tv_calendar.getText() != null && tv_calendar.getText().equals("去设置")) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.READ_CALENDAR,WRITE_CALENDAR).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            tv_calendar.setText("已开启");
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            tv_calendar.setText("去设置");
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.ll_audio:
                if (tv_audio.getText() != null && tv_audio.getText().equals("去设置")) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.RECORD_AUDIO).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            tv_audio.setText("已开启");
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            tv_audio.setText("去设置");
                        }
                    });
                } else {
                    JumpSetting();
                }
                break;
            case R.id.tv_location_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/12", "权限规则");
                break;
            case R.id.tv_camera_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/13", "权限规则");
                break;
            case R.id.tv_album_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/14", "权限规则");
                break;
            case R.id.tv_notice_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/15", "权限规则");
                break;
            case R.id.tv_call_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/16", "权限规则");
                break;
            case R.id.tv_calendar_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/17", "权限规则");
                break;
            case R.id.tv_audio_rule:
                WebUtil.getInstance().startWebActivity(mContext, "https://sjapp.yuemei.com/service/repayment/type/18", "权限规则");
                break;
        }
    }

    private void JumpSetting() {
        Intent i = new Intent();
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            i.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            i.setData(Uri.fromParts("package", mContext.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            i.setAction(Intent.ACTION_VIEW);
            i.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            i.putExtra("com.android.settings.ApplicationPkgName", mContext.getPackageName());
        }
        startActivity(i);
    }

    /**
     * @param context
     */
    public static void invoke(Context context) {
        Intent intent = new Intent(context, PrivacySettingsActivity.class);
        context.startActivity(intent);
    }

    /**
     * 判断是否缺少权限
     */

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static boolean lacksPermission(Context mContexts, String permission) {
        return ContextCompat.checkSelfPermission(mContexts, permission) == PackageManager.PERMISSION_DENIED;
    }

    public static class NotificationsUtils {

        private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
        private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

        @SuppressLint("NewApi")
        public static boolean isNotificationEnabled(Context context) {

            AppOpsManager mAppOps =
                    (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);

            ApplicationInfo appInfo = context.getApplicationInfo();
            String pkg = context.getApplicationContext().getPackageName();
            int uid = appInfo.uid;
            Class appOpsClass = null;

            /* Context.APP_OPS_MANAGER */
            try {
                appOpsClass = Class.forName(AppOpsManager.class.getName());

                Method checkOpNoThrowMethod =
                        appOpsClass.getMethod(CHECK_OP_NO_THROW,
                                Integer.TYPE, Integer.TYPE, String.class);

                Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
                int value = (Integer) opPostNotificationValue.get(Integer.class);

                return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) ==
                        AppOpsManager.MODE_ALLOWED);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }


}
