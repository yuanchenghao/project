package com.module.my.controller.activity;

import org.kymjs.aframe.ui.activity.BaseActivity;

import com.baidu.mobstat.StatService;
import com.quicklyask.activity.R;

/**
 * 提交问题、体验成功
 * 
 * @author Rubin
 * 
 */
public class SubmitSuccess2Activity extends BaseActivity {

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_submit_success2);
	}

	public void onResume() {
		super.onResume();
		StatService.onResume(this);
	}

	public void onPause() {
		super.onPause();
		StatService.onPause(this);
	}
}
