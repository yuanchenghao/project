package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.home.controller.adapter.SignInAdapter;
import com.module.my.model.api.InitializeHttpApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.SignInResult;
import com.quicklyask.service.NotificationService;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.PopUpwindowLayout;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;


/**
 * 签到页
 *
 * @author 裴成浩
 */
public class SignInActivity extends YMBaseActivity {
    private String TAG = "SignInActivity";

    @BindView(R.id.order_time_all_ly)
    LinearLayout orderTimeAllLy;

    @BindView(R.id.banner)
    Banner banner;

    @BindView(R.id.tv_tianshu)
    TextView tvTianshu;

    @BindView(R.id.switch_if_sign)
    Switch switchIfSign;

    @BindView(R.id.rv_list_sign)
    RecyclerView mRecyclerView;

    @BindView(R.id.btn_qiandao)
    Button btnQiandao;

    @BindView(R.id.tv_yanzhigs)
    TextView tvYanzhigs;

    @BindView(R.id.ll_guize)
    LinearLayout llGuize;

    @BindView(R.id.ll_cjjl)
    LinearLayout llCjjl;

    @BindView(R.id.ll_wdyzb)
    LinearLayout llWdyzb;

    private SignInAdapter mAdapter;

    private TextView tvItemSign;
    private ImageView ivItemSignQd;

    private int decode;
    String dayNum = "0";

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:                 //签到后消息动画效果
                    if (!"".equals(mSignInResult.getIsDaySignin().get(decode - 1).getUrl())) {
                        btnQiandao.setEnabled(true);
                        btnQiandao.setText("去抽奖");
                    } else {
                        btnQiandao.setEnabled(false);
                        String s = decode == isDaySignin.size() ? "二十八天全部签完" : "已签到，明天可得" + isDaySignin.get(decode).getIntegral() + "颜值币";
                        btnQiandao.setText(s);
                    }

                    tvTianshu.setText(decode + "");

                    mAdapter.notifyItemChanged(decode - 1);
                    mHandler.sendEmptyMessageDelayed(3, 100);

                    break;
                case 3:

                    HashMap<String, SignInAdapter.ViewHolder> map = mAdapter.getHolder();
                    SignInAdapter.ViewHolder viewHolder1 = map.get((decode - 1) + "");
                    if (viewHolder1 != null) {
                        ivItemSign = viewHolder1.ivItemSign;
                        tvItemSign = viewHolder1.tvItemSign;
                        ivItemSignQd = viewHolder1.ivItemSignQd;
                    }

                    tvItemSign.setVisibility(View.GONE);
                    ivItemSignQd.setVisibility(View.GONE);
                    startShakeByViewAnim(ivItemSign, 5.0f, 250);
                    break;
            }

        }
    };
    private SignInResult.DataBean mSignInResult;
    private List<SignInResult.DataBean.IsDaySigninBean> isDaySignin;
    private String isSignin;
    private PopupWindow popupWindow;
    private ImageView ivItemSign;
    private PopupWindow popupWindow1;
    private String integral11 = "0";


    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign;
    }

    @Override
    protected void initView() {

        String localDelivery = mFunctionManager.loadStr(FinalConstant.SIGN_FLAG, "0");
        Log.e(TAG, "localDelivery == " + localDelivery);
        if ("0".equals(localDelivery)) {      //默认关闭

            switchIfSign.setChecked(false);

        } else {

            switchIfSign.setChecked(true);

        }

        //签到提示
        switchIfSign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    mFunctionManager.saveStr(FinalConstant.SIGN_FLAG, "1");

                    if (!NotificationService.isRunning(SignInActivity.this)) {                           //如果这个服务不存在
                        Intent startIntent = new Intent(SignInActivity.this, NotificationService.class);
                        startService(startIntent);                                                  //开启本地推送服务
                    }

                    showDialogExitEdit3("签到提示已开启", "每天11:00之前没有签到，会收到消息提示");

                } else {
                    mFunctionManager.saveStr(FinalConstant.SIGN_FLAG, "0");
                    if (NotificationService.isRunning(SignInActivity.this)) {                    //如果这个服务存在
                        Intent startIntent = new Intent(SignInActivity.this, NotificationService.class);
                        stopService(startIntent);                                                       //关闭本地推送服务
                    }
                    showDialogExitEdit3("签到提示已关闭", "关闭消息提示可能会错过更多颜值币~");
                }
            }
        });

        //签到按钮点击事件
        btnQiandao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Log.e("TAG", "isSignin == " + isSignin);
                Log.e("TAG", "decode == " + decode);
                if ("1".equals(isSignin)) {                 //已签到
                    if (!"".equals(isDaySignin.get(decode - 1).getUrl())) {
                        String url = isDaySignin.get(decode - 1).getUrl();
                        Log.e("TAG", "url == " + url);
                        WebUrlTypeUtil.getInstance(SignInActivity.this).urlToApp(url, "31", "0");
                    } else {
                        sumitHttpCode1();
                    }
                } else {
                    sumitHttpCode1();
                }
            }
        });


        //抽奖记录
        llCjjl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("TAG", "url === " + mSignInResult.getDui_record());
                WebUrlTypeUtil.getInstance(SignInActivity.this).urlToApp(mSignInResult.getDui_record(), "32", "0");
            }
        });

        //我的颜值币
        llWdyzb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(SignInActivity.this, MyPointsActivity.class));
            }
        });
    }

    @Override
    protected void initData() {
        initializeHttp();
    }

    /**
     * 初始化接口
     */
    private void initializeHttp() {

        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", Utils.getUid());

        new InitializeHttpApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {


            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {

                    mSignInResult = JSONUtil.TransformSignResult(serverData.data);
                    isDaySignin = mSignInResult.getIsDaySignin();
                    isSignin = mSignInResult.getIs_signin();
                    final List<SignInResult.DataBean.TopImgBean> topImg = mSignInResult.getTopImg();

                    List<String> imags = new ArrayList<>();
                    for (int i = 0; i < topImg.size(); i++) {
                        imags.add(topImg.get(i).getImg());
                    }

                    List<String> titles = new ArrayList<>();
                    for (int i = 0; i < topImg.size(); i++) {
                        titles.add(topImg.get(i).getTitle());
                    }

                    //设置banner样式
                    banner.setBannerStyle(BannerConfig.NOT_INDICATOR);
                    //设置图片加载器
                    banner.setImageLoader(new GlideImageLoader());
                    //设置图片集合
                    banner.setImages(imags);
                    //设置banner动画效果
                    banner.setBannerAnimation(Transformer.DepthPage);
                    //设置标题集合（当banner样式有显示title时）
                    banner.setBannerTitles(titles);
                    //设置自动轮播，默认为true
                    banner.isAutoPlay(true);
                    //设置轮播时间
                    banner.setDelayTime(1500);
                    //设置指示器位置（当banner模式中有指示器时）
                    banner.setIndicatorGravity(BannerConfig.CENTER);
                    //banner设置方法全部调用完毕时最后调用
                    banner.start();

                    banner.setOnBannerListener(new OnBannerListener() {
                        @Override
                        public void OnBannerClick(int position) {
                            Log.e(TAG, "topImg.get(position).getUrl() === " + topImg.get(position).getUrl());
                            if (!"".equals(topImg.get(position).getUrl())) {
                                WebUrlTypeUtil.getInstance(SignInActivity.this).urlToApp(topImg.get(position).getUrl(), "30", topImg.get(position).getQid());
                            }
                        }
                    });

                    tvTianshu.setText(mSignInResult.getContinue_signin());

                    boolean haveSign = false;           //默认28天都签到过了
                    for (int i = 0; i < isDaySignin.size(); i++) {
                        if ("0".equals(isDaySignin.get(i).getIs_signin())) {                    //如果有未签到的
                            haveSign = true;
                            if (i != 0) {                                                        //不是第0天
                                dayNum = isDaySignin.get(i - 1).getDay();
                            } else {
                                dayNum = "0";
                            }
                            break;
                        }
                    }

                    //如果28天都签到过了
                    if (!haveSign) {
                        dayNum = "28";
                    }

                    Log.e(TAG, "dayNum == " + dayNum);


                    //设置布局管理器
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(SignInActivity.this, LinearLayoutManager.HORIZONTAL, false));
                    ((DefaultItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果(是因为局部刷新的是每一个item,而不是item的部分)
                    //设置适配器
                    mAdapter = new SignInAdapter(SignInActivity.this, SignInActivity.this.isDaySignin, dayNum);
                    mRecyclerView.setAdapter(mAdapter);

                    //滚动监听
                    mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            mAdapter.setIsSignin(false);
                        }
                    });


                    String rules = mSignInResult.getRule();
                    Log.e(TAG, "rules == " + rules);

                    String[] strs = rules.split("\n");

                    for (int i = 0; i < strs.length; i++) {
                        TextView view = new TextView(mContext);
                        view.setTextColor(mContext.getResources().getColor(R.color._66));
                        view.setTextSize(getResources().getDimension(R.dimen.space_15));
                        view.setText(strs[i]);
                        view.setLineSpacing(Utils.dip2px(mContext, 9), 1);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        if (i == 0) {
                            params.setMargins(0, 0, 0, Utils.dip2px(mContext, 6));
                        } else if (i == strs.length - 1) {
                            params.setMargins(0, Utils.dip2px(mContext, 6), 0, Utils.dip2px(mContext, 15));
                        } else {
                            params.setMargins(0, Utils.dip2px(mContext, 6), 0, Utils.dip2px(mContext, 6));
                        }
                        view.setLayoutParams(params);

                        llGuize.addView(view);
                    }


                    //签到按钮文字内容
                    decode = Integer.parseInt(dayNum);
                    Log.e("TAG", "ignResult.getIs_signin() === " + isSignin);
                    if ("0".equals(isSignin)) {      //未签到
                        Log.e("TAG", "qqq");
                        btnQiandao.setEnabled(true);

                        Log.e(TAG, "isDaySignin == " + isDaySignin.size());
                        Log.e(TAG, "decode == " + decode);
                        if (!"".equals(isDaySignin.get(decode).getUrl())) {
                            btnQiandao.setText("签到抽大奖");
                        } else {
                            btnQiandao.setText("签到领" + SignInActivity.this.isDaySignin.get(decode).getIntegral() + "颜值币");
                        }

                    } else if ("1".equals(isSignin)) {                 //已签到
                        Log.e(TAG, "decode == " + decode);
                        if (!"".equals(isDaySignin.get((decode == 0 ? 1 : decode) - 1).getUrl())) {
                            Log.e("TAG", "www");
                            btnQiandao.setEnabled(true);
                            btnQiandao.setText("去抽奖");
                        } else {
                            Log.e("TAG", "eee");
                            btnQiandao.setEnabled(false);
                            btnQiandao.setText("已签到，明天可得" + SignInActivity.this.isDaySignin.get(decode).getIntegral() + "颜值币");
                        }
                    } else {
                        Log.e("TAG", "rrrr");
                    }
                    tvYanzhigs.setText(mSignInResult.getIntegral());

                    if (decode > 4) {           //滚动到的位置
                        mRecyclerView.scrollToPosition(decode - 4);
                    }
                }
            }

        });

    }


    /**
     * 动画效果
     *
     * @param view
     * @param shakeDegrees
     * @param duration
     */
    private void startShakeByViewAnim(View view, float shakeDegrees, long duration) {
        if (view == null) {
            return;
        }
        //TODO 验证参数的有效性

        //从左向右
        Animation rotateAnim = new TranslateAnimation(-shakeDegrees, shakeDegrees, 0, 0);

        rotateAnim.setDuration(duration / 4);
        rotateAnim.setRepeatMode(Animation.REVERSE);
        rotateAnim.setRepeatCount(4);


        view.startAnimation(rotateAnim);
        rotateAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tvItemSign.setVisibility(View.VISIBLE);

                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(300);
                tvItemSign.startAnimation(alphaAnimation);
                alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @TargetApi(Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        String s1 = tvYanzhigs.getText().toString();
                        if (s1 == null || "".equals(s1)) {
                            s1 = "0";
                        }

                        if (integral11 == null || "".equals(integral11)) {
                            integral11 = "0";
                        }
                        tvYanzhigs.setText(Integer.parseInt(s1) + Integer.parseInt(integral11) + "");

                        ivItemSignQd.setVisibility(View.VISIBLE);
                        if (decode != isDaySignin.size()) {                 //不是最后一天
                            String url = isDaySignin.get(decode - 1).getUrl();
                            if (!"".equals(url)) {
                                String s = Cfg.loadStr(SignInActivity.this, FinalConstant.TANCENG, "");
                                if (!"1".equals(s)) {
                                    Cfg.saveStr(SignInActivity.this, FinalConstant.TANCENG, "1");
                                    prizePopupwindows();
                                }
                            }
                        }

                        //弹提示
                        if ("1".equals(isSignin)) {
                            if (decode == 4) {
                                String s = Cfg.loadStr(SignInActivity.this, FinalConstant.TISHI, "");
                                if (!"1".equals(s)) {
                                    Cfg.saveStr(SignInActivity.this, FinalConstant.TISHI, "1");
                                    poorPopupwindows();

                                    new CountDownTimer(3000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                                        @Override
                                        public void onTick(long millisUntilFinished) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            popupWindow.dismiss();
                                        }
                                    }.start();
                                }
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    //dialog提示
    void showDialogExitEdit3(String tishi, String neirong) {
        final EditExitDialog editDialog = new EditExitDialog(SignInActivity.this, R.style.mystyle, R.layout.dialog_lingqu_daijinjuan1);
        editDialog.setCanceledOnTouchOutside(false);
        if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
            editDialog.show();
        }

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setText(tishi);

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText(neirong);

        titleTv88.setHeight(50);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("我知道了");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
                    editDialog.dismiss();
                }
            }
        });
    }


    /**
     * 签到上传接口
     */
    void sumitHttpCode1() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", "2");
        maps.put("uid", Utils.getUid());
        new SumitHttpAip().getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    integral11 = jfjyData.getIntegral();

                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);    //获取年
                    int month = cal.get(Calendar.MONTH) + 1;   //获取月份，0表示1月份
                    int day = cal.get(Calendar.DAY_OF_MONTH);    //获取当前天数
                    String dates = year + "" + month + "" + day;
                    Log.e(TAG, "dates === " + dates);
                    Cfg.saveStr(SignInActivity.this, FinalConstant.CURRENTDATE, dates);

                    //动画效果
                    MyToast.makeTextToast1(SignInActivity.this, "签到成功", 1000).show();

                    //动画效果
                    Log.e("TAG", "decode -- 111 ===" + decode);
                    decode = decode + 1;
                    isSignin = "1";
                    isDaySignin.get(decode - 1).setIs_signin("1");      //设置
                    mAdapter.setIsSignin(true);
                    mHandler.sendEmptyMessage(1);
                }
            }
        });
    }

    public class GlideImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            if (Util.isOnMainThread()) {
                Glide.with(mContext).load((String) path).into(imageView);
            }


        }
    }

    /**
     * 提示抽奖弹层
     */
    public void prizePopupwindows() {

        final View view = View.inflate(this, R.layout.pop_lucky_draw_project, null);

        popupWindow1 = new PopupWindow(view, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        popupWindow1.setFocusable(true);

        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        popupWindow1.setBackgroundDrawable(dw);

        // 设置popWindow的显示和消失动画
        popupWindow1.setAnimationStyle(R.anim.fade_ins);
        if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
            popupWindow1.showAtLocation(orderTimeAllLy, Gravity.CENTER, 0, 0);
        }

        Button btnPopupChouj = view.findViewById(R.id.btn_popup_chouj);
        LinearLayout llPopupDel = view.findViewById(R.id.ll_popup_del);

        llPopupDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
                    popupWindow1.dismiss();
                }
            }
        });

        //签到抽奖
        btnPopupChouj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = isDaySignin.get(decode - 1).getUrl();
                if (!"".equals(url)) {
                    WebUrlTypeUtil.getInstance(SignInActivity.this).urlToApp(url, "31", "0");
                    if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
                        popupWindow1.dismiss();
                    }
                }

            }
        });
    }

    /**
     * 提示还差几天抽奖
     */
    public void poorPopupwindows() {

        ArrayList<String> titles = new ArrayList<>();
        titles.add(" ");
        View view = getLayoutInflater().inflate(R.layout.pop_poor_draw_project, null);
        PopUpwindowLayout popUpwindowLayout = view.findViewById(R.id.llayout_popupwindow);
        popUpwindowLayout.initViews(SignInActivity.this, titles, false);
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int popupWidth = view.getMeasuredWidth();
        int popupHeight = view.getMeasuredHeight();
        int[] location = new int[2];
        // 允许点击外部消失
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(false);
        popupWindow.setFocusable(false);
        // 获得位置

        HashMap<String, SignInAdapter.ViewHolder> map = mAdapter.getHolder();
        SignInAdapter.ViewHolder viewHolder1 = map.get("6");

        viewHolder1.view.getLocationOnScreen(location);
        popupWindow.setAnimationStyle(R.style.mypopwindow_anim_style);
        if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
            popupWindow.showAtLocation(viewHolder1.view, Gravity.NO_GRAVITY, (location[0] + viewHolder1.view.getWidth() / 2) - popupWidth / 2 - 65, location[1] - popupHeight + 20);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (SignInActivity.this != null && !SignInActivity.this.isFinishing()) {
            popupWindow1.dismiss();
            popupWindow1 = null;
            popupWindow.dismiss();
            popupWindow = null;
        }
        if (Util.isOnMainThread() && !this.isFinishing()) {
            Glide.with(MyApplication.getContext()).pauseRequests();
        }
    }
}
