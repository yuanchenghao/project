package com.module.my.controller.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2017/7/4.
 */

public class SetCoverPageAdapter extends PagerAdapter {

    private final Context mContext;
    private final ArrayList<String> mImgPaths;
    private final boolean isScaleType;

    public SetCoverPageAdapter(Context context, ArrayList<String> imgPaths, boolean isScaleType) {
        this.mContext = context;
        this.mImgPaths = imgPaths;
        this.isScaleType = isScaleType;
    }

    @Override
    public int getCount() {
        return mImgPaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);

        if (isScaleType) {
            Log.e("GGG", "长 》》》》》 宽");
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }

//        Picasso.with(mContext)
//                .load(new File(mImgPaths.get(position)))
//                .into(imageView);
        Glide.with(mContext)
                .load(new File(mImgPaths.get(position)))
                .into(imageView);

        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeViewAt(position);
    }
}
