package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.module.my.controller.adapter.SelectVideoAdapter;
import com.module.my.model.bean.BitmapEntity;
import com.quicklyask.activity.R;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 视频选取
 *
 * @author 裴成浩
 */
public class SelectVideoActivity extends BaseActivity {

    private String TAG = "SelectVideoActivity";
    @BindView(id = R.id.recy_select_video)
    private RecyclerView mRecycler;

    @BindView(id = R.id.selector_button_back)
    private ImageView mBack;

    @BindView(id = R.id.selector_button_confirm)
    private Button mConfirm;

    @BindView(id = R.id.tv_video_yulan)
    private TextView videoYulan;
    @BindView(id = R.id.tv_video_quxiao)
    private TextView videoQuxiao;

    private List<BitmapEntity> bit = new ArrayList<>();

    private SelectVideoAdapter adapter;

    private String selectPath = "";          //选中视频的路径
    public int mPos = -1;                  //当前被选中的位置

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_select_video);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        selectPath = getIntent().getStringExtra("selectNum");
        if (selectPath == null) {
            selectPath = "";
        }

        new Search_photo().start();
        //返回
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("selectNum", "");
                setResult(76, intent);
                finish();
                overridePendingTransition(0, R.anim.activity_close);
            }
        });

        //确定
        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectPath != null && selectPath.length() > 0) {
                    long size = bit.get(mPos).getSize();
                    long duration = bit.get(mPos).getDuration();
                    Log.e("WriteNoteActivity", "duration333 == " + duration);
                    Log.e("ZZZZ", "size == " + size);
                    Log.e("ZZZZ", "duration == " + duration);
                    if (size > 0 && size < 500 * 1024 * 1024 && duration > 0 && duration < 5 * 60 * 1000) {
                        Intent intent = new Intent();
                        intent.putExtra("selectNum", selectPath);
                        intent.putExtra("duration", duration + "");
                        setResult(76, intent);
                        finish();
                        overridePendingTransition(0, R.anim.activity_close);
                    } else {
                        showDialogExitEdit3();
                    }

                } else {
                    Toast.makeText(SelectVideoActivity.this, "请选择您要上传的视频", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //预览
        videoYulan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectPath != null && selectPath.length() > 0) {
                    long duration = bit.get(mPos).getDuration();
                    Intent intent = new Intent(SelectVideoActivity.this, VideoPlayerActivity.class);
                    intent.putExtra("is_network_video", false);
                    intent.putExtra("selectNum", selectPath);
                    intent.putExtra("duration", duration + "");
                    startActivity(intent);
                } else {
                    Toast.makeText(SelectVideoActivity.this, "请选择要预览的视频", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //取消
        videoQuxiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogExitEdit2();
            }
        });

    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressLint("ShowToast")
        public void handleMessage(final Message msg) {
            if (msg.what == 1 && bit != null) {
                mRecycler.setLayoutManager(new GridLayoutManager(SelectVideoActivity.this, 3, GridLayoutManager.VERTICAL, false));
                ((DefaultItemAnimator) mRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
                adapter = new SelectVideoAdapter(SelectVideoActivity.this, bit, selectPath);
                mRecycler.setAdapter(adapter);

                adapter.setOnItemClickListener(new SelectVideoAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int pos) {
                        if (mPos == pos) {            //点击的是选中的
                            selectPath = "";
                            adapter.setmSelectPath(selectPath);
                            adapter.notifyItemChanged(mPos);
                            mPos = -1;

                        } else {                     //点击的是其他未选中的

                            if (mPos != -1) {                                                    //之前选中的改为未选中
                                selectPath = "";
                                adapter.setmSelectPath(selectPath);
                                adapter.notifyItemChanged(mPos);
                            }

                            selectPath = bit.get(pos).getPath();                                     //当前点击的改为选中
                            adapter.setmSelectPath(selectPath);
                            adapter.notifyItemChanged(pos);

//                            mPos = pos;
                        }
                    }
                });
            }
        }
    };

    /**
     * 遍历系统数据库找出相应的是视频的信息，每找出一条视频信息的同时利用与之关联的找出对应缩略图的uri
     * 再异步加载缩略图，
     * 由于查询速度非常快，全部查找完成在设置，等待时间不会太长
     *
     * @author Administrator
     */
    class Search_photo extends Thread {
        @Override
        public void run() {
            // 如果有sd卡（外部存储卡）
            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                Uri originalUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                ContentResolver cr = SelectVideoActivity.this.getApplicationContext().getContentResolver();
                Cursor cursor = cr.query(originalUri, null, null, null, null);
                if (cursor == null) {
                    return;
                }
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    String title = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));
                    String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
                    long size = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));
                    long duration = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
                    //获取当前Video对应的Id，然后根据该ID获取其缩略图的uri
                    int id = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID));
                    Log.e(TAG,"title =="+title);
                    Log.e(TAG,"path =="+path);
                    Log.e(TAG,"size =="+size);
                    Log.e(TAG,"duration =="+duration);
                    String[] selectionArgs = new String[]{id + ""};
                    String[] thumbColumns = new String[]{MediaStore.Video.Thumbnails.DATA, MediaStore.Video.Thumbnails.VIDEO_ID};
                    String selection = MediaStore.Video.Thumbnails.VIDEO_ID + "=?";

                    String uri_thumb = "";
                    Cursor thumbCursor = (SelectVideoActivity.this.getApplicationContext().getContentResolver()).query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, thumbColumns, selection, selectionArgs, null);

                    if (thumbCursor != null && thumbCursor.moveToFirst()) {
                        uri_thumb = thumbCursor.getString(thumbCursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA));

                    }
                    if (path.endsWith(".mp4")){
                        BitmapEntity bitmapEntity = new BitmapEntity(title, path, size, uri_thumb, duration);
                        bit.add(bitmapEntity);
                    }


                }
                if (cursor != null) {
                    cursor.close();
                    mHandler.sendEmptyMessage(1);
                }
            }
        }
    }

    /**
     * dialog提示（是否取消上传视频）
     */
    void showDialogExitEdit2() {
        final EditExitDialog editDialog = new EditExitDialog(SelectVideoActivity.this, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("确定取消上传视频？");

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });


        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setTextColor(Color.parseColor("#ffa5cc"));
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("selectNum", "");
                setResult(76, intent);
                finish();
                overridePendingTransition(0, R.anim.activity_close);
            }
        });
    }

    /**
     * dialog提示（视频过大或时间过长）
     */
    void showDialogExitEdit3() {
        final EditExitDialog editDialog = new EditExitDialog(SelectVideoActivity.this, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("您选择的视频超过5分钟，或视频过大，请尝试换一个视频");

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("我知道了");
        cancelBt88.setTextColor(Color.parseColor("#ffa5cc"));
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    /**
     * 监听Back键按下事件
     * 注意:
     * 返回值表示:是否能完全处理该事件
     * 在此处返回false,所以会继续传播该事件.
     * 在具体项目中此处的返回值视情况而定.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            showDialogExitEdit2();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }
}
