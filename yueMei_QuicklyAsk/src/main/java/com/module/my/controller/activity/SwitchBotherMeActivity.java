package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.my.model.api.DisturbApi;
import com.module.my.model.bean.BotherData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 夜间防打扰开关
 * 
 * @author Rubin
 * 
 */
public class SwitchBotherMeActivity extends BaseActivity {

	@BindView(id = R.id.bother_open, click = true)
	private RelativeLayout openRly;
	@BindView(id = R.id.bother_close, click = true)
	private RelativeLayout closeRly;

	@BindView(id = R.id.bother_open_iv, click = true)
	private ImageView opneIv;
	@BindView(id = R.id.bother_close_iv, click = true)
	private ImageView closeIv;

	@BindView(id = R.id.switch_bother_top)
	private CommonTopBar mTop;// 返回

	private Context mContext;
	private String uid = "";
	private String type = "";

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_switch_bother);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = SwitchBotherMeActivity.this;

		mTop.setCenterText("夜晚防打扰");

		uid = Utils.getUid();
		initOpen();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						SwitchBotherMeActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@Override
	protected void initWidget() {
		super.initWidget();
	}

	void initOpen() {
		Map<String, Object> keyValues = new HashMap<>();
		keyValues.put("uid",uid);
		keyValues.put("flag","1");

		new DisturbApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)) {

					BotherData data = JSONUtil.TransformBotherData(serverData.data);
					String initType = data.getType();

					if (initType.equals("2")) {
						opneIv.setVisibility(View.GONE);
						closeIv.setVisibility(View.VISIBLE);
					} else {
						opneIv.setVisibility(View.VISIBLE);
						closeIv.setVisibility(View.GONE);
					}
				}
			}

		});

	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.bother_open:
			opneIv.setVisibility(View.VISIBLE);
			closeIv.setVisibility(View.GONE);
			// open = "1";
			type = "1";
			initHttp();
			break;
		case R.id.bother_close:
			opneIv.setVisibility(View.GONE);
			closeIv.setVisibility(View.VISIBLE);
			// open = "0";
			type = "2";
			initHttp();
			break;
		}
	}

	void initHttp() {
		Map<String, Object> keyValues = new HashMap<>();
		keyValues.put("uid",uid);
		keyValues.put("type",type);
		keyValues.put("flag","2");

		new DisturbApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);
					onBackPressed();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
