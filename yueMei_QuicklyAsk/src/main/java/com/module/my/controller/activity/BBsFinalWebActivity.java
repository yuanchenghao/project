package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.my.controller.other.BBsFinalWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.emoji.Expressions;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.view.ElasticScrollView.OnRefreshListener;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 帖子最终页
 *
 * @author Rubin
 */
@SuppressLint("SetJavaScriptEnabled")
public class BBsFinalWebActivity extends BaseActivity {

    private final String TAG = "BBsFinalWebActivity";

    @BindView(id = R.id.bbs_web_det_final_scrollview1, click = true)
    private ElasticScrollView scollwebView;
    @BindView(id = R.id.bbs_web_det_final_linearlayout, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.bbs_det_final_web_back, click = true)
    private RelativeLayout back;// 返回
    @BindView(id = R.id.bbs_web_final_see, click = true)
    private RelativeLayout startCollectRLY;// 查看问题

    private WebView bbsDetWeb;

    @BindView(id = R.id.bbs_detail_final_input_rly)
    public RelativeLayout inputTxtRly;
    @BindView(id = R.id.bbs_web_input_final_content_et)
    public EditText inputContentEt;
    @BindView(id = R.id.bbs_web_final_cancel_rly)
    public RelativeLayout cancelRly;
    @BindView(id = R.id.bbs_web_final_sumbit_rly)
    private RelativeLayout sumbitRly;
    @BindView(id = R.id.bbs_web_final_cancel_iv)
    private ImageView cancelIv;
    @BindView(id = R.id.bbs_web_final_sumbit_bt)
    public Button sumbitBt;
    @BindView(id = R.id.bbs_details_final_name)
    private TextView loucTv;

    private BBsFinalWebActivity mContext;

    private String url;
    public JSONObject obj_http;

    private String qid;
    private String cid;

    private String louc;

    private String uid;
    private String userid;
    private String askorshare;


    private String type = "0";// 1为提问帖 2为体验贴
    private String typeroot = "0";

    private boolean isCanNext2 = false;

    public Pattern emoji = Pattern
            .compile(
                    "[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
                    Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

    // 输入表情前的光标位置
    private int cursorPos;
    // 输入表情前EditText中的文本
    private String tmp;
    // 是否重置了EditText的内容
    private boolean resetText;

    @BindView(id = R.id.suibian_biaoqing_input_rly)
    private RelativeLayout biaoqingBt1;
    @BindView(id = R.id.biaoqing_shuru_content_ly1)
    public LinearLayout biaoqingContentLy;
    @BindView(id = R.id.colse_biaoqingjian_bt)
    private ImageButton closeImBt;
    // 表情
    private ViewPager viewPager;
    private ArrayList<GridView> grids;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private GridView gView1;
    private GridView gView2;
    private ImageView page0;
    private ImageView page1;

    @BindView(id = R.id.iput_final_ly)
    private LinearLayout inputly1;
    public BaseWebViewClientMessage baseWebViewClientMessage;
    public HashMap<String, Object> urlMap1 = new HashMap<>();
    private HashMap<String, Object> urlMap2 = new HashMap<>();

    @Override
    public void setRootView() {
        setContentView(R.layout.web_acty_bbs_final);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = BBsFinalWebActivity.this;
        findView();
        Intent it0 = getIntent();
        url = it0.getStringExtra("url");
        typeroot = it0.getStringExtra("typeroot");

        qid = it0.getStringExtra("qid");
        louc = it0.getStringExtra("louc");
        userid = it0.getStringExtra("userid");
        askorshare = it0.getStringExtra("askorshare");

        if (louc.equals("0")) {
            loucTv.setText("回复详情");
        } else {
            loucTv.setText(louc + "楼");
        }
        scollwebView.GetLinearLayout(contentWeb);

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BBsFinalWebViewClient(mContext));
        initWebview();
        scollwebView.setonRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                baseWebViewClientMessage.startLoading();
                webReload();
            }
        });

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        BBsFinalWebActivity.this.finish();
                    }
                });

        sumbitBt.setPressed(true);
        sumbitBt.setClickable(false);
        inputContentEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String textStr = inputContentEt.getText().toString().trim();
                // Log.e(TAG, "textP===" + textStr);
                if (textStr.length() != 0 && !isCanNext2) {
                    isCanNext2 = true;
                    sumbitBt.setPressed(false);
                    sumbitBt.setClickable(true);
                }
                if (textStr.length() == 0) {
                    isCanNext2 = false;
                    sumbitBt.setPressed(true);
                    sumbitBt.setClickable(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2,
                                          int arg3) {
            }

        });
    }

    void findView() {
        // 回复的
        page0 = findViewById(R.id.page0_select);
        page1 = findViewById(R.id.page1_select);
        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;

        // 创建ViewPager
        viewPager = findViewById(R.id.viewpager);
        initViewPager1();

        biaoqingBt1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(),
                            0);
                }
                biaoqingContentLy.setVisibility(View.VISIBLE);
            }
        });

        closeImBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        inputContentEt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
            }
        });

        inputly1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }


    @Override
    protected void onStart() {
        super.onStart();

        LodUrl(url);
    }

    public void initWebview() {
        bbsDetWeb = new WebView(mContext);
        bbsDetWeb.getSettings().setJavaScriptEnabled(true);
        bbsDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        bbsDetWeb.getSettings().setUseWideViewPort(true);
        bbsDetWeb.getSettings().supportMultipleWindows();
        bbsDetWeb.getSettings().setNeedInitialFocus(true);
        bbsDetWeb.setWebViewClient(baseWebViewClientMessage);
        bbsDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(bbsDetWeb);
    }

    protected void OnReceiveData(String str) {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {

        LodUrl(url);
    }

    /**
     * 加载web
     */
    public void LodUrl(String url) {
        LodUrl(url,urlMap1,urlMap2);
    }
    public void LodUrl(String url,Map<String,Object> paramMap,Map<String, Object> headMap) {
        baseWebViewClientMessage.startLoading();
        String time = (System.currentTimeMillis() / 1000) + "";
        headMap.put("id", qid);
        if (!TextUtils.isEmpty(askorshare)){
            headMap.put("askorshare", askorshare);
        }

        headMap.put(FinalConstant1.TIME, time);
        paramMap.put(FinalConstant1.TIME, time);
        Log.e(TAG, "url === " + url);
        Log.e(TAG, "paramMap === " + paramMap.toString());
        Log.e(TAG, "headMap === " + headMap.toString());
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url,paramMap,headMap);
        if (null != bbsDetWeb) {
            bbsDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.bbs_det_final_web_back:
                // finish();
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                onBackPressed();
                break;
            case R.id.bbs_web_final_see:// 查看贴子详情页
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (type.equals("0")) {
                    onBackPressed();
                } else {
                    String baseurl = FinalConstant.baseUrl + FinalConstant.VER;
                    if (type.equals("1")) {
                        String url = baseurl + "/forum/askinfo/id/" + qid + "/";
                        Intent it2 = new Intent();
                        it2.putExtra("url", url);
                        it2.putExtra("qid", qid);
                        it2.setClass(mContext, DiariesAndPostsActivity.class);
                        startActivity(it2);
                        finish();
                    } else {
                        String url = baseurl + "/forum/shareinfo/id/" + qid + "/";
                        Intent it2 = new Intent();
                        it2.putExtra("url", url);
                        it2.putExtra("qid", qid);
                        it2.setClass(mContext, DiariesAndPostsActivity.class);
                        startActivity(it2);
                        finish();
                    }
                }
                break;
        }
    }


    private void initViewPager1() {
        LayoutInflater inflater = LayoutInflater.from(this);
        grids = new ArrayList<GridView>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(
                BBsFinalWebActivity.this, listItems, R.layout.singleexpression,
                new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    inputContentEt.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(getResources(),
                            expressionImages[arg2 % expressionImages.length]);

                    bitmap = zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(
                            expressionImageNames[arg2].substring(1,
                                    expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0,
                            expressionImageNames[arg2].length() - 2,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    inputContentEt.append(spannableString);

                    // System.out.println("edit的内容 = " + spannableString);
                }
            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);

        // grids.add(gView3);
        // System.out.println("GridView的长度 = " + grids.size());

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);
        // viewPager.setAdapter();

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(getResources().getDrawable(
                            R.drawable.page_focused));
                    page1.setImageDrawable(getResources().getDrawable(
                            R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(getResources().getDrawable(
                            R.drawable.page_focused));
                    page0.setImageDrawable(getResources().getDrawable(
                            R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext,
                            listItems, R.layout.singleexpression,
                            new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                inputContentEt.onKeyDown(KeyEvent.KEYCODE_DEL,
                                        event); // 抛给系统处理了
                            } else {

                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(
                                        getResources(), expressionImages1[pos
                                                % expressionImages1.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext,
                                        bitmap);
                                SpannableString spannableString = new SpannableString(
                                        expressionImageNames1[pos]
                                                .substring(1,
                                                        expressionImageNames1[pos]
                                                                .length() - 1));

                                spannableString.setSpan(imageSpan, 0,
                                        expressionImageNames1[pos].length() - 2,
                                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                inputContentEt.append(spannableString);

                                System.out.println("edit的内容 = " + spannableString);
                            }
                        }
                    });
                    break;
            }
        }
    }

    public static Bitmap zoomImage(Bitmap bgimage, double newWidth,
                                   double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
