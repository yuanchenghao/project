package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.view.CommonTopBar;
import com.module.doctor.model.bean.GroupRongIMData;
import com.quicklyask.activity.R;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 在线客服
 *
 * Created by dwb on 16/7/21.
 */
public class OnlineKefuActivity extends BaseActivity {

    private final String TAG = "OnlineKefuActivity";

    private Activity mContext;

    @BindView(id = R.id.onlinekefu_top)
    private CommonTopBar mTop;// 返回

    @BindView(id = R.id.zaixiankefu_rly1, click = true)
    private RelativeLayout kefu1;
    @BindView(id = R.id.zaixiankefu_rly2, click = true)
    private RelativeLayout kefu2;
    @BindView(id = R.id.zaixiankefu_rly3, click = true)
    private RelativeLayout kefu3;
    @BindView(id = R.id.zaixiankefu_rly4, click = true)
    private RelativeLayout kefu4;
    @BindView(id = R.id.zaixiankefu_rly5, click = true)
    private RelativeLayout kefu5;
    @BindView(id = R.id.zaixiankefu_rly6, click = true)
    private RelativeLayout kefu6;
    @BindView(id = R.id.zaixiankefu_rly7, click = true)
    private RelativeLayout kefu7;
    @BindView(id = R.id.zaixiankefu_rly8, click = true)
    private RelativeLayout kefu8;

    SildingFinishLayout mSildingFinishLayout;

    private String type="1";

    private GroupRongIMData groupRIMdata;
    private PageJumpManager pageJumpManager;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_onlinekefu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = OnlineKefuActivity.this;
        Intent it=getIntent();
        type=it.getStringExtra("type");

        pageJumpManager = new PageJumpManager(mContext);

        if(type.equals("1")) {
            kefu4.setVisibility(View.VISIBLE);
            kefu5.setVisibility(View.VISIBLE);
            kefu6.setVisibility(View.VISIBLE);
        }else {
            kefu4.setVisibility(View.GONE);
            kefu5.setVisibility(View.GONE);
            kefu6.setVisibility(View.GONE);
        }

        mTop.setCenterText("在线客服");

        mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        OnlineKefuActivity.this.finish();
                    }
                });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }


    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.zaixiankefu_rly1://购买整形服务及订单问题

                break;
            case R.id.zaixiankefu_rly2://术后效果意见反馈

                break;
            case R.id.zaixiankefu_rly3://退款问题

                break;
            case R.id.zaixiankefu_rly4://社区问题及日记返现

                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId("70278")
                        .setObjId("0")
                        .setObjType("0")
                        .setYmClass("0")
                        .setYmId("0")
                        .build();
                pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                break;
            case R.id.zaixiankefu_rly5://APP功能建议
                ChatParmarsData chatParmarsData1 = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId("81964")
                        .setObjId("0")
                        .setObjType("0")
                        .setYmClass("0")
                        .setYmId("0")
                        .build();
                pageJumpManager.jumpToChatBaseActivity(chatParmarsData1);

                break;
            case R.id.zaixiankefu_rly6://其他

                break;
            case R.id.zaixiankefu_rly7:

                break;
            case R.id.zaixiankefu_rly8://颜值币商城相关问题
                ChatParmarsData chatParmarsData2 = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId("81964")
                        .setObjId("0")
                        .setObjType("0")
                        .setYmClass("0")
                        .setYmId("0")
                        .build();
                pageJumpManager.jumpToChatBaseActivity(chatParmarsData2);

                break;
        }
    }



//    private void initGroupRongIM() {
//        String uid = Cfg.loadStr(mContext, FinalConstant.UID, "");
//        KJHttp kjh = new KJHttp();
//        KJStringParams params = new KJStringParams();
//        params.put("uid", uid);
//        params.put("duid", "0");
//        params.put("flag", "1");
//        kjh.post(FinalConstant.RONGYUN_GROUP, params, new StringCallBack() {
//
//            @Override
//            public void onSuccess(String json) {
//                if (null != json) {
//                    String code = JSONUtil
//                            .resolveJson(json, FinalConstant.CODE);
//                    if (code.equals("1")) {
//                        GroupRongIM rim = new GroupRongIM();
//                        try {
//                            rim = JSONUtil.TransformSingleBean(json,
//                                    GroupRongIM.class);
//                            groupRIMdata = rim.getData();
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        });
//
//    }


    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}