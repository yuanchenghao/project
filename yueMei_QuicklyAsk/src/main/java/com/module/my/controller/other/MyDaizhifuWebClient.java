package com.module.my.controller.other;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.format.Time;
import android.util.Log;
import android.view.View;

import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.my.controller.activity.MyDaizhifuActivity;
import com.module.my.model.api.CancelOrderApi;
import com.module.my.model.api.ChangeTimeApi;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mirko.android.datetimepicker.date.DatePickerDialog;



/**
 * Created by Administrator on 2018/4/20.
 */

public class MyDaizhifuWebClient implements BaseWebViewClientCallback {
    public static final String TAG="MyDaizhifuWebClient";
    private final MyDaizhifuActivity mActivity;
    private final Intent intent;

    private final Calendar mCalendar = Calendar.getInstance();
    private final MyDaizhifuWebClient myOrdersWebViewClient;

    private int day = mCalendar.get(Calendar.DAY_OF_MONTH);

    private int month = mCalendar.get(Calendar.MONTH);

    private int year = mCalendar.get(Calendar.YEAR);

    private String dateStr = "";
    private String time_order_id = "";

    private String hosName;
    private String titleContetn;
    private String hosPrice;
    private String netPrice;
    private String uid;

    public MyDaizhifuWebClient(Activity activity) {
        this.mActivity = (MyDaizhifuActivity) activity;
        myOrdersWebViewClient = MyDaizhifuWebClient.this;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 返现跳日记

                String link = obj.getString("link");
                String qid = obj.getString("id");

                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                intent.putExtra("url", link);
                intent.putExtra("qid", qid);
                mActivity.startActivity(intent);

                break;

            case "6":// 问答详情

                String link6 = obj.getString("link");
                String qid6 = obj.getString("id");

                intent.putExtra("url", link6);
                intent.putExtra("qid", qid6);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);

                break;
            case "442"://取消订单
                String order_id = obj.getString("order_id");
                showDialog(order_id);

                break;

            case "443"://更改预约时间

                time_order_id = obj.getString("order_id");
                // CHANGE_TIME 更改预约时间
                String tag = "";
                datePickerDialog.show(mActivity.getFragmentManager(), tag);

                break;

            case "447": //跳转到淘整形首页
//                MainTableActivity.tabHost.setCurrentTab(0);
//                MainTableActivity.bnBottom[0].setChecked(true);
//                MainTableActivity.bnBottom[1].setChecked(false);
//                MainTableActivity.bnBottom[2].setChecked(false);
//                MainTableActivity.bnBottom[3].setChecked(false);
//                MainTableActivity.bnBottom[4].setChecked(false);
                MainTableActivity.mainBottomBar.setCheckedPos(0);
                mActivity.finish();

                break;
            case "535":
                ViewInject.toast("正在拨打中·····");
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(mActivity).request(new AcpOptions.Builder()
                                    .setPermissions(Manifest.permission.CALL_PHONE)
                                    .build(),
                            new AcpListener() {
                                @Override
                                public void onGranted() {        //判断权限是否开启
                                    phoneCall();
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                    ViewInject.toast("没有电话权限");
                                }
                            });
                } else {
                    phoneCall();
                }
                break;
        }
    }

    /**
     * 打电话
     */
    private void phoneCall() {

        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        int minute = t.minute;
        int second = t.second;
        Log.e(TAG, "hour === " + hour);
        if (hour > 9 && hour < 22) {
            ViewInject.toast("正在拨打中·····");
            Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "4000567118"));
            try {
                mActivity.startActivity(it);
            } catch (Exception e) {
                Log.e(TAG, "e === " + e.toString());
                e.printStackTrace();
            }
        } else if (hour == 9 && minute >= 30) {
            ViewInject.toast("正在拨打中·····");
            Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "4000567118"));
            try {
                mActivity.startActivity(it);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (hour == 21 && minute <= 30) {
            ViewInject.toast("正在拨打中·····");
            Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "4000567118"));
            try {
                mActivity.startActivity(it);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * 获取订单信息
     */
    void showDialog(final String order_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        // builder.setMessage("确定退出登录？");
        View view = View.inflate(mActivity, R.layout.dialog_cancel_order, null);
        builder.setView(view);

        // builder.setTitle("退出");
        builder.setPositiveButton("否", new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        builder.setNegativeButton("是", new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                cancelOrder(order_id);
            }
        });
        builder.create().show();
    }

    final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePickerDialog datePickerDialog,
                                      int year, int month, int day) {
                    if (year >= myOrdersWebViewClient.year) {
                        if (year == myOrdersWebViewClient.year) {
                            if (month >= myOrdersWebViewClient.month) {
                                if (month == myOrdersWebViewClient.month) {
                                    if (day >= myOrdersWebViewClient.day) {
                                        dateStr = year + "-" + (month + 1)
                                                + "-" + day;
                                        changeTime(time_order_id, dateStr);
                                    } else {
                                        dateStr = "";
                                        ViewInject.toast("预约时间无效");
                                    }
                                } else {
                                    dateStr = year + "-" + (month + 1) + "-"
                                            + day;
                                    changeTime(time_order_id, dateStr);
                                }
                            } else {
                                dateStr = "";
                                ViewInject.toast("预约时间无效");
                            }
                        } else {
                            dateStr = year + "-" + (month + 1) + "-" + day;
                            changeTime(time_order_id, dateStr);
                        }
                    } else {
                        dateStr = "";
                        ViewInject.toast("预约时间无效");
                    }
                }
            }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
            mCalendar.get(Calendar.DAY_OF_MONTH));

    void cancelOrder(String order_id) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("order_id", order_id);
        maps.put("uid", uid);
        new CancelOrderApi().getCallBack(mActivity, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    ViewInject.toast("取消成功");
//                    mActivity.webReload();
                }
            }

        });
    }

    void changeTime(String order_id, String data) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("arrive_time", data);
        maps.put("order_id", order_id);
        maps.put("uid", uid);
        new ChangeTimeApi().getCallBack(mActivity, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    ViewInject.toast("修改成功");
//                    mActivity.webReload();
                }

            }

        });
    }
}
