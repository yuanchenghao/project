package com.module.my.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.home.controller.activity.WebUrlTitleActivity;
import com.module.my.controller.other.MyPointsWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 我的颜值币
 * 
 * @author Rubin
 * 
 */

public class MyPointsActivity extends BaseActivity {

	private final String TAG = "MyPointsActivity";

	@BindView(id = R.id.jifen_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.my_posts_top)
	private CommonTopBar mTop;// 返回

	public WebView docDetWeb;

	private MyPointsActivity mContex;

	public JSONObject obj_http;

	@BindView(id = R.id.all_content)
	private LinearLayout contentLy;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_my_points);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = MyPointsActivity.this;

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setView(contentLy);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new MyPointsWebViewClient(mContex));

		initWebview();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						MyPointsActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				Intent it = new Intent();
				it.setClass(mContex, WebUrlTitleActivity.class);
				it.putExtra("link", FinalConstant.JIFEN_GUIZE_);
				it.putExtra("title", "颜值币规则");
				startActivity(it);
			}
		});
	}

	public void onResume() {
		super.onResume();
		LodUrl1(FinalConstant.MY_POINTS_URL);
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}


	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		if (null != docDetWeb) {
			docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
		}

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
			case 888:
				if (data != null) {
					Log.e("AAAAAAAAA","AAAAAAAAAAA");

					webReload();
				}
				break;
		}

	}

	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}


	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}