package com.module.my.controller.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.quicklyask.activity.R;
import com.module.my.controller.activity.SetCoverActivity;


import java.io.File;
import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2017/7/4.
 */

public class SetCoverRecAdapter extends RecyclerView.Adapter<SetCoverRecAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private final String TAG = "SetCoverRecAdapter";
    private Context mContext;
    private ArrayList<String> mImgPaths = new ArrayList<>();

    public int typePos = 0;

    public SetCoverRecAdapter(Context context, ArrayList<String> imgPaths, int typePos) {
        this.mContext = context;
        this.mImgPaths = imgPaths;
        Log.e(TAG, "imgPaths == " + imgPaths.toString());
        this.typePos = typePos;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public SetCoverRecAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.set_cover_item, parent, false);
        return new ViewHolder(itemView);        //把这个布局传到ViewHolder中
    }

    @Override
    public void onBindViewHolder(SetCoverRecAdapter.ViewHolder holder, int position) {

        if (position == typePos) {
            ((SetCoverActivity) mContext).typePos = position;
            holder.vIsSelect.setVisibility(View.VISIBLE);

            if (position == 0) {
                holder.vIsSelect.setImageResource(R.drawable.shape_left_hollow_ff5c77);
            } else if (position == mImgPaths.size() - 1) {
                holder.vIsSelect.setImageResource(R.drawable.shape_right_hollow_ff5c77);
            } else {
                holder.vIsSelect.setImageResource(R.drawable.shape_hollow_ff5c77);
            }

        } else {
            holder.vIsSelect.setVisibility(View.GONE);
        }
//        Picasso.with(mContext)
//                .load(new File(mImgPaths.get(position)))
//                .transform(new myTransformation(position))
//                .into(holder.ivFenMian);
        Glide.with(mContext)
                .load(new File(mImgPaths.get(position)))
                .transform(new myTransformation(mContext))
                .into(holder.ivFenMian);
    }

    @Override
    public int getItemCount() {
        return mImgPaths.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivFenMian;
        public ImageView vIsSelect;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFenMian = itemView.findViewById(R.id.iv_fen_mian);
            vIsSelect = itemView.findViewById(R.id.v_is_select);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(view, getLayoutPosition());
                }
            });
        }
    }

    //设置被选中的
    public void setTypePos(int typePos) {
        this.typePos = typePos;
    }

    /**
     * item的监听器
     */
    public interface OnItemClickListener {
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    class myTransformation extends BitmapTransformation {


        public myTransformation(Context context) {
            super(context);
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            int width = toTransform.getWidth();
            int height = toTransform.getHeight();
            if (height > width) {
                int mHeight = (45 * width) / 65;
                int y = (height - mHeight) / 2;
                Bitmap bitmap = Bitmap.createBitmap(toTransform, 0, y, width, mHeight);
                toTransform.recycle();
                return bitmap;
            } else {
                return toTransform;
            }
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }
//    class myTransformation implements Transformation {
//
//        private final int mPos;
//
//        public myTransformation(int pos) {
//            this.mPos = pos;
//        }
//
//        @Override
//        public Bitmap transform(Bitmap source) {
//            int width = source.getWidth();
//            int height = source.getHeight();
//            if (height > width) {
//                int mHeight = (45 * width) / 65;
//                int y = (height - mHeight) / 2;
//                Bitmap bitmap = Bitmap.createBitmap(source, 0, y, width, mHeight);
//                source.recycle();
//                return bitmap;
//            } else {
//                return source;
//            }
//        }
//
//        @Override
//        public String key() {
//            return "video" + mPos;
//        }
//    }
}
