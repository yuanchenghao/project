package com.module.my.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.my.controller.adapter.MyFocusAdapter;
import com.module.my.model.api.MyFansApi;
import com.module.my.model.bean.MyFansData;
import com.quicklyask.activity.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 我的粉丝
 */
public class MyFansActivity extends YMBaseActivity {

    @BindView(R.id.my_fans_not)
    LinearLayout mFansNot;
    @BindView(R.id.fans_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.my_fans_list)
    ListView mListView;
    private MyFansApi myFansApi;

    private String TAG = "MyFansActivity";
    private int mPage = 1;
    private MyFocusAdapter myFocusAdapter;
    private int mTempPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_fans;
    }

    @Override
    protected void initView() {

        //加载更多和刷新回调
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                downloadData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                myFocusAdapter = null;
                downloadData();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<MyFansData> fansDatas = myFocusAdapter.getmFansDatas();
                if (position != fansDatas.size()) {
                    mTempPos = position;
                    MyFansData myFansData = fansDatas.get(position);
                    Intent mIntent = new Intent();
                    switch (myFansData.getObj_type()) {
                        case "6":
                            mIntent.setClass(mContext, PersonCenterActivity641.class);
                            mIntent.putExtra("id", myFansData.getObj_id());
                            break;
                        case "1":
                            mIntent.setClass(mContext, DoctorDetailsActivity592.class);
                            mIntent.putExtra("docId", myFansData.getObj_id());
                            mIntent.putExtra("docName", myFansData.getName());
                            mIntent.putExtra("partId", "");
                            break;
                        case "3":
                            mIntent.setClass(mContext, HosDetailActivity.class);
                            mIntent.putExtra("hosid", myFansData.getObj_id());
                            break;
                        case "0":
                            mIntent.setClass(mContext, QuanziDetailActivity.class);
                            mIntent.putExtra("url_name", myFansData.getUrl());
                            break;
                    }
                    startActivityForResult(mIntent, 10);
                }
            }
        });
    }

    @Override
    protected void initData() {
        myFansApi = new MyFansApi();

        initView();
        downloadData();
    }

    /**
     * 网络请求数据
     */
    private void downloadData() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("page", mPage + "");

        myFansApi.getCallBack(mContext, hashMap, new BaseCallBackListener<List<MyFansData>>() {
            @Override
            public void onSuccess(List<MyFansData> myFansDatas) {
                if (mRefresh != null) {
                    //刷新加载样式结束
                    mRefresh.finishRefresh();
                    Log.e(TAG, "myFansDatas == " + myFansDatas.size());
                    if (myFansDatas.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }

                    if (myFansDatas.size() == 0 && mPage == 1) {
                        mFansNot.setVisibility(View.VISIBLE);
                        mRefresh.setVisibility(View.GONE);
                    }else{
                        mFansNot.setVisibility(View.GONE);
                        mRefresh.setVisibility(View.VISIBLE);
                        if (myFocusAdapter == null) {
                            myFocusAdapter = new MyFocusAdapter(mContext, myFansDatas, "2");
                            mListView.setAdapter(myFocusAdapter);
                        } else {
                            myFocusAdapter.addData(myFansDatas);
                        }
                    }

                }
                mPage++;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!"3".equals(myFocusAdapter.getmFansDatas().get(mTempPos).getObj_type())) {
            if (requestCode == 10 && resultCode == 100) {
                String focus = data.getStringExtra("focus");
                if (!TextUtils.isEmpty(focus) && mTempPos >= 0) {
                    myFocusAdapter.setEachFollowing(mTempPos, focus);
                    mTempPos = -1;
                }
            }
        } else {
            isFocu("3");
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String type) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", myFocusAdapter.getmFansDatas().get(mTempPos).getObj_id());
        hashMap.put("type", type);
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                myFocusAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }
}
