package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.module.api.SecurityCodeApi;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.my.model.api.InitCode1Api;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.api.YuyueSumbitApi;
import com.module.my.model.bean.LoginData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.HashMap;
import java.util.Map;

public class YuYueDocActivity extends BaseActivity {

	private final String TAG = "YuYueDocActivity";

	private Context mContex;

	@BindView(id = R.id.order_time_back)
	private RelativeLayout back;

	@BindView(id = R.id.order_phone_number_et)
	private EditText phoneNumberEt;
	@BindView(id = R.id.order_phone_code_et)
	private EditText phoneCode;
	@BindView(id = R.id.input_order_other_et)
	private EditText otherEt;

	@BindView(id = R.id.yanzheng_code_rly)
	private RelativeLayout sendEMSRly;
	@BindView(id = R.id.yanzheng_code_tv)
	private TextView emsTv;

	@BindView(id = R.id.reg_bt)
	private Button sumbitBt;

	@BindView(id = R.id.want_to_mei_title)
	private TextView titleTv;

	private String docid;
	private String uid = "";
	private String bdPhone = "";// 绑定的手机号
	private String status;// 用户的三种状态
	private String partId;
	private String docname = "";
	private String upPhone = "";

	@BindView(id = R.id.write_phone_bangding_ly)
	private RelativeLayout bangdingLy;// 绑定手机框
	@BindView(id = R.id.write_bangding_phone_tv)
	private TextView phoneTv;// 绑定的手机号

	@BindView(id = R.id.phone_code_weibangding_ly)
	private LinearLayout nobangdingLy;// 未绑定手机框

	private UserData userData;
	private LoginData loginData;

	private static final int BACK2 = 3;

	@BindView(id = R.id.nocode_message_rly)
	private RelativeLayout nocodeRly;
	@BindView(id = R.id.nocode_message_tv)
	private TextView nocodeTv;

	private PopupWindows yuyinCodePop;
	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout allcontent;


	private GetPhoneCodePopWindow phoneCodePop;


	ImageOptions imageOptions;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.kymjs.aframe.ui.activity.I_KJActivity#setRootView()
	 */
	@Override
	public void setRootView() {
		setContentView(R.layout.acty_want_beautiful_595);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.kymjs.aframe.ui.activity.BaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = YuYueDocActivity.this;
		uid = Utils.getUid();
		bdPhone = Cfg.loadStr(mContex, FinalConstant.UPHONE, "");

		imageOptions = new ImageOptions.Builder()
				.setUseMemCache(false)
				.setImageScaleType(ImageView.ScaleType.FIT_XY)
				.build();

		Intent it0 = getIntent();
		docid = it0.getStringExtra("docid");
		partId = it0.getStringExtra("partId");
		docname = it0.getStringExtra("docname");
		titleTv.setText("预约" + docname + "面诊");

		if (Utils.isLogin()) {// 登录情况下
			if (bdPhone.length() > 0) {// 用户是否绑定手机
				status = "1";
				bangdingLy.setVisibility(View.VISIBLE);
				nobangdingLy.setVisibility(View.GONE);

				upPhone = bdPhone;

				String spj = bdPhone;
				String a = spj.substring(0, 3);
				String b = spj.substring(bdPhone.length() - 4, spj.length());
				String ss = a + "****" + b;
				phoneTv.setText(ss);

			} else {// 用户没有绑定手机
				status = "2";
				bangdingLy.setVisibility(View.GONE);
				nobangdingLy.setVisibility(View.VISIBLE);

			}
		} else {// 没有登录

			status = "3";
			bangdingLy.setVisibility(View.GONE);
			nobangdingLy.setVisibility(View.VISIBLE);

		}

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						YuYueDocActivity.this.finish();
					}
				});

		sendEMSRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String phones = phoneNumberEt.getText().toString().trim();

				if (null != phones && phones.length() > 0) {
					if (ifPhoneNumber()) {
						if (status.equals("3")) {// 没有登录的情况
							sendEMS();

							nocodeRly.setVisibility(View.VISIBLE);
							nocodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？"
									+ "</u>"));
						} else {
							sendEMS1();

							nocodeRly.setVisibility(View.VISIBLE);
							nocodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？"
									+ "</u>"));
						}
					} else {
						ViewInject.toast("请输入正确的手机号");
					}
				} else {
					ViewInject.toast("请输入手机号");
				}
			}
		});

		sumbitBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (Utils.isFastDoubleClick()) {
					return;
				}

				String exstr = otherEt.getText().toString().trim();

				if (status.equals("1")) {
					if (exstr.length() > 0) {
						yuyueSumbit();
					} else {
						ViewInject.toast("请提出您的需求");
					}
				} else {
					String code = phoneCode.getText().toString();
					String phones = phoneNumberEt.getText().toString().trim();
					if (null != phones && phones.length() > 0) {
						if (ifPhoneNumber()) {
							if (null != code && code.length() > 0) {
								if (status.equals("2")) {// 没绑定手机
									if (exstr.length() > 0) {

										initCode11();
									} else {
										ViewInject.toast("请提出您的需求");
									}
								} else if (status.equals("3")) {// 没登录
									if (exstr.length() > 0) {
										// Log.e("ssss", "status===" + status);
										initCode1();
									} else {
										ViewInject.toast("请提出您的需求");
									}
								}
							} else {
								ViewInject.toast("请输入验证码");
							}
						} else {
							ViewInject.toast("请输人正确的手机号");
						}
					} else {
						ViewInject.toast("请输人手机号");
					}
				}
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		// 更改手机号
		bangdingLy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent it3 = new Intent();
				it3.setClass(mContex, AddPhoneActivity.class);
				startActivityForResult(it3, BACK2);
			}
		});

		nocodeRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				yuyinCodePop = new PopupWindows(mContex, allcontent);
				yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

				Glide.with(mContex).load(FinalConstant.TUXINGCODE).into(codeIv);
			}
		});
	}

	EditText codeEt1;
	ImageView codeIv;

	/**
	 * 获取手机号 并验证
	 * 
	 * @author dwb
	 * 
	 */
	public class PopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PopupWindows(Context mContext, View parent) {

			final View view = View.inflate(mContext, R.layout.pop_yuyincode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt);
			Button tureBt = view.findViewById(R.id.zixun_bt);
			codeEt1 = view.findViewById(R.id.no_pass_login_code_et);
			codeIv = view.findViewById(R.id.yuyin_code_iv);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly);

			rshCodeRly.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					x.image().bind(codeIv,FinalConstant.TUXINGCODE,imageOptions);
				}
			});

			tureBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String codes = codeEt1.getText().toString();
					if (codes.length() > 1) {
						yanzhengCode(codes);
					} else {
						ViewInject.toast("请输入图中数字");
					}
				}
			});

			cancelBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	void yanzhengCode(String codes) {
		String phones = phoneNumberEt.getText().toString().trim();
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phones);
		maps.put("code",codes);
		if (status.equals("3")) {
			maps.put("flag", "codelogin");
		} else {
			maps.put("flag", "beautify");
		}
		new CallAndSecurityCodeApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData s) {
				if ("1".equals(s.code)){
					yuyinCodePop.dismiss();
					ViewInject.toast("正在拨打您的电话，请注意接听");
				}else{
					ViewInject.toast("数字错误，请重新输入");
				}
			}
		});
	}

	private boolean ifPhoneNumber() {
		String textPhn = phoneNumberEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	/**
	 * 绑定手机验证验证码
	 */
	void initCode11() {
		String codeStr = phoneCode.getText().toString();
		String phoness = phoneNumberEt.getText().toString();
		upPhone = phoness;
		Map<String,Object> maps=new HashMap<>();
		maps.put("uid", uid);
		maps.put("phone", phoness);
		maps.put("code", codeStr);
		maps.put("flag", "1");
		new SecurityCodeApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {

			@Override
			public void onSuccess(ServerData s) {
				if ("1".equals(s.code)){
					yuyueSumbit();
				}else{
					ViewInject.toast(s.message);
				}
			}
		});

	}

	/**
	 * 无密码登录提交验证码
	 * 
	 */
	void initCode1() {
		String codeStr = phoneCode.getText().toString();
		String phoness = phoneNumberEt.getText().toString();
		upPhone = phoness;
		Map<String,Object>params=new HashMap<>();
		params.put("phone", phoness);
		params.put("code", codeStr);
		new InitCode1Api().getCallBack(mContex, params, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)){
					userData = JSONUtil.TransformLogin(serverData.data);
					String id = userData.get_id();

					getUserInfoLogin(id);

					String img = userData.getImg();
					String nickName = userData.getNickname();
					String province = userData.getProvince();
					String city = userData.getCity();
					String sex = userData.getSex();
					String birthday = userData.getBirthday();

					Utils.setUid(id);
					Cfg.saveStr(mContex, FinalConstant.UHEADIMG, img);
					Cfg.saveStr(mContex, FinalConstant.UNAME, nickName);
					Cfg.saveStr(mContex, FinalConstant.UPROVINCE,
							province);
					Cfg.saveStr(mContex, FinalConstant.UCITY, city);
					Cfg.saveStr(mContex, FinalConstant.USEX, sex);
					Cfg.saveStr(mContex, FinalConstant.UBIRTHDAY, birthday);

//							Utils.bindBaiduHttp(mContex,"1");

					PushManager.startWork(mContex, PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);
				}else {
					ViewInject.toast(serverData.message);
				}
			}
		});
	}

	private void getUserInfoLogin(String uid) {
		BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
		Map<String,Object> params=new HashMap<>();
		params.put("id",uid);
		userInfoApi.getCallBack(mContex, params, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)){
					UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
					String id = userData.get_id();
					String img = userData.getImg();
					String nickName = userData.getNickname();
					String province = userData.getProvince();
					String city = userData.getCity();
					String sex = userData.getSex();
					String birthday = userData.getBirthday();

					// String daiComment = userData.getEvaluate();
					// String lvStr = userData.getLevel();
					//
					// String yqma = userData.getYqma();
					// String isphone = userData.getIsphone();
					String phoneStr = userData.getPhone();

					Utils.setUid(id);
					Cfg.saveStr(mContex, FinalConstant.UHEADIMG,
							img);
					Cfg.saveStr(mContex, FinalConstant.UNAME,
							nickName);
					Cfg.saveStr(mContex, FinalConstant.UPROVINCE,
							province);
					Cfg.saveStr(mContex, FinalConstant.UCITY, city);
					Cfg.saveStr(mContex, FinalConstant.USEX, sex);
					Cfg.saveStr(mContex, FinalConstant.UBIRTHDAY, birthday);

					if (phoneStr.equals("0")) {
						Cfg.saveStr(mContex, FinalConstant.UPHONE,
								"");
					} else {
						Cfg.saveStr(mContex, FinalConstant.UPHONE,
								phoneStr);
					}

					yuyueSumbit();
				}else {
					Toast.makeText(mContex,serverData.message,Toast.LENGTH_SHORT).show();
				}

			}
		});
	}


	void yuyueSumbit() {
		uid = Utils.getUid();
		String exStr = otherEt.getText().toString();
		Map<String,Object>maps=new HashMap<>();
		maps.put("partid", partId);
		maps.put("phone", upPhone);
		maps.put("docid", docid);
		maps.put("uid", uid);
		maps.put("ex", exStr);
		new YuyueSumbitApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					// ViewInject.toast(message);
					Intent it = new Intent();
					it.setClass(mContex,
							DoctorDetailsActivity592.class);
					it.putExtra("code", "1");
					setResult(88, it);
					finish();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	/**
	 * 绑定手机号发送验证码
	 */
	void sendEMS1() {
		String phone = phoneNumberEt.getText().toString();
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid",uid);
		maps.put("phone",phone);
		maps.put("flag","1");
		new SendEMSApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)) {
					ViewInject.toast("请求成功");

					sendEMSRly.setClickable(false);
					phoneCode.requestFocus();

					new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

						@Override
						public void onTick(long millisUntilFinished) {
							sendEMSRly
									.setBackgroundResource(R.drawable.biankuang_hui);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_zi));
							emsTv.setText("(" + millisUntilFinished
									/ 1000 + ")重新获取");
						}

						@Override
						public void onFinish() {
							sendEMSRly
									.setBackgroundResource(R.drawable.shape_bian_ff5c77);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_bian_hong1));
							sendEMSRly.setClickable(true);
							emsTv.setText("重发验证码");
						}
					}.start();
				}else{
					ViewInject.toast("请求错误");
				}
			}
		});
	}

	/**
	 * 无密码登录发送验证码
	 */
	void sendEMS() {
		String phone = phoneNumberEt.getText().toString();
		Map<String,Object> maps=new HashMap<>();
		maps.put("phone",phone);
		maps.put("flag","1");
		new SendEMSApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);

					sendEMSRly.setClickable(false);
					phoneCode.requestFocus();

					new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

						@Override
						public void onTick(long millisUntilFinished) {
							sendEMSRly
									.setBackgroundResource(R.drawable.biankuang_hui);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_zi));
							emsTv.setText("(" + millisUntilFinished / 1000
									+ ")重新获取");
						}

						@Override
						public void onFinish() {
							sendEMSRly
									.setBackgroundResource(R.drawable.shape_bian_ff5c77);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_bian_hong1));
							sendEMSRly.setClickable(true);
							emsTv.setText("重发验证码");
						}
					}.start();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {

		case BACK2:
			if (null != data) {
				String phone = data.getStringExtra("phone");

				upPhone = phone;

				String spj = phone;
				String a = spj.substring(0, 3);
				String b = spj.substring(bdPhone.length() - 4, spj.length());
				String ss = a + "****" + b;
				phoneTv.setText(ss);
			}
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
