package com.module.my.controller.other;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.module.MyApplication;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.service.NotificationService;
import com.quicklyask.util.Cfg;
import com.quicklyask.view.YueMeiDialog;

public class SignCallBack {
    private String TAG = "SignCallBack";
    private Activity mContext;
    private WebView mWebView;


    public SignCallBack(Activity mContext, WebView webView) {
        Log.e(TAG,"SignCallBack ====>");
        this.mContext = mContext;
        this.mWebView = webView;
    }

    /**
     * 获取APP通知开启状态
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @JavascriptInterface
    public void getAppNoticeStatus(){
        Log.e(TAG,"getAppNoticeStatus ====>");
        String isOpen="";
        //判断通知权限是否开启 true开启 false关闭
        boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
        if (b){
            isOpen="1";
        }else {
            isOpen="0";
        }
        //判断当前签到状态
        SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
        String isSign = sign.getString(FinalConstant.SIGN_FLAG, "0");
        final String str = isOpen+"|"+isSign;
        Log.e(TAG,"isopen=="+isOpen+"  issign=="+isSign);
        //APP通知开启状态回调
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("javascript:appNoticeStatusCallBack(\""+str+"\")");
            }
        });

    }


    /**
     *  开启或关闭签到通知
     * @param openOrClose 签到开关 0是关  |  是否已经签到 0未签到
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @JavascriptInterface
    public void appOpenOrCloseCheckInNotice(String openOrClose){
        String[] signArr = openOrClose.split("\\|");
        String signNotifi=signArr[0];
        String isSign=signArr[1];
        Log.e(TAG,"appOpenOrCloseCheckInNotice ==>signNotifi->"+signNotifi+"isSign->"+isSign);
        boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
        if (!b) {
            final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "检测到您没有打开通知权限，是否去打开?", "取消", "确定");
            yueMeiDialog.setCanceledOnTouchOutside(false);
            yueMeiDialog.show();
            yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                @Override
                public void leftBtnClick() {
                    yueMeiDialog.dismiss();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT, "no", "0", "1"));
                }

                @Override
                public void rightBtnClick() {
                    yueMeiDialog.dismiss();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT, "yes", "0", "1"));
                    Intent localIntent = new Intent();
                    localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (Build.VERSION.SDK_INT >= 9) {
                        localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                        localIntent.setData(Uri.fromParts("package", mContext.getPackageName(), null));
                    } else if (Build.VERSION.SDK_INT <= 8) {
                        localIntent.setAction(Intent.ACTION_VIEW);

                        localIntent.setClassName("com.android.settings",
                                "com.android.settings.InstalledAppDetails");

                        localIntent.putExtra("com.android.settings.ApplicationPkgName",
                                mContext.getPackageName());
                    }
                        mContext.startActivity(localIntent);
//                    mContext.startActivityForResult(localIntent, 100);
                }
            });
            return;
        }

        if ("1".equals(signNotifi)){
            if (!NotificationService.isRunning(mContext)) {   //如果这个服务不存在
                SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
                sign.edit().putString(FinalConstant.SIGN_FLAG,"1").apply();
                Intent startIntent = new Intent(mContext, NotificationService.class);
                mContext.startService(startIntent);             //开启本地推送服务
                Log.e(TAG,"startService");

            }
            if ("0".equals(isSign)){
                //开启签到通知回调
                Log.e(TAG,"openOrCloseCallBack==>1");
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl("javascript:openOrCloseCallBack(\"1\")");
                    }
                });
            }else {
                //关闭签到通知回调
                Log.e(TAG,"openOrCloseCallBack==>1");
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl("javascript:openOrCloseCallBack(\"1\")");
                    }
                });
            }

        }else {

            if (NotificationService.isRunning(mContext)) {
                SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
                sign.edit().putString(FinalConstant.SIGN_FLAG,"0").apply();
                Intent startIntent = new Intent(mContext, NotificationService.class);
                mContext.stopService(startIntent);               //关闭本地推送服务
                Log.e(TAG, "stopService");
                Log.e(TAG,Cfg.loadStr(MyApplication.getContext(),FinalConstant.SIGN_FLAG, "0"));
            }
            if ("0".equals(isSign)) {
                Log.e(TAG,"openOrCloseCallBack==>0");
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl("javascript:openOrCloseCallBack(\"0\")");
                    }
                });
            }else {

                //关闭签到通知回调
                Log.e(TAG,"openOrCloseCallBack==>0");
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl("javascript:openOrCloseCallBack(\"0\")");
                    }
                });
            }
        }



    }

    //这个用户已经签到了
    @JavascriptInterface
    public void userCheckedIn(){
        Log.e(TAG,"userCheckedIn ====>");
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.IS_SIGN, "1");


    }

    /**
     * 调用开通APP通知
     */
    @JavascriptInterface
    public void openAppNoticeAuth(){
        Log.e(TAG,"openAppNoticeAuth ====>");
        String isOpen="";
        //判断通知权限是否开启 true开启 false关闭
        boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
        if (b){
            isOpen="1";
        }else {
            isOpen="0";
            final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "检测到您没有打开通知权限，是否去打开?", "取消", "确定");
            yueMeiDialog.setCanceledOnTouchOutside(false);
            yueMeiDialog.show();
            yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                @Override
                public void leftBtnClick() {
                    yueMeiDialog.dismiss();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT,"no","0","1"));
                }

                @Override
                public void rightBtnClick() {
                    yueMeiDialog.dismiss();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT,"yes","0","1"));
                    Intent localIntent = new Intent();
                    localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (Build.VERSION.SDK_INT >= 9) {
                        localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                        localIntent.setData(Uri.fromParts("package", mContext.getPackageName(), null));
                    } else if (Build.VERSION.SDK_INT <= 8) {
                        localIntent.setAction(Intent.ACTION_VIEW);

                        localIntent.setClassName( "com.android.settings",
                                "com.android.settings.InstalledAppDetails");

                        localIntent.putExtra("com.android.settings.ApplicationPkgName",
                                mContext.getPackageName());
                    }
                    mContext.startActivity(localIntent);

                }
            });
            return;
        }
        //判断当前签到状态
        SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
        String isSign = sign.getString(FinalConstant.SIGN_FLAG, "0");
        final String str = isOpen+"|"+isSign;
        Log.e(TAG,"isopen=="+isOpen+"  issign=="+isSign);
        //APP通知开启状态回调
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("javascript:appNoticeStatusCallBack(\""+str+"\")");
            }
        });
    }

    /**
     * 刷新当前页面
     */
    @JavascriptInterface
    public void refreshWebView(){
        Log.e(TAG,"refreshWebView==>");
//        mWebView.reload();
//        loadUrl(mUrl);
    }

    protected void loadUrl(String url) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

    }



}
