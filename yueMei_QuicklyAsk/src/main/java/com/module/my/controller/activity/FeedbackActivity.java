package com.module.my.controller.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.TuCaoApi;
import com.quicklyask.activity.R;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 意见与反馈
 * 
 * @author Rubin
 * 
 */
public class FeedbackActivity extends BaseActivity {

	private final String TAG = "FeedbackActivity";

	private FeedbackActivity mContext;

	@BindView(id = R.id.feedback_back, click = true)
	private RelativeLayout feedback;
	@BindView(id = R.id.feedback_submit, click = true)
	private TextView submit;
	@BindView(id = R.id.feedback_content)
	private EditText content;

	Pattern emoji = Pattern
			.compile(
					"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
					Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

	// 输入表情前的光标位置
	private int cursorPos;
	// 输入表情前EditText中的文本
	private String tmp;
	// 是否重置了EditText的内容
	private boolean resetText;
	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_feedback);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = FeedbackActivity.this;

		mDialog = new LoadingProgress(mContext);

		content.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {

			}

			@Override
			public void onTextChanged(CharSequence s, int st, int be, int count) {
				// Log.e(TAG, "onTextChanged==" + "(s=" + s + ")/(start=" + st
				// + ")/(before=" + be + ")/(count=" + count + ")");

			}

			@Override
			public void beforeTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
				// Log.e(TAG, "beforeTextChanged==" + "(s=" + s + ")/(start="
				// + arg1 + ")/(before=" + arg2 + ")/(count=" + arg3 + ")");
			}

		});
	}

	@SuppressWarnings("deprecation")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.feedback_back:
			finish();
			break;
		case R.id.feedback_submit:
			String feedContent = content.getText().toString();

			Matcher matcher = emoji.matcher(feedContent);

			if (matcher.find()) {
				Toast.makeText(FeedbackActivity.this, "暂不支持表情输入",
						Toast.LENGTH_SHORT).show();
			} else {
				mDialog.startLoading();
				if (feedContent.length() > 0) {
					String contentStr = feedContent;
					Map<String,Object> maps=new HashMap<>();
					maps.put("content",contentStr);
					new TuCaoApi().getCallBack(mContext, maps, new BaseCallBackListener<String>() {
						@Override
						public void onSuccess(String s) {
							mDialog.stopLoading();
							// Log.d(TAG, "json=" + t);
							ViewInject.toast("提交成功！");
							finish();
						}
					});

				} else {
					mDialog.stopLoading();
					ViewInject.toast(mContext, "输入的内容不能为空");
				}

			}
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
