/**
 * 
 */
package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.utils.StatisticalManage;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.RegisterApi;
import com.module.my.model.api.RegisterDataApi;
import com.module.my.model.bean.RegisterData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.tendcloud.appcpa.TalkingDataAppCpa;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.HashMap;
import java.util.Map;

/**
 * 注册用户
 * 
 * @author Robin
 * 
 */
public class RegisterActivity548 extends BaseActivity {

	private final String TAG = "RegisterActivity548";

	private RegisterActivity548 mContext;

	@BindView(id = R.id.reg_username)
	private EditText regUerEt;// 用户手机号
	@BindView(id = R.id.reg_password)
	private EditText regPasswordEt;// 密码
	@BindView(id = R.id.reg_yanzheng_code)
	private EditText regCodeEt;// 输入验证码

	@BindView(id = R.id.reg_bt, click = true)
	private Button regBt;// 注册

	@BindView(id = R.id.reg_send_code_rly, click = true)
	private RelativeLayout sendCodeRly;// 发送验证码按钮
	@BindView(id = R.id.reg_send_code_tv)
	private TextView sendCodeTv;// 发送验证码文字
	@BindView(id = R.id.yueme_agremment)
	private TextView yuemeiAgremment;//

	private final int BACK3 = 3;

	@BindView(id = R.id.password_if_ming_iv, click = true)
	private ImageView passIfShowIv;
	private boolean passIsShow = false;

	@BindView(id = R.id.nocde_message_tv, click = true)
	private TextView noCodeTv;// 没收到验证码

	private PopupWindows yuyinCodePop;
	private TuPopupWindows tupianCodePop;
	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout allcontent;

	private GetPhoneCodePopWindow phoneCodePop;

	@BindView(id = R.id.yuemei_yinsi_ly, click = true)
	private LinearLayout yuemeiYinsiLy;

	ImageOptions imageOptions;
	private LoadingProgress mDialog;


	@Override
	public void setRootView() {
		setContentView(R.layout.acty_reg_548);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = RegisterActivity548.this;

		imageOptions = new ImageOptions.Builder()
				.setUseMemCache(false)
				.setImageScaleType(ImageView.ScaleType.FIT_XY)
				.build();
		mDialog = new LoadingProgress(mContext);
		findViewById(R.id.order_time_all_ly);
		initAgremment();

	}



	private void initAgremment() {
		SpannableString spannableString = new SpannableString("注册即表示您同意并愿意遵守《悦美用户使用协议》《隐私政策》");
		spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#666666")), 0, 12, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
		spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#4d7bbc")), 13,spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
		spannableString.setSpan(new Clickable(clickListener), 13, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		spannableString.setSpan(new Clickable(clickListener2), 23, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		yuemeiAgremment.setText(spannableString);
		yuemeiAgremment.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.reg_send_code_rly:// 发送验证码
			if (Utils.isFastDoubleClick()) {
				return;
			}
			String phoneNumber = regUerEt.getText().toString();
			if (null != phoneNumber && phoneNumber.length() > 0) {
				if (judgeEmailAndPhone(phoneNumber)) {

					tupianCodePop = new TuPopupWindows(mContext, allcontent);
					tupianCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

					x.image().bind(codeIv2,FinalConstant.TUXINGCODE,imageOptions);

				} else {
					ViewInject.toast("请输入正确的手机号");
				}
			} else {
				ViewInject.toast("请输入手机号");
			}
			break;
		case R.id.reg_bt:// 注册
			if (Utils.isFastDoubleClick()) {
				return;
			}
			String codeStr = regCodeEt.getText().toString().trim();
			String password = regPasswordEt.getText().toString().trim();
			String phone = regUerEt.getText().toString().trim();
			if (null != phone && phone.length() > 0) {
				if (judgeEmailAndPhone(phone)) {
					if (null != codeStr && codeStr.length() > 0) {
						if (null != password && password.length() > 0) {
							if (password.length() > 5 && password.length() < 26) {
								registerData(codeStr, password, phone);
							} else {
								ViewInject.toast("请输入6到25个字符密码");
							}
						} else {
							ViewInject.toast("请输入密码");
						}
					} else {
						ViewInject.toast("请输入验证码");
					}
				} else {
					ViewInject.toast("请输入正确的手机号");
				}
			} else {
				ViewInject.toast("请输入手机号");
			}
			break;
		case R.id.password_if_ming_iv:// 密码是否铭文
			if (Utils.isFastDoubleClick()) {
				return;
			}
			if (passIsShow) {
				passIfShowIv.setBackgroundResource(R.drawable.miwen_);
				regPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_PASSWORD);
				Editable etext = regPasswordEt.getText();
				Selection.setSelection(etext, etext.length());
				passIsShow = false;
			} else {
				passIfShowIv.setBackgroundResource(R.drawable.mingwen_);
				regPasswordEt
						.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
				Editable etext = regPasswordEt.getText();
				Selection.setSelection(etext, etext.length());
				passIsShow = true;
			}
			break;
		case R.id.nocde_message_tv:// 没收到验证码
			if (Utils.isFastDoubleClick()) {
				return;
			}
			yuyinCodePop = new PopupWindows(mContext, allcontent);
			yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

			Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);

			break;
		case R.id.yuemei_yinsi_ly://悦美隐私政策
			Intent it=new Intent();
			it.setClass(mContext,UserAgreementWebActivity.class);
			startActivity(it);
			break;

		}
	}


	EditText codeEt2;
	ImageView codeIv2;

	/**
	 * 获取手机号 并验证
	 *
	 * @author dwb
	 *
	 */
	public class TuPopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public TuPopupWindows(final Context mContext, View parent) {

			final View view = View.inflate(mContext, R.layout.pop_tupiancode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt2);
			Button tureBt = view.findViewById(R.id.zixun_bt2);
			codeEt2 = view.findViewById(R.id.no_pass_login_code_et2);
			codeIv2 = view.findViewById(R.id.yuyin_code_iv2);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly2);

			rshCodeRly.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					x.image().bind(codeIv2,FinalConstant.TUXINGCODE,imageOptions);
				}
			});

			tureBt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (Utils.isFastDoubleClick()){
						return;
					}
						String codes = codeEt2.getText().toString();
						if (codes.length() > 1) {
							String phoneNumber = regUerEt.getText().toString();
							noCodeTv.setVisibility(View.VISIBLE);
							noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？" + "</u>"));
							sendCode(phoneNumber,codes);
							dismiss();
						} else {
							ViewInject.toast("请输入图中数字");
						}



				}
			});

			cancelBt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}





	EditText codeEt1;
	ImageView codeIv;

	/**
	 * 获取手机号 并验证
	 * 
	 * @author dwb
	 * 
	 */
	public class PopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PopupWindows(Context mContext, View parent) {

			final View view = View.inflate(mContext, R.layout.pop_yuyincode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt);
			Button tureBt = view.findViewById(R.id.zixun_bt);
			codeEt1 = view.findViewById(R.id.no_pass_login_code_et);
			codeIv = view.findViewById(R.id.yuyin_code_iv);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly);

			rshCodeRly.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					x.image().bind(codeIv,FinalConstant.TUXINGCODE,imageOptions);
				}
			});

			tureBt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String codes = codeEt1.getText().toString();
					if (codes.length() > 1) {
						yanzhengCode(codes);
					} else {
						ViewInject.toast("请输入图中数字");
					}
				}
			});

			cancelBt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	void yanzhengCode(String codes) {
		String phones = regUerEt.getText().toString().trim();
		Map<String,Object> maps=new HashMap<>();
		maps.put("phone",phones);
		maps.put("code",codes);
		maps.put("flag","register");
		new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData s) {
				Log.e(TAG, "1111");
				if ("1".equals(s.code)){
					yuyinCodePop.dismiss();
					ViewInject.toast("正在拨打您的电话，请注意接听");
				}else{
					ViewInject.toast("数字错误，请重新输入");
				}
			}

		});
	}

	private boolean judgeEmailAndPhone(String nameStr) {
		if (nameStr.contains("@")) {
            return Utils.emailFormat(nameStr);
		} else {
            return Utils.isMobile(nameStr);
		}
    }

	void sendCode(String phoneNumber,String code) {
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phoneNumber);
		maps.put("code",code);
		new RegisterApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				Log.e(TAG, "2222");
				if ("1".equals(serverData.code)){
					//ViewInject.toast(serverData.message);
					regCodeEt.requestFocus();
					sendCodeRly.setClickable(false);
					new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

						@Override
						public void onTick(long millisUntilFinished) {
							sendCodeRly.setBackgroundResource(R.drawable.biankuang_hui);
							sendCodeTv.setTextColor(getResources().getColor(R.color.button_zi));
							sendCodeTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
						}

						@Override
						public void onFinish() {
							sendCodeRly.setBackgroundResource(R.drawable.shape_bian_ff5c77);
							sendCodeTv.setTextColor(getResources()
									.getColor(
											R.color.button_bian_hong1));
							sendCodeRly.setClickable(true);
							sendCodeTv.setText("重发验证码");
						}
					}.start();
				}else{
					ViewInject.toast(serverData.message);
				}

			}
		});
	}

	void registerData(String code, String pass, String phone) {
		Map<String,Object>maps=new HashMap<>();
		maps.put("flag", "2");
		maps.put("code", code);
		maps.put("phone", phone);
		maps.put("password", pass);
		new RegisterDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);
					RegisterData codeReg = JSONUtil
							.TransformRegister(serverData.data);
					String uid = codeReg.getUid();
					Utils.setUid(uid);
					Cfg.saveStr(mContext, FinalConstant.HOME_PERSON_UID, uid);
					binBaidu();

					TalkingDataAppCpa.onRegister(uid);

					StatisticalManage.getInstance().growingIO("logon");

					Intent it = new Intent();
					it.setClass(mContext, LoginActivity605.class);
					setResult(BACK3, it);
					finish();
				} else {
					ViewInject.toast(serverData.message);
				}

			}

		});

	}

	void binBaidu() {
		// *****************消息推送*********************//
		Utils.logStringCache = Utils.getLogText(getApplicationContext());
		PushManager.startWork(getApplicationContext(),
				PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);
		// ********************************************//
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent it = new Intent();
			it.putExtra(UserAgreementWebActivity.mIsYueMeiAgremment,1);
			it.setClass(mContext, UserAgreementWebActivity.class);
			startActivity(it);
		}
	};

	private View.OnClickListener clickListener2 = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent it = new Intent();
			it.putExtra(UserAgreementWebActivity.mIsYueMeiAgremment,0);
			it.setClass(mContext, UserAgreementWebActivity.class);
			startActivity(it);
		}
	};

	private class Clickable extends ClickableSpan {
		private final View.OnClickListener mListener;

		public Clickable(View.OnClickListener l) {
			mListener = l;
		}
		/**
		 * 重写父类点击事件
		 */
		@Override
		public void onClick(View v) {
			mListener.onClick(v);
		}

		/**
		 * 重写父类updateDrawState方法  我们可以给TextView设置字体颜色,背景颜色等等...
		 */
		@Override
		public void updateDrawState(TextPaint ds) {

		}
	}
}
