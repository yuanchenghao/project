package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.IsFocuData;
import com.module.commonview.view.FocusButton2;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyShareBoardlistener;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.community.web.WebUtil;
import com.module.my.model.api.InitTopTitleApi;
import com.module.my.model.api.QuanziDetailApi;
import com.module.my.model.bean.QuanziTitleData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 圈子详情页
 */
public class QuanziDetailActivity extends YMBaseActivity {

    private final String TAG = "QuanziDetailActivity";


    private String partId;
    private String partName;
    private String typeBbs = "1";

    private String url_name;
    private QuanziTitleData qzTitleData;

    @BindView(R.id.quanzhi_container)
    FrameLayout contentContainer;
    @BindView(R.id.quanzhi_nodata)
    FrameLayout contentNoData;
    @BindView(R.id.all_content_s)
    LinearLayout allcontent;

    @BindView(R.id.wan_beautiful_web_back)
    RelativeLayout back;
    @BindView(R.id.wan_beautifu_docname)
    TextView titleTv;
    @BindView(R.id.tao_web_share_rly111)
    RelativeLayout shareLy;
    @BindView(R.id.bbs_detail_bottom_content_rly)
    LinearLayout bottomWrite;//写日记

    // List
    @BindView(R.id.homepage_bbs_list_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.homepage_bbs_list_view)
    ListView mListView;
    private int mCurPage = 1;

    private BBsListAdapter bbsListAdapter;

    private View headView;
    // Top
    private ImageView topIv;
    private TextView topDesc;
    private LinearLayout topMoreLy;
    private TextView topTitleTv;
    private TextView sxTopTv;
    private LinearLayout topNodataLy;
    private FocusButton2 mQuanFans;

    // 分享
    private String shareUrl = "";
    private String shareContent = "";
    private String shareWXImgUrl = "";
    private String shareTitle;
    private String sharepic = "";

    private String top_id = "";


    public Dialog dialog = null;
    private String full_name;
    private String mobile;
    private QuanziDetailApi quanziDetailApi;
    private IsFocuData mFsFocuData;
    private FocusAndCancelData mFocusAndCancelData;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_quanzi_detail;
    }

    @Override
    protected void initView() {
        Intent it1 = getIntent();
        url_name = it1.getStringExtra("url_name");

    }

    @Override
    protected void initData() {
        quanziDetailApi = new QuanziDetailApi();
        mDialog.startLoading();
        initTopTitle();
        initList();


        Log.e(TAG, "url_name === " + url_name);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        /**
         * 分享
         */
        shareLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != shareContent && null != shareUrl && null != sharepic) {
                    setShare();
                }
            }
        });
    }


    /**
     * 设置分享
     */
    private void setShare() {
        BaseShareView baseShareView = new BaseShareView(mContext);
        baseShareView.setShareContent(shareTitle).ShareAction();

        Log.e(TAG, "shareWXImgUrl === " + shareWXImgUrl);
        Log.e(TAG, "sharepic === " + sharepic);

        MyShareBoardlistener shareBoardlistener = baseShareView.getShareBoardlistener();

        shareBoardlistener.setSinaText(shareContent + " " + shareUrl).setSinaThumb(new UMImage(mContext, sharepic)).setSmsText(shareTitle + "，" + shareUrl).setTencentUrl(shareUrl).setTencentTitle(shareTitle);

        if (TextUtils.isEmpty(shareWXImgUrl)) {
            shareBoardlistener.setTencentThumb(new UMImage(mContext, R.drawable.ic_launcher));
        } else {
            shareBoardlistener.setTencentThumb(new UMImage(mContext, shareWXImgUrl));
        }

        shareBoardlistener.setTencentDescription(shareContent).setTencentText(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {

                if (!platform.equals(SHARE_MEDIA.SMS)) {
                    Toast.makeText(mContext, " 分享成功啦", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });
    }

    void initList() {

        LayoutInflater mInflater = getLayoutInflater();
        // 表头
        headView = mInflater.inflate(R.layout.head_quanzi_detail, null);
        topIv = headView.findViewById(R.id.quanzi_top_iv);
        topDesc = headView.findViewById(R.id.quanzi_top_title);
        topMoreLy = headView.findViewById(R.id.quanzi_top_a_ly);
        topTitleTv = headView.findViewById(R.id.top_yiqiliaoliao_tv);
        sxTopTv = headView.findViewById(R.id.quanzi_huati_title_tv);
        topNodataLy = headView.findViewById(R.id.nodata_ly);
        mQuanFans = headView.findViewById(R.id.quanzi_fans);

        ViewGroup.LayoutParams params = topIv.getLayoutParams();
        params.height = (windowsWight * 280 / 750);
        topIv.setLayoutParams(params);

        mListView.addHeaderView(headView);

        //下拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                lodHotBBsListData1("1".equals(typeBbs) ? "0" : "1");
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {

                if (bbsListAdapter != null) {
                    List<BBsListData550> bBsListData550s = bbsListAdapter.getmHotIssues();
                    if (pos >= 1 && bBsListData550s != null && bBsListData550s.size() > 0) {
                        String url = bBsListData550s.get(pos - 1).getUrl();
                        String qid = bBsListData550s.get(pos - 1).getQ_id();
                        Intent it2 = new Intent(mContext, DiariesAndPostsActivity.class);
                        it2.putExtra("url", url);
                        it2.putExtra("qid", qid);
                        startActivity(it2);
                    }
                }

            }
        });

        topMoreLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopupWindows(allcontent);
            }
        });


        bottomWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    getUserData();
                }

            }
        });

    }


    /**
     * 判断是否关注
     */
    private void isFocu() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", partId);
        hashMap.put("type", "0");
        Log.e(TAG, "partId == " + partId);
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                mFsFocuData = isFocuData;

                setFocusButton();

                //关注按钮点击
                mQuanFans.setFocusClickListener(mFsFocuData.getFolowing(), new FocusButton2.ClickCallBack() {
                    @Override
                    public void onClick(View v) {
                        FocusAndCancel();
                    }
                });
            }
        });
    }

    /**
     * 设置关注按钮
     */
    private void setFocusButton() {

        switch (mFsFocuData.getFolowing()) {
            case "0":               //未关注
                mQuanFans.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                break;
            case "1":               //已关注
                mQuanFans.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                break;
            case "2":               //互相关注
                mQuanFans.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                break;
        }
    }

    /**
     * 关注和取消关注
     */
    private void FocusAndCancel() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", partId);
        hashMap.put("type", "0");
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

                if ("1".equals(serverData.code)) {
                    try {
                        mFocusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (mFocusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mFsFocuData.setFolowing("0");
                                mQuanFans.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                                break;
                            case "1":          //已关注
                                mFsFocuData.setFolowing("1");
                                mQuanFans.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                                break;
                            case "2":          //互相关注
                                mFsFocuData.setFolowing("2");
                                mQuanFans.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                                break;
                        }

                        //关注按钮点击
                        mQuanFans.setFocusClickListener(mFsFocuData.getFolowing(), new FocusButton2.ClickCallBack() {
                            @Override
                            public void onClick(View v) {
                                FocusAndCancel();
                            }
                        });

                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 获取发帖人信息页
     */
    private void getUserData() {
        Map<String, Object> keyValues = new HashMap<>();

        keyValues.put("id", Utils.getUid());
        new BBsDetailUserInfoApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    full_name = userData.getFull_name();
                    mobile = userData.getMobile();
                    if (full_name.length() > 0 && mobile.length() > 0) {

                        if ("1090".equals(top_id)) {
                            Intent it2 = new Intent();
                            it2.putExtra("cateid", partId);
                            it2.setClass(mContext, SelectSendPostsActivity.class);
                            startActivity(it2);
                        } else {
                            Intent it3 = new Intent();
                            it3.putExtra("cateid", partId);
                            it3.setClass(mContext, WriteSuibianLiaoActivity647.class);
                            startActivity(it3);
                        }
                    } else {
                        if ("1090".equals(top_id)) {
                            Intent intent = new Intent(QuanziDetailActivity.this, PostingMessageActivity.class);
                            intent.putExtra("cateid", partId);
                            intent.putExtra("tiaozhuan", "4");
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(QuanziDetailActivity.this, PostingMessageActivity.class);
                            intent.putExtra("userid", "0");
                            intent.putExtra("cateid", partId);
                            intent.putExtra("tiaozhuan", "5");
                            startActivity(intent);
                        }
                    }
                }else {
                    Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                }

            }

        });
    }


    /**
     * 圈子详情头部信息
     */
    private void initTopTitle() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("url", url_name);
        Log.e(TAG, "url_name == " + url_name);
        new InitTopTitleApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    contentContainer.setVisibility(View.VISIBLE);
                    contentNoData.setVisibility(View.GONE);
                    try {
                        Log.e(TAG, "获取数据成功");
                        qzTitleData = JSONUtil.TransformSingleBean(serverData.data, QuanziTitleData.class);
                        partId = qzTitleData.getId();
                        partName = qzTitleData.getTitle();
                        sharepic = qzTitleData.getShare_wbimg();
                        shareWXImgUrl = qzTitleData.getShare_wximg();
                        shareUrl = qzTitleData.getShare_url();
                        shareTitle = qzTitleData.getShare_title();
                        shareContent = qzTitleData.getShare_content();

                        Log.e(TAG, "sharepic == " + sharepic);
                        Log.e(TAG, "shareWXImgUrl == " + shareWXImgUrl);
                        Log.e(TAG, "shareUrl == " + shareUrl);
                        Log.e(TAG, "shareTitle == " + shareTitle);
                        Log.e(TAG, "shareContent == " + shareContent);

                        top_id = qzTitleData.getTop_id();

                        titleTv.setText(partName);
                        topDesc.setText(qzTitleData.getDesc());
                        topTitleTv.setText("#" + partName + "#");

                        if (null != qzTitleData.getImg() && qzTitleData.getImg().length() > 0) {
                            Glide.with(mContext).load(qzTitleData.getImg()).into(topIv);

                            topNodataLy.setVisibility(View.VISIBLE);
                        } else {
                            topNodataLy.setVisibility(View.GONE);
                        }


                        lodHotBBsListData1("1".equals(typeBbs) ? "0" : "1");

                        if (Utils.isLogin()) {
                            isFocu();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    contentContainer.setVisibility(View.GONE);
                    contentNoData.setVisibility(View.VISIBLE);
                    MyToast.makeTextToast2(mContext,"无法获取数据",Toast.LENGTH_SHORT).show();
                    mDialog.dismiss();
                }
            }

        });

    }

    private void lodHotBBsListData1(String type) {
        Log.e(TAG, "partId === " + partId);
        Log.e(TAG, "type === " + type);
        HashMap<String, Object> quanziDetailMap = new HashMap<>();
        quanziDetailMap.put("type", type);
        quanziDetailMap.put("id", partId);
        quanziDetailMap.put("page", mCurPage + "");
        quanziDetailApi.getCallBack(mContext, quanziDetailMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if("1".equals(serverData.code)){
                    List<BBsListData550> docData = JSONUtil.jsonToArrayList(serverData.data,BBsListData550.class);
                    mCurPage++;
                    mDialog.stopLoading();
                    mRefresh.finishRefresh();
                    if (docData.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }
                    if (bbsListAdapter == null) {
                        bbsListAdapter = new BBsListAdapter(mContext, docData);
                        mListView.setAdapter(bbsListAdapter);
                    } else {
                        bbsListAdapter.add(docData);
                        bbsListAdapter.notifyDataSetChanged();
                    }
                }else {
                    Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }


    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(View parent) {

            final View view = View.inflate(mContext, R.layout.pop_home_quanzi_sort, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button bt1 = view.findViewById(R.id.item_popupwindows_camera);
            Button bt2 = view.findViewById(R.id.item_popupwindows_Photo);
            Button bt4 = view.findViewById(R.id.item_popupwindows_Photo1);


            Button bt3 = view.findViewById(R.id.item_popupwindows_cancel);

            if (typeBbs.equals("1")) {
                bt1.setTextColor(Color.parseColor("#ff5c77"));
                bt2.setTextColor(Color.parseColor("#333333"));
                bt4.setTextColor(Color.parseColor("#333333"));
            } else if (typeBbs.equals("2")) {
                bt1.setTextColor(Color.parseColor("#333333"));
                bt2.setTextColor(Color.parseColor("#ff5c77"));
                bt4.setTextColor(Color.parseColor("#333333"));
            }

            bt1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    typeBbs = "1";
                    refresh();
                    sxTopTv.setText("话题");

                    dismiss();

                }
            });

            bt2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    typeBbs = "2";
                    refresh();
                    sxTopTv.setText("整形日记");

                    dismiss();
                }
            });

            bt4.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    WebUtil.getInstance().startWebActivity(mContext, FinalConstant.BBS_TITLE_BAIKE + partId + "/" + Utils.getTokenStr());
                    dismiss();
                }
            });


            bt3.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            // 点击 之外 消失
            view.setOnTouchListener(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });

        }
    }

    /**
     * 刷新
     */
    private void refresh() {
        mDialog.startLoading();
        mCurPage = 1;
        bbsListAdapter = null;
        initTopTitle();
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(100, intent);
        if (mFocusAndCancelData != null) {
            intent.putExtra("focus", mFocusAndCancelData.getIs_following());
        }
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }
}
