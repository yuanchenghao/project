/**
 * 
 */
package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.SecurityCodeApi;
import com.module.commonview.module.api.SendEMSApi;
import com.module.commonview.view.CommonTopBar;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 添加手机号
 * 
 * @author Robin
 * 
 */
public class AddPhoneActivity extends BaseActivity {

	private final String TAG = "AddPhoneActivity";
	private AddPhoneActivity mContext;

	@BindView(id = R.id.addphone_top)
	private CommonTopBar mTop;// 返回

	@BindView(id = R.id.no_pass_login_phone_et)
	private EditText phoneNumberEt;
	@BindView(id = R.id.no_pass_login_code_et)
	private EditText codeEt;
	@BindView(id = R.id.no_pass_login_bt, click = true)
	private Button loginBt;

	@BindView(id = R.id.no_pass_yanzheng_code_rly)
	private RelativeLayout sendEMSRly;
	@BindView(id = R.id.yanzheng_code_tv)
	private TextView emsTv;

	private String phone;
	private String codeStr;

	private static final int BACK2 = 3;

	private String uid;

	@BindView(id = R.id.nocde_message_tv, click = true)
	private TextView noCodeTv;// 没收到验证码

	private PopupWindows yuyinCodePop;
	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout allcontent;

	private GetPhoneCodePopWindow phoneCodePop;

	private LoadingProgress mDialog;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_addphone);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = AddPhoneActivity.this;

		mDialog = new LoadingProgress(mContext);

		mTop.setCenterText("手机号");
		uid = Utils.getUid();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						AddPhoneActivity.this.finish();
					}
				});

		sendEMSRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String phone = phoneNumberEt.getText().toString();
				if (phone.length() > 0) {
					if (ifPhoneNumber()) {
						sendEMS();
						noCodeTv.setVisibility(View.VISIBLE);
						noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？"
								+ "</u>"));
					} else {
						ViewInject.toast("请输入正确的手机号");
					}
				} else {
					ViewInject.toast("请输入手机号");
				}
			}
		});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	private boolean ifPhoneNumber() {
		String textPhn = phoneNumberEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

	void sendEMS() {
		phone = phoneNumberEt.getText().toString();
		Map<String,Object> maps=new HashMap<>();
		maps.put("uid",uid);
		maps.put("phone",phone);
		maps.put("flag","2");
		new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData s) {
				if ("1".equals(s.code)){
					ViewInject.toast("请求成功");

					sendEMSRly.setClickable(false);
					codeEt.requestFocus();

					new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

						@Override
						public void onTick(long millisUntilFinished) {
							sendEMSRly
									.setBackgroundResource(R.drawable.biankuang_hui);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_zi));
							emsTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
						}

						@Override
						public void onFinish() {
							sendEMSRly
									.setBackgroundResource(R.drawable.shape_bian_ff5c77);
							emsTv.setTextColor(getResources().getColor(
									R.color.button_bian_hong1));
							sendEMSRly.setClickable(true);
							emsTv.setText("重发验证码");
						}
					}.start();
				}else{
					ViewInject.toast(s.message);
				}
			}
		});
	}

	void initCode1() {
		codeStr = codeEt.getText().toString();
		Map<String,Object> maps =new HashMap<>();
		maps.put("uid", uid);
		maps.put("phone", phone);
		maps.put("code", codeStr);
		maps.put("flag", "2");
		new SecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {

			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)){
					mDialog.stopLoading();
					Intent it = new Intent();
					it.setClass(mContext, ModifyMyDataActivity.class);
					it.putExtra("phone", phone);
					setResult(BACK2, it);
					finish();
				}else{
					mDialog.stopLoading();
					ViewInject.toast(serverData.message);
				}
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.no_pass_login_bt:// 登录
			codeStr = codeEt.getText().toString();
			phone = phoneNumberEt.getText().toString().trim();
			if (phone.length() > 0) {
				if (codeStr.length() > 0) {
					mDialog.startLoading();
					initCode1();
				} else {
					ViewInject.toast("请输入验证码！");
				}
			} else {
				ViewInject.toast("请输入手机号！");
			}
			break;
		case R.id.nocde_message_tv:// 没收到验证码
			yuyinCodePop = new PopupWindows(mContext, allcontent);
			yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

			Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);

			break;
		}
	}

	EditText codeEt1;
	ImageView codeIv;

	/**
	 * 获取手机号 并验证
	 * 
	 * @author dwb
	 * 
	 */
	public class PopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PopupWindows(Context mContextt, View parent) {

			final View view = View.inflate(mContextt, R.layout.pop_yuyincode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContextt,
					R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt);
			Button tureBt = view.findViewById(R.id.zixun_bt);
			codeEt1 = view.findViewById(R.id.no_pass_login_code_et);
			codeIv = view.findViewById(R.id.yuyin_code_iv);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly);

			rshCodeRly.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);
				}
			});

			tureBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String codes = codeEt1.getText().toString();
					if (codes.length() > 1) {
						yanzhengCode(codes);
					} else {
						ViewInject.toast("请输入图中数字");
					}
				}
			});

			cancelBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	void yanzhengCode(String codes) {
		String phones = phoneNumberEt.getText().toString().trim();
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phones);
		maps.put("code",codes);
		maps.put("flag","bundling");
		new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData s) {
				if ("1".equals(s.code)){
					yuyinCodePop.dismiss();
					ViewInject.toast("正在拨打您的电话，请注意接听");
				}else {
					ViewInject.toast("数字错误，请重新输入");
				}
			}

		});
//		KJHttp kjh = new KJHttp();
//		KJStringParams params = new KJStringParams();
//		params.put("phone", phones);
//		params.put("code", codes);
//		params.put("flag", "bundling");
//		kjh.post(FinalConstant.YANZCODE, params, new StringCallBack() {
//
//			@Override
//			public void onSuccess(String json) {
//				if (null != json && json.length() > 0) {
//					String code = JSONUtil
//							.resolveJson(json, FinalConstant.CODE);
//					String message = JSONUtil.resolveJson(json,
//							FinalConstant.MESSAGE);
//					if (code.equals("1")) {
//						yuyinCodePop.dismiss();
//						ViewInject.toast("正在拨打您的电话，请注意接听");
//					} else {
//						ViewInject.toast("数字错误，请重新输入");
//					}
//
//				}
//			}
//		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
