package com.module.my.controller.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CeshiActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tvNumIndicator)
//    TextView mTvNumIndicator;
//    @BindView(R.id.tvNumIndicator_video)
    TextView mTvNumIndicatorVideo;
//    @BindView(R.id.indicator_video)
//    LinearLayout mIndicatorVideo;
//    @BindView(R.id.tvNumIndicator_image)
//    TextView mTvNumIndicatorImage;
//    @BindView(R.id.indicator_image)
    LinearLayout mIndicatorImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lgallery);
        ButterKnife.bind(this);
    }

//    @OnClick({R.id.indicator_video, R.id.indicator_image})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.indicator_video:
//                mIndicatorVideo.setBackground(ContextCompat.getDrawable(this,R.drawable.lgallery_video_select));
//                mIndicatorImage.setBackground(ContextCompat.getDrawable(this,R.drawable.lgralley_video_unselect));
//                break;
//            case R.id.indicator_image:
//                mIndicatorImage.setBackground(ContextCompat.getDrawable(this,R.drawable.lgallery_video_select));
//                mIndicatorVideo.setBackground(ContextCompat.getDrawable(this,R.drawable.lgralley_video_unselect));
//                break;
//        }
//    }
}
