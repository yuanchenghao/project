package com.module.my.controller.other;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.api.PayALiPay;
import com.module.my.model.api.PayWeixinApi;
import com.module.my.model.api.PayYinlianApi;
import com.module.my.model.bean.ALiPayData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.weixin.Constants;
import com.quicklyask.entity.PrePayIdData;
import com.quicklyask.entity.Result;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.Map;

/**
 * 会员支付管理类
 * Created by 裴成浩 on 2018/9/6.
 */
public class OpeningMemberManage {

    private static Activity mAtivity;
    private String TAG = "OpeningMemberManage";

    private OpeningMemberManage() {
    }

    private static OpeningMemberManage openingMemberManage = null;

    public synchronized static OpeningMemberManage getInstance(Activity activity) {
        mAtivity = activity;
        if (openingMemberManage == null) {
            openingMemberManage = new OpeningMemberManage();
        }
        return openingMemberManage;
    }


    /**
     * 支付宝支付
     *
     * @param name
     * @param order_no
     * @param price
     */
    public void payALiPay(String name, String order_no, String price) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("product_name", name);
        maps.put("order_no", order_no);
        maps.put("order_price", price);
        maps.put("installments_num", "0");
        maps.put("weikuan", "0");

        Log.e(TAG, "支付宝product_name == " + name);
        Log.e(TAG, "支付宝order_no == " + order_no);
        Log.e(TAG, "支付宝order_price == " + price);

        new PayALiPay().getCallBack(mAtivity, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(final ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        ALiPayData alidata = JSONUtil.TransformSingleBean(serverData.data, ALiPayData.class);

                        final String payinfo = alidata.getPayInfo();

                        Runnable payRunnable = new Runnable() {

                            @Override
                            public void run() {
                                // 构造PayTask 对象
                                PayTask alipay = new PayTask(mAtivity);
                                // 调用支付接口
                                String result = alipay.pay(payinfo, true);
                                Result resultObj = new Result(result);
                                String resultStatus = resultObj.resultStatus;

                                if (payStatusCallback != null) {
                                    Log.e(TAG, "result === " + result);
                                    Log.e(TAG, "resultStatus === " + resultStatus);
                                    payStatusCallback.PayStatus(resultStatus);
                                }
                            }
                        };

                        Thread payThread = new Thread(payRunnable);
                        payThread.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(mAtivity, serverData.message, Toast.LENGTH_LONG).show();
                }
            }

        });
    }

    /**
     * 微信支付
     *
     * @param name
     * @param order_no
     * @param price
     */
    public void payWeixin(String name, String order_no, String price) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("product_name", name);//
        maps.put("order_no", order_no);//
        maps.put("order_price", price);

        Log.e(TAG, "微信product_name == " + name);
        Log.e(TAG, "微信order_no == " + order_no);
        Log.e(TAG, "微信order_price == " + price);

        new PayWeixinApi().getCallBack(mAtivity, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    PrePayIdData payData = JSONUtil.TransformWXpayidShare(serverData.data);
                    String prepayId = payData.getPrepay_id();
                    String sign = payData.getSign();
                    String noncestr = payData.getNoncestr();
                    String timestamp = payData.getTimestamp();

                    String orderid = payData.getOrder_id();

                    Cfg.saveStr(mAtivity, "order_id", orderid);
                    Cfg.saveStr(mAtivity, "pay_sao", "3");
                    Cfg.saveStr(mAtivity, "is_group", "0");

                    genPayReq(prepayId, sign, noncestr, timestamp);
                } else {
                    ViewInject.toast(serverData.message);
                }
            }

        });
    }

    /**
     * 微信支付
     *
     * @param prepay_id
     */
    private void genPayReq(String prepay_id, String sign, String noncestr, String timestamp) {
        PayReq req = new PayReq();
        req.appId = Constants.APP_ID;
        req.partnerId = Constants.MCH_ID;
        req.prepayId = prepay_id;
        req.packageValue = "prepay_id=" + prepay_id;
        req.nonceStr = noncestr;
        req.timeStamp = timestamp;

        req.sign = sign;

        int type = req.getType();

        StringBuffer sb = new StringBuffer();
        sb.append("sign\n" + req.sign + "\n\n");

        IWXAPI msgApi = WXAPIFactory.createWXAPI(mAtivity, null);
        // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
        msgApi.registerApp(Constants.APP_ID);
        msgApi.sendReq(req);

    }


    /**
     * 银联支付
     *
     * @param name
     * @param order_no
     * @param price
     */
    public void payYinlian(String name, String order_no, String price) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("product_name", name);
        maps.put("order_no", order_no);
        maps.put("order_price", price);
        new PayYinlianApi().getCallBack(mAtivity, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    PrePayIdData payData = JSONUtil.TransformWXpayidShare(serverData.data);
                    String tn = payData.getPrepay_id();

                    if (!TextUtils.isEmpty(tn)) {
                        UPPayAssistEx.startPayByJAR(mAtivity, PayActivity.class, null, null, tn, "00");
                    } else {
                        ViewInject.toast("请稍后重试");
                    }

                } else {
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }

    private PayStatusCallback payStatusCallback;

    public interface PayStatusCallback {
        void PayStatus(String code);
    }

    public void setPayStatusCallback(PayStatusCallback payStatusCallback) {
        this.payStatusCallback = payStatusCallback;
    }
}
