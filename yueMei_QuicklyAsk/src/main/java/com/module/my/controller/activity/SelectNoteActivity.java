package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.my.controller.adapter.NoteAdapter;
import com.module.my.model.api.LoadLinkDataApi;
import com.module.my.model.bean.NoteBookListData;
import com.module.other.activity.MakeNewNoteActivity2;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 选择要写日记的本子
 * <p>
 * Created by dwb on 16/3/21.
 */
public class SelectNoteActivity extends BaseActivity {

    @BindView(id = R.id.write_link_list)
    private ListView taolist;
    @BindView(id = R.id.write_exp_back)
    private RelativeLayout back;

    private Activity mContext;

    private String uid;

    private List<NoteBookListData> lvlinkData = new ArrayList<>();
    private NoteAdapter mlinkAdapter;

    private View bottomView;
    private LinearLayout newNoteLy;

    private String cateid;
    private String hosid;
    private String hosname;
    private String userid;
    private String docname;
    private String fee;
    private String taoid;
    private String server_id;


    private String cateid_;
    private String hosid_;
    private String hosname_;
    private String userid_;
    private String docname_;
    private String fee_;
    private String taoid_;
    private String server_id_;
    private String sharetime_;
    private String noteid_;
    private String notetitle_;

    private String noteIvstr;
    private String noteTitlestr;
    private String noteFuTstr;
    private String pageStr;


    @BindView(id = R.id.new_make_rly)
    private RelativeLayout makeNewRLy;
    @BindView(id = R.id.note_new_ly1)
    private LinearLayout newNoteLy1;

    @BindView(id = R.id.wriet_note_tips)
    private RelativeLayout writeTips;

    private int onclik = 1;


    @BindView(id = R.id.all_content)
    private LinearLayout allcontent;
    private String TAG = "SelectNoteActivity";
    private String beforeMaxDay;
    private PageJumpManager pageJumpManager;
    private HashMap<String, String> mMap = new HashMap<>();

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_select_note);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = SelectNoteActivity.this;

        pageJumpManager = new PageJumpManager(mContext);

        uid = Utils.getUid();
        Intent it = getIntent();

        cateid = it.getStringExtra("cateid");
        hosid = it.getStringExtra("hosid");
        hosname = it.getStringExtra("hosname");
        userid = it.getStringExtra("userid");
        docname = it.getStringExtra("docname");
        fee = it.getStringExtra("fee");
        taoid = it.getStringExtra("taoid");
        server_id = it.getStringExtra("server_id");

        loadlinkData();

        LayoutInflater mInflater = getLayoutInflater();
        if (bottomView == null) {
            bottomView = mInflater.inflate(R.layout.item_note_select_final, null);
            taolist.addFooterView(bottomView);

            newNoteLy = bottomView.findViewById(R.id.note_new_ly);
            newNoteLy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, MakeNewNoteActivity2.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("cateid", cateid);
                    bundle.putString("userid", userid);
                    bundle.putString("hosid", hosid);
                    bundle.putString("hosname", hosname);
                    bundle.putString("docname", docname);
                    bundle.putString("fee", fee);
                    bundle.putString("taoid", taoid);
                    bundle.putString("server_id", server_id);

                    intent.putExtra("data", bundle);

                    startActivity(intent);
                    finish();
                }
            });
        }

        newNoteLy1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MakeNewNoteActivity2.class);

                Bundle bundle = new Bundle();
                bundle.putString("cateid", cateid);
                bundle.putString("userid", userid);
                bundle.putString("hosid", hosid);
                bundle.putString("hosname", hosname);
                bundle.putString("docname", docname);
                bundle.putString("fee", fee);
                bundle.putString("taoid", taoid);
                bundle.putString("server_id", server_id);

                intent.putExtra("data", bundle);

                startActivity(intent);
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });


        String ifTips = Cfg.loadStr(mContext, "select_note", "");
        if (ifTips.length() > 0) {
            writeTips.setVisibility(View.GONE);
        } else {
            writeTips.setVisibility(View.VISIBLE);
            Cfg.saveStr(mContext, "select_note", "1");
        }


        writeTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onclik == 1) {
                    writeTips.setBackgroundResource(R.drawable.newnote_tips_1);
                    onclik++;
                } else if (onclik == 2) {
                    writeTips.setBackgroundResource(R.drawable.newnote_tips_2);
                    onclik++;
                } else if (onclik == 3) {
                    writeTips.setBackgroundResource(R.drawable.newnote_tips_3);
                    onclik++;
                } else if (onclik == 4) {
                    writeTips.setVisibility(View.GONE);
                }
            }
        });

    }

    void loadlinkData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        new LoadLinkDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {

                    try {
                        lvlinkData = JSONUtil.transformNoteBookList(serverData.data);


                        mlinkAdapter = new NoteAdapter(mContext, lvlinkData);
                        taolist.setAdapter(mlinkAdapter);

                        taolist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> arg0, View arg1, int po, long arg3) {

                                cateid_ = "1090";
                                userid_ = lvlinkData.get(po).getDoc_id();
                                hosid_ = lvlinkData.get(po).getHos_id();
                                hosname_ = lvlinkData.get(po).getHosname();
                                docname_ = lvlinkData.get(po).getDocname();
                                fee_ = lvlinkData.get(po).getPrice();
                                taoid_ = lvlinkData.get(po).get_id();
                                server_id_ = lvlinkData.get(po).getServer_id();
                                sharetime_ = lvlinkData.get(po).getSharetime();
                                noteid_ = lvlinkData.get(po).getT_id();
                                notetitle_ = lvlinkData.get(po).getTitle();


                                noteIvstr = lvlinkData.get(po).getImg();
                                noteTitlestr = lvlinkData.get(po).getTitle();
                                noteFuTstr = lvlinkData.get(po).getSubtitle();
                                pageStr = lvlinkData.get(po).getPage();
                                beforeMaxDay = lvlinkData.get(po).getBefore_max_day();

                                String isnew = lvlinkData.get(po).getIs_new();

                                if (isnew.equals("1")) {

                                    HashMap<String, String> mMap = new HashMap<>();
                                    mMap.put("cateid", "1090");
                                    mMap.put("userid", userid_);
                                    mMap.put("hosid", hosid_);
                                    mMap.put("hosname", hosname_);
                                    mMap.put("docname", docname_);
                                    mMap.put("fee", fee_);
                                    mMap.put("taoid", taoid_);
                                    mMap.put("server_id", server_id_);
                                    mMap.put("sharetime", sharetime_);
                                    mMap.put("type", "2");
                                    mMap.put("noteid", noteid_);
                                    mMap.put("notetitle", notetitle_);
                                    mMap.put("beforemaxday", beforeMaxDay);

                                    mMap.put("noteiv", noteIvstr);
                                    mMap.put("notetitle_", noteTitlestr);
                                    mMap.put("notefutitle", noteFuTstr);
                                    mMap.put("page", pageStr);
                                    mMap.put("consumer_certificate", "0");
                                    pageJumpManager.jumpToWriteNoteActivity(PageJumpManager.DEFAULT_LOGO, mMap);

                                    finish();

                                } else {
                                    new PopupWindows(SelectNoteActivity.this, allcontent);
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    makeNewRLy.setVisibility(View.VISIBLE);
                    taolist.setVisibility(View.GONE);
                }
            }

        });
    }


    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(final Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_select_note_look, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button bt1 = view.findViewById(R.id.item_popupwindows_camera);

            Button bt5 = view.findViewById(R.id.item_popupwindows_Photo2);

            Button bt3 = view.findViewById(R.id.item_popupwindows_cancel);


            //查看日记
            bt1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent it2 = new Intent();
                    it2.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + "/forum/shareinfo/id/" + noteid_ + "/");
                    it2.putExtra("qid", noteid_);
                    it2.setClass(mContext, DiariesAndPostsActivity.class);
                    startActivity(it2);

                    dismiss();

                }
            });


            //继续写日记
            bt5.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    mMap.put("cateid", "1090");
                    mMap.put("userid", userid_);
                    mMap.put("hosid", hosid_);
                    mMap.put("hosname", hosname_);
                    mMap.put("docname", docname_);
                    mMap.put("fee", fee_);
                    mMap.put("taoid", taoid_);
                    mMap.put("server_id", server_id_);
                    mMap.put("sharetime", sharetime_);
                    mMap.put("type", "1");
                    mMap.put("noteid", noteid_);
                    mMap.put("notetitle", notetitle_);
                    mMap.put("beforemaxday", beforeMaxDay);

                    mMap.put("noteiv", noteIvstr);
                    mMap.put("notetitle_", noteTitlestr);
                    mMap.put("notefutitle", noteFuTstr);
                    mMap.put("page", pageStr);
                    mMap.put("consumer_certificate", "0");

                    pageJumpManager.jumpToWriteNoteActivity(PageJumpManager.DEFAULT_LOGO, mMap);

                    finish();

                    dismiss();

                }
            });

            //取消
            bt3.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            // 点击 之外 消失
            view.setOnTouchListener(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });

        }
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}