package com.module.my.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.module.api.VersionApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.base.view.ui.HomeBanner;
import com.module.base.view.ui.YMBanner;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.InstructionWebActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.utils.DialogUtils;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.my.controller.adapter.MyIconAdapter;
import com.module.my.model.api.MyTaoListApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.MyIconBean;
import com.module.my.model.bean.RecoveryReminder;
import com.module.my.model.bean.ShenHeData;
import com.module.my.model.bean.UserData;
import com.module.my.view.view.VipScrollView;
import com.module.other.activity.HomeDiarySXActivity;
import com.module.other.activity.ProjectContrastActivity;
import com.module.other.activity.SearchProjectActivity;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.module.shopping.model.api.CartSkuNumberApi;
import com.module.shopping.model.bean.CartSkuNumberData;
import com.module.taodetail.model.bean.HomePersonTao;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.HuanDengData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.qmuiteam.qmui.widget.QMUIObservableScrollView;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.VersionJCData;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.CustomDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog2;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class HomePersonActivity extends YMBaseActivity {

    private final String TAG = "HomePersonActivity";
    @BindView(R.id.riv_user_head)
    ImageView personHeadIv;      //用户头像
    @BindView(R.id.tv_user_name)
    TextView personNameTv;        //用户名
    @BindView(R.id.tv_user_level)
    TextView tvUserLevel;       //用户等级
    @BindView(R.id.plus_biaoshi)
    ImageView mPlusBiaoshi;      //会员标识
    @BindView(R.id.tv_my_modifieddata)
    TextView myModifiedData;      //登录注册
    @BindView(R.id.rl_my_loginclick)
    RelativeLayout rlMyLoginclick;
    @BindView(R.id.puls_kaitong)
    LinearLayout mPlusYikaitong;
    @BindView(R.id.puls_weikaikong)
    LinearLayout mPlusWeikaitong;
    @BindView(R.id.puls_text)
    TextView pulsText;
    @BindView(R.id.plus_yuji_visibity)
    LinearLayout plusYujiVisibity;
    @BindView(R.id.plus_content)
    RelativeLayout plusContent;
    @BindView(R.id.newuser_redpacket)
    ImageView newuserRedpacket;     //新人红包
    @BindView(R.id.mu_order_click)
    RelativeLayout muOrderClick;    //我的订单
    @BindView(R.id.my_icon_list)
    RecyclerView myIconList;        //购物车收藏优惠卷等
    @BindView(R.id.home_person_banner)
    YMBanner homePersonBanner;        //我的页banner
    @BindView(R.id.banner_visorgone)
    LinearLayout bannerVisorgone;   //banner容器
    @BindView(R.id.my_tao_list)
    RecyclerView myTaoList;         //淘列表
    @BindView(R.id.home_person_sview)
    VipScrollView homePersonSview;      //动效view
    @BindView(R.id.title_name)
    TextView titleName;     //标题名字
    @BindView(R.id.home_person_bg_rly)
    RelativeLayout homePersonBgRly;  //标题
    @BindView(R.id.my_setting)
    TextView mySetting;
    @BindView(R.id.person_setting_new_iv)
    View personSettingNewIv;        //设置的小红点
    @BindView(R.id.home_person_bg_line)
    View titleLine;
    @BindView(R.id.self_setting)
    RelativeLayout selfSetting;     //设置
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.tv_my_diary_num)
    TextView tvMyDiaryNum;
    @BindView(R.id.ll_diary_click)
    LinearLayout llDiaryClick;
    @BindView(R.id.tv_my_invitaon_num)
    TextView tvMyInvitaonNum;
    @BindView(R.id.ll_invitaon_click)
    LinearLayout llInvitaonClick;
    @BindView(R.id.tv_my_footmark_num)
    TextView tvMyFootmarkNum;
    @BindView(R.id.ll_footmark_click)
    LinearLayout llFootmarkClick;
    @BindView(R.id.tv_my_attention_num)
    TextView tvMyAttentionNum;
    @BindView(R.id.ll_attention_click)
    LinearLayout llAttentionClick;
    @BindView(R.id.tv_my_fans_num)
    TextView tvMyFansNum;
    @BindView(R.id.ll_fans_click)
    LinearLayout llFansClick;
    @BindView(R.id.my_staypay_news)
    TextView stayPayNews;
    @BindView(R.id.my_staypay_click)
    LinearLayout myStaypayClick;
    @BindView(R.id.person_comment_new_iv)
    TextView stayConsumNews;
    @BindView(R.id.my_stayconsume_click)
    LinearLayout myStayconsumeClick;
    @BindView(R.id.my_stay_writediary_news)
    TextView stayWriteDiaryNews;
    @BindView(R.id.my_evaluation_click)
    LinearLayout myEvaluationClick;
    @BindView(R.id.person_evaluation_new_iv)
    TextView stayEvaluationNews;
    @BindView(R.id.my_staywritediary_click)
    LinearLayout myStaywritediaryClick;
    @BindView(R.id.my_refund_click)
    LinearLayout myRefundClick;
    @BindView(R.id.calendar_container)
    RelativeLayout calendarContainer;
    @BindView(R.id.calendar_title)
    TextView calendarTitle;
    @BindView(R.id.calendar_btn)
    Button calendarBtn;

    private Activity mContext;
    private PageJumpManager pageJumpManager;
    private ArrayList<String> myIconName = new ArrayList<>(Arrays.asList(
            "购物车", "我的收藏", "我的优惠券", "悦美钱包"
            , "签到有奖", "任务中心", "我的问答", "我的回复"
            , "领券中心", "安心保障", "颜值币商城", "问医生"
            , "在线客服", "电话咨询"));
    private ArrayList<Integer> myIcon = new ArrayList<>(Arrays.asList(
            R.drawable.my_shopping_cart, R.drawable.my_collect, R.drawable.my_daijinquan, R.drawable.ymwallet
            , R.drawable.sign_person, R.drawable.task_center, R.drawable.my_question, R.drawable.my_answer
            , R.drawable.lingquanzhongxing, R.drawable.secure, R.drawable.my_mall, R.drawable.my_ask_doc
            , R.drawable.my_onlineservice, R.drawable.my_phone
    ));
    private ArrayList<MyIconBean> myIconBeans = new ArrayList<>();
    private List<String> bannerList = new ArrayList<>();
    private List<HomeTaoData> lvHotIssueData = new ArrayList<>();
    private MyIconAdapter myIconAdapter;
    private String isphone;
    private String mUser_more;
    private String phoneStr;
    private String phone;
    private HashMap<String, String> mRecoveryReminderEvent_params;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_home_person;
    }

    @Override
    protected void initView() {
        mContext = HomePersonActivity.this;
        QMUIStatusBarHelper.setStatusBarLightMode(mContext);
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.LayoutParams params = homePersonBgRly.getLayoutParams();
        params.height = statusbarHeight + Utils.dip2px(50);
        homePersonBgRly.setLayoutParams(params);
        ViewGroup.LayoutParams params2 = selfSetting.getLayoutParams();
        params2.height = statusbarHeight + Utils.dip2px(50);
        selfSetting.setLayoutParams(params2);
        pageJumpManager = new PageJumpManager(mContext);
        homePersonSview.setSpringback();
        homePersonSview.addOnScrollChangedListener(new QMUIObservableScrollView.OnScrollChangedListener() {
            @Override
            public void onScrollChanged(QMUIObservableScrollView scrollView, int l, int t, int oldl, int oldt) {
                if (t > Utils.dip2px(100)) {
                    homePersonBgRly.setVisibility(View.VISIBLE);

                    float alpha = (t - Utils.dip2px(100)) / ((Utils.dip2px(237) - homePersonBgRly.getHeight()) * 1.0f); //产生渐变效果
                    Log.e(TAG, "alpha == " + alpha);

                    if (alpha > 0.3) {
                        mySetting.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                    } else {
                        mySetting.setTextColor(Utils.getLocalColor(mContext, R.color.white));
                    }

                    if (alpha < 1) {
                        titleLine.setVisibility(View.GONE);
                    } else {
                        titleLine.setVisibility(View.VISIBLE);
                    }

                    homePersonBgRly.setAlpha(alpha);

                } else {
                    homePersonBgRly.setVisibility(View.GONE);
                }
            }
        });

        myIconBeans.clear();
        for (int i = 0; i < myIconName.size(); i++) {
            MyIconBean myIconBean = new MyIconBean();
            myIconBean.setName(myIconName.get(i));
            myIconBean.setIcon(myIcon.get(i));
            myIconBeans.add(myIconBean);

        }
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 4);
        gridLayoutManager.setScrollEnable(false);
        myIconList.setLayoutManager(gridLayoutManager);
        myIconAdapter = new MyIconAdapter(R.layout.my_icon_list_item, myIconBeans, windowsWight);
        myIconList.setAdapter(myIconAdapter);
        myIconAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position == 0 || position == myIconBeans.size() - 1) {
                    onItemClickJump(position);
                } else {
                    if (Utils.isLoginAndBind(mContext)) {
                        onItemClickJump(position);
                    }
                }
            }
        });
        calendarContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)){
                    if (mRecoveryReminderEvent_params != null){
                        YmStatistics.getInstance().tongjiApp(mRecoveryReminderEvent_params);
                    }

                    WebData webData = new WebData(FinalConstant.HOME_PERSON_CALENDER);
                    webData.setShowTitle(false);
//                                    webData.setThemeResid(R.style.AppThemeprice);
                    WebUtil.getInstance().startWebActivity(mContext, webData);
                }else {
                    HashMap<String, String> parms = new HashMap<>();
                    parms.put("event_name","calendar_enter_click");
                    parms.put("type","62");
                    parms.put("to_page_type","160");
                    parms.put("to_page_id","0");
                    YmStatistics.getInstance().tongjiApp(mRecoveryReminderEvent_params);
                }

            }
        });
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)){
                    if (mRecoveryReminderEvent_params != null){
                        YmStatistics.getInstance().tongjiApp(mRecoveryReminderEvent_params);
                    }
                    WebData webData = new WebData(FinalConstant.HOME_PERSON_CALENDER);
                    webData.setShowTitle(false);
//                                    webData.setThemeResid(R.style.AppThemeprice);
                    WebUtil.getInstance().startWebActivity(mContext, webData);
                }else {
                    HashMap<String, String> parms = new HashMap<>();
                    parms.put("event_name","calendar_enter_click");
                    parms.put("type","62");
                    parms.put("to_page_type","160");
                    parms.put("to_page_id","0");
                    YmStatistics.getInstance().tongjiApp(mRecoveryReminderEvent_params);
                }
            }
        });
    }

    @Override
    protected void initData() {

        ExposureLoginData exposureLoginData = new ExposureLoginData("62", "0");
        mFunctionManager.saveStr(FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));

        initVsersion();
        loadTaoData();
        initShenHe();
    }


    /**
     * 版本检测
     */
    void initVsersion() {

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("uid", Utils.getUid());
        new VersionApi().getCallBack(mContext, keyValues, new BaseCallBackListener<VersionJCData>() {
            @Override
            public void onSuccess(VersionJCData vrData) {
                String vserCode = vrData.getVer();

                String vserIntNet = vserCode.replace(".", "");
                int vserInt = Integer.parseInt(vserIntNet);


                String versionName = Utils.getVersionName();
                String versionCode = versionName.length() == 3 ? versionName + "0" : versionName;
                int vserIntLoc = Integer.parseInt(versionCode);

                if (vserIntLoc < vserInt) {
                    personSettingNewIv.setVisibility(View.VISIBLE);
                } else {
                    personSettingNewIv.setVisibility(View.GONE);
                }
            }

        });
    }

    /**
     * 登录或取消登录
     */
    private void loginOrCancle() {

        if (isLogin()) {
            loadUserData();
            newuserRedpacket.setVisibility(View.GONE);
            String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
            if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
                MyIconBean myIconBean = myIconBeans.get(0);
                myIconBean.setNum(cartNumber);
                myIconAdapter.notifyDataSetChanged();
            }
        } else {
            Glide.with(mContext).load(R.drawable.default_head).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.default_head).error(R.drawable.default_head).into(personHeadIv);
            personNameTv.setText("快来登录吧~");
            tvUserLevel.setVisibility(View.GONE);
            myModifiedData.setVisibility(View.VISIBLE);
            mPlusBiaoshi.setVisibility(View.GONE);
            myModifiedData.setText("登录/注册");
            mPlusYikaitong.setVisibility(View.GONE);
            mPlusWeikaitong.setVisibility(View.VISIBLE);
            plusYujiVisibity.setVisibility(View.GONE);
            tvMyDiaryNum.setText("");
            tvMyInvitaonNum.setText("");
            tvMyFootmarkNum.setText("");
            tvMyAttentionNum.setText("");
            tvMyFansNum.setText("");
            stayPayNews.setVisibility(View.GONE);
            stayConsumNews.setVisibility(View.GONE);
            stayWriteDiaryNews.setVisibility(View.GONE);
            stayEvaluationNews.setVisibility(View.GONE);
            String is_show = Cfg.loadStr(mContext, FinalConstant.ISSHOW, "1");
            if (TextUtils.isEmpty(is_show)) {
                newuserRedpacket.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(R.drawable.newuser_redpacket).asGif().into(newuserRedpacket);
            } else {
                newuserRedpacket.setVisibility(View.GONE);
            }

            if ("客多多".equals(myIconBeans.get(myIconBeans.size() - 1).getName())) {
                myIconAdapter.removeData(myIconBeans.size() - 1);
            }
            MyIconBean iconBean = myIconBeans.get(4);
            iconBean.setIsSign("0");
            myIconAdapter.notifyItemChanged(4);

            CartSkuNumberApi cartSkuNumberApi = new CartSkuNumberApi();
            cartSkuNumberApi.getCallBack(mContext, cartSkuNumberApi.getmCartSkuNumberHashMap(), new BaseCallBackListener<CartSkuNumberData>() {
                @Override
                public void onSuccess(CartSkuNumberData data) {
                    if (Integer.parseInt(data.getCart_number()) >= 100) {
                        Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, "99+");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, data.getCart_number());
                    }
                    //购物车数量
                    String cartNumber = data.getCart_number();
                    if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
                        MyIconBean myIconBean = myIconBeans.get(0);
                        myIconBean.setNum(cartNumber);
                        myIconAdapter.notifyDataSetChanged();
                    }
                }
            });

        }


    }

    /**
     * 用来解决SP MyApplication.getContext()没有实时性  1已登录 0未登录
     *
     * @return
     */


    private boolean isLogin() {
        String uid = Cfg.loadStr(mContext, FinalConstant.HOME_PERSON_UID, "0");
        if ("0".equals(uid) || TextUtils.isEmpty(uid)) {
            return false;
        } else return true;
    }

    private void initShenHe() {
        new ShenHeApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {

            @Override
            public void onSuccess(ShenHeData shenHeData) {
                if (shenHeData != null) {
                    phone = shenHeData.getPhone();
                    Log.e(TAG, "phone ==" + phone);

                }
            }
        });
    }


    /**
     * 获取用户信息
     */
    private void loadUserData() {
        Map<String, Object> params = new HashMap<>();
        params.put("id", Utils.getUid());
        new BBsDetailUserInfoApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    String id = userData.get_id();
                    String img = userData.getImg();
                    String nickName = userData.getNickname();
                    String sex = userData.getSex();
                    String birthday = userData.getBirthday();
                    String province = userData.getProvince();
                    String city = userData.getCity();
                    String lvStr = userData.getLevel();  //用户等级
                    String loginphone = userData.getLoginphone();
                    String real_name = userData.getReal_name();
                    Cfg.saveStr(mContext, "real_name", real_name);
                    isphone = userData.getIsphone();
                    phoneStr = userData.getPhone();
                    String mSharenum = userData.getSharenum();             //日记数
                    String mPostnum = userData.getPostnum();        //帖子数
                    String mFollowingnum = userData.getFollowingnum();//关注数
                    String mFollowingmenum = userData.getFollowingmenum();//粉丝数
                    String mBrowsenum = userData.getBrowsenum();//足迹数
                    tvMyDiaryNum.setText(mSharenum);
                    tvMyInvitaonNum.setText(mPostnum);
                    tvMyFootmarkNum.setText(mBrowsenum);
                    tvMyAttentionNum.setText(mFollowingnum);
                    tvMyFansNum.setText(mFollowingmenum);

                    //	1是客多多
                    mUser_more = userData.getUser_more();
                    String mSetuser_desc = userData.getSetuser_desc();


                    String mNopaynum = userData.getNopaynum();//待支付数
                    String mPaynum = userData.getPaynum();//待消费数
                    String mEvaluationnum = userData.getEvaluationnum();//待评价数
                    String mNodiarynum = userData.getNodiarynum();//待写日记数

                    if (!TextUtils.isEmpty(mNopaynum)){
                        if (!"0".equals(mNopaynum)) {
                            stayPayNews.setVisibility(View.VISIBLE);
                            int i = Integer.parseInt(mNopaynum);
                            if (i > 99) {
                                stayPayNews.setText("99+");
                            } else {
                                stayPayNews.setText(mNopaynum);
                            }
                        }
                    }

                    if (!TextUtils.isEmpty(mPaynum)){
                        if (!"0".equals(mPaynum)) {
                            stayConsumNews.setVisibility(View.VISIBLE);
                            int i = Integer.parseInt(mPaynum);
                            if (i > 99) {
                                stayConsumNews.setText("99+");
                            } else {
                                stayConsumNews.setText(mPaynum);
                            }
                        }
                    }


                    //待评价数
                    if (!TextUtils.isEmpty(mEvaluationnum)){
                        if (!"0".equals(mEvaluationnum)) {
                            stayEvaluationNews.setVisibility(View.VISIBLE);
                            int i = Integer.parseInt(mEvaluationnum);
                            if (i > 99) {
                                stayEvaluationNews.setText("99+");
                            } else {
                                stayEvaluationNews.setText(mEvaluationnum);
                            }
                        }
                    }

                    if (!TextUtils.isEmpty(mNodiarynum)){
                        if (!"0".equals(mNodiarynum)) {
                            stayWriteDiaryNews.setVisibility(View.VISIBLE);
                            int i = Integer.parseInt(mNodiarynum);
                            if (i > 99) {
                                stayWriteDiaryNews.setText("99+");
                            } else {
                                stayWriteDiaryNews.setText(mNodiarynum);
                            }
                        }
                    }


                    MyIconBean myIconBean = myIconBeans.get(0);
                    //购物车数量
                    String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
                    if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
                        myIconBean.setNum(cartNumber);
                        myIconAdapter.notifyItemChanged(0);
                    }

                    if ("1".equals(mUser_more)) {
                        MyIconBean myIconBean1 = new MyIconBean();
                        myIconBean1.setName("客多多");
                        myIconBean1.setIcon(R.drawable.keduoduo);
                        if (!"客多多".equals(myIconBeans.get(myIconBeans.size() - 1).getName())) {
                            myIconAdapter.addData(myIconBeans.size(), myIconBean1);
                        }

                    }
                    String is_signin = userData.getIs_signin();
                    if ("0".equals(is_signin)) {
                        MyIconBean iconBean = myIconBeans.get(4);
                        iconBean.setIsSign("0");
                        myIconAdapter.notifyItemChanged(4);
                    } else {
                        MyIconBean iconBean = myIconBeans.get(4);
                        iconBean.setIsSign("1");
                        myIconAdapter.notifyItemChanged(4);
                    }

                    String is_member = userData.getIs_member(); //	1是会员
                    String member_discount = userData.getMember_discount();//	会员预计可省金额
                    plusYujiVisibity.setVisibility(View.VISIBLE);
                    pulsText.setText("¥" + member_discount);


                    if (null != lvStr) {
                        tvUserLevel.setVisibility(View.VISIBLE);
                        tvUserLevel.setText("Lv." + lvStr);
                        if ("1".equals(is_member)) {
                            myModifiedData.setVisibility(View.GONE);
                            mPlusBiaoshi.setVisibility(View.VISIBLE);
                            mPlusBiaoshi.setBackgroundResource(R.drawable.plus_biaoshi);
                            mPlusYikaitong.setVisibility(View.VISIBLE);
                            mPlusYikaitong.setClickable(true);
                            mPlusWeikaitong.setVisibility(View.GONE);


                        } else {
                            if ("1".equals(mUser_more)) {
                                mPlusBiaoshi.setVisibility(View.VISIBLE);
                                mPlusBiaoshi.setBackgroundResource(R.drawable.keduoduobiaoshi);
                            } else {
                                mPlusBiaoshi.setVisibility(View.GONE);
                            }
                            myModifiedData.setVisibility(View.VISIBLE);
                            mPlusYikaitong.setVisibility(View.GONE);
                            mPlusWeikaitong.setVisibility(View.VISIBLE);
                            mPlusWeikaitong.setClickable(true);
                            myModifiedData.setText(mSetuser_desc);
                        }
                    }

                    Glide.with(mContext).load(img).transform(new GlideCircleTransform(mContext))
                            .placeholder(R.drawable.default_head).error(R.drawable.default_head)
                            .into(personHeadIv);

                    personNameTv.setText(nickName);

                    //我的日历
                    RecoveryReminder recoveryReminder = userData.getRecoveryReminder();
                    if (recoveryReminder != null){
                        String showButtonTitle = recoveryReminder.getShow_button_title();
                        String showTitle = recoveryReminder.getShow_title();
                        String showType = recoveryReminder.getShow_type();
                        mRecoveryReminderEvent_params = recoveryReminder.getEvent_params();

                        if (!TextUtils.isEmpty(showTitle)){
                            calendarTitle.setText(showTitle);
                            calendarBtn.setText(showButtonTitle);

                        }
                    }

                    Utils.setUid(id);
                    Cfg.saveStr(mContext, FinalConstant.UHEADIMG, img);
                    Cfg.saveStr(mContext, FinalConstant.UNAME, nickName);
                    Cfg.saveStr(mContext, FinalConstant.UPROVINCE, province);
                    Cfg.saveStr(mContext, FinalConstant.UCITY, city);
                    Cfg.saveStr(mContext, FinalConstant.USEX, sex);
                    Cfg.saveStr(mContext, FinalConstant.UBIRTHDAY, birthday);


                    if (phoneStr.length() > 0) {
                        if (phoneStr.equals("0")) {
                            Cfg.saveStr(mContext, FinalConstant.UPHONE, "");
                        } else {
                            Cfg.saveStr(mContext, FinalConstant.UPHONE, phoneStr);
                        }
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.UPHONE, "");
                    }

                    if ("".equals(loginphone) || "0".equals(loginphone)) {
                        Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE, loginphone);
                    }

                    if ("".equals(isphone) || "0".equals(isphone)) {
                        Cfg.saveStr(mContext, FinalConstant.USERISPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.USERISPHONE, isphone);
                    }
                } else {
                    mFunctionManager.showShort(serverData.message);
//                    Utils.setUid("0");
//                    Cfg.saveStr(mContext, FinalConstant.UHEADIMG, "");
//                    Cfg.saveStr(mContext, FinalConstant.UNAME, "");
//                    Cfg.saveStr(mContext, FinalConstant.UPROVINCE, "");
//                    Cfg.saveStr(mContext, FinalConstant.UCITY, "");
//                    Cfg.saveStr(mContext, FinalConstant.USEX, "");
//                    Cfg.saveStr(mContext, FinalConstant.UBIRTHDAY, "");
                }

            }
        });
    }

    /**
     * banner 淘列表的数据
     */
    private void loadTaoData() {
        new MyTaoListApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    HomePersonTao homePersonTao = JSONUtil.TransformSingleBean(serverData.data, HomePersonTao.class);
                    dataToview(homePersonTao);
                } else {
                    mFunctionManager.showShort(serverData.message);
                }

            }
        });
    }

    /**
     * 加载我的页淘列表和banner
     *
     * @param homePersonTao
     */
    void dataToview(HomePersonTao homePersonTao) {
        try {
            bannerList.clear();
            lvHotIssueData = homePersonTao.getList();
            final List<HuanDengData> huandeng = homePersonTao.getHuandeng();
            if (huandeng != null && huandeng.size() > 0) {
                bannerVisorgone.setVisibility(View.VISIBLE);
                for (int i = 0; i < huandeng.size(); i++) {
                    bannerList.add(huandeng.get(i).getImg());
                }
                //设置banner样式
                homePersonBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                //设置图片集合
                homePersonBanner.setImagesData(bannerList);
                homePersonBanner.setOnBannerListener(new OnBannerListener() {
                    @Override
                    public void OnBannerClick(int position) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MY_HUANDENG, (position + 1) + ""), huandeng.get(position).getEvent_params());
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(huandeng.get(position).getUrl(), "0", "0");
                    }
                });
            } else {
                bannerVisorgone.setVisibility(View.GONE);
            }
            TaoListAdapter taoAdapter = new TaoListAdapter(mContext, lvHotIssueData);
            AutoLinearLayoutManager autoLinearLayoutManager = new AutoLinearLayoutManager(mContext);
            myTaoList.setHasFixedSize(true);
            myTaoList.setNestedScrollingEnabled(false);
            myTaoList.setFocusableInTouchMode(false);
            myTaoList.setAdapter(taoAdapter);
            myTaoList.setLayoutManager(autoLinearLayoutManager);
            taoAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(TaoListDataType taoData, int position) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    HomeTaoData data = taoData.getTao();
                    final String _id = data.get_id();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MY_TAOLIST, (position + 1) + ""), lvHotIssueData.get(position).getEvent_params());
                    Intent it2 = new Intent();
                    it2.putExtra("id", _id);
                    it2.putExtra("source", "0");
                    it2.putExtra("objid", "0");
                    it2.setClass(mContext, TaoDetailActivity.class);
                    startActivity(it2);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!SystemTool.checkNet(mContext)) {
            mFunctionManager.showShort(getResources().getString(R.string.no_wifi_tips));
        }
        loginOrCancle();
    }


    private void onItemClickJump(int position) {
        switch (position) {
            case 0://购物车
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "cart"));
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
                break;
            case 1://我的收藏
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "collect"));
                Intent itco = new Intent();
                itco.setClass(mContext, MyCollectActivity550.class);
                startActivity(itco);
                break;
            case 2://我的优惠券
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "mycoupons"));
                Intent it6 = new Intent();
                it6.setClass(mContext, MyDaijinjuanActivity.class);
                it6.putExtra("link", "");
                startActivity(it6);
                break;
            case 3://悦美钱包
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "wallet"));
                Intent it7 = new Intent();
                it7.setClass(mContext, YuemeiWalletActitivy.class);
                startActivity(it7);
                break;
            case 4://签到有奖
                Intent intent6721 = new Intent(mContext, SignWebActivity.class);
                intent6721.putExtra("link", FinalConstant.SIGN_WEB);
                startActivity(intent6721);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "checkinPrize"), new ActivityTypeData("62"));
                break;
            case 5://任务中心
                Intent intentTask = new Intent(mContext, SignWebActivity.class);
                intentTask.putExtra("link", FinalConstant.TASK);
                startActivity(intentTask);
                break;
            case 6://我的问答
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "myask"));
                pageJumpManager.jumpToQuestionAnswerActivity(FinalConstant.QUESTION_ANSWER_URL, "我的问答");
                break;
            case 7://我的回复
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "myreply"));
                Intent it11 = new Intent();
                it11.setClass(mContext, MyPostsActivity.class);
                it11.putExtra("tid", 2);  //2代表从回复跳过去的
                startActivity(it11);
                break;
            case 8://领券中心
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "coupons_center"));
                String url6491 = FinalConstant1.BASE_HTML_URL + FinalConstant1.SYMBOL2 + FinalConstant.VER + "/coupons/center/flag/1/";
                WebUtil.getInstance().startWebActivity(mContext, url6491);
                break;
            case 9://安心保障
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "peace_of_mind"));
                Intent it13 = new Intent();
                it13.putExtra("type", "6");
                it13.setClass(mContext, InstructionWebActivity.class);
                startActivity(it13);
                break;
            case 10://颜值币商城
                WebUtil.getInstance().startWebActivity(mContext, FinalConstant.YANZHIBI);
                break;
            case 11://问医生
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ASK_ENTRY, "mine"));
                startActivity(new Intent(mContext, TypeProblemActivity.class));
                break;
            case 12://在线客服
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "zxkf"));
                Intent it = new Intent();
                it.setClass(mContext, OnlineKefuWebActivity.class);
                it.putExtra("link", "/service/zxzx/");
                it.putExtra("title", "在线客服");
                startActivity(it);
                break;
            case 13://电话咨询
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MINE_CLICK, "tel"));
                showDialog1();
//                startActivity(new Intent(mContext, CeshiCeshiActivity.class));
                break;
            case 14://客多多
                Intent intent = new Intent(mContext, PlusVipActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void showDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("拨打" + phone);
        builder.setPositiveButton("拨打", new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                ViewInject.toast("正在拨打中·····");
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {        //判断权限是否开启
                            phoneCall();
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            ViewInject.toast("没有电话权限");
                        }
                    });
                } else {
                    phoneCall();
                }
            }
        });
        builder.setNegativeButton("取消", new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        builder.create().show();
    }

    /**
     * 打电话
     */
    @SuppressLint("MissingPermission")
    private void phoneCall() {
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(mContext, "数据获取错误", Toast.LENGTH_SHORT).show();
            return;
        }
        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        int minute = t.minute;
        if (hour > 9 && hour < 22) {
            ViewInject.toast("正在拨打中·····");
            Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            try {
                startActivity(it);
            } catch (Exception e) {
                Log.e(TAG, "e === " + e.toString());
                e.printStackTrace();
            }
        } else if (hour == 9 && minute >= 30) {
            ViewInject.toast("正在拨打中·····");
            Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            try {
                startActivity(it);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (hour == 21 && minute <= 30) {
            ViewInject.toast("正在拨打中·····");
            Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            try {
                startActivity(it);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(mContext, "不在营业时间内", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.riv_user_head, R.id.tv_user_name, R.id.tv_my_modifieddata, R.id.rl_my_loginclick, R.id.newuser_redpacket, R.id.self_setting,
            R.id.ll_diary_click, R.id.ll_invitaon_click, R.id.ll_footmark_click, R.id.ll_attention_click, R.id.ll_fans_click,
            R.id.plus_content, R.id.puls_weikaikong, R.id.puls_kaitong, R.id.mu_order_click, R.id.my_staypay_click, R.id.my_stayconsume_click, R.id.my_evaluation_click, R.id.my_staywritediary_click, R.id.my_refund_click})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.riv_user_head:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it0 = new Intent();
                    it0.putExtra("type", "1");
                    it0.putExtra("flag", "1");
                    it0.putExtra("isphone", isphone);
                    it0.setClass(mContext, ModifyMyDataActivity.class);
                    startActivity(it0);
                }
                break;
            case R.id.tv_user_name:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it0 = new Intent();
                    it0.putExtra("type", "1");
                    it0.putExtra("flag", "1");
                    it0.putExtra("isphone", isphone);
                    it0.setClass(mContext, ModifyMyDataActivity.class);
                    startActivity(it0);

                }
                break;
            case R.id.tv_my_modifieddata:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it0 = new Intent();
                    it0.putExtra("type", "1");
                    it0.putExtra("flag", "1");
                    it0.putExtra("isphone", isphone);
                    it0.setClass(mContext, ModifyMyDataActivity.class);
                    startActivity(it0);

                }
                break;
            case R.id.rl_my_loginclick:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it0 = new Intent();
                    it0.putExtra("type", "1");
                    it0.putExtra("flag", "1");
                    it0.putExtra("isphone", isphone);
                    it0.setClass(mContext, ModifyMyDataActivity.class);
                    startActivity(it0);
                }
                break;
            case R.id.newuser_redpacket:
                WebUrlTypeUtil.getInstance(mContext).urlToApp("https://m.yuemei.com/tao_zt/6025.html", "0", "0");
                break;
            case R.id.self_setting:
                Intent it4 = new Intent();
                it4.setClass(mContext, SelfSettingActivity.class);
                startActivity(it4);
//                DialogUtils.showDiscountExpiredDialog(mContext, "测试测试测试测试测试测试测试测试",new DialogUtils.CallBack() {
//                    @Override
//                    public void onItemClick(int pos) {
//                        MyToast.makeTextToast2(mContext,pos +"",1000).show();
//                    }
//
//                    @Override
//                    public void onDismiss() {
//                        MyToast.makeTextToast2(mContext,"消失",1000).show();
//                    }
//
//                    @Override
//                    public void onSee() {
//                        MyToast.makeTextToast2(mContext,"立即查看",1000).show();
//                    }
//                });
                break;
            case R.id.ll_diary_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it0 = new Intent();
                    it0.putExtra("flag", "1");//1代表从我的日记跳过去的
                    it0.setClass(mContext, HomeDiarySXActivity.class);
                    startActivity(it0);
                }
                break;
            case R.id.ll_invitaon_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it1 = new Intent();
                    it1.setClass(mContext, MyPostsActivity.class);
                    it1.putExtra("tid", 1);  //1代表从帖子跳过去的
                    startActivity(it1);
                }
                break;
            case R.id.ll_footmark_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it1 = new Intent();
                    it1.putExtra("name", "我的足迹");
                    it1.putExtra("zid", "足迹");
                    it1.setClass(mContext, MyDaizhifuActivity.class);
                    startActivity(it1);
                }
                break;
            case R.id.ll_attention_click:
                if (Utils.isLoginAndBind(mContext)) {
                    startActivity(new Intent(mContext, MyFocusActivity.class));
                }
                break;
            case R.id.ll_fans_click:
                if (Utils.isLoginAndBind(mContext)) {
                    startActivity(new Intent(mContext, MyFansActivity.class));
                }
                break;
            case R.id.my_staypay_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it0 = new Intent();
                    it0.setClass(mContext, MyOrdersActivity.class);
                    it0.putExtra("item", 1);
                    startActivity(it0);
                }
                break;
            case R.id.my_stayconsume_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it1 = new Intent();
                    it1.setClass(mContext, MyOrdersActivity.class);
                    it1.putExtra("item", 2);
                    startActivity(it1);
                }
                break;
            case R.id.my_evaluation_click:
                if (Utils.isLoginAndBind(mContext)) {
                    WebData webData = new WebData(FinalConstant1.BASE_VER_URL + "/comment/commentlist/");
                    webData.setShowRefresh(false);
                    WebUtil.getInstance().startWebActivity(mContext, webData);
                }
                break;
            case R.id.my_staywritediary_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it2 = new Intent();
                    it2.setClass(mContext, MyOrdersActivity.class);
                    it2.putExtra("item", 3);
                    startActivity(it2);
                }
                break;
            case R.id.my_refund_click:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it3 = new Intent();
                    it3.setClass(mContext, MyOrdersActivity.class);
                    it3.putExtra("item", 4);
                    startActivity(it3);
                }
                break;
            case R.id.plus_content:
                if (Utils.isLoginAndBind(mContext)) {
                    if ("1".equals(mUser_more)) {
                        final YueMeiDialog2 yueMeiDialog2 = new YueMeiDialog2(mContext, "此活动不支持客多多用户开通", "确定");
                        yueMeiDialog2.setCanceledOnTouchOutside(false);
                        yueMeiDialog2.show();
                        yueMeiDialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                            @Override
                            public void BtnClick() {
                                yueMeiDialog2.dismiss();
                            }
                        });
                    } else {
                        Intent intent = new Intent(mContext, PlusVipActivity.class);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.puls_weikaikong:
                if (Utils.isLoginAndBind(mContext)) {
                    if ("1".equals(mUser_more)) {
                        final YueMeiDialog2 yueMeiDialog2 = new YueMeiDialog2(mContext, "此活动不支持客多多用户开通", "确定");
                        yueMeiDialog2.setCanceledOnTouchOutside(false);
                        yueMeiDialog2.show();
                        yueMeiDialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                            @Override
                            public void BtnClick() {
                                yueMeiDialog2.dismiss();
                            }
                        });
                    } else {
                        Intent intent = new Intent(mContext, PlusVipActivity.class);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.puls_kaitong:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent intent = new Intent(mContext, PlusVipActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.mu_order_click:// 我的订单
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it6 = new Intent(mContext, MyOrdersActivity.class);
                    startActivity(it6);
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog = new CustomDialog(this, R.style.mystyle, R.layout.customdialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        super.onDestroy();
    }
}
