package com.module.my.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 支付页面优惠劵详情
 * Created by 裴成浩 on 2018/3/21.
 */

public class YouHuiCoupons implements Parcelable {

    /**
     * card_id : 15721024
     * lowest_consumption : 12800
     * title : 双十一促销红包
     * money : 688
     * couponsType : 3
     * mankeyong : 满12800可用
     * shiyongtiaojian : 使用条件：限部分商品可用限北京卓艺整形美容诊所医院商品可用,
     * time : 有效期：2017-11-11至2020-11-30
     */

    private String card_id = "0";
    private String lowest_consumption = "0";
    private String title = "";
    private String money = "0";
    private String couponsType = "";
    private String mankeyong = "";
    private String shiyongtiaojian = "";
    private String time = "";


    public YouHuiCoupons() {
        super();
    }

    public YouHuiCoupons(Parcel in) {
        card_id = in.readString();
        lowest_consumption = in.readString();
        title = in.readString();
        money = in.readString();
        couponsType = in.readString();
        mankeyong = in.readString();
        shiyongtiaojian = in.readString();
        time = in.readString();
    }


    /**
     * 内容描述接口
     *
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 写入接口函数，打包
     *
     * @param parcel
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(card_id);
        parcel.writeString(lowest_consumption);
        parcel.writeString(title);
        parcel.writeString(money);
        parcel.writeString(couponsType);
        parcel.writeString(mankeyong);
        parcel.writeString(shiyongtiaojian);
        parcel.writeString(time);
    }

    public static final Parcelable.Creator<YouHuiCoupons> CREATOR = new Parcelable.Creator<YouHuiCoupons>() {
        public YouHuiCoupons createFromParcel(Parcel in) {
            return new YouHuiCoupons(in);
        }

        public YouHuiCoupons[] newArray(int size) {
            return new YouHuiCoupons[size];
        }
    };

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public float getLowest_consumption() {
        return Float.parseFloat(lowest_consumption);
    }

    public void setLowest_consumption(String lowest_consumption) {
        this.lowest_consumption = lowest_consumption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getMoney() {
        return  Float.parseFloat(money);
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCouponsType() {
        return couponsType;
    }

    public void setCouponsType(String couponsType) {
        this.couponsType = couponsType;
    }

    public String getMankeyong() {
        return mankeyong;
    }

    public void setMankeyong(String mankeyong) {
        this.mankeyong = mankeyong;
    }

    public String getShiyongtiaojian() {
        return shiyongtiaojian;
    }

    public void setShiyongtiaojian(String shiyongtiaojian) {
        this.shiyongtiaojian = shiyongtiaojian;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    @Override
    public String toString() {
        return "card_id == " + card_id +
                "，lowest_consumption +== " + lowest_consumption +
                "，title == " + title +
                "，money == " + money +
                "，couponsType == " + couponsType +
                "，mankeyong == " + mankeyong +
                "，shiyongtiaojian == " + shiyongtiaojian +
                "，time == " + time;
    }

}
