package com.module.my.model.bean;

public class SaoXiadan {

	private String code;
	private String message;
	private SaoXiadanData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SaoXiadanData getData() {
		return data;
	}

	public void setData(SaoXiadanData data) {
		this.data = data;
	}

}
