package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/2/26.
 */

public class PostTextQueApi implements BaseCallBackApi {
    private String TAG = "PostTextQueApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public PostTextQueApi() {
        mHashMap = new HashMap<>();
    }


    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.FORUM, "share", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.toString());
                listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
