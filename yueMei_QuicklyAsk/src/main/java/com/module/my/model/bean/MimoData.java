package com.module.my.model.bean;

import org.kymjs.aframe.database.annotate.Id;

/**
 * Created by dwb on 17/3/28.
 */

public class MimoData {

    @Id()
    private int id;
    private String oderid;
    private String mimofenqiPaytype;
    private int mimoNanNvpo;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOderid() {
        return oderid;
    }

    public void setOderid(String oderid) {
        this.oderid = oderid;
    }

    public String getMimofenqiPaytype() {
        return mimofenqiPaytype;
    }

    public void setMimofenqiPaytype(String mimofenqiPaytype) {
        this.mimofenqiPaytype = mimofenqiPaytype;
    }

    public int getMimoNanNvpo() {
        return mimoNanNvpo;
    }

    public void setMimoNanNvpo(int mimoNanNvpo) {
        this.mimoNanNvpo = mimoNanNvpo;
    }
}
