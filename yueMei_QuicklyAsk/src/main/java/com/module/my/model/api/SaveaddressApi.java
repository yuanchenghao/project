package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * 新增修改 保存收货地址
 * Created by 裴成浩 on 2018/9/6.
 */
public class SaveaddressApi implements BaseCallBackApi {
    private String TAG = "SaveaddressApi";
    private HashMap<String, Object> mSaveaddressHashMap;  //传值容器

    public SaveaddressApi() {
        mSaveaddressHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "saveaddress", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getSaveaddressHashMap() {
        return mSaveaddressHashMap;
    }

    public void addData(String key, String value) {
        mSaveaddressHashMap.put(key, value);
    }
}
