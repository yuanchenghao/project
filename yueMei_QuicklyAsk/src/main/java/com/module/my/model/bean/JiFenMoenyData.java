package com.module.my.model.bean;

public class JiFenMoenyData {

	private String discount_id;
	private String integral;
	private String priceintegral;

	public String getDiscount_id() {
		return discount_id;
	}

	public void setDiscount_id(String discount_id) {
		this.discount_id = discount_id;
	}

	public String getIntegral() {
		return integral;
	}

	public void setIntegral(String integral) {
		this.integral = integral;
	}

	public String getPriceintegral() {
		return priceintegral;
	}

	public void setPriceintegral(String priceintegral) {
		this.priceintegral = priceintegral;
	}

}
