package com.module.my.model.bean;

/**
 * Created by dwb on 16/9/21.
 */
public class ALiPay {

    private String code;
    private String message;
    private ALiPayData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ALiPayData getData() {
        return data;
    }

    public void setData(ALiPayData data) {
        this.data = data;
    }
}
