package com.module.my.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/31.
 */

public class InitializeHttpApi implements BaseCallBackApi {


    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
//        71661
        NetWork.getInstance().call(FinalConstant1.INTEGRALTASK, "signindata", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);
            }
        });
    }
}
