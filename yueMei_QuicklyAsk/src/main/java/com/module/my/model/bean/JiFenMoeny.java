package com.module.my.model.bean;

public class JiFenMoeny {
	private String code;
	private String message;
	private JiFenMoenyData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JiFenMoenyData getData() {
		return data;
	}

	public void setData(JiFenMoenyData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "JiFenMoeny [code=" + code + ", message=" + message + ", data="
				+ data + "]";
	}

}
