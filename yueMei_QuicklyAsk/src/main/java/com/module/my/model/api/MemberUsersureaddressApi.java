package com.module.my.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2018/9/17.
 */
public class MemberUsersureaddressApi implements BaseCallBackApi {
    private String TAG = "MemberUsersureaddressApi";
    private HashMap<String, Object> mUsersureaddresstHashMap;  //传值容器

    public MemberUsersureaddressApi() {
        mUsersureaddresstHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.MEMBER, "usersureaddress", maps,new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);
            }
        });
    }
    public HashMap<String, Object> getUsersureaddresstHashMap() {
        return mUsersureaddresstHashMap;
    }

    public void addData(String key, String value) {
        mUsersureaddresstHashMap.put(key, value);
    }
}
