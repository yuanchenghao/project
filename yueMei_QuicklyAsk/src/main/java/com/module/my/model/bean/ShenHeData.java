/**
 * 
 */
package com.module.my.model.bean;

/**
 * 审核判断
 * 
 * @author Rubin
 * 
 */
public class ShenHeData {


	private String shenhe;

	private String share;
	private String alipay;
	private String hb_alipay;
	private String wxpay;
	private String bankpay;

	private String board;
	private String sex;
	private String surname;
	private String face_type;
	private String phone;

	public String getShenhe() {
		return shenhe;
	}

	public void setShenhe(String shenhe) {
		this.shenhe = shenhe;
	}

	public String getShare() {
		return share;
	}

	public void setShare(String share) {
		this.share = share;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getHb_alipay() {
		return hb_alipay;
	}

	public void setHb_alipay(String hb_alipay) {
		this.hb_alipay = hb_alipay;
	}

	public String getWxpay() {
		return wxpay;
	}

	public void setWxpay(String wxpay) {
		this.wxpay = wxpay;
	}

	public String getBankpay() {
		return bankpay;
	}

	public void setBankpay(String bankpay) {
		this.bankpay = bankpay;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFace_type() {
		return face_type;
	}

	public void setFace_type(String face_type) {
		this.face_type = face_type;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
