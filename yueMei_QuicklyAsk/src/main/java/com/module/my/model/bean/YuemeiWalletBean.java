package com.module.my.model.bean;

public class YuemeiWalletBean {


    /**
     * balance : 0.00
     * on_extract_balance : 0.00
     * desc :
     * proceeds_account :
     * month_max_money : 800
     * day_max_limit : 3
     * withdrawNum : 0
     * withdrawMonthNum : 0
     * is_real : 0
     * month_money : 800
     * poundage : 0
     * real_name :
     */

    private String balance;
    private String on_extract_balance;
    private String desc;
    private String proceeds_account;
    private int month_max_money;
    private int day_max_limit;
    private int withdrawNum;
    private float withdrawMonthNum;
    private String is_real;
    private float month_money;
    private String poundage;
    private String real_name;
    private int every_min_money;
    private int account_type;
    private String un_extract_balance;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getOn_extract_balance() {
        return on_extract_balance;
    }

    public void setOn_extract_balance(String on_extract_balance) {
        this.on_extract_balance = on_extract_balance;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProceeds_account() {
        return proceeds_account;
    }

    public void setProceeds_account(String proceeds_account) {
        this.proceeds_account = proceeds_account;
    }

    public int getMonth_max_money() {
        return month_max_money;
    }

    public void setMonth_max_money(int month_max_money) {
        this.month_max_money = month_max_money;
    }

    public int getDay_max_limit() {
        return day_max_limit;
    }

    public void setDay_max_limit(int day_max_limit) {
        this.day_max_limit = day_max_limit;
    }

    public int getWithdrawNum() {
        return withdrawNum;
    }

    public void setWithdrawNum(int withdrawNum) {
        this.withdrawNum = withdrawNum;
    }

    public float getWithdrawMonthNum() {
        return withdrawMonthNum;
    }

    public void setWithdrawMonthNum(float withdrawMonthNum) {
        this.withdrawMonthNum = withdrawMonthNum;
    }

    public String getIs_real() {
        return is_real;
    }

    public void setIs_real(String is_real) {
        this.is_real = is_real;
    }

    public float getMonth_money() {
        return month_money;
    }

    public void setMonth_money(float month_money) {
        this.month_money = month_money;
    }

    public String getPoundage() {
        return poundage;
    }

    public void setPoundage(String poundage) {
        this.poundage = poundage;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public int getEvery_min_money() {
        return every_min_money;
    }

    public void setEvery_min_money(int every_min_money) {
        this.every_min_money = every_min_money;
    }

    public int getAccount_type() {
        return account_type;
    }

    public void setAccount_type(int account_type) {
        this.account_type = account_type;
    }

    public String getUn_extract_balance() {
        return un_extract_balance;
    }

    public void setUn_extract_balance(String un_extract_balance) {
        this.un_extract_balance = un_extract_balance;
    }
}
