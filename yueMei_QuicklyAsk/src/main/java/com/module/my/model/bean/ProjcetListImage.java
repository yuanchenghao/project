package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2019/5/23
 */
public class ProjcetListImage {
    private String before;
    private String after;

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }
}
