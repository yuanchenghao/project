package com.module.my.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * 我的粉丝数据
 * Created by 裴成浩 on 2018/4/19.
 */

public class MyFansData implements Parcelable {

    private String img;
    private String name;
    private String desc;
    private String v;           //0没有v,1红色v,2蓝色v
    private String obj_id;
    private String obj_type;
    private String url;
    private HashMap<String,String> event_params;
    private String each_following;

    protected MyFansData(Parcel in) {
        img = in.readString();
        name = in.readString();
        desc = in.readString();
        v = in.readString();
        obj_id = in.readString();
        obj_type = in.readString();
        url = in.readString();
        each_following = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeString(v);
        dest.writeString(obj_id);
        dest.writeString(obj_type);
        dest.writeString(url);
        dest.writeString(each_following);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyFansData> CREATOR = new Creator<MyFansData>() {
        @Override
        public MyFansData createFromParcel(Parcel in) {
            return new MyFansData(in);
        }

        @Override
        public MyFansData[] newArray(int size) {
            return new MyFansData[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getEach_following() {
        return each_following;
    }

    public void setEach_following(String each_following) {
        this.each_following = each_following;
    }
}
