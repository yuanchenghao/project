package com.module.my.model.bean;

import java.util.List;

public class SaoOrderList {

	private String code;
	private String message;
	private List<SaoOrderListData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<SaoOrderListData> getData() {
		return data;
	}

	public void setData(List<SaoOrderListData> data) {
		this.data = data;
	}

}
