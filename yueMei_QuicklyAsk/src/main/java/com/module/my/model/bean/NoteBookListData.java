package com.module.my.model.bean;

/**
 * Created by dwb on 16/3/21.
 */
public class NoteBookListData {

    private String img;
    private String is_new;
    private String t_id;
    private String _id;
    private String server_id;
    private String price;
    private String title;
    private String doc_id;
    private String hos_id;
    private String docname;
    private String hosname;
    private String sharetime;
    private String is_fanxian;
    private String page;
    private String subtitle;
    private String before_max_day;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSharetime() {
        return sharetime;
    }

    public void setSharetime(String sharetime) {
        this.sharetime = sharetime;
    }

    public String getIs_new() {
        return is_new;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getServer_id() {
        return server_id;
    }

    public void setServer_id(String server_id) {
        this.server_id = server_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getHosname() {
        return hosname;
    }

    public void setHosname(String hosname) {
        this.hosname = hosname;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getIs_fanxian() {
        return is_fanxian;
    }

    public void setIs_fanxian(String is_fanxian) {
        this.is_fanxian = is_fanxian;
    }

    public String getBefore_max_day() {
        return before_max_day;
    }

    public void setBefore_max_day(String before_max_day) {
        this.before_max_day = before_max_day;
    }
}
