package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/9/20.
 */
public class ForumTextData {

    public ForumTextData(String text) {
        this.text = text;
    }

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
