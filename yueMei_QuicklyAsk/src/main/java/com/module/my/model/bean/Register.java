package com.module.my.model.bean;

/**
 * 注册返回数据
 * 
 * @author Rubin
 * 
 */
public class Register {
	private String code;
	private String message;
	private RegisterData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public RegisterData getData() {
		return data;
	}

	public void setData(RegisterData data) {
		this.data = data;
	}


}
