package com.module.my.model.bean;

/**
 * Created by dwb on 17/3/9.
 */

public class HbFenqi {

    private String code;
    private String message;
    private HBMMFenqi data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HBMMFenqi getData() {
        return data;
    }

    public void setData(HBMMFenqi data) {
        this.data = data;
    }
}
