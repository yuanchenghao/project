package com.module.my.model.api;

import android.content.Context;

import com.lzy.okgo.model.HttpParams;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseCallBackUploadApi;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * 保存上传文件的方法
 * Created by 裴成浩 on 2018/3/27.
 */

public class ModifyMyDataApi implements BaseCallBackUploadApi {
    private String TAG = "ModifyMyDataApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, HttpParams uploadPamas, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.USERNEW, "setuser", maps, uploadPamas,new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);

            }
        });
    }
}
