package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteImage {

    private String imgPath;             //图片本地路径
    private boolean isCover = false;    //是否是封面，默认不是
    private PostingUploadImage imgUrlData;   //网络存储图片数据

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public boolean isCover() {
        return isCover;
    }

    public void setCover(boolean cover) {
        isCover = cover;
    }


    public PostingUploadImage getImgUrlData() {
        return imgUrlData;
    }

    public void setImgUrlData(PostingUploadImage imgUrlData) {
        this.imgUrlData = imgUrlData;
    }
}
