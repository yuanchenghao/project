package com.module.my.model.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.bean.DocListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * 收藏的医生
 * Created by 裴成浩 on 2018/3/13.
 */

public class CollectDocApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "mycollectdoc", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if("1".equals(mData.code)){
                    List<DocListData> docData = JSONUtil.jsonToArrayList(mData.data,DocListData.class);
                    listener.onSuccess(docData);
                }
            }
        });
    }
}
