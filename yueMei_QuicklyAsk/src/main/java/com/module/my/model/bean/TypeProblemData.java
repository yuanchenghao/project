package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2019/3/18
 */
public class TypeProblemData {
    private String title;
    private String desc;
    private String tag_id;
    private String hos_userid;
    private String is_rongyun;
    private String event_name;
    private String event_pos;
    private String event_others;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(String event_pos) {
        this.event_pos = event_pos;
    }

    public String getEvent_others() {
        return event_others;
    }

    public void setEvent_others(String event_others) {
        this.event_others = event_others;
    }
}
