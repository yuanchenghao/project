package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.YouHuiData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * 预订信息页面优惠券接口
 * Created by Administrator on 2017/11/2.
 */

public class InitYouHuiApi implements BaseCallBackApi {
    private String TAG = "InitDaijinjuanApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.COUPNOS, "usecoupons", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " + mData.toString());
                if ("1".equals(mData.code)) {
                    try {
                        YouHuiData youHuiData = JSONUtil.TransformSingleBean(mData.data, YouHuiData.class);
                        listener.onSuccess(youHuiData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
