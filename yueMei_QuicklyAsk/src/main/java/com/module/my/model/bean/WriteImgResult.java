package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2017/6/14.
 */

public class WriteImgResult {

    /**
     * code : 1
     * message : 成功
     * data : {"img":"/export/home/www/yuemei.com/wwwroot/upload/forum/image/20170614/170614103849_942ba6.jpg"}
     */

    private String code;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * img : /export/home/www/yuemei.com/wwwroot/upload/forum/image/20170614/170614103849_942ba6.jpg
         */

        private String img;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }
}

