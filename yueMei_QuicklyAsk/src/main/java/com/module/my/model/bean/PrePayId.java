/**
 * 
 */
package com.module.my.model.bean;

import com.quicklyask.entity.PrePayIdData;

/**
 * @author lenovo17
 * 
 */
public class PrePayId {
	private String code;
	private String message;
	private PrePayIdData data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public PrePayIdData getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(PrePayIdData data) {
		this.data = data;
	}

}
