package com.module.my.model.bean;

import java.util.List;

/**
 * Created by dwb on 17/3/27.
 */

public class HBMMFenqi {

    private List<HbFenqiItem> hb;
    private MoMoNanNv momo;


    public List<HbFenqiItem> getHb() {
        return hb;
    }

    public void setHb(List<HbFenqiItem> hb) {
        this.hb = hb;
    }

    public MoMoNanNv getMomo() {
        return momo;
    }

    public void setMomo(MoMoNanNv momo) {
        this.momo = momo;
    }
}
