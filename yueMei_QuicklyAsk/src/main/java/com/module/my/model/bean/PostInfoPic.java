package com.module.my.model.bean;

/**
 * Created by dwb on 16/5/5.
 */
public class PostInfoPic {

    private String pic_id;
    private String weight;
    private String img;

    public String getPic_id() {
        return pic_id;
    }

    public void setPic_id(String pic_id) {
        this.pic_id = pic_id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
