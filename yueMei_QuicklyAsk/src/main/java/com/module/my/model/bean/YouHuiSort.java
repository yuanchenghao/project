package com.module.my.model.bean;

import android.util.Log;

import java.util.Comparator;

/**
 * 优惠券排序
 * Created by 裴成浩 on 2018/3/22.
 */

public class YouHuiSort implements Comparator<YouHuiCoupons> {
    private String TAG = "YouHuiSort";

    @Override
    public int compare(YouHuiCoupons youHuiData, YouHuiCoupons t1) {
//        int flag = (youHuiData.getMoney() + "").compareTo(t1.getMoney() + "");
//        if (flag == 0) {
//            return youHuiData.getLowest_consumption().compareTo(t1.getLowest_consumption());
//        } else {
//            return flag;
//        }
        Log.e(TAG, "1111 === " + youHuiData.getMoney());
        Log.e(TAG, "2222 === " + t1.getMoney());
        return (((int)youHuiData.getMoney()) + "").compareTo(((int)t1.getMoney()) + "");
    }
}