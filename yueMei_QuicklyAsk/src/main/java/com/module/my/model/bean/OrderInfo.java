package com.module.my.model.bean;

import com.quicklyask.entity.OrderInfoData;

public class OrderInfo {
	
	private String code;
	private String message;
	private OrderInfoData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OrderInfoData getData() {
		return data;
	}

	public void setData(OrderInfoData data) {
		this.data = data;
	}

}
