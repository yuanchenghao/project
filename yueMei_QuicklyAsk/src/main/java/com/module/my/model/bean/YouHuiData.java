package com.module.my.model.bean;

import java.util.List;

/**
 * 支付页面优惠劵列表
 * Created by 裴成浩 on 2018/3/21.
 */

public class YouHuiData{

    private String couponsnum;
    private List<YouHuiCoupons> coupons;

    public String getCouponsnum() {

        return couponsnum;
    }

    public void setCouponsnum(String couponsnum) {
        this.couponsnum = couponsnum;
    }

    public List<YouHuiCoupons> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<YouHuiCoupons> coupons) {
        this.coupons = coupons;
    }

}
