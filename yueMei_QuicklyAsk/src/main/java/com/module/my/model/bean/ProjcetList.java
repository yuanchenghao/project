package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2019/5/23
 */
public class ProjcetList {
    private String id;
    private String title;
    private String postName;
    private String postVal;
    private ProjcetListImage image;
    private boolean is_selected = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostVal() {
        return postVal;
    }

    public void setPostVal(String postVal) {
        this.postVal = postVal;
    }

    public ProjcetListImage getImage() {
        return image;
    }

    public void setImage(ProjcetListImage image) {
        this.image = image;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
