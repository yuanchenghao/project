package com.module.my.model.bean;

/**
 * Created by dwb on 16/3/9.
 */
public class QuanziTitleData {

    private String id;
    private String title;
    private String img;
    private String desc;
    private String share_wximg;
    private String share_wbimg;
    private String share_title;
    private String share_content;
    private String share_url;
    private String top_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getShare_wximg() {
        return share_wximg;
    }

    public void setShare_wximg(String share_wximg) {
        this.share_wximg = share_wximg;
    }

    public String getShare_wbimg() {
        return share_wbimg;
    }

    public void setShare_wbimg(String share_wbimg) {
        this.share_wbimg = share_wbimg;
    }

    public String getShare_title() {
        return share_title;
    }

    public void setShare_title(String share_title) {
        this.share_title = share_title;
    }

    public String getShare_content() {
        return share_content;
    }

    public void setShare_content(String share_content) {
        this.share_content = share_content;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getTop_id() {
        return top_id;
    }

    public void setTop_id(String top_id) {
        this.top_id = top_id;
    }
}
