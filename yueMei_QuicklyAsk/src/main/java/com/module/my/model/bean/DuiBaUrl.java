package com.module.my.model.bean;

import com.quicklyask.entity.DataUrl;

/**
 * Created by dwb on 16/10/19.
 */
public class DuiBaUrl {

    private String code;
    private String message;
    private DataUrl data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataUrl getData() {
        return data;
    }

    public void setData(DataUrl data) {
        this.data = data;
    }
}
