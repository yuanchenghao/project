package com.module.my.model.bean;

import android.net.Uri;

import com.quicklyask.entity.WriteVideoResult;

/**
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteVideo {
    private String videoPath;             //视频本地路径
    private long videoDuration;           //视频时间
    private Uri videoThumbnail;           //视频缩略图
    private PostingVideoCover videoCover;            //视频封面
    private WriteVideoResult writeVideoResult;  //上传又拍云后返回的数据

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public Uri getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(Uri videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }


    public long getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(long videoDuration) {
        this.videoDuration = videoDuration;
    }

    public PostingVideoCover getVideoCover() {
        return videoCover;
    }

    public void setVideoCover(PostingVideoCover videoCover) {
        this.videoCover = videoCover;
    }

    public WriteVideoResult getWriteVideoResult() {
        return writeVideoResult;
    }

    public void setWriteVideoResult(WriteVideoResult writeVideoResult) {
        this.writeVideoResult = writeVideoResult;
    }
}
