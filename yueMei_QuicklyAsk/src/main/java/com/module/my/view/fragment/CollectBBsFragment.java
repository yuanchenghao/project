/**
 *
 */
package com.module.my.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.CollectBBsApi;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收藏的帖子
 *
 * @author Robin
 */
public class CollectBBsFragment extends Fragment {

    private final String TAG = "CollectBBsFragment";

    private Activity mContext;

    // List
    private LoadMoreListView mlist;
    private int mCurPage = 1;
    private List<BBsListData550> lvBBslistData = new ArrayList<>();
    private BBsListAdapter bbsListAdapter;

    private LinearLayout nodataTv;
    private String uid;
    private CollectBBsApi collectBBsApi;
    private HashMap<String, Object> collectBBsMap = new HashMap<>();
    private LoadingProgress mDialog;
    private boolean isNoMore = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_collect_san_550, container, false);
        nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata);
        mlist = v.findViewById(R.id.collect_san_list);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
        if (isAdded()) {
            uid = Utils.getUid();
        }

        collectBBsApi = new CollectBBsApi();
        mDialog = new LoadingProgress(mContext);
        initList();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
        StatService.onResume(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();

        uid = Utils.getUid();
    }

    void initList() {
        mDialog.startLoading();
        lodBBsListData550();
        mlist.setLoadMoreListener(new com.module.base.refresh.loadmore.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isNoMore) {
                    lodBBsListData550();
                } else {
                    mlist.loadMoreEnd();
                }

            }
        });

        mlist.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                DeleteDialog(pos);
                return true;
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {
                if(Utils.isFastDoubleClick()){
                    return;
                }
                if (pos != bbsListAdapter.getmHotIssues().size()) {
                    List<BBsListData550> bBsListData550s = bbsListAdapter.getmHotIssues();
                    String url = bBsListData550s.get(pos).getAppmurl();
                    if (url.length() > 0) {

                        if ("404".equals(bbsListAdapter.getmHotIssues().get(pos).getAskorshare())) {

                            ViewInject.toast("该帖子已被删除");
                        } else {
                            String qid = bbsListAdapter.getmHotIssues().get(pos).getQ_id();
                            WebUrlTypeUtil.getInstance(getActivity()).urlToApp(url, "0", "0");
                        }

                    }
                }
            }
        });
    }

    void lodBBsListData550() {
        collectBBsMap.put("type", "2");
        collectBBsMap.put("id", uid);
        collectBBsMap.put("page", mCurPage + "");
        collectBBsApi.getCallBack(mContext, collectBBsMap, new BaseCallBackListener<List<BBsListData550>>() {
            @Override
            public void onSuccess(List<BBsListData550> bBsListData550s) {
                mCurPage++;
                lvBBslistData = bBsListData550s;

                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDialog.stopLoading();

                        if (bbsListAdapter == null) {
                            bbsListAdapter = new BBsListAdapter(getActivity(), lvBBslistData);
                            mlist.setAdapter(bbsListAdapter);
                        } else {
                            bbsListAdapter.add(lvBBslistData);
                            bbsListAdapter.notifyDataSetChanged();
                        }

                        Log.e(TAG, "lvBBslistData.size() === " + lvBBslistData.size());
                        if (lvBBslistData.size() < 20) {
                            isNoMore = true;
                        }

                        if (isNoMore) {
                            mlist.loadMoreEnd();
                        } else {
                            mlist.loadMoreComplete();
                        }

                    }
                });
            }
        });
    }

    /**
     * 长按item删除对话框
     *
     * @param pos
     */
    private void DeleteDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("      确定取消对帖子的收藏？");

        // 确定按钮监听
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String userid = lvBBslistData.get(pos).getQ_id();
                deleteCollect(userid);

                lvBBslistData.remove(pos);
                if (lvBBslistData == null || lvBBslistData.equals("")) {
                    nodataTv.setVisibility(View.VISIBLE);
                }

                if (lvBBslistData.size() == 1 && pos == 0) {
                    nodataTv.setVisibility(View.VISIBLE);
                    mlist.setVisibility(View.GONE);
                }
                bbsListAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
        builder.create().setCanceledOnTouchOutside(false);
    }

    /**
     * 取消对专家的收藏
     *
     * @param userid
     */
    void deleteCollect(String userid) {
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", userid);
        params.put("type", "2");
        cancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {

            }
        });
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
        StatService.onPause(getActivity());
    }
}
