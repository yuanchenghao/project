package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.module.api.AmountRefundApi;
import com.module.commonview.module.bean.AmountRefundBean;
import com.module.my.controller.adapter.ApplyMoneyBackAdapter2;
import com.module.my.model.api.ApplyMoneyBackApi;
import com.module.my.model.api.InitOrderInfoApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.OrderInfoData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 申请退款
 *
 * @author Rubin
 */
public class ApplyMoneyBackActivity extends YMBaseActivity {

    private final String TAG = "ApplyMoneyBackActivity";
    @BindView(R.id.order_phone_test_back)
    RelativeLayout mOrderPhoneTestBack;
    @BindView(R.id.backmoney_title_tv)
    TextView mBackmoneyTitleTv;
    @BindView(R.id.self_iv_tiezi)
    ImageView mSelfIvTiezi;
    @BindView(R.id.order_time_date_tv)
    TextView mOrderTimeDateTv;
    @BindView(R.id.moneyback_price)
    public TextView mMoneybackPrice;
    @BindView(R.id.oder_money_back_des_rly)
    RelativeLayout mOderMoneyBackDesRly;
    @BindView(R.id.moneyback_list)
    RecyclerView mMoneybackList;
    @BindView(R.id.cancel_reson_check1)
    CheckBox mCancelResonCheck1;
    @BindView(R.id.cancel_reson_check2)
    CheckBox mCancelResonCheck2;
    @BindView(R.id.order_time_all_ly)
    LinearLayout mOrderTimeAllLy;
    @BindView(R.id.sildingFinishLayout)
    RelativeLayout mSildingFinishLayout;
    @BindView(R.id.money_back_person_click)
    RelativeLayout mMoneyBackPersonClick;
    @BindView(R.id.money_back_hos_click)
    RelativeLayout mMoneyBackHosClick;
    @BindView(R.id.money_back_btn)
    Button mMoneyBackBtn;
    @BindView(R.id.person_txt)
    TextView mPersonTxt;
    @BindView(R.id.hos_txt)
    TextView mHosTxt;


    private ApplyMoneyBackActivity mContext;
    private String uid;
    private String order_id="";
    private String paytype;
    private String repayment_type = "0";
    private SparseArray<String> mData=new SparseArray<>();
    private List<String> mGoodsId=new ArrayList<>();
    private String mRefundReason="";
    private String mGoodsId1;
    private ApplyMoneyBackAdapter2 mApplyMoneyBackAdapter;
    private ArrayList<Integer> mArr;
    private int mPostion=-1;
    private String mMessage;
    private ArrayList<OrderInfoData> mOrderInfoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = ApplyMoneyBackActivity.this;
        uid = Utils.getUid();

    }

    @Override
    protected int getLayoutId() {
        return R.layout.acty_apply_moneyback;
    }

    @Override
    protected void initView() {

    }
    Object obj;
    @Override
    protected void initData() {

        Intent it = getIntent();
        order_id = it.getStringExtra("order_id");
        CookieConfig.getInstance().setCookie("https", "sjapp.yuemei.com","sjapp.yuemei.com");
        final Map<String, Object> maps = new HashMap<>();
        maps.put("order_id", order_id);
        new InitOrderInfoApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

                if (serverData.code.equals("1")) {
                    mOrderInfoData = JSONUtil.jsonToArrayList(serverData.data, OrderInfoData.class);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                    mApplyMoneyBackAdapter = new ApplyMoneyBackAdapter2(mOrderInfoData,mContext);
                    mMoneybackList.setLayoutManager(linearLayoutManager);
                    mMoneybackList.setNestedScrollingEnabled(false);
                    mMoneybackList.setFocusableInTouchMode(false);
                    mMoneybackList.setAdapter(mApplyMoneyBackAdapter);
                    mApplyMoneyBackAdapter.setRecyclerViewOnItemClickListener(new ApplyMoneyBackAdapter2.RecyclerViewOnItemClickListener() {
                        @Override
                        public void onItemClickListener(View view, int position) {
                            if ("0".equals(mOrderInfoData.get(position).getIs_refund())){
                                mFunctionManager.showShort("该服务码不支持退款");
                                return;
                            }
                            mApplyMoneyBackAdapter.setSelectItem(position);
                            mFunctionManager.showShort("退款金额计算中...");
                            //获取你选中的item
                            mGoodsId.clear();
                            Map<Integer, Boolean> map = mApplyMoneyBackAdapter.getMap();
                            for (int i = 0; i < map.size(); i++) {
                                if (map.get(i)) {
                                    mGoodsId.add(mOrderInfoData.get(i).getGoods_id());
                                }
                            }
                            HashMap<String,Object>maps=new HashMap<>();
                            mGoodsId1 = StringUtils.strip(mGoodsId.toString(), "[]");
                            if (TextUtils.isEmpty(mGoodsId1)){
                                mMoneybackPrice.setText("0元");
                                return;
                            }
                            maps.put("goodsIDs", mGoodsId1.trim());
                            Log.e(TAG,"mGoodsId"+ mGoodsId1);
                            new AmountRefundApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                                @Override
                                public void onSuccess(ServerData serverData1) {
                                    if ("1".equals(serverData1.code)){
                                        try {
                                            AmountRefundBean amountRefundBean = JSONUtil.TransformSingleBean(serverData1.data, AmountRefundBean.class);
                                            float payTotalFee = Float.parseFloat(amountRefundBean.getPay_total_fee());
                                            float extract_balance_pay_money = Float.parseFloat(amountRefundBean.getExtract_balance_pay_money());
                                            float unextract_balance_pay_money = Float.parseFloat(amountRefundBean.getUnextract_balance_pay_money());
                                            float price=payTotalFee+extract_balance_pay_money+unextract_balance_pay_money;
                                            Log.e(TAG,"pric---"+price);
                                            mMoneybackPrice.setText(price+"元");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }else {
                                        mFunctionManager.showShort(serverData1.message);
                                    }
                                }
                            });
                        }
                    });
                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }
        });
    }


    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }


    @Override
    public void onResume() {
        super.onResume();
        mMoneyBackBtn.setClickable(true);
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


    @OnClick({R.id.order_phone_test_back, R.id.oder_money_back_des_rly, R.id.cancel_reson_check1, R.id.cancel_reson_check2,R.id.money_back_person_click, R.id.money_back_hos_click,R.id.money_back_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.order_phone_test_back:
                    finish();
                break;
            case R.id.oder_money_back_des_rly:
                Intent it = new Intent();
                it.setClass(mContext, MoneyBackDescription.class);
                startActivity(it);
                break;
            case R.id.cancel_reson_check1:
                if (mCancelResonCheck1.isChecked()){
                    mCancelResonCheck1.setChecked(false);
                }else {
                    mCancelResonCheck1.setChecked(true);
                    mCancelResonCheck2.setChecked(false);
                    Intent intent = new Intent(this,ReasonActivity.class);
                    intent.putExtra("type","1");
                    if (mPostion != -1){
                        intent.putExtra("position",mPostion);
                    }
                    if (!TextUtils.isEmpty(mRefundReason)){
                        intent.putExtra("message",mRefundReason);
                    }
                    startActivityForResult(intent,99);
                }
                break;
            case R.id.cancel_reson_check2:
                if (mCancelResonCheck2.isChecked()){
                    mCancelResonCheck2.setChecked(false);
                }else {
                    mCancelResonCheck2.setChecked(true);
                    mCancelResonCheck1.setChecked(false);
                    Intent intent = new Intent(this,ReasonActivity.class);
                    intent.putExtra("type","2");
                    if (mPostion != -1){
                        intent.putExtra("position",mPostion);
                    }
                    if (!TextUtils.isEmpty(mRefundReason)){
                        intent.putExtra("message",mRefundReason);
                    }
                    startActivityForResult(intent,100);
                }
                break;
            case R.id.money_back_person_click:
                if (mCancelResonCheck1.isChecked()){
                    mCancelResonCheck1.setChecked(false);
                }else {
                    mCancelResonCheck1.setChecked(true);
                    mCancelResonCheck2.setChecked(false);

                    Intent intent = new Intent(this,ReasonActivity.class);
                    intent.putExtra("type","1");
                    if (mPostion != -1){
                        intent.putExtra("position",mPostion);
                    }
                    if (!TextUtils.isEmpty(mRefundReason)){
                        intent.putExtra("message",mRefundReason);
                    }
                    startActivityForResult(intent,99);
                }
                break;
            case R.id.money_back_hos_click:
                if (mCancelResonCheck2.isChecked()){
                    mCancelResonCheck2.setChecked(false);
                }else {
                    mCancelResonCheck2.setChecked(true);
                    mCancelResonCheck1.setChecked(false);
                    Intent intent = new Intent(this,ReasonActivity.class);
                    intent.putExtra("type","2");
                    if (mPostion != -1){
                        intent.putExtra("position",mPostion);
                    }
                    if (!TextUtils.isEmpty(mRefundReason)){
                        intent.putExtra("message",mRefundReason);
                    }
                    startActivityForResult(intent,100);
                }
                break;
            case R.id.money_back_btn:
                if (Utils.isFastDoubleClick()){
                    return;
                }
                mGoodsId.clear();
                Map<Integer, Boolean> map = mApplyMoneyBackAdapter.getMap();
                for (int i = 0; i < map.size(); i++) {
                    if (map.get(i)) {
                        mGoodsId.add(mOrderInfoData.get(i).getGoods_id());
                    }
                }
                mGoodsId1 = StringUtils.strip(mGoodsId.toString(), "[]");
                Log.e(TAG,"mGoodsId1======"+mGoodsId1);
                if (TextUtils.isEmpty(mGoodsId1)){
                    mFunctionManager.showShort("请选择要退款的项目");
                    return;
                }
                if (TextUtils.isEmpty(mRefundReason)){
                    mFunctionManager.showShort("请选择退款原因");
                    return;
                }
                applyMoney();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == 55){
            mPostion = data.getIntExtra("position", -1);
            if (requestCode == 99){ //个人
                mRefundReason = data.getStringExtra("str");
                if (TextUtils.isEmpty(mRefundReason)){

                }
                mPersonTxt.setText("个人方面原因("+mRefundReason+")");
                mHosTxt.setText("医院方面原因");
                Log.e(TAG,"str-g---"+mRefundReason);
            }else if (requestCode == 100){//医院
                mRefundReason= data.getStringExtra("str");
                mHosTxt.setText("医院方面原因("+mRefundReason+")");
                mPersonTxt.setText("个人方面原因");
                Log.e(TAG,"hstr----"+mRefundReason);
            }
        }else {
            mCancelResonCheck1.setChecked(false);
            mCancelResonCheck2.setChecked(false);
            mPersonTxt.setText("个人方面原因");
            mHosTxt.setText("医院方面原因");
        }
    }

    private void applyMoney(){
        HashMap<String,Object>maps=new HashMap<>();
        maps.put("goodsIDs",mGoodsId1.trim());
        maps.put("refund_reason",mRefundReason);
        new ApplyMoneyBackApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    mMoneyBackBtn.setClickable(false);
                    Intent it = new Intent();
                    try {
                        JSONObject jsonObject = new JSONObject(serverData.data);
                        String refund_id = jsonObject.getString("refund_id");
                        it.putExtra("refund_id", refund_id);
                        it.setClass(mContext, MoneyBackSuccessActivity.class);
                        startActivity(it);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    MyToast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
