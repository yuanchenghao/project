package com.module.my.view;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.MyApplication;
import com.module.base.view.FunctionManager;
import com.module.commonview.module.bean.ChatBackSuccessBean;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.module.bean.RewardAlertData;
import com.quicklyask.activity.R;
import com.yy.mobile.rollingtextview.CharOrder;
import com.yy.mobile.rollingtextview.RollingTextView;
import com.yy.mobile.rollingtextview.strategy.Strategy;

import java.util.HashMap;

public class RewardAlertToast {

    public static void show(RewardAlertData rewardAlertData){
        Activity currentActivity = MyApplication.getInstance().getCurrentActivity();
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.sign_success_pop, null);
        TextView rewardTitle = view.findViewById(R.id.reward_title);
        TextView rewardUnit = view.findViewById(R.id.reward_unit);
        RollingTextView signCoin = view.findViewById(R.id.sign_coin);
        TextView rewardDesc = view.findViewById(R.id.reward_notice_desc);


        String motionType = rewardAlertData.getMotionType();
        String rewardTitle1 = rewardAlertData.getRewardTitle();
        String rewardUnit1 = rewardAlertData.getRewardUnit();
        String rewardMoney = rewardAlertData.getRewardMoney();
        String rewardNoticeDesc = rewardAlertData.getRewardNoticeDesc();


        rewardTitle.setText(rewardTitle1);
        rewardUnit.setText(rewardUnit1);
        if ("1".equals(motionType)){
            signCoin.setAnimationDuration(2000L);
            signCoin.addCharOrder(CharOrder.Number);
            signCoin.setCharStrategy(Strategy.CarryBitAnimation());
            signCoin.setText("0");
            signCoin.setText(rewardMoney);
        }else {
            signCoin.setText(rewardMoney);
        }
        if (!TextUtils.isEmpty(rewardNoticeDesc)){
            rewardDesc.setVisibility(View.VISIBLE);
            rewardDesc.setText(rewardNoticeDesc);
        }else {
            rewardDesc.setVisibility(View.GONE);
        }


        Toast toast = new Toast(currentActivity);
        //设置Toast要显示的位置，水平居中并在底部，X轴偏移0个单位，Y轴偏移70个单位，
        toast.setGravity(Gravity.CENTER,0,0);
        //设置显示时间
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }



    public static void showChatBackToast(ChatBackSuccessBean successBean,final String hos_id){
        final Activity currentActivity = MyApplication.getInstance().getCurrentActivity();
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.chat_backget_pop, null);
        ImageView background = view.findViewById(R.id.chat_back_bg);
        TextView title = view.findViewById(R.id.chat_back_title);
        TextView unit = view.findViewById(R.id.chat_back_unit);
        TextView desc = view.findViewById(R.id.chat_back_desc);
        TextView rollingTextView = view.findViewById(R.id.chat_back_coin);
        Button getBtn = view.findViewById(R.id.backget_btn);

//        Glide.with(currentActivity)
//                .load(successBean.getRewardImg())
//                .placeholder(R.drawable.chat_back_popbg)
//                .error(R.drawable.chat_back_popbg)
//                .into(background);
//        title.setText(successBean.getSuccessNoticeTitle());
//        unit.setText(successBean.getRewardType());
//        desc.setText(successBean.getSuccessNoticeDesc());


//        rollingTextView.setAnimationDuration(2000L);
//        rollingTextView.addCharOrder(CharOrder.Number);
//        rollingTextView.setCharStrategy(Strategy.CarryBitAnimation());
//        rollingTextView.setText("0");
        rollingTextView.setText("50");

        final Toast toast = new Toast(currentActivity);
        //设置Toast要显示的位置，水平居中并在底部，X轴偏移0个单位，Y轴偏移70个单位，
        toast.setGravity(Gravity.CENTER,0,0);
        //设置显示时间
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
        getBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast.cancel();
                new FunctionManager(currentActivity).showShort("奖励已经存入您的账户~");
                HashMap<String,String> parms = new HashMap<>();

                parms.put("id",hos_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_ALERT,"yes"),parms,new ActivityTypeData("151"));
            }
        });
    }
}
