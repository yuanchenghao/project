package com.module.my.view.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.ViewInject;

/**
 * 获取手机号 并验证
 * Created by 裴成浩 on 2018/3/19.
 */

public class PopupWindows extends PopupWindow {

    @SuppressWarnings("deprecation")
    public PopupWindows(final Context mContext, View parent) {

        final View view = View.inflate(mContext, R.layout.pop_yuyincode,
                null);

        view.startAnimation(AnimationUtils.loadAnimation(mContext,
                R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        // showAtLocation(parent, Gravity.BOTTOM, 0, 0);
        update();

        Button cancelBt = view.findViewById(R.id.cancel_bt);
        Button tureBt = view.findViewById(R.id.zixun_bt);
        final EditText codeEt = view.findViewById(R.id.no_pass_login_code_et);
        final ImageView codeIv = view.findViewById(R.id.yuyin_code_iv);
        RelativeLayout rshCodeRly = view
                .findViewById(R.id.no_pass_yanzheng_code_rly);

        Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);

        tureBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String codes = codeEt.getText().toString();
                if (codes.length() > 1) {
                    onTureClickListener.onTureClick(codes);
                } else {
                    ViewInject.toast("请输入图中数字");
                }
            }
        });

        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        rshCodeRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);

            }
        });
    }

    public interface OnTureClickListener {
        void onTureClick(String codes);
    }

    private OnTureClickListener onTureClickListener;

    public void setOnTureClickListener(OnTureClickListener onTureClickListener) {
        this.onTureClickListener = onTureClickListener;
    }
}
