package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.community.model.api.SubmitPayOrderApi;
import com.module.my.controller.adapter.SaoOrderListAdpter;
import com.module.my.model.api.CommitSecurityCodeInLoginApi;
import com.module.my.model.api.GetUserPhoneNumberApi;
import com.module.my.model.api.InitSaoOrderListApi;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.bean.SaoOrderListData;
import com.module.my.model.bean.SaoXiadanData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.UserPhoneData;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.NoScrollListView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 扫描后的订单信息
 * 
 * @author dwb
 * 
 */
public class SaoOrderMessageActivity extends BaseActivity {

	private final String TAG = "SaoOrderMessageActivity";
	private Context mContex;

	@BindView(id = R.id.sao_order_top)
	private CommonTopBar mTop;// 返回

	public static int postion_sao;
	// 预订服务的列表
	@BindView(id = R.id.listView)
	private NoScrollListView orderList;
	private List<SaoOrderListData> saolist = new ArrayList<SaoOrderListData>();
	private SaoOrderListAdpter saoAdapter;
	// 传过来的参数
	private String price;
	private String doctor_id;
	private String doc_assistant_id = "0";
	private String uid;
	// 验证码
	private boolean ishavePhone = false;

	GradientDrawable drawable1;
	GradientDrawable drawable2;
	private String uphone;
	private String phone;
	private String codeStr;

	@BindView(id = R.id.input_order_message_ly)
	private LinearLayout tianMessageLy;// 验证手机号框
	@BindView(id = R.id.order_phone_number_et)
	private EditText phoneNumberEt;
	@BindView(id = R.id.order_phone_code_et)
	private EditText phoneCode;
	@BindView(id = R.id.yanzheng_code_rly)
	private RelativeLayout sendEMSRly;
	@BindView(id = R.id.yanzheng_code_tv)
	private TextView emsTv;

	@BindView(id = R.id.write_phone_bangding_ly, click = true)
	private RelativeLayout bangdingLy;// 绑定手机框
	@BindView(id = R.id.write_bangding_phone_tv)
	private TextView phoneTv;// 绑定的手机号

	// 结算信息
	@BindView(id = R.id.order_sao_zhifu_tv)
	private TextView orderPriceTv;// 订单金额
	@BindView(id = R.id.sao_order_lijian_tv)
	private TextView dingjinTv;// 已付订金
	@BindView(id = R.id.order_sao_hos_tv)
	private TextView daoyuanTv;// 到院支付

	@BindView(id = R.id.sumbit_order_bt, click = true)
	private Button submitBt;// 提交

	private String finalprice;
	private String order_id = "0";

	@BindView(id = R.id.sao_yuding_ly)
	private LinearLayout saoYudingLy;


	@Override
	public void setRootView() {
		setContentView(R.layout.acty_saoordermessage);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = SaoOrderMessageActivity.this;
		postion_sao = -1;
		mTop.setCenterText("订单信息");
		uid = Utils.getUid();

		Intent it = getIntent();
		price = it.getStringExtra("price");
		doctor_id = it.getStringExtra("doctor_id");
		doc_assistant_id = it.getStringExtra("doc_assistant_id");

		orderPriceTv.setText("￥" + price);
		daoyuanTv.setText("￥" + price);
		finalprice = price;
		initSaoOrderList();

		drawable1 = new GradientDrawable();
		drawable1.setShape(GradientDrawable.RECTANGLE); // 画框
		drawable1.setStroke(1, getResources().getColor(R.color.red_ff4965)); // 边框粗细及颜色
		drawable1.setColor(0xFFFFFFFF); // 边框内部颜色

		drawable2 = new GradientDrawable();
		drawable2.setShape(GradientDrawable.RECTANGLE); // 画框
		drawable2.setStroke(1, getResources().getColor(R.color.button_bian2)); // 边框粗细及颜色
		drawable2.setColor(0xFFFFFFFF); // 边框内部颜色

		initGetPhone();

		sendEMSRly.setBackgroundDrawable(drawable1); // 设置背景（效果就是有边框及底色）

		sendEMSRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String phones = phoneNumberEt.getText().toString().trim();
				if (phones.length() > 0 && null != phones) {
					if (ifPhoneNumber()) {
						sendEMSRly.setClickable(false);
						sendEMS();
						// phoneNumberEt.clearFocus();
						phoneCode.requestFocus();
						InputMethodManager inputManager = (InputMethodManager) phoneCode
								.getContext().getSystemService(
										Context.INPUT_METHOD_SERVICE);
						inputManager.showSoftInput(phoneCode, 0);
						// phoneCode.clearFocus();

						new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

							@Override
							public void onTick(long millisUntilFinished) {
								sendEMSRly.setBackgroundDrawable(drawable2);
								emsTv.setTextColor(getResources().getColor(
										R.color.button_zi));
								emsTv.setText("(" + millisUntilFinished / 1000
										+ ")重新获取");
							}

							@Override
							public void onFinish() {
								sendEMSRly.setBackgroundDrawable(drawable1);
								emsTv.setTextColor(getResources().getColor(
										R.color.button_bian_hong1));
								sendEMSRly.setClickable(true);
								emsTv.setText("重新获取验证码");
							}
						}.start();
					} else {
						ViewInject.toast("请输入正确的手机号");
					}
				} else {
					ViewInject.toast("请输入手机号");
				}
			}
		});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

	}

	private void initSaoOrderList() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid",uid);
		maps.put("docid",doctor_id);
		new InitSaoOrderListApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					try {
						SaoOrderListData saoOrderListData = JSONUtil.TransformSingleBean(serverData.data,
								SaoOrderListData.class);
						saolist.add(saoOrderListData);

						if (null != saolist && saolist.size() > 0) {
							saoYudingLy.setVisibility(View.VISIBLE);

							saoAdapter = new SaoOrderListAdpter(mContex,
									saolist);
							orderList.setAdapter(saoAdapter);

							orderList
									.setOnItemClickListener(new OnItemClickListener() {

										@Override
										public void onItemClick(
												AdapterView<?> parent,
												View view, int position,
												long id) {

											String status = saolist.get(
													position).getStatus();

											if (position == postion_sao) {// 取消选中
												postion_sao = -1;

												saoAdapter = new SaoOrderListAdpter(
														mContex, saolist);
												orderList
														.setAdapter(saoAdapter);

												dingjinTv.setText("0");
												daoyuanTv.setText("￥"
														+ price);

												finalprice = price;
												order_id = "0";
											} else {

												// 未付定金
												if (status.equals("1")) {

													dingjinTv.setText("0");
													daoyuanTv.setText("￥"
															+ price);

													finalprice = price;
													order_id = saolist.get(
															position)
															.getOrder_id();

													postion_sao = position;

													saoAdapter = new SaoOrderListAdpter(
															mContex,
															saolist);
													orderList
															.setAdapter(saoAdapter);
												} else {// 已付订金

													String dj = saolist
															.get(position)
															.getPrice();
													dingjinTv.setText("-￥"
															+ dj);

													int dji = Integer
															.parseInt(dj);
													int zz = Integer
															.parseInt(price);

													if (dji > zz) {

														postion_sao = -1;

														saoAdapter = new SaoOrderListAdpter(
																mContex,
																saolist);
														orderList
																.setAdapter(saoAdapter);

														MyToast.makeTextToast(
																mContex,
																"不可选", 1000)
																.show();

													} else {
														int cha = zz - dji;
														daoyuanTv
																.setText("￥"
																		+ cha);

														finalprice = cha
																+ "";
														order_id = saolist
																.get(position)
																.getOrder_id();

														postion_sao = position;

														saoAdapter = new SaoOrderListAdpter(
																mContex,
																saolist);
														orderList
																.setAdapter(saoAdapter);
													}

												}

											}
										}
									});
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					saoYudingLy.setVisibility(View.GONE);
				}
			}

		});
	}

	/**
	 * 获取手机号
	 */
	void initGetPhone() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid",uid);
		new GetUserPhoneNumberApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					UserPhoneData userPhoneData = JSONUtil.TransformUserPhone(serverData.data);
					uphone = userPhoneData.getPhone();
					String spj = uphone;

					if (uphone.equals("0")) {
						bangdingLy.setVisibility(View.GONE);
						tianMessageLy.setVisibility(View.VISIBLE);

						ishavePhone = false;
					} else {
						phone = uphone;
						bangdingLy.setVisibility(View.VISIBLE);
						tianMessageLy.setVisibility(View.GONE);

						String a = spj.substring(0, 3);
						String b = spj.substring(phone.length() - 4,
								spj.length());
						String ss = a + "****" + b;
						phoneTv.setText(ss);

						ishavePhone = true;
					}
				}
			}

		});
	}

	void sendEMS() {
		phone = phoneNumberEt.getText().toString();
		Map<String,Object> maps=new HashMap<>();
		maps.put("phone",phone);
		maps.put("flag","1");
		new SendEMSApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	/**
	 * 登录状态下提交验证码
	 */
	void initCode() {
		codeStr = phoneCode.getText().toString();
		phone = phoneNumberEt.getText().toString();
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phone);
		maps.put("code",codeStr);
		new CommitSecurityCodeInLoginApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					submitPayOrder();
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	private boolean ifPhoneNumber() {
		String textPhn = phoneNumberEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.sumbit_order_bt:// 提交

			if (ishavePhone) {
				submitPayOrder();
			} else {
				codeStr = phoneCode.getText().toString();
				phone = phoneNumberEt.getText().toString();

				if (phone.length() > 0) {
					if (codeStr.length() > 0) {
						initCode();
					} else {
						MyToast.makeTextToast(mContex, "请输入验证码", 1000);
					}
				} else {
					MyToast.makeTextToast(mContex, "请输入手机号", 1000);
				}
			}
			break;
		case R.id.write_phone_bangding_ly:// 更改手机号
			Intent it = new Intent();
			it.setClass(mContex, OrderPhoneModifyActivity.class);
			startActivityForResult(it, 6);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 6:
			if (null != data) {
				String phonenum = data.getStringExtra("phone");
				phone = phonenum;

				String spj = phone;
				String a = spj.substring(0, 3);
				String b = spj.substring(spj.length() - 4, spj.length());
				String ss = a + "****" + b;
				phoneTv.setText(ss);
			}
			break;
		}
	}

	void submitPayOrder() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid", uid);
		maps.put("phone", phone);
		maps.put("price", price);
		maps.put("docid", doctor_id);
		maps.put("doc_assistant_id", doc_assistant_id);
		maps.put("order_id", order_id);
		new SubmitPayOrderApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					try {
						SaoXiadanData sdata =JSONUtil.TransformSingleBean(serverData.data,
								SaoXiadanData.class);
						String server_id = sdata.getServer_id();
						String order_id = sdata.getOrder_id();
						String title = sdata.getTitle();
						String order_time = sdata.getOrder_time();
						String money_=sdata.getMoney();

						Intent it = new Intent();
						it.putExtra("price", money_);
						it.putExtra("server_id", server_id);
						it.putExtra("order_id", order_id);
						it.putExtra("tao_title", title);
						it.putExtra("order_time", order_time);
						it.setClass(mContex, OrdePaySaoActivity.class);
						startActivity(it);

						Cfg.saveStr(mContex, "server_id", server_id);
						Cfg.saveStr(mContex, "order_id", order_id);
						Cfg.saveStr(mContex, "taotitle", title);
						Cfg.saveStr(mContex, "price", money_);

						finish();

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					MyToast.makeTextToast(mContex, serverData.message, 1000).show();
				}
			}

		});
//		KJHttp kjh = new KJHttp();
//		KJStringParams params = new KJStringParams();
//		params.put("uid", uid);
//		params.put("phone", phone);
//		params.put("price", price);
//		params.put("docid", doctor_id);
//		params.put("doc_assistant_id", doc_assistant_id);
//		params.put("order_id", order_id);
//		// Log.e("AAAA", "params===" + params);
//		kjh.post(FinalConstant.SAO_SUBMIT_ORDER, params, new StringCallBack() {
//
//			@Override
//			public void onSuccess(String json) {
//
//				// Log.e("AAAAA", "json===" + json);
//
//				if (null != json && json.length() > 0) {
//					String code = JSONUtil
//							.resolveJson(json, FinalConstant.CODE);
//					String message = JSONUtil.resolveJson(json,
//							FinalConstant.MESSAGE);
//					if (code.equals("1")) {
//						try {
//							SaoXiadan saox = new SaoXiadan();
//							saox = JSONUtil.TransformSingleBean(json,
//									SaoXiadan.class);
//							SaoXiadanData sdata = new SaoXiadanData();
//							sdata = saox.getData();
//							String server_id = sdata.getServer_id();
//							String order_id = sdata.getOrder_id();
//							String title = sdata.getTitle();
//							String order_time = sdata.getOrder_time();
//							String money_=sdata.getMoney();
//
//							Intent it = new Intent();
//							it.putExtra("price", money_);
//							it.putExtra("server_id", server_id);
//							it.putExtra("order_id", order_id);
//							it.putExtra("tao_title", title);
//							it.putExtra("order_time", order_time);
//							it.setClass(mContex, OrdePaySaoActivity.class);
//							startActivity(it);
//
//							Cfg.saveStr(mContex, "server_id", server_id);
//							Cfg.saveStr(mContex, "order_id", order_id);
//							Cfg.saveStr(mContex, "taotitle", title);
//							Cfg.saveStr(mContex, "price", money_);
//
//							finish();
//
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					} else {
//						MyToast.makeTextToast(mContex, message, 1000).show();
//					}
//				} else {
//					MyToast.makeTextToast(mContex, "请求出错了", 1000).show();
//				}
//			}
//		});
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
