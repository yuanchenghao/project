package com.module.my.view.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;

import com.qmuiteam.qmui.widget.QMUIRadiusImageView;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import org.xutils.common.util.DensityUtil;

/**
 * 带进度的ImageView
 * Created by 裴成浩 on 2018/8/8.
 */
//AppCompatImageView
public class PostNoteUploadImageView extends QMUIRadiusImageView {
    private Context mContext;

    RectF mViewRectB = new RectF();
    private int mProgress;
    private String TAG = "PostNoteUploadImageView";
    private Bitmap bitmapVideo;
    private boolean isVideo;
    private int mWidth;
    private RectF mRoundRect;

    public PostNoteUploadImageView(Context context) {
        this(context, null);
    }

    public PostNoteUploadImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PostNoteUploadImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    /**
     * 设置UI
     */
    private void initView() {
        bitmapVideo = BitmapFactory.decodeResource(getResources(), R.drawable.ic_video_play_1x);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = Math.min(getMeasuredWidth(), getMeasuredHeight());
        setMeasuredDimension(mWidth, mWidth);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {

        Log.e(TAG, "mProgress == " + mProgress);

        Paint mBitmapPaint = new Paint();
        mBitmapPaint.setAntiAlias(true);
        mRoundRect = new RectF(0, 0, mWidth, mWidth);
        canvas.drawRoundRect(mRoundRect, Utils.dip2px(5), Utils.dip2px(5), mBitmapPaint);

        super.onDraw(canvas);

        //如果是视频
        if (isVideo) {
            int mBitWidth = bitmapVideo.getWidth();
            int mBitHeight = bitmapVideo.getHeight();
            Rect mSrcRect = new Rect(0, 0, mBitWidth, mBitHeight);
            // 计算左边位置
            int left = mWidth / 2 - mBitWidth / 2;
            // 计算上边位置
            int top = mWidth / 2 - mBitHeight / 2;
            Rect mDestRect = new Rect(left, top, left + mBitWidth, top + mBitHeight);
            canvas.drawBitmap(bitmapVideo, mSrcRect, mDestRect, null);
        }

        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true); // 消除锯齿
        mPaint.setStyle(Paint.Style.FILL);

        Log.e(TAG, "mProgress === " + mProgress);
        mPaint.setColor(Color.parseColor("#e6ffc6d2"));// 半透明
        mViewRectB.left = 0;
        mViewRectB.top = getHeight() * mProgress / 100;
        mViewRectB.right = getWidth();
        mViewRectB.bottom = getHeight();
        canvas.drawRoundRect(mViewRectB, DensityUtil.dip2px(5), DensityUtil.dip2px(5), mPaint);

//        mPaint.setColor(Color.parseColor("#00000000"));// 全透明
//        mViewRectQ.left = 0;
//        mViewRectQ.top = 0;
//        mViewRectQ.right = getWidth();
//        mViewRectQ.bottom = getHeight() * mProgress / 100;
//
//        canvas.drawRoundRect(mViewRectQ, DensityUtil.dip2px(5), DensityUtil.dip2px(5), mPaint);
    }

    /**
     * 开始上传
     *
     * @param progress 进度
     */
    public void startUpload(int progress) {
        this.mProgress = progress;

        invalidate();
    }

    /**
     * 判断是否是视频
     *
     * @param isVideo
     */
    public void setVideoButton(boolean isVideo) {
        this.isVideo = isVideo;
    }

}
