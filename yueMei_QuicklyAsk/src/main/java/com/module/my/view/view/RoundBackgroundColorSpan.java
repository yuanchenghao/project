package com.module.my.view.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import com.quicklyask.util.Utils;

/**
 * 优惠卷圆角富文本
 * Created by 裴成浩 on 2018/3/29.
 */

public class RoundBackgroundColorSpan extends ReplacementSpan {

    private int mSize;
    private int mColor;
    private int mRadius;
    private int mTextColor;
    private int height;

    /**
     * @param color        背景颜色
     * @param textColor    文字颜色
     * @param radius       圆角半径
     * @param baseTextSize baseTextSize
     */
    public RoundBackgroundColorSpan(int color, int textColor, int radius, int baseTextSize) {
        mColor = color;
        mRadius = radius;
        mTextColor = textColor;
        height = baseTextSize;
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        mSize = (int) (paint.measureText(text, start, end) + Utils.dip2px(1) * mRadius);
        return mSize + Utils.dip2px(5);
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {

        paint.setColor(mColor);//设置背景颜色
        paint.setAntiAlias(true);// 设置画笔的锯齿效果


        //文字高度  顶线-底线
        float centHeight = paint.descent() - paint.ascent();

        float diffHeight = height - centHeight;

        RectF oval = new RectF(x, y + paint.ascent() - diffHeight, x + mSize, y + paint.descent());
        canvas.drawRoundRect(oval, mRadius, mRadius, paint);
        paint.setColor(mTextColor);

        canvas.drawText(text, start, end, x + mRadius, y - diffHeight / 2, paint);
    }

}
