/**
 * 
 */
package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;

/**
 * 银行转账说明
 *
 * @author Robin
 *
 */
public class BankTransferActivity extends BaseActivity {

	private final String TAG = "BankTransferActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.wan_beautiful_web_back, click = true)
	private RelativeLayout back;// 返回

	private WebView docDetWeb;

	private BankTransferActivity mContex;

	public JSONObject obj_http;

	private String price;
	private String order_id;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_banktransfer);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = BankTransferActivity.this;

		Intent it = getIntent();
		price = it.getStringExtra("price");
		order_id = it.getStringExtra("order_id");

		scollwebView.GetLinearLayout(contentWeb);
		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		initWebview();

		LodUrl1(FinalConstant.BANK_TRANSFER);

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						BankTransferActivity.this.finish();
					}
				});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		contentWeb.addView(docDetWeb);
	}

	/**
	 * 处理webview里面的按钮
	 *
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		Log.e(TAG, urlStr);
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();

		HashMap<String, Object> urlMap = new HashMap<>();
		urlMap.put("order_id",order_id);
		urlMap.put("price",price);

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.wan_beautiful_web_back:
			onBackPressed();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
