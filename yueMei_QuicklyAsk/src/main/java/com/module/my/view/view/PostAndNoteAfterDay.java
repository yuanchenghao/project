package com.module.my.view.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 术后天数
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostAndNoteAfterDay extends LinearLayout {

    private final Context mContext;
    private TextView mTime;

    public PostAndNoteAfterDay(Context context) {
        this(context, null);
    }

    public PostAndNoteAfterDay(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PostAndNoteAfterDay(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView() {
        setOrientation(VERTICAL);

        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(HORIZONTAL);
        LayoutParams linearLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(linearLayoutParams);

        //术后图标
        ImageView imageView = new ImageView(mContext);
        imageView.setBackgroundResource(R.drawable.after_time);


        //术后天数
        mTime = new TextView(mContext);
        mTime.setTextColor(Utils.getLocalColor(mContext, R.color._66));
        mTime.setTextSize(14);
        setAfterDay(0);                 //设置术后天数默认是0天
        LayoutParams mTimeParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mTimeParams.leftMargin = Utils.dip2px(9);
        mTimeParams.weight = 1;
        mTime.setLayoutParams(mTimeParams);

        //左边文案
        TextView mWenan = new TextView(mContext);
        mWenan.setTextColor(Utils.getLocalColor(mContext, R.color._66));
        mWenan.setTextSize(14);
        LayoutParams mWenanParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mWenanParams.gravity = Gravity.CENTER;
        mWenanParams.rightMargin = Utils.dip2px(9);
        mWenan.setLayoutParams(mWenanParams);

        //术后图标
        ImageView imageViewRight = new ImageView(mContext);
        imageViewRight.setBackgroundResource(R.drawable.arrow_right_gray);

        linearLayout.addView(mTime);
        linearLayout.addView(mWenan);
        linearLayout.addView(imageViewRight);

        //设置分割线
        View line = new View(mContext);
        line.setBackgroundColor(Utils.getLocalColor(mContext, R.color._e5));
        LayoutParams lineParams = new LayoutParams(LayoutParams.MATCH_PARENT, Utils.dip2px(1));
        line.setLayoutParams(lineParams);

        //设置子布局
        addView(linearLayout);
        addView(line);
    }

    /**
     * 获取术后天数
     *
     * @return
     */
    public int getAfterDay() {
        return Integer.parseInt(mTime.getText().toString().trim());
    }

    /**
     * 设置术后天数
     */
    @SuppressLint("SetTextI18n")
    public void setAfterDay(int day) {
        mTime.setText(day + "");
    }

}
