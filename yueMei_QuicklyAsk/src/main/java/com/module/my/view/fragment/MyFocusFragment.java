package com.module.my.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.base.refresh.loadmore.LoadMoreListener;
import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.base.refresh.refresh.RefreshListener;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.my.controller.activity.QuanziDetailActivity;
import com.module.my.controller.adapter.MyFocusAdapter;
import com.module.my.model.api.MyFocusApi;
import com.module.my.model.bean.MyFansData;
import com.module.my.model.bean.MyFocusData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * 我的关注fragment
 * Created by 裴成浩 on 2018/4/12.
 */

public class MyFocusFragment extends Fragment {

    private String mType;
    private int mPage = 1;

    private MyPullRefresh mRefresh;
    private LoadMoreListView mListView;
    private MyFocusApi myFocusApi;
    private MyFocusAdapter myFocusAdapter;
    private boolean isNoMore = false;
    private LayoutInflater mInflater;
    private String TAG = "MyFocusFragment";
    private List<MyFansData> mDatas = new ArrayList<>();
    private List<MyFansData> mInterests = new ArrayList<>();
    private View headerView = null;
    private int mTempPos = -1;
    private FragmentActivity mActivity;

    public static MyFocusFragment newInstance(String diaryId) {
        MyFocusFragment fragment = new MyFocusFragment();
        Bundle bundle = new Bundle();
        bundle.putString("diaryId", diaryId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mType = getArguments().getString("diaryId");
        mActivity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_focus, container, false);

        mRefresh = view.findViewById(R.id.focuns_refresh);
        mListView = view.findViewById(R.id.my_focus_list);

        myFocusApi = new MyFocusApi();
        mInflater = inflater;
        initData();
        loadData();

        return view;
    }

    private void initData() {
        mListView.setLoadMoreListener(new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isNoMore) {
                    loadData();
                } else {
                    mListView.loadMoreEnd();
                }
            }
        });

        mRefresh.setRefreshListener(new RefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                mDatas.clear();
                mInterests.clear();
                mListView.setEnabled(false);
                myFocusAdapter = null;
                isNoMore = false;
                loadData();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<MyFansData> fansDatas = myFocusAdapter.getmFansDatas();

                Log.e(TAG, "fansDatas.size() == " + fansDatas.size());
                Log.e(TAG, "position == " + position);
                if (headerView != null) {
                    if (position != 0) {
                        jump(position - 1, fansDatas);
                    }
                } else {
                    if (position != fansDatas.size()) {
                        jump(position, fansDatas);
                    }
                }
            }
        });
    }

    private void jump(int position, List<MyFansData> fansDatas) {
        mTempPos = position;
        MyFansData myFansData = fansDatas.get(position);
        Intent mIntent = new Intent();
        switch (mType) {
            case "6":
                mIntent.setClass(mActivity, PersonCenterActivity641.class);
                mIntent.putExtra("id", myFansData.getObj_id());
                break;
            case "1":
                mIntent.setClass(mActivity, DoctorDetailsActivity592.class);
                mIntent.putExtra("docId", myFansData.getObj_id());
                mIntent.putExtra("docName", myFansData.getName());
                mIntent.putExtra("partId", "");
                break;
            case "3":
                mIntent.setClass(mActivity, HosDetailActivity.class);
                mIntent.putExtra("hosid", myFansData.getObj_id());
                break;
            case "0":
                mIntent.setClass(mActivity, QuanziDetailActivity.class);
                mIntent.putExtra("url_name", myFansData.getUrl());
                break;
        }
        startActivityForResult(mIntent, 10);
    }

    private void loadData() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("page", mPage + "");
        hashMap.put("type", mType);

        myFocusApi.getCallBack(mActivity, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    MyFocusData focusDatas = JSONUtil.TransformSingleBean(serverData.data, MyFocusData.class);
                    Log.e(TAG, "mType === " + mType);
                    mPage++;
                    mRefresh.finishRefresh();
                    mDatas = focusDatas.getData();
                    mInterests = focusDatas.getInterest();

                    if (myFocusAdapter == null) {
                        if (mDatas.size() != 0) {
                            if (headerView != null) {
                                mListView.removeHeaderView(headerView);
                            }
                            myFocusAdapter = new MyFocusAdapter(mActivity, mDatas, "1");
                        } else {
                            if (headerView == null) {
                                focusCopy();
                            }
                            myFocusAdapter = new MyFocusAdapter(mActivity, mInterests, "0");
                        }
                        mListView.setAdapter(myFocusAdapter);
                    } else {
                        if (mDatas.size() != 0) {
                            myFocusAdapter.addData(mDatas);
                        } else {
                            isNoMore = true;
                        }
                    }

                    if (isNoMore) {
                        mListView.loadMoreEnd();
                    } else {
                        mListView.loadMoreComplete();
                    }
                    mListView.setEnabled(true);
                }else {
                    Toast.makeText(mActivity,serverData.message,Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    /**
     * 添加头布局
     */
    private void focusCopy() {
        headerView = mInflater.inflate(R.layout.fragment_my_focus_header, null, false);
        TextView focusCopy = headerView.findViewById(R.id.header_focus_copy);
        switch (mType) {
            case "6":
                focusCopy.setText("还没有关注任何悦美用户哦");
                break;
            case "1":
                focusCopy.setText("还没有关注任何医生哦");
                break;
            case "3":
                focusCopy.setText("还没有关注任何医院哦");
                break;
            case "0":
                focusCopy.setText("还没有关注任何圈子哦");
                break;
        }
        mListView.addHeaderView(headerView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 && resultCode == 100) {
            if (!"3".equals(mType)) {
                String focus = data.getStringExtra("focus");
                if (!TextUtils.isEmpty(focus) && mTempPos >= 0) {
                    myFocusAdapter.setEachFollowing(mTempPos, focus);
                    mTempPos = -1;
                }
            } else {
                isFocu("3");
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String type) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", myFocusAdapter.getmFansDatas().get(mTempPos).getObj_id());
        hashMap.put("type", type);
        new IsFocuApi().getCallBack(mActivity, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                myFocusAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }

}
