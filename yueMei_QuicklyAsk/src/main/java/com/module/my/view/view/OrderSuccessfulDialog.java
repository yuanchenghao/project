package com.module.my.view.view;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.module.community.web.WebUtil;
import com.module.community.web.WebViewInitImpl;
import com.quicklyask.activity.R;

/**
 * 支付成功页面弹层
 * Created by 裴成浩 on 2019/8/21
 */
public class OrderSuccessfulDialog extends Dialog {
    private final Activity mActivity;
    private final WebViewInitImpl mWebViewInitializer;
    private final String mUrl;

    public OrderSuccessfulDialog(Activity activity,String url) {
        super(activity, R.style.mystyle1);
        this.mActivity = activity;
        this.mUrl = url;

        // WebView初始化对象
        mWebViewInitializer = new WebViewInitImpl(mActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.order_successful_dialog_view);
        FrameLayout mWebViewContainer = findViewById(R.id.order_successful_dialog);
        ImageView mClose = findViewById(R.id.order_successful_close);

        WebView mWebView = mWebViewInitializer.initWebView(new WebView(mActivity));
        mWebView.setWebViewClient(mWebViewInitializer.initWebViewClient());
        mWebView.setWebChromeClient(mWebViewInitializer.initWebChromeClient());

        mWebViewContainer.addView(mWebView);

        WebUtil.getInstance().loadPage(mWebView, mUrl);

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setCanceledOnTouchOutside(false);
    }
}
