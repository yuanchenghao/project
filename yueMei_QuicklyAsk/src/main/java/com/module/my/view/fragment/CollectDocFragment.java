/**
 * 
 */
package com.module.my.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.adapter.DoctorListAdpter;
import com.module.doctor.model.bean.DocListData;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.CollectDocApi;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收藏的医生
 * 
 * @author Robin
 * 
 */
public class CollectDocFragment extends ListFragment {

	private final String TAG = "CollectDocFragment";

	private Activity mCotext;

	private LinearLayout docText;
	// List
	private DropDownListView mlist;
	private int mCurPage = 1;
	private Handler mHandler;

	private List<DocListData> lvHotIssueData = new ArrayList<DocListData>();
	private List<DocListData> lvHotIssueMoreData = new ArrayList<DocListData>();
	private DoctorListAdpter hotAdpter;

	private LinearLayout nodataTv;

	private String uid;
	private LoadingProgress mDialog;
	private CollectDocApi collectDocApi;
	private HashMap<String, Object> collectDocApiMap = new HashMap<>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_collect_san_550, container,
				false);
		nodataTv = v
				.findViewById(R.id.my_collect_post_tv_nodata);
		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Log.e(TAG, "onCreate");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
		super.onActivityCreated(savedInstanceState);
		mCotext = getActivity();
		mDialog = new LoadingProgress(mCotext);
		collectDocApi = new CollectDocApi();

		mlist = (DropDownListView) getListView();
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MainScreen"); // 统计页面
		StatService.onResume(getActivity());
		// Log.e(TAG, "onResume");

		initList();
	}

	@Override
	public void onStart() {
		super.onStart();

		uid = Utils.getUid();
	}

	void initList() {

		mHandler = getHandler();
		mDialog.startLoading();
		lodHotIssueData(true);

		mlist.setOnBottomListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lodHotIssueData(false);
			}
		});

		mlist.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				DeleteDialog(pos);
				return true;
			}
		});

		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpter, View v, int pos,
					long arg3) {

				String docId = lvHotIssueData.get(pos).getUser_id();
				String docName = lvHotIssueData.get(pos).getUsername();
				Intent it0 = new Intent();
				it0.putExtra("docId", docId);
				it0.putExtra("docName", docName);
				it0.putExtra("partId", "");
				it0.setClass(getActivity(), DoctorDetailsActivity592.class);
				startActivity(it0);

			}
		});
	}

	void lodHotIssueData(final boolean isDonwn) {
		collectDocApiMap.put("type","1");
		collectDocApiMap.put("id",uid);
		collectDocApiMap.put("page",mCurPage+"");
		collectDocApi.getCallBack(mCotext, collectDocApiMap, new BaseCallBackListener<List<DocListData>>() {
			@Override
			public void onSuccess(List<DocListData> docListDatas) {
				Message msg = null;
				if (isDonwn) {
					if (mCurPage == 1) {
						lvHotIssueData = docListDatas;

						msg = mHandler.obtainMessage(1);
						msg.sendToTarget();
					}
				} else {
					mCurPage++;
					lvHotIssueMoreData = docListDatas;

					msg = mHandler.obtainMessage(2);
					msg.sendToTarget();
				}
			}
		});
	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (null != lvHotIssueData && lvHotIssueData.size() > 0) {
						nodataTv.setVisibility(View.GONE);

						mDialog.stopLoading();
						if (null != getActivity()) {
							hotAdpter = new DoctorListAdpter(getActivity(),
									lvHotIssueData);
							setListAdapter(hotAdpter);
						}

						mlist.onBottomComplete();
					} else {
						mDialog.stopLoading();
						nodataTv.setVisibility(View.VISIBLE);
						mlist.setVisibility(View.GONE);
						// ViewInject.toast("您还没有收藏的专家");
					}
					break;
				case 2:
					if (null != lvHotIssueMoreData
							&& lvHotIssueMoreData.size() > 0) {
						hotAdpter.add(lvHotIssueMoreData);
						hotAdpter.notifyDataSetChanged();
						mlist.onBottomComplete();
					} else {
						mlist.setHasMore(false);
						mlist.setShowFooterWhenNoMore(true);
						mlist.onBottomComplete();
					}
					break;
				}

			}
		};
	}

	/**
	 * 长按item删除对话框
	 * 
	 * @param pos
	 */
	private void DeleteDialog(final int pos) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setMessage("      确定取消对该专家的收藏？");
		// 确定按钮监听
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				String userid = lvHotIssueData.get(pos).getUser_id();
				deleteCollect(userid);

				lvHotIssueData.remove(pos);

				if (lvHotIssueData.equals("") || lvHotIssueData == null) {
					nodataTv.setVisibility(View.VISIBLE);
				}


				if (lvHotIssueData.size() == 1 && pos == 0) {
					nodataTv.setVisibility(View.VISIBLE);
					mlist.setVisibility(View.GONE);
				}

				hotAdpter.notifyDataSetChanged();
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		builder.create().show();
		builder.create().setCanceledOnTouchOutside(false);
	}

	/**
	 * 取消对专家的收藏
	 * 
	 * @param userid
	 */
	void deleteCollect(String userid) {
		CancelCollectApi cancelCollectApi = new CancelCollectApi();
		Map<String, Object> params = new HashMap<>();
		params.put("uid", uid);
		params.put("objid", userid);
		params.put("type", "1");
		cancelCollectApi.getCallBack(mCotext, params, new BaseCallBackListener() {
			@Override
			public void onSuccess(Object o) {

			}
		});
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MainScreen");
		StatService.onPause(getActivity());
	}
}
