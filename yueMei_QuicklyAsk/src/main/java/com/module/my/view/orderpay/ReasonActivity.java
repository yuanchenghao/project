package com.module.my.view.orderpay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.KeyBoardUtils;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class ReasonActivity extends YMBaseActivity {
    public static final String TAG = "ReasonActivity";
    @BindView(R.id.order_phone_test_back)
    RelativeLayout mOrderPhoneTestBack;
    @BindView(R.id.order_cancel_title)
    RelativeLayout mOrderCancelTitle;
    @BindView(R.id.backmoney_title_tv)
    TextView mBackmoneyTitleTv;
    @BindView(R.id.person_reason_list)
    RecyclerView mPersonReasonList;

    @BindView(R.id.person_ok_btn)
    Button mPersonOkBtn;
    @BindView(R.id.person_ohter_edt)
    public EditText mPersonOhterEdt;
    @BindView(R.id.message_visorgone)
    public LinearLayout mMessageVisorgone;
    @BindView(R.id.keyboard_container)
    public LinearLayout keyboardContainer;
    @BindView(R.id.reason_scrollview)
    public ScrollView mScrollView;
    private int mPostion;
    Context mContext;
    String[] personReason = {"使用红包、代金券重新下单", "预约时间不合适，没时间消费", "个人身体原因", "距离远，不想做",
            "订单已过期", "买错了，买多了", "想更换其他医院/医生", "更多不爽？必须吐槽"};
    String[] hospatilReason = {"联系不上医院/医生", "向我推销其他项目", "医院说可直接以悦美价到院消费", "医院服务与悦美描述不符",
            "更换了医生", "与医院沟通不畅", "医院拒绝提供服务", "医院单方面限购", "对医院/医生不太满意", "更多不爽？必须吐槽"};
    List<String> mDataList = new ArrayList<>();
    private String mType;
    private MyAdapter mResonAdapter;
    private int mPosition;
    private String mMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        mContext = ReasonActivity.this;
        return R.layout.activity_person_reason;
    }

    @Override
    protected void initView() {
        Intent intent = getIntent();
        mType = intent.getStringExtra("type");
        mMessage = intent.getStringExtra("message");
        mPosition = intent.getIntExtra("position", -1);
        ViewGroup.LayoutParams layoutParams = mOrderCancelTitle.getLayoutParams();
        layoutParams.height = Utils.dip2px(50) + statusbarHeight;
        mOrderCancelTitle.setLayoutParams(layoutParams);

    }

    @Override
    protected void initData() {
        int pos = 0;
        if ("1".equals(mType)) {
            for (int i = 0; i < personReason.length; i++) {
                mDataList.add(personReason[i]);
            }
        } else {
            for (int i = 0; i < hospatilReason.length; i++) {
                mDataList.add(hospatilReason[i]);
            }
        }
        if (mDataList.size()-1 ==mPosition) {
            mMessageVisorgone.setVisibility(View.VISIBLE);
            mPersonOhterEdt.setText(mMessage);
            if ("2".equals(mType)) {


                mScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        mScrollView.scrollTo(0, Utils.dip2px(200));
                    }
                });
            }
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            mResonAdapter = new MyAdapter(mContext, mDataList, mPosition);
            mPersonReasonList.setLayoutManager(linearLayoutManager);
            mPersonReasonList.setAdapter(mResonAdapter);
            mResonAdapter.setRecyclerViewOnItemClickListener(new MyAdapter.RecyclerViewOnItemClickListener() {
                @Override
                public void onItemClickListener(View view, int position) {

                    mResonAdapter.singleSelect(position);
                    if (position == mDataList.size() - 1) {
                        mMessageVisorgone.setVisibility(View.VISIBLE);
                        KeyBoardUtils.showKeyBoard(mContext, mPersonOhterEdt);
                        mScrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                mScrollView.scrollTo(0, Utils.dip2px(290));
                            }
                        });
                    } else {
                        mMessageVisorgone.setVisibility(View.GONE);
                    }
                }
            });



    }

    int postion = -1;

    @OnClick({R.id.order_phone_test_back, R.id.person_ok_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.order_phone_test_back:
                finish();
                break;
            case R.id.person_ok_btn:
                StringBuffer stringBuffer = new StringBuffer();

                //获取你选中的item
                Map<Integer, Boolean> map = mResonAdapter.getMap();
                for (int i = 0; i < map.size(); i++) {
                    if (map.get(i)) {
                        postion = i;
                        if (i==mDataList.size()-1){
                            mMessage=mPersonOhterEdt.getText().toString().trim();
                        }else {
                            stringBuffer.append(mDataList.get(i));
                        }

                    }
                }

                if (TextUtils.isEmpty(stringBuffer.toString())&&TextUtils.isEmpty(mMessage)) {
                    mFunctionManager.showShort("请选择退款原因");
                    return;
                }
                Intent intent = new Intent();
                Log.e(TAG, "str==" + stringBuffer.toString());
                Log.e(TAG, "message==" + mMessage);
                if (TextUtils.isEmpty(stringBuffer.toString())){
                    intent.putExtra("str", mMessage);
                }else {
                    intent.putExtra("str", stringBuffer.toString());
                }
                intent.putExtra("position", postion);
                setResult(55, intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(56, new Intent());
    }
}
