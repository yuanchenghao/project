/**
 *
 */
package com.module.my.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.my.model.api.MyCollectTaoApi;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收藏的淘整形
 *
 * @author Robin
 */
public class ColletTaoFragment extends Fragment {

    private final String TAG = "ColletTaoFragment";

    private Activity mContext;
    private LoadMoreListView mlist;
    private int mCurPage = 1;

    private List<HomeTaoData> lvHotIssueData = new ArrayList<>();
    private TaoAdpter623 hotAdpter;

    private LinearLayout nodataTv;
    private MyCollectTaoApi myCollectTaoApi;
    private HashMap<String, Object> myCollectTaoMap = new HashMap<>();
    private boolean isNoMore = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_collect_tao, container, false);
        nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata1);
        mlist = v.findViewById(R.id.collect_san_list1);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);

        myCollectTaoApi = new MyCollectTaoApi();

    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
        StatService.onResume(getActivity());

        initList();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    void initList() {
        lodHotIssueData();
        mlist.setLoadMoreListener(new com.module.base.refresh.loadmore.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isNoMore) {
                    lodHotIssueData();
                } else {
                    mlist.loadMoreEnd();
                }
            }
        });

        mlist.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                DeleteDialog(pos);
                return true;
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                List<HomeTaoData> taoCollectData = hotAdpter.getmHotIssues();
                if(pos != taoCollectData.size()){
                    String id = taoCollectData.get(pos).get_id();
                    Intent it1 = new Intent();
                    it1.putExtra("id", id);
                    it1.putExtra("source", "16");
                    it1.putExtra("objid", "0");
                    it1.setClass(mContext, TaoDetailActivity.class);
                    startActivity(it1);
                }
            }
        });
    }

    void lodHotIssueData() {
        myCollectTaoMap.put("id", Utils.getUid());
        myCollectTaoMap.put("page", mCurPage + "");

        myCollectTaoApi.getCallBack(mContext, myCollectTaoMap, new BaseCallBackListener<ArrayList<HomeTaoData>>() {
            @Override
            public void onSuccess(ArrayList<HomeTaoData> taoCollectDatas) {
                mCurPage++;
                lvHotIssueData = taoCollectDatas;
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (hotAdpter == null) {
                            hotAdpter = new TaoAdpter623(getActivity(), lvHotIssueData);
                            mlist.setAdapter(hotAdpter);
                        } else {
                            hotAdpter.add(lvHotIssueData);
                            hotAdpter.notifyDataSetChanged();
                        }

                        Log.e(TAG, "lvHotIssueData.size() === " + lvHotIssueData.size());
                        if (lvHotIssueData.size() < 20) {
                            isNoMore = true;
                        }

                        if (isNoMore) {
                            mlist.loadMoreEnd();
                        } else {
                            mlist.loadMoreComplete();
                        }
                    }
                });
            }
        });

    }

    /**
     * 长按item删除对话框
     *
     * @param pos
     */
    private void DeleteDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("确定取消对该淘整形的收藏？");
        // 确定按钮监听
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (hotAdpter.getData().size() > 0) {
                    String userid = hotAdpter.getData().get(pos).get_id();
                    deleteCollect(userid);
                    hotAdpter.getData().remove(pos);
                } else {
                    nodataTv.setVisibility(View.VISIBLE);
                }
                if (hotAdpter.getData().size() == 1 && pos == 0) {
                    nodataTv.setVisibility(View.VISIBLE);
                    mlist.setVisibility(View.GONE);
                }

                hotAdpter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
        builder.create().setCanceledOnTouchOutside(false);
    }

    /**
     * 取消对专家的收藏
     *
     * @param
     */
    void deleteCollect(String taoid) {
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", Utils.getUid());
        params.put("objid", taoid);
        params.put("type", "4");
        cancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {

            }
        });
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
        StatService.onPause(getActivity());
    }
}
