package com.module.my.view.orderpay;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.activity.UserAgreementWebActivity;
import com.module.my.model.api.PayALiPay;
import com.module.my.model.api.PayWeixinApi;
import com.module.my.model.api.PayYinlianApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.ALiPay;
import com.module.my.model.bean.ALiPayData;
import com.module.my.model.bean.PrePayId;
import com.module.my.model.bean.ShenHeData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.activity.weixin.Constants;
import com.quicklyask.activity.weixin.MD5;
import com.quicklyask.entity.PrePayIdData;
import com.quicklyask.entity.Result;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.unionpay.UPPayAssistEx;
import com.unionpay.uppay.PayActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * 扫描选择支付方式
 *
 * @author dwb
 */
public class OrdePaySaoActivity extends BaseActivity {

	private final String TAG = "OrdePaySaoActivity";

	@BindView(id = R.id.order_phone_test_back, click = true)
	private RelativeLayout back;// 返回
	@BindView(id = R.id.sumbit_order_bt, click = true)
	private Button next1;// 确认支付
	@BindView(id = R.id.order_method_shifu_tv)
	private TextView shifuTv;// 实付的价钱

	@BindView(id = R.id.order_zhifubao_check)
	private CheckBox check1;// 支付宝
	@BindView(id = R.id.order_yinhangka_check)
	private CheckBox check3;// 线下刷卡支付
	@BindView(id = R.id.order_weixin_check)
	private CheckBox check2;// 微信支付
	@BindView(id = R.id.order_yinlian_check)
	private CheckBox check4;// 银联支付
	@BindView(id = R.id.order_paytype_zhifubao1, click = true)
	private RelativeLayout zhifubaoLy;// 支付宝1
	@BindView(id = R.id.order_paytype_weixin, click = true)
	private RelativeLayout weixinLy;// 微信2
	@BindView(id = R.id.order_paytype_yinhang, click = true)
	private RelativeLayout yinhangLy;// 线下刷卡支付3
	@BindView(id = R.id.order_paytype_yinlian, click = true)
	private RelativeLayout yinlianLy;// 银联支付4

	private String pay_id = "6";// 支付类型 默认支付宝支付

	private OrdePaySaoActivity mContex;
	private String uid;
	// 支付宝
	public static  String ALI_RSA="";
	public static  String ALI_PARTNER="";
	public static  String ALI_SELLER="";

	private static final int SDK_PAY_FLAG = 1;
	private static final int SDK_CHECK_FLAG = 2;

	// 微信
	PayReq req;
	final IWXAPI msgApi = WXAPIFactory.createWXAPI(this, null);
	Map<String, String> resultunifiedorder;
	StringBuffer sb;

	private String server_id;
	private String order_id;
	private String price;
	private String finalPrice;
	private String taoTitle;// 淘整形 标题
	private String order_time;

	@BindView(id = R.id.yuemei_yinsi_ly, click = true)
	private LinearLayout yuemeiYinsiLy;
	private LoadingProgress mDialog;


	@Override
	public void setRootView() {
		setContentView(R.layout.acty_pay_saoorder);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = OrdePaySaoActivity.this;
		uid = Utils.getUid();

		mDialog = new LoadingProgress(mContex);

		req = new PayReq();
		sb = new StringBuffer();
		msgApi.registerApp(Constants.APP_ID);

		initShenHe();

		Intent it = getIntent();
		price = it.getStringExtra("price");
		taoTitle = it.getStringExtra("tao_title");
		server_id = it.getStringExtra("server_id");
		order_id = it.getStringExtra("order_id");
		order_time = it.getStringExtra("order_time");

		finalPrice = price;
		shifuTv.setText("￥" + price);

		check1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if (arg1) {
					pay_id = "6";
					check2.setChecked(false);
					check3.setChecked(false);
					check4.setChecked(false);
				}
			}
		});

		check2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if (arg1) {
					pay_id = "2";
					check1.setChecked(false);
					check3.setChecked(false);
					check4.setChecked(false);
				}
			}
		});

		check3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if (arg1) {
					pay_id = "3";
					check1.setChecked(false);
					check2.setChecked(false);
					check4.setChecked(false);
				}
			}
		});

		check4.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if (arg1) {
					pay_id = "4";
					check1.setChecked(false);
					check2.setChecked(false);
					check3.setChecked(false);
				}
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						OrdePaySaoActivity.this.finish();
					}
				});


	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	void initShenHe() {
		new ShenHeApi().getCallBack(mContex, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {
			@Override
			public void onSuccess(ShenHeData sdata) {
				String shenhe = sdata.getShenhe();

				String alipay = sdata.getAlipay();
				if (alipay.equals("1")) {
					zhifubaoLy.setVisibility(View.GONE);
				} else {
					zhifubaoLy.setVisibility(View.VISIBLE);
				}
				String wxpay = sdata.getWxpay();
				if (wxpay.equals("1")) {
					weixinLy.setVisibility(View.GONE);
				} else {
					weixinLy.setVisibility(View.VISIBLE);
				}

				String bankpay = sdata.getBankpay();
				if (bankpay.equals("1")) {
					yinlianLy.setVisibility(View.GONE);
				} else {
					yinlianLy.setVisibility(View.VISIBLE);
				}
			}

		});
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
			case R.id.order_phone_test_back:
				if (Utils.isFastDoubleClick()) {
					return;
				}
				onBackPressed();
				break;
			case R.id.sumbit_order_bt:
				if (Utils.isFastDoubleClick()) {
					return;
				}
				if (check1.isChecked() || check2.isChecked() || check3.isChecked()
						|| check4.isChecked()) {

					if (pay_id.equals("6")) {// 支付宝
						payALiPay(taoTitle, order_id, finalPrice);
					} else if (pay_id.equals("2")) {// 微信
						payWeixin(taoTitle, order_id, finalPrice);
					} else if (pay_id.equals("4")) {// 银联支付
						payYinlian(taoTitle, order_id, finalPrice);
					} else if (pay_id.equals("3")) {// 线下刷卡支付
						// 下单成功 跳转一个下单成功页即可
						payXianXia();
					}
				} else {
					ViewInject.toast("请选择支付方式");
				}
				break;
			case R.id.order_paytype_zhifubao1:
				check1.setChecked(true);
				check2.setChecked(false);
				check3.setChecked(false);
				check4.setChecked(false);
				break;
			case R.id.order_paytype_yinhang:
				check1.setChecked(false);
				check3.setChecked(true);
				check2.setChecked(false);
				check4.setChecked(false);
				break;
			case R.id.order_paytype_weixin:
				check1.setChecked(false);
				check3.setChecked(false);
				check2.setChecked(true);
				check4.setChecked(false);
				break;
			case R.id.order_paytype_yinlian:
				check1.setChecked(false);
				check3.setChecked(false);
				check2.setChecked(false);
				check4.setChecked(true);
				break;
			case R.id.yuemei_yinsi_ly://悦美隐私政策
				Intent it = new Intent();
				it.setClass(mContex, UserAgreementWebActivity.class);
				startActivity(it);
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
			default:
				// Log.e("activityRes", "resCode:" + resultCode);
				String msg = "";
			/*
			 * 支付控件返回字符串:success、fail、cancel 分别代表支付成功，支付失败，支付取消
			 */
				String str = data.getExtras().getString("pay_result");
				if (str.equalsIgnoreCase("success")) {
					msg = "支付成功！";
					ViewInject.toast(msg);
					Intent it = new Intent();
					it.putExtra("order_id", order_id);
					it.putExtra("price", finalPrice);
					it.setClass(mContex, OrderZhifuSaoSuessActivity.class);
					startActivity(it);
					finish();
				} else if (str.equalsIgnoreCase("fail")) {
					msg = "支付失败！";
					ViewInject.toast(msg);
					Intent it = new Intent();
					it.putExtra("server_id", server_id);
					it.putExtra("order_id", order_id);
					it.putExtra("taotitle", taoTitle);
					it.putExtra("price", price);
					it.putExtra("order_time", order_time);
					it.setClass(getApplicationContext(),
							OrderZhifuSaoFailsActivity.class);
					startActivity(it);
					finish();
				} else if (str.equalsIgnoreCase("cancel")) {
					msg = "用户取消了支付";
					ViewInject.toast(msg);
					Intent it = new Intent();
					it.putExtra("server_id", server_id);
					it.putExtra("order_id", order_id);
					it.putExtra("taotitle", taoTitle);
					it.putExtra("price", price);
					it.putExtra("order_time", order_time);
					it.setClass(getApplicationContext(),
							OrderZhifuSaoFailsActivity.class);
					startActivity(it);
					finish();
				}
				break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 支付宝支付返回结果
	 */
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case SDK_PAY_FLAG: {
					Result resultObj = new Result((String) msg.obj);
					String resultStatus = resultObj.resultStatus;

					String trade_no = "";

					// Log.e(TAG, "resultStatus==" + resultStatus);

					// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
					if (TextUtils.equals(resultStatus, "9000")) {
						Toast.makeText(OrdePaySaoActivity.this, "支付成功",
								Toast.LENGTH_SHORT).show();

						Intent it = new Intent();
						it.putExtra("order_id", order_id);
						it.putExtra("price", finalPrice);
						it.setClass(mContex, OrderZhifuSaoSuessActivity.class);
						startActivity(it);
						finish();
						// sucessZhifu(trade_no);
					} else {
						// 判断resultStatus 为非“9000”则代表可能支付失败
						// “8000”
						// 代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
						if (TextUtils.equals(resultStatus, "8000")) {
							Toast.makeText(OrdePaySaoActivity.this, "支付结果确认中",
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(OrdePaySaoActivity.this, "支付失败",
									Toast.LENGTH_SHORT).show();
						}
						Intent it = new Intent();
						it.putExtra("server_id", server_id);
						it.putExtra("order_id", order_id);
						it.putExtra("taotitle", taoTitle);
						it.putExtra("price", price);
						it.putExtra("order_time", order_time);
						it.setClass(getApplicationContext(),
								OrderZhifuSaoFailsActivity.class);
						startActivity(it);
						finish();
					}
					break;
				}
				case SDK_CHECK_FLAG: {
					Toast.makeText(OrdePaySaoActivity.this, "检查结果为：" + msg.obj,
							Toast.LENGTH_SHORT).show();
					break;
				}
				default:
					break;
			}
		}

    };

	/**
	 * call alipay sdk pay. 调用SDK支付
	 */
	public void pay() {
		String orderInfo = getOrderInfo(taoTitle, taoTitle, finalPrice);
		// String orderInfo = getOrderInfo(taoTitle, taoTitle, "0.01");
		// Log.e(TAG, "orderInfo==" + orderInfo);

		String sign = sign(orderInfo);
		try {
			// 仅需对sign 做URL编码
			sign = URLEncoder.encode(sign, "UTF-8");

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
				+ getSignType();

		Runnable payRunnable = new Runnable() {

			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask alipay = new PayTask(OrdePaySaoActivity.this);
				// 调用支付接口
				String result = alipay.pay(payInfo,true);

				Message msg = new Message();
				msg.what = SDK_PAY_FLAG;
				msg.obj = result;
				mHandler.sendMessage(msg);
			}
		};

		Thread payThread = new Thread(payRunnable);
		payThread.start();
	}

	/**
	 * check whether the device has authentication alipay account.
	 * 查询终端设备是否存在支付宝认证账户
	 */
//	public void check(View v) {
//		Runnable checkRunnable = new Runnable() {
//
//			@Override
//			public void run() {
//				PayTask payTask = new PayTask(OrdePaySaoActivity.this);
//				boolean isExist = payTask.checkAccountIfExist();
//
//				Message msg = new Message();
//				msg.what = SDK_CHECK_FLAG;
//				msg.obj = isExist;
//				mHandler.sendMessage(msg);
//			}
//		};
//
//		Thread checkThread = new Thread(checkRunnable);
//		checkThread.start();
//
//	}

	/**
	 * get the sdk version. 获取SDK版本号
	 */
	public void getSDKVersion() {
		PayTask payTask = new PayTask(this);
		String version = payTask.getVersion();
		Toast.makeText(this, version, Toast.LENGTH_SHORT).show();
	}

	/**
	 * create the order info. 创建订单信息
	 */
	public String getOrderInfo(String subject, String body, String price) {
		// 合作者身份ID
		String orderInfo = "partner=" + "\"" + ALI_PARTNER + "\"";

		// 卖家支付宝账号
		orderInfo += "&seller_id=" + "\"" + ALI_SELLER + "\"";

		// 商户网站唯一订单号
		orderInfo += "&out_trade_no=" + "\"" + order_id + "\"";
		// // 商户网站唯一订单号
		// orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

		// 商品名称
		orderInfo += "&subject=" + "\"" + subject + "\"";

		// 商品详情
		orderInfo += "&body=" + "\"" + body + "\"";

		// 商品金额
		orderInfo += "&total_fee=" + "\"" + price + "\"";

		// 服务器异步通知页面路径
		// orderInfo += "&notify_url=" + "\"" +
		// "http://notify.msp.hk/notify.htm"
		// + "\"";

		orderInfo += "&notify_url=" + "\"" + FinalConstant.SUCESS_ZHIFU
				+ Utils.getTokenStr() + "\"";

		// 接口名称， 固定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";

		// 支付类型， 固定值
		orderInfo += "&payment_type=\"1\"";

		// 参数编码， 固定值
		orderInfo += "&_input_charset=\"utf-8\"";

		// 设置未付款交易的超时时间
		// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		orderInfo += "&it_b_pay=" + "\"" + order_time + "\"";
		// orderInfo += "&it_b_pay=\"30m\"";
		// orderInfo += "&it_b_pay=\"5m\"";

		// 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
		orderInfo += "&return_url=\"m.alipay.com\"";

		// 调用银行卡支付，需配置此参数，参与签名， 固定值
		// orderInfo += "&paymethod=\"expressGateway\"";

		return orderInfo;
	}

	/**
	 * get the out_trade_no for an order. 获取外部订单号
	 */
	public String getOutTradeNo() {
		SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
				Locale.getDefault());
		Date date = new Date();
		String key = format.format(date);

		Random r = new Random();
		key = key + r.nextInt();
		key = key.substring(0, 15);
		return key;
	}

	/**
	 * sign the order info. 对订单信息进行签名
	 *
	 * @param content 待签名订单信息
	 */
	public String sign(String content) {

		// Log.e(TAG, "sign==" + SignUtils.sign(content, RSA_PRIVATE));

		return SignUtils.sign(content, ALI_RSA);
	}

	/**
	 * get the sign type we use. 获取签名方式
	 */
	public String getSignType() {
		return "sign_type=\"RSA\"";
	}


	void  payALiPay(String name, String order_no, String price){
		Map<String,Object>maps=new HashMap<>();
		maps.put("product_name", name);//
		maps.put("order_no", order_no);//
		maps.put("order_price", price);
		new PayALiPay().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {

					try {
						ALiPay aLiPay = new ALiPay();
						ALiPayData alidata = JSONUtil.TransformSingleBean(serverData.data, ALiPayData.class);
						final String payinfo=alidata.getPayInfo();

						Runnable payRunnable = new Runnable() {

							@Override
							public void run() {
								// 构造PayTask 对象
								PayTask alipay = new PayTask(OrdePaySaoActivity.this);
								// 调用支付接口
								String result = alipay.pay(payinfo,true);

								Message msg = new Message();
								msg.what = SDK_PAY_FLAG;
								msg.obj = result;
								mHandler.sendMessage(msg);
							}
						};

						Thread payThread = new Thread(payRunnable);
						payThread.start();

					}catch (Exception e){
						e.printStackTrace();
					}

				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}




	void payYinlian(String name, String order_no, String price) {
		Map<String,Object>maps=new HashMap<>();
		maps.put("product_name", name);//
		maps.put("order_no", order_no);//
		maps.put("order_price", price);
		new PayYinlianApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					PrePayIdData payData = JSONUtil.TransformWXpayidShare(serverData.data);
					String tn = payData.getPrepay_id();

					// Log.e(TAG, "tn===" + tn);

					UPPayAssistEx.startPayByJAR(OrdePaySaoActivity.this,
							PayActivity.class, null, null, tn, "00");

				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	/**
	 * 微信支付
	 *
	 * @param name
	 * @param order_no
	 * @param price
	 */
	void payWeixin(String name, String order_no, String price) {
		Map<String,Object>maps=new HashMap<>();
		maps.put("product_name", name);//
		maps.put("order_no", order_no);//
		maps.put("order_price", price);
		new PayWeixinApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					PrePayId payid = new PrePayId();
					PrePayIdData payData = JSONUtil.TransformWXpayidShare(serverData.data);
					String prepayId = payData.getPrepay_id();

					String orderid = payData.getOrder_id();
					Cfg.saveStr(mContex, "order_id", orderid);
					Cfg.saveStr(aty, "pay_sao", "2");
					Cfg.saveStr(mContex, "price", finalPrice);

					genPayReq(prepayId);
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	private String genNonceStr() {
		Random random = new Random();
		return MD5.getMessageDigest(String.valueOf(random.nextInt(10000))
				.getBytes());
	}

	private long genTimeStamp() {
		return System.currentTimeMillis() / 1000;
	}

	/**
	 * 微信支付
	 *
	 * @param prepay_id
	 */
	private void genPayReq(String prepay_id) {

		// Log.e(TAG, "pre_id==" + prepay_id);

		req.appId = Constants.APP_ID;
		req.partnerId = Constants.MCH_ID;
		req.prepayId = prepay_id;
		req.packageValue = "prepay_id=" + prepay_id;
		req.nonceStr = genNonceStr();
		req.timeStamp = String.valueOf(genTimeStamp());

		List<NameValuePair> signParams = new LinkedList<NameValuePair>();
		signParams.add(new BasicNameValuePair("appid", req.appId));
		signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
		signParams.add(new BasicNameValuePair("package", req.packageValue));
		signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
		signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
		signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));

		// Log.e(TAG, "timestamp==" + req.timeStamp);

		req.sign = genAppSign(signParams);

		sb.append("sign\n" + req.sign + "\n\n");

		// show.setText(sb.toString());

		// Log.e("orion", signParams.toString());

		// 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
		msgApi.registerApp(Constants.APP_ID);
		msgApi.sendReq(req);

		finish();
	}

	private String genAppSign(List<NameValuePair> params) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < params.size(); i++) {
			sb.append(params.get(i).getName());
			sb.append('=');
			sb.append(params.get(i).getValue());
			sb.append('&');
		}
		sb.append("key=");
		sb.append(Constants.API_KEY);

		this.sb.append("sign str\n" + sb.toString() + "\n\n");

		// Log.e(TAG, "sb==" + sb.toString());

		String appSign = MD5.getMessageDigest(sb.toString().getBytes());

		return appSign;
	}

	void payXianXia() {
		Intent it = new Intent();
		it.putExtra("order_id", order_id);
		it.putExtra("price", finalPrice);
		it.setClass(mContex, OrderSaoXiadanActivity.class);
		startActivity(it);

		finish();
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);

	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

}
