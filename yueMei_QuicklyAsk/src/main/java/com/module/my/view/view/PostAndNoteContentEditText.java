package com.module.my.view.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 日记本帖子发布内容
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostAndNoteContentEditText extends android.support.v7.widget.AppCompatEditText {
    private final Context mContext;
    private ClickCallBack clickCallBack;
    private PostAndNoteTitle postAndNoteTitle;
    public static final String EditTitle1 = "请详细描述您的问题，最少20字";
    public static final String EditTitle2 = "在此处写下你的整形体会，手术效果等，你的经验将为其他小伙伴提供很大的帮助";
    public static final String EditTitle3 = "聊聊您的变美心得最少20字";
    public static final String EditTitle4 = "1、说一说为什么想做这个项目\n" +
                                            "2、对自己哪些部位不满意，想怎样改善\n" +
                                            "3、对你的生活有什么影响，写的越详细越容易被选中哦~";
    public static final String EditTitle5 = "体验怎么样？快来写下你的感受分享给小伙伴吧！提交评价即可获得红包和颜值币哦～";
    private int minLength;

    public PostAndNoteContentEditText(Context context) {
        this(context, null);
    }

    public PostAndNoteContentEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PostAndNoteContentEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    /**
     * 设置样式
     */
    private void initView() {
        setGravity(Gravity.TOP);
        setBackground(null);
        setTextColor(Utils.getLocalColor(mContext, R.color._33));
        setTextSize(16);
        setMinLength(20);
        setClickable(true);
        setHintTextColor(Utils.getLocalColor(mContext, R.color._bb));
        setPadding(Utils.dip2px(5), Utils.dip2px(5), Utils.dip2px(5), Utils.dip2px(5));
        LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        editTextParams.weight = 1;
        editTextParams.setMargins(Utils.dip2px(10), Utils.dip2px(10), Utils.dip2px(10), Utils.dip2px(5));
        setLayoutParams(editTextParams);

        //帖子内容编辑框点击
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickCallBack != null) {
                    clickCallBack.onContentEditorClick(v);
                }
            }
        });

        //帖子内容文字变化回调
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (postAndNoteTitle != null) {
                    if (postAndNoteTitle.getTitleLength() >= postAndNoteTitle.getMinLength() && getContentLength() >= getMinLines()) {
                        if (clickCallBack != null) {
                            clickCallBack.onReleaseClick(true);
                        }
                    } else {
                        if (clickCallBack != null) {
                            clickCallBack.onReleaseClick(false);
                        }
                    }
                }
            }
        });
    }

    /**
     * 获取内容
     *
     * @return
     */
    public String getContent() {
        return getText().toString().trim();
    }

    /**
     * 获取内容长度
     *
     * @return
     */
    public int getContentLength() {
        return getText().toString().trim().length();
    }

    /**
     * 设置内容提示语
     *
     * @param hint
     */
    public void setContentHint(String hint) {
        setHint(hint);
    }

    /**
     * 设置帖子标题编辑框
     *
     * @param postAndNoteTitle
     */
    public void setContenteEditText(PostAndNoteTitle postAndNoteTitle) {
        this.postAndNoteTitle = postAndNoteTitle;
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

    /**
     * 设置最少多少个字
     *
     * @param minLength
     */
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    /**
     * 获取最少多少个字
     *
     * @return
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * 帖子内容的回调
     */
    public interface ClickCallBack {
        void onContentEditorClick(View v);

        void onReleaseClick(boolean isClick);
    }
}
