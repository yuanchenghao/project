package com.module.base.view.ui;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.youth.banner.loader.ImageLoader;

/**
 * Created by 裴成浩 on 2019/9/18
 */
public class GlideImageLoader extends ImageLoader {
    private final Context mContext;
    private final int mRadius;
    private String TAG = "GlideImageLoader";

    public GlideImageLoader(Context context,int radius) {
        this.mContext = context;
        this.mRadius = radius;
    }

    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        Log.e(TAG, "path === " + path);
        if (Util.isOnMainThread()) {
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            Glide.with(mContext).load((String) path).transform(new GlideRoundTransform(mContext,mRadius)).placeholder(R.drawable.home_focal_placeholder).error(R.drawable.home_focal_placeholder).into(imageView);
        }

    }
}