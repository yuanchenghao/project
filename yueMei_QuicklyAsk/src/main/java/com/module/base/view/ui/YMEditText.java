package com.module.base.view.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;

/**
 * 优化后的EditText
 * Created by 裴成浩 on 2018/9/5.
 */
public class YMEditText extends android.support.v7.widget.AppCompatEditText {
    public YMEditText(Context context) {
        this(context, null);
    }

    public YMEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public YMEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {

        //帖子内容编辑框点击
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickCallBack != null) {
                    clickCallBack.onContentEditorClick(v);
                }
            }
        });

        //帖子内容文字变化回调
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (clickCallBack != null) {
                    clickCallBack.onReleaseClick(getLength());
                }
            }
        });
    }

    /**
     * 获取内容
     *
     * @return
     */
    public String getContent() {
        return getText().toString().trim();
    }

    /**
     * 获取内容长度
     *
     * @return
     */
    public int getLength() {
        return getText().toString().trim().length();
    }

    /**
     * 默认是否获取焦点
     *
     * @param flag
     */
    public void setEditTextState(boolean flag) {
        setCursorVisible(flag);
        setFocusable(flag);
        setFocusableInTouchMode(flag);
    }

    /**
     * 帖子内容的回调
     */

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onContentEditorClick(View v);

        void onReleaseClick(int length);
    }

    /**
     * 设置回调
     *
     * @param clickCallBack
     */
    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

}
