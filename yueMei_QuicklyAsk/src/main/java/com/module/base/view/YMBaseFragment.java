package com.module.base.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lzy.okgo.OkGo;
import com.module.home.view.LoadingProgress;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.taobao.weex.utils.WXViewToImageUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by 裴成浩 on 2018/6/14.
 */
public abstract class YMBaseFragment extends Fragment implements View.OnClickListener{
    protected static final String TAG = YMBaseFragment.class.getSimpleName();
    protected Activity mContext;
    protected LayoutInflater mInflater; //初始化获取布局类
    protected LoadingProgress mDialog;  //旋转等待
    protected int statusbarHeight;      //状态栏高度
    protected int windowsWight;         //屏幕宽度
    protected int windowsHeight;        //屏幕高度
    protected View mView;               //布局
    private Unbinder mUnbinder;         //解除绑定使用
    protected FunctionManager mFunctionManager;     //方法管理器

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mInflater = LayoutInflater.from(mContext);
        mDialog = new LoadingProgress(mContext);
        //状态栏高度
        statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);

        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
        windowsHeight = metric.heightPixels;

        //获取方法管理器
        mFunctionManager = new FunctionManager(mContext);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(getLayoutId(), container, false);
        mUnbinder = ButterKnife.bind(this, mView);

        initView(mView);
        initData(mView);

        //获取方法管理器
        mFunctionManager = new FunctionManager(mContext);

        return mView;
    }

    @Override
    public void onDestroyView() {
        mUnbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
    }

    /**
     * 提供加载的布局的方法
     *
     * @return ：布局引用
     */
    protected abstract int getLayoutId();

    /**
     * 初始化UI
     */
    protected abstract void initView(View view);

    /**
     * 初始化内容数据
     */
    protected abstract void initData(View view);


    /**
     * 设置多点击监听器
     *
     * @param views
     */
    protected void setMultiOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    /**
     * 设置布局的fragment
     */
    protected void setActivityFragment(int id, Fragment fragment) {
        if (!isAdded()) {
            return;
        }
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(id, fragment);
        transaction.commitAllowingStateLoss();
    }
}
