package com.module.base.view;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/3/12
 */
public abstract class YMRecyclerViewAdapter<E, VH extends YMViewHolder> extends RecyclerView.Adapter<VH> {
    protected Context mContext;
    protected List<E> mData;
    private OnItemClickListener<E> mOnItemClickListener;
    protected static final String TAG = YMRecyclerViewAdapter.class.getSimpleName();
    public FunctionManager mFunctionManager;

    protected YMRecyclerViewAdapter(@Nullable List<E> data) {
        this.mData = data == null ? new ArrayList<E>() : data;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (this.mContext == null) {
            this.mContext = parent.getContext();
        }
        if (this.mFunctionManager == null) {
            this.mFunctionManager = new FunctionManager(mContext);
        }
        View view = LayoutInflater.from(mContext).inflate(findResById(), parent, false);
        final VH baseViewHolder = createBaseViewHolder(view);
        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, baseViewHolder.getLayoutPosition(), mData.get(baseViewHolder.getLayoutPosition()));
                }
            }
        });

        baseViewHolder.setAdapter(this);
        return baseViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        convert(holder, getItemByPosition(position));
    }

    /**
     * 重写此方法，解决更新单个item时闪烁的问题
     *
     * @param holder
     * @param position
     * @param payloads
     */
    @Override
    public void onBindViewHolder(@NonNull VH holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else { //更新控件
            updateView(holder, position, payloads);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    /**
     * 添加数据，在原有数据的末尾继续添加新的数据（加载更多/下拉刷新）
     *
     * @param data     待添加的数据
     * @param isDelete 是否删除原有的数据
     */
    public void updateData(List<E> data, boolean isDelete) {
        if (data == null)
            return;
        int oldSize = mData.size();
        int newSize = data.size();
        if (isDelete) { //mData清空的时候要通知adapter也将数据清空，否则在更新数据的时候会报错
            mData.clear();
            notifyItemRangeRemoved(0, oldSize);
        }
        mData.addAll(data);
        notifyItemRangeInserted(oldSize, oldSize + newSize);
    }

    /**
     * 删除列表中指定位置的item
     *
     * @param position 指定删除的位置
     */
    public void removeItem(int position) {
        if (mData.size() == 0)
            return;
        if (position >= mData.size())
            return;
        mData.remove(position);
        notifyItemRemoved(position);
        //删除一个item后，通知adapter去刷新position否则会出现位置错乱
        notifyItemRangeChanged(0, mData.size());
    }

    /**
     * 更新列表指定位置的数据
     *
     * @param position 数据更新的位置
     * @param data     需要更新的数据
     * @param obj      不为空时将调用{@link # updateView(RecyclerView.ViewHolder, int, List)}方法更新指定的控件，
     *                 否则将更新整个item，此时会有闪烁的问题
     */
    public void updateItem(int position, E data, @Nullable Object obj) {
        if (mData.size() == 0)
            return;
        if (position > mData.size())
            return;
        if (position < mData.size()) {
            mData.remove(position);
        }
        mData.add(position, data);
        notifyItemChanged(position, obj);
    }

    /**
     * item局部刷新
     *
     * @param holder
     * @param position
     * @param payloads
     */
    protected void updateView(VH holder, int position, @NonNull List<Object> payloads) {

    }

    /**
     * 获取单条数据
     *
     * @param position
     * @return
     */
    protected E getItemByPosition(int position) {
        return mData.get(position);
    }

    protected abstract int findResById();

    protected abstract void convert(VH helper, E item);

    public void setOnItemClickListener(OnItemClickListener<E> listener) {
        mOnItemClickListener = listener;
    }

    /**
     * RecyclerView的item点击事件接口
     */
    public interface OnItemClickListener<E> {
        void onItemClick(View view, int position, E data);
    }

    /**
     * 获取BaseViewHolder对象
     *
     * @param view view
     * @return new ViewHolder
     */
    @SuppressWarnings("unchecked")
    private VH createBaseViewHolder(View view) {
        Class temp = getClass();
        Class z = null;
        while (z == null && null != temp) {
            z = getInstancedGenericKClass(temp);
            temp = temp.getSuperclass();
        }
        VH k;
        // 泛型擦除会导致z为null
        if (z == null) {
            k = (VH) new YMViewHolder(view);
        } else {
            k = createGenericKInstance(z, view);
        }
        return k != null ? k : (VH) new YMViewHolder(view);
    }

    /**
     * 创建泛型VH实例
     *
     * @param z
     * @param view
     * @return
     */
    @SuppressWarnings("unchecked")
    private VH createGenericKInstance(Class z, View view) {
        try {
            Constructor constructor;
            if (z.isMemberClass() && !Modifier.isStatic(z.getModifiers())) {
                constructor = z.getDeclaredConstructor(getClass(), View.class);
                constructor.setAccessible(true);
                return (VH) constructor.newInstance(this, view);
            } else {
                constructor = z.getDeclaredConstructor(View.class);
                constructor.setAccessible(true);
                return (VH) constructor.newInstance(view);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 得到泛型参数 VH
     *
     * @param z
     * @return
     */
    private Class getInstancedGenericKClass(Class z) {
        Type type = z.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            Type[] types = ((ParameterizedType) type).getActualTypeArguments();
            for (Type temp : types) {
                if (temp instanceof Class) {
                    Class tempClass = (Class) temp;
                    if (BaseViewHolder.class.isAssignableFrom(tempClass)) {
                        return tempClass;
                    }
                } else if (temp instanceof ParameterizedType) {
                    Type rawType = ((ParameterizedType) temp).getRawType();
                    if (rawType instanceof Class && BaseViewHolder.class.isAssignableFrom((Class<?>) rawType)) {
                        return (Class<?>) rawType;
                    }
                }
            }
        }
        return null;
    }
}

