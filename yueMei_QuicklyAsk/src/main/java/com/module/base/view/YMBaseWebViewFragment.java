package com.module.base.view;

import android.annotation.SuppressLint;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;

import java.util.Map;

/**
 * Created by 裴成浩 on 2018/6/14.
 */
public abstract class YMBaseWebViewFragment extends YMBaseFragment {

    protected ScrollWebView mWebView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        mWebView = new ScrollWebView(mContext);

        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
        mWebView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mWebView.setHorizontalScrollBarEnabled(false);              //水平滚动条不显示
        mWebView.setVerticalScrollBarEnabled(false);                //垂直滚动条不显示
        mWebView.setLongClickable(true);
        mWebView.setScrollbarFadingEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.setDrawingCacheEnabled(true);
        mWebView.setLayerType(View.LAYER_TYPE_NONE, null);
        mWebView.requestFocus();

        WebSettings settings = mWebView.getSettings();
        settings.setNeedInitialFocus(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.supportMultipleWindows();
        settings.setBuiltInZoomControls(true);
        settings.setAllowFileAccess(true);
        settings.setJavaScriptEnabled(true);                                //设置webview支持javascript
        settings.setLoadsImagesAutomatically(true);                         //支持自动加载图片
        settings.setUseWideViewPort(true);                                  //设置webview推荐使用的窗口，使html界面自适应屏幕
        settings.setLoadWithOverviewMode(true);
        settings.setGeolocationEnabled(true);
        settings.setSaveFormData(true);                                     //设置webview保存表单数据
        settings.setSavePassword(true);                                     //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);            //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);                                      //支持缩放

        settings.setSupportMultipleWindows(true);
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url) {
                onYmLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return ymShouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                onYmPageFinished(view, url);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                onYmReceivedSslError(view, handler, error);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                onYmReceivedError(view, request, error);
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return onYmJsAlert(view, url, message, result);
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return onYmJsConfirm(view, url, message, result);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                onYmProgressChanged(view, newProgress);
            }
        });
    }

    protected void onYmLoadResource(WebView view, String url) {

    }

    protected boolean ymShouldOverrideUrlLoading(WebView view, String request) {
        return false;
    }

    protected void onYmPageFinished(WebView view, String url) {

    }

    protected void onYmReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

    }

    protected void onYmReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

    }


    /**
     * onJsAlert回调
     *
     * @param view
     * @param url
     * @param message
     * @param result
     * @return
     */
    protected boolean onYmJsAlert(WebView view, String url, String message, JsResult result) {
        return true;
    }

    /**
     * onJsConfirm回调
     *
     * @param view
     * @param url
     * @param message
     * @param result
     * @return
     */
    protected boolean onYmJsConfirm(WebView view, String url, String message, JsResult result) {
        return true;
    }

    /**
     * onProgressChanged回调
     *
     * @param view
     * @param newProgress
     */
    protected void onYmProgressChanged(WebView view, int newProgress) {
    }

    /**
     * 加载webView
     *
     * @param url
     * @param paramMap
     * @param headMap
     */
    protected void loadUrl(String url, Map<String, Object> paramMap, Map<String, Object> headMap) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, paramMap, headMap);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    protected void loadUrl(String url, Map<String, Object> paramMap) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, paramMap);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    protected void loadUrl(String url) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

}
