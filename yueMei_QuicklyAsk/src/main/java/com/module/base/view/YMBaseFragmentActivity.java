package com.module.base.view;

import android.annotation.SuppressLint;
import android.view.View;

/**
 * 公共的FragmentActiviy
 * Created by 裴成浩 on 2018/3/30.
 */

@SuppressLint("Registered")
public class YMBaseFragmentActivity extends YMBaseActivity {

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public void onClick(View v) {

    }
}
