package com.module.base.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

/**
 * 悦美共有管理器接口
 * Created by 裴成浩 on 2019/3/12
 */
public interface ManagerCallback {
    void goToActivity(Class activity);
    void goToActivity(Class activity, Bundle bundle);
    void showShort(String msg);
    void setTextValue(View v, int id, String txt);
    void setImageSrc(ImageView imageView, String url);
    void setPlaceholderImageSrc(ImageView imageView, String url);
    void setImgSrc(ImageView imageView, int src);
    void setCircleImageSrc(ImageView imageView, String url);
    void setRoundImageSrc(ImageView imageView, String url, int round);
    void setRoundImageSrc(ImageView imageView, int drawable, int round);
    void setImgBackground(ImageView imageView, int background);
    int getLocalColor(int color);
    Drawable getLocalDrawable(int drawable);
    int setCustomColor(String color);
    String loadStr(String key, String defStr);
    void saveStr(String key, String value);
    int loadInt(String key, int defVal);
    void saveInt(String key, int value);
    void clear();
    int getWindowWidth();
    int getWindowheight();
}
