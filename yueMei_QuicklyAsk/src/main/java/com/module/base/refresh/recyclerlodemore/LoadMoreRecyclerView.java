package com.module.base.refresh.recyclerlodemore;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.module.base.refresh.loadmore.LoadingMoreFooter;
import com.module.base.refresh.loadmore.ProgressView;

import java.util.ArrayList;
import java.util.List;

/**
 * 可以添加头的RecyclerView
 * Created by 裴成浩 on 2018/7/13.
 */
public class LoadMoreRecyclerView extends RecyclerView {

    private Context mContext;
    private boolean isLoadingData = false;
    private View mFootView;        //底部加载更多View
    private WrapAdapter mWrapAdapter;   //adpater
    private final RecyclerView.AdapterDataObserver mDataObserver = new DataObserver();
    private View mEmptyView;
    private boolean loadingMoreEnabled = true;
    private static final int TYPE_FOOTER = 10001;
    private ArrayList<LoadMoreData> mHeaderViews = new ArrayList<>();
    private static final int HEADER_INIT_INDEX = 10002;
    private CustomFooterViewCallBack footerViewCallBack;
    private LoadMoreListener mLoadingListener;
    private ProgressView progressView;
    private String TAG = "LoadMoreRecyclerView";

    public LoadMoreRecyclerView(Context context) {
        this(context, null);
    }

    public LoadMoreRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadMoreRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        init();
    }

    private void init() {
        //设置自定义加载中效果
        LoadingMoreFooter footView = new LoadingMoreFooter(mContext);
        progressView = new ProgressView(mContext);
        footView.addFootLoadingView(progressView);
        mFootView = footView;
        mFootView.setVisibility(GONE);

        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                startLoadMore();
            }
        });
    }

    @Deprecated
    public void setEmptyView(View emptyView) {
        this.mEmptyView = emptyView;
        mDataObserver.onChanged();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        mWrapAdapter = new WrapAdapter(adapter);
        super.setAdapter(mWrapAdapter);
        adapter.registerAdapterDataObserver(mDataObserver);
        mDataObserver.onChanged();
    }

    /**
     * 避免用户自己调用getAdapter() 引起的ClassCastException
     *
     * @return
     */
    @Override
    public Adapter getAdapter() {
        if (mWrapAdapter != null) {
            return mWrapAdapter.getOriginalAdapter();
        } else {
            return null;
        }
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);
        if (mWrapAdapter != null) {
            if (layout instanceof GridLayoutManager) {
                final GridLayoutManager gridManager = ((GridLayoutManager) layout);
                gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return (mWrapAdapter.isHeader(position) || mWrapAdapter.isFooter(position)) ? gridManager.getSpanCount() : 1;
                    }
                });
            }
        }
    }

    /**
     * ===================== 当你调用下面这些函数时，试着调整Recycler的位置 ======================
     */
    public <T> void notifyItemRemoved(List<T> listData, int position) {
        if (mWrapAdapter.adapter == null) return;
        int headerSize = getHeaders_includingRefreshCount();
        int adjPos = position + headerSize;
        mWrapAdapter.adapter.notifyItemRemoved(adjPos);
        mWrapAdapter.adapter.notifyItemRangeChanged(headerSize, listData.size(), new Object());
    }

    public <T> void notifyItemInserted(List<T> listData, int position) {
        if (mWrapAdapter.adapter == null) return;
        int headerSize = getHeaders_includingRefreshCount();
        int adjPos = position + headerSize;
        mWrapAdapter.adapter.notifyItemInserted(adjPos);
        mWrapAdapter.adapter.notifyItemRangeChanged(headerSize, listData.size(), new Object());
    }

    public void notifyItemChanged(int position) {
        if (mWrapAdapter.adapter == null) return;
        int adjPos = position + getHeaders_includingRefreshCount();
        mWrapAdapter.adapter.notifyItemChanged(adjPos);
    }

    public void notifyItemChanged(int position, Object o) {
        if (mWrapAdapter.adapter == null) return;
        int adjPos = position + getHeaders_includingRefreshCount();
        mWrapAdapter.adapter.notifyItemChanged(adjPos, o);
    }

    public int getHeaders_includingRefreshCount() {
        return mWrapAdapter.getHeadersCount() + 1;
    }

    /*** ======================================================= end =======================================================*/

    /**
     * 开始加载更多
     */
    private void startLoadMore() {
        LayoutManager layoutManager = getLayoutManager();
        int lastVisibleItemPosition;
        if (layoutManager instanceof GridLayoutManager) {
            lastVisibleItemPosition = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            int[] into = new int[((StaggeredGridLayoutManager) layoutManager).getSpanCount()];
            ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(into);
            lastVisibleItemPosition = findMax(into);
            if (backTopListener != null) {
                backTopListener.onBackTop();
            }

        } else {
            lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        }

        int adjAdapterItemCount = layoutManager.getItemCount() + getHeaders_includingRefreshCount();

        if (layoutManager.getChildCount() > 0 && lastVisibleItemPosition >= adjAdapterItemCount - 1 && adjAdapterItemCount >= layoutManager.getChildCount() && !isLoadingData) {
            isLoadingData = true;
            if (mFootView instanceof LoadingMoreFooter) {
                ((LoadingMoreFooter) mFootView).setVisible();
                progressView.rotatingAnimation();
            } else {
                if (footerViewCallBack != null) {
                    footerViewCallBack.onLoadingMore(mFootView);
                }
            }
            if(mLoadingListener != null){
                mLoadingListener.onLoadMore();
            }
        }

    }

    private int findMax(int[] lastPositions) {
        int max = lastPositions[0];
        for (int value : lastPositions) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }

    private class DataObserver extends RecyclerView.AdapterDataObserver {
        @Override
        public void onChanged() {
            if (mWrapAdapter != null) {
                mWrapAdapter.notifyDataSetChanged();
            }
            if (mWrapAdapter != null && mEmptyView != null) {
                int emptyCount = 1 + mWrapAdapter.getHeadersCount();
                if (loadingMoreEnabled) {
                    emptyCount++;
                }
                if (mWrapAdapter.getItemCount() == emptyCount) {
                    mEmptyView.setVisibility(View.VISIBLE);
                    setVisibility(View.GONE);
                } else {
                    mEmptyView.setVisibility(View.GONE);
                    setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            mWrapAdapter.notifyItemRangeInserted(positionStart, itemCount);
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            mWrapAdapter.notifyItemRangeChanged(positionStart, itemCount);
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            mWrapAdapter.notifyItemRangeChanged(positionStart, itemCount, payload);
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            mWrapAdapter.notifyItemRangeRemoved(positionStart, itemCount);
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            mWrapAdapter.notifyItemMoved(fromPosition, toPosition);
        }
    }

    private class WrapAdapter extends RecyclerView.Adapter<ViewHolder> {

        private RecyclerView.Adapter adapter;

        public WrapAdapter(RecyclerView.Adapter adapter) {
            this.adapter = adapter;
        }

        public RecyclerView.Adapter getOriginalAdapter() {
            return this.adapter;
        }

        public boolean isHeader(int position) {
            if (mHeaderViews == null) {
                return false;
            } else {
                return position >= 0 && position < mHeaderViews.size();
            }
        }

        public boolean isFooter(int position) {
            if (loadingMoreEnabled) {
                return position == getItemCount() - 1;
            } else {
                return false;
            }
        }

        public int getHeadersCount() {
            if (mHeaderViews == null) {
                return 0;
            } else {
                return mHeaderViews.size();
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Log.e(TAG, "33333");
            if (isHeaderType(viewType)) {
                Log.e(TAG, "111");
                return new WrapAdapter.SimpleViewHolder(getHeaderViewByType(viewType));
            } else if (viewType == TYPE_FOOTER) {
                Log.e(TAG, "22222");
                return new WrapAdapter.SimpleViewHolder(mFootView);
            }
            return adapter.onCreateViewHolder(parent, viewType);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (isHeader(position)) {
                return;
            }
            int adjPosition = position - getHeadersCount();
            int adapterCount;
            if (adapter != null) {
                adapterCount = adapter.getItemCount();
                if (adjPosition < adapterCount) {
                    adapter.onBindViewHolder(holder, adjPosition);
                }
            }
        }

        /**
         * 有些时候我们需要重写这个
         */
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
            if (isHeader(position)) {
                return;
            }

            int adjPosition = position - getHeadersCount();
            int adapterCount;
            if (adapter != null) {
                adapterCount = adapter.getItemCount();
                if (adjPosition < adapterCount) {
                    if (payloads.isEmpty()) {
                        adapter.onBindViewHolder(holder, adjPosition);
                    } else {
                        adapter.onBindViewHolder(holder, adjPosition, payloads);
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            int adjLen = (loadingMoreEnabled ? 1 : 0);
            if (adapter != null) {
                return getHeadersCount() + adapter.getItemCount() + adjLen;
            } else {
                return getHeadersCount() + adjLen;
            }
        }

        @Override
        public int getItemViewType(int position) {
            Log.e(TAG, "position == " + position);
            int adjPosition = position - getHeadersCount();
            if (isHeader(position)) {
                return mHeaderViews.get(position).getPos();
            }
            if (isFooter(position)) {
                return TYPE_FOOTER;
            }
            int adapterCount;

            if (adapter != null) {
                adapterCount = adapter.getItemCount();
                if (adjPosition < adapterCount) {
                    Log.e(TAG, "adjPosition == " + adjPosition);
                    int type = adapter.getItemViewType(adjPosition);
                    if (isReservedItemViewType(type)) {
                        throw new IllegalStateException("RecyclerView require itemViewType in adapter should be less than 10000 ");
                    }
                    return type;
                }
            }
            return 0;
        }

        @Override
        public long getItemId(int position) {
            if (adapter != null && position >= getHeadersCount() + 1) {
                int adjPosition = position - (getHeadersCount() + 1);
                if (adjPosition < adapter.getItemCount()) {
                    return adapter.getItemId(adjPosition);
                }
            }
            return -1;
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
            RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
            if (manager instanceof GridLayoutManager) {
                final GridLayoutManager gridManager = ((GridLayoutManager) manager);
                gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return (isHeader(position) || isFooter(position)) ? gridManager.getSpanCount() : 1;
                    }
                });
            }
            adapter.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            adapter.onDetachedFromRecyclerView(recyclerView);
        }

        @Override
        public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
            super.onViewAttachedToWindow(holder);

            if (getLayoutManager() instanceof StaggeredGridLayoutManager && (isHeader(holder.getLayoutPosition())) || isFooter(holder.getLayoutPosition())) {

                ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
                boolean bbbb = lp instanceof StaggeredGridLayoutManager.LayoutParams;
                Log.e("66666666", "ccc === " + bbbb);
                if (lp != null && lp instanceof StaggeredGridLayoutManager.LayoutParams) {
                    StaggeredGridLayoutManager.LayoutParams p = (StaggeredGridLayoutManager.LayoutParams) lp;
                    p.setFullSpan(true);
                }
            }
            adapter.onViewAttachedToWindow(holder);
        }

        @Override
        public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
            adapter.onViewDetachedFromWindow(holder);
        }

        @Override
        public void onViewRecycled(RecyclerView.ViewHolder holder) {
            adapter.onViewRecycled(holder);
        }

        @Override
        public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
            return adapter.onFailedToRecycleView(holder);
        }

        @Override
        public void unregisterAdapterDataObserver(AdapterDataObserver observer) {
            adapter.unregisterAdapterDataObserver(observer);
        }

        @Override
        public void registerAdapterDataObserver(AdapterDataObserver observer) {
            adapter.registerAdapterDataObserver(observer);
        }

        private class SimpleViewHolder extends RecyclerView.ViewHolder {
            public SimpleViewHolder(View itemView) {
                super(itemView);
            }
        }
    }

    /**
     * 判断一个type是否为HeaderType
     *
     * @param itemViewType
     * @return
     */
    private boolean isHeaderType(int itemViewType) {
        if (mHeaderViews == null) {
            return false;
        } else {
            for (LoadMoreData data : mHeaderViews) {
                if (mHeaderViews.size() > 0 && data.getPos() == itemViewType) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * 添加头布局
     *
     * @param view
     */
    public void addHeaderView(View view) {
        if (mHeaderViews == null) {
            return;
        }
        LoadMoreData mHeader = new LoadMoreData();
        mHeader.setPos(HEADER_INIT_INDEX + mHeaderViews.size());
        mHeader.setView(view);
        mHeaderViews.add(mHeader);
        Log.e(TAG, "mHeader == " + mHeader.getPos());
        Log.e(TAG, "mHeader == " + mHeader.getView());
        if (mWrapAdapter != null) {
            mWrapAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 删除头布局
     *
     * @param view
     */
    public void removeHeaderView(View view) {
        if (mHeaderViews == null || view == null) {
            return;
        }
        int pos = -1;
        for (int i = 0; i < mHeaderViews.size(); i++) {
            LoadMoreData data = mHeaderViews.get(i);
            if (data.getView() == view) {
                mHeaderViews.remove(i);
                pos = i;
                for (LoadMoreData aaa : mHeaderViews) {
                    aaa.setPos(aaa.getPos() - 1);
                }
                break;
            }
        }

        if (mWrapAdapter != null) {
            if (pos >= 0) {
                mWrapAdapter.notifyItemRemoved(pos);
            }
        }
    }

    /**
     * 根据header的ViewType判断是哪个header
     *
     * @param itemType
     * @return
     */
    private View getHeaderViewByType(int itemType) {
        if (!isHeaderType(itemType)) {
            return null;
        }
        if (mHeaderViews == null) return null;
        return mHeaderViews.get(itemType - HEADER_INIT_INDEX).getView();
    }

    /**
     * 判断是否是RecyclerView保留的itemViewType
     *
     * @param itemViewType
     * @return
     */
    private boolean isReservedItemViewType(int itemViewType) {
        for (LoadMoreData data : mHeaderViews) {
            if (data.getPos() == itemViewType) {
                return true;
            }
        }

        return itemViewType == TYPE_FOOTER;
    }

    /**
     * 设置其他样式的加载样式
     *
     * @param view
     * @param footerViewCallBack
     */
    public void setFootView(@NonNull final View view, @NonNull CustomFooterViewCallBack footerViewCallBack) {
        mFootView = view;
        this.footerViewCallBack = footerViewCallBack;
    }

    /**
     * 加载更多加载中的样式
     */
    public void loadMoreComplete() {
        isLoadingData = false;
        if (mFootView instanceof LoadingMoreFooter) {
            ((LoadingMoreFooter) mFootView).setGone();
        } else {
            if (footerViewCallBack != null) {
                footerViewCallBack.onLoadMoreComplete(mFootView);
            }
        }
    }

    /**
     * 没有更多的样式
     *
     * @param noMore
     */
    public void setNoMore(boolean noMore) {
        isLoadingData = noMore;
        if (mFootView instanceof LoadingMoreFooter) {
            ((LoadingMoreFooter) mFootView).setEnd();
        } else {
            if (footerViewCallBack != null) {
                footerViewCallBack.onSetNoMore(mFootView, noMore);
            }
        }
    }

    public void setLoadMoreListener(LoadMoreListener listener) {
        mLoadingListener = listener;
    }

    public interface LoadMoreListener {
        void onLoadMore();
    }

    private BackTopListener backTopListener;

    public interface BackTopListener {
        void onBackTop();
    }

    public void setBackTopListener(BackTopListener backTopListener) {
        this.backTopListener = backTopListener;
    }

    private class LoadMoreData {
        private View view;
        private Integer pos;

        public View getView() {
            return view;
        }

        public void setView(View view) {
            this.view = view;
        }

        public Integer getPos() {
            return pos;
        }

        public void setPos(Integer pos) {
            this.pos = pos;
        }
    }

    /**
     * 设置是否需要下拉加载更多
     *
     * @param loadingMoreEnabled
     */
    public void setLoadingMoreEnabled(boolean loadingMoreEnabled) {
        this.loadingMoreEnabled = loadingMoreEnabled;
    }
}
