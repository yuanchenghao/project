package com.module.base.refresh.refresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.qmuiteam.qmui.widget.pullRefreshLayout.QMUIFollowRefreshOffsetCalculator;
import com.qmuiteam.qmui.widget.pullRefreshLayout.QMUIPullRefreshLayout;

/**
 * 自己的下拉刷新控件
 * Created by 裴成浩 on 2018/2/23.
 */

public class MyPullRefresh extends QMUIPullRefreshLayout {

    private RefreshListener refreshListener;

    public MyPullRefresh(Context context) {
        this(context, null);
    }

    public MyPullRefresh(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyPullRefresh(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        //设置下拉后位置
        setRefreshOffsetCalculator(new QMUIFollowRefreshOffsetCalculator());

        setOnPullListener(new OnPullListener() {
            @Override
            public void onMoveTarget(int offset) {

            }

            @Override
            public void onMoveRefreshView(int offset) {

            }

            @Override
            public void onRefresh() {
                if (refreshListener != null) {
                    refreshListener.onRefresh();
                }
            }
        });
    }

    /**
     * 设置自定义的刷新头
     *
     * @return
     */
    @Override
    protected View createRefreshView() {
        return new CustomRefresh1(getContext());
    }


    /**
     * @param listener
     */
    public void setRefreshListener(RefreshListener listener) {
        this.refreshListener = listener;
    }

}
