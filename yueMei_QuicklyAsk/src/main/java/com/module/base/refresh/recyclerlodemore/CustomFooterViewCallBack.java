package com.module.base.refresh.recyclerlodemore;

import android.view.View;

/**
 * 加载更多接口
 * Created by 裴成浩 on 2018/7/13.
 */
public interface CustomFooterViewCallBack {
    void onLoadingMore(View yourFooterView);

    void onLoadMoreComplete(View yourFooterView);

    void onSetNoMore(View yourFooterView, boolean noMore);
}
