package com.module.base.refresh.refresh;

/**
 * Created by 裴成浩 on 2018/7/13.
 */
public interface RefreshListener {
    void onRefresh();
}
