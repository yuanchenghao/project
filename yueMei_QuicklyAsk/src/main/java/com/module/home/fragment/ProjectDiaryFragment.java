package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.UserClickData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.ProjectDiaryAdapter;
import com.module.home.model.api.ProjectDetailsApi;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.home.view.DiaryScreeningView;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/2/22
 */
public class ProjectDiaryFragment extends YMBaseFragment {

    private String TAG = "ProjectDiaryFragment";
    @BindView(R.id.project_diary_refresh)
    SmartRefreshLayout mDiaryRefresh;
    @BindView(R.id.project_diary_rec)
    RecyclerView mDiaryRecycler;
    @BindView(R.id.project_diary_screening)
    DiaryScreeningView mDiaryScreening;
    @BindView(R.id.project_diary_refresh_more)
    YMLoadMore mDiaryRefreshMore;
    @BindView(R.id.fragment_project_details_not)
    NestedScrollView mNotData;

    private ProjectDetailsApi mProjectDetailsApi;
    private ArrayList<BBsListData550> mFragmentData;
    private ProjectDiaryAdapter mProjectDiaryAdapter;
    private int mPage = 1;
    private int mTempPos = -1;
    private ProjectDetailsBean detailsBean;
    private String sortId = "1";

    public static ProjectDiaryFragment newInstance(ArrayList<BBsListData550> datas, ProjectDetailsBean bean) {
        ProjectDiaryFragment fragment = new ProjectDiaryFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("bean", bean);
        bundle.putParcelableArrayList("datas", datas);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_details_diary;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            detailsBean = getArguments().getParcelable("bean");
            mFragmentData = getArguments().getParcelableArrayList("datas");
        }

        Log.e(TAG, "mFragmentData == " + mFragmentData);

        //上拉加载更多
        mDiaryRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mDiaryRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                getProjectDetailsData();
            }
        });

        //筛选点击
        mDiaryScreening.setOnClickCallBackListener(new DiaryScreeningView.OnClickCallBackListener() {
            @Override
            public void onClick(String id) {
                sortId = id;
                refresh();
            }
        });
    }

    @Override
    protected void initData(View view) {
        mProjectDetailsApi = new ProjectDetailsApi();
        if (mFragmentData.size() == 0) {
            getProjectDetailsData();
        } else {
            mPage++;
            mDiaryRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mProjectDiaryAdapter = new ProjectDiaryAdapter(mContext, mFragmentData);
            mDiaryRecycler.setAdapter(mProjectDiaryAdapter);

            mProjectDiaryAdapter.setOnItemPersonClickListener(new ProjectDiaryAdapter.OnItemPersonClickListener() {
                @Override
                public void onItemClick(int pos) {
                    mTempPos = pos;
                    String appmurl = mProjectDiaryAdapter.getDatas().get(pos).getAppmurl();
                    HashMap<String, String> event_params = mProjectDiaryAdapter.getDatas().get(pos).getEvent_params();
                    if (!TextUtils.isEmpty(appmurl)) {

                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "channel|sharelist_" + Utils.getCity() + "_" + getTagId() + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE + ""), event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                    }
                }

                @Override
                public void onItemPersonClick(String id, int pos) {
                    mTempPos = pos;
                    Intent intent = new Intent(mContext, PersonCenterActivity641.class);
                    intent.putExtra("id", id);
                    startActivityForResult(intent, 18);
                }
            });
        }
    }

    /**
     * 获取页面数据
     */
    private void getProjectDetailsData() {
        Log.e(TAG, "parentLabelID == " + detailsBean.getTwoLabelId());
        Log.e(TAG, "labelID == " + detailsBean.getFourLabelId());
        Log.e(TAG, "mHomeSource == " + detailsBean.getHomeSource());

        mProjectDetailsApi.addData("parentLabelID", detailsBean.getTwoLabelId());
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId())) {
            mProjectDetailsApi.addData("labelID", detailsBean.getFourLabelId());
        }
        mProjectDetailsApi.addData("listType", "4");
        mProjectDetailsApi.addData("homeSource", detailsBean.getHomeSource());
        mProjectDetailsApi.addData("page", mPage + "");
        mProjectDetailsApi.addData("sort", sortId);
        mProjectDetailsApi.getCallBack(mContext, mProjectDetailsApi.getProjectDetailsHashMap(), new BaseCallBackListener<ProjectDetailsData>() {
            @Override
            public void onSuccess(ProjectDetailsData data) {
                mPage++;
                if (data.getDoctorsList().size() == 0) {
                    mDiaryRefresh.finishLoadMoreWithNoMoreData();
                } else {
                    mDiaryRefresh.finishLoadMore();
                }

                if (mProjectDiaryAdapter == null) {
                    if (data.getDiaryList().size() != 0) {
                        mDiaryRefresh.setVisibility(View.VISIBLE);
                        mNotData.setVisibility(View.GONE);

                        setRecycler(data.getDiaryList());
                    } else {
                        mDiaryRefresh.setVisibility(View.GONE);
                        mNotData.setVisibility(View.VISIBLE);
                    }

                } else {
                    mProjectDiaryAdapter.addData(data.getDiaryList());
                }

                Log.e(TAG, "data == " + data.getDoctorsList().size());
            }
        });
    }

    private void setRecycler(ArrayList<BBsListData550> diaryList) {
        mDiaryRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mProjectDiaryAdapter = new ProjectDiaryAdapter(mContext, diaryList);
        mDiaryRecycler.setAdapter(mProjectDiaryAdapter);

        mProjectDiaryAdapter.setOnItemPersonClickListener(new ProjectDiaryAdapter.OnItemPersonClickListener() {
            @Override
            public void onItemClick(int pos) {
                mTempPos = pos;
                String appmurl = mProjectDiaryAdapter.getDatas().get(pos).getAppmurl();
                if (!TextUtils.isEmpty(appmurl)) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "channel|sharelist_" + Utils.getCity() + "_" + getTagId() + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE + ""));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                }
            }

            @Override
            public void onItemPersonClick(String id, int pos) {
                UserClickData userClickData = mProjectDiaryAdapter.getDatas().get(pos).getUserClickData();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_SHARELIST_USER_CLICK, (1 + pos) + ""), userClickData.getEvent_params(), new ActivityTypeData("100"));
                mTempPos = pos;
                Intent intent = new Intent(mContext, PersonCenterActivity641.class);
                intent.putExtra("id", id);
                startActivityForResult(intent, 18);
            }
        });
    }

    /**
     * 获取选中标签
     *
     * @return
     */
    private String getTagId() {
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId()) && !"0".equals(detailsBean.getFourLabelId())) {
            return detailsBean.getFourLabelId();
        } else {
            return detailsBean.getTwoLabelId();
        }
    }


    /**
     * 四级标签点击，刷新数据
     *
     * @param bbsList
     * @param bean
     */
    public void setRefreshData(ArrayList<BBsListData550> bbsList, ProjectDetailsBean bean) {
        if (mProjectDetailsApi != null) {
            detailsBean = bean;
            mFragmentData = bbsList;

            refresh();
        }
    }

    /**
     * 刷新数据
     */
    private void refresh() {
        mPage = 1;
        mProjectDiaryAdapter = null;
        getProjectDetailsData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (mTempPos >= 0) {
                isFocu(mProjectDiaryAdapter.getmHotIssues().get(mTempPos).getUser_id());
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                mProjectDiaryAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }

}
