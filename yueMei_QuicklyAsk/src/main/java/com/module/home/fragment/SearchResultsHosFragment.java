package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.doctor.model.bean.HosListData;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.ProjectHosAdapter;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData5;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/4/17
 */
public class SearchResultsHosFragment extends YMBaseFragment {

    @BindView(R.id.search_results_hos_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.search_results_hos_refresh)
    public SmartRefreshLayout mRefresh;
    @BindView(R.id.search_results_hos_recycler)
    RecyclerView mRecycler;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView hosNotView;

    private LodHotIssueDataApi lodHotIssueDataApi;
    private String TAG = "SearchResultsHosFragment";

    private String mKey;
    private String mType;
    private int mPage = 1;
    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private String mSort = "1";                                 //排序的选中id
    private ProjectHosAdapter projectHosAdapter;
    private Gson mGson;

    public static SearchResultsHosFragment newInstance(String key,String type) {
        SearchResultsHosFragment fragment = new SearchResultsHosFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_hos_view;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mType = getArguments().getString("type");

        mScreen.initView(false);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener1() {
            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                    }
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }
        });

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
    }

    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();
        mGson = new Gson();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(10), Utils.getLocalColor(mContext, R.color._f6)));
        mRecycler.setLayoutManager(linearLayoutManager);

        cityPop = new BaseCityPopwindows(mContext, mScreen);
        setSortData();
        loadingData();

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }
                refresh();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));
        lodHotIssueDataApi.addData("type", mType);
        lodHotIssueDataApi.addData("page", mPage + "");
        lodHotIssueDataApi.addData("sort", mSort);

        lodHotIssueDataApi.getCallBack(mContext, lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if(onEventClickListener != null){
                    onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
                }
                Log.e(TAG, "JSON1:==" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        SearchResultData5 searchResultData5 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData5.class);
                        List<HosListData> lvHotIssueData = searchResultData5.getList();
                        final String searchHitBoardId = searchResultData5.getSearch_hit_board_id();
                        //刷新隐藏
                        mRefresh.finishRefresh();
                        if (lvHotIssueData.size() == 0) {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        } else {
                            mRefresh.finishLoadMore();
                        }

                        if (mPage == 1 && lvHotIssueData.size() == 0) {
                            hosNotView.setVisibility(View.VISIBLE);
                            mRefresh.setVisibility(View.GONE);
                        } else {
                            hosNotView.setVisibility(View.GONE);
                            mRefresh.setVisibility(View.VISIBLE);

                            if (projectHosAdapter == null) {
                                projectHosAdapter = new ProjectHosAdapter(mContext, lvHotIssueData);
                                mRecycler.setAdapter(projectHosAdapter);
                                projectHosAdapter.setOnEventClickListener(new ProjectHosAdapter.OnEventClickListener() {
                                    @Override
                                    public void onItemClick(View v,HosListData data, int pos) {
                                        String hosid = data.getHos_id();
                                        HashMap<String, String> event_params = data.getEvent_params();
                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH, "hospitallist|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE), event_params, new ActivityTypeData("44"));
                                        Intent it = new Intent(mContext, HosDetailActivity.class);
                                        it.putExtra("hosid", hosid);
                                        if (!TextUtils.isEmpty(searchHitBoardId)){
                                            it.putExtra("search_hit_board_id", searchHitBoardId);
                                        }
                                        mContext.startActivity(it);
                                    }
                                });
                            } else {
                                projectHosAdapter.addData(lvHotIssueData);
                            }
                        }

                        mPage++;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    hosNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 排序数据
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("7");
        a2.setName("离我最近");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("5");
        a4.setName("案例数多");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("8");
        a5.setName("评分高");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a2);
        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (sortPop != null) {
                    sortPop.dismiss();
                    mSort = sortId;
                    mScreen.initSortView(sortPop.isShowing());
                    mScreen.setSortTitle(sortName);
                }
                refresh();
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    /**
     * 刷新
     */
    private void refresh() {
        projectHosAdapter = null;
        mPage = 1;
        loadingData();
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}