package com.module.home.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.TaoScreenTitleData;
import com.module.commonview.view.CenterAlignImageSpan;
import com.module.commonview.view.MyURLSpan;
import com.module.commonview.view.TaoScreenTitleView;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.controller.adapter.TaoListStaggeredAdapter;
import com.module.community.model.bean.TaoListData;
import com.module.community.model.bean.TaoListDataEnum;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.model.bean.TaoListDoctors;
import com.module.community.model.bean.TaoListDoctorsCompared;
import com.module.community.model.bean.TaoListDoctorsEnum;
import com.module.community.model.bean.TaoListType;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.other.StaggeredItemDecoretion;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.FilterDataApi;
import com.module.home.model.api.SearchShowApi;
import com.module.home.model.bean.SearchActivityData;
import com.module.home.model.bean.SearchResultCompare;
import com.module.home.model.bean.SearchResultLike;
import com.module.home.model.bean.SearchResultTaoData2;
import com.module.home.model.bean.SearchTao;
import com.module.home.view.SearchResultsTaoTopView;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.my.view.view.RoundBackgroundColorSpan1;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.view.adapter.SearhBoardRecyclerAdapter;
import com.module.taodetail.view.adapter.SearhMethodRecyclerAdapter;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.entity.SearchResultMethod;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 相关商品页面
 * Created by 裴成浩 on 2019/4/17
 */
public class SearchResultsTaoFragment extends YMBaseFragment {
    public String TAG = "SearchResultsTaoFragment";
    @BindView(R.id.search_results_tao_screen)
    TaoScreenTitleView mScreen;
    @BindView(R.id.search_results_tao_refresh)
    public SmartRefreshLayout mRefresh;

    @BindView(R.id.search_results_tao_recycler)
    LoadMoreRecyclerView mRecycler;          //淘列表
    @BindView(R.id.search_results_not_view)
    NestedScrollView taoNotView;

    @BindView(R.id.search_results_tao_screening_view)
    LinearLayout tagScreening;

    //四级方法列表
    @BindView(R.id.search_results_tao_tag_container)
    LinearLayout tagContainer;
    @BindView(R.id.search_results_tao_tag_recycler)
    RecyclerView tagRecycler;

    //方法列表
    @BindView(R.id.search_results_tao_methods_container)
    LinearLayout methodsContainer;
    @BindView(R.id.search_results_tao_methods_recycler)
    RecyclerView methodsRecycler;
    @BindView(R.id.search_results_tao_methods_pk)
    ImageView methodsPk;

    //活动列表
    @BindView(R.id.search_results_tao_activity_container)
    LinearLayout activityContainer;
    @BindView(R.id.search_results_tao_activity_recycler)
    RecyclerView activityRecycler;

    //头部提示数据
    @BindView(R.id.search_results_tao_change_word_title)
    TextView changeWordTitle;

    //大促
    @BindView(R.id.search_results_tao_sales_view)
    SearchResultsTaoTopView salesView;

    private BaseNetWorkCallBackApi lodHotIssueDataApi;
    private int mPage = 1;
    private String mKey;
    private String mType;

    private String searchMethod;                                    //方法
    private SearchResultBoard searchActivity;                       //活动
    private SearchResultBoard mActiveLeftData;                      //大促左
    private SearchResultBoard mActiveRightData;                     //大促右
    private TaoListAdapter taoListAdapter;
    private TaoListStaggeredAdapter taoListStaggeredAdapter;
    private FilterDataApi filterDataApi;
    private SearhMethodRecyclerAdapter mTagRecyclerAdapter;
    private SearhMethodRecyclerAdapter mMethodRecyclerAdapter;
    private SearhBoardRecyclerAdapter mBoardRecyclerAdapter;
    private Gson mGson;
    private String not_err_word;                                    //如果是任搜索点击执行的搜索传1
    private BaseWebViewClientMessage webViewClientManager;
    private int lastVisibleItemPosition;                            //可显示的最后一个数据
    private String hospital_id;
    private SearchResultTaoData2 mSearchResultTaoData;
    private MyRecyclerViewDivider mMyRecyclerViewDivider;
    private StaggeredItemDecoretion mStaggeredItemDecoretion;
    private int  mIsChange = -1; //1 双侧 0 单侧
    private int[] mLastVisibleItemPositions;

    public static SearchResultsTaoFragment newInstance(String key, String type, String hospital_id) {
        SearchResultsTaoFragment fragment = new SearchResultsTaoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        bundle.putString("hospital_id", hospital_id);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.search_results_tao_view;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mKey = getArguments().getString("key");
            mType = getArguments().getString("type");
            hospital_id = getArguments().getString("hospital_id");
        }

        //关闭上拉加载更多
//        mRecycler.setLoadingMoreEnabled(false);

        //刷新和加载更多
//        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        mRecycler.setLoadMoreListener(new LoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadingData();
            }
        });

//        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
//                loadingData();
//            }
//
//            @Override
//            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
//                refresh();
//            }
//        });

        if (TextUtils.isEmpty(hospital_id)) {
            if (taoListAdapter != null) {
                taoListAdapter.isHeadView(false);
            }
            mScreen.setVisibility(View.VISIBLE);
            mScreen.initView(true);
        } else {
            mScreen.setVisibility(View.GONE);
        }

    }


    @SuppressLint("NewApi")
    @Override
    protected void initData(View view) {
        if (TextUtils.isEmpty(hospital_id)) {
            lodHotIssueDataApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "index613");
        } else {
            lodHotIssueDataApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "hossearch");
        }

        filterDataApi = new FilterDataApi();
        mGson = new Gson();

        setSortData();
        loadKindData();
        loadingData();

        //筛选刷新
        mScreen.setOnEventClickListener(new TaoScreenTitleView.OnEventClickListener() {
            @Override
            public void onScreenClick() {
                refresh();
            }

            @Override
            public void onChangeClick(boolean isChange) {//切换按钮回调
                if (isChange){
                    if (mSearchResultTaoData != null){
                        mIsChange = 0;
                        Cfg.saveInt(mContext,"is_bilateral",0);
                        HashMap<String, String> switchEventParams = mSearchResultTaoData.getSwitch_event_params();
                        YmStatistics.getInstance().tongjiApp(switchEventParams);
                        if (mStaggeredItemDecoretion != null){
                            mRecycler.removeItemDecoration(mStaggeredItemDecoretion);
                        }
                        setTaoAdapter(mSearchResultTaoData);
                        taoListAdapter.refreshData(null,mSearchResultTaoData);
                        refresh();
                        saveBilateralData();
                        taoListStaggeredAdapter=null;
                    }
                }else {
                    if (mSearchResultTaoData != null){
                        mIsChange = 1;
                        Cfg.saveInt(mContext,"is_bilateral",1);
                        HashMap<String, String> switchEventParams = mSearchResultTaoData.getSwitch_event_params();
                        YmStatistics.getInstance().tongjiApp(switchEventParams);
                        if (mMyRecyclerViewDivider != null){
                            mRecycler.removeItemDecoration(mMyRecyclerViewDivider);
                        }
                        setStaggeredAdapter(mSearchResultTaoData);
                        taoListStaggeredAdapter.refreshData(mSearchResultTaoData);
                        refresh();
                        saveData();
                        taoListAdapter=null;

                    }

                }
            }

        });

    }

    @Override
    public void onPause() {
        saveData();
        saveBilateralData();
        super.onPause();
    }

    /**
     * 获取筛选的数据
     */
    private void loadKindData() {
        filterDataApi.getCallBack(mContext, filterDataApi.getHashMap(), new BaseCallBackListener<ArrayList<ProjcetData>>() {
            @Override
            public void onSuccess(ArrayList<ProjcetData> data) {
                if(mScreen != null){
                    mScreen.setScreeData(data);
                }
            }

        });
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        TaoScreenTitleData taoScreenTitleData = mScreen.getTaoScreenTitleData();

        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));
        lodHotIssueDataApi.addData("type", mType);
        lodHotIssueDataApi.addData("page", mPage + "");
        lodHotIssueDataApi.addData("sort", taoScreenTitleData.getSort());
        lodHotIssueDataApi.addData("homepagecity", taoScreenTitleData.getHomepagecity());

        if (!TextUtils.isEmpty(hospital_id)) {
            lodHotIssueDataApi.addData("id", hospital_id);
        }

        Log.e(TAG, "mKey == " + mKey);
        Log.e(TAG, "mPage == " + mPage);
        Log.e(TAG, "homepagecity == " + taoScreenTitleData.getHomepagecity());
        Log.e(TAG, "mSort == " + taoScreenTitleData.getSort());

        //筛选
        for (ProjcetList data : taoScreenTitleData.getKindStr()) {
            Log.e(TAG, data.getPostName() + ":" + data.getPostVal());
            lodHotIssueDataApi.addData(data.getPostName(), data.getPostVal());
        }

        //方法
        Log.e(TAG, "searchMethod == " + searchMethod);
        if (!TextUtils.isEmpty(searchMethod)) {
            lodHotIssueDataApi.addData("search_method", searchMethod);
        }
        //活动
        Log.e(TAG, "searchActivity == " + searchActivity);
        if (searchActivity != null) {
            lodHotIssueDataApi.addData(searchActivity.getPostName(), searchActivity.getPostVal());
        }

        //大促左边
        if (mActiveLeftData != null) {
            Log.e(TAG, "左Name === " + mActiveLeftData.getPostName());
            Log.e(TAG, "左Val === " + mActiveLeftData.getPostVal());
            lodHotIssueDataApi.addData(mActiveLeftData.getPostName(), mActiveLeftData.getPostVal());
        }

        //大促右边
        if (mActiveRightData != null) {
            Log.e(TAG, "右Name === " + mActiveLeftData.getPostName());
            Log.e(TAG, "右Val === " + mActiveLeftData.getPostVal());
            lodHotIssueDataApi.addData(mActiveRightData.getPostName(), mActiveRightData.getPostVal());
        }

        //如果是任搜索点击执行的搜索传1
        if (!TextUtils.isEmpty(not_err_word)) {
            lodHotIssueDataApi.addData("not_err_word", not_err_word);
            not_err_word = "0";
        }

        lodHotIssueDataApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                if (getActivity() != null) {
                    setResultData(serverData);
                    if (onEventClickListener != null) {
                        onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
                    }
                }
            }
        });
    }

    /**
     * 设置返回数据
     *
     * @param serverData
     */
    private void setResultData(ServerData serverData) {
        if ("1".equals(serverData.code)) {
            mSearchResultTaoData = JSONUtil.TransformSingleBean(serverData.data, SearchResultTaoData2.class);
            List<SearchResultMethod> search_partChild = mSearchResultTaoData.getSearch_partChild();          //四级标签方法数据
            List<SearchResultMethod> search_method = mSearchResultTaoData.getSearch_method();                //方法数据
            List<SearchResultBoard> screen_board = mSearchResultTaoData.getScreen_board();                   //活动数据
            List<SearchResultBoard> screen_active = mSearchResultTaoData.getScreen_active();                 //大促数据

            int total = Integer.parseInt(mSearchResultTaoData.getTotal());
            String list_tips = mSearchResultTaoData.getList_tips();
            String search_key = mSearchResultTaoData.getSearch_key();
            SearchResultCompare baikeCompare = mSearchResultTaoData.getBaikeCompare();

            Log.e(TAG, "total == " + total);
            Log.e(TAG, "list_tips == " + list_tips);
            Log.e(TAG, "search_key == " + search_key);

            //刷新加载样式结束
            mRefresh.finishRefresh();
            if (mSearchResultTaoData.getList().size() == 0) {
                mRefresh.finishLoadMoreWithNoMoreData();
                mRecycler.setNoMore(true);
            } else {
                mRefresh.finishLoadMore();
                mRecycler.loadMoreComplete();
            }

            //设置大促数据
            if (screen_active != null && screen_active.size() > 0) {
                salesView.setVisibility(View.VISIBLE);
                tagScreening.setVisibility(View.GONE);
                if (salesView.isReLoading(screen_active)) {
                    salesView.initView(screen_active);
                    salesView.setOnEventClickListener(new SearchResultsTaoTopView.OnEventClickListener() {
                        @Override
                        public void onEventClick(SearchResultBoard dataLeft, SearchResultBoard dataRight) {

                            //点击左侧
                            if (dataLeft != null && mActiveLeftData == null) {
                                YmStatistics.getInstance().tongjiApp(dataLeft.getEvent_params());
                            }

                            //点击右侧
                            if (dataRight != null && mActiveRightData == null) {
                                YmStatistics.getInstance().tongjiApp(dataRight.getEvent_params());
                            }

                            mActiveLeftData = dataLeft;
                            mActiveRightData = dataRight;

                            refresh();
                        }
                    });
                }
            } else {
                salesView.setVisibility(View.GONE);
                tagScreening.setVisibility(View.VISIBLE);

                //设置四级签方法列表
                if (search_partChild != null) {
                    Log.e(TAG, "search_partChild == " + search_partChild.size());
                    setTagList(search_partChild);
                }

                //设置方法列表
                if (search_method != null) {
                    Log.e(TAG, "search_method == " + search_method.size());
                    setMethodList(search_method, baikeCompare);
                }

                //设置活动列表
                if (screen_board != null) {
                    Log.e(TAG, "screen_board == " + screen_board.size());
                    setBoardList(screen_board);
                }
            }

            //设置头部提示
            setTopPrompt(list_tips, search_key);

            //设置淘数据
            setTaoData(mSearchResultTaoData);

            //页码+1
            mPage++;
        } else {
            taoNotView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 四级签方法列表
     *
     * @param search_partChild
     */
    private void setTagList(List<SearchResultMethod> search_partChild) {
        if (mTagRecyclerAdapter == null) {
            if (search_partChild.size() > 0) {
                tagContainer.setVisibility(View.VISIBLE);
                //四级签方法列表设置
                LinearLayoutManager tagLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                tagRecycler.setLayoutManager(tagLinearLayoutManager);
                DefaultItemAnimator itemAnimator = (DefaultItemAnimator) tagRecycler.getItemAnimator();
                if (itemAnimator != null){
                    itemAnimator.setSupportsChangeAnimations(false);
                }
                mTagRecyclerAdapter = new SearhMethodRecyclerAdapter(mContext, search_partChild, true);
                tagRecycler.setAdapter(mTagRecyclerAdapter);
                mTagRecyclerAdapter.setOnItemClickListener(new SearhMethodRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(SearchResultMethod data) {
                        searchAgain(data.getTitle());
                    }
                });
            } else {
                tagContainer.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 设置方法列表
     *
     * @param search_method
     * @param baikeCompare
     */
    private void setMethodList(List<SearchResultMethod> search_method, final SearchResultCompare baikeCompare) {
        if (mMethodRecyclerAdapter == null) {
            if (search_method.size() > 0) {
                methodsContainer.setVisibility(View.VISIBLE);
                //方法列表设置
                LinearLayoutManager methodsLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                methodsRecycler.setLayoutManager(methodsLinearLayoutManager);
                DefaultItemAnimator itemAnimator = (DefaultItemAnimator) methodsRecycler.getItemAnimator();
                if (itemAnimator != null){
                    itemAnimator.setSupportsChangeAnimations(false);
                }
                mMethodRecyclerAdapter = new SearhMethodRecyclerAdapter(mContext, search_method, false);
                methodsRecycler.setAdapter(mMethodRecyclerAdapter);
                mMethodRecyclerAdapter.setOnItemClickListener(new SearhMethodRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(SearchResultMethod data) {
                        if (data != null) {
                            searchMethod = data.getId();
                        } else {
                            searchMethod = "";
                        }
                        refresh();
                    }
                });
            } else {
                methodsContainer.setVisibility(View.GONE);
            }

            //PK是否显示
            if (baikeCompare != null) {
                String img = baikeCompare.getImg();
                if (!TextUtils.isEmpty(img)) {
                    methodsPk.setVisibility(View.VISIBLE);
                    mFunctionManager.setImageSrc(methodsPk, img);
                    //性价比pk点击
                    methodsPk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (webViewClientManager == null) {
                                webViewClientManager = new BaseWebViewClientMessage(mContext);
                            }
                            Log.e(TAG, "baikeCompare.getUrl() == " + baikeCompare.getUrl());
                            YmStatistics.getInstance().tongjiApp(baikeCompare.getEvent_params());
                            webViewClientManager.showWebDetail(baikeCompare.getUrl());
                        }
                    });
                } else {
                    methodsPk.setVisibility(View.GONE);
                }
            } else {
                methodsPk.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 设置活动列表
     *
     * @param screen_board
     */
    private void setBoardList(List<SearchResultBoard> screen_board) {
        if (mBoardRecyclerAdapter == null) {
            if (screen_board.size() > 0) {
                activityContainer.setVisibility(View.VISIBLE);
                //活动列表设置
                LinearLayoutManager activityLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                activityRecycler.setLayoutManager(activityLinearLayoutManager);
                DefaultItemAnimator itemAnimator = (DefaultItemAnimator) activityRecycler.getItemAnimator();
                if (itemAnimator != null){
                    itemAnimator.setSupportsChangeAnimations(false);
                }
                mBoardRecyclerAdapter = new SearhBoardRecyclerAdapter(mContext, screen_board);
                activityRecycler.setAdapter(mBoardRecyclerAdapter);
                mBoardRecyclerAdapter.setOnEventClickListener(new SearhBoardRecyclerAdapter.OnEventClickListener() {
                    @Override
                    public void onItemClick(SearchResultBoard data) {
                        searchActivity = data;
                        refresh();
                    }
                });
            } else {
                activityContainer.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 设置头部提示
     *
     * @param list_tips
     * @param search_key
     */
    private void setTopPrompt(String list_tips, String search_key) {
        if (!TextUtils.isEmpty(list_tips)) {
            changeWordTitle.setVisibility(View.VISIBLE);
            //设置开始的图片
            SpannableString spanString;

            String tips = "占位 " + list_tips;
            if (!TextUtils.isEmpty(search_key)) {
                tips = tips + "  ";
                spanString = new SpannableString(tips + search_key);
            } else {
                spanString = new SpannableString(tips);
            }

            //设置前面小灯泡
            CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mContext, Utils.zoomImage(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.item_search_null_bulb), Utils.dip2px(15), Utils.dip2px(15)));
            spanString.setSpan(imageSpan, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            //设置文字后标签
            if (!TextUtils.isEmpty(search_key)) {
                //设置文字后标签背景
                RoundBackgroundColorSpan1 colorSpan = new RoundBackgroundColorSpan1(Utils.getLocalColor(mContext, R.color._f6), Utils.getLocalColor(mContext, R.color._33), Utils.dip2px(14), Utils.dip2px(8));
                spanString.setSpan(colorSpan, tips.length(), tips.length() + search_key.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                //设置字体大小
                RelativeSizeSpan sizeSpan = new RelativeSizeSpan(0.85f);
                spanString.setSpan(sizeSpan, tips.length(), tips.length() + search_key.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                //设置文字后标签点击
                MyURLSpan myURLSpan = new MyURLSpan(search_key, Utils.getLocalColor(mContext, R.color._33));
                spanString.setSpan(myURLSpan, tips.length(), tips.length() + search_key.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                //设置文字后标签点击回调
                myURLSpan.setOnClickListener(new MyURLSpan.OnClickListener() {
                    @Override
                    public void onClick(View arg0, String search_key) {
                        Log.e(TAG, "search_key === " + search_key);
                        if (search_key.equals(mKey)) {
                            not_err_word = "1";
                        } else {
                            not_err_word = "0";
                        }
                        searchAgain(search_key);
                    }
                });
            }
            changeWordTitle.setText(spanString);
            changeWordTitle.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            changeWordTitle.setVisibility(View.GONE);
        }
    }

    /**
     * 设置淘数据
     *
     * @param searchResultTaoData
     */
    private void setTaoData(SearchResultTaoData2 searchResultTaoData) {
        List<SearchTao> homeTaoLists = searchResultTaoData.getList();                                 //淘数据

        if (TextUtils.isEmpty(hospital_id)) {
            List<SearchTao> recomendList = searchResultTaoData.getRecomend_list();                        //推荐数据
            //淘列表设置
            if (homeTaoLists != null && recomendList != null) {
                Log.e(TAG, "homeTaoLists == " + homeTaoLists.size());
                Log.e(TAG, "recomendList == " + recomendList.size());
                if (homeTaoLists.size() == 0 && recomendList.size() == 0 && mPage == 1) {
                    if (taoListAdapter != null) {
                        taoListAdapter.refreshData(null,null);
                    }
                    taoNotView.setVisibility(View.VISIBLE);
                } else {
                    taoNotView.setVisibility(View.GONE);
                    //是否是双侧流 1 是
                    int is_bilateral = Cfg.loadInt(mContext, "is_bilateral", 0);
//                    int is_bilateral = searchResultTaoData.getIs_bilateral();

                    if (mIsChange != -1){
                        if (mIsChange == 1){
                            setStaggeredAdapter(searchResultTaoData);
                        }else {
                            setTaoAdapter(searchResultTaoData);
                        }
                    }else {
                        if (is_bilateral == 1){
                            mScreen.initChangeView(true);
                            setStaggeredAdapter(searchResultTaoData);
                        }else {
                            mScreen.initChangeView(false);
                            setTaoAdapter(searchResultTaoData);
                        }
                    }

                }
            } else {
                if (taoListAdapter != null) {
                    taoListAdapter.refreshData(null,null);
                }
                taoNotView.setVisibility(View.VISIBLE);
            }
        } else {
            if (homeTaoLists != null) {
                if (homeTaoLists.size() == 0 && mPage == 1) {
                    if (taoListAdapter != null) {
                        taoListAdapter.refreshData(null,null);
                    }
                    taoNotView.setVisibility(View.VISIBLE);
                } else {
                    taoNotView.setVisibility(View.GONE);
                    setTaoAdapter(searchResultTaoData);
                }
            } else {
                if (taoListAdapter != null) {
                    taoListAdapter.refreshData(null,null);
                }
                taoNotView.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 设置普通adapter
     *
     * @param searchResultTaoData
     */
    private void setTaoAdapter(SearchResultTaoData2 searchResultTaoData) {
        List<SearchTao> homeTaoLists = searchResultTaoData.getList();                                 //淘数据
        List<SearchTao> recomendList = searchResultTaoData.getRecomend_list();                                                                                                            //推荐数据
        if (taoListAdapter == null) {

            //淘整形列表设置
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            mRecycler.setLayoutManager(linearLayoutManager);
            mMyRecyclerViewDivider = new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color.subscribe_item_drag_bg));
            mRecycler.addItemDecoration(mMyRecyclerViewDivider);
            DefaultItemAnimator itemAnimator = (DefaultItemAnimator) mRecycler.getItemAnimator();
            if (itemAnimator != null){
                itemAnimator.setSupportsChangeAnimations(false);
            }
            taoListAdapter = new TaoListAdapter(mContext, searchResultTaoData,true);
            mRecycler.setAdapter(taoListAdapter);

            taoListAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(TaoListDataType taoData, int pos) {
                    HomeTaoData data = taoData.getSearchTao().getTao();
                    if (taoData.getType() == TaoListDataEnum.RECOMMENDED) {
                        HashMap<String, String> event_params = data.getEvent_params();
                        event_params.put("show_style","692_oneside");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_RECOMEND, (pos + 1) + ""),event_params , new ActivityTypeData("39"));
                    } else {
                        HashMap<String, String> event_params = data.getEvent_params();
                        event_params.put("show_style","692_oneside");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_TAO, (pos + 1) + ""),event_params , new ActivityTypeData("39"));
                    }
                    Intent it1 = new Intent(getActivity(), TaoDetailActivity.class);
                    it1.putExtra("id", data.get_id());
                    it1.putExtra("source", "2");
                    it1.putExtra("objid", "0");
                    startActivity(it1);
                }
            });

            taoListAdapter.setOnLikeClickListener(new TaoListAdapter.OnLikeClickListener() {
                @Override
                public void onLikeClick(int position, SearchResultLike data) {
                    searchAgain(data.getName());
                }
            });

            //咨询统计
            taoListAdapter.setOnDoctorChatlickListener(new TaoListAdapter.OnDoctorChatlickListener() {
                @Override
                public void onDoctorChatClick(HashMap<String, String> event_params) {
                    event_params.put("show_style","692_oneside");
                    YmStatistics.getInstance().tongjiApp(event_params);

                }

                @Override
                public void onChangeClick(TaoListDoctorsCompared compared, int changePos) {
                    TaoListDoctorsEnum doctorsEnum = compared.getDoctorsEnum();
                    HashMap<String, String> changeOneEventParams = compared.getChangeOneEventParams();
                    changeOneEventParams.put("show_style","692_oneside");
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARED_CHANGE_ONE_CLICK, (changePos + 1) + "", (doctorsEnum == TaoListDoctorsEnum.TOP ? "0" : "1")), changeOneEventParams);
                }
            });
            mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        int lastItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                        Log.e(TAG, "lastItemPosition == " + lastItemPosition);
                        Log.e(TAG, "lastVisibleItemPosition == " + lastVisibleItemPosition);
                        if (lastItemPosition > lastVisibleItemPosition) {
                            lastVisibleItemPosition = lastItemPosition;
                            Log.e(TAG, "lastVisibleItemPosition == " + lastVisibleItemPosition);
                        }
                    }
                }

            });




        } else {
            if (mPage == 1) {
                //刷新数据
                taoListAdapter.refreshData(null,searchResultTaoData);
            } else {
                //加载更多
                taoListAdapter.addDataSearch(homeTaoLists, recomendList);
            }
        }
    }


    /**
     * 瀑布流适配器
     * @param searchResultTaoData
     */
    private void setStaggeredAdapter(SearchResultTaoData2 searchResultTaoData){
        List<SearchTao> homeTaoLists = searchResultTaoData.getList();                                 //淘数据
        List<SearchTao> recomendList = searchResultTaoData.getRecomend_list();
        if (taoListStaggeredAdapter == null) {
            //淘整形列表设置
            final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
            staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
            mRecycler.setItemAnimator(null);
            mRecycler.setLayoutManager(staggeredGridLayoutManager);

            taoListStaggeredAdapter = new TaoListStaggeredAdapter(mContext, searchResultTaoData);
            mRecycler.setAdapter(taoListStaggeredAdapter);

            taoListStaggeredAdapter.setOnItemClickListener(new TaoListStaggeredAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(TaoListDataType taoData, int pos) {
                    HomeTaoData data = taoData.getSearchTao().getTao();
                    if (taoData.getType() == TaoListDataEnum.RECOMMENDED) {
                        HashMap<String, String> event_params = data.getEvent_params();
                        event_params.put("show_style","692_bilateral");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_RECOMEND, (pos + 1) + ""),event_params, new ActivityTypeData("39"));
                    } else {
                        HashMap<String, String> event_params = data.getEvent_params();
                        event_params.put("show_style","692_bilateral");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_TAO, (pos + 1) + ""),event_params , new ActivityTypeData("39"));
                    }
                    Intent it1 = new Intent(getActivity(), TaoDetailActivity.class);
                    it1.putExtra("id", data.get_id());
                    it1.putExtra("source", "2");
                    it1.putExtra("objid", "0");
                    startActivity(it1);
                }
            });


            //咨询统计
            taoListStaggeredAdapter.setOnDoctorChatlickListener(new TaoListStaggeredAdapter.OnDoctorChatlickListener() {
                @Override
                public void onDoctorChatClick(HashMap<String, String> event_params) {
                    event_params.put("show_style","692_bilateral");
                    YmStatistics.getInstance().tongjiApp(event_params);

                }

                @Override
                public void onChangeClick(TaoListDoctorsCompared compared, int changePos) {
                    TaoListDoctorsEnum doctorsEnum = compared.getDoctorsEnum();
                    HashMap<String, String> changeOneEventParams = compared.getChangeOneEventParams();
                    changeOneEventParams.put("show_style","692_bilateral");
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARED_CHANGE_ONE_CLICK, (changePos + 1) + "", (doctorsEnum == TaoListDoctorsEnum.TOP ? "0" : "1")), changeOneEventParams);
                }
            });
            mStaggeredItemDecoretion = new StaggeredItemDecoretion();
            mRecycler.addItemDecoration(mStaggeredItemDecoretion);
            mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        int[] endPos = new int[staggeredGridLayoutManager.getSpanCount()];
                        mLastVisibleItemPositions = staggeredGridLayoutManager.findLastVisibleItemPositions(endPos);
                        for (int i = 0; i < mLastVisibleItemPositions.length; i++) {
                            Log.e(TAG,"lastVisibleItemPositions =="+ mLastVisibleItemPositions[i]);
                        }

//                        Log.e(TAG, "lastItemPosition == " + lastItemPosition);
//                        Log.e(TAG, "lastVisibleItemPosition == " + lastVisibleItemPosition);
//                        if (lastItemPosition > lastVisibleItemPosition) {
//                            lastVisibleItemPosition = lastItemPosition;
//                        }
                    }
                }

            });

        } else {
            if (mPage == 1) {
                //刷新数据
                taoListStaggeredAdapter.refreshData(searchResultTaoData);
            } else {
                //加载更多
                taoListStaggeredAdapter.addData(homeTaoLists, recomendList);
            }
        }
    }

    /**
     * 排序数据
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("综合");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("3");
        a2.setName("价格从低到高");
        TaoPopItemData a3 = new TaoPopItemData();
        a3.set_id("2");
        a3.setName("价格从高到低");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("7");
        a6.setName("离我最近");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a2);
        lvSortData.add(a3);
        lvSortData.add(a6);
        mScreen.setSortData(lvSortData);
    }

    /**
     * 重新搜索
     *
     * @param key :重新搜索的关键词
     */
    private void searchAgain(String key) {
        mKey = key;
        searchActivity = null;              //清除当前选中的方法数据
        searchActivity = null;              //清除当前选中的活动数据
        mTagRecyclerAdapter = null;
        mMethodRecyclerAdapter = null;
        mBoardRecyclerAdapter = null;
        mRecycler.scrollTo(0, 0);                 //回到顶部
        refresh();
        if (onEventClickListener != null) {
            onEventClickListener.onSearchKeyClick(key);
        }
    }

    /**
     * 刷新
     */
    private void refresh() {
        saveData();
        mPage = 1;
        loadingData();
    }

    /**
     * 保存普通曝光数据
     */
    private void saveData() {
        Log.e(TAG, "saveData......................");
        HashMap<String, String> baoguang_params = null;
        try {
            List<HashMap<String, String>> doctors = new ArrayList<>();
            HashMap<String, String> showIds = new HashMap<>();
            HashMap<String, String> taoIds = new HashMap<>();
            if (taoListAdapter != null) {
                List<HomeTaoData> datas = taoListAdapter.getSearchData();
                Log.e(TAG, "lastVisibleItemPosition111 == " + lastVisibleItemPosition);
                if (lastVisibleItemPosition <= 0){
                    return;
                }
                Log.e(TAG, "datas.size() == " + datas.size());
                int size = lastVisibleItemPosition < datas.size() ? lastVisibleItemPosition : datas.size();
                for (int i = 0; i < size; i++) {

                    List<TaoListData> taoDatas = taoListAdapter.getTaoDatas();
                    if (i < taoDatas.size()) {
                        TaoListType taolistType = taoDatas.get(i).getTaoList().getSearchTao().getTaolistType();
                        switch (taolistType){
                            case DATA:
                                HomeTaoData data = datas.get(i);
                                if (data != null) {
                                    baoguang_params = data.getBaoguang_params();
                                    Log.e(TAG, "baoguang_params == " + baoguang_params);
                                    if (baoguang_params != null) {
                                        taoIds.put((i+1)+"",data.getId());
                                    }else {
                                        showIds.put((i+1)+"",data.getId());
                                    }
                                }
                                break;
                            case DOCTOR_LIST:
                                List<TaoListDoctors> comparChatDatas = taoListAdapter.getComparChatData();
                                for (int j = 0; j < comparChatDatas.size(); j++) {
                                    TaoListDoctorsCompared compared = comparChatDatas.get(j).getCompared();
                                    int doctorSelectPos = Integer.parseInt(compared.getShowSkuListPosition());
                                    if (doctorSelectPos >= 0 && i == doctorSelectPos) {
                                        doctors.add(compared.getExposureEventParams());
                                    }
                                }
                                break;
                            case FLASH_SALE:
                                List<SearchTao> activeData = taoListAdapter.getActiveData();
                                for (int h = 0; h < activeData.size(); h++) {
                                    SearchTao searchTao = activeData.get(h);
                                    if (searchTao != null){
                                        SearchActivityData flashSale = searchTao.getFlash_sale();
                                        if (flashSale != null && searchTao.getTaolistType() == TaoListType.FLASH_SALE){
                                            doctors.add(flashSale.getExposure_event_params());
                                        }

                                    }

                                }
                                break;
                            case LEADER_BOARD:
                                List<SearchTao> lieaerData = taoListAdapter.getActiveData();
                                for (int h = 0; h < lieaerData.size(); h++) {
                                    SearchTao searchTao = lieaerData.get(h);
                                    if (searchTao != null){
                                        SearchActivityData flashSale = searchTao.getLeader_board();
                                        if (flashSale != null && searchTao.getTaolistType() == TaoListType.LEADER_BOARD){
                                            doctors.add(flashSale.getExposure_event_params());
                                        }

                                    }
                                }
                                break;
                            case BIG_PROMOTION:
                                List<SearchTao> promotionData = taoListAdapter.getActiveData();
                                for (int h = 0; h < promotionData.size(); h++) {
                                    SearchTao searchTao = promotionData.get(h);
                                    if (searchTao != null){
                                        SearchActivityData flashSale = searchTao.getBig_promotion();
                                        if (flashSale != null && searchTao.getTaolistType() == TaoListType.BIG_PROMOTION){
                                            doctors.add(flashSale.getExposure_event_params());
                                        }

                                    }
                                }
                                break;
                            case RECOMMEND:
                                List<SearchTao> pecommendData = taoListAdapter.getActiveData();
                                for (int h = 0; h < pecommendData.size(); h++) {
                                    SearchTao searchTao = pecommendData.get(h);
                                    if (searchTao != null){
                                        SearchActivityData flashSale = searchTao.getRecommend();
                                        if (flashSale != null && searchTao.getTaolistType() == TaoListType.RECOMMEND){
                                            doctors.add(flashSale.getExposure_event_params());
                                        }

                                    }
                                }
                                break;
                        }
                    }
                }

                if (lastVisibleItemPosition != 0){
                    Log.e("YmStatistics","saveData =======");
                    HashMap<String, Object> hashMap = new HashMap<>();
                    Gson gson = new Gson();
                    String taoId = gson.toJson(taoIds);
                    String showId = gson.toJson(showIds);
                    hashMap.put("show_id", showId);
                    hashMap.put("tao_id", taoId);
                    hashMap.put("source","1");
                    hashMap.put("event","1");
                    hashMap.put("utm_source",FinalConstant1.YUEMEI_MARKET);
                    hashMap.put("event_name",FinalEventName.SEARCH_BAOGUANG);
                    if (baoguang_params != null){
                        hashMap.put("baoguang_params",gson.toJson(baoguang_params));
                    }
                    hashMap.put("show_style","692_oneside");
                    new SearchShowApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData s) {
                            if ("1".equals(s.code)){
                                Log.e(TAG,"统计成功 === ");
                            }
                        }
                    });
//                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_BAOGUANG), hashMap, new ActivityTypeData("39"));
                    for (HashMap<String, String> doc : doctors) {
                        if (doc != null) {
                            Log.e(TAG, "doc == " + doc.toString());
                            doc.put("show_style","692_oneside");
                            YmStatistics.getInstance().tongjiApp(doc);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveBilateralData() {
        HashMap<String, String> baoguang_params =null;
        try {
            List<HashMap<String, String>> doctors = new ArrayList<>();
            HashMap<String, String> showIds = new HashMap<>();
            HashMap<String, String> taoIds = new HashMap<>();
            if (taoListStaggeredAdapter != null) {
                List<HomeTaoData> datas = taoListStaggeredAdapter.getSearchData();
                if (mLastVisibleItemPositions == null){
                    return;
                }
                    int visibleItemPosition = mLastVisibleItemPositions[mLastVisibleItemPositions.length-1];
                    Log.e(TAG, "lastVisibleItemPosition222 == " + visibleItemPosition);
                    Log.e(TAG, "datas.size() == " + datas.size());
                    int size = visibleItemPosition < datas.size() ? visibleItemPosition : datas.size();
                    for (int i = 0; i < size; i++) {
                        List<TaoListData> taoDatas = taoListStaggeredAdapter.getTaoDatas();
                        if (i < taoDatas.size()) {
                            TaoListType taolistType = taoDatas.get(i).getTaoList().getSearchTao().getTaolistType();
                            switch (taolistType){
                                case DATA:
                                    HomeTaoData data = datas.get(i);
                                    if (data != null) {
                                        baoguang_params = data.getBaoguang_params();
                                        Log.e(TAG, "baoguang_params == " + baoguang_params);
                                        if (baoguang_params != null) {
                                            taoIds.put((i+1)+"",data.getId());
                                        }else {
                                            showIds.put((i+1)+"",data.getId());
                                        }
                                    }
                                    break;
                                case DOCTOR_LIST:
                                    List<TaoListDoctors> comparChatDatas = taoListStaggeredAdapter.getComparChatData();
                                    for (int j = 0; j < comparChatDatas.size(); j++) {
                                        TaoListDoctorsCompared compared = comparChatDatas.get(j).getCompared();
                                        int doctorSelectPos = Integer.parseInt(compared.getShowSkuListPosition());
                                        if (doctorSelectPos >= 0 && i == doctorSelectPos) {
                                            doctors.add(compared.getExposureEventParams());
                                        }
                                    }
                                    break;
                                case FLASH_SALE:
                                    List<SearchTao> activeData = taoListStaggeredAdapter.getActiveData();
                                    for (int h = 0; h < activeData.size(); h++) {
                                        SearchTao searchTao = activeData.get(h);
                                        if (searchTao != null){
                                            SearchActivityData flashSale = searchTao.getFlash_sale();
                                            if (flashSale != null && searchTao.getTaolistType() == TaoListType.FLASH_SALE){
                                                doctors.add(flashSale.getExposure_event_params());
                                            }

                                        }

                                    }
                                    break;
                                case LEADER_BOARD:
                                    List<SearchTao> lieaerData = taoListStaggeredAdapter.getActiveData();
                                    for (int h = 0; h < lieaerData.size(); h++) {
                                        SearchTao searchTao = lieaerData.get(h);
                                        if (searchTao != null){
                                            SearchActivityData flashSale = searchTao.getLeader_board();
                                            if (flashSale != null && searchTao.getTaolistType() == TaoListType.LEADER_BOARD){
                                                doctors.add(flashSale.getExposure_event_params());
                                            }

                                        }
                                    }
                                    break;
                                case BIG_PROMOTION:
                                    List<SearchTao> promotionData = taoListStaggeredAdapter.getActiveData();
                                    for (int h = 0; h < promotionData.size(); h++) {
                                        SearchTao searchTao = promotionData.get(h);
                                        if (searchTao != null){
                                            SearchActivityData flashSale = searchTao.getBig_promotion();
                                            if (flashSale != null && searchTao.getTaolistType() == TaoListType.BIG_PROMOTION){
                                                doctors.add(flashSale.getExposure_event_params());
                                            }

                                        }
                                    }
                                    break;
                                case RECOMMEND:
                                    List<SearchTao> pecommendData = taoListStaggeredAdapter.getActiveData();
                                    for (int h = 0; h < pecommendData.size(); h++) {
                                        SearchTao searchTao = pecommendData.get(h);
                                        if (searchTao != null){
                                            SearchActivityData flashSale = searchTao.getRecommend();
                                            if (flashSale != null && searchTao.getTaolistType() == TaoListType.RECOMMEND){
                                                doctors.add(flashSale.getExposure_event_params());
                                            }

                                        }
                                    }
                                    break;
                            }
                        }
                    }
                Log.e("YmStatistics","saveBilateralData =======");
                HashMap<String, Object> hashMap = new HashMap<>();
                Gson gson = new Gson();
                String taoId = gson.toJson(taoIds);
                String showId = gson.toJson(showIds);
                hashMap.put("show_id", showId);
                hashMap.put("tao_id", taoId);
                hashMap.put("source","1");
                hashMap.put("event","1");
                hashMap.put("utm_source",FinalConstant1.YUEMEI_MARKET);
                hashMap.put("event_name",FinalEventName.SEARCH_BAOGUANG);
                hashMap.put("show_style","692_bilateral");
                if (baoguang_params != null){
                    hashMap.put("baoguang_params",gson.toJson(baoguang_params));
                }
                new SearchShowApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData s) {
                        if ("1".equals(s.code)){
                            Log.e(TAG,"统计成功 === ");
                        }
                    }
                });
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_BAOGUANG), hashMap, new ActivityTypeData("39"));
                for (HashMap<String, String> doc : doctors) {
                    if (doc != null) {
                        Log.e(TAG, "doc == " + doc.toString());
                        doc.put("show_style","692_bilateral");
                        YmStatistics.getInstance().tongjiApp(doc);
                    }
                }

                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);

        void onSearchKeyClick(String key);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
