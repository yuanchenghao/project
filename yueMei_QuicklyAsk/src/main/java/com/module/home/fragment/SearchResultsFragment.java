package com.module.home.fragment;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.jcodecraeer.xrecyclerview.AppBarStateChangeListener;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.model.bean.SearchResultsUiData;
import com.module.home.model.bean.SearchTitleData;
import com.module.home.model.bean.SearchTitleDataData;
import com.module.home.model.bean.SearchTitleList;
import com.module.home.view.SearchTitleView;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 搜索完成后的页面
 * Created by 裴成浩 on 2019/4/16
 */
public class SearchResultsFragment extends YMBaseFragment {

    @BindView(R.id.search_tablayout_top)
    FrameLayout mTablayoutTop;
    @BindView(R.id.search_tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.search_app_bar)
    AppBarLayout mAppBar;
    @BindView(R.id.search_view_page)
    ViewPager mViewPage;
    @BindView(R.id.search_coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    private String TAG = "SearchResultsFragment";

    private SearchResultsTopFragment searchResultsTopFragment;      //头布局

    private LodHotIssueDataApi lodHotIssueDataApi;
    private String mKey;
    private int mSelectPos;
    private ArrayList<SearchResultsUiData> searchResultsUiDatas = new ArrayList<>();
    private SearchTitleData mTopData;
    private final String DOCTOR = "医生";
    private final String HOSPITAL = "医院";

    //搜索结果页面Fragment的参数集合
    public String[] fragmentUrlList;

    public static SearchResultsFragment newInstance(String key, int pos) {
        SearchResultsFragment fragment = new SearchResultsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putInt("pos", pos);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.search_results_view;
    }

    @Override
    protected void initView(View vw) {
        if (getActivity() instanceof SearchAllActivity668) {
            SearchAllActivity668 activity = (SearchAllActivity668) getActivity();
            activity.mRecycler.setVisibility(View.GONE);
        }

        if (getArguments() != null) {
            mKey = getArguments().getString("key");
            mSelectPos = getArguments().getInt("pos");
        }

        Log.e(TAG, "mKey == " + mKey);

        //设置Ui数据
        setUiData();

        List<String> titleDatas = getTitleDatas();
        List<YMBaseFragment> fragmentDatas = getFragmentDatas();
        mViewPage.setAdapter(new YMTabLayoutAdapter(getChildFragmentManager(), titleDatas, fragmentDatas));
        mTabLayout.setupWithViewPager(mViewPage);
        //默认选中项
        mTabLayout.getTabAt(mSelectPos).select();

        YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(17), Utils.dip2px(17));

        //fragment回调监听
        fragmentListening();

        mViewPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                tabSelected(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    /**
     * 设置UI数据
     */
    private void setUiData() {

        SearchResultsUiData allUiData = new SearchResultsUiData("8", "综合");
        SearchResultsUiData taoUiData = new SearchResultsUiData("4", "商品");
        SearchResultsUiData dailyUiData = new SearchResultsUiData("3", "日记");
        SearchResultsUiData bbsUiData = new SearchResultsUiData("7", "帖子");
        SearchResultsUiData docUiData = new SearchResultsUiData("1", DOCTOR);
        SearchResultsUiData hosUiData = new SearchResultsUiData("5", HOSPITAL);
        SearchResultsUiData userUiData = new SearchResultsUiData("9", "用户");
        SearchResultsUiData askUiData = new SearchResultsUiData("2", "问答");

        allUiData.setFragment(SearchResultsAllFragment.newInstance(mKey, allUiData.getType()));
        taoUiData.setFragment(SearchResultsTaoFragment.newInstance(mKey, taoUiData.getType(), ""));
        dailyUiData.setFragment(SearchResultsDailyFragment.newInstance(mKey, dailyUiData.getType(), ""));
        bbsUiData.setFragment(SearchResultsBBsFragment.newInstance(mKey, bbsUiData.getType()));
        docUiData.setFragment(SearchResultsDocFragment.newInstance(mKey, docUiData.getType()));
        hosUiData.setFragment(SearchResultsHosFragment.newInstance(mKey, hosUiData.getType()));
        userUiData.setFragment(SearchResultsUserFragment.newInstance(mKey, userUiData.getType()));
        askUiData.setFragment(SearchResultsAskFragment.newInstance(mKey, askUiData.getType()));

        searchResultsUiDatas.add(allUiData);
        searchResultsUiDatas.add(taoUiData);
        searchResultsUiDatas.add(hosUiData);
        searchResultsUiDatas.add(docUiData);
        searchResultsUiDatas.add(dailyUiData);
        searchResultsUiDatas.add(bbsUiData);
        searchResultsUiDatas.add(askUiData);
        searchResultsUiDatas.add(userUiData);


        //设置无埋点
        int size = searchResultsUiDatas.size();
        fragmentUrlList = new String[size];
        for (int i = 0; i < size; i++) {
            fragmentUrlList[i] = "";
        }
    }

    /**
     * 无埋点获取选中项
     *
     * @param selectPos
     */
    private void tabSelected(int selectPos) {
        Log.e(TAG, "selectPos == " + selectPos);
        mSelectPos = selectPos;
    }

    /**
     * fragment回调监听
     */
    private void fragmentListening() {

        for (int i = 0; i < searchResultsUiDatas.size(); i++) {
            final int finalI = i;
            SearchResultsUiData data = searchResultsUiDatas.get(i);
            YMBaseFragment fragment = data.getFragment();
            if (fragment instanceof SearchResultsAllFragment) {

                //综合页
                ((SearchResultsAllFragment) fragment).setOnEventClickListener(new SearchResultsAllFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }

                    @Override
                    public void onSearchKeyClick(String key) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onSearchKeyClick(key);
                        }
                    }

                    @Override
                    public void onSearchMoreClick(String type) {
                        Log.e(TAG, "type == " + type);
                        int typeToIndex = getTypeToIndex(type);
                        if (typeToIndex >= 0) {
                            mTabLayout.getTabAt(typeToIndex).select();
                        }
                    }
                });

            } else if (fragment instanceof SearchResultsTaoFragment) {

                //淘整形
                ((SearchResultsTaoFragment) fragment).setOnEventClickListener(new SearchResultsTaoFragment.OnEventClickListener() {
                    /**
                     * 重新搜索
                     * @param para
                     */
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }

                    @Override
                    public void onSearchKeyClick(String key) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onSearchKeyClick(key);
                        }
                    }
                });

            } else if (fragment instanceof SearchResultsDailyFragment) {

                //日记
                ((SearchResultsDailyFragment) fragment).setOnEventClickListener(new SearchResultsDailyFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }
                });

            } else if (fragment instanceof SearchResultsBBsFragment) {
                //帖子
                ((SearchResultsBBsFragment) fragment).setOnEventClickListener(new SearchResultsBBsFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }
                });
            } else if (fragment instanceof SearchResultsDocFragment) {

                //医生
                ((SearchResultsDocFragment) fragment).setOnEventClickListener(new SearchResultsDocFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }
                });
            } else if (fragment instanceof SearchResultsHosFragment) {

                //医院
                ((SearchResultsHosFragment) fragment).setOnEventClickListener(new SearchResultsHosFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }
                });
            } else if (fragment instanceof SearchResultsUserFragment) {
                //用户
                ((SearchResultsUserFragment) fragment).setOnEventClickListener(new SearchResultsUserFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }
                });

            } else if (fragment instanceof SearchResultsAskFragment) {

                //医院
                ((SearchResultsAskFragment) fragment).setOnEventClickListener(new SearchResultsAskFragment.OnEventClickListener() {
                    @Override
                    public void onSearchParaClick(String para) {
                        fragmentUrlList[finalI] = para;
                        tabSelected(mSelectPos);
                    }
                });
            }
        }

        //联动刷新控制
        mAppBar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Log.e(TAG, "state == " + state);
                switch (state) {
                    case IDLE:
                        //TabLayout部分露出/全部露出
                        setRefresh(false);
                        break;
                    case COLLAPSED:
                        //TabLayout完全隐藏
                        setRefresh(false);
                        break;
                    case EXPANDED:
                        //最大化展开
                        setRefresh(true);
                        break;
                }
            }
        });
    }


    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();
        loadTitleData();
    }

    /**
     * 获取数据
     */
    private void loadTitleData() {

        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));
        lodHotIssueDataApi.addData("high", "1");

        lodHotIssueDataApi.getCallBack(mContext, lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    Log.e(TAG, "Json:==" + s.data);
                    try {
                        mTopData = JSONUtil.TransformSingleBean(s.data, SearchTitleData.class);
                        SearchTitleList data = mTopData.getData();
                        if (data != null) {

                            List<SearchTitleDataData> list = data.getList();
                            if (list.size() > 0) {
                                if (searchResultsTopFragment != null) {
                                    searchResultsTopFragment.mTopView.setVisibility(View.VISIBLE);
                                } else {
                                    mTablayoutTop.setVisibility(View.VISIBLE);
                                }
                                searchResultsTopFragment = SearchResultsTopFragment.newInstance(mTopData, mKey);
                                setActivityFragment(R.id.search_tablayout_top, searchResultsTopFragment);

                                searchResultsTopFragment.setOnEventClickListener(new SearchResultsTopFragment.OnEventClickListener() {
                                    @Override
                                    public void onLookClick(String type) {

                                        int selectedIndex = -1;
                                        if ("doctor".equals(type)) {
                                            selectedIndex = getDoctorHospitalIndex(DOCTOR);
                                        } else if ("hospital".equals(type)) {
                                            selectedIndex = getDoctorHospitalIndex(HOSPITAL);
                                        }

                                        if (selectedIndex >= 0) {
                                            mTabLayout.getTabAt(selectedIndex).select();
                                        }
                                    }
                                });

                                //回调监听
                                mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {
                                        //选中
                                        String type = mTopData.getData().getType();
                                        Log.e(TAG, "type == " + type);
                                        int selectedIndex = -1;
                                        if ("doctor".equals(type)) {
                                            selectedIndex = getDoctorHospitalIndex(DOCTOR);
                                        } else if ("hospital".equals(type)) {
                                            selectedIndex = getDoctorHospitalIndex(HOSPITAL);
                                        }

                                        if (selectedIndex >= 0 && tab.getPosition() == selectedIndex) {
                                            if (searchResultsTopFragment != null) {
                                                searchResultsTopFragment.mTopView.setVisibility(View.GONE);
                                            } else {
                                                if (searchResultsTopFragment != null) {
                                                    searchResultsTopFragment.mTopView.setVisibility(View.GONE);
                                                } else {
                                                    mTablayoutTop.setVisibility(View.GONE);
                                                }
                                            }
                                        } else {
                                            if (searchResultsTopFragment != null) {
                                                searchResultsTopFragment.mTopView.setVisibility(View.VISIBLE);
                                            } else {
                                                if (searchResultsTopFragment != null) {
                                                    searchResultsTopFragment.mTopView.setVisibility(View.VISIBLE);
                                                } else {
                                                    mTablayoutTop.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {

                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {

                                    }
                                });
                            } else {
                                if (searchResultsTopFragment != null) {
                                    searchResultsTopFragment.mTopView.setVisibility(View.GONE);
                                } else {
                                    mTablayoutTop.setVisibility(View.GONE);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "e == " + e.toString());
                    }
                }
            }
        });
    }

    /**
     * 设置刷新的功能，解决联动冲突
     *
     * @param enabled
     */
    private void setRefresh(boolean enabled) {
        for (YMBaseFragment fragment : getFragmentDatas()) {
            if (fragment instanceof SearchResultsAllFragment) {
                SmartRefreshLayout mAllRefresh = ((SearchResultsAllFragment) fragment).mRefresh;
                if (mAllRefresh != null) {
                    mAllRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsTaoFragment) {
                SmartRefreshLayout mTaoRefresh = ((SearchResultsTaoFragment) fragment).mRefresh;
                if (mTaoRefresh != null) {
                    mTaoRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsDailyFragment) {
                SmartRefreshLayout mDailyRefresh = ((SearchResultsDailyFragment) fragment).mRefresh;
                if (mDailyRefresh != null) {
                    mDailyRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsBBsFragment) {
                SmartRefreshLayout mBBsRefresh = ((SearchResultsBBsFragment) fragment).mRefresh;
                if (mBBsRefresh != null) {
                    mBBsRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsDocFragment) {
                SmartRefreshLayout mDocRefresh = ((SearchResultsDocFragment) fragment).mRefresh;
                if (mDocRefresh != null) {
                    mDocRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsHosFragment) {
                SmartRefreshLayout mHosRefresh = ((SearchResultsHosFragment) fragment).mRefresh;
                if (mHosRefresh != null) {
                    mHosRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsUserFragment) {
                SmartRefreshLayout mUserRefresh = ((SearchResultsUserFragment) fragment).mRefresh;
                if (mUserRefresh != null) {
                    mUserRefresh.setEnableRefresh(enabled);
                }
            } else if (fragment instanceof SearchResultsAskFragment) {
                SmartRefreshLayout mAskRefresh = ((SearchResultsAskFragment) fragment).mRefresh;
                if (mAskRefresh != null) {
                    mAskRefresh.setEnableRefresh(enabled);
                }
            }
        }
    }


    /**
     * 获取医生/医院下标
     *
     * @param flag
     * @return
     */
    private int getDoctorHospitalIndex(String flag) {
        for (int i = 0; i < getTitleDatas().size(); i++) {
            String title = getTitleDatas().get(i);
            if (flag.equals(title)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 根据type值获取下标
     *
     * @param type
     * @return
     */
    private int getTypeToIndex(String type) {
        for (int i = 0; i < searchResultsUiDatas.size(); i++) {
            SearchResultsUiData data = searchResultsUiDatas.get(i);
            if (type.equals(data.getType())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 获取头部标题
     *
     * @return
     */
    private List<String> getTitleDatas() {
        ArrayList<String> titleDatas = new ArrayList<>();
        for (SearchResultsUiData data : searchResultsUiDatas) {
            titleDatas.add(data.getTitle());
        }
        return titleDatas;
    }

    /**
     * 获取fragment数据
     *
     * @return
     */
    private List<YMBaseFragment> getFragmentDatas() {
        ArrayList<YMBaseFragment> fragmentDatas = new ArrayList<>();
        for (SearchResultsUiData data : searchResultsUiDatas) {
            Log.e(TAG, " == " + data.getFragment()
            );
            fragmentDatas.add(data.getFragment());
        }
        return fragmentDatas;
    }

    public interface OnEventClickListener {

        void onSearchKeyClick(String key);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
