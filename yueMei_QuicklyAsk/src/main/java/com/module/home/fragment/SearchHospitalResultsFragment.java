package com.module.home.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 医院搜索结果页面
 * Created by 裴成浩 on 2019/9/12
 */
public class SearchHospitalResultsFragment extends YMBaseFragment {

    @BindView(R.id.search_results_hospital_tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.search_results_hospital_view_pager)
    ViewPager mViewPager;

    private List<String> titleDatas = new ArrayList<>();
    private List<YMBaseFragment> fragmentDatas = new ArrayList<>();
    private String mKey;
    private String hospital_id;

    public static SearchHospitalResultsFragment newInstance(String key, String id) {
        SearchHospitalResultsFragment fragment = new SearchHospitalResultsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("id", id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_hospital_view;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mKey = getArguments().getString("key");
            hospital_id = getArguments().getString("id");
        }

        titleDatas.add("淘整型");
        titleDatas.add("日记");

        fragmentDatas.add(SearchResultsTaoFragment.newInstance(mKey, "4", hospital_id));
        fragmentDatas.add(SearchResultsDailyFragment.newInstance(mKey, "3", hospital_id));

        mViewPager.setAdapter(new YMTabLayoutAdapter(getChildFragmentManager(), titleDatas, fragmentDatas));
        mTabLayout.setupWithViewPager(mViewPager);

        YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(5), Utils.dip2px(5));
    }

    @Override
    protected void initData(View view) {

    }
}
