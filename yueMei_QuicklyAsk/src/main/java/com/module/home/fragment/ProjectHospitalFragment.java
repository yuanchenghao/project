package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.bean.HosListData;
import com.module.home.controller.adapter.ProjectHosAdapter;
import com.module.home.model.api.ProjectDetailsApi;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/2/22
 */
public class ProjectHospitalFragment extends YMBaseFragment {
    private static String TAG = "ProjectHospitalFragment";
    @BindView(R.id.project_hos_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.project_hos_refresh)
    SmartRefreshLayout mHosRefresh;
    @BindView(R.id.project_hos_rec)
    RecyclerView mHosRecycler;
    @BindView(R.id.project_hos_refresh_more)
    YMLoadMore mHosRefreshMore;
    @BindView(R.id.fragment_project_details_not)
    NestedScrollView mNotData;

    private ProjectDetailsApi mProjectDetailsApi;
    private ArrayList<HosListData> mFragmentData;
    private ProjectHosAdapter mProjectHosAdapter;
    private int mPage = 1;
    private ProjectDetailsBean detailsBean;

    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private BaseSortPopupwindows kindPop;
    private String mSort = "1";                                 //排序的选中id
    private String mKindId = "0";                               //筛选id

    public static ProjectHospitalFragment newInstance(ArrayList<HosListData> datas, ProjectDetailsBean bean) {
        ProjectHospitalFragment fragment = new ProjectHospitalFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("datas", datas);
        bundle.putParcelable("bean", bean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_details_hospital;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mFragmentData = getArguments().getParcelableArrayList("datas");
            detailsBean = getArguments().getParcelable("bean");
        }

        mScreen.initView(true);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener() {
            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                        if (onEventClickListener != null) {
                            onEventClickListener.onPopupWindowClick(cityPop);
                        }
                    }
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                        if (onEventClickListener != null) {
                            onEventClickListener.onPopupWindowClick(sortPop);
                        }
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }

            @Override
            public void onKindClick() {
                if (kindPop != null) {
                    if (kindPop.isShowing()) {
                        kindPop.dismiss();
                    } else {
                        kindPop.showPop();
                        if (onEventClickListener != null) {
                            onEventClickListener.onPopupWindowClick(sortPop);
                        }
                    }
                    mScreen.initSortView(kindPop.isShowing());
                }
            }
        });

        //上拉加载更多
        mHosRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mHosRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                getProjectDetailsData();
            }
        });
    }

    @Override
    protected void initData(View view) {
        mProjectDetailsApi = new ProjectDetailsApi();
        if (mFragmentData.size() == 0 && getActivity() != null) {
            getProjectDetailsData();
        } else {
            mPage++;
            setRecyclerData(mFragmentData);
        }

        cityPop = new BaseCityPopwindows(mContext, mScreen);
        setSortData();
        setKindData();

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }
                refresh();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });

    }

    /**
     * 获取页面数据
     */
    private void getProjectDetailsData() {

        Log.e(TAG, "parentLabelID == " + detailsBean.getTwoLabelId());
        Log.e(TAG, "mLabelId == " + detailsBean.getFourLabelId());
        Log.e(TAG, "mHomeSource == " + detailsBean.getHomeSource());

        mProjectDetailsApi.getProjectDetailsHashMap().clear();

        mProjectDetailsApi.addData("parentLabelID", detailsBean.getTwoLabelId());
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId())) {
            mProjectDetailsApi.addData("labelID", detailsBean.getFourLabelId());
        }
        mProjectDetailsApi.addData("listType", "2");
        mProjectDetailsApi.addData("homeSource", detailsBean.getHomeSource());
        mProjectDetailsApi.addData("page", mPage + "");
        mProjectDetailsApi.addData("sort", mSort);
        mProjectDetailsApi.addData("kind", mKindId);

        mProjectDetailsApi.getCallBack(mContext, mProjectDetailsApi.getProjectDetailsHashMap(), new BaseCallBackListener<ProjectDetailsData>() {
            @Override
            public void onSuccess(ProjectDetailsData data) {
                if (getActivity() != null) {
                    mPage++;
                    if (data.getTaoList().size() == 0) {
                        mHosRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mHosRefresh.finishLoadMore();
                    }

                    if (mProjectHosAdapter == null) {
                        if (data.getHospitalList().size() != 0) {
                            mHosRefresh.setVisibility(View.VISIBLE);
                            mNotData.setVisibility(View.GONE);

                            setRecyclerData(data.getHospitalList());
                        } else {
                            mHosRefresh.setVisibility(View.GONE);
                            mNotData.setVisibility(View.VISIBLE);
                        }

                    } else {
                        mProjectHosAdapter.addData(data.getHospitalList());
                    }
                }
            }
        });
    }

    private void setRecyclerData(ArrayList<HosListData> hospitalList) {
        mHosRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
//        mHosRecycler.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(10), Utils.getLocalColor(mContext, R.color._f6)));
        mProjectHosAdapter = new ProjectHosAdapter(mContext, hospitalList);
        mHosRecycler.setAdapter(mProjectHosAdapter);

        mProjectHosAdapter.setOnEventClickListener(new ProjectHosAdapter.OnEventClickListener() {
            @Override
            public void onItemClick(View v, HosListData data, int pos) {
                HashMap event_params = data.getEvent_params();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "channel|hospitallist_" + Utils.getCity() + "_" + getTagId() + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE + ""), event_params);

                String hosid = data.getHos_id();
                Intent it = new Intent(mContext, HosDetailActivity.class);
                it.putExtra("hosid", hosid);
                mContext.startActivity(it);
            }
        });
    }

    /**
     * 获取选中标签
     *
     * @return
     */
    private String getTagId() {
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId()) && !"0".equals(detailsBean.getFourLabelId())) {
            return detailsBean.getFourLabelId();
        } else {
            return detailsBean.getTwoLabelId();
        }
    }

    /**
     * 四级标签点击，刷新数据
     *
     * @param hosList
     * @param bean
     */
    public void setRefreshData(ArrayList<HosListData> hosList, ProjectDetailsBean bean) {
        if (mProjectDetailsApi != null) {
            detailsBean = bean;
            mFragmentData = hosList;

            refresh();
        }
    }

    /**
     * 刷新
     */
    private void refresh() {
        mPage = 1;
        mProjectHosAdapter = null;
        getProjectDetailsData();
    }

    /**
     * 排序数据
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("2");
        a2.setName("离我最近");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("3");
        a4.setName("案例数多");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("4");
        a5.setName("评分高");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a2);
        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (sortPop != null) {
                    sortPop.dismiss();
                    mSort = sortId;
                    mScreen.initSortView(sortPop.isShowing());
                    mScreen.setSortTitle(sortName);
                }
                refresh();
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    /**
     * 筛选数据
     */
    private void setKindData() {
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("0");
        a5.setName("不限职称");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("1");
        a6.setName("住院医师");
        TaoPopItemData a7 = new TaoPopItemData();
        a7.set_id("2");
        a7.setName("主治医师");
        TaoPopItemData a8 = new TaoPopItemData();
        a8.set_id("3");
        a8.setName("副主任医师");
        TaoPopItemData a9 = new TaoPopItemData();
        a9.set_id("4");
        a9.setName("主任医师");
        List<TaoPopItemData> lvKindData = new ArrayList<>();
        lvKindData.add(a5);
        lvKindData.add(a6);
        lvKindData.add(a7);
        lvKindData.add(a8);
        lvKindData.add(a9);

        kindPop = new BaseSortPopupwindows(mContext, mScreen, lvKindData);

        kindPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (kindPop != null) {
                    kindPop.dismiss();
                    mKindId = sortId;
                    mScreen.initKindView(kindPop.isShowing(), false);
                    mScreen.setKindTitle(sortName);
                }
                refresh();
            }
        });

        kindPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    public interface OnEventClickListener {
        void onPopupWindowClick(PopupWindow popupWindow);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
