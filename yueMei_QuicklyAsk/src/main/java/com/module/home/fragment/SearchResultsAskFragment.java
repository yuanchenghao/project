package com.module.home.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.ProjectAnswerAdapter;
import com.module.home.model.SearchResultAskData;
import com.module.home.model.bean.QuestionListData;
import com.module.my.controller.activity.TypeProblemActivity;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/6/18
 */
public class SearchResultsAskFragment extends YMBaseFragment {

    @BindView(R.id.search_results_ask_refresh)
    public SmartRefreshLayout mRefresh;
    @BindView(R.id.search_results_top_tip)
    LinearLayout mTopTip;
    @BindView(R.id.search_results_top_title)
    TextView mTopTitle;
    @BindView(R.id.search_results_ask_recycler)
    RecyclerView mRecycler;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView dailyNotView;

    private LodHotIssueDataApi lodHotIssueDataApi;

    private String TAG = "SearchResultsAskFragment";

    private int mPage = 1;
    private String mKey;
    private String mType;
    private ProjectAnswerAdapter mProjectAnswerAdapter;
    private Gson mGson;

    public static SearchResultsAskFragment newInstance(String key,String type) {
        SearchResultsAskFragment fragment = new SearchResultsAskFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_ask_view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mType = getArguments().getString("type");

        mTopTitle.setText("关于“" + mKey + "”");

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mProjectAnswerAdapter = null;
                mPage = 1;
                loadingData();
            }
        });

        //头部点击
        mTopTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ASK_ENTRY, "search_ask"));
                    mFunctionManager.goToActivity(TypeProblemActivity.class);
                }
            }
        });
    }

    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();
        mGson = new Gson();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(linearLayoutManager);
        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));             //筛选词
        lodHotIssueDataApi.addData("type",mType);            //搜索数据类型
        lodHotIssueDataApi.addData("page", mPage + "");     //页码

        lodHotIssueDataApi.getCallBack(mContext, lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (onEventClickListener != null) {
                    onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
                }
                Log.e(TAG, "JSON1:==" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        SearchResultAskData searchResultAskData = JSONUtil.TransformSingleBean(serverData.data, SearchResultAskData.class);
                        List<QuestionListData> questionList = searchResultAskData.getList();

                        //刷新隐藏
                        mRefresh.finishRefresh();
                        if (questionList.size() == 0) {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        } else {
                            mRefresh.finishLoadMore();
                        }

                        if (mPage == 1 && questionList.size() == 0) {
                            dailyNotView.setVisibility(View.VISIBLE);
                            mRefresh.setVisibility(View.GONE);
                        } else {
                            dailyNotView.setVisibility(View.GONE);
                            mRefresh.setVisibility(View.VISIBLE);

                            if (mProjectAnswerAdapter == null) {
                                mProjectAnswerAdapter = new ProjectAnswerAdapter(mContext, questionList,"");
                                mRecycler.setAdapter(mProjectAnswerAdapter);

                                //item点击事件
                                mProjectAnswerAdapter.setOnItemClickListener(new ProjectAnswerAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int pos, QuestionListData data) {
                                        String jumpUrl = data.getJumpUrl();
                                        HashMap<String, String> event_params = mProjectAnswerAdapter.getDatas().get(pos).getEvent_params();
                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH, "asklist|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE), event_params, new ActivityTypeData("42"));
                                        if (!TextUtils.isEmpty(jumpUrl)) {
                                            WebUrlTypeUtil.getInstance(mContext).urlToApp(jumpUrl);
                                        }
                                    }
                                });
                            } else {
                                mProjectAnswerAdapter.addData(questionList);
                            }
                        }

                        mPage++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    dailyNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                }
            }
        });
    }


    public interface OnEventClickListener {
        void onSearchParaClick(String para);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
