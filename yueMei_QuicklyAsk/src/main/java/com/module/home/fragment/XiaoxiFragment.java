package com.module.home.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cookie.store.CookieStore;
import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.loadmore.LoadMoreListView;
import com.module.base.refresh.loadmore.LoadMoreListener;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.commonview.PageJumpManager;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.chatnet.IMManager;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.module.bean.ChatListBean;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.home.controller.adapter.MessageAdapter;
import com.module.home.controller.adapter.RecomendAdapter;
import com.module.home.model.api.XiaoxiChatListAPI;
import com.module.home.model.api.XiaoxiChatUpdatereadApi;
import com.module.home.view.LoadingProgress;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wuxiaolong.pullloadmorerecyclerview.PullLoadMoreRecyclerView;

import org.json.JSONObject;
import org.kymjs.aframe.utils.SystemTool;
import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;
import okhttp3.Cookie;
import okhttp3.HttpUrl;
import sqlite.OrderDao;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * 消息
 * Created by 裴成浩 on 2017/10/17.
 */
@SuppressLint("ValidFragment")
public class XiaoxiFragment extends Fragment {

    private MessageFragmentActivity1 mActivity;
    private String TAG = "XiaoxiFragment";
    private LoadingProgress dialog;
    private String errorHtml;
    private JSONObject obj_http;
    private PageJumpManager pageJumpManager;
    private ChatListBean mChatListBeen;
    private LoadMoreRecyclerView xiaoxiList;
    private SmartRefreshLayout messageRefresh;
    public MessageAdapter messageAdapter;
    private int typePosition;
    private String domain = "chat.yuemei.com";
    private long expiresAt = 1544493729973L;
    private String name = "";
    private String path = "/";
    private String value = "";
    private String mYuemeiinfo;
    private int mPage = 1;
    private boolean isNoMore = false;
    private String mRegistrationID;
    private View headerView;
    private TextView headerTitle;
    private RecyclerView headerList;
    private List<ChatListBean.RecomendBean.ListBean> list=new ArrayList<>();
    private boolean isHead=false; //如果是true list下标减一
    private boolean isLoadData=false;
    private boolean mLogin;

    public XiaoxiFragment() {
    }

    public static  XiaoxiFragment getInstance(){
        return new XiaoxiFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page2, container, false);
        this.mActivity = (MessageFragmentActivity1)getActivity();
        xiaoxiList = view.findViewById(R.id.fragment_xiaoxi);
        messageRefresh = view.findViewById(R.id.messsage_refresh);
        mRegistrationID = JPushInterface.getRegistrationID(getActivity());
        errorHtml = "<html><body><h1> </h1></body></html>";
        mLogin = Utils.isLogin();
        pageJumpManager = new PageJumpManager(getActivity());
        mYuemeiinfo = Utils.getYuemeiInfo();


        ExposureLoginData exposureLoginData = new ExposureLoginData("63","0");
        Cfg.saveStr(getActivity(),FinalConstant.EXPOSURE_LOGIN,new Gson().toJson(exposureLoginData));

        if (TextUtils.isEmpty(mYuemeiinfo)) {
            getUserInfoLogin();
        } else {
            getPersimmions();
        }
        messageRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                isLoadData=true;
                mPage=1;
                chatList(true);
            }
        });

        //列表上拉加载更多
        xiaoxiList.setLoadMoreListener(new LoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e(TAG, "1111111111");
                isLoadData=true;
                if (!isNoMore) {
                    chatList(false);
                } else {
                    xiaoxiList.loadMoreComplete();
                }
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mLogin != Utils.isLogin()){
            mLogin = Utils.isLogin();
            chatList(true);
        }

    }



    protected void getPersimmions() {
        Acp.getInstance(getActivity()).request(new AcpOptions.Builder().setPermissions(Manifest.permission.READ_PHONE_STATE, WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
            @Override
            public void onGranted() {
                chatList(false);
            }

            @Override
            public void onDenied(List<String> permissions) {
                chatList(false);
            }
        });
    }
    /**
     * 初始化私信列表
     */
    public void chatList(final boolean isRefresh) {

        CookieConfig.getInstance().setCookie("https", "chat.yuemei.com","chat.yuemei.com");

        Map<String, Object> maps = new HashMap<>();
        maps.put("page", mPage + "");
        Log.e(TAG, "page === " + mPage);

        new XiaoxiChatListAPI().getCallBack(getContext(), maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        if (messageRefresh != null){
                            messageRefresh.finishRefresh();
                        }
                        mChatListBeen = JSONUtil.TransformSingleBean(serverData.data,ChatListBean.class);
                        List<ChatListBean.RecomendBean.ListBean> listBeans = mChatListBeen.getRecomend().getList();
                        if (listBeans.size() != 0){
                            list=listBeans;
                        }
                        if (list != null && list.size() > 0) {
                            if (!isLoadData){
                                isHead = true;
                                headerView = LayoutInflater.from(mActivity).inflate(R.layout.footview, null);
                                headerTitle = headerView.findViewById(R.id.head_title);
                                headerList = headerView.findViewById(R.id.head_list);
                                xiaoxiList.addHeaderView(headerView);
                                headerTitle.setText(mChatListBeen.getRecomend().getTitle());
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                                headerList.setLayoutManager(linearLayoutManager);
                                RecomendAdapter recomendAdapter = new RecomendAdapter(R.layout.chat_list_head, XiaoxiFragment.this.list);
                                headerList.setAdapter(recomendAdapter);
                                recomendAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                                        if (XiaoxiFragment.this.list == null || XiaoxiFragment.this.list.size() == 0)
                                            return;
                                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                                .setDirectId(XiaoxiFragment.this.list.get(position).getId())
                                                .setObjId("0")
                                                .setObjType("0")
                                                .setYmClass("110")
                                                .setYmId("0")
                                                .build();
                                        pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                                        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                                                .setEvent_name("chat_hospital")
                                                .setEvent_pos("chatlist_recomend")
                                                .setHos_id(XiaoxiFragment.this.list.get(position).getHos_id())
                                                .setDoc_id("0")
                                                .setTao_id("0")
                                                .setEvent_others(XiaoxiFragment.this.list.get(position).getHos_id())
                                                .setId("0")
                                                .setReferrer("17")
                                                .setType("63")
                                                .build();
                                        Utils.chatTongJi(getActivity(), tongJiParams);

                                    }
                                });
                            }

                        }

                        mPage++;
                        if (!isNoMore) {
                            if (messageAdapter == null) {
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                                xiaoxiList.setLayoutManager(linearLayoutManager);
                                messageAdapter = new MessageAdapter(mChatListBeen.getList(),getActivity());
                                xiaoxiList.setAdapter(messageAdapter);
                            } else {
                                if (isRefresh){
                                    messageAdapter.setmChatList2(mChatListBeen.getList());
                                }else {
                                    messageAdapter.setmChatList(mChatListBeen.getList());
                                }
                            }

                            if (mChatListBeen.getList().size() < 10) {
                                isNoMore = true;
                                xiaoxiList.loadMoreComplete();
                            } else {
                                xiaoxiList.loadMoreComplete();
                            }
                            messageAdapter.setOnItemCallBackListener(new MessageAdapter.ItemCallBackListener() {
                                @Override
                                public void onItemClick(View v, int position) {
                                    List<ChatListBean.ListBean> mData = messageAdapter.getmChatList();
                                    Log.e(TAG, "position === " + position);
                                    Log.e(TAG, "xiaoxiAdapter.getmChatList().size() === " + mData.size());
                                    if (position != mData.size() + 1) {
                                        if (isHead){
                                            typePosition=position-1;
                                        }else typePosition=position;
                                        if (typePosition <0 )return;
                                        String mId = mData.get(typePosition).getId();
                                        String group_id = mData.get(typePosition).getGroup_id();
                                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                                .setDirectId(mId)
                                                .setGroup_id(group_id)
                                                .setObjId("0")
                                                .setObjType("0")
                                                .setYmClass("109")
                                                .setYmId("0")
                                                .build();
                                        pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                                        HashMap<String, String> event_params = mData.get(typePosition).getEvent_params();
                                        YmStatistics.getInstance().tongjiApp(event_params);
//                                        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
//                                                .setEvent_name("chat_hospital")
//                                                .setEvent_pos("chatlist_top")
//                                                .setHos_id(mData.get(typePosition).getHos_id())
//                                                .setDoc_id("0")
//                                                .setTao_id("0")
//                                                .setEvent_others(mData.get(typePosition).getHos_id())
//                                                .setId("0")
//                                                .setReferrer("17")
//                                                .setType("63")
//                                                .build();
//                                        Utils.chatTongJi(getActivity(),tongJiParams);

                                        //本地刷新列表机构消息数
                                        String noread = mData.get(typePosition).getNoread();
                                        mData.get(typePosition).setNoread("0");

                                        int mySixinNum = Cfg.loadInt(getActivity(),FinalConstant.SIXIN_ID,0);               //自己的私信

                                        int sixinNum = mySixinNum - Integer.parseInt(noread);
                                        Cfg.saveInt(getActivity(),FinalConstant.SIXIN_ID, sixinNum < 0 ? 0 : sixinNum);
                                        messageAdapter.notifyDataSetChanged();

                                        //本地刷新私信fragment消息数
                                        mActivity.mBadgeCountList.set(0, sixinNum < 0 ? 0 : sixinNum);
                                        mActivity.setUpTabBadge(0);

                                        //本地刷新消息界面消息数
                                        int newsNum = Cfg.loadInt(getActivity(),FinalConstant.ZONG_ID,0);
                                        newsNum = newsNum - Integer.parseInt(noread) < 0 ? 0 : newsNum - Integer.parseInt(noread);
                                        if (Utils.isLogin() && Utils.isBind()) {
                                            MainTableActivity.mainBottomBar.setMessageNum(newsNum);
                                        }
                                        //刷新服务器消息数
                                        chatUpdateread(mData.get(typePosition).getId());
                                    }
                                }
                            });

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if ("10001".equals(serverData.code)) {
                    Toast.makeText(mActivity, "当前账号密码变动，请重新登录", Toast.LENGTH_SHORT).show();
                    Utils.clearUserData();
                    Utils.jumpLogin(mActivity);
                }

            }
        });
    }

    /**
     * 刷新消息未读数
     */
    public void chatUpdateread(String id) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", id);

        new XiaoxiChatUpdatereadApi().getCallBack(getContext(), maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {

                    Log.e(TAG, "执行到此 === " + serverData.toString());

                } else {

                    ViewInject.toast(serverData.message);
                }
            }
        });
    }

    private void getUserInfoLogin() {
        BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
        Map<String, Object> params = new HashMap<>();
        params.put("id", Utils.getUid());
        userInfoApi.getCallBack(getContext(), params, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    Utils.setYuemeiInfo(userData.getYuemeiinfo());
                    mYuemeiinfo = Utils.getYuemeiInfo();
                    chatList(false);
                }else {
                    Toast.makeText(mActivity,serverData.message,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    /**
     * 加载效果
     */
    public void startLoading() {
        if (dialog == null) {
            dialog = new LoadingProgress(mActivity, R.style.MyDialog);
        }
        if (!SystemTool.isWiFi(mActivity)) {
            dialog.show();
        }
    }

    /**
     * 对话框消失
     */
    public void stopLoading() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        Cfg.saveStr(getActivity(), FinalConstant.EXPOSURE_LOGIN, "");
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 && resultCode == 100) {
            //网络刷新
//            chatList("0");

            //本地列表刷新
            String lastData = data.getStringExtra("lastData");
            String lastTime = data.getStringExtra("lastTime");
            Log.e(TAG, "lastData === " + lastData);
            Log.e(TAG, "lastTime === " + lastTime);
           if (messageAdapter == null){
               return;
           }
            if (!TextUtils.isEmpty(lastData)) {
                messageAdapter.setNewContent(typePosition, lastData);
            }

            if (!TextUtils.isEmpty(lastTime)) {
                messageAdapter.setNewTime(typePosition, lastTime);
            }
            if (messageAdapter.getmChatList() == null){
                return;
            }
            String noread = messageAdapter.getmChatList().get(typePosition).getNoread();
            messageAdapter.setNoread(typePosition, "0");
            messageAdapter.notifyDataSetChanged();

            //本地刷新私信fragment消息数
            int mySixinNum =Cfg.loadInt(getActivity(),FinalConstant.SIXIN_ID,0);
            int sixinNum = mySixinNum - Integer.parseInt(noread);
            Cfg.saveInt(getActivity(),FinalConstant.SIXIN_ID, sixinNum < 0 ? 0 : sixinNum);//保存自己的私信
            int fragNum = sixinNum < 0 ? 0 : sixinNum;

            Log.e(TAG, "mySixinNum === " + mySixinNum);
            Log.e(TAG, "sixinNum === " + sixinNum);
            Log.e(TAG, "fragNum === " + fragNum);

            mActivity.mBadgeCountList.set(0, fragNum);
            mActivity.setUpTabBadge(0);

            //本地刷新消息界面消息数
            int newsNum = Cfg.loadInt(getActivity(),FinalConstant.ZONG_ID,0);
            newsNum = newsNum - Integer.parseInt(noread) < 0 ? 0 : newsNum - Integer.parseInt(noread);
            if (Utils.isLogin() && Utils.isBind()) {
                MainTableActivity.mainBottomBar.setMessageNum(newsNum);
            }
            //刷新服务器消息数
            chatUpdateread(messageAdapter.getmChatList().get(typePosition).getId());
        }
        Log.e(TAG,"onActivityResult");
    }




}

