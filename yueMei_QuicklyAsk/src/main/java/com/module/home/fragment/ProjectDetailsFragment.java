package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jcodecraeer.xrecyclerview.AppBarStateChangeListener;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.SortScreenPopwin;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.home.controller.activity.ProjectDetailsActivity;
import com.module.home.controller.adapter.ProjectBrandAdapter;
import com.module.home.controller.adapter.ProjectDetailsManualAdapter;
import com.module.home.controller.adapter.ProjectDetailsTagAdapter;
import com.module.home.model.api.ProjectDetailsApi;
import com.module.home.model.bean.LabelEncyclopedia;
import com.module.home.model.bean.ManualPosition;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsBrand;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.home.model.bean.ProjectDetailsListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/2/22
 */
public class ProjectDetailsFragment extends YMBaseFragment {

    @BindView(R.id.project_details_fragment_appBar)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.project_details_fragment_top)
    LinearLayout mFragmentTop;
    @BindView(R.id.project_details_fragment_manual)
    RecyclerView mFragmentManual;                                     //手工位设置
    @BindView(R.id.project_details_fragment_label)
    CardView mFragmentLabel;
    @BindView(R.id.project_details_fragment_label_title)
    TextView mFragmentLabelTitle;
    @BindView(R.id.project_details_fragment_label_content)
    TextView mFragmentLabelContent;
    @BindView(R.id.project_details_fragment_rec)
    RecyclerView mFragmentRec;
    @BindView(R.id.project_details_fragment_brand)
    LinearLayout mFragmentBrand;
    @BindView(R.id.project_details_fragment_brand_all)
    TextView mFragmentBrandAll;
    @BindView(R.id.project_details_fragment_brand_rec)
    RecyclerView mFragmentBrandRec;
    @BindView(R.id.project_details_fragment_tab_latoyt)
    TabLayout mTabLayout;
    @BindView(R.id.project_details_fragment_view_page)
    ViewPager mViewPage;

    private final String TAG = "ProjectDetailsFragment";
    private List<YMBaseFragment> mFragmentList = new ArrayList<>();
    private List<String> mPageTitleList = new ArrayList<>();
    private ProjectDetailsApi mProjectDetailsApi;
    private ArrayList<ProjectDetailsListData> mFragmentData;
    private ProjectDetailsBean detailsBean;
    private int mPos;
    private BaseWebViewClientMessage webViewClientManager;
    private ProjectDetailsActivity activity;
    private PopupWindow mPopupWindow;
    private AppBarStateChangeListener.State mState;

    public static ProjectDetailsFragment newInstance(ArrayList<ProjectDetailsListData> data, ProjectDetailsBean bean, int pos) {
        ProjectDetailsFragment fragment = new ProjectDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("data", data);
        bundle.putParcelable("bean", bean);
        bundle.putInt("pos", pos);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_details;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mFragmentData = getArguments().getParcelableArrayList("data");
            detailsBean = getArguments().getParcelable("bean");
            mPos = getArguments().getInt("pos");

            Log.e(TAG, "11111data.getId() == " + detailsBean.getTwoLabelId());
            Log.e(TAG, "222222data.getId() == " + detailsBean.getFourLabelId());
        }

        mPageTitleList.add("商品");
        mPageTitleList.add("医院");
        mPageTitleList.add("医生");
        mPageTitleList.add("日记");
        mPageTitleList.add("问答");

        //设置标签
        if (mFragmentData != null) {
            setTag();
        }

        //设置联动
        setLinkage();
    }

    @Override
    protected void initData(View view) {
        mProjectDetailsApi = new ProjectDetailsApi();
        webViewClientManager = new BaseWebViewClientMessage(mContext);
        getProjectDetailsData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            mFragmentList.get(mTabLayout.getSelectedTabPosition()).onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * 获取页面数据
     */
    private void getProjectDetailsData() {

        Log.e(TAG, "homeSource == " + detailsBean.getHomeSource());
        Log.e(TAG, "parentLabelID == " + detailsBean.getTwoLabelId());

        mProjectDetailsApi.addData("parentLabelID", detailsBean.getTwoLabelId());
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId())) {
            Log.e(TAG, "labelID == " + detailsBean.getFourLabelId());
            mProjectDetailsApi.addData("labelID", detailsBean.getFourLabelId());
        }
        mProjectDetailsApi.addData("listType", "1");
        mProjectDetailsApi.addData("homeSource", detailsBean.getHomeSource());
        mProjectDetailsApi.addData("page", "1");
        mProjectDetailsApi.getCallBack(mContext, mProjectDetailsApi.getProjectDetailsHashMap(), new BaseCallBackListener<ProjectDetailsData>() {
            @Override
            public void onSuccess(final ProjectDetailsData data) {
                //设置手工位
                setManual(data.getManualPosition());
                //百科介绍
                setWikipedia(data.getLabelEncyclopedia());
                //品牌馆
                setBrand(data.getBrand());
                //TabLayout适配
                setTabLayout(data);
            }
        });
    }


    /**
     * 标签是否显示
     */
    private void setTag() {
        if (mFragmentData.size() != 0) {
            mFragmentRec.setVisibility(View.VISIBLE);
            mFragmentRec.setLayoutManager(new GridLayoutManager(mContext, 3, LinearLayoutManager.VERTICAL, false));
            ProjectDetailsTagAdapter detailsTagAdapter = new ProjectDetailsTagAdapter(mContext, mFragmentData);
            mFragmentRec.setAdapter(detailsTagAdapter);
            detailsTagAdapter.setOnEventClickListener(new ProjectDetailsTagAdapter.OnEventClickListener() {
                @Override
                public void onLoadedClick(ProjectDetailsListData data) {
                    detailsBean.setFourLabelId(data.getId());
                    getProjectDetailsData();
                }
            });
        } else {
            mFragmentRec.setVisibility(View.GONE);
        }
    }

    /**
     * 设置联动
     */
    private void setLinkage() {
        activity = (ProjectDetailsActivity) getActivity();
        if (activity != null) {
            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) mTabLayout.getLayoutParams();
            if (activity.tagDatas.size() > 2) {
                params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS | AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP);
                //设置联动
                mAppBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
                    @Override
                    public void onStateChanged(AppBarLayout appBarLayout, State state) {
                        Log.e(TAG, "state == " + state);
                        mState = state;
                        switch (state) {
                            case IDLE:
                                //TabLayout部分露出/全部露出
                                activity.mViewPage.setScanScroll(false);
                                break;
                            case COLLAPSED:
                                //TabLayout完全隐藏
                                activity.mViewPage.setScanScroll(true);
                                setPopwindows();
                                break;
                            case EXPANDED:
                                //最大化展开
                                activity.mViewPage.setScanScroll(false);
                                break;
                        }
                    }
                });
            } else {
                params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP);
            }
            mTabLayout.setLayoutParams(params);
        }
    }

    /**
     * 设置手工位
     *
     * @param manualPosition
     */
    private void setManual(ArrayList<ManualPosition> manualPosition) {
        ScrollLayoutManager scrollLinear = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFragmentManual.setLayoutManager(scrollLinear);
        ProjectDetailsManualAdapter detailsManualAdapter = new ProjectDetailsManualAdapter(mContext, manualPosition, mPos, null);
        mFragmentManual.setAdapter(detailsManualAdapter);
    }

    /**
     * 百科介绍
     *
     * @param labelEncyclopedia
     */
    private void setWikipedia(final LabelEncyclopedia labelEncyclopedia) {
        if (!TextUtils.isEmpty(labelEncyclopedia.getId())) {
            mFragmentLabel.setVisibility(View.VISIBLE);
            mFragmentLabelTitle.setText(labelEncyclopedia.getTitle());
            mFragmentLabelContent.setText(labelEncyclopedia.getDesc());
            mFragmentLabel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.tongjiApp(mContext, "encyclopedia", "1", labelEncyclopedia.getId(), "96");
                    webViewClientManager.showWebDetail(labelEncyclopedia.getUrl());
                }
            });
        } else {
            mFragmentLabel.setVisibility(View.GONE);
        }
    }

    /**
     * 品牌馆
     *
     * @param brand
     */
    private void setBrand(final ProjectDetailsBrand brand) {
        Log.e(TAG, "brand.getBrand_list() == " + brand.getBrand_list().size());
        if (brand.getBrand_list().size() != 0) {
            mFragmentBrand.setVisibility(View.VISIBLE);
            mFragmentBrandRec.setVisibility(View.VISIBLE);

            if ("1".equals(brand.getIsShowAllBtn())) {
                mFragmentBrandAll.setVisibility(View.VISIBLE);
                mFragmentBrandAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        webViewClientManager.showWebDetail(brand.getJumpUrl());
                    }
                });
            } else {
                mFragmentBrandAll.setVisibility(View.GONE);
            }
            mFragmentBrandRec.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            ProjectBrandAdapter projectBrandAdapter = new ProjectBrandAdapter(mContext, brand.getBrand_list());
            mFragmentBrandRec.setAdapter(projectBrandAdapter);

        } else {
            mFragmentBrand.setVisibility(View.GONE);
            mFragmentBrandRec.setVisibility(View.GONE);
        }
    }


    /**
     * TabLayout适配
     *
     * @param data
     */
    private void setTabLayout(final ProjectDetailsData data) {
        mFragmentTop.post(new Runnable() {
            @Override
            public void run() {
                int mTopHeight = mFragmentTop.getMeasuredHeight();
                detailsBean.setTopHeight(mTopHeight);
                Log.e(TAG, "mTopHeight == " + mTopHeight);

                if (mFragmentList.size() == 0) {
                    ProjectSkuFragment projectSkuFragment = ProjectSkuFragment.newInstance(data.getTaoList(), data.getComparedConsultative(), detailsBean);
                    ProjectHospitalFragment projectHospitalFragment = ProjectHospitalFragment.newInstance(data.getHospitalList(), detailsBean);
                    ProjectDoctorFragment projectDoctorFragment = ProjectDoctorFragment.newInstance(data.getDoctorsList(), detailsBean);
                    mFragmentList.add(projectSkuFragment);
                    mFragmentList.add(projectHospitalFragment);
                    mFragmentList.add(projectDoctorFragment);
                    mFragmentList.add(ProjectDiaryFragment.newInstance(data.getDiaryList(), detailsBean));
                    mFragmentList.add(ProjectAnswerFragment.newInstance(data.getQuestionList(), detailsBean));

//                    projectSkuFragment.setOnEventClickListener(new ProjectSkuFragment.OnEventClickListener() {
//                        @Override
//                        public void onPopupWindowClick(PopupWindow popupWindow) {
//                            mPopupWindow = popupWindow;
//                            if (mState != AppBarStateChangeListener.State.COLLAPSED) {
//                                mAppBarLayout.setExpanded(false, true);
//                            } else {
//                                setPopwindows();
//                            }
//                        }
//                    });

                    projectHospitalFragment.setOnEventClickListener(new ProjectHospitalFragment.OnEventClickListener() {
                        @Override
                        public void onPopupWindowClick(PopupWindow popupWindow) {
                            mPopupWindow = popupWindow;
                            if (mState != AppBarStateChangeListener.State.COLLAPSED) {
                                mAppBarLayout.setExpanded(false, true);
                            } else {
                                setPopwindows();
                            }
                        }
                    });

                    projectDoctorFragment.setOnEventClickListener(new ProjectDoctorFragment.OnEventClickListener() {
                        @Override
                        public void onPopupWindowClick(PopupWindow popupWindow) {
                            mPopupWindow = popupWindow;
                            if (mState != AppBarStateChangeListener.State.COLLAPSED) {
                                mAppBarLayout.setExpanded(false, true);
                            } else {
                                setPopwindows();
                            }
                        }
                    });


                    YMTabLayoutAdapter mPagerAdapter = new YMTabLayoutAdapter(getChildFragmentManager(), mPageTitleList, mFragmentList);
                    mViewPage.setAdapter(mPagerAdapter);
                    mTabLayout.setupWithViewPager(mViewPage);

                    YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(17), Utils.dip2px(17));
                } else {
                    for (YMBaseFragment fragment : mFragmentList) {
                        if (fragment instanceof ProjectSkuFragment) {

                            ProjectSkuFragment projectSkuFragment = (ProjectSkuFragment) fragment;
                            projectSkuFragment.setRefreshData(data.getTaoList(), data.getComparedConsultative(), detailsBean);

                        } else if (fragment instanceof ProjectHospitalFragment) {

                            ProjectHospitalFragment projectHosFragment = (ProjectHospitalFragment) fragment;
                            projectHosFragment.setRefreshData(data.getHospitalList(), detailsBean);

                        } else if (fragment instanceof ProjectDoctorFragment) {

                            Log.e(TAG, "data.getDoctorsList() == " + data.getDoctorsList());
                            Log.e(TAG, "mLabelId == " + detailsBean.getFourLabelId());
                            Log.e(TAG, "mHomeSource == " + detailsBean.getHomeSource());
                            Log.e(TAG, "mTopHeight == " + mTopHeight);
                            ProjectDoctorFragment projectDocFragment = (ProjectDoctorFragment) fragment;
                            projectDocFragment.setRefreshData(data.getDoctorsList(), detailsBean);

                        } else if (fragment instanceof ProjectDiaryFragment) {

                            ProjectDiaryFragment projectDiaryFragment = (ProjectDiaryFragment) fragment;
                            projectDiaryFragment.setRefreshData(data.getDiaryList(), detailsBean);
                        }
                    }

                }
            }
        });
    }


    /**
     * 筛选的显示隐藏
     */
    private void setPopwindows() {
        if (mPopupWindow != null) {
            if (mPopupWindow instanceof BaseCityPopwindows) {
                BaseCityPopwindows baseCityPopwindows = (BaseCityPopwindows) mPopupWindow;
                baseCityPopwindows.showPop();
            } else if (mPopupWindow instanceof BaseSortPopupwindows) {
                BaseSortPopupwindows baseSortPopupwindows = (BaseSortPopupwindows) mPopupWindow;
                baseSortPopupwindows.showPop();
            } else if (mPopupWindow instanceof SortScreenPopwin) {
                SortScreenPopwin sortScreenPopwin = (SortScreenPopwin) mPopupWindow;
                sortScreenPopwin.showPop();
            }
        }

        mPopupWindow = null;
    }
}
