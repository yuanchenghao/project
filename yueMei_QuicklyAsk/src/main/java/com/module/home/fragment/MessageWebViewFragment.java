package com.module.home.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.my.controller.activity.BBsFinalWebActivity;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * 回复、通知
 * Created by 裴成浩 on 2017/10/18.
 */

public class MessageWebViewFragment extends Fragment {

    private String mUrl;
    private MessageFragmentActivity1 mActivity;
    private WebView contentWeb;
    private SmartRefreshLayout ymRefreshHead;
    private LinearLayout webContainer;
    private String TAG = "MessageWebViewFragment";
    public JSONObject obj_http;
    private BaseWebViewClientMessage mBaseWebViewClientMessage;
    HashMap<String, Object> mSingStr = new HashMap<>();
    public MessageWebViewFragment(){}

    public static MessageWebViewFragment newInstance(MessageFragmentActivity1 activity, String url, String type) {
        MessageWebViewFragment fragment = new MessageWebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("type", type);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page3, container, false);
        ymRefreshHead = view.findViewById(R.id.community_web_view);
        webContainer = view.findViewById(R.id.acty_tao_container);
        mBaseWebViewClientMessage = new BaseWebViewClientMessage(getActivity());
        mBaseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });
        mBaseWebViewClientMessage.startLoading();
        Bundle arguments = getArguments();
        mUrl= arguments.getString("url");
        String type = arguments.getString("type");
        mSingStr.put("type",type);
        initWebview();
        ymRefreshHead.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadAction();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        contentWeb.clearCache(true);
        loadAction();

    }

    /**
     * 加载url
     */
    public void loadAction() {

        Log.e(TAG,"url111====" + mUrl);
        Log.e(TAG,"mSingStr====" + mSingStr);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(mUrl,mSingStr);
        Log.e(TAG,"url====" + addressAndHead.getUrl());
        Log.e(TAG,"httpHeaders====" + addressAndHead.getHttpHeaders());
        contentWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void initWebview() {
        contentWeb=new WebView(getActivity());
        contentWeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        contentWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        contentWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        contentWeb.getSettings().setJavaScriptEnabled(true);
        contentWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        contentWeb.getSettings().setLoadsImagesAutomatically(true);    //支持自动加载图片
        contentWeb.getSettings().setUseWideViewPort(true);
        contentWeb.getSettings().setLoadWithOverviewMode(true);
        contentWeb.getSettings().setSaveFormData(true);    //设置webview保存表单数据
        contentWeb.getSettings().setSavePassword(true);    //设置webview保存密码
        contentWeb.getSettings().supportMultipleWindows();
        contentWeb.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        contentWeb.getSettings().setSupportZoom(true);
        contentWeb.getSettings().setDefaultTextEncodingName("UTF-8"); // 设置默认的显示编码
        contentWeb.getSettings().setNeedInitialFocus(true);
        contentWeb.setWebViewClient(mBaseWebViewClientMessage);
        contentWeb.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100){
                    ymRefreshHead.finishRefresh();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            contentWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webContainer.addView(contentWeb);
    }

    public void showWebDetail(String urlStr) {
        Log.e("message_Url======", urlStr);
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (Utils.isFastDoubleClick()) {
                return;
            }

            if (obj.getString("type").equals("1")) {// 问题详情页
                try {
                    Log.e(TAG, "obj111111 == " + obj);
                    String link = obj.getString("link");
                    String qid = obj.getString("id");
                    String askorshare = obj.getString("askorshare");
                    Intent it = new Intent();
                    it.putExtra("url", link);
                    it.putExtra("qid", qid);
                    it.putExtra("askorshare", askorshare);
                    it.setClass(mActivity, DiariesAndPostsActivity.class);
                    startActivity(it);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (obj.getString("type").equals("6")) {// 查看更多问题

                try {
                    if (urlStr.contains("userid")) {
                        Log.e(TAG, "obj22222== " + obj);
                        String link = obj.getString("link");
                        String userid = obj.getString("userid");
                        String id = obj.getString("id");
                        String askorshare = obj.getString("askorshare");
                        Intent it = new Intent();
                        it.setClass(mActivity, BBsFinalWebActivity.class);
                        it.putExtra("url", link);
                        it.putExtra("askorshare", askorshare);
                        it.putExtra("louc", "0");
                        it.putExtra("qid", id);
                        it.putExtra("userid", userid);
                        it.putExtra("typeroot", "0");
                        startActivity(it);
                    } else {
                        Log.e(TAG, "obj3333 == " + obj);
                        String link = obj.getString("link");
                        String askorshare = obj.getString("askorshare");
                        String id = obj.getString("id");
                        Intent it = new Intent();
                        it.setClass(mActivity, BBsFinalWebActivity.class);
                        it.putExtra("url", link);
                        it.putExtra("askorshare", askorshare);
                        it.putExtra("louc", "0");
                        it.putExtra("qid", id);
                        it.putExtra("userid", "0");
                        it.putExtra("typeroot", "0");
                        startActivity(it);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


//    private boolean isVisibleToUser;
//
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        this.isVisibleToUser = isVisibleToUser;
//        if (isVisibleToUser) {
//    // TODO：1，如果在Fragment中加载数据时可在这个方法中实现，注意：当这个方法执行时可能页面还没有初始化完成
//        }
//    }


}

