package com.module.home.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/4/3
 */
public class ManualPosition {
    private ManualPositionTag list_one;                              //标签类型1
    private ManualPositionTag list_two;                              //标签类型2
    private ManualPositionTag list_three;                          //标签类型3
    private String recommendTitle;                                  //title
    private String moreUrl;                                         //跳转
    private List<ProjectFocuMapData> focusImage;                    //轮播图
    private List<List<ProjectFocuMapData>> metro;                   //手工位
    private List<HotBean> hotTao;                                   //特卖
    private List<ProjectFocuMapData> activity;                      //活动
    private List<BbsBean> chosenDiary;                              //教你闭坑
    private List<RecommendDoctors> recommendDoctors;                //医院列表
    private List<RecommendHospital> recommendHospital;              //医生列表


    public ManualPositionTag getListOne() {
        return list_one;
    }

    public void setListOne(ManualPositionTag listOne) {
        this.list_one = listOne;
    }

    public ManualPositionTag getListTwo() {
        return list_two;
    }

    public void setListTwo(ManualPositionTag listTwo) {
        this.list_two = listTwo;
    }

    public ManualPositionTag getListThree() {
        return list_three;
    }

    public void setListThree(ManualPositionTag listThree) {
        this.list_three = listThree;
    }

    public String getRecommendTitle() {
        return recommendTitle;
    }

    public void setRecommendTitle(String recommendTitle) {
        this.recommendTitle = recommendTitle;
    }

    public String getMoreUrl() {
        return moreUrl;
    }

    public void setMoreUrl(String moreUrl) {
        this.moreUrl = moreUrl;
    }

    public List<ProjectFocuMapData> getFocusImage() {
        return focusImage;
    }

    public void setFocusImage(List<ProjectFocuMapData> focusImage) {
        this.focusImage = focusImage;
    }

    public List<List<ProjectFocuMapData>> getMetro() {
        return metro;
    }

    public void setMetro(List<List<ProjectFocuMapData>> metro) {
        this.metro = metro;
    }

    public List<HotBean> getHotTao() {
        return hotTao;
    }

    public void setHotTao(List<HotBean> hotTao) {
        this.hotTao = hotTao;
    }

    public List<ProjectFocuMapData> getActivity() {
        return activity;
    }

    public void setActivity(List<ProjectFocuMapData> activity) {
        this.activity = activity;
    }

    public List<BbsBean> getChosenDiary() {
        return chosenDiary;
    }

    public void setChosenDiary(List<BbsBean> chosenDiary) {
        this.chosenDiary = chosenDiary;
    }

    public List<RecommendDoctors> getRecommendDoctors() {
        return recommendDoctors;
    }

    public void setRecommendDoctors(List<RecommendDoctors> recommendDoctors) {
        this.recommendDoctors = recommendDoctors;
    }

    public List<RecommendHospital> getRecommendHospital() {
        return recommendHospital;
    }

    public void setRecommendHospital(List<RecommendHospital> recommendHospital) {
        this.recommendHospital = recommendHospital;
    }
}
