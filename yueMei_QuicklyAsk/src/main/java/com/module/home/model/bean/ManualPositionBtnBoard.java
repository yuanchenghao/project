package com.module.home.model.bean;

import java.util.HashMap;
import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/5
 */
public class ManualPositionBtnBoard {
    private String title;
    private String channel_title;
    private String url;
    private String back_img;
    private String id;
    private String one_id;
    private String level;
    private List<String> back_color;
    private HashMap<String,String> event_params;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel_title() {
        return channel_title;
    }

    public void setChannel_title(String channel_title) {
        this.channel_title = channel_title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBack_img() {
        return back_img;
    }

    public void setBack_img(String back_img) {
        this.back_img = back_img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOne_id() {
        return one_id;
    }

    public void setOne_id(String one_id) {
        this.one_id = one_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<String> getBack_color() {
        return back_color;
    }

    public void setBack_color(List<String> back_color) {
        this.back_color = back_color;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
