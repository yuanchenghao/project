package com.module.home.model.bean;

import com.module.community.model.bean.CommunityStaggeredListData;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/6/18
 */
public class SearchResultBBsData {
    private List<CommunityStaggeredListData> list;
    private String total;
    private String desc;
    private String type;

    public List<CommunityStaggeredListData> getList() {
        return list;
    }

    public void setList(List<CommunityStaggeredListData> list) {
        this.list = list;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
