package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2018/4/21.
 */

public class CashBackBean implements Parcelable {

    /**
     * is_cashback : 0
     * cashback_complete :
     */

    private String is_cashback;
    private String cashback_complete;

    protected CashBackBean(Parcel in) {
        is_cashback = in.readString();
        cashback_complete = in.readString();
    }

    public static final Creator<CashBackBean> CREATOR = new Creator<CashBackBean>() {
        @Override
        public CashBackBean createFromParcel(Parcel in) {
            return new CashBackBean(in);
        }

        @Override
        public CashBackBean[] newArray(int size) {
            return new CashBackBean[size];
        }
    };

    public String getIs_cashback() {
        return is_cashback;
    }

    public void setIs_cashback(String is_cashback) {
        this.is_cashback = is_cashback;
    }

    public String getCashback_complete() {
        return cashback_complete;
    }

    public void setCashback_complete(String cashback_complete) {
        this.cashback_complete = cashback_complete;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(is_cashback);
        dest.writeString(cashback_complete);
    }
}
