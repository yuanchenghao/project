package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * Created by 裴成浩 on 2017/12/27.
 */

public class XiaoxiChatUpdatereadApi implements BaseCallBackApi {

    private String TAG = "XiaoxiChatUpdatereadApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.CHAT, "updateread", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "执行到此 === " + mData.toString());
                listener.onSuccess(mData);
            }
        });
    }
}