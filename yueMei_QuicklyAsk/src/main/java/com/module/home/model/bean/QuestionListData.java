package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/5/14
 */
public class QuestionListData implements Parcelable {
    private String id;
    private String title;
    private String img;
    private QuestionListReplyData replyData;
    private List<String> tagList;
    private String viewNum;
    private String answerNum;
    private String agreeNum;
    private String isAgree;
    private String userID;
    private String jumpUrl;
    private HashMap<String,String> event_params;

    protected QuestionListData(Parcel in) {
        id = in.readString();
        title = in.readString();
        img = in.readString();
        replyData = in.readParcelable(QuestionListReplyData.class.getClassLoader());
        tagList = in.createStringArrayList();
        viewNum = in.readString();
        answerNum = in.readString();
        agreeNum = in.readString();
        isAgree = in.readString();
        userID = in.readString();
        jumpUrl = in.readString();
        event_params = (HashMap<String, String>) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(img);
        dest.writeParcelable(replyData, flags);
        dest.writeStringList(tagList);
        dest.writeString(viewNum);
        dest.writeString(answerNum);
        dest.writeString(agreeNum);
        dest.writeString(isAgree);
        dest.writeString(userID);
        dest.writeString(jumpUrl);
        dest.writeSerializable(event_params);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionListData> CREATOR = new Creator<QuestionListData>() {
        @Override
        public QuestionListData createFromParcel(Parcel in) {
            return new QuestionListData(in);
        }

        @Override
        public QuestionListData[] newArray(int size) {
            return new QuestionListData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public QuestionListReplyData getReplyData() {
        return replyData;
    }

    public void setReplyData(QuestionListReplyData replyData) {
        this.replyData = replyData;
    }

    public List<String> getTagList() {
        return tagList;
    }

    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    public String getViewNum() {
        return viewNum;
    }

    public void setViewNum(String viewNum) {
        this.viewNum = viewNum;
    }

    public String getAnswerNum() {
        return answerNum;
    }

    public void setAnswerNum(String answerNum) {
        this.answerNum = answerNum;
    }

    public String getAgreeNum() {
        return agreeNum;
    }

    public void setAgreeNum(String agreeNum) {
        this.agreeNum = agreeNum;
    }

    public String getIsAgree() {
        return isAgree;
    }

    public void setIsAgree(String isAgree) {
        this.isAgree = isAgree;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
