package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/6/10
 */
public class SearchResultDoctorControlParams implements Parcelable {

    /**
     * isHide : 1
     * isRefresh : 0
     * enableSafeArea : 1
     * webType : 1
     * bgColor : #ffffff
     * enableBottomSafeArea : 1
     */

    private String isHide;
    private String isRefresh;
    private String enableSafeArea;
    private String webType;
    private String bgColor;
    private String enableBottomSafeArea;

    public String getIsHide() {
        return isHide;
    }

    public void setIsHide(String isHide) {
        this.isHide = isHide;
    }

    public String getIsRefresh() {
        return isRefresh;
    }

    public void setIsRefresh(String isRefresh) {
        this.isRefresh = isRefresh;
    }

    public String getEnableSafeArea() {
        return enableSafeArea;
    }

    public void setEnableSafeArea(String enableSafeArea) {
        this.enableSafeArea = enableSafeArea;
    }

    public String getWebType() {
        return webType;
    }

    public void setWebType(String webType) {
        this.webType = webType;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getEnableBottomSafeArea() {
        return enableBottomSafeArea;
    }

    public void setEnableBottomSafeArea(String enableBottomSafeArea) {
        this.enableBottomSafeArea = enableBottomSafeArea;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.isHide);
        dest.writeString(this.isRefresh);
        dest.writeString(this.enableSafeArea);
        dest.writeString(this.webType);
        dest.writeString(this.bgColor);
        dest.writeString(this.enableBottomSafeArea);
    }

    public SearchResultDoctorControlParams() {
    }

    protected SearchResultDoctorControlParams(Parcel in) {
        this.isHide = in.readString();
        this.isRefresh = in.readString();
        this.enableSafeArea = in.readString();
        this.webType = in.readString();
        this.bgColor = in.readString();
        this.enableBottomSafeArea = in.readString();
    }

    public static final Parcelable.Creator<SearchResultDoctorControlParams> CREATOR = new Parcelable.Creator<SearchResultDoctorControlParams>() {
        @Override
        public SearchResultDoctorControlParams createFromParcel(Parcel source) {
            return new SearchResultDoctorControlParams(source);
        }

        @Override
        public SearchResultDoctorControlParams[] newArray(int size) {
            return new SearchResultDoctorControlParams[size];
        }
    };
}
