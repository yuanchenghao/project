package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsListData550;
import com.module.home.model.bean.ShareListNewData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import simplecache.ACache;

/**
 * 首页日记流
 * Created by Administrator on 2018/2/27.
 */

public class LoadDataApi implements BaseCallBackApi {
    private String TAG = "LoadDataApi";
    private HashMap<String, Object> mLoadDataHashMap;  //传值容器
    private final Context mContext;

    public LoadDataApi(Context context) {
        this.mContext = context;
        mLoadDataHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.HOME_NEW, "sharelistnew", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
              listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getLoadDataHashMap() {
        return mLoadDataHashMap;
    }

    public void addData(String key, String value) {
        mLoadDataHashMap.put(key, value);
    }
}
