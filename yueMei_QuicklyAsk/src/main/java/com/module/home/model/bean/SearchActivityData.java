package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchActivityData implements Parcelable {
    private String main_pic; //模块主图
    private String secondary_pic; //模块副图
    private String title; //模块标题
    private String subtitle; //模块副标题（如果时间存在则副标题为空优先展示倒计时）
    private String start_time; //开始时间
    private String end_time; //结束时间
    private String jumpUrl; //模块跳转地址
    private List<SearchTaolistData> taoList;

    private HashMap<String,String> event_params;
    private HashMap<String,String> exposure_event_params;

    public String getMain_pic() {
        return main_pic;
    }

    public void setMain_pic(String main_pic) {
        this.main_pic = main_pic;
    }

    public String getSecondary_pic() {
        return secondary_pic;
    }

    public void setSecondary_pic(String secondary_pic) {
        this.secondary_pic = secondary_pic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    public List<SearchTaolistData> getTaoList() {
        return taoList;
    }

    public void setTaoList(List<SearchTaolistData> taoList) {
        this.taoList = taoList;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public HashMap<String, String> getExposure_event_params() {
        return exposure_event_params;
    }

    public void setExposure_event_params(HashMap<String, String> exposure_event_params) {
        this.exposure_event_params = exposure_event_params;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.main_pic);
        dest.writeString(this.secondary_pic);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.start_time);
        dest.writeString(this.end_time);
        dest.writeString(this.jumpUrl);
        dest.writeList(this.taoList);
        dest.writeSerializable(this.event_params);
        dest.writeSerializable(this.exposure_event_params);
    }

    public SearchActivityData() {
    }

    protected SearchActivityData(Parcel in) {
        this.main_pic = in.readString();
        this.secondary_pic = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.start_time = in.readString();
        this.end_time = in.readString();
        this.jumpUrl = in.readString();
        this.taoList = new ArrayList<SearchTaolistData>();
        in.readList(this.taoList, SearchTaolistData.class.getClassLoader());
        this.event_params = (HashMap<String, String>) in.readSerializable();
        this.exposure_event_params = (HashMap<String, String>) in.readSerializable();
    }

    public static final Parcelable.Creator<SearchActivityData> CREATOR = new Parcelable.Creator<SearchActivityData>() {
        @Override
        public SearchActivityData createFromParcel(Parcel source) {
            return new SearchActivityData(source);
        }

        @Override
        public SearchActivityData[] newArray(int size) {
            return new SearchActivityData[size];
        }
    };
}
