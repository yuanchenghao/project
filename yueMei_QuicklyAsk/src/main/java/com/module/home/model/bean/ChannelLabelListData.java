package com.module.home.model.bean;

/**
 * @author 裴成浩
 * @data 2019/11/5
 */
public class ChannelLabelListData {
    private ManualPositionTag module_178_184;                      //标签类型1
    private ManualPositionTag module_187;                           //标签类型2
    private ManualPositionTag module_181;                          //标签类型3

    public ManualPositionTag getModule_178_184() {
        return module_178_184;
    }

    public void setModule_178_184(ManualPositionTag module_178_184) {
        this.module_178_184 = module_178_184;
    }

    public ManualPositionTag getModule_187() {
        return module_187;
    }

    public void setModule_187(ManualPositionTag module_187) {
        this.module_187 = module_187;
    }

    public ManualPositionTag getModule_181() {
        return module_181;
    }

    public void setModule_181(ManualPositionTag module_181) {
        this.module_181 = module_181;
    }
}
