package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.doctor.model.bean.HosListData;

import java.util.ArrayList;
import java.util.List;

public class SearchTitleData implements Parcelable {

    private String desc;
    private String type;
    private String total;
    private List<SearchTao> list;
    private SearchTitleList data;



    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<SearchTao> getList() {
        return list;
    }

    public void setList(List<SearchTao> list) {
        this.list = list;
    }

    public SearchTitleList getData() {
        return data;
    }

    public void setData(SearchTitleList data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.desc);
        dest.writeString(this.type);
        dest.writeString(this.total);
        dest.writeList(this.list);
        dest.writeParcelable(this.data, flags);
    }

    public SearchTitleData() {
    }

    protected SearchTitleData(Parcel in) {
        this.desc = in.readString();
        this.type = in.readString();
        this.total = in.readString();
        this.list = new ArrayList<SearchTao>();
        in.readList(this.list, SearchTao.class.getClassLoader());
        this.data = in.readParcelable(SearchTitleList.class.getClassLoader());
    }

    public static final Parcelable.Creator<SearchTitleData> CREATOR = new Parcelable.Creator<SearchTitleData>() {
        @Override
        public SearchTitleData createFromParcel(Parcel source) {
            return new SearchTitleData(source);
        }

        @Override
        public SearchTitleData[] newArray(int size) {
            return new SearchTitleData[size];
        }
    };
}
