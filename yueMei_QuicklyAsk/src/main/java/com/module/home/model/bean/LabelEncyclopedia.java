package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class LabelEncyclopedia implements Parcelable {
    private String id;                          //标签id
    private String title;                       //标签名称
    private String desc;                        //标签百科介绍
    private String url;                         //url
    private String effective_time;              //维持时间（例如:维持时间：持久）
    private String reference_price;             //参考价格（例如:参考价格：4000-10000元/次）
    private List<LabelEncyclopediaBtnList> btn_list;  //按钮
    private String back_img;                    //百科背景图
    private HashMap<String,String> event_params;

    protected LabelEncyclopedia(Parcel in) {
        id = in.readString();
        title = in.readString();
        desc = in.readString();
        url = in.readString();
        effective_time = in.readString();
        reference_price = in.readString();
        back_img = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(url);
        dest.writeString(effective_time);
        dest.writeString(reference_price);
        dest.writeString(back_img);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LabelEncyclopedia> CREATOR = new Creator<LabelEncyclopedia>() {
        @Override
        public LabelEncyclopedia createFromParcel(Parcel in) {
            return new LabelEncyclopedia(in);
        }

        @Override
        public LabelEncyclopedia[] newArray(int size) {
            return new LabelEncyclopedia[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEffective_time() {
        return effective_time;
    }

    public void setEffective_time(String effective_time) {
        this.effective_time = effective_time;
    }

    public String getReference_price() {
        return reference_price;
    }

    public void setReference_price(String reference_price) {
        this.reference_price = reference_price;
    }

    public List<LabelEncyclopediaBtnList> getBtn_list() {
        return btn_list;
    }

    public void setBtn_list(List<LabelEncyclopediaBtnList> btn_list) {
        this.btn_list = btn_list;
    }

    public String getBack_img() {
        return back_img;
    }

    public void setBack_img(String back_img) {
        this.back_img = back_img;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
