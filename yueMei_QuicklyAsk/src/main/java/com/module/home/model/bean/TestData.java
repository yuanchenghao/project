package com.module.home.model.bean;

import java.util.List;

public class TestData {

    /**
     * search_method : []
     * baikeCompare : {"id":"duibi","name":"对比","img":"","url":"","event_params":{"event_name":"search_baikecompare"}}
     * search_partChild : []
     * screen_board : [{"id":"1","title":"尾款红包","postName":"deposit","postVal":"1","image":{"before":"","after":""},"event_params":{"event_name":"search_board","type":39,"event_pos":1}},{"id":"2","title":"首件优惠","postName":"isfirst","postVal":"1","image":{"before":"","after":""},"event_params":{"event_name":"search_board","type":39,"event_pos":2}},{"id":"3","title":"买多优惠","postName":"ismulti","postVal":"1","image":{"before":"","after":""},"event_params":{"event_name":"search_board","type":39,"event_pos":3}},{"id":"4","title":"PLUS会员价","postName":"member","postVal":"1","image":{"before":"","after":""},"event_params":{"event_name":"search_board","type":39,"event_pos":4}},{"id":"6","title":"信用卡分期","postName":"repayment","postVal":"1","image":{"before":"","after":""},"event_params":{"event_name":"search_board","type":39,"event_pos":6}}]
     * total : 66
     * list : [{"is_show_member":"1","pay_dingjin":"1100","number":"20","start_number":"1","pay_price_discount":"5500","member_price":"5225","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0430/200_200/jt190430163329_937fbf.jpg","title":"面部吸脂","subtitle":"①全面部吸脂瘦脸②可吸面颊/下颌缘/双下巴/嘴角③私信送现金红包/脱毛","hos_name":"悦美好医医疗美容门诊部","doc_name":"门智和","price":"9800","price_discount":"5500","price_range_max":"0","id":"142036","_id":"142036","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满1000减88,满2000减188,满5000减488","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"36人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"10647","doctor_id":"84987073","is_rongyun":"3","hos_userid":"700893","business_district":"中关村","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/21/30/85_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/21/31/13_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/21/45/03_avatar_50_50.jpg"],"surgInfo":{"url_name":"faceslimm","name":"瘦脸","alias":"","id":"329","title":"瘦脸"},"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":142036,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"distance":"17.5km"},{"is_show_member":"1","pay_dingjin":"200","number":"20","start_number":"1","pay_price_discount":"999","member_price":"949","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0220/200_200/jt190220175916_4b59ea.jpg","title":"医学博士吸脂瘦脸","subtitle":"北京吸脂瘦脸compact面部精雕 单部位","hos_name":"北京蜜邦医疗美容诊所","doc_name":"栾志宏","price":"4000","price_discount":"999","price_range_max":"0","id":"117166","_id":"117166","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满1000减50,满2000减100,满5000减300,满10000减1000","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"1803人预订","feeScale":"/部位","is_fanxian":"0","promotion":[],"hospital_id":"12268","doctor_id":"","is_rongyun":"3","hos_userid":"85652638","business_district":"亚运村","hospital_top":{"level":2,"desc":"北京面部轮廓月销TOP2医院"},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/09/53/49_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/09/54/57_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/09/54/89_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":117166,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"7.5km"},{"is_show_member":"0","pay_dingjin":"150","number":"10","start_number":"1","pay_price_discount":"704","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0323/200_200/jt190323132545_9c5ef6.jpg","title":"吸脂瘦脸","subtitle":"（朱金成面吸）①CCTV-1邀访②2大吸脂专利③3大填充专利④30余年博士团队","hos_name":"北京润美玉之光医疗美容门诊部","doc_name":"朱金成","price":"1980","price_discount":"704","price_range_max":"0","id":"203614","_id":"203614","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"1536人预订","feeScale":"/部位","is_fanxian":"0","promotion":[],"hospital_id":"3354","doctor_id":"","is_rongyun":"3","hos_userid":"85280507","business_district":"亚运村","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/31/79/59_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/32/29/41_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/32/32/91_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":203614,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"7.3km"},{"is_show_member":"0","pay_dingjin":"800","number":"20","start_number":"1","pay_price_discount":"3980","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0703/200_200/jt190703140954_279957.jpg","title":"吸脂瘦脸","subtitle":"双下巴超V吸脂  V脸轮廓重塑 打造流畅下颌缘","hos_name":"煤炭总医院","doc_name":"李艳","price":"7100","price_discount":"3980","price_range_max":"0","id":"217018","_id":"217018","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满100减5","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"2864","doctor_id":"","is_rongyun":"3","hos_userid":"85279475","business_district":"太阳宫","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/21/31/13_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/21/45/03_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/21/45/41_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":217018,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"7.7km"},{"is_show_member":"0","pay_dingjin":"1000","number":"20","start_number":"1","pay_price_discount":"5000","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0727/200_200/jt190727155004_c9862e.jpg","title":"吸脂瘦脸","subtitle":"下颌缘+双下巴吸脂 告别婴儿肥嘟嘟脸 精致上镜脸  美得刚刚好","hos_name":"北京京通医院","doc_name":"陈莉","price":"29800","price_discount":"5000","price_range_max":"0","id":"213997","_id":"213997","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"11111","doctor_id":"","is_rongyun":"3","hos_userid":"85282827","business_district":"武夷花园","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/000/65/94/41_avatar_50_50.jpg","https://p21.yuemei.com/avatar/000/65/96/11_avatar_50_50.jpg","https://p21.yuemei.com/avatar/000/65/98/55_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":213997,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"20.3km"},{"is_show_member":"1","pay_dingjin":"240","number":"20","start_number":"1","pay_price_discount":"1180","member_price":"1121","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0304/200_200/jt190304191900_49e31b.jpg","title":"面部吸脂","subtitle":"北京V雕紧塑面部吸脂 不磨骨即可实现紧致小巧脸型 全方位精雕紧致童颜发日记高返现","hos_name":"北京炫美医疗美容诊所","doc_name":"周亮","price":"9800","price_discount":"1180","price_range_max":"0","id":"55831","_id":"55831","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"480人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"3340","doctor_id":"","is_rongyun":"3","hos_userid":"677401","business_district":"大望路","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/21/45/57_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/23/64/27_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/23/64/67_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":55831,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"12.8km"},{"is_show_member":"1","pay_dingjin":"280","number":"20","start_number":"1","pay_price_discount":"1380","member_price":"1311","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0624/200_200/jt190624162940_42c804.jpg","title":"吸脂瘦脸","subtitle":"单部位面部吸脂 从U到V的改变 瘦出高级脸 轻松改善面部轮廓","hos_name":"北京华悦府医疗美容诊所","doc_name":"王宁博","price":"4800","price_discount":"1380","price_range_max":"0","id":"198136","_id":"198136","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满3000减200,满10000减800,满30000减3000","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"376人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"10755","doctor_id":"","is_rongyun":"3","hos_userid":"85007051","business_district":"姚家园","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/63/33/69_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/63/35/10_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/63/35/43_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":198136,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"8.9km"},{"is_show_member":"0","pay_dingjin":"400","number":"1","start_number":"1","pay_price_discount":"1979","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0722/200_200/jt190722155821_d8abea.jpg","title":"吸脂瘦脸","subtitle":"多维立体面部吸脂/单部位❤新客下单送面膜 ①轻松甩掉嘟嘟脸②恢复期短③上镜网红脸","hos_name":"北京京城皮肤医院","doc_name":"管乐","price":"3980","price_discount":"1979","price_range_max":"0","id":"200449","_id":"200449","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"299人预订","feeScale":"/部位","is_fanxian":"1","promotion":[],"hospital_id":"7961","doctor_id":"","is_rongyun":"3","hos_userid":"85124417","business_district":"奥运村","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://www.yuemei.com/images/weibo/noavatar1_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar4_50_50.jpg","https://www.yuemei.com/images/weibo/noavatar5_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":200449,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"11.1km"},{"is_show_member":"0","pay_dingjin":"380","number":"20","start_number":"1","pay_price_discount":"1880","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0712/200_200/jt190712172150_e637a4.png","title":"吸脂减双下巴","subtitle":"吸脂瘦脸 @李朕院长亲传大弟子 幼幼脸/小V脸/高级脸大力推荐","hos_name":"北京圣嘉新医疗美容医院","doc_name":"杨佳琦","price":"8000","price_discount":"1880","price_range_max":"0","id":"152929","_id":"152929","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"202人预订","feeScale":"/部位","is_fanxian":"1","promotion":[],"hospital_id":"7307","doctor_id":"","is_rongyun":"3","hos_userid":"85141983","business_district":"西直门","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/09/65/71_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/09/66/81_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/09/76/11_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":152929,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"14.7km"},{"is_show_member":"1","pay_dingjin":"1400","number":"1","start_number":"1","pay_price_discount":"6990","member_price":"6640","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2018/1221/200_200/jt181221112708_c00c9b.jpg","title":"面部吸脂","subtitle":"面部吸脂 精确定位塑形 下颌缘+双下巴优选 精致减龄 吸出上镜小脸 告别瘦脸特效","hos_name":"北京健宫医院","doc_name":"王沧娟","price":"16800","price_discount":"6990","price_range_max":"0","id":"142594","_id":"142594","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满1000减60","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"357人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"12325","doctor_id":"","is_rongyun":"3","hos_userid":"85834711","business_district":"天桥","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/11/78/25_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/35_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/43_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":142594,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"17.6km"},{"is_show_member":"1","pay_dingjin":"180","number":"20","start_number":"1","pay_price_discount":"880","member_price":"836","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0611/200_200/jt190611180836_415cc8.jpg","title":"减肥塑形","subtitle":"北京吸脂瘦脸 面部轮廓精雕 精致小V脸 安全精雕巴掌小脸 面部精细吸脂单部位","hos_name":"北京唯专呼家楼医疗美容诊所","doc_name":"丁雅晴","price":"9800","price_discount":"880","price_range_max":"0","id":"68955","_id":"68955","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"满1000减100","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"149人预订","feeScale":"/部位","is_fanxian":"0","promotion":[],"hospital_id":"10119","doctor_id":"","is_rongyun":"3","hos_userid":"84837375","business_district":"团结湖","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/33/54/93_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/34/06/35_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/34/34/79_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":68955,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"10.1km"},{"is_show_member":"0","pay_dingjin":"760","number":"1","start_number":"1","pay_price_discount":"3800","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2018/1231/200_200/jt181231141026_28c38e.jpg","title":"吸脂瘦脸","subtitle":"面部减脂精雕术-打造小v脸","hos_name":"北京英煌医疗美容诊所","doc_name":"王勋","price":"3880","price_discount":"3800","price_range_max":"0","id":"174757","_id":"174757","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"485人预订","feeScale":"/部位","is_fanxian":"1","promotion":[],"hospital_id":"3453","doctor_id":"","is_rongyun":"3","hos_userid":"85279795","business_district":"西单","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/11/78/69_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/75_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/79_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":174757,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"15.5km"},{"is_show_member":"0","pay_dingjin":"1980","number":"20","start_number":"1","pay_price_discount":"9880","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0627/200_200/jt190627114315_36c04d.jpg","title":"吸脂瘦脸","subtitle":"北京面部吸脂 紧塑吸脂瘦脸 面颊+下颌缘+苹果肌 小创伤不磨骨改善面部轮廓","hos_name":"北京世熙医疗美容门诊部","doc_name":"宋玉龙","price":"49800","price_discount":"9880","price_range_max":"0","id":"35997","_id":"35997","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满888减58,满2888减200,满6888减500","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"278人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"8721","doctor_id":"","is_rongyun":"3","hos_userid":"85047575","business_district":"中关村","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/12/04/07_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/12/62/57_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/12/82/61_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":35997,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"15.5km"},{"is_show_member":"0","pay_dingjin":"340","number":"1","start_number":"1","pay_price_discount":"1680","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0629/200_200/jt190629152540_487129.jpg","title":"吸脂瘦脸","subtitle":"面部精雕吸脂 无需包扎随做随走 30分钟紧致柔和取脂 面颊双下巴下颌缘吸脂","hos_name":"北京彤美医疗美容门诊部","doc_name":"桂行军","price":"9800","price_discount":"1680","price_range_max":"0","id":"214339","_id":"214339","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"3人预订","feeScale":"/部位","is_fanxian":"1","promotion":[],"hospital_id":"9835","doctor_id":"","is_rongyun":"3","hos_userid":"84945547","business_district":"三里屯","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/17/05/01_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/17/15/89_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/17/16/75_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":214339,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"9.2km"},{"is_show_member":"1","pay_dingjin":"1160","number":"20","start_number":"1","pay_price_discount":"5800","member_price":"5510","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0214/200_200/lq_190214113441_156bf5.jpg","title":"吸脂瘦脸","subtitle":"北京吸脂瘦脸 面部吸脂 10年以上整形从业专家亲诊  吸脂满意度>90%","hos_name":"北京斯悦涵美医疗美容","doc_name":"李新欣","price":"18800","price_discount":"5800","price_range_max":"0","id":"135814","_id":"135814","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满999减100","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"424人预订","feeScale":"/部位","is_fanxian":"1","promotion":[],"hospital_id":"12736","doctor_id":"","is_rongyun":"3","hos_userid":"86162605","business_district":"南磨房","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/18/05/93_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/18/06/11_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/18/10/51_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":135814,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"13.4km"},{"is_show_member":"1","pay_dingjin":"240","number":"20","start_number":"1","pay_price_discount":"1180","member_price":"1121","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0708/200_200/jt190708150643_396eb8.jpg","title":"吸脂瘦脸","subtitle":"面部吸脂  轻松甩掉大饼脸 即刻拥有巴掌小V脸 私信即送超微小气泡一次","hos_name":"北京知音医疗美容门诊部","doc_name":"李奇军","price":"9800","price_discount":"1180","price_range_max":"0","id":"177634","_id":"177634","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"516人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"8249","doctor_id":"","is_rongyun":"3","hos_userid":"85281849","business_district":"三里屯","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/11/78/87_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/99_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/79/01_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":177634,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"11.0km"},{"is_show_member":"0","pay_dingjin":"200","number":"20","start_number":"1","pay_price_discount":"999","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0701/200_200/jt190701102327_f3d40e.jpg","title":"减肥塑形","subtitle":"北京面部吸脂首部位特价 面颊/下颌缘/双下巴吸脂瘦脸 DM匠心吸脂术恢复快","hos_name":"北京达美如艺医疗美容门诊部","doc_name":"国海军","price":"3000","price_discount":"999","price_range_max":"0","id":"89609","_id":"89609","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"431人预订","feeScale":"/部位","is_fanxian":"0","promotion":[],"hospital_id":"8247","doctor_id":"","is_rongyun":"3","hos_userid":"85281847","business_district":"中关村","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/61/81/29_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/61/81/71_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/61/81/92_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":89609,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"17.2km"},{"is_show_member":"0","pay_dingjin":"1760","number":"20","start_number":"1","pay_price_discount":"8800","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0629/200_200/jt190629125936_86e289.jpg","title":"小v脸套餐","subtitle":"私信减2000元面部吸脂 面颊+下颌缘+双下巴 +嘴角","hos_name":"北京清木医疗美容诊所","doc_name":"沈薇","price":"22400","price_discount":"8800","price_range_max":"0","id":"32833","_id":"32833","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"756人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"3497","doctor_id":"","is_rongyun":"3","hos_userid":"593447","business_district":"三里屯","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/11/78/25_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/35_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/78/43_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":32833,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"11.0km"},{"is_show_member":"1","pay_dingjin":"300","number":"20","start_number":"1","pay_price_discount":"1500","member_price":"1425","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0731/200_200/jt190731171632_03b5e8.jpg","title":"面部吸脂","subtitle":"♥V-max紧塑面部吸脂\u2022首部位体验①ML层梯填充技术②线条流畅饱满③博士团定制","hos_name":"北京玲珑梵宫医疗美容医院","doc_name":"张殿祥","price":"9800","price_discount":"1500","price_range_max":"0","id":"132796","_id":"132796","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满999减100,满9999减500","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"939人预订","feeScale":"/部位","is_fanxian":"1","promotion":[],"hospital_id":"12808","doctor_id":"","is_rongyun":"3","hos_userid":"86056009","business_district":"奥运村","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/14/11/97_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/14/19/49_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/14/19/67_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":132796,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"9.4km"},{"is_show_member":"0","pay_dingjin":"460","number":"20","start_number":"1","pay_price_discount":"2280","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/0605/200_200/jt190605171524_c86964.jpg","title":"吸脂瘦脸","subtitle":"❤面部精吸下颌缘  吸脂瘦脸 超性价比 一次吸出精致小V脸","hos_name":"北京欧扬医疗美容门诊部","doc_name":"胡智鹏","price":"9800","price_discount":"2280","price_range_max":"0","id":"209851","_id":"209851","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"472人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"8135","doctor_id":"","is_rongyun":"3","hos_userid":"85057871","business_district":"阜成门","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/11/79/01_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/79/05_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/11/79/15_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":209851,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"15.6km"},{"is_show_member":"0","pay_dingjin":"1360","number":"20","start_number":"1","pay_price_discount":"6800","member_price":"-1","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2018/0919/200_200/jt180919150441_991939.jpg","title":"减肥塑形","subtitle":"2CM轻负压全面部紧塑吸脂 轻创伤微创变美紧致轮廓","hos_name":"北京南加门诊部","doc_name":"张清峰","price":"23800","price_discount":"6800","price_range_max":"0","id":"95632","_id":"95632","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"241人预订","feeScale":"/次","is_fanxian":"1","promotion":[],"hospital_id":"10837","doctor_id":"","is_rongyun":"3","hos_userid":"85283337","business_district":"四季青","hospital_top":{"level":0,"desc":""},"is_have_video":"0","userImg":["https://p21.yuemei.com/avatar/085/63/71/58_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/64/21/59_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/64/64/64_avatar_50_50.jpg"],"surgInfo":null,"videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":95632,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66},"baoguang_params":{"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34},"distance":"20.7km"}]
     * recomend_list : []
     * desc :
     * list_tips :
     * recomend_tips :
     * search_key :
     * data : {"type":"","list":[]}
     * parts : [{"id":"183","url":"https://m.yuemei.com/fan/qjzdsl","name":"取颊脂垫瘦脸","event_params":{"event_name":"search_tag_click","type":39,"id":"183","event_others":"183","referrer":"面部吸脂"}},{"id":"184","url":"https://m.yuemei.com/fan/slzhen","name":"瘦脸针","event_params":{"event_name":"search_tag_click","type":39,"id":"184","event_others":"184","referrer":"面部吸脂"}},{"id":"832","url":"https://m.yuemei.com/fan/sprzsl","name":"射频溶脂瘦脸","event_params":{"event_name":"search_tag_click","type":39,"id":"832","event_others":"832","referrer":"面部吸脂"}},{"id":"8249","url":"https://m.yuemei.com/fan/rxrzsl","name":"光纤溶脂瘦脸","event_params":{"event_name":"search_tag_click","type":39,"id":"8249","event_others":"8249","referrer":"面部吸脂"}},{"id":"10762","url":"https://m.yuemei.com/fan/jgrzsl","name":"激光溶脂瘦脸","event_params":{"event_name":"search_tag_click","type":39,"id":"10762","event_others":"10762","referrer":"面部吸脂"}}]
     * comparedConsultative : {"comparedTitle":"- 吸脂瘦脸名医 -","comparedA":{"showSkuListPosition":"2","changeOneEventParams":{"type":"39","id":"8247","event_others":"0","event_name":"compared_change_one_click","search_word":""},"exposureEventParams":{"type":"39","id":"8247","event_others":"0","event_name":"compare_baoguang","search_word":""},"doctorsList":[[{"doctorsUserID":"86163571","doctorsImg":"https://p21.yuemei.com/avatar/086/16/35/71_avatar_120_120.jpg","doctorsName":"周亮","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京炫美医疗美容诊所","diary_pf":"5.0","sku_order_num":"27481","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/S3J0Zy9CbjU3YmdMUHVFZld4WFA1QT09/group_id/3121/uid/0/hos_id/3340/doctorID/86163571/labelID/8247/comparedDoctorID/39206/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86163571/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"39206","doctorsImg":"https://p21.yuemei.com/avatar/000/03/92/06_avatar_120_120.jpg","doctorsName":"吴宇宏","doctorsTitle":"主治医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京华真堂医疗美容诊所","diary_pf":"5.0","sku_order_num":"5921","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/a0R0K1pUSzdJQTR0dHEyaGhxOHZyUT09/group_id/11755/uid/0/hos_id/12571/doctorID/39206/labelID/8247/comparedDoctorID/86163571/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/00039206/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"86350774","doctorsImg":"https://p21.yuemei.com/avatar/086/35/07/74_avatar_120_120.jpg","doctorsName":"李新欣","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京斯悦涵美医疗美容","diary_pf":"5.0","sku_order_num":"19270","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SmpxdER5cnJNZTRiNmNQVm1RKy9uZz09/group_id/11923/uid/0/hos_id/12736/doctorID/86350774/labelID/8247/comparedDoctorID/86361646/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86350774/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"86361646","doctorsImg":"https://p21.yuemei.com/avatar/086/36/16/46_avatar_120_120.jpg","doctorsName":"陈进勇","doctorsTitle":"主治医师","doctorsTag":"手术榜冠军","doctorsTagID":"3","hospitalName":"北京溪峰聚美仕医疗美容诊所","diary_pf":"5.0","sku_order_num":"3437","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/VC95NTlLUi9PSzFxUERXQjdzTzZGUT09/group_id/11785/uid/0/hos_id/8717/doctorID/86361646/labelID/8247/comparedDoctorID/86350774/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86361646/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"86373205","doctorsImg":"https://p21.yuemei.com/avatar/086/37/32/05_avatar_120_120.jpg","doctorsName":"朱金成","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京润美玉之光医疗美容门诊部","diary_pf":"5.0","sku_order_num":"25279","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/YUF0Lzg1SDRjd0kxYmducnhRTEQwUT09/group_id/4699/uid/0/hos_id/3354/doctorID/86373205/labelID/8247/comparedDoctorID/84793493/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86373205/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"84793493","doctorsImg":"https://p21.yuemei.com/avatar/084/79/34/93_avatar_120_120.jpg","doctorsName":"国海军","doctorsTitle":"副主任医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京达美如艺医疗美容门诊部","diary_pf":"5.0","sku_order_num":"8130","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/L3ZXUkhuS1B5OGlCYjZ4V3Z1eGVrQT09/group_id/7168/uid/0/hos_id/8247/doctorID/84793493/labelID/8247/comparedDoctorID/86373205/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84793493/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"84872389","doctorsImg":"https://p21.yuemei.com/avatar/084/87/23/89_avatar_120_120.jpg","doctorsName":"桂行军","doctorsTitle":"主任医师","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京彤美医疗美容门诊部","diary_pf":"5.0","sku_order_num":"13117","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SHBqd1RRVUdoNThiZDZka0NDZ1pmUT09/group_id/8647/uid/0/hos_id/9835/doctorID/84872389/labelID/8247/comparedDoctorID/85349505/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84872389/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"85349505","doctorsImg":"https://p21.yuemei.com/avatar/085/34/95/05_avatar_120_120.jpg","doctorsName":"宋玉龙","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京世熙医疗美容门诊部","diary_pf":"5.0","sku_order_num":"9991","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/TzhIeXc5RHdwYnl2UEE0eW5pYjVCUT09/group_id/7465/uid/0/hos_id/8721/doctorID/85349505/labelID/8247/comparedDoctorID/84872389/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85349505/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"85885483","doctorsImg":"https://p21.yuemei.com/avatar/085/88/54/83_avatar_120_120.jpg","doctorsName":"沈薇","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京清木医疗美容诊所","diary_pf":"5.0","sku_order_num":"4766","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/VTEwVnZaeDcweUMyZkpvVUhocUp2QT09/group_id/3481/uid/0/hos_id/3497/doctorID/85885483/labelID/8247/comparedDoctorID/87259834/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85885483/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"87259834","doctorsImg":"https://p21.yuemei.com/avatar/087/25/98/34_avatar_120_120.jpg","doctorsName":"王宁博","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京华悦府医疗美容诊所","diary_pf":"5.0","sku_order_num":"11164","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SlZNeUVSWmcxcVRVZzg3cG5jSmxUdz09/group_id/9694/uid/0/hos_id/10755/doctorID/87259834/labelID/8247/comparedDoctorID/85885483/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/87259834/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}]]},"comparedB":{"showSkuListPosition":"15","changeOneEventParams":{"type":"39","id":"8247","event_others":"1","event_name":"compared_change_one_click","search_word":""},"exposureEventParams":{"type":"39","id":"8247","event_others":"1","event_name":"compare_baoguang","search_word":""},"doctorsList":[[{"doctorsUserID":"85424257","doctorsImg":"https://p21.yuemei.com/avatar/085/42/42/57_avatar_120_120.jpg","doctorsName":"张春彦","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京雅靓医疗美容诊所","diary_pf":"5.0","sku_order_num":"11014","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/NE4xcHcrWFVwK0Z2dFMxOVpmOWZwZz09/group_id/3547/uid/0/hos_id/3518/doctorID/85424257/labelID/8247/comparedDoctorID/87959590/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85424257/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"87959590","doctorsImg":"https://p21.yuemei.com/avatar/087/95/95/90_avatar_120_120.jpg","doctorsName":"栾志宏","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京蜜邦医疗美容诊所","diary_pf":"5.0","sku_order_num":"17691","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/L2s0dVdYMk95OGtoSlE0OTRxOWg2dz09/group_id/11440/uid/0/hos_id/12268/doctorID/87959590/labelID/8247/comparedDoctorID/85424257/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/87959590/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"86348263","doctorsImg":"https://p21.yuemei.com/avatar/086/34/82/63_avatar_120_120.jpg","doctorsName":"胡智鹏","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京欧扬医疗美容门诊部","diary_pf":"5.0","sku_order_num":"5513","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/UHhmaDlBK280YjF5b1NObU5qR0xEZz09/group_id/7057/uid/0/hos_id/8135/doctorID/86348263/labelID/8247/comparedDoctorID/85606816/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86348263/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"85606816","doctorsImg":"https://p21.yuemei.com/avatar/085/60/68/16_avatar_120_120.jpg","doctorsName":"丁雅晴","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京唯专呼家楼医疗美容诊所","diary_pf":"5.0","sku_order_num":"5635","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/em1tWWtGaGRzbnBYb2MzeDdhejVUdz09/group_id/9010/uid/0/hos_id/10119/doctorID/85606816/labelID/8247/comparedDoctorID/86348263/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85606816/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"84877903","doctorsImg":"https://p21.yuemei.com/avatar/084/87/79/03_avatar_120_120.jpg","doctorsName":"蒋亚楠","doctorsTitle":"副主任医师","doctorsTag":"手术榜冠军","doctorsTagID":"3","hospitalName":"北京亚楠容悦医疗美容诊所","diary_pf":"5.0","sku_order_num":"4985","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/RCtHNDUvM0lBRUtxeVBpUlNOd1lxQT09/group_id/10729/uid/0/hos_id/11575/doctorID/84877903/labelID/8247/comparedDoctorID/85429477/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84877903/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"85429477","doctorsImg":"https://p21.yuemei.com/avatar/085/42/94/77_avatar_120_120.jpg","doctorsName":"乔海卫","doctorsTitle":"","doctorsTag":"大家都爱问","doctorsTagID":"4","hospitalName":"唯颜时代（北京）医疗美容诊所","diary_pf":"5.0","sku_order_num":"1575","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/Nk8wV3J2RS9sZENkeEZDbEZUam5MQT09/group_id/8959/uid/0/hos_id/10197/doctorID/85429477/labelID/8247/comparedDoctorID/84877903/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85429477/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"85626124","doctorsImg":"https://p21.yuemei.com/avatar/085/62/61/24_avatar_120_120.jpg","doctorsName":"张丽丽","doctorsTitle":"主任医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京百达丽医疗美容门诊部","diary_pf":"5.0","sku_order_num":"2516","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/OGZuU0JOR2YzdG5MbnRwNFZPOEFMdz09/group_id/5815/uid/0/hos_id/6996/doctorID/85626124/labelID/8247/comparedDoctorID/420621/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85626124/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"420621","doctorsImg":"https://p21.yuemei.com/avatar/000/42/06/21_avatar_120_120.jpg","doctorsName":"黎星","doctorsTitle":"住院医师","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京尚益嘉容医疗美容诊所","diary_pf":"5.0","sku_order_num":"6593","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/Q2VSK3RrejVzamxmS0tqUXpRb0tTZz09/group_id/11722/uid/0/hos_id/12544/doctorID/420621/labelID/8247/comparedDoctorID/85626124/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/00420621/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}]]}}
     * type : 4
     */

    private BaikeCompareBean baikeCompare;
    private String total;
    private String desc;
    private String list_tips;
    private String recomend_tips;
    private String search_key;
    private DataBean data;
    private ComparedConsultativeBean comparedConsultative;
    private String type;
    private List<?> search_method;
    private List<?> search_partChild;
    private List<ScreenBoardBean> screen_board;
    private List<ListBean> list;
    private List<?> recomend_list;
    private List<PartsBean> parts;

    public BaikeCompareBean getBaikeCompare() {
        return baikeCompare;
    }

    public void setBaikeCompare(BaikeCompareBean baikeCompare) {
        this.baikeCompare = baikeCompare;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getList_tips() {
        return list_tips;
    }

    public void setList_tips(String list_tips) {
        this.list_tips = list_tips;
    }

    public String getRecomend_tips() {
        return recomend_tips;
    }

    public void setRecomend_tips(String recomend_tips) {
        this.recomend_tips = recomend_tips;
    }

    public String getSearch_key() {
        return search_key;
    }

    public void setSearch_key(String search_key) {
        this.search_key = search_key;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public ComparedConsultativeBean getComparedConsultative() {
        return comparedConsultative;
    }

    public void setComparedConsultative(ComparedConsultativeBean comparedConsultative) {
        this.comparedConsultative = comparedConsultative;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<?> getSearch_method() {
        return search_method;
    }

    public void setSearch_method(List<?> search_method) {
        this.search_method = search_method;
    }

    public List<?> getSearch_partChild() {
        return search_partChild;
    }

    public void setSearch_partChild(List<?> search_partChild) {
        this.search_partChild = search_partChild;
    }

    public List<ScreenBoardBean> getScreen_board() {
        return screen_board;
    }

    public void setScreen_board(List<ScreenBoardBean> screen_board) {
        this.screen_board = screen_board;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public List<?> getRecomend_list() {
        return recomend_list;
    }

    public void setRecomend_list(List<?> recomend_list) {
        this.recomend_list = recomend_list;
    }

    public List<PartsBean> getParts() {
        return parts;
    }

    public void setParts(List<PartsBean> parts) {
        this.parts = parts;
    }

    public static class BaikeCompareBean {
        /**
         * id : duibi
         * name : 对比
         * img :
         * url :
         * event_params : {"event_name":"search_baikecompare"}
         */

        private String id;
        private String name;
        private String img;
        private String url;
        private EventParamsBean event_params;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public EventParamsBean getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBean event_params) {
            this.event_params = event_params;
        }

        public static class EventParamsBean {
            /**
             * event_name : search_baikecompare
             */

            private String event_name;

            public String getEvent_name() {
                return event_name;
            }

            public void setEvent_name(String event_name) {
                this.event_name = event_name;
            }
        }
    }

    public static class DataBean {
        /**
         * type :
         * list : []
         */

        private String type;
        private List<?> list;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<?> getList() {
            return list;
        }

        public void setList(List<?> list) {
            this.list = list;
        }
    }

    public static class ComparedConsultativeBean {
        /**
         * comparedTitle : - 吸脂瘦脸名医 -
         * comparedA : {"showSkuListPosition":"2","changeOneEventParams":{"type":"39","id":"8247","event_others":"0","event_name":"compared_change_one_click","search_word":""},"exposureEventParams":{"type":"39","id":"8247","event_others":"0","event_name":"compare_baoguang","search_word":""},"doctorsList":[[{"doctorsUserID":"86163571","doctorsImg":"https://p21.yuemei.com/avatar/086/16/35/71_avatar_120_120.jpg","doctorsName":"周亮","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京炫美医疗美容诊所","diary_pf":"5.0","sku_order_num":"27481","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/S3J0Zy9CbjU3YmdMUHVFZld4WFA1QT09/group_id/3121/uid/0/hos_id/3340/doctorID/86163571/labelID/8247/comparedDoctorID/39206/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86163571/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"39206","doctorsImg":"https://p21.yuemei.com/avatar/000/03/92/06_avatar_120_120.jpg","doctorsName":"吴宇宏","doctorsTitle":"主治医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京华真堂医疗美容诊所","diary_pf":"5.0","sku_order_num":"5921","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/a0R0K1pUSzdJQTR0dHEyaGhxOHZyUT09/group_id/11755/uid/0/hos_id/12571/doctorID/39206/labelID/8247/comparedDoctorID/86163571/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/00039206/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"86350774","doctorsImg":"https://p21.yuemei.com/avatar/086/35/07/74_avatar_120_120.jpg","doctorsName":"李新欣","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京斯悦涵美医疗美容","diary_pf":"5.0","sku_order_num":"19270","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SmpxdER5cnJNZTRiNmNQVm1RKy9uZz09/group_id/11923/uid/0/hos_id/12736/doctorID/86350774/labelID/8247/comparedDoctorID/86361646/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86350774/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"86361646","doctorsImg":"https://p21.yuemei.com/avatar/086/36/16/46_avatar_120_120.jpg","doctorsName":"陈进勇","doctorsTitle":"主治医师","doctorsTag":"手术榜冠军","doctorsTagID":"3","hospitalName":"北京溪峰聚美仕医疗美容诊所","diary_pf":"5.0","sku_order_num":"3437","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/VC95NTlLUi9PSzFxUERXQjdzTzZGUT09/group_id/11785/uid/0/hos_id/8717/doctorID/86361646/labelID/8247/comparedDoctorID/86350774/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86361646/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"86373205","doctorsImg":"https://p21.yuemei.com/avatar/086/37/32/05_avatar_120_120.jpg","doctorsName":"朱金成","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京润美玉之光医疗美容门诊部","diary_pf":"5.0","sku_order_num":"25279","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/YUF0Lzg1SDRjd0kxYmducnhRTEQwUT09/group_id/4699/uid/0/hos_id/3354/doctorID/86373205/labelID/8247/comparedDoctorID/84793493/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86373205/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"84793493","doctorsImg":"https://p21.yuemei.com/avatar/084/79/34/93_avatar_120_120.jpg","doctorsName":"国海军","doctorsTitle":"副主任医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京达美如艺医疗美容门诊部","diary_pf":"5.0","sku_order_num":"8130","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/L3ZXUkhuS1B5OGlCYjZ4V3Z1eGVrQT09/group_id/7168/uid/0/hos_id/8247/doctorID/84793493/labelID/8247/comparedDoctorID/86373205/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84793493/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"84872389","doctorsImg":"https://p21.yuemei.com/avatar/084/87/23/89_avatar_120_120.jpg","doctorsName":"桂行军","doctorsTitle":"主任医师","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京彤美医疗美容门诊部","diary_pf":"5.0","sku_order_num":"13117","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SHBqd1RRVUdoNThiZDZka0NDZ1pmUT09/group_id/8647/uid/0/hos_id/9835/doctorID/84872389/labelID/8247/comparedDoctorID/85349505/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84872389/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"85349505","doctorsImg":"https://p21.yuemei.com/avatar/085/34/95/05_avatar_120_120.jpg","doctorsName":"宋玉龙","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京世熙医疗美容门诊部","diary_pf":"5.0","sku_order_num":"9991","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/TzhIeXc5RHdwYnl2UEE0eW5pYjVCUT09/group_id/7465/uid/0/hos_id/8721/doctorID/85349505/labelID/8247/comparedDoctorID/84872389/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85349505/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"85885483","doctorsImg":"https://p21.yuemei.com/avatar/085/88/54/83_avatar_120_120.jpg","doctorsName":"沈薇","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京清木医疗美容诊所","diary_pf":"5.0","sku_order_num":"4766","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/VTEwVnZaeDcweUMyZkpvVUhocUp2QT09/group_id/3481/uid/0/hos_id/3497/doctorID/85885483/labelID/8247/comparedDoctorID/87259834/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85885483/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"87259834","doctorsImg":"https://p21.yuemei.com/avatar/087/25/98/34_avatar_120_120.jpg","doctorsName":"王宁博","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京华悦府医疗美容诊所","diary_pf":"5.0","sku_order_num":"11164","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SlZNeUVSWmcxcVRVZzg3cG5jSmxUdz09/group_id/9694/uid/0/hos_id/10755/doctorID/87259834/labelID/8247/comparedDoctorID/85885483/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/87259834/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}]]}
         * comparedB : {"showSkuListPosition":"15","changeOneEventParams":{"type":"39","id":"8247","event_others":"1","event_name":"compared_change_one_click","search_word":""},"exposureEventParams":{"type":"39","id":"8247","event_others":"1","event_name":"compare_baoguang","search_word":""},"doctorsList":[[{"doctorsUserID":"85424257","doctorsImg":"https://p21.yuemei.com/avatar/085/42/42/57_avatar_120_120.jpg","doctorsName":"张春彦","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京雅靓医疗美容诊所","diary_pf":"5.0","sku_order_num":"11014","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/NE4xcHcrWFVwK0Z2dFMxOVpmOWZwZz09/group_id/3547/uid/0/hos_id/3518/doctorID/85424257/labelID/8247/comparedDoctorID/87959590/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85424257/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"87959590","doctorsImg":"https://p21.yuemei.com/avatar/087/95/95/90_avatar_120_120.jpg","doctorsName":"栾志宏","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京蜜邦医疗美容诊所","diary_pf":"5.0","sku_order_num":"17691","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/L2s0dVdYMk95OGtoSlE0OTRxOWg2dz09/group_id/11440/uid/0/hos_id/12268/doctorID/87959590/labelID/8247/comparedDoctorID/85424257/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/87959590/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"86348263","doctorsImg":"https://p21.yuemei.com/avatar/086/34/82/63_avatar_120_120.jpg","doctorsName":"胡智鹏","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京欧扬医疗美容门诊部","diary_pf":"5.0","sku_order_num":"5513","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/UHhmaDlBK280YjF5b1NObU5qR0xEZz09/group_id/7057/uid/0/hos_id/8135/doctorID/86348263/labelID/8247/comparedDoctorID/85606816/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86348263/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"85606816","doctorsImg":"https://p21.yuemei.com/avatar/085/60/68/16_avatar_120_120.jpg","doctorsName":"丁雅晴","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京唯专呼家楼医疗美容诊所","diary_pf":"5.0","sku_order_num":"5635","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/em1tWWtGaGRzbnBYb2MzeDdhejVUdz09/group_id/9010/uid/0/hos_id/10119/doctorID/85606816/labelID/8247/comparedDoctorID/86348263/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85606816/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"84877903","doctorsImg":"https://p21.yuemei.com/avatar/084/87/79/03_avatar_120_120.jpg","doctorsName":"蒋亚楠","doctorsTitle":"副主任医师","doctorsTag":"手术榜冠军","doctorsTagID":"3","hospitalName":"北京亚楠容悦医疗美容诊所","diary_pf":"5.0","sku_order_num":"4985","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/RCtHNDUvM0lBRUtxeVBpUlNOd1lxQT09/group_id/10729/uid/0/hos_id/11575/doctorID/84877903/labelID/8247/comparedDoctorID/85429477/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84877903/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"85429477","doctorsImg":"https://p21.yuemei.com/avatar/085/42/94/77_avatar_120_120.jpg","doctorsName":"乔海卫","doctorsTitle":"","doctorsTag":"大家都爱问","doctorsTagID":"4","hospitalName":"唯颜时代（北京）医疗美容诊所","diary_pf":"5.0","sku_order_num":"1575","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/Nk8wV3J2RS9sZENkeEZDbEZUam5MQT09/group_id/8959/uid/0/hos_id/10197/doctorID/85429477/labelID/8247/comparedDoctorID/84877903/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85429477/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"85626124","doctorsImg":"https://p21.yuemei.com/avatar/085/62/61/24_avatar_120_120.jpg","doctorsName":"张丽丽","doctorsTitle":"主任医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京百达丽医疗美容门诊部","diary_pf":"5.0","sku_order_num":"2516","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/OGZuU0JOR2YzdG5MbnRwNFZPOEFMdz09/group_id/5815/uid/0/hos_id/6996/doctorID/85626124/labelID/8247/comparedDoctorID/420621/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85626124/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"420621","doctorsImg":"https://p21.yuemei.com/avatar/000/42/06/21_avatar_120_120.jpg","doctorsName":"黎星","doctorsTitle":"住院医师","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京尚益嘉容医疗美容诊所","diary_pf":"5.0","sku_order_num":"6593","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/Q2VSK3RrejVzamxmS0tqUXpRb0tTZz09/group_id/11722/uid/0/hos_id/12544/doctorID/420621/labelID/8247/comparedDoctorID/85626124/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/00420621/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}]]}
         */

        private String comparedTitle;
        private ComparedABean comparedA;
        private ComparedBBean comparedB;

        public String getComparedTitle() {
            return comparedTitle;
        }

        public void setComparedTitle(String comparedTitle) {
            this.comparedTitle = comparedTitle;
        }

        public ComparedABean getComparedA() {
            return comparedA;
        }

        public void setComparedA(ComparedABean comparedA) {
            this.comparedA = comparedA;
        }

        public ComparedBBean getComparedB() {
            return comparedB;
        }

        public void setComparedB(ComparedBBean comparedB) {
            this.comparedB = comparedB;
        }

        public static class ComparedABean {
            /**
             * showSkuListPosition : 2
             * changeOneEventParams : {"type":"39","id":"8247","event_others":"0","event_name":"compared_change_one_click","search_word":""}
             * exposureEventParams : {"type":"39","id":"8247","event_others":"0","event_name":"compare_baoguang","search_word":""}
             * doctorsList : [[{"doctorsUserID":"86163571","doctorsImg":"https://p21.yuemei.com/avatar/086/16/35/71_avatar_120_120.jpg","doctorsName":"周亮","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京炫美医疗美容诊所","diary_pf":"5.0","sku_order_num":"27481","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/S3J0Zy9CbjU3YmdMUHVFZld4WFA1QT09/group_id/3121/uid/0/hos_id/3340/doctorID/86163571/labelID/8247/comparedDoctorID/39206/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86163571/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"39206","doctorsImg":"https://p21.yuemei.com/avatar/000/03/92/06_avatar_120_120.jpg","doctorsName":"吴宇宏","doctorsTitle":"主治医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京华真堂医疗美容诊所","diary_pf":"5.0","sku_order_num":"5921","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/a0R0K1pUSzdJQTR0dHEyaGhxOHZyUT09/group_id/11755/uid/0/hos_id/12571/doctorID/39206/labelID/8247/comparedDoctorID/86163571/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/00039206/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"86350774","doctorsImg":"https://p21.yuemei.com/avatar/086/35/07/74_avatar_120_120.jpg","doctorsName":"李新欣","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京斯悦涵美医疗美容","diary_pf":"5.0","sku_order_num":"19270","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SmpxdER5cnJNZTRiNmNQVm1RKy9uZz09/group_id/11923/uid/0/hos_id/12736/doctorID/86350774/labelID/8247/comparedDoctorID/86361646/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86350774/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"86361646","doctorsImg":"https://p21.yuemei.com/avatar/086/36/16/46_avatar_120_120.jpg","doctorsName":"陈进勇","doctorsTitle":"主治医师","doctorsTag":"手术榜冠军","doctorsTagID":"3","hospitalName":"北京溪峰聚美仕医疗美容诊所","diary_pf":"5.0","sku_order_num":"3437","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/VC95NTlLUi9PSzFxUERXQjdzTzZGUT09/group_id/11785/uid/0/hos_id/8717/doctorID/86361646/labelID/8247/comparedDoctorID/86350774/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86361646/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"86373205","doctorsImg":"https://p21.yuemei.com/avatar/086/37/32/05_avatar_120_120.jpg","doctorsName":"朱金成","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京润美玉之光医疗美容门诊部","diary_pf":"5.0","sku_order_num":"25279","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/YUF0Lzg1SDRjd0kxYmducnhRTEQwUT09/group_id/4699/uid/0/hos_id/3354/doctorID/86373205/labelID/8247/comparedDoctorID/84793493/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86373205/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"84793493","doctorsImg":"https://p21.yuemei.com/avatar/084/79/34/93_avatar_120_120.jpg","doctorsName":"国海军","doctorsTitle":"副主任医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京达美如艺医疗美容门诊部","diary_pf":"5.0","sku_order_num":"8130","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/L3ZXUkhuS1B5OGlCYjZ4V3Z1eGVrQT09/group_id/7168/uid/0/hos_id/8247/doctorID/84793493/labelID/8247/comparedDoctorID/86373205/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84793493/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"84872389","doctorsImg":"https://p21.yuemei.com/avatar/084/87/23/89_avatar_120_120.jpg","doctorsName":"桂行军","doctorsTitle":"主任医师","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京彤美医疗美容门诊部","diary_pf":"5.0","sku_order_num":"13117","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SHBqd1RRVUdoNThiZDZka0NDZ1pmUT09/group_id/8647/uid/0/hos_id/9835/doctorID/84872389/labelID/8247/comparedDoctorID/85349505/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84872389/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"85349505","doctorsImg":"https://p21.yuemei.com/avatar/085/34/95/05_avatar_120_120.jpg","doctorsName":"宋玉龙","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京世熙医疗美容门诊部","diary_pf":"5.0","sku_order_num":"9991","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/TzhIeXc5RHdwYnl2UEE0eW5pYjVCUT09/group_id/7465/uid/0/hos_id/8721/doctorID/85349505/labelID/8247/comparedDoctorID/84872389/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85349505/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}],[{"doctorsUserID":"85885483","doctorsImg":"https://p21.yuemei.com/avatar/085/88/54/83_avatar_120_120.jpg","doctorsName":"沈薇","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京清木医疗美容诊所","diary_pf":"5.0","sku_order_num":"4766","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/VTEwVnZaeDcweUMyZkpvVUhocUp2QT09/group_id/3481/uid/0/hos_id/3497/doctorID/85885483/labelID/8247/comparedDoctorID/87259834/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85885483/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}},{"doctorsUserID":"87259834","doctorsImg":"https://p21.yuemei.com/avatar/087/25/98/34_avatar_120_120.jpg","doctorsName":"王宁博","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京华悦府医疗美容诊所","diary_pf":"5.0","sku_order_num":"11164","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/SlZNeUVSWmcxcVRVZzg3cG5jSmxUdz09/group_id/9694/uid/0/hos_id/10755/doctorID/87259834/labelID/8247/comparedDoctorID/85885483/ymaq_class/39/ymaq_id/8247/comparedPosition/0/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/87259834/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":0,"type":39,"id":8247}}]]
             */

            private String showSkuListPosition;
            private ChangeOneEventParamsBean changeOneEventParams;
            private ExposureEventParamsBean exposureEventParams;
            private List<List<DoctorsListBean>> doctorsList;

            public String getShowSkuListPosition() {
                return showSkuListPosition;
            }

            public void setShowSkuListPosition(String showSkuListPosition) {
                this.showSkuListPosition = showSkuListPosition;
            }

            public ChangeOneEventParamsBean getChangeOneEventParams() {
                return changeOneEventParams;
            }

            public void setChangeOneEventParams(ChangeOneEventParamsBean changeOneEventParams) {
                this.changeOneEventParams = changeOneEventParams;
            }

            public ExposureEventParamsBean getExposureEventParams() {
                return exposureEventParams;
            }

            public void setExposureEventParams(ExposureEventParamsBean exposureEventParams) {
                this.exposureEventParams = exposureEventParams;
            }

            public List<List<DoctorsListBean>> getDoctorsList() {
                return doctorsList;
            }

            public void setDoctorsList(List<List<DoctorsListBean>> doctorsList) {
                this.doctorsList = doctorsList;
            }

            public static class ChangeOneEventParamsBean {
                /**
                 * type : 39
                 * id : 8247
                 * event_others : 0
                 * event_name : compared_change_one_click
                 * search_word :
                 */

                private String type;
                private String id;
                private String event_others;
                private String event_name;
                private String search_word;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getEvent_others() {
                    return event_others;
                }

                public void setEvent_others(String event_others) {
                    this.event_others = event_others;
                }

                public String getEvent_name() {
                    return event_name;
                }

                public void setEvent_name(String event_name) {
                    this.event_name = event_name;
                }

                public String getSearch_word() {
                    return search_word;
                }

                public void setSearch_word(String search_word) {
                    this.search_word = search_word;
                }
            }

            public static class ExposureEventParamsBean {
                /**
                 * type : 39
                 * id : 8247
                 * event_others : 0
                 * event_name : compare_baoguang
                 * search_word :
                 */

                private String type;
                private String id;
                private String event_others;
                private String event_name;
                private String search_word;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getEvent_others() {
                    return event_others;
                }

                public void setEvent_others(String event_others) {
                    this.event_others = event_others;
                }

                public String getEvent_name() {
                    return event_name;
                }

                public void setEvent_name(String event_name) {
                    this.event_name = event_name;
                }

                public String getSearch_word() {
                    return search_word;
                }

                public void setSearch_word(String search_word) {
                    this.search_word = search_word;
                }
            }

            public static class DoctorsListBean {
                /**
                 * doctorsUserID : 86163571
                 * doctorsImg : https://p21.yuemei.com/avatar/086/16/35/71_avatar_120_120.jpg
                 * doctorsName : 周亮
                 * doctorsTitle :
                 * doctorsTag : 金牌专家
                 * doctorsTagID : 1
                 * hospitalName : 北京炫美医疗美容诊所
                 * diary_pf : 5.0
                 * sku_order_num : 27481
                 * jumpUrl : https://chat.yuemei.com/comparedchat/index/targetId/S3J0Zy9CbjU3YmdMUHVFZld4WFA1QT09/group_id/3121/uid/0/hos_id/3340/doctorID/86163571/labelID/8247/comparedDoctorID/39206/ymaq_class/39/ymaq_id/8247/comparedPosition/0/
                 * type : 6751
                 * typeControlParams : {"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"}
                 * doctorsCenterUrl : https://m.yuemei.com/dr/86163571/
                 * event_params : {"event_name":"compare_to_chat","event_pos":"left","event_others":0,"type":39,"id":8247}
                 */

                private String doctorsUserID;
                private String doctorsImg;
                private String doctorsName;
                private String doctorsTitle;
                private String doctorsTag;
                private String doctorsTagID;
                private String hospitalName;
                private String diary_pf;
                private String sku_order_num;
                private String jumpUrl;
                private String type;
                private TypeControlParamsBean typeControlParams;
                private String doctorsCenterUrl;
                private EventParamsBeanX event_params;

                public String getDoctorsUserID() {
                    return doctorsUserID;
                }

                public void setDoctorsUserID(String doctorsUserID) {
                    this.doctorsUserID = doctorsUserID;
                }

                public String getDoctorsImg() {
                    return doctorsImg;
                }

                public void setDoctorsImg(String doctorsImg) {
                    this.doctorsImg = doctorsImg;
                }

                public String getDoctorsName() {
                    return doctorsName;
                }

                public void setDoctorsName(String doctorsName) {
                    this.doctorsName = doctorsName;
                }

                public String getDoctorsTitle() {
                    return doctorsTitle;
                }

                public void setDoctorsTitle(String doctorsTitle) {
                    this.doctorsTitle = doctorsTitle;
                }

                public String getDoctorsTag() {
                    return doctorsTag;
                }

                public void setDoctorsTag(String doctorsTag) {
                    this.doctorsTag = doctorsTag;
                }

                public String getDoctorsTagID() {
                    return doctorsTagID;
                }

                public void setDoctorsTagID(String doctorsTagID) {
                    this.doctorsTagID = doctorsTagID;
                }

                public String getHospitalName() {
                    return hospitalName;
                }

                public void setHospitalName(String hospitalName) {
                    this.hospitalName = hospitalName;
                }

                public String getDiary_pf() {
                    return diary_pf;
                }

                public void setDiary_pf(String diary_pf) {
                    this.diary_pf = diary_pf;
                }

                public String getSku_order_num() {
                    return sku_order_num;
                }

                public void setSku_order_num(String sku_order_num) {
                    this.sku_order_num = sku_order_num;
                }

                public String getJumpUrl() {
                    return jumpUrl;
                }

                public void setJumpUrl(String jumpUrl) {
                    this.jumpUrl = jumpUrl;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public TypeControlParamsBean getTypeControlParams() {
                    return typeControlParams;
                }

                public void setTypeControlParams(TypeControlParamsBean typeControlParams) {
                    this.typeControlParams = typeControlParams;
                }

                public String getDoctorsCenterUrl() {
                    return doctorsCenterUrl;
                }

                public void setDoctorsCenterUrl(String doctorsCenterUrl) {
                    this.doctorsCenterUrl = doctorsCenterUrl;
                }

                public EventParamsBeanX getEvent_params() {
                    return event_params;
                }

                public void setEvent_params(EventParamsBeanX event_params) {
                    this.event_params = event_params;
                }

                public static class TypeControlParamsBean {
                    /**
                     * isHide : 1
                     * isRefresh : 0
                     * enableSafeArea : 1
                     * webType : 1
                     * bgColor : #ffffff
                     * enableBottomSafeArea : 1
                     */

                    private String isHide;
                    private String isRefresh;
                    private String enableSafeArea;
                    private String webType;
                    private String bgColor;
                    private String enableBottomSafeArea;

                    public String getIsHide() {
                        return isHide;
                    }

                    public void setIsHide(String isHide) {
                        this.isHide = isHide;
                    }

                    public String getIsRefresh() {
                        return isRefresh;
                    }

                    public void setIsRefresh(String isRefresh) {
                        this.isRefresh = isRefresh;
                    }

                    public String getEnableSafeArea() {
                        return enableSafeArea;
                    }

                    public void setEnableSafeArea(String enableSafeArea) {
                        this.enableSafeArea = enableSafeArea;
                    }

                    public String getWebType() {
                        return webType;
                    }

                    public void setWebType(String webType) {
                        this.webType = webType;
                    }

                    public String getBgColor() {
                        return bgColor;
                    }

                    public void setBgColor(String bgColor) {
                        this.bgColor = bgColor;
                    }

                    public String getEnableBottomSafeArea() {
                        return enableBottomSafeArea;
                    }

                    public void setEnableBottomSafeArea(String enableBottomSafeArea) {
                        this.enableBottomSafeArea = enableBottomSafeArea;
                    }
                }

                public static class EventParamsBeanX {
                    /**
                     * event_name : compare_to_chat
                     * event_pos : left
                     * event_others : 0
                     * type : 39
                     * id : 8247
                     */

                    private String event_name;
                    private String event_pos;
                    private int event_others;
                    private int type;
                    private int id;

                    public String getEvent_name() {
                        return event_name;
                    }

                    public void setEvent_name(String event_name) {
                        this.event_name = event_name;
                    }

                    public String getEvent_pos() {
                        return event_pos;
                    }

                    public void setEvent_pos(String event_pos) {
                        this.event_pos = event_pos;
                    }

                    public int getEvent_others() {
                        return event_others;
                    }

                    public void setEvent_others(int event_others) {
                        this.event_others = event_others;
                    }

                    public int getType() {
                        return type;
                    }

                    public void setType(int type) {
                        this.type = type;
                    }

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }
                }
            }
        }

        public static class ComparedBBean {
            /**
             * showSkuListPosition : 15
             * changeOneEventParams : {"type":"39","id":"8247","event_others":"1","event_name":"compared_change_one_click","search_word":""}
             * exposureEventParams : {"type":"39","id":"8247","event_others":"1","event_name":"compare_baoguang","search_word":""}
             * doctorsList : [[{"doctorsUserID":"85424257","doctorsImg":"https://p21.yuemei.com/avatar/085/42/42/57_avatar_120_120.jpg","doctorsName":"张春彦","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京雅靓医疗美容诊所","diary_pf":"5.0","sku_order_num":"11014","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/NE4xcHcrWFVwK0Z2dFMxOVpmOWZwZz09/group_id/3547/uid/0/hos_id/3518/doctorID/85424257/labelID/8247/comparedDoctorID/87959590/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85424257/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"87959590","doctorsImg":"https://p21.yuemei.com/avatar/087/95/95/90_avatar_120_120.jpg","doctorsName":"栾志宏","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京蜜邦医疗美容诊所","diary_pf":"5.0","sku_order_num":"17691","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/L2s0dVdYMk95OGtoSlE0OTRxOWg2dz09/group_id/11440/uid/0/hos_id/12268/doctorID/87959590/labelID/8247/comparedDoctorID/85424257/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/87959590/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"86348263","doctorsImg":"https://p21.yuemei.com/avatar/086/34/82/63_avatar_120_120.jpg","doctorsName":"胡智鹏","doctorsTitle":"","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京欧扬医疗美容门诊部","diary_pf":"5.0","sku_order_num":"5513","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/UHhmaDlBK280YjF5b1NObU5qR0xEZz09/group_id/7057/uid/0/hos_id/8135/doctorID/86348263/labelID/8247/comparedDoctorID/85606816/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86348263/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"85606816","doctorsImg":"https://p21.yuemei.com/avatar/085/60/68/16_avatar_120_120.jpg","doctorsName":"丁雅晴","doctorsTitle":"","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京唯专呼家楼医疗美容诊所","diary_pf":"5.0","sku_order_num":"5635","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/em1tWWtGaGRzbnBYb2MzeDdhejVUdz09/group_id/9010/uid/0/hos_id/10119/doctorID/85606816/labelID/8247/comparedDoctorID/86348263/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85606816/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"84877903","doctorsImg":"https://p21.yuemei.com/avatar/084/87/79/03_avatar_120_120.jpg","doctorsName":"蒋亚楠","doctorsTitle":"副主任医师","doctorsTag":"手术榜冠军","doctorsTagID":"3","hospitalName":"北京亚楠容悦医疗美容诊所","diary_pf":"5.0","sku_order_num":"4985","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/RCtHNDUvM0lBRUtxeVBpUlNOd1lxQT09/group_id/10729/uid/0/hos_id/11575/doctorID/84877903/labelID/8247/comparedDoctorID/85429477/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84877903/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"85429477","doctorsImg":"https://p21.yuemei.com/avatar/085/42/94/77_avatar_120_120.jpg","doctorsName":"乔海卫","doctorsTitle":"","doctorsTag":"大家都爱问","doctorsTagID":"4","hospitalName":"唯颜时代（北京）医疗美容诊所","diary_pf":"5.0","sku_order_num":"1575","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/Nk8wV3J2RS9sZENkeEZDbEZUam5MQT09/group_id/8959/uid/0/hos_id/10197/doctorID/85429477/labelID/8247/comparedDoctorID/84877903/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85429477/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}],[{"doctorsUserID":"85626124","doctorsImg":"https://p21.yuemei.com/avatar/085/62/61/24_avatar_120_120.jpg","doctorsName":"张丽丽","doctorsTitle":"主任医师","doctorsTag":"口碑一级棒","doctorsTagID":"2","hospitalName":"北京百达丽医疗美容门诊部","diary_pf":"5.0","sku_order_num":"2516","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/OGZuU0JOR2YzdG5MbnRwNFZPOEFMdz09/group_id/5815/uid/0/hos_id/6996/doctorID/85626124/labelID/8247/comparedDoctorID/420621/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/85626124/","event_params":{"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}},{"doctorsUserID":"420621","doctorsImg":"https://p21.yuemei.com/avatar/000/42/06/21_avatar_120_120.jpg","doctorsName":"黎星","doctorsTitle":"住院医师","doctorsTag":"金牌专家","doctorsTagID":"1","hospitalName":"北京尚益嘉容医疗美容诊所","diary_pf":"5.0","sku_order_num":"6593","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/Q2VSK3RrejVzamxmS0tqUXpRb0tTZz09/group_id/11722/uid/0/hos_id/12544/doctorID/420621/labelID/8247/comparedDoctorID/85626124/ymaq_class/39/ymaq_id/8247/comparedPosition/1/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"},"doctorsCenterUrl":"https://m.yuemei.com/dr/00420621/","event_params":{"event_name":"compare_to_chat","event_pos":"right","event_others":1,"type":39,"id":8247}}]]
             */

            private String showSkuListPosition;
            private ChangeOneEventParamsBeanX changeOneEventParams;
            private ExposureEventParamsBeanX exposureEventParams;
            private List<List<DoctorsListBeanX>> doctorsList;

            public String getShowSkuListPosition() {
                return showSkuListPosition;
            }

            public void setShowSkuListPosition(String showSkuListPosition) {
                this.showSkuListPosition = showSkuListPosition;
            }

            public ChangeOneEventParamsBeanX getChangeOneEventParams() {
                return changeOneEventParams;
            }

            public void setChangeOneEventParams(ChangeOneEventParamsBeanX changeOneEventParams) {
                this.changeOneEventParams = changeOneEventParams;
            }

            public ExposureEventParamsBeanX getExposureEventParams() {
                return exposureEventParams;
            }

            public void setExposureEventParams(ExposureEventParamsBeanX exposureEventParams) {
                this.exposureEventParams = exposureEventParams;
            }

            public List<List<DoctorsListBeanX>> getDoctorsList() {
                return doctorsList;
            }

            public void setDoctorsList(List<List<DoctorsListBeanX>> doctorsList) {
                this.doctorsList = doctorsList;
            }

            public static class ChangeOneEventParamsBeanX {
                /**
                 * type : 39
                 * id : 8247
                 * event_others : 1
                 * event_name : compared_change_one_click
                 * search_word :
                 */

                private String type;
                private String id;
                private String event_others;
                private String event_name;
                private String search_word;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getEvent_others() {
                    return event_others;
                }

                public void setEvent_others(String event_others) {
                    this.event_others = event_others;
                }

                public String getEvent_name() {
                    return event_name;
                }

                public void setEvent_name(String event_name) {
                    this.event_name = event_name;
                }

                public String getSearch_word() {
                    return search_word;
                }

                public void setSearch_word(String search_word) {
                    this.search_word = search_word;
                }
            }

            public static class ExposureEventParamsBeanX {
                /**
                 * type : 39
                 * id : 8247
                 * event_others : 1
                 * event_name : compare_baoguang
                 * search_word :
                 */

                private String type;
                private String id;
                private String event_others;
                private String event_name;
                private String search_word;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getEvent_others() {
                    return event_others;
                }

                public void setEvent_others(String event_others) {
                    this.event_others = event_others;
                }

                public String getEvent_name() {
                    return event_name;
                }

                public void setEvent_name(String event_name) {
                    this.event_name = event_name;
                }

                public String getSearch_word() {
                    return search_word;
                }

                public void setSearch_word(String search_word) {
                    this.search_word = search_word;
                }
            }

            public static class DoctorsListBeanX {
                /**
                 * doctorsUserID : 85424257
                 * doctorsImg : https://p21.yuemei.com/avatar/085/42/42/57_avatar_120_120.jpg
                 * doctorsName : 张春彦
                 * doctorsTitle :
                 * doctorsTag : 金牌专家
                 * doctorsTagID : 1
                 * hospitalName : 北京雅靓医疗美容诊所
                 * diary_pf : 5.0
                 * sku_order_num : 11014
                 * jumpUrl : https://chat.yuemei.com/comparedchat/index/targetId/NE4xcHcrWFVwK0Z2dFMxOVpmOWZwZz09/group_id/3547/uid/0/hos_id/3518/doctorID/85424257/labelID/8247/comparedDoctorID/87959590/ymaq_class/39/ymaq_id/8247/comparedPosition/1/
                 * type : 6751
                 * typeControlParams : {"isHide":"1","isRefresh":"0","enableSafeArea":"1","webType":"1","bgColor":"#ffffff","enableBottomSafeArea":"1"}
                 * doctorsCenterUrl : https://m.yuemei.com/dr/85424257/
                 * event_params : {"event_name":"compare_to_chat","event_pos":"left","event_others":1,"type":39,"id":8247}
                 */

                private String doctorsUserID;
                private String doctorsImg;
                private String doctorsName;
                private String doctorsTitle;
                private String doctorsTag;
                private String doctorsTagID;
                private String hospitalName;
                private String diary_pf;
                private String sku_order_num;
                private String jumpUrl;
                private String type;
                private TypeControlParamsBeanX typeControlParams;
                private String doctorsCenterUrl;
                private EventParamsBeanXX event_params;

                public String getDoctorsUserID() {
                    return doctorsUserID;
                }

                public void setDoctorsUserID(String doctorsUserID) {
                    this.doctorsUserID = doctorsUserID;
                }

                public String getDoctorsImg() {
                    return doctorsImg;
                }

                public void setDoctorsImg(String doctorsImg) {
                    this.doctorsImg = doctorsImg;
                }

                public String getDoctorsName() {
                    return doctorsName;
                }

                public void setDoctorsName(String doctorsName) {
                    this.doctorsName = doctorsName;
                }

                public String getDoctorsTitle() {
                    return doctorsTitle;
                }

                public void setDoctorsTitle(String doctorsTitle) {
                    this.doctorsTitle = doctorsTitle;
                }

                public String getDoctorsTag() {
                    return doctorsTag;
                }

                public void setDoctorsTag(String doctorsTag) {
                    this.doctorsTag = doctorsTag;
                }

                public String getDoctorsTagID() {
                    return doctorsTagID;
                }

                public void setDoctorsTagID(String doctorsTagID) {
                    this.doctorsTagID = doctorsTagID;
                }

                public String getHospitalName() {
                    return hospitalName;
                }

                public void setHospitalName(String hospitalName) {
                    this.hospitalName = hospitalName;
                }

                public String getDiary_pf() {
                    return diary_pf;
                }

                public void setDiary_pf(String diary_pf) {
                    this.diary_pf = diary_pf;
                }

                public String getSku_order_num() {
                    return sku_order_num;
                }

                public void setSku_order_num(String sku_order_num) {
                    this.sku_order_num = sku_order_num;
                }

                public String getJumpUrl() {
                    return jumpUrl;
                }

                public void setJumpUrl(String jumpUrl) {
                    this.jumpUrl = jumpUrl;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public TypeControlParamsBeanX getTypeControlParams() {
                    return typeControlParams;
                }

                public void setTypeControlParams(TypeControlParamsBeanX typeControlParams) {
                    this.typeControlParams = typeControlParams;
                }

                public String getDoctorsCenterUrl() {
                    return doctorsCenterUrl;
                }

                public void setDoctorsCenterUrl(String doctorsCenterUrl) {
                    this.doctorsCenterUrl = doctorsCenterUrl;
                }

                public EventParamsBeanXX getEvent_params() {
                    return event_params;
                }

                public void setEvent_params(EventParamsBeanXX event_params) {
                    this.event_params = event_params;
                }

                public static class TypeControlParamsBeanX {
                    /**
                     * isHide : 1
                     * isRefresh : 0
                     * enableSafeArea : 1
                     * webType : 1
                     * bgColor : #ffffff
                     * enableBottomSafeArea : 1
                     */

                    private String isHide;
                    private String isRefresh;
                    private String enableSafeArea;
                    private String webType;
                    private String bgColor;
                    private String enableBottomSafeArea;

                    public String getIsHide() {
                        return isHide;
                    }

                    public void setIsHide(String isHide) {
                        this.isHide = isHide;
                    }

                    public String getIsRefresh() {
                        return isRefresh;
                    }

                    public void setIsRefresh(String isRefresh) {
                        this.isRefresh = isRefresh;
                    }

                    public String getEnableSafeArea() {
                        return enableSafeArea;
                    }

                    public void setEnableSafeArea(String enableSafeArea) {
                        this.enableSafeArea = enableSafeArea;
                    }

                    public String getWebType() {
                        return webType;
                    }

                    public void setWebType(String webType) {
                        this.webType = webType;
                    }

                    public String getBgColor() {
                        return bgColor;
                    }

                    public void setBgColor(String bgColor) {
                        this.bgColor = bgColor;
                    }

                    public String getEnableBottomSafeArea() {
                        return enableBottomSafeArea;
                    }

                    public void setEnableBottomSafeArea(String enableBottomSafeArea) {
                        this.enableBottomSafeArea = enableBottomSafeArea;
                    }
                }

                public static class EventParamsBeanXX {
                    /**
                     * event_name : compare_to_chat
                     * event_pos : left
                     * event_others : 1
                     * type : 39
                     * id : 8247
                     */

                    private String event_name;
                    private String event_pos;
                    private int event_others;
                    private int type;
                    private int id;

                    public String getEvent_name() {
                        return event_name;
                    }

                    public void setEvent_name(String event_name) {
                        this.event_name = event_name;
                    }

                    public String getEvent_pos() {
                        return event_pos;
                    }

                    public void setEvent_pos(String event_pos) {
                        this.event_pos = event_pos;
                    }

                    public int getEvent_others() {
                        return event_others;
                    }

                    public void setEvent_others(int event_others) {
                        this.event_others = event_others;
                    }

                    public int getType() {
                        return type;
                    }

                    public void setType(int type) {
                        this.type = type;
                    }

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }
                }
            }
        }
    }

    public static class ScreenBoardBean {
        /**
         * id : 1
         * title : 尾款红包
         * postName : deposit
         * postVal : 1
         * image : {"before":"","after":""}
         * event_params : {"event_name":"search_board","type":39,"event_pos":1}
         */

        private String id;
        private String title;
        private String postName;
        private String postVal;
        private ImageBean image;
        private EventParamsBeanXXX event_params;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPostName() {
            return postName;
        }

        public void setPostName(String postName) {
            this.postName = postName;
        }

        public String getPostVal() {
            return postVal;
        }

        public void setPostVal(String postVal) {
            this.postVal = postVal;
        }

        public ImageBean getImage() {
            return image;
        }

        public void setImage(ImageBean image) {
            this.image = image;
        }

        public EventParamsBeanXXX getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBeanXXX event_params) {
            this.event_params = event_params;
        }

        public static class ImageBean {
            /**
             * before :
             * after :
             */

            private String before;
            private String after;

            public String getBefore() {
                return before;
            }

            public void setBefore(String before) {
                this.before = before;
            }

            public String getAfter() {
                return after;
            }

            public void setAfter(String after) {
                this.after = after;
            }
        }

        public static class EventParamsBeanXXX {
            /**
             * event_name : search_board
             * type : 39
             * event_pos : 1
             */

            private String event_name;
            private int type;
            private int event_pos;

            public String getEvent_name() {
                return event_name;
            }

            public void setEvent_name(String event_name) {
                this.event_name = event_name;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getEvent_pos() {
                return event_pos;
            }

            public void setEvent_pos(int event_pos) {
                this.event_pos = event_pos;
            }
        }
    }

    public static class ListBean {
        /**
         * is_show_member : 1
         * pay_dingjin : 1100
         * number : 20
         * start_number : 1
         * pay_price_discount : 5500
         * member_price : 5225
         * m_list_logo :
         * bmsid : 0
         * seckilling : 0
         * img : https://p24.yuemei.com/tao/2019/0430/200_200/jt190430163329_937fbf.jpg
         * title : 面部吸脂
         * subtitle : ①全面部吸脂瘦脸②可吸面颊/下颌缘/双下巴/嘴角③私信送现金红包/脱毛
         * hos_name : 悦美好医医疗美容门诊部
         * doc_name : 门智和
         * price : 9800
         * price_discount : 5500
         * price_range_max : 0
         * id : 142036
         * _id : 142036
         * showprice : 1
         * specialPrice : 0
         * show_hospital : 1
         * invitation : 0
         * lijian : 0
         * baoxian :
         * insure : {"is_insure":"0","insure_pay_money":"0","title":""}
         * img66 :
         * repayment : 最高可享12期分期付款：花呗分期
         * hos_red_packet : 满1000减88,满2000减188,满5000减488
         * mingyi : 0
         * hot : 0
         * newp : 0
         * shixiao : 0
         * extension_user :
         * postStr :
         * depreciate :
         * rate : 36人预订
         * feeScale : /次
         * is_fanxian : 1
         * promotion : []
         * hospital_id : 10647
         * doctor_id : 84987073
         * is_rongyun : 3
         * hos_userid : 700893
         * business_district : 中关村
         * hospital_top : {"level":0,"desc":""}
         * is_have_video : 0
         * userImg : ["https://p21.yuemei.com/avatar/085/21/30/85_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/21/31/13_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/21/45/03_avatar_50_50.jpg"]
         * surgInfo : {"url_name":"faceslimm","name":"瘦脸","alias":"","id":"329","title":"瘦脸"}
         * videoTaoTitle :
         * event_params : {"to_page_type":2,"to_page_id":142036,"event_others":0,"search_word":"面部吸脂","url":"面部吸脂_北京_4_1565697872","search_result_num":66}
         * distance : 17.5km
         * baoguang_params : {"search_word":"面部吸脂","search_hit_id":"board_8247","operate_area_id":34}
         */

        private String is_show_member;
        private String pay_dingjin;
        private String number;
        private String start_number;
        private String pay_price_discount;
        private String member_price;
        private String m_list_logo;
        private String bmsid;
        private String seckilling;
        private String img;
        private String title;
        private String subtitle;
        private String hos_name;
        private String doc_name;
        private String price;
        private String price_discount;
        private String price_range_max;
        private String id;
        private String _id;
        private String showprice;
        private String specialPrice;
        private String show_hospital;
        private String invitation;
        private String lijian;
        private String baoxian;
        private InsureBean insure;
        private String img66;
        private String repayment;
        private String hos_red_packet;
        private String mingyi;
        private String hot;
        private String newp;
        private String shixiao;
        private String extension_user;
        private String postStr;
        private String depreciate;
        private String rate;
        private String feeScale;
        private String is_fanxian;
        private String hospital_id;
        private String doctor_id;
        private String is_rongyun;
        private String hos_userid;
        private String business_district;
        private HospitalTopBean hospital_top;
        private String is_have_video;
        private SurgInfoBean surgInfo;
        private String videoTaoTitle;
        private EventParamsBeanXXXX event_params;
        private String distance;
        private BaoguangParamsBean baoguang_params;
        private List<?> promotion;
        private List<String> userImg;

        public String getIs_show_member() {
            return is_show_member;
        }

        public void setIs_show_member(String is_show_member) {
            this.is_show_member = is_show_member;
        }

        public String getPay_dingjin() {
            return pay_dingjin;
        }

        public void setPay_dingjin(String pay_dingjin) {
            this.pay_dingjin = pay_dingjin;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStart_number() {
            return start_number;
        }

        public void setStart_number(String start_number) {
            this.start_number = start_number;
        }

        public String getPay_price_discount() {
            return pay_price_discount;
        }

        public void setPay_price_discount(String pay_price_discount) {
            this.pay_price_discount = pay_price_discount;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }

        public String getM_list_logo() {
            return m_list_logo;
        }

        public void setM_list_logo(String m_list_logo) {
            this.m_list_logo = m_list_logo;
        }

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getSeckilling() {
            return seckilling;
        }

        public void setSeckilling(String seckilling) {
            this.seckilling = seckilling;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getHos_name() {
            return hos_name;
        }

        public void setHos_name(String hos_name) {
            this.hos_name = hos_name;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice_range_max() {
            return price_range_max;
        }

        public void setPrice_range_max(String price_range_max) {
            this.price_range_max = price_range_max;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getShowprice() {
            return showprice;
        }

        public void setShowprice(String showprice) {
            this.showprice = showprice;
        }

        public String getSpecialPrice() {
            return specialPrice;
        }

        public void setSpecialPrice(String specialPrice) {
            this.specialPrice = specialPrice;
        }

        public String getShow_hospital() {
            return show_hospital;
        }

        public void setShow_hospital(String show_hospital) {
            this.show_hospital = show_hospital;
        }

        public String getInvitation() {
            return invitation;
        }

        public void setInvitation(String invitation) {
            this.invitation = invitation;
        }

        public String getLijian() {
            return lijian;
        }

        public void setLijian(String lijian) {
            this.lijian = lijian;
        }

        public String getBaoxian() {
            return baoxian;
        }

        public void setBaoxian(String baoxian) {
            this.baoxian = baoxian;
        }

        public InsureBean getInsure() {
            return insure;
        }

        public void setInsure(InsureBean insure) {
            this.insure = insure;
        }

        public String getImg66() {
            return img66;
        }

        public void setImg66(String img66) {
            this.img66 = img66;
        }

        public String getRepayment() {
            return repayment;
        }

        public void setRepayment(String repayment) {
            this.repayment = repayment;
        }

        public String getHos_red_packet() {
            return hos_red_packet;
        }

        public void setHos_red_packet(String hos_red_packet) {
            this.hos_red_packet = hos_red_packet;
        }

        public String getMingyi() {
            return mingyi;
        }

        public void setMingyi(String mingyi) {
            this.mingyi = mingyi;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getNewp() {
            return newp;
        }

        public void setNewp(String newp) {
            this.newp = newp;
        }

        public String getShixiao() {
            return shixiao;
        }

        public void setShixiao(String shixiao) {
            this.shixiao = shixiao;
        }

        public String getExtension_user() {
            return extension_user;
        }

        public void setExtension_user(String extension_user) {
            this.extension_user = extension_user;
        }

        public String getPostStr() {
            return postStr;
        }

        public void setPostStr(String postStr) {
            this.postStr = postStr;
        }

        public String getDepreciate() {
            return depreciate;
        }

        public void setDepreciate(String depreciate) {
            this.depreciate = depreciate;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getFeeScale() {
            return feeScale;
        }

        public void setFeeScale(String feeScale) {
            this.feeScale = feeScale;
        }

        public String getIs_fanxian() {
            return is_fanxian;
        }

        public void setIs_fanxian(String is_fanxian) {
            this.is_fanxian = is_fanxian;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getBusiness_district() {
            return business_district;
        }

        public void setBusiness_district(String business_district) {
            this.business_district = business_district;
        }

        public HospitalTopBean getHospital_top() {
            return hospital_top;
        }

        public void setHospital_top(HospitalTopBean hospital_top) {
            this.hospital_top = hospital_top;
        }

        public String getIs_have_video() {
            return is_have_video;
        }

        public void setIs_have_video(String is_have_video) {
            this.is_have_video = is_have_video;
        }

        public SurgInfoBean getSurgInfo() {
            return surgInfo;
        }

        public void setSurgInfo(SurgInfoBean surgInfo) {
            this.surgInfo = surgInfo;
        }

        public String getVideoTaoTitle() {
            return videoTaoTitle;
        }

        public void setVideoTaoTitle(String videoTaoTitle) {
            this.videoTaoTitle = videoTaoTitle;
        }

        public EventParamsBeanXXXX getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBeanXXXX event_params) {
            this.event_params = event_params;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public BaoguangParamsBean getBaoguang_params() {
            return baoguang_params;
        }

        public void setBaoguang_params(BaoguangParamsBean baoguang_params) {
            this.baoguang_params = baoguang_params;
        }

        public List<?> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<?> promotion) {
            this.promotion = promotion;
        }

        public List<String> getUserImg() {
            return userImg;
        }

        public void setUserImg(List<String> userImg) {
            this.userImg = userImg;
        }

        public static class InsureBean {
            /**
             * is_insure : 0
             * insure_pay_money : 0
             * title :
             */

            private String is_insure;
            private String insure_pay_money;
            private String title;

            public String getIs_insure() {
                return is_insure;
            }

            public void setIs_insure(String is_insure) {
                this.is_insure = is_insure;
            }

            public String getInsure_pay_money() {
                return insure_pay_money;
            }

            public void setInsure_pay_money(String insure_pay_money) {
                this.insure_pay_money = insure_pay_money;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class HospitalTopBean {
            /**
             * level : 0
             * desc :
             */

            private int level;
            private String desc;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }
        }

        public static class SurgInfoBean {
            /**
             * url_name : faceslimm
             * name : 瘦脸
             * alias :
             * id : 329
             * title : 瘦脸
             */

            private String url_name;
            private String name;
            private String alias;
            private String id;
            private String title;

            public String getUrl_name() {
                return url_name;
            }

            public void setUrl_name(String url_name) {
                this.url_name = url_name;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAlias() {
                return alias;
            }

            public void setAlias(String alias) {
                this.alias = alias;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class EventParamsBeanXXXX {
            /**
             * to_page_type : 2
             * to_page_id : 142036
             * event_others : 0
             * search_word : 面部吸脂
             * url : 面部吸脂_北京_4_1565697872
             * search_result_num : 66
             */

            private int to_page_type;
            private int to_page_id;
            private int event_others;
            private String search_word;
            private String url;
            private int search_result_num;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }

            public int getEvent_others() {
                return event_others;
            }

            public void setEvent_others(int event_others) {
                this.event_others = event_others;
            }

            public String getSearch_word() {
                return search_word;
            }

            public void setSearch_word(String search_word) {
                this.search_word = search_word;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getSearch_result_num() {
                return search_result_num;
            }

            public void setSearch_result_num(int search_result_num) {
                this.search_result_num = search_result_num;
            }
        }

        public static class BaoguangParamsBean {
            /**
             * search_word : 面部吸脂
             * search_hit_id : board_8247
             * operate_area_id : 34
             */

            private String search_word;
            private String search_hit_id;
            private int operate_area_id;

            public String getSearch_word() {
                return search_word;
            }

            public void setSearch_word(String search_word) {
                this.search_word = search_word;
            }

            public String getSearch_hit_id() {
                return search_hit_id;
            }

            public void setSearch_hit_id(String search_hit_id) {
                this.search_hit_id = search_hit_id;
            }

            public int getOperate_area_id() {
                return operate_area_id;
            }

            public void setOperate_area_id(int operate_area_id) {
                this.operate_area_id = operate_area_id;
            }
        }
    }

    public static class PartsBean {
        /**
         * id : 183
         * url : https://m.yuemei.com/fan/qjzdsl
         * name : 取颊脂垫瘦脸
         * event_params : {"event_name":"search_tag_click","type":39,"id":"183","event_others":"183","referrer":"面部吸脂"}
         */

        private String id;
        private String url;
        private String name;
        private EventParamsBeanXXXXX event_params;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public EventParamsBeanXXXXX getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBeanXXXXX event_params) {
            this.event_params = event_params;
        }

        public static class EventParamsBeanXXXXX {
            /**
             * event_name : search_tag_click
             * type : 39
             * id : 183
             * event_others : 183
             * referrer : 面部吸脂
             */

            private String event_name;
            private int type;
            private String id;
            private String event_others;
            private String referrer;

            public String getEvent_name() {
                return event_name;
            }

            public void setEvent_name(String event_name) {
                this.event_name = event_name;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getEvent_others() {
                return event_others;
            }

            public void setEvent_others(String event_others) {
                this.event_others = event_others;
            }

            public String getReferrer() {
                return referrer;
            }

            public void setReferrer(String referrer) {
                this.referrer = referrer;
            }
        }
    }
}
