package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/5/22
 */
public class SearchTitleList implements Parcelable {
    private String type;
    private List<SearchTitleDataData> list;

    protected SearchTitleList(Parcel in) {
        type = in.readString();
        list = in.createTypedArrayList(SearchTitleDataData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeTypedList(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchTitleList> CREATOR = new Creator<SearchTitleList>() {
        @Override
        public SearchTitleList createFromParcel(Parcel in) {
            return new SearchTitleList(in);
        }

        @Override
        public SearchTitleList[] newArray(int size) {
            return new SearchTitleList[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<SearchTitleDataData> getList() {
        return list;
    }

    public void setList(List<SearchTitleDataData> list) {
        this.list = list;
    }
}
