package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.community.model.bean.TaoListDoctors;
import com.module.community.model.bean.TaoListType;
import com.module.taodetail.model.bean.HomeTaoData;

public class SearchTao implements Parcelable {

    private HomeTaoData tao; //正常淘列表数据

    private DcotorComparData compared; //对比咨询数据

    private SearchActivityData flash_sale; //秒杀

    private SearchActivityData big_promotion;//大促

    private SearchActivityData leader_board;//销量排行榜

    private SearchActivityData recommend; //推荐

    private TaoListType taolistType;

    public void handleListType(){
        if (tao != null){
            this.taolistType = TaoListType.DATA;
        }else if (flash_sale != null){
            this.taolistType = TaoListType.FLASH_SALE;
        }
        else if (compared != null){
            this.taolistType = TaoListType.DOCTOR_LIST;
        }
        else if (big_promotion != null){
            this.taolistType = TaoListType.BIG_PROMOTION;
        }else if (leader_board != null){
            this.taolistType = TaoListType.LEADER_BOARD;
        }else if (recommend != null){
            this.taolistType = TaoListType.RECOMMEND;
        }
    }


    public HomeTaoData getTao() {
        return tao;
    }

    public void setTao(HomeTaoData tao) {
        this.tao = tao;
    }

    public SearchActivityData getFlash_sale() {
        return flash_sale;
    }

    public void setFlash_sale(SearchActivityData flash_sale) {
        this.flash_sale = flash_sale;
    }

    public SearchActivityData getBig_promotion() {
        return big_promotion;
    }

    public void setBig_promotion(SearchActivityData big_promotion) {
        this.big_promotion = big_promotion;
    }

    public SearchActivityData getLeader_board() {
        return leader_board;
    }

    public void setLeader_board(SearchActivityData leader_board) {
        this.leader_board = leader_board;
    }

    public SearchActivityData getRecommend() {
        return recommend;
    }

    public void setRecommend(SearchActivityData recommend) {
        this.recommend = recommend;
    }

    public DcotorComparData getCompared() {
        return compared;
    }

    public void setCompared(DcotorComparData compared) {
        this.compared = compared;
    }

    public TaoListType getTaolistType() {
        return taolistType;
    }

    public void setTaolistType(TaoListType taolistType) {
        this.taolistType = taolistType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.tao, flags);
        dest.writeParcelable(this.compared, flags);
        dest.writeParcelable(this.flash_sale, flags);
        dest.writeParcelable(this.big_promotion, flags);
        dest.writeParcelable(this.leader_board, flags);
        dest.writeParcelable(this.recommend, flags);
        dest.writeInt(this.taolistType == null ? -1 : this.taolistType.ordinal());
    }

    public SearchTao() {
    }

    protected SearchTao(Parcel in) {
        this.tao = in.readParcelable(HomeTaoData.class.getClassLoader());
        this.compared = in.readParcelable(DcotorComparData.class.getClassLoader());
        this.flash_sale = in.readParcelable(SearchActivityData.class.getClassLoader());
        this.big_promotion = in.readParcelable(SearchActivityData.class.getClassLoader());
        this.leader_board = in.readParcelable(SearchActivityData.class.getClassLoader());
        this.recommend = in.readParcelable(SearchActivityData.class.getClassLoader());
        int tmpTaolistType = in.readInt();
        this.taolistType = tmpTaolistType == -1 ? null : TaoListType.values()[tmpTaolistType];
    }

    public static final Parcelable.Creator<SearchTao> CREATOR = new Parcelable.Creator<SearchTao>() {
        @Override
        public SearchTao createFromParcel(Parcel source) {
            return new SearchTao(source);
        }

        @Override
        public SearchTao[] newArray(int size) {
            return new SearchTao[size];
        }
    };
}
