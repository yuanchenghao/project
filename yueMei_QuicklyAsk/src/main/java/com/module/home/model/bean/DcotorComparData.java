package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.community.model.bean.TaoListDoctorsCompared;

public class DcotorComparData implements Parcelable {

    private String comparedTitle;

    private ComparedBean compared;

    public String getComparedTitle() {
        return comparedTitle;
    }

    public void setComparedTitle(String comparedTitle) {
        this.comparedTitle = comparedTitle;
    }

    public ComparedBean getCompared() {
        return compared;
    }

    public void setCompared(ComparedBean compared) {
        this.compared = compared;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.comparedTitle);
        dest.writeParcelable(this.compared, flags);
    }

    public DcotorComparData() {
    }

    protected DcotorComparData(Parcel in) {
        this.comparedTitle = in.readString();
        this.compared = in.readParcelable(ComparedBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<DcotorComparData> CREATOR = new Parcelable.Creator<DcotorComparData>() {
        @Override
        public DcotorComparData createFromParcel(Parcel source) {
            return new DcotorComparData(source);
        }

        @Override
        public DcotorComparData[] newArray(int size) {
            return new DcotorComparData[size];
        }
    };
}
