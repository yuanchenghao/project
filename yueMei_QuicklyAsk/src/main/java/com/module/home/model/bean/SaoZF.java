package com.module.home.model.bean;

public class SaoZF {

	private String code;
	private String message;
	private SaozfData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SaozfData getData() {
		return data;
	}

	public void setData(SaozfData data) {
		this.data = data;
	}

}
