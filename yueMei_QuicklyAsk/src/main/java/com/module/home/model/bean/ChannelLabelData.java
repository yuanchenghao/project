package com.module.home.model.bean;

import java.util.HashMap;
import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/5
 */
public class ChannelLabelData {
    private List<ChannelLabelListData> board;
    private HashMap<String,String> more_event_params;

    public List<ChannelLabelListData> getBoard() {
        return board;
    }

    public void setBoard(List<ChannelLabelListData> board) {
        this.board = board;
    }

    public HashMap<String, String> getMore_event_params() {
        return more_event_params;
    }

    public void setMore_event_params(HashMap<String, String> more_event_params) {
        this.more_event_params = more_event_params;
    }
}
