package com.module.home.model.bean;

import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/5
 */
public class ManualPositionTag {
    private List<String> back_color;
    private String back_img;
    private ManualPositionBtnBoard left_btn_board;
    private String more_show_num;
    private List<ManualPositionBtnBoard> board_list;

    public String getBack_img() {
        return back_img;
    }

    public void setBack_img(String back_img) {
        this.back_img = back_img;
    }

    public ManualPositionBtnBoard getLeft_btn_board() {
        return left_btn_board;
    }

    public void setLeft_btn_board(ManualPositionBtnBoard left_btn_board) {
        this.left_btn_board = left_btn_board;
    }

    public String getMore_show_num() {
        return more_show_num;
    }

    public void setMore_show_num(String more_show_num) {
        this.more_show_num = more_show_num;
    }

    public List<String> getBack_color() {
        return back_color;
    }

    public void setBack_color(List<String> back_color) {
        this.back_color = back_color;
    }

    public List<ManualPositionBtnBoard> getBoard_list() {
        return board_list;
    }

    public void setBoard_list(List<ManualPositionBtnBoard> board_list) {
        this.board_list = board_list;
    }
}
