package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/7/25
 */
public class SearchCompositeLinkdata {
    private String urltitle;
    private String url;
    private HashMap<String, String> event_params;

    public String getUrltitle() {
        return urltitle;
    }

    public void setUrltitle(String urltitle) {
        this.urltitle = urltitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
