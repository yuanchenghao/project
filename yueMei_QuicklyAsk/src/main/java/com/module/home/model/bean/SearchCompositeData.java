package com.module.home.model.bean;

import com.alibaba.fastjson.JSONArray;

/**
 * Created by 裴成浩 on 2019/7/25
 */
public class SearchCompositeData {
    private String show_type;
    private String title;
    private JSONArray data;
    private SearchCompositeLinkdata linkdata;

    public String getShow_type() {
        return show_type;
    }

    public void setShow_type(String show_type) {
        this.show_type = show_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public JSONArray getData() {
        return data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }

    public SearchCompositeLinkdata getLinkdata() {
        return linkdata;
    }

    public void setLinkdata(SearchCompositeLinkdata linkdata) {
        this.linkdata = linkdata;
    }
}
