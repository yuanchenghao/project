package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.module.bean.MakeTagData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/2/25
 */
public class ChannelAllpartApi implements BaseCallBackApi {
    private String TAG = "ChannelAllpartApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.CHANNEL, "allpart", maps, new ServerCallback() {

            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.toString());
                List<MakeTagData> dataBeen = JSONUtil.jsonToArrayList(mData.data,MakeTagData.class);
                listener.onSuccess(dataBeen);
            }
        });
    }
}
