package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/3/21
 */
public class HomeAskEntry {
    private String url;
    private String img;
    private String img_width;
    private String img_height;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_width() {
        return img_width;
    }

    public void setImg_width(String img_width) {
        this.img_width = img_width;
    }

    public String getImg_height() {
        return img_height;
    }

    public void setImg_height(String img_height) {
        this.img_height = img_height;
    }
}
