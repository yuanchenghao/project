package com.module.home.model.bean;

import java.util.HashMap;

public class CardBean {

    /**
     * bmsid : 473674
     * qid : 0
     * url :
     * flag : 0
     * img : https://p11.yuemei.com/tag/1529393619ef34.png
     * img_new : https://p11.yuemei.com/tag/1529393619ef34.png
     * title : 精选日记
     * type : 6230
     * metro_line : 0
     * h_w : 1
     * desc : 精选日记
     */

    private String bmsid;
    private String qid;
    private String url;
    private String flag;
    private String img;
    private String img_new;
    private String title;
    private String type;
    private String metro_line;
    private String h_w;
    private String desc;
    private int color;
    private HashMap<String, String> event_params;
    private String isCheckin;

    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_new() {
        return img_new;
    }

    public void setImg_new(String img_new) {
        this.img_new = img_new;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMetro_line() {
        return metro_line;
    }

    public void setMetro_line(String metro_line) {
        this.metro_line = metro_line;
    }

    public String getH_w() {
        return h_w;
    }

    public void setH_w(String h_w) {
        this.h_w = h_w;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getIsCheckin() {
        return isCheckin;
    }

    public void setIsCheckin(String isCheckin) {
        this.isCheckin = isCheckin;
    }
}
