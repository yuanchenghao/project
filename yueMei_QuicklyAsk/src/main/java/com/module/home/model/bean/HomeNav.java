package com.module.home.model.bean;

import com.module.taodetail.model.bean.TaoTagItem;

import java.util.List;

/**
 * Created by dwb on 16/2/22.
 */
public class HomeNav {

    private String bgImage;

    private List<TabContent> content;
    private List<TaoTagItem> tag;

    public String getBgImage() {
        return bgImage;
    }

    public void setBgImage(String bgImage) {
        this.bgImage = bgImage;
    }

    public List<TabContent> getContent() {
        return content;
    }

    public void setContent(List<TabContent> content) {
        this.content = content;
    }

    public List<TaoTagItem> getTag() {
        return tag;
    }

    public void setTag(List<TaoTagItem> tag) {
        this.tag = tag;
    }
}
