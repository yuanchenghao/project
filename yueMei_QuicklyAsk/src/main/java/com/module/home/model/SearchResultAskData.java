package com.module.home.model;

import com.module.home.model.bean.QuestionListData;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/6/18
 */
public class SearchResultAskData {
    private List<QuestionListData> list;
    private String total;
    private String desc;
    private String type;

    public List<QuestionListData> getList() {
        return list;
    }

    public void setList(List<QuestionListData> list) {
        this.list = list;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
