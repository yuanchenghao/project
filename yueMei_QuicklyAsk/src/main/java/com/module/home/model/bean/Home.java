package com.module.home.model.bean;

/**
 * Created by dwb on 16/2/23.
 */
public class Home {

    private String code;
    private String message;
    private HuangDeng1 data;

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public HuangDeng1 getData() {
        return data;
    }

    public void setData(HuangDeng1 data) {

        this.data = data;
    }

    @Override
    public String toString() {
        return "Home{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
