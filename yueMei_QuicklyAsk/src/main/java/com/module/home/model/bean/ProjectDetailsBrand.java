package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class ProjectDetailsBrand implements Parcelable {
    private ArrayList<ProjectBrandList> brand_list;
    private String isShowAllBtn;
    private String jumpUrl;

    protected ProjectDetailsBrand(Parcel in) {
        isShowAllBtn = in.readString();
        jumpUrl = in.readString();
    }

    public static final Creator<ProjectDetailsBrand> CREATOR = new Creator<ProjectDetailsBrand>() {
        @Override
        public ProjectDetailsBrand createFromParcel(Parcel in) {
            return new ProjectDetailsBrand(in);
        }

        @Override
        public ProjectDetailsBrand[] newArray(int size) {
            return new ProjectDetailsBrand[size];
        }
    };

    public ArrayList<ProjectBrandList> getBrand_list() {
        return brand_list;
    }

    public void setBrand_list(ArrayList<ProjectBrandList> brand_list) {
        this.brand_list = brand_list;
    }

    public String getIsShowAllBtn() {
        return isShowAllBtn;
    }

    public void setIsShowAllBtn(String isShowAllBtn) {
        this.isShowAllBtn = isShowAllBtn;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(isShowAllBtn);
        dest.writeString(jumpUrl);
    }
}
