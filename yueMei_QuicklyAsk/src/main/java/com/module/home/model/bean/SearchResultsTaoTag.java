package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/5/23
 */
public class SearchResultsTaoTag {
    private String content;
    private String tagText;
    private int resource;

    public SearchResultsTaoTag(String content, String tagText, int resource) {
        this.content = content;
        this.tagText = tagText;
        this.resource = resource;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }
}
