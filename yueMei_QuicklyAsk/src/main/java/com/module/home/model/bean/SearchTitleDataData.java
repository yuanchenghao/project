package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class SearchTitleDataData implements Parcelable {

    private String type;
    private String name;
    private String title;
    private String talent;
    private String kind;
    private String sku_order_num;
    private String distance;
    private String id;
    private String img;
    private String hospital;
    private String rate;
    private String people;

    protected SearchTitleDataData(Parcel in) {
        type = in.readString();
        name = in.readString();
        title = in.readString();
        talent = in.readString();
        kind = in.readString();
        sku_order_num = in.readString();
        distance = in.readString();
        id = in.readString();
        img = in.readString();
        hospital = in.readString();
        rate = in.readString();
        people = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(name);
        dest.writeString(title);
        dest.writeString(talent);
        dest.writeString(kind);
        dest.writeString(sku_order_num);
        dest.writeString(distance);
        dest.writeString(id);
        dest.writeString(img);
        dest.writeString(hospital);
        dest.writeString(rate);
        dest.writeString(people);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchTitleDataData> CREATOR = new Creator<SearchTitleDataData>() {
        @Override
        public SearchTitleDataData createFromParcel(Parcel in) {
            return new SearchTitleDataData(in);
        }

        @Override
        public SearchTitleDataData[] newArray(int size) {
            return new SearchTitleDataData[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getSku_order_num() {
        return sku_order_num;
    }

    public void setSku_order_num(String sku_order_num) {
        this.sku_order_num = sku_order_num;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "SearchTitleDataData{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", talent='" + talent + '\'' +
                ", kind='" + kind + '\'' +
                ", id='" + id + '\'' +
                ", img='" + img + '\'' +
                ", hospital='" + hospital + '\'' +
                ", rate='" + rate + '\'' +
                ", people='" + people + '\'' +
                '}';
    }
}
