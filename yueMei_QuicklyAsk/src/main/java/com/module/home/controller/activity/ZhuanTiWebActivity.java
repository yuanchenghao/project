/**
 *
 */
package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.ScrollWebView;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.api.ShareDataApiZt;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.module.bean.TaoShareData;
import com.module.commonview.module.bean.VideoShareData;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.community.web.WebUtil;
import com.module.home.controller.other.ZhuanTiWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.Log;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.OnClick;

/**
 * 专题web页
 *
 * @author Robin
 */
@SuppressLint("SetJavaScriptEnabled")
public class ZhuanTiWebActivity extends YMBaseActivity {

    private final String TAG = "ZhuanTiWebActivity";

    public ScrollWebView docDetWeb;
    @butterknife.BindView(R.id.wan_beautiful_web_back)
    RelativeLayout wanBeautifulWebBack;
    @butterknife.BindView(R.id.wan_beautifu_docname)
    TextView titleBarTv;
    @butterknife.BindView(R.id.tao_web_share_rly111)
    RelativeLayout shareBt;
    @butterknife.BindView(R.id.wan_beautifu_linearlayout3)
    RelativeLayout contentWeb;
    @butterknife.BindView(R.id.choujiang_iv)
    ImageView choujiangIv;
    @butterknife.BindView(R.id.choujinag_colse_iv)
    ImageView choujinagColseIv;
    @butterknife.BindView(R.id.zhuanti_title)
    RelativeLayout zhuantiTitle;
    @butterknife.BindView(R.id.tao_web_refresh_rly111)
    RelativeLayout refresh;

    private ZhuanTiWebActivity mContex;

    private String urlStr;

    private String mTitle;

    public Handler handler = new Handler();

    public JSONObject obj_http;

    private String ztid = "";

    // 分享
    private String shareUrl = "";
    public String shareContent = "";
    private String shareImgUrl;
    private String shareWXImgUrl;
    private String shareTitle;

    @BindView(id = R.id.zhuanti_choujiang_rly)
    private RelativeLayout lingHongbaoRly;
    @BindView(id = R.id.choujiang_iv, click = true)
    private ImageView lingHongbaoIv;

    @BindView(id = R.id.container)
    private FrameLayout allLinely;

    private PopupWindows hongbaoPop;

    String curCity = "全国";

    private String login = "0";

    private ShareWechat mWechat;

    private BaseWebViewClientMessage baseWebViewClientMessage;
    private VideoShareData videoShareData;
    private ZhuanTiWebViewClient zhuanTiWebViewClient;
    private boolean mIsLogin;
//    private AudioManager mAudioManager;
//    private boolean isPause=false;




    @Override
    protected int getLayoutId() {
        return R.layout.web_zhuanti;
    }

    @Override
    protected void initView() {
        mContex = ZhuanTiWebActivity.this;
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) zhuantiTitle.getLayoutParams();
        layoutParams.topMargin=statusbarHeight;
        zhuantiTitle.setLayoutParams(layoutParams);
        curCity = Cfg.loadStr(mContex, FinalConstant.DWCITY, "");
        if (curCity.length() > 0) {

            if (curCity.equals("失败")) {
                curCity = "全国";
            }
        } else {
            curCity = "全国";
        }

        String scheme = getIntent().getScheme();    // 获得Scheme名称
        String data = getIntent().getDataString();  // 获得Uri全部路径
        Log.e(TAG,"scheme == "+scheme);
        Log.e(TAG,"data == "+data);

        urlStr = getIntent().getStringExtra("url");
        if (null == urlStr && !TextUtils.isEmpty(data)){
            urlStr = data;
        }
        mIsLogin = Utils.isLogin();
        if (urlStr.contains("taozt/id/6025")) {
            shareBt.setVisibility(View.GONE);
        } else {
            shareBt.setVisibility(View.VISIBLE);
        }

        mTitle = getIntent().getStringExtra("title");
        ztid = getIntent().getStringExtra("ztid");

        tongji();
        initShareData();
        setWebType();
        initWebview();
        LodUrl(urlStr);

        Log.e(TAG, "urlStr == " + urlStr);


        hongbaoPop = new PopupWindows(mContex, allLinely);

        if (null != ztid) {

            if (ztid.equals("675")) {

                String hongbao = Cfg.loadStr(mContex, "hongbao", "");
                if (hongbao.length() > 0) {
                } else {
                    new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            hongbaoPop.showAtLocation(allLinely, Gravity.BOTTOM, 0, 0);
                        }
                    }.start();
                }
            }
        }

//        mAudioManager= (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    /**
     * 设置点击事件
     */
    private void setWebType() {
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setView(allLinely);
        baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
            @Override
            public void typeOutside(WebView view, String url) {
                Log.e(TAG, "url === " + url);
                if (url.startsWith("type")) {
                    baseWebViewClientMessage.showWebDetail(url);
                } else if (url.contains("user.yuemei.com/home/taozt/")) {
                    docDetWeb.loadUrl(url);
                } else {
                    if (url.contains("player.yuemei.com")) {
                        WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "8", ztid, videoShareData);
                    } else {
                        Log.d(TAG, "--------->" + url);
                        WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "8", ztid, urlStr);
                    }
                }

            }
        });
        zhuanTiWebViewClient = new ZhuanTiWebViewClient(mContex);
        baseWebViewClientMessage.setBaseWebViewClientCallback(zhuanTiWebViewClient);

    }

    /**
     * GrowingIO统计
     */
    private void tongji() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("ztid",ztid);
        StatisticalManage.getInstance().growingIO("taozt", hashMap);
    }

    public void onResume() {
        docDetWeb.onResume();
        super.onResume();
        Log.e(TAG,"onResume  ====");
        StatService.onResume(this);
        MobclickAgent.onResume(this);
        TCAgent.onResume(this);
        initShareData();
        boolean isLogin = Utils.isLogin();
        if (!mIsLogin){
            if (isLogin) {
                String uid = Utils.getUid(mContex);
                LodUrl(urlStr + "uid/" + uid);
                mIsLogin = isLogin;
                Log.e(TAG, "onRestart==" + uid);
            }
        }

//        isPause=false;

    }

    private boolean isRestart = true;

    @Override
    protected void onRestart() {
        super.onRestart();

//        LodUrl(urlStr);
        isRestart = false;
//        docDetWeb.scrollTo(0, 0);
//        //重新加载
//        docDetWeb.reload();

    }

    @SuppressLint({"InlinedApi", "AddJavascriptInterface"})
    public void initWebview() {
        docDetWeb = new ScrollWebView(mContex);
        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示

        docDetWeb.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        docDetWeb.setLongClickable(true);
//        docDetWeb.setScrollbarFadingEnabled(true);
//        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        docDetWeb.setDrawingCacheEnabled(true);
        //设置 缓存模式
        docDetWeb.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        // 开启 DOM storage API 功能
        docDetWeb.getSettings().setDomStorageEnabled(true);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.getSettings().supportMultipleWindows();
        docDetWeb.getSettings().setNeedInitialFocus(true);
        docDetWeb.getSettings().setSupportZoom(true); // 可以缩放
        docDetWeb.getSettings().setLoadWithOverviewMode(true);// 缩放至屏幕的大小
        docDetWeb.getSettings().setUseWideViewPort(true);// 将图片调整到适合webview大小
        docDetWeb.getSettings().setBuiltInZoomControls(false);// 设置支持缩放
        docDetWeb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN); //支持内容重新布局
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                    Log.e(TAG, "onKeyDown");
                    docDetWeb.evaluateJavascript("javascript:htmlBackReFresh()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            Log.e(TAG, "value----》" + value);
                            Log.e(TAG, "isRestart----》" + isRestart);
                            if (isRestart) {
                                docDetWeb.reload();
                            }

                        }
                    });
                }
                return false;
            }

        });
        //设置 缓存模式
        WebSettings settings = docDetWeb.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setJavaScriptEnabled(true);
        docDetWeb.addJavascriptInterface(new JavaScriptCallBack(mContex, docDetWeb, urlStr), "android");
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.supportMultipleWindows();
        settings.setNeedInitialFocus(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        docDetWeb.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {

                Uri uri = Uri.parse(message);
                Log.e(TAG, "uri====" + uri);
                return super.onJsPrompt(view, url, message, defaultValue, result);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                String[] strs = message.split("\n");

                showDialogExitEdit2("确定", message, result, strs.length);

                return true;
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if ("0".equals(mTitle) || TextUtils.isEmpty(mTitle)) {
                    titleBarTv.setText(title);
                } else {
                    titleBarTv.setText(mTitle);
                }
            }
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100){
                    mDialog.stopLoading();
                }
            }


        });
        contentWeb.addView(docDetWeb);

    }


    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit2(String title, String content, final JsResult result, int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContex, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        result.confirm();       //关闭js的弹窗
        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);


        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContex, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

    }

    void initShareData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", ztid);
        if (urlStr.contains("//")){
            String str1=urlStr.substring(0, urlStr.indexOf("//"));
            String str2=urlStr.substring(str1.length()+1, urlStr.length());
            String[] params = str2.split("/");
            Log.e(TAG,"str2 == "+ str2);
            Log.e(TAG,"params == "+ Arrays.toString(params));
            for (int i = 0; i < params.length; i++) {
                if (i >3){
                    maps.put(params[i],params[i+1]);
                    i++;
                }
            }
            Log.e(TAG,"maps =="+maps.toString());
        }

        new ShareDataApiZt().getCallBack(mContex, maps, new BaseCallBackListener<TaoShareData>() {
            @Override
            public void onSuccess(TaoShareData taoShareData) {

                shareUrl = taoShareData.getUrl();
                Log.d(TAG, "shareUrl=========>" + shareUrl);
                shareContent = taoShareData.getContent();
                shareImgUrl = taoShareData.getImg_weibo();
                shareWXImgUrl = taoShareData.getImg_weix();
                shareTitle = taoShareData.getTitle();
                mWechat = taoShareData.getWechat();
                baseWebViewClientMessage.setParameter5983(ztid, "1", shareTitle, shareImgUrl, shareUrl, "16", ztid);
                videoShareData = new VideoShareData().setUrl(shareUrl).setTitle(shareTitle).setContent(shareContent).setImg_weibo(shareImgUrl).setImg_weix(shareWXImgUrl).setAskorshare("").setWechat(mWechat);

            }

        });
    }





    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    public void destroyWebView() {

        contentWeb.removeAllViews();

        if (docDetWeb != null) {
//            docDetWeb.clearHistory();
            docDetWeb.clearCache(true);
//            docDetWeb.loadUrl("about:blank"); // clearView() should be changed to loadUrl("about:blank"), since clearView() is deprecated now
            docDetWeb.freeMemory();
//            docDetWeb.pauseTimers();
            docDetWeb = null; // Note that mWebView.destroy() and mWebView = null do the exact same thing
            Log.e(TAG,"destroyWebView ==");
        }
    }


    /**
     * 加载web
     */
    public void LodUrl(String url) {
        mDialog.startLoading();
        WebUtil.getInstance().loadPage(docDetWeb,url);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        if (requestCode == 13 && resultCode == 10) {
            MyToast.makeTextToast1(mContex, "该活动已结束", 1000).show();
        }
    }

    ImageView hongbaoBgIv;
    RelativeLayout hongbaoBgRly;
    RelativeLayout hongbaoColseRly;
    Button hongbaoLingSahreBt;
    Button hongbaoShareBt;
    LinearLayout hongbaoZjly;

    @OnClick({R.id.wan_beautiful_web_back, R.id.tao_web_refresh_rly111, R.id.tao_web_share_rly111, R.id.choujiang_iv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.wan_beautiful_web_back:
                finish();
                break;
            case R.id.tao_web_refresh_rly111:

                LodUrl(urlStr);
                break;
            case R.id.tao_web_share_rly111:
                if (!TextUtils.isEmpty(shareContent)) {

                    if ("2000".equals(ztid)) {
                        if (zhuanTiWebViewClient != null) {
                            HashMap<String, String> shareParame = new LinkedHashMap<>();
                            shareParame.put("tencentUrl", shareUrl);
                            shareParame.put("tencentTitle", shareTitle);
                            shareParame.put("shareImgUrl", shareImgUrl);
                            shareParame.put("tencentText", shareContent);
                            zhuanTiWebViewClient.setShare(shareUrl, shareParame);
                        }
                    } else {
                        BaseShareView baseShareView = new BaseShareView(mContex);
                        baseShareView.setShareContent(shareContent).ShareAction(mWechat);

                        baseShareView.getShareBoardlistener().setSinaText(shareTitle + "分享自@悦美整形APP" + shareUrl).setSinaThumb(new UMImage(mContex, shareImgUrl)).setSmsText(shareTitle + "，" + shareUrl).setTencentUrl(shareUrl).setTencentTitle(shareTitle).setTencentThumb(new UMImage(mContex, shareWXImgUrl)).setTencentDescription(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                            @Override
                            public void onShareResultClick(SHARE_MEDIA platform) {
                                if (!platform.equals(SHARE_MEDIA.SMS)) {
                                    Toast.makeText(mContex, " 分享成功啦", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                            }
                        });
                    }
                }
                break;
            case R.id.choujiang_iv:
                lingHongbaoRly.setVisibility(View.GONE);
                hongbaoPop.showAtLocation(allLinely, Gravity.BOTTOM, 0, 0);
                break;
        }
    }

    /**
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(final Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_qiang_hongbao, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            hongbaoBgIv = view.findViewById(R.id.hongbao_bg_iv);
            hongbaoBgRly = view.findViewById(R.id.hongbao_bg_rly);
            hongbaoColseRly = view.findViewById(R.id.hongbao_close_cha_rly);
            hongbaoLingSahreBt = view.findViewById(R.id.lingqu_share_bt);
            hongbaoShareBt = view.findViewById(R.id.lingqu_share_bt_share);
            hongbaoZjly = view.findViewById(R.id.hongbao_zhongjia_wen_ly);

            hongbaoColseRly.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                    lingHongbaoRly.setVisibility(View.VISIBLE);
                }
            });

            hongbaoBgRly.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                    lingHongbaoRly.setVisibility(View.VISIBLE);
                }
            });

            hongbaoLingSahreBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (Utils.isLogin()) {
                        if (Utils.isBind()) {
                            hongbaoLingqu();
                        } else {
                            Utils.jumpBindingPhone(mContext);

                        }

                    } else {
                        Utils.jumpLogin(mContext);
                    }
                }
            });

            /**
             * 红包分享
             */
            hongbaoShareBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    BaseShareView baseShareView = new BaseShareView(mContex);
                    baseShareView.setShareContent("【悦美】猴年大吉，抢千元红包").ShareAction();

                    baseShareView.getShareBoardlistener().setSinaText("一不小心又中奖了！呵呵，我刚刚抢到了@悦美整形APP，网发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~http://m.yuemei.com/app/ym.html" + shareUrl).setSinaThumb(new UMImage(mContex, R.drawable.hongbao_weibo_share)).setSmsText("一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~http://m.yuemei.com/app/ym.html").setTencentUrl(shareUrl).setTencentTitle(shareTitle).setTencentThumb(new UMImage(mContex, R.drawable.hongbao_weibo_share)).setTencentDescription("一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~").setSinaText("一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~").getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                        @Override
                        public void onShareResultClick(SHARE_MEDIA platform) {
                            if (!platform.equals(SHARE_MEDIA.SMS)) {
                                Toast.makeText(mContex, " 分享成功啦", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                        }
                    });

                }
            });

        }
    }

    void hongbaoLingqu() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", "461");
        new DaiJinJuanApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null) {
                    hongbaoLingSahreBt.setVisibility(View.GONE);
                    hongbaoShareBt.setVisibility(View.VISIBLE);
                    hongbaoZjly.setVisibility(View.VISIBLE);
                    hongbaoBgIv.setBackgroundResource(R.drawable.zhongjiang_2x);

                    Cfg.saveStr(mContex, "hongbao", "1");
                } else {
                    ViewInject.toast(serverData.message);
                }
            }

        });
    }



    public void onPause() {
//        docDetWeb.reload();
        super.onPause();
        StatService.onPause(this);
        MobclickAgent.onPause(this);
        TCAgent.onPause(this);
        docDetWeb.onPause();
//        isPause=true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        destroyWebView();
        docDetWeb.destroy();
//        mAudioManager.abandonAudioFocus(audioFocusChangeListener);
    }

//    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
//        @Override
//        public void onAudioFocusChange(int focusChange) {
//            if (isPause && focusChange == AudioManager.AUDIOFOCUS_LOSS) {
//                requestAudioFocus();
//            }
//        }
//    };


//    //音频控件获取焦点
//
//    private void requestAudioFocus() {
//        int result = mAudioManager.requestAudioFocus(audioFocusChangeListener,
//                AudioManager.STREAM_MUSIC,
//                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
//        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
//            Log.e(TAG, "audio focus been granted");
//        }
//    }


}