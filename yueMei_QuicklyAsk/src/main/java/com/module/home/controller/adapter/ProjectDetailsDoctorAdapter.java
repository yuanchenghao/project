package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.RecommendDoctors;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 频道首页名医推荐适配器
 * Created by 裴成浩 on 2019/4/2
 */
public class ProjectDetailsDoctorAdapter extends RecyclerView.Adapter<ProjectDetailsDoctorAdapter.ViewHolder> {

    private final Context mContext;
    private final List<RecommendDoctors> mDatas;
    private final FunctionManager mFunctionManager;
    private String TAG = "ProjectDetailsDoctorAdapter";

    public ProjectDetailsDoctorAdapter(Context context, List<RecommendDoctors> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public ProjectDetailsDoctorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_doctor, parent, false);
        return new ProjectDetailsDoctorAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProjectDetailsDoctorAdapter.ViewHolder viewHolder, int position) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) viewHolder.mDoctor.getLayoutParams();
        if (position == 0) {
            params.leftMargin = Utils.dip2px(20);
            params.rightMargin = Utils.dip2px(5);
        } else if (position == mDatas.size() - 1) {
            params.leftMargin = Utils.dip2px(5);
            params.rightMargin = Utils.dip2px(20);
        } else {
            params.leftMargin = Utils.dip2px(5);
            params.rightMargin = Utils.dip2px(5);
        }
        viewHolder.mDoctor.setLayoutParams(params);

        RecommendDoctors data = mDatas.get(position);

        mFunctionManager.setCircleImageSrc(viewHolder.mDoctorImage, data.getImg());
        Log.e(TAG, "data.getTitle() === " + data.getTitle());
        viewHolder.mDoctorTitle.setText(data.getTitle());
        viewHolder.mDoctorHos.setText(data.getDesc());
        viewHolder.mDoctorPeople.setText(data.getOrder_num() + "人预约过");
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView mDoctor;
        ImageView mDoctorImage;
        TextView mDoctorTitle;
        TextView mDoctorHos;
        TextView mDoctorPeople;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mDoctor = itemView.findViewById(R.id.item_project_details_doctor);
            mDoctorImage = itemView.findViewById(R.id.item_project_details_doctor_img);
            mDoctorTitle = itemView.findViewById(R.id.item_project_details_doctor_name);
            mDoctorHos = itemView.findViewById(R.id.item_project_details_doctor_hos);
            mDoctorPeople = itemView.findViewById(R.id.item_project_details_doctor_people);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RecommendDoctors data = mDatas.get(getLayoutPosition());
                    String url = data.getUrl();
                    HashMap<String, String> event_params = data.getEvent_params();
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_DOCTOR, (getLayoutPosition() + 1) + ""),event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }
}
