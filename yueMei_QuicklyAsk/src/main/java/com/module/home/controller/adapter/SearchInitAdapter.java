package com.module.home.controller.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.base.view.ui.HomeBanner;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollStaggeredGridLayoutManager;
import com.module.community.controller.adapter.CommunityStagFragmentAdapter;
import com.module.community.model.bean.CommunityStaggeredListData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.controller.activity.AllProjectActivity;
import com.module.home.controller.activity.ChannelFourActivity;
import com.module.home.controller.activity.ChannelPartsActivity;
import com.module.home.controller.activity.ChannelTwoActivity;
import com.module.home.model.bean.BannerData;
import com.module.home.model.bean.HotWordsData;
import com.module.home.model.bean.SearchCompositeData;
import com.module.home.model.bean.SearchCompositeLinkdata;
import com.module.home.model.bean.SearchHotHospitalData;
import com.module.home.model.bean.SearchNavtagData;
import com.module.home.view.SearchInitHotFlowLayout;
import com.module.taodetail.model.bean.TaoTagItem;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * 搜索初始化页面列表
 * Created by 裴成浩 on 2019/9/6
 */
public class SearchInitAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<SearchCompositeData> mDatas;
    private final LayoutInflater mInflater;

    private final int ITEM_TYPE_FOUND = 0;                  //探索发现
    private final int ITEM_TYPE_HOSPITAL = 1;               //人气医院
    private final int ITEM_TYPE_HOT = 2;                    //热门推荐
    private final int ITEM_TYPE_CLASSIFICATION = 3;         //常见分类
    private final int ITEM_TYPE_RECOMMEND = 4;              //本院推荐
    private final int ITEM_TYPE_BANNER = 5;              //banner
    private String TAG = "SearchInitAdapter";

    public SearchInitAdapter(Activity context, ArrayList<SearchCompositeData> datas) {
        this.mContext = context;
        this.mDatas = datas;

        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDatas.get(position).getShow_type()) {
            case "banner":
                return ITEM_TYPE_BANNER;
            case "discovery":
                return ITEM_TYPE_FOUND;
            case "popularityHospital":
                return ITEM_TYPE_HOSPITAL;
            case "Waterfalls":
                return ITEM_TYPE_HOT;
            case "navtag":
                return ITEM_TYPE_CLASSIFICATION;
            case "searchtag":
                return ITEM_TYPE_RECOMMEND;
            default:
                return -1;
        }
    }


    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_BANNER:
                return new BannerViewHolder(mInflater.inflate(R.layout.item_project_init_banner_view, parent, false));
            case ITEM_TYPE_FOUND:
                return new FoundViewHolder(mInflater.inflate(R.layout.item_project_init_found_view, parent, false));
            case ITEM_TYPE_HOSPITAL:
                return new HospitalViewHolder(mInflater.inflate(R.layout.item_project_init_view, parent, false));
            case ITEM_TYPE_HOT:
                return new HotViewHolder(mInflater.inflate(R.layout.item_project_init_view, parent, false));
            case ITEM_TYPE_CLASSIFICATION:
                return new ClassificationViewHolder(mInflater.inflate(R.layout.item_project_init_view, parent, false));
            case ITEM_TYPE_RECOMMEND:
                return new RecommendViewHolder(mInflater.inflate(R.layout.item_project_init_view, parent, false));
            default:
                return new NullViewHolder(mInflater.inflate(R.layout.recyclerview_placeholder_view, parent, false));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BannerViewHolder) {
            setBannerView((BannerViewHolder) holder, position);
        } else if (holder instanceof FoundViewHolder) {

            setFoundView((FoundViewHolder) holder, position);

        } else if (holder instanceof HospitalViewHolder) {

            setHospitalView((HospitalViewHolder) holder, position);

        } else if (holder instanceof HotViewHolder) {

            setHotView((HotViewHolder) holder, position);

        } else if (holder instanceof ClassificationViewHolder) {

            setClassificationView((ClassificationViewHolder) holder, position);

        } else if (holder instanceof RecommendViewHolder) {

            setRecommendView((RecommendViewHolder) holder, position);

        }
    }

    private void setBannerView(BannerViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);
        final ArrayList<BannerData> mGroupData = JSONUtil.jsonToArrayList(data.getData().toJSONString(), BannerData.class);
        ArrayList<String> imgUrls = new ArrayList<>();
        for (BannerData mGroupDatum : mGroupData) {
            imgUrls.add(mGroupDatum.getImg());
        }
        holder.mBanner.setImagesData(imgUrls);
        holder.mBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(mGroupData.get(position).getUrl());
            }
        });
    }

    /**
     * 探索发现
     *
     * @param holder
     * @param position
     */
    private void setFoundView(FoundViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);
        //标题设置
        holder.title.setText(data.getTitle());

        //热门推荐词
        ArrayList<HotWordsData> mGroupData = JSONUtil.jsonToArrayList(data.getData().toJSONString(), HotWordsData.class);
        SearchInitHotFlowLayout mSearchInitHotFlowLayout = new SearchInitHotFlowLayout(mContext, holder.list, mGroupData);

        //热门推荐词点击事件
        mSearchInitHotFlowLayout.setClickCallBack(new SearchInitHotFlowLayout.ClickCallBack() {
            @Override
            public void onClick(View v, int pos, HotWordsData data) {

                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH_HOT, "" + (pos + 1)), data.getEvent_params());

                if (TextUtils.isEmpty(data.getUrl())) {
                    // 先隐藏键盘
                    Utils.hideSoftKeyboard(mContext);
                    if (onEventClickListener != null) {
                        onEventClickListener.onHotClick(v, data.getKeywords());
                    }
                } else {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUrl(), "0", "0");
                }
            }
        });
    }

    /**
     * 人气医院
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setHospitalView(HospitalViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);
        //标题设置
        holder.title.setText(data.getTitle());

        //加载更多设置
        SearchCompositeLinkdata linkdata = data.getLinkdata();
        if (linkdata != null && !TextUtils.isEmpty(linkdata.getUrltitle())) {
            holder.more.setVisibility(View.VISIBLE);
            Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.results_all_card_right);
            drawable.setBounds(0, 0, Utils.dip2px(10), Utils.dip2px(10));
            holder.more.setCompoundDrawables(null, null, drawable, null);
            holder.more.setCompoundDrawablePadding(Utils.dip2px(2));
            holder.more.setText(linkdata.getUrltitle());
        } else {
            holder.more.setVisibility(View.GONE);
        }

        //列表设置
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.list.setLayoutManager(linearLayoutManager);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.list.getItemAnimator())).setSupportsChangeAnimations(false);

        //人气医院
        ArrayList<SearchHotHospitalData> hosListDatas = JSONUtil.jsonToArrayList(data.getData().toJSONString(), SearchHotHospitalData.class);
        HotHospitalAdapter hotHospitalAdapter = new HotHospitalAdapter(mContext, hosListDatas);
        holder.list.setAdapter(hotHospitalAdapter);
    }

    /**
     * 热门推荐
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setHotView(HotViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);
        //标题设置
        holder.title.setText(data.getTitle());
        holder.title.setPadding(Utils.dip2px(14), Utils.dip2px(14), Utils.dip2px(14), 0);

        //加载更多设置
        SearchCompositeLinkdata linkdata = data.getLinkdata();
        if (linkdata != null && !TextUtils.isEmpty(linkdata.getUrltitle())) {
            holder.more.setVisibility(View.VISIBLE);
            holder.more.setPadding(Utils.dip2px(14), Utils.dip2px(14), Utils.dip2px(14), 0);
            Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.results_all_card_right);
            drawable.setBounds(0, 0, Utils.dip2px(10), Utils.dip2px(10));
            holder.more.setCompoundDrawables(null, null, drawable, null);
            holder.more.setCompoundDrawablePadding(Utils.dip2px(2));
            holder.more.setText(linkdata.getUrltitle());
        } else {
            holder.more.setVisibility(View.GONE);
        }

        //列表设置
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.list.getLayoutParams();
        layoutParams.leftMargin = Utils.dip2px(5);
        layoutParams.rightMargin = Utils.dip2px(5);
        holder.list.setLayoutParams(layoutParams);

        ScrollStaggeredGridLayoutManager scrollLinearLayoutManager = new ScrollStaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        scrollLinearLayoutManager.setScrollEnable(false);
//        scrollLinearLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        holder.list.setLayoutManager(scrollLinearLayoutManager);
        holder.list.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);

                //判断左右列，方法2（推荐）
                StaggeredGridLayoutManager.LayoutParams params = ((StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams());

                int viewLayoutPosition = params.getViewLayoutPosition();

                if (params.getSpanIndex() != GridLayoutManager.LayoutParams.INVALID_SPAN_ID) {
                    //getSpanIndex方法不管控件高度如何，始终都是左右左右返回index
                    if (viewLayoutPosition != 0) {
                        if (params.getSpanIndex() % 2 == 0) {
                            //左列
                            outRect.right = Utils.dip2px((int) 2.5);
                        } else {
                            //右列
                            outRect.left = Utils.dip2px((int) 2.5);
                        }
                    }
                }
            }
        });

        //热门推荐
        ArrayList<CommunityStaggeredListData> communityStaggeredListData = JSONUtil.jsonToArrayList(data.getData().toJSONString(), CommunityStaggeredListData.class);
        final CommunityStagFragmentAdapter communityStagFragmentAdapter = new CommunityStagFragmentAdapter(mContext, communityStaggeredListData);
        holder.list.setAdapter(communityStagFragmentAdapter);

        communityStagFragmentAdapter.setOnItemCallBackListener(new CommunityStagFragmentAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {
                List<CommunityStaggeredListData> communityStaggeredListData1 = communityStagFragmentAdapter.getmData();
                CommunityStaggeredListData listData = communityStaggeredListData1.get(pos);
                HashMap<String, String> event_params = listData.getEvent_params();
                Log.e(TAG, "event_params热门推荐item点击 == " + event_params);
                YmStatistics.getInstance().tongjiApp(event_params);
                WebUrlTypeUtil.getInstance(mContext).urlToApp(listData.getUrl());
            }
        });
    }

    /**
     * 常见分类
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setClassificationView(ClassificationViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);
        //标题设置
        holder.title.setText(data.getTitle());

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.list.getLayoutParams();
        layoutParams.leftMargin = Utils.dip2px(5);
        layoutParams.rightMargin = Utils.dip2px(5);
        holder.list.setLayoutParams(layoutParams);

        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 5);
        gridLayoutManager.setScrollEnable(false);
        holder.list.setLayoutManager(gridLayoutManager);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.list.getItemAnimator())).setSupportsChangeAnimations(false);

        //常见分类
        ArrayList<TaoTagItem> taoTagItem = JSONUtil.jsonToArrayList(data.getData().toJSONString(), TaoTagItem.class);
        SearchNavtagAdapter searchNavtagaAapter = new SearchNavtagAdapter(mContext, taoTagItem);
        holder.list.setAdapter(searchNavtagaAapter);

        searchNavtagaAapter.setOnEventClickListener(new SearchNavtagAdapter.OnEventClickListener() {
            @Override
            public void onHotClick(View v, TaoTagItem data) {
                String one_id = data.getOne_id();
                String two_id = data.getTwo_id();
                String three_id = data.getThree_id();
                String homesource = data.getHomeSource();
                String isAll = data.getIsAll();

                HashMap<String, String> event_params = data.getEvent_params();
                YmStatistics.getInstance().tongjiApp(event_params);

                if ("1".equals(isAll)) {
                    Intent intent = new Intent(mContext, AllProjectActivity.class);
                    intent.putExtra("home_source",homesource);
                    mContext.startActivity(intent);
                } else {
                    switch (data.getLevel()) {
                        case "2":
                            //部位
                            Intent intent2 = new Intent(mContext, ChannelPartsActivity.class);
                            intent2.putExtra("id", one_id);
                            intent2.putExtra("title", data.getChannel_title());
                            intent2.putExtra("home_source", homesource);
                            mContext.startActivity(intent2);
                            break;
                        case "3":
                            //二级
                            Intent intent3 = new Intent(mContext, ChannelTwoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(ChannelTwoActivity.TITLE, data.getChannel_title());
                            bundle.putString(ChannelTwoActivity.TWO_ID, two_id);
                            bundle.putString(ChannelTwoActivity.HOME_SOURCE, homesource);
                            intent3.putExtra("data", bundle);
                            mContext.startActivity(intent3);
                            break;
                        case "4":
                            //四级
                            Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                            intent4.putExtra("id", three_id);
                            intent4.putExtra("title", data.getChannel_title());
                            intent4.putExtra("home_source", homesource);
                            mContext.startActivity(intent4);
                            break;
                    }
                }
            }
        });
    }

    /**
     * 本院推荐
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setRecommendView(RecommendViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);
        //标题设置
        holder.title.setText(data.getTitle());

        //列表设置
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 5);
        gridLayoutManager.setScrollEnable(false);
        holder.list.setLayoutManager(gridLayoutManager);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.list.getItemAnimator())).setSupportsChangeAnimations(false);

        //本院推荐
        ArrayList<SearchNavtagData> hosListDatas = JSONUtil.jsonToArrayList(data.getData().toJSONString(), SearchNavtagData.class);
        SearchSearchtagAdapter searchSearchtagAapter = new SearchSearchtagAdapter(mContext, hosListDatas);
        holder.list.setAdapter(searchSearchtagAapter);

        searchSearchtagAapter.setOnEventClickListener(new SearchSearchtagAdapter.OnEventClickListener() {
            @Override
            public void onHotClick(View v, SearchNavtagData data) {
                if (onEventClickListener != null) {
                    HashMap<String, String> event_params = data.getEvent_params();
                    Log.e(TAG, "event_params常见分类item点击 == " + event_params);
                    YmStatistics.getInstance().tongjiApp(event_params);
                    onEventClickListener.onHotClick(v, data.getCate_name());
                }
            }
        });
    }


    /**
     * banner
     */
    public class BannerViewHolder extends RecyclerView.ViewHolder {
        private HomeBanner mBanner;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            mBanner = itemView.findViewById(R.id.item_project_banner);
        }
    }

    /**
     * 探索发现
     */
    public class FoundViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private FlowLayout list;

        public FoundViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_init_found_title);
            list = itemView.findViewById(R.id.item_init_found_list);
        }
    }

    /**
     * 人气医院
     */
    public class HospitalViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView more;
        private RecyclerView list;

        public HospitalViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_init_title);
            more = itemView.findViewById(R.id.item_init_title_more);
            list = itemView.findViewById(R.id.item_init_list);

            //点击查看更多
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchCompositeData searchCompositeData = mDatas.get(getLayoutPosition());
                        SearchCompositeLinkdata linkdata = searchCompositeData.getLinkdata();
                        String url = linkdata.getUrl();
                        Log.e(TAG, "url == " + url);
                        if (!TextUtils.isEmpty(url)) {
                            HashMap<String, String> event_params = linkdata.getEvent_params();
                            Log.e(TAG, "event_params人气医院更多参数 == " + event_params);
                            YmStatistics.getInstance().tongjiApp(event_params);
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, url);
                        }
                    }
                }
            });
        }
    }

    /**
     * 热门推荐
     */
    public class HotViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView more;
        private RecyclerView list;

        public HotViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_init_title);
            more = itemView.findViewById(R.id.item_init_title_more);
            list = itemView.findViewById(R.id.item_init_list);

            //点击查看更多
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchCompositeData searchCompositeData = mDatas.get(getLayoutPosition());
                        SearchCompositeLinkdata linkdata = searchCompositeData.getLinkdata();
                        String url = linkdata.getUrl();
                        Log.e(TAG, "url == " + url);
                        if (!TextUtils.isEmpty(url)) {
                            HashMap<String, String> event_params = linkdata.getEvent_params();
                            Log.e(TAG, "event_params人气医院更多参数 == " + event_params);
                            YmStatistics.getInstance().tongjiApp(event_params);
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, url);
                        }
                    }
                }
            });
        }
    }

    /**
     * 常见分类
     */
    public class ClassificationViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private RecyclerView list;

        public ClassificationViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_init_title);
            list = itemView.findViewById(R.id.item_init_list);
        }
    }

    /**
     * 本院推荐
     */
    public class RecommendViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private RecyclerView list;

        public RecommendViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_init_title);
            list = itemView.findViewById(R.id.item_init_list);
        }
    }

    /**
     * 占位数据
     */
    public class NullViewHolder extends RecyclerView.ViewHolder {
        NullViewHolder(View itemView) {
            super(itemView);
        }
    }

    //item点击回调
    public interface OnEventClickListener {
        void onHotClick(View v, String key);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
