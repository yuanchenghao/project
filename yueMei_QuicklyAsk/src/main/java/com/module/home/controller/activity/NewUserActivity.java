package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.api.ShareDataApiZt;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.module.bean.TaoShareData;
import com.module.commonview.module.bean.VideoShareData;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.home.controller.other.NewUserViewClient;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class NewUserActivity extends BaseActivity {

    private final String TAG = "NewUserActivity";

    @org.kymjs.aframe.ui.BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private RelativeLayout contentWeb;

    @org.kymjs.aframe.ui.BindView(id = R.id.wan_beautiful_web_back, click = true)
    private RelativeLayout back;// 返回
    @org.kymjs.aframe.ui.BindView(id = R.id.wan_beautifu_docname)
    private TextView titleBarTv;
    @org.kymjs.aframe.ui.BindView(id = R.id.tao_web_share_rly111, click = true)
    private RelativeLayout shareBt;// 分享

    @org.kymjs.aframe.ui.BindView(id = R.id.tao_web_refresh_rly111, click = true)
    private RelativeLayout refreshBt;// 刷新

    public WebView docDetWeb;

    private NewUserActivity mContex;

    private String urlStr = "file:///android_asset/newuser.html";

    private String mTitle;

    public Handler handler = new Handler();

    public JSONObject obj_http;

    private String ztid = "0";

    // 分享
    private String shareUrl = "";
    public String shareContent = "";
    private String shareImgUrl;
    private String shareWXImgUrl;
    private String shareTitle;

    @org.kymjs.aframe.ui.BindView(id = R.id.zhuanti_choujiang_rly)
    private RelativeLayout lingHongbaoRly;
    @org.kymjs.aframe.ui.BindView(id = R.id.choujiang_iv, click = true)
    private ImageView lingHongbaoIv;

    @org.kymjs.aframe.ui.BindView(id = R.id.all_ly)
    private LinearLayout allLinely;

    private NewUserActivity.PopupWindows hongbaoPop;

    String curCity = "全国";

    private String login = "0";

    private ShareWechat mWechat;

    private BaseWebViewClientMessage baseWebViewClientMessage;
    private VideoShareData videoShareData;
    private NewUserViewClient zhuanTiWebViewClient;


    @Override
    public void setRootView() {
        setContentView(R.layout.activity_new_user);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = NewUserActivity.this;

        curCity = Cfg.loadStr(mContex, FinalConstant.DWCITY, "");
        if (curCity.length() > 0) {

            if (curCity.equals("失败")) {
                curCity = "全国";
            }
        } else {
            curCity = "全国";
        }

        if (urlStr.contains("taozt/id/6025")) {
            refreshBt.setVisibility(View.GONE);
            shareBt.setVisibility(View.GONE);
        } else {
            refreshBt.setVisibility(View.VISIBLE);
            shareBt.setVisibility(View.VISIBLE);
        }


        tongji();
        initShareData();
        setWebType();
        initWebview();

        com.umeng.socialize.utils.Log.e(TAG, "urlStr == " + urlStr);
        LodUrl(urlStr);

        hongbaoPop = new NewUserActivity.PopupWindows(mContex, allLinely);

        if (null != ztid) {

            if (ztid.equals("675")) {

                String hongbao = Cfg.loadStr(mContex, "hongbao", "");
                if (hongbao.length() > 0) {
                } else {
                    new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            hongbaoPop.showAtLocation(allLinely, Gravity.BOTTOM, 0, 0);
                        }
                    }.start();
                }
            }
        }

    }

    /**
     * 设置点击事件
     */
    private void setWebType() {
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
            @Override
            public void typeOutside(WebView view, String url) {
                com.umeng.socialize.utils.Log.e(TAG, "url === " + url);
                if (url.startsWith("type")) {
                    baseWebViewClientMessage.showWebDetail(url);
                } else if (url.contains("user.yuemei.com/home/taozt/")) {
                    docDetWeb.loadUrl(url);
                } else {
                    if (url.contains("player.yuemei.com")) {
                        WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "8", ztid, videoShareData);
                    } else {
                        com.umeng.socialize.utils.Log.d(TAG, "--------->" + urlStr);
                        WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "8", ztid, urlStr);
                    }
                }

            }
        });
        zhuanTiWebViewClient = new NewUserViewClient(mContex);
        baseWebViewClientMessage.setBaseWebViewClientCallback(zhuanTiWebViewClient);

    }

    /**
     * GrowingIO统计
     */
    private void tongji() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("ztid", ztid);
        StatisticalManage.getInstance().growingIO("taozt", hashMap);
    }

    public void onResume() {
        super.onResume();
        StatService.onResume(this);
        MobclickAgent.onResume(this);
        TCAgent.onResume(this);

        initShareData();
        LodUrl(urlStr);
    }


    @SuppressLint({"InlinedApi", "AddJavascriptInterface", "SetJavaScriptEnabled"})
    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示

        docDetWeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        docDetWeb.setLongClickable(true);
        docDetWeb.setScrollbarFadingEnabled(true);
        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        docDetWeb.setDrawingCacheEnabled(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        //设置 缓存模式
        WebSettings settings = docDetWeb.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setJavaScriptEnabled(true);
        docDetWeb.addJavascriptInterface(new JavaScriptCallBack(mContex, docDetWeb, urlStr), "android");
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.supportMultipleWindows();
        settings.setNeedInitialFocus(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        docDetWeb.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                String[] strs = message.split("\n");

                showDialogExitEdit2("确定", message, result, strs.length);

                return true;
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

        });


        contentWeb.addView(docDetWeb);
    }


    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit2(String title, String content, final JsResult result, int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContex, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        result.confirm();       //关闭js的弹窗
        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);


        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContex, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

    }

    void initShareData() {

        Map<String, Object> maps = new HashMap<>();
        maps.put("id", ztid);
        new ShareDataApiZt().getCallBack(mContex, maps, new BaseCallBackListener<TaoShareData>() {
            @Override
            public void onSuccess(TaoShareData taoShareData) {

                shareUrl = taoShareData.getUrl();
                com.umeng.socialize.utils.Log.d(TAG, "shareUrl=========>" + shareUrl);
                shareContent = taoShareData.getContent();
                shareImgUrl = taoShareData.getImg_weibo();
                shareWXImgUrl = taoShareData.getImg_weix();
                shareTitle = taoShareData.getTitle();
                mWechat = taoShareData.getWechat();
                baseWebViewClientMessage.setParameter5983(ztid, "1", shareTitle, shareImgUrl, shareUrl,"0","0");
                videoShareData = new VideoShareData().setUrl(shareUrl).setTitle(shareTitle).setContent(shareContent).setImg_weibo(shareImgUrl).setImg_weix(shareWXImgUrl).setAskorshare("").setWechat(mWechat);

            }

        });
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }


    /**
     * 加载web
     */
    public void LodUrl(String url) {
        baseWebViewClientMessage.startLoading();
//        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
//        if (null != docDetWeb) {
//            com.umeng.socialize.utils.Log.e(TAG, "addressAndHead.getUrl() === " + addressAndHead.getUrl());
//            com.umeng.socialize.utils.Log.e(TAG, "addressAndHead.getHttpHeaders() === " + addressAndHead.getHttpHeaders());
//            docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
//        }

        docDetWeb.loadUrl(url);
    }



    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.wan_beautiful_web_back:
                finish();
                break;
            case R.id.tao_web_share_rly111:// 分享按钮
                if (!TextUtils.isEmpty(shareContent)) {

                    if ("2000".equals(ztid)) {
                        if (zhuanTiWebViewClient != null) {
                            HashMap<String, String> shareParame = new LinkedHashMap<>();
                            shareParame.put("tencentUrl", shareUrl);
                            shareParame.put("tencentTitle", shareTitle);
                            shareParame.put("shareImgUrl", shareImgUrl);
                            shareParame.put("tencentText", shareContent);
                            zhuanTiWebViewClient.setShare(shareUrl, shareParame);
                        }
                    } else {
                        BaseShareView baseShareView = new BaseShareView(mContex);
                        baseShareView.setShareContent(shareContent).ShareAction(mWechat);

                        baseShareView.getShareBoardlistener().setSinaText(shareTitle + "分享自@悦美整形APP" + shareUrl).setSinaThumb(new UMImage(mContex, shareImgUrl)).setSmsText(shareTitle + "，" + shareUrl).setTencentUrl(shareUrl).setTencentTitle(shareTitle).setTencentThumb(new UMImage(mContex, shareWXImgUrl)).setTencentDescription(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                            @Override
                            public void onShareResultClick(SHARE_MEDIA platform) {
                                if (!platform.equals(SHARE_MEDIA.SMS)) {
                                    Toast.makeText(mContex, " 分享成功啦", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                            }
                        });
                    }
                }

                break;

            case R.id.tao_web_refresh_rly111:
                webReload();

                break;
            case R.id.choujiang_iv:// 抢红包
                lingHongbaoRly.setVisibility(View.GONE);
                hongbaoPop.showAtLocation(allLinely, Gravity.BOTTOM, 0, 0);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        if (requestCode == 13 && resultCode == 10) {
            MyToast.makeTextToast1(mContex, "该活动已结束", 1000).show();
        }
    }

    ImageView hongbaoBgIv;
    RelativeLayout hongbaoBgRly;
    RelativeLayout hongbaoColseRly;
    Button hongbaoLingSahreBt;
    Button hongbaoShareBt;
    LinearLayout hongbaoZjly;

    /**
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(final Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_qiang_hongbao, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            hongbaoBgIv = view.findViewById(R.id.hongbao_bg_iv);
            hongbaoBgRly = view.findViewById(R.id.hongbao_bg_rly);
            hongbaoColseRly = view.findViewById(R.id.hongbao_close_cha_rly);
            hongbaoLingSahreBt = view.findViewById(R.id.lingqu_share_bt);
            hongbaoShareBt = view.findViewById(R.id.lingqu_share_bt_share);
            hongbaoZjly = view.findViewById(R.id.hongbao_zhongjia_wen_ly);

            hongbaoColseRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                    lingHongbaoRly.setVisibility(View.VISIBLE);
                }
            });

            hongbaoBgRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                    lingHongbaoRly.setVisibility(View.VISIBLE);
                }
            });

            hongbaoLingSahreBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (Utils.isLogin()) {
                        if (Utils.isBind()) {
                            hongbaoLingqu();
                        } else {
                            Utils.jumpBindingPhone(mContext);

                        }

                    } else {
                        Utils.jumpLogin(mContext);
                    }
                }
            });

            /**
             * 红包分享
             */
            hongbaoShareBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    BaseShareView baseShareView = new BaseShareView(mContex);
                    baseShareView.setShareContent("【悦美】猴年大吉，抢千元红包").ShareAction();

                    baseShareView.getShareBoardlistener().setSinaText("一不小心又中奖了！呵呵，我刚刚抢到了@悦美整形APP，网发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~http://m.yuemei.com/app/ym.html" + shareUrl).setSinaThumb(new UMImage(mContex, R.drawable.hongbao_weibo_share)).setSmsText("一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~http://m.yuemei.com/app/ym.html").setTencentUrl(shareUrl).setTencentTitle(shareTitle).setTencentThumb(new UMImage(mContex, R.drawable.hongbao_weibo_share)).setTencentDescription("一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~").setSinaText("一不小心又中奖了！呵呵，我刚刚抢到了悦美整形APP发出的红包！新的一年肯定必有特福（beautiful）〜下载悦美微整形APP，试试手气吧~").getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                        @Override
                        public void onShareResultClick(SHARE_MEDIA platform) {
                            if (!platform.equals(SHARE_MEDIA.SMS)) {
                                Toast.makeText(mContex, " 分享成功啦", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                        }
                    });

                }
            });

        }
    }

    void hongbaoLingqu() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", "461");
        new DaiJinJuanApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null) {
                    hongbaoLingSahreBt.setVisibility(View.GONE);
                    hongbaoShareBt.setVisibility(View.VISIBLE);
                    hongbaoZjly.setVisibility(View.VISIBLE);
                    hongbaoBgIv.setBackgroundResource(R.drawable.zhongjiang_2x);

                    Cfg.saveStr(mContex, "hongbao", "1");
                } else {
                    ViewInject.toast(serverData.message);
                }
            }

        });
    }

    public void onPause() {
        super.onPause();
        StatService.onPause(this);
        MobclickAgent.onPause(this);
        TCAgent.onPause(this);
    }

}