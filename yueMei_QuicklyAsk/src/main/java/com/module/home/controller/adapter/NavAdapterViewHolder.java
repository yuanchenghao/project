package com.module.home.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.home.view.Holder;
import com.module.taodetail.model.bean.TaoTagItem;
import com.quicklyask.activity.R;

/**
 * @Author: Zaaach
 * @Date: 2019/11/25
 * @Email: zaaach@aliyun.com
 * @Description:
 */
public class NavAdapterViewHolder extends Holder<TaoTagItem> {
    LinearLayout tagView;
    ImageView tagImg;
    TextView tagTitle;

    public NavAdapterViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    @Override
    protected void initView(View itemView) {
        tagView = itemView.findViewById(R.id.tag_view);
        tagImg = itemView.findViewById(R.id.tag_img);
        tagTitle = itemView.findViewById(R.id.tag_title);
    }

    @Override
    public void bindData(Context context, TaoTagItem data) {
        Glide.with(context)
                .load(data.getImg())
                .into(tagImg);

        tagTitle.setText(data.getTitle());
    }
}
