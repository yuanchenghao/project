/**
 *
 */
package com.module.home.controller.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.cn.demo.pinyin.AssortView;
import com.cn.demo.pinyin.AssortView.OnTouchAssortListener;
import com.cn.demo.pinyin.PinyinAdapter591;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.InitLoadCityApi;
import com.module.commonview.utils.StatisticalManage;
import com.module.doctor.controller.activity.TabDocAndHosListActivity;
import com.module.doctor.controller.adapter.HotCityAdapter;
import com.module.doctor.model.api.HotCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.MainCityData;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.other.RequestAndResultCode;
import com.module.taodetail.controller.activity.TaoActivity;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyGridView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 标题栏城市选择
 *
 * @author Robin
 */
public class MainCitySelectActivity560 extends BaseActivity {

    private final String TAG = "MainCitySelect";

    private Context mContext;
    private ExpandableListView eListView;
    private AssortView assortView;
    private PinyinAdapter591 pinAdapter;

    private View headView;
    private String city = "";
    private String cityLocation;
    private RelativeLayout back;

    private MyGridView hotGridlist;
    private HotCityAdapter hotcityAdapter;

    private TextView title;
    private TextView cityLocTv;


    private String type;

    private List<CityDocDataitem> hotcityList;

    private List<MainCityData> citylist = new ArrayList<>();

    // 城市缓存
    private KJDB kjdb;
    private List<MainCityData> citylistCache = new ArrayList<>();
    private RelativeLayout cityAntuo;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_city_select_doc);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = MainCitySelectActivity560.this;
        cityLocation = Cfg.loadStr(mContext, FinalConstant.LOCATING_CITY, "失败");
        initView();
        kjdb = KJDB.create(mContext, "yuemeicity");

        Intent it = getIntent();
        type = it.getStringExtra("type");

        title.setText("当前所选-" + Utils.getCity());

        initViewData();
        cityLocTv.setText(cityLocation);
    }

    void initView() {
        eListView = findViewById(R.id.elist1);
        assortView = findViewById(R.id.assort1);
        back = findViewById(R.id.doc_list_city_back);
        title = findViewById(R.id.doc_list_city_title_tv);

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }

    void initViewData() {

        LayoutInflater mInflater = getLayoutInflater();
        headView = mInflater.inflate(R.layout.main_city_select__head_560, null);
        eListView.addHeaderView(headView);
        hotGridlist = headView.findViewById(R.id.group_grid_list1);

        // 比较当前时间
        int cur1 = (int) System.currentTimeMillis();
        int cur2 = Cfg.loadInt(mContext, "timeCity", 0);
        if ((cur1 - cur2) >= (3600 * 1000 * 24 * 7)) {
            initloadCity();
        } else {
            citylistCache = kjdb.findAll(MainCityData.class);

            if (null != citylistCache && citylistCache.size() > 0) {

                pinAdapter = new PinyinAdapter591(mContext, citylistCache);
                eListView.setAdapter(pinAdapter);

                // 展开所有
                for (int i = 0, length = pinAdapter.getGroupCount(); i < length; i++) {
                    eListView.expandGroup(i);
                }

                // 字母按键回调
                assortView.setOnTouchAssortListener(new OnTouchAssortListener() {

                    View layoutView = LayoutInflater.from(mContext)
                            .inflate(R.layout.alert_dialog_menu_layout,
                                    null);

                    RelativeLayout alRly = (RelativeLayout) layoutView
                            .findViewById(R.id.pop_city_rly);

                    PopupWindow popupWindow;

                    public void onTouchAssortListener(String str) {
                        int index = pinAdapter.getAssort()
                                .getHashList().indexOfKey(str);
                        if (index != -1) {
                            eListView.setSelectedGroup(index);
                        }
                        if (popupWindow != null) {
                            // text.setText(str);
                        } else {
                            popupWindow = new PopupWindow(layoutView,
                                    80, 80, false);
                            popupWindow.showAtLocation(alRly,
                                    Gravity.CENTER, 0, 0);
                        }
                    }

                    public void onTouchAssortUP() {
                        if (popupWindow != null)
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                });
                //非十二城点击
                eListView.setOnChildClickListener(new OnChildClickListener() {

                    @Override
                    public boolean onChildClick(ExpandableListView arg0,
                                                View arg1, int groupPosition, int childPosition,
                                                long arg4) {
                        int tolal = 0;

                        for (int j = 0; j < groupPosition; j++) {
                            int chidsize1 = pinAdapter.getChildrenCount(j);
                            tolal = tolal + chidsize1;
                        }

                        city = PinyinAdapter591.assortAA.getHashList()
                                .getValueIndex(groupPosition, childPosition);

                        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                        Utils.getCityOneToHttp(mContext, "1");

                        startFinish();
                        Log.e(TAG, "选择的城市是 === " + city);

                        cityStatistics();

                        return true;
                    }
                });
            } else {
                initloadCity();
            }
        }


        loadHotCity();

        RelativeLayout cityAll = headView.findViewById(R.id.city_all_doc_rly);
        cityAll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                city = "全国";
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                Utils.getCityOneToHttp(mContext, "1");

                cityStatistics();

                startFinish();
            }
        });

        cityLocTv = headView.findViewById(R.id.doc_city_select_tv);
        cityAntuo = headView.findViewById(R.id.city_auto_loaction_rly);
        cityAntuo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(MainCitySelectActivity560.this).request(new AcpOptions.Builder()
                                    .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)
                                    .build(),
                            new AcpListener() {
                                @Override
                                public void onGranted() {
                                    cityLocTv.setText(cityLocation);

                                    if (!"失败".equals(cityLocation)) {

                                        locatingCity();
                                    }
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                }
                            });
                } else {

                    if (null != cityLocation && cityLocation.length() > 0) {

                        locatingCity();
                    } else {
                        ViewInject.toast("正在获取...");
                    }

                }
            }
        });

    }

    private void startFinish() {
        if ("2".equals(type)) {
            Intent it1 = new Intent();
            it1.putExtra("city", city);
            setResult(4, it1);
        } else if ("4".equals(type)) {
            Intent it1 = new Intent(mContext, TabDocAndHosListActivity.class);
            it1.putExtra("city", city);
            setResult(4, it1);
        } else if ("5".equals(type)) {
            Intent it1 = new Intent(mContext, TaoActivity.class);
            it1.putExtra("city", city);
            setResult(5, it1);
        } else {
            Intent it1 = new Intent();
            it1.putExtra("city", city);
            setResult(RequestAndResultCode.CITY_RESULT_CODE, it1);
        }

        onBackPressed();
    }

    /**
     * 定位城市
     */
    private void locatingCity() {
        city = cityLocation;
        if ("失败".equals(city)) {
            city = "全国";
        }
        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
        Utils.getCityOneToHttp(mContext, "1");

        cityStatistics();
        startFinish();
    }

    private void cityStatistics() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("area01", "失败".equals(city) ? "全国" : city);
        StatisticalManage.getInstance().growingIO("location_area", hashMap);
    }

    private void initloadCity() {
        new InitLoadCityApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    citylist = JSONUtil.jsonToArrayList(serverData.data, MainCityData.class);
                    if (null != citylist && citylist.size() > 0) {
                        pinAdapter = new PinyinAdapter591(mContext, citylist);
                        eListView.setAdapter(pinAdapter);
                    }

                    // 展开所有
                    for (int i = 0, length = pinAdapter
                            .getGroupCount(); i < length; i++) {
                        eListView.expandGroup(i);
                    }

                    // 字母按键回调
                    assortView.setOnTouchAssortListener(new OnTouchAssortListener() {

                        View layoutView = LayoutInflater
                                .from(mContext)
                                .inflate(
                                        R.layout.alert_dialog_menu_layout,
                                        null);

                        RelativeLayout alRly = (RelativeLayout) layoutView
                                .findViewById(R.id.pop_city_rly);

                        PopupWindow popupWindow;

                        public void onTouchAssortListener(
                                String str) {
                            int index = pinAdapter
                                    .getAssort()
                                    .getHashList()
                                    .indexOfKey(str);
                            if (index != -1) {
                                eListView.setSelectedGroup(index);
                            }
                            if (popupWindow != null) {
                                // text.setText(str);
                            } else {
                                popupWindow = new PopupWindow(
                                        layoutView, 80, 80,
                                        false);
                                popupWindow.showAtLocation(
                                        alRly,
                                        Gravity.CENTER, 0,
                                        0);
                            }
                        }

                        public void onTouchAssortUP() {
                            if (popupWindow != null)
                                popupWindow.dismiss();
                            popupWindow = null;
                        }
                    });

                    eListView.setOnChildClickListener(new OnChildClickListener() {

                        @Override
                        public boolean onChildClick(
                                ExpandableListView arg0,
                                View arg1,
                                int groupPosition,
                                int childPosition, long arg4) {
                            int tolal = 0;

                            for (int j = 0; j < groupPosition; j++) {
                                int chidsize1 = pinAdapter
                                        .getChildrenCount(j);
                                tolal = tolal + chidsize1;
                            }
                            city = PinyinAdapter591.assortAA
                                    .getHashList()
                                    .getValueIndex(
                                            groupPosition,
                                            childPosition);

                            Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                            Utils.getCityOneToHttp(mContext, "1");

                            cityStatistics();

                            startFinish();

                            return true;
                        }
                    });

                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            // 数据保存
                            List<MainCityData> hsdatas = kjdb.findAll(MainCityData.class);

                            if (null != hsdatas && hsdatas.size() > 0) {
                                for (int i = 0; i < hsdatas.size(); i++) {
                                    kjdb.delete(hsdatas.get(i));
                                }
                            }
                            for (int i = 0; i < citylist.size(); i++) {
                                try {
                                    kjdb.save(citylist.get(i));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "Exception====" + e.toString());
                                }
                            }
                            // 保存当前时间
                            int cur = (int) System.currentTimeMillis();
                            Cfg.saveInt(mContext, "timeCity", cur);
                        }
                    }).start();
                }
            }
        });

    }

    void loadHotCity() {
        new HotCityApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitem) {
                hotcityList = cityDocDataitem;
                hotcityAdapter = new HotCityAdapter(mContext,
                        hotcityList);
                hotGridlist.setAdapter(hotcityAdapter);
                hotGridlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    //十二城点击
                    @Override
                    public void onItemClick(
                            AdapterView<?> arg0,
                            View arg1, int pos,
                            long arg3) {
                        city = hotcityList.get(pos).getName();
                        Log.e(TAG, "city == " + city);
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                        Utils.getCityOneToHttp(mContext, "1");

                        cityStatistics();

                        startFinish();
                    }
                });
            }

        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
