package com.module.home.controller.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.home.model.bean.ProjectDetailsListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/27
 */
public class ProjectDetailsTagAdapter extends RecyclerView.Adapter<ProjectDetailsTagAdapter.ViewHolder> {

    private String TAG = "ProjectDetailsTagAdapter";
    private Activity mContext;
    private ArrayList<ProjectDetailsListData> mDatas;                                       //当前要使用的Data数据
    private ArrayList<ProjectDetailsListData> mClosedDatas = new ArrayList<>();             //收起的数据
    private ArrayList<ProjectDetailsListData> mAnDatas = new ArrayList<>();                 //展开的数据
    private final String IS_SELECTED = "is_selected";
    private final int MAX_NUMBER = 6;
    private final String CLICK_MORE = "更多";
    private final String CLICK_PACK = "收起";
    private final int windowsWight;

    public ProjectDetailsTagAdapter(Activity context, ArrayList<ProjectDetailsListData> datas) {
        this.mContext = context;

        //设置收缩数据
        if (datas.size() > MAX_NUMBER) {
            for (int i = 0; i < MAX_NUMBER - 1; i++) {
                mClosedDatas.add(datas.get(i));
            }
            mClosedDatas.add(MAX_NUMBER - 1, new ProjectDetailsListData("0", CLICK_MORE, false));

            //设置展开数据
            mAnDatas.addAll(datas);
            mAnDatas.add(datas.size(), new ProjectDetailsListData("0", CLICK_PACK, true));
            //当前数据为收缩数据
            mDatas = new ArrayList<>(mClosedDatas);
        } else {
            mDatas = new ArrayList<>(datas);
        }

        Log.e(TAG, "mDatas = " + mDatas.size());
        Log.e(TAG, "mClosedDatas = " + mClosedDatas.size());
        Log.e(TAG, "mAnDatas = " + mAnDatas.size());

        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_tag, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(viewHolder, position, payloads);
        if (payloads.isEmpty()) {
            onBindViewHolder(viewHolder, position);
        } else {
            for (Object payload : payloads) {
                switch ((String) payload) {
                    case IS_SELECTED:
                        setItemUI(viewHolder, position);
                        break;
                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ViewGroup.MarginLayoutParams layoutParams1 = (ViewGroup.MarginLayoutParams) viewHolder.projectDetailsTag.getLayoutParams();
        layoutParams1.leftMargin = Utils.dip2px(5.5f);
        layoutParams1.rightMargin = Utils.dip2px(5.5f);
        layoutParams1.width = (windowsWight - Utils.dip2px(52)) / 3;
        Log.e(TAG, "(windowsWight - Utils.dip2px(52)) / 3 == " + (windowsWight - Utils.dip2px(52)) / 3);
        viewHolder.projectDetailsTag.setLayoutParams(layoutParams1);

        viewHolder.projectTagText.setText(mDatas.get(position).getName());

        setItemUI(viewHolder, position);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    private void setItemUI(@NonNull ViewHolder viewHolder, int position) {
        if ("0".equals(mDatas.get(position).getId())) {
            viewHolder.projectTagText.setCompoundDrawablePadding(Utils.dip2px(6));
            if (mDatas.get(position).isSelected()) {
                Drawable nav_up = mContext.getResources().getDrawable(R.drawable.item_project_details_tag_on);
                nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());
                viewHolder.projectTagText.setCompoundDrawables(null, null, nav_up, null);
            } else {
                Drawable nav_up = mContext.getResources().getDrawable(R.drawable.item_project_details_tag_under);
                nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());
                viewHolder.projectTagText.setCompoundDrawables(null, null, nav_up, null);
            }
            viewHolder.projectTagText.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            viewHolder.projectDetailsTag.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_yuanjiao_f6f6f6));
        } else {
            viewHolder.projectTagText.setCompoundDrawablePadding(0);
            viewHolder.projectTagText.setCompoundDrawables(null, null, null, null);
            if (mDatas.get(position).isSelected()) {
                viewHolder.projectTagText.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                viewHolder.projectDetailsTag.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_tuoyuan_fdf1f3));
            } else {
                viewHolder.projectTagText.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                viewHolder.projectDetailsTag.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_yuanjiao_f6f6f6));
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout projectDetailsTag;
        TextView projectTagText;

        public ViewHolder(View itemView) {
            super(itemView);
            projectDetailsTag = itemView.findViewById(R.id.item_project_details_tag);
            projectTagText = itemView.findViewById(R.id.project_details_tag_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        ProjectDetailsListData data = mDatas.get(getLayoutPosition());
                        if ("0".equals(data.getId())) {
                            //点击的是展开收缩
                            int clickItemPos = getClickItemPos();
                            if (clickItemPos >= 0) {
                                //展开收起
                                Log.e(TAG, "mDatas1111 = " + mDatas.size());
                                Log.e(TAG, "mClosedDatas1111 = " + mClosedDatas.size());
                                Log.e(TAG, "mAnDatas1111 = " + mAnDatas.size());
                                mDatas.clear();
                                Log.e(TAG, "mDatas2222 = " + mDatas.size());
                                Log.e(TAG, "mClosedDatas2222 = " + mClosedDatas.size());
                                Log.e(TAG, "mAnDatas2222 = " + mAnDatas.size());
                                if (data.isSelected()) {
                                    //展开状态，改为收起状态
                                    mDatas = new ArrayList<>(mClosedDatas);
                                    Log.e(TAG, "mDatas333 = " + mDatas.size());
                                    Log.e(TAG, "mClosedDatas333 = " + mClosedDatas.size());
                                    Log.e(TAG, "mAnDatas333 = " + mAnDatas.size());
                                } else {
                                    //收起状态，改为展开状态
                                    mDatas = new ArrayList<>(mAnDatas);
                                    Log.e(TAG, "mDatas444 = " + mDatas.size());
                                    Log.e(TAG, "mClosedDatas444 = " + mClosedDatas.size());
                                    Log.e(TAG, "mAnDatas444 = " + mAnDatas.size());
                                }

                                notifyDataSetChanged();
                            }
                        } else {
                            if (!data.isSelected()) {
                                Utils.tongjiApp(mContext, "channel_part", (getLayoutPosition() + 1) + "", data.getId(), "96");
                                setSelected(getLayoutPosition());

                                onEventClickListener.onLoadedClick(getSelectedData());
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 设置选中
     *
     * @param pos 选中item的下标
     */
    private void setSelected(int pos) {
        for (int i = 0; i < mDatas.size(); i++) {
            ProjectDetailsListData data = mDatas.get(i);
            if (data.isSelected() && pos != i && !"0".equals(data.getId())) {
                data.setSelected(false);
                notifyItemChanged(i, IS_SELECTED);
                break;
            }
        }

        if(!"0".equals(mDatas.get(pos).getId())){
            mDatas.get(pos).setSelected(true);
            notifyItemChanged(pos, IS_SELECTED);
        }
    }

    /**
     * 获取展开收起按钮的下标
     *
     * @return 如果返回-1说明不存在
     */
    private int getClickItemPos() {
        for (int i = 0; i < mDatas.size(); i++) {
            if ("0".equals(mDatas.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 获取选中项数据
     *
     * @return 选中项对象
     */
    private ProjectDetailsListData getSelectedData() {
        for (ProjectDetailsListData data : mDatas) {
            if (data.isSelected()) {
                return data;
            }
        }
        return null;
    }

    public interface OnEventClickListener {
        void onLoadedClick(ProjectDetailsListData data);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
