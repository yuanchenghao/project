package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.SearchResultLike;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.List;

/**
 * 搜索猜你喜欢适配器
 * Created by 裴成浩 on 2019/5/25
 */
public class SearhLikeRecyclerAdapter extends RecyclerView.Adapter<SearhLikeRecyclerAdapter.ViewHolder> {

    private final Activity mContext;
    private final List<SearchResultLike> mDatas;
    private final LayoutInflater mInflater;
    private final int windowsWight;

    public SearhLikeRecyclerAdapter(Activity context, List<SearchResultLike> data) {
        this.mContext = context;
        this.mDatas = data;
        mInflater = LayoutInflater.from(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public SearhLikeRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.item_search_tao_like_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearhLikeRecyclerAdapter.ViewHolder viewHolder, int i) {
        ViewGroup.LayoutParams layoutParams = viewHolder.mText.getLayoutParams();
        layoutParams.width = (windowsWight - Utils.dip2px(52)) / 3;
        viewHolder.mText.setLayoutParams(layoutParams);

        SearchResultLike searchResultLike = mDatas.get(i);
        viewHolder.mText.setText(searchResultLike.getName());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mText = itemView.findViewById(R.id.earch_tao_like_item_text);

            mText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchResultLike searchResultLike = mDatas.get(getLayoutPosition());
                        HashMap<String, String> event_params = searchResultLike.getEvent_params();
                        if(event_params != null){
                            YmStatistics.getInstance().tongjiApp(event_params);
                        }
                        onEventClickListener.onItemClick(searchResultLike);
                    }
                }
            });
        }
    }

    public interface OnEventClickListener {
        void onItemClick(SearchResultLike data);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
