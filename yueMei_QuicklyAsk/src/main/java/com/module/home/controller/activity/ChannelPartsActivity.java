package com.module.home.controller.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.fragment.ChannelBottomFragment;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.HashMap;

import butterknife.BindView;


public class ChannelPartsActivity extends YMBaseActivity {

    @BindView(R.id.channel_parts_titlebar_goback)
    LinearLayout mTitlebarGoback;
    @BindView(R.id.channel_parts_titlebar_title)
    TextView mTitlebarTitle;
    @BindView(R.id.channel_parts_titlebar_search)
    LinearLayout mTitlebarSearch;
    @BindView(R.id.channel_parts_titlebar_shopcar)
    RelativeLayout mTitlebarShopcar;
    @BindView(R.id.channel_parts_titlebar_num)
    TextView mTitlebarNum;
    @BindView(R.id.activity_channel_parts_titlebar)
    LinearLayout mTitlebar;
    public String id;
    public String homeSource;
    public ChannelBottomFragment channelBottomFragment;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_channel_parts;
    }

    @Override
    protected void initView() {
        id = getIntent().getStringExtra("id");
        String title = getIntent().getStringExtra("title");
        homeSource = getIntent().getStringExtra("home_source");

        mTitlebarTitle.setText(title);

        //设置高度
        Log.e(TAG, "statusbarHeight == " + statusbarHeight);
        ViewGroup.LayoutParams layoutParams = mTitlebar.getLayoutParams();
        layoutParams.height = Utils.dip2px(50) + statusbarHeight;
        mTitlebar.setLayoutParams(layoutParams);

        //返回点击
        mTitlebarGoback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //搜索点击
        mTitlebarSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_SEARCH),hashMap,new ActivityTypeData("96"));

                Intent it2 = new Intent(mContext, SearchAllActivity668.class);
                it2.putExtra(SearchAllActivity668.TYPE, "4");
                startActivity(it2);
            }
        });

        setCartNum();
        //购物车点击
        mTitlebarShopcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHOPPING_CART_CLICK),hashMap,new ActivityTypeData("96"));

                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });

    }

    @Override
    protected void initData() {
        //要传入的值
        ProjectDetailsBean detailsBean = new ProjectDetailsBean();
        detailsBean.setTwoLabelId(id);
        detailsBean.setFourLabelId("");
        detailsBean.setHomeSource(homeSource);

        channelBottomFragment = ChannelBottomFragment.newInstance(detailsBean);
        setActivityFragment(R.id.parts_channel_fragment, channelBottomFragment);
    }

    /**
     * 设置购物车数量
     */
    public void setCartNum() {
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            mTitlebarNum.setVisibility(View.VISIBLE);
            mTitlebarNum.setText(cartNumber);
        } else {
            mTitlebarNum.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        setCartNum();
    }
}
