package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.community.model.bean.HistorySearchWords;
import com.quicklyask.activity.R;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/4/17
 */
public class HistoryWordsAdapter668 extends RecyclerView.Adapter<HistoryWordsAdapter668.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<HistorySearchWords> mDatas;
    private final String TAG = "HistoryWordsAdapter668";

    public HistoryWordsAdapter668(Activity context, ArrayList<HistorySearchWords> hsdatas1) {
        this.mContext = context;
        this.mDatas = hsdatas1;
        Log.e(TAG, "mDatas ==  " + mDatas.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_history_text, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        HistorySearchWords historySearchWords = mDatas.get(position);

        viewHolder.mPart1NameTV.setText(historySearchWords.getHwords());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mPart1NameTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mPart1NameTV = itemView.findViewById(R.id.pop_history_item_name_tv);

            //item点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public ArrayList<HistorySearchWords> getDatas() {
        return mDatas;
    }

    //item点击回调
    public interface OnEventClickListener {
        void onItemClick(View v, int pos);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
