package com.module.home.controller.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.FunctionManager;
import com.module.community.model.bean.DoctorData;
import com.module.community.model.bean.HospitalData;
import com.module.community.model.bean.SearchAboutData;
import com.module.doctor.view.StaScoreBar;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 联想搜索页面
 * Created by 裴成浩 on 2019/5/21
 */
public class SearchKeywordsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Activity mContext;
    private List<SearchAboutData> mData;
    protected FunctionManager mFunctionManager;     //方法管理器
    private String TAG = "SearchKeywordsAdapter";
    public static final int COMMON = 0; //普通样式
    public static final int HOSPITAL = 1;//医院样式
    public static final int DOCTOR = 2;//医生样式
    private final LayoutInflater mInflater;

    public SearchKeywordsAdapter(Activity context, List<SearchAboutData> data) {
        this.mContext = context;
        this.mData = data;
        mInflater = LayoutInflater.from(mContext);
        //获取方法管理器
        mFunctionManager = new FunctionManager(mContext);
    }

    @Override
    public int getItemViewType(int position) {
        SearchAboutData searchAboutData = mData.get(position);
        if (searchAboutData != null){
            HospitalData hospitalData = searchAboutData.getHospital_data();
            DoctorData doctorsData = searchAboutData.getDoctors_data();
            if (hospitalData != null){
                return HOSPITAL;
            }else if (doctorsData != null){
                return DOCTOR;
            }else return COMMON;
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case HOSPITAL:
                return new HosViewHolder(mInflater.inflate(R.layout.item_search_keyword_hospital, parent, false));
            case DOCTOR:
                return new DocViewHolder(mInflater.inflate(R.layout.item_search_keyword_doctor, parent, false));
            case COMMON:
                return new ViewHolder(mInflater.inflate(R.layout.item_search_keywords_view, parent, false));
            default:
                return new ViewHolder(mInflater.inflate(R.layout.item_search_keywords_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int pos) {
        if (viewHolder instanceof HosViewHolder){
            setHosViewData((HosViewHolder)viewHolder,pos);
        }else if (viewHolder instanceof DocViewHolder){
            setDocViewData((DocViewHolder)viewHolder,pos);
        }else if (viewHolder instanceof ViewHolder){
            setCommonViewData((ViewHolder)viewHolder,pos);
        }
    }

    /**
     * 设普通数据
     * @param viewHolder
     * @param pos
     */
    private void setCommonViewData(ViewHolder viewHolder, int pos) {
        SearchAboutData data = mData.get(pos);
        String img = data.getImg();
        String dataNum = data.getNum();
        String keywords = data.getKeywords();
        String high_keywords = data.getHigh_keywords();

        Log.e(TAG, "dataNum == " + dataNum);
        Log.e(TAG, "keywords == " + keywords);
        Log.e(TAG, "high_keywords == " + high_keywords);

        viewHolder.mNumber.setText(data.getNum());

        if (!TextUtils.isEmpty(high_keywords) && keywords != null) {
            SpannableString s = new SpannableString(keywords);
            Pattern p = Pattern.compile(high_keywords);     //这里为关键字
            Matcher m = p.matcher(s);

            while (m.find()) {
                int start = m.start();
                int end = m.end();
                Log.e(TAG, "start == " + start);
                Log.e(TAG, "end == " + end);
                s.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            viewHolder.mName.setText(s);
        } else {
            viewHolder.mName.setText(keywords);
        }


        //设置文字图标
        if (!TextUtils.isEmpty(img)) {
            mFunctionManager.setImageSrc(viewHolder.mImg, img);
        } else {
            mFunctionManager.setImgSrc(viewHolder.mImg, R.drawable.item_search_keywords_image);
        }
    }

    /**
     * 设置医生数据
     * @param viewHolder
     * @param pos
     */
    private void setDocViewData(DocViewHolder viewHolder, int pos) {
        SearchAboutData data = mData.get(pos);
        if (data != null){
            String keywords = data.getKeywords();
            String high_keywords = data.getHigh_keywords();
            if (!TextUtils.isEmpty(high_keywords) && keywords != null) {
                SpannableString s = new SpannableString(keywords);
                Pattern p = Pattern.compile(high_keywords);     //这里为关键字
                Matcher m = p.matcher(s);

                while (m.find()) {
                    int start = m.start();
                    int end = m.end();
                    Log.e(TAG, "start == " + start);
                    Log.e(TAG, "end == " + end);
                    s.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                viewHolder.mDocName.setText(s);
            } else {
                viewHolder.mDocName.setText(keywords);
            }
            DoctorData doctorsData = data.getDoctors_data();
            if (doctorsData != null){
                String docImg = doctorsData.getDoc_img();
                String bookNum = doctorsData.getBook_num();
                String diaryNum = doctorsData.getDiary_num();
                String hospitalName = doctorsData.getHospital_name();
                Glide.with(mContext).load(docImg).transform(new GlideCircleTransform(mContext)).into(viewHolder.mDocIcon);
                if (!TextUtils.isEmpty(bookNum) || !"0".equals(bookNum)){
                    viewHolder.mDocOrding.setVisibility(View.VISIBLE);
                    viewHolder.mDocLine1.setVisibility(View.VISIBLE);
                    viewHolder.mDocOrding.setText(bookNum);
                }else {
                    viewHolder.mDocOrding.setVisibility(View.GONE);
                    viewHolder.mDocLine1.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(diaryNum)){
                    viewHolder.mDocCase.setVisibility(View.VISIBLE);
                    viewHolder.mDocLine2.setVisibility(View.VISIBLE);
                    viewHolder.mDocCase.setText(diaryNum);
                }else {
                    viewHolder.mDocCase.setVisibility(View.GONE);
                    viewHolder.mDocLine2.setVisibility(View.GONE);
                }
                viewHolder.mDocHosadress.setText(hospitalName);
            }
        }

    }

    /**
     * 设置医院数据
     * @param viewHolder
     * @param pos
     */
    private void setHosViewData(HosViewHolder viewHolder, int pos) {
        SearchAboutData data = mData.get(pos);
        if (data != null){
            HospitalData hospitalData = data.getHospital_data();
            if (hospitalData != null){
                String hosImg = hospitalData.getHos_img();
                String rateScale = hospitalData.getRateScale();
                String bookNum = hospitalData.getBook_num();
                String hosName = hospitalData.getHos_name();
                String diaryNum = hospitalData.getDiary_num();
                String distance = hospitalData.getDistance();
                String bigPromotionIcon = hospitalData.getBig_promotion_icon();
                String businessDistrict = hospitalData.getBusiness_district();
                String hospitalType = hospitalData.getHospital_type();

                String high_keywords = data.getHigh_keywords();
                if (!TextUtils.isEmpty(high_keywords) && hosName != null) {
                    SpannableString s = new SpannableString(hosName);
                    Pattern p = Pattern.compile(high_keywords);     //这里为关键字
                    Matcher m = p.matcher(s);

                    while (m.find()) {
                        int start = m.start();
                        int end = m.end();
                        Log.e(TAG, "start == " + start);
                        Log.e(TAG, "end == " + end);
                        s.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    viewHolder.mHosName.setText(s);
                } else {
                    viewHolder.mHosName.setText(hosName);
                }
                Glide.with(mContext).load(hosImg).transform(new GlideCircleTransform(mContext)).into(viewHolder.mHosIcon);
                if (!TextUtils.isEmpty(rateScale) && !"0".equals(rateScale)){
                    int ratescale = Integer.parseInt(rateScale);
                    viewHolder.mHosRatingBar.setProgressBar(ratescale);
                }
                if (!TextUtils.isEmpty(bookNum)){
                    viewHolder.mHosOdring.setVisibility(View.VISIBLE);
                    viewHolder.mHosOdring.setText(bookNum);
                }else {
                    viewHolder.mHosOdring.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(diaryNum)){
                    viewHolder.mHosCase.setVisibility(View.VISIBLE);
                    viewHolder.mHosCase.setText(diaryNum);
                }else {
                    viewHolder.mHosCase.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(hospitalType)){
                    viewHolder.mHosHos.setVisibility(View.VISIBLE);
                    viewHolder.mHosLine.setVisibility(View.VISIBLE);
                    viewHolder.mHosHos.setText(hospitalType);
                }else {
                    viewHolder.mHosHos.setVisibility(View.GONE);
                    viewHolder.mHosLine.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(businessDistrict)){
                    viewHolder.mHosAdress.setVisibility(View.VISIBLE);
                    viewHolder.mHosAdress.setText(businessDistrict);
                }else {
                    viewHolder.mHosAdress.setVisibility(View.GONE);
                }


                if (!TextUtils.isEmpty(distance)){
                    viewHolder.mHosDistance.setVisibility(View.VISIBLE);
                    viewHolder.mHosDistance.setText(distance);
                }else {
                    viewHolder.mHosDistance.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(bigPromotionIcon)){
                    viewHolder.mHosPromotion.setVisibility(View.VISIBLE);
                    mFunctionManager.setImageSrc(viewHolder.mHosPromotion, bigPromotionIcon);
                }else {
                    viewHolder.mHosPromotion.setVisibility(View.GONE);
                }

            }
        }
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * 替换数据
     *
     * @param data
     */
    public void replaceData(List<SearchAboutData> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImg;
        TextView mName;
        TextView mNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.item_search_keywords_img);
            mName = itemView.findViewById(R.id.item_search_keywords_name);
            mNumber = itemView.findViewById(R.id.item_search_keywords_number);

            //点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchAboutData data = mData.get(getLayoutPosition());
                        onEventClickListener.onItemViewClick(v, data.getKeywords(), data.getEvent_params());
                    }
                }
            });
        }
    }

    /**
     * 医院的viewholder
     */
    public class HosViewHolder extends RecyclerView.ViewHolder{

        private final ImageView mHosIcon;
        private final TextView mHosName;
        private final StaScoreBar mHosRatingBar;
        private final TextView mHosOdring;
        private final TextView mHosCase;
        private final TextView mHosHos;
        private final View mHosLine;
        private final TextView mHosAdress;
        private final TextView mHosDistance;
        private final ImageView mHosPromotion;

        public HosViewHolder(@NonNull View itemView) {
            super(itemView);
            mHosIcon = itemView.findViewById(R.id.search_keyword_hos_icon);
            mHosName = itemView.findViewById(R.id.search_keyword_hos_name);
            mHosRatingBar = itemView.findViewById(R.id.search_keyword_hos_ratingbar);
            mHosOdring = itemView.findViewById(R.id.search_keyword_hos_ording);
            mHosCase = itemView.findViewById(R.id.search_keyword_hos_case);
            mHosHos = itemView.findViewById(R.id.search_keywords_hospital_hos);
            mHosLine = itemView.findViewById(R.id.center_line);
            mHosAdress = itemView.findViewById(R.id.search_keyword_hos_adress);
            mHosDistance = itemView.findViewById(R.id.search_keyword_hos_distance);
            mHosPromotion = itemView.findViewById(R.id.search_keyword_hos_promotion);
            //点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchAboutData data = mData.get(getLayoutPosition());
                        onEventClickListener.onItemViewClick(v, data.getKeywords(), data.getEvent_params());
                    }
                }
            });
        }
    }

    /**
     * 医生的viewholder
     */
    public class DocViewHolder extends RecyclerView.ViewHolder{

        private final ImageView mDocIcon;
        private final TextView mDocName;
        private final TextView mDocOrding;
        private final View mDocLine1;
        private final View mDocLine2;
        private final TextView mDocCase;
        private final TextView mDocHosadress;
        public DocViewHolder(@NonNull View itemView) {
            super(itemView);
            mDocIcon = itemView.findViewById(R.id.search_keyword_doctor_icon);
            mDocName = itemView.findViewById(R.id.search_keyword_doctor_name);
            mDocOrding = itemView.findViewById(R.id.search_keyword_doctor_ording);
            mDocLine1 = itemView.findViewById(R.id.search_keyword_doctor_line1);
            mDocLine2 = itemView.findViewById(R.id.search_keyword_doctor_line2);
            mDocCase = itemView.findViewById(R.id.search_keyword_doctor_case);
            mDocHosadress = itemView.findViewById(R.id.search_keyword_doctor_hosadress);
            //点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchAboutData data = mData.get(getLayoutPosition());
                        onEventClickListener.onItemViewClick(v, data.getKeywords(), data.getEvent_params());
                    }
                }
            });
        }
    }

    //item点击回调
    public interface OnEventClickListener {

        void onItemViewClick(View v, String keys, HashMap<String, String> event_params);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
