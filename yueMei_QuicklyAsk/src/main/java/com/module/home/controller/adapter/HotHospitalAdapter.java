package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.SearchHotHospitalData;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/9/9
 */
public class HotHospitalAdapter extends RecyclerView.Adapter<HotHospitalAdapter.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<SearchHotHospitalData> mDatas;
    private final LayoutInflater mInflater;
    private String TAG = "HotHospitalAdapter";

    public HotHospitalAdapter(Activity context, ArrayList<SearchHotHospitalData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public HotHospitalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new HotHospitalAdapter.ViewHolder(mInflater.inflate(R.layout.item_hot_hospital_view, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HotHospitalAdapter.ViewHolder holder, int pos) {
        SearchHotHospitalData data = mDatas.get(pos);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.mItemView.getLayoutParams();
        if (pos == 0) {
            layoutParams.leftMargin = Utils.dip2px(14);
            layoutParams.rightMargin = Utils.dip2px(4);
        } else if (pos == mDatas.size() - 1) {
            layoutParams.leftMargin = Utils.dip2px(4);
            layoutParams.rightMargin = Utils.dip2px(14);
        } else {
            layoutParams.leftMargin = Utils.dip2px(4);
            layoutParams.rightMargin = Utils.dip2px(4);
        }
        holder.mItemView.setLayoutParams(layoutParams);

        //医院图
        Glide.with(mContext).load(data.getHospital_banner())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.mImage);

        //医院名称
        holder.mName.setText(data.getHospital_name());

        //排名
        holder.mNumber.setText("NO." + (pos + 1));
        switch (pos) {
            case 0:
                holder.mNumber.setBackgroundResource(R.drawable.shape_item_hot_hos_no1);
                break;
            case 1:
                holder.mNumber.setBackgroundResource(R.drawable.shape_item_hot_hos_no2);
                break;
            case 2:
                holder.mNumber.setBackgroundResource(R.drawable.shape_item_hot_hos_no3);
                break;
            default:
                holder.mNumber.setBackgroundResource(R.drawable.shape_item_hot_hos_non);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView mItemView;
        private ImageView mImage;
        private TextView mNumber;
        private TextView mName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mItemView = itemView.findViewById(R.id.item_hot_hospital_view);
            mImage = itemView.findViewById(R.id.hot_hospital_image);
            mNumber = itemView.findViewById(R.id.hot_hospital_number);
            mName = itemView.findViewById(R.id.hot_hospital_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SearchHotHospitalData data = mDatas.get(getLayoutPosition());
                    String hospital_url = data.getHospital_url();
                    if (!TextUtils.isEmpty(hospital_url)) {
                        HashMap<String, String> event_params = data.getEvent_params();
                        Log.e(TAG, "event_params人气医院item点击 == " + event_params);
                        YmStatistics.getInstance().tongjiApp(event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(hospital_url);
                    }
                }
            });
        }

    }
}
