package com.module.home.controller.adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.ChatListBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/12/19.
 */
public class XiaoxiAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private String TAG = "XiaoxiAdapter";
    private ChatListBean mChatListBean;
    private Context mContext;
    List<ChatListBean.ListBean> mChatList;

    public XiaoxiAdapter(Context context, ChatListBean chatListBeans) {
        this.mContext = context;
        this.mChatListBean = chatListBeans;
        mChatList= mChatListBean.getList();
        Log.e(TAG, "mChatList长度 === " + mChatList.size());
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mChatList.size();
    }

    @Override
    public Object getItem(int position) {
        return mChatList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_huanxin_kefu, null);

            viewHolder = new ViewHolder();
            viewHolder.ivInform = convertView.findViewById(R.id.item_inform_iv);
            viewHolder.tvInform = convertView.findViewById(R.id.item_inform_tv_);

            viewHolder.kefuTitle = convertView.findViewById(R.id.kefu_title);
            viewHolder.kefuLastmessage = convertView.findViewById(R.id.kefu_lastmessage);
            viewHolder.kefuTime = convertView.findViewById(R.id.kefu_time);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ChatListBean.ListBean listBean = mChatList.get(position);
        String id = listBean.getId();           //聊天对象Id
        String message = listBean.getMessage(); //内容
        String timeSet = listBean.getTimeSet(); //时间显示
        String noread = listBean.getNoread();   //未读消息数

        String dateTime = listBean.getDateTime();   //加载下一页要传的
        String fromName = listBean.getFromName();   // 聊天对象是医院用的
        String hos_id = listBean.getHos_id();       //医院客服id
        String hos_name = listBean.getHos_name();   // 聊天对象是用户用的
        String group_id = listBean.getGroup_id();   //客服组id
        String userImg = listBean.getUserImg(); //用户头像

        //头像
        Glide.with(mContext).load(userImg).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.ease_default_avatar).error(R.drawable.ease_default_avatar).into(viewHolder.ivInform);
        //设置未读消息数
        if (Integer.parseInt(noread) > 0) {
            viewHolder.tvInform.setVisibility(View.VISIBLE);
            viewHolder.tvInform.setText(noread);
        } else {
            viewHolder.tvInform.setVisibility(View.GONE);
        }

        viewHolder.kefuTitle.setText(hos_name);

        //消息内容
        viewHolder.kefuLastmessage.setText(message);

        //消息时间
        viewHolder.kefuTime.setText(timeSet);

        return convertView;
    }

    class ViewHolder {

        public ImageView ivInform;    //头像

        public TextView tvInform;           //消息数
        public TextView kefuTime;           //消息时间

        public TextView kefuTitle;           //医院名
        public TextView kefuLastmessage;     //消息内容
    }

    /**
     * 设置最新数据
     *
     * @param pos
     * @param message
     */
    public void setNewContent(int pos, String message) {
        try {
            mChatList.get(pos).setMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置最新数据时间
     *
     * @param pos
     * @param time
     */
    public void setNewTime(int pos, String time) {
        mChatList.get(pos).setTimeSet(time);
    }

    /**
     * 设置最新的消息数
     *
     * @param pos
     * @param noread
     */
    public void setNoread(int pos, String noread) {
        mChatList.get(pos).setNoread(noread);
    }

    public void clearNoread(){
        for (int i = 0; i <mChatList.size() ; i++) {
            mChatList.get(i).setNoread("0");
        }
    }


    public List<ChatListBean.ListBean> getmChatList() {
        return mChatList;
    }

    public void setmChatList(List<ChatListBean.ListBean> mChatList) {
        this.mChatList.addAll(mChatList);
    }
}
