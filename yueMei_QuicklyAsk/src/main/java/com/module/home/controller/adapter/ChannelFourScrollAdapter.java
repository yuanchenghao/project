package com.module.home.controller.adapter;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.module.base.view.YMRecyclerViewAdapter;
import com.module.base.view.YMViewHolder;
import com.module.home.model.bean.LabelEncyclopediaBtnList;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/7
 */
public class ChannelFourScrollAdapter extends YMRecyclerViewAdapter<LabelEncyclopediaBtnList, YMViewHolder> {

    public ChannelFourScrollAdapter(@Nullable List<LabelEncyclopediaBtnList> data) {
        super(data);
    }

    @Override
    protected int findResById() {
        return R.layout.item_channel_four_scroll_view;
    }

    @Override
    protected void convert(YMViewHolder helper, LabelEncyclopediaBtnList item) {
        LinearLayout scrollView = helper.getView(R.id.item_channel_four_scroll_view);
        ViewGroup.LayoutParams layoutParams = scrollView.getLayoutParams();
        layoutParams.width = (mFunctionManager.getWindowWidth() - Utils.dip2px(15)) / 4;

        helper.setText(R.id.item_channel_four_scroll_tv, item.getBtn_title());
        mFunctionManager.setImageSrc((ImageView) helper.getView(R.id.item_channel_four_scroll_iv), item.getBtn_icon());
    }

}
