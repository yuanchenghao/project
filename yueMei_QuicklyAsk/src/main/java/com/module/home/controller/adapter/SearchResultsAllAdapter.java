package com.module.home.controller.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.RecommGridViewAdapter;
import com.module.commonview.module.bean.DiaryOtherPostBean;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.bean.HosListData;
import com.module.home.model.bean.SearchAllBaikeData;
import com.module.home.model.bean.SearchCompositeData;
import com.module.home.model.bean.SearchCompositeLinkdata;
import com.module.home.view.SearchInitFlowLayout;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


/**
 * 搜索综合页适配器
 * Created by 裴成浩 on 2019/7/25
 */
public class SearchResultsAllAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "SearchResultsAllAdapter";
    private final Activity mContext;
    private final int ITEM_TYPE_TAG = 1;
    private final int ITEM_TYPE_TAO = 2;
    private final int ITEM_TYPE_BAIKE = 3;
    private final int ITEM_TYPE_DIARY = 4;
    private final int ITEM_TYPE_HOSPITAL = 5;
    private final int ITEM_TYPE_PLACEHOLDER = 6;

    private final LayoutInflater mInflater;

    private List<SearchCompositeData> mDatas;

    public SearchResultsAllAdapter(Activity context, List<SearchCompositeData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDatas.get(position).getShow_type()) {
            case "tag":
                return ITEM_TYPE_TAG;
            case "taoList":
                return ITEM_TYPE_TAO;
            case "baikeList":
                return ITEM_TYPE_BAIKE;
            case "diaryList":
                return ITEM_TYPE_DIARY;
            case "hospitalList":
                return ITEM_TYPE_HOSPITAL;
            default:
                return ITEM_TYPE_PLACEHOLDER;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_TAG:
                return new TagViewHolder(mInflater.inflate(R.layout.search_results_all_tag_header_view, parent, false));
            case ITEM_TYPE_TAO:
                return new TaoViewHolder(mInflater.inflate(R.layout.search_results_all_item_view, parent, false));
            case ITEM_TYPE_BAIKE:
                return new BaikeViewHolder(mInflater.inflate(R.layout.search_results_all_item_view, parent, false));
            case ITEM_TYPE_DIARY:
                return new DiaryViewHolder(mInflater.inflate(R.layout.search_results_all_item_view, parent, false));
            case ITEM_TYPE_HOSPITAL:
                return new HospitalViewHolder(mInflater.inflate(R.layout.search_results_all_item_view, parent, false));
            default:
                return new NullViewHolder(mInflater.inflate(R.layout.recyclerview_placeholder_view, parent, false));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TagViewHolder) {

            setTagView((TagViewHolder) holder, position);

        } else if (holder instanceof TaoViewHolder) {

            setTaoView((TaoViewHolder) holder, position);

        } else if (holder instanceof BaikeViewHolder) {

            setBaikeView((BaikeViewHolder) holder, position);

        } else if (holder instanceof DiaryViewHolder) {

            setDiaryView((DiaryViewHolder) holder, position);

        } else if (holder instanceof HospitalViewHolder) {

            setHospitalView((HospitalViewHolder) holder, position);

        }
    }

    /**
     * tag标签设置
     *
     * @param holder
     * @param position
     */
    private void setTagView(TagViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);

        Log.e(TAG, "data.getData().toJSONString() == " + data.getData().toJSONString());
        List<String> hsdatas1 = JSONObject.parseArray(data.getData().toJSONString(), String.class);

        holder.tagContent.setMaxLine(2);
        SearchInitFlowLayout mSearchInitFlowLayout = new SearchInitFlowLayout(mContext, holder.tagContent, hsdatas1);

        //未查找到推荐标签点击
        mSearchInitFlowLayout.setClickCallBack(new SearchInitFlowLayout.ClickCallBack() {
            @Override
            public void onClick(View v, int pos, String key) {
                if (onEventClickListener != null) {
                    onEventClickListener.onSearchKeyClick(key);
                }
            }
        });
    }

    /**
     * 淘数据设置
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setTaoView(TaoViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);

        //标题设置
        setTitleView(holder.mTitleClick, holder.mTitle, holder.mMore, holder.mLine, data, position);
        //列表设置
        ScrollLayoutManager scrollLinearLayoutManager1 = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager1.setScrollEnable(false);

        //设置分割线
        holder.mList.setLayoutManager(scrollLinearLayoutManager1);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.mList.getItemAnimator())).setSupportsChangeAnimations(false);

        //加载淘列表
        ArrayList<HomeTaoData> homeTaoDatas = JSONUtil.jsonToArrayList(data.getData().toJSONString(), HomeTaoData.class);
        TaoListAdapter taoListAdapter = new TaoListAdapter(mContext, homeTaoDatas,true);
        holder.mList.setAdapter(taoListAdapter);

        //点击事件
        taoListAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TaoListDataType taoData, int pos) {
                HomeTaoData  data = taoData.getTao();
                YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                String id = data.get_id();
                Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                it1.putExtra("id", id);
                it1.putExtra("source", "107");
                it1.putExtra("objid", "0");
                mContext.startActivity(it1);
            }

        });
    }


    /**
     * 百科数据设置
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setBaikeView(BaikeViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);

        //标题设置
        setTitleView(holder.mTitleClick, holder.mTitle, holder.mMore, holder.mLine, data, position);

        //列表设置
        ScrollLayoutManager scrollLinearLayoutManager1 = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager1.setScrollEnable(false);
        holder.mList.setLayoutManager(scrollLinearLayoutManager1);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.mList.getItemAnimator())).setSupportsChangeAnimations(false);

        //加载百科数据
        ArrayList<SearchAllBaikeData> searchAllBaiKeData = JSONUtil.jsonToArrayList(data.getData().toJSONString(), SearchAllBaikeData.class);
        SearchResultsAllBaikeAdapter allBaikeAdapter = new SearchResultsAllBaikeAdapter(mContext, searchAllBaiKeData);
        holder.mList.setAdapter(allBaikeAdapter);
    }

    /**
     * 日记数据设置
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setDiaryView(DiaryViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);

        //标题设置
        setTitleView(holder.mTitleClick, holder.mTitle, holder.mMore, holder.mLine, data, position);

        //列表设置
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 2);
        gridLayoutManager.setScrollEnable(false);
        holder.mList.setLayoutManager(gridLayoutManager);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.mList.getItemAnimator())).setSupportsChangeAnimations(false);

        //加载日记数据
        ArrayList<DiaryOtherPostBean> diaryOtherPostBeans = JSONUtil.jsonToArrayList(data.getData().toJSONString(), DiaryOtherPostBean.class);
        RecommGridViewAdapter recommGridViewAdapter = new RecommGridViewAdapter(mContext, diaryOtherPostBeans, Utils.dip2px(15), Utils.dip2px(15), Utils.dip2px(15));
        holder.mList.setAdapter(recommGridViewAdapter);

        //点击事件
        recommGridViewAdapter.setOnItemCallBackListener(new RecommGridViewAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, DiaryOtherPostBean data, int pos) {
                YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUrl());
            }
        });
    }

    /**
     * 医院数据设置
     *
     * @param holder
     * @param position
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setHospitalView(HospitalViewHolder holder, int position) {
        SearchCompositeData data = mDatas.get(position);

        //标题设置
        setTitleView(holder.mTitleClick, holder.mTitle, holder.mMore, holder.mLine, data, position);

        //列表设置
        ScrollLayoutManager scrollLinearLayoutManager1 = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager1.setScrollEnable(false);
        holder.mList.setLayoutManager(scrollLinearLayoutManager1);
        ((DefaultItemAnimator) Objects.requireNonNull(holder.mList.getItemAnimator())).setSupportsChangeAnimations(false);

        //医院数据
        ArrayList<HosListData> hosListDatas = JSONUtil.jsonToArrayList(data.getData().toJSONString(), HosListData.class);
        ProjectHosAdapter projectHosAdapter = new ProjectHosAdapter(mContext, hosListDatas);
        holder.mList.setAdapter(projectHosAdapter);

        //淘整形列表
        projectHosAdapter.setOnEventClickListener(new ProjectHosAdapter.OnEventClickListener() {
            @Override
            public void onItemClick(View v, HosListData data, int pos) {
                YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                Intent it = new Intent(mContext, HosDetailActivity.class);
                it.putExtra("hosid", data.getHos_id());
                mContext.startActivity(it);
            }
        });
    }

    /**
     * 共有数据设置
     *
     * @param titleView
     * @param moreView
     * @param data
     */
    private void setTitleView(LinearLayout mTitleClick, TextView titleView, TextView moreView, View line, SearchCompositeData data, int pos) {
        String title = data.getTitle();
        SearchCompositeLinkdata linkdata = data.getLinkdata();

        //标题设置
        if (!TextUtils.isEmpty(title)) {
            if (pos != 0) {
                line.setVisibility(View.VISIBLE);
            } else {
                line.setVisibility(View.GONE);
            }
            mTitleClick.setVisibility(View.VISIBLE);
            titleView.setText(title);

            //加载更多设置
            if (linkdata != null && !TextUtils.isEmpty(linkdata.getUrltitle())) {
                moreView.setVisibility(View.VISIBLE);

                Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.results_all_card_right);
                drawable.setBounds(0, 0, Utils.dip2px(10), Utils.dip2px(10));
                moreView.setCompoundDrawables(null, null, drawable, null);
                moreView.setCompoundDrawablePadding(Utils.dip2px(2));

                moreView.setText(linkdata.getUrltitle());
            } else {
                moreView.setVisibility(View.GONE);
            }
        } else {
            line.setVisibility(View.GONE);
            mTitleClick.setVisibility(View.GONE);
        }


    }

    /**
     * 加载更多
     *
     * @param data
     */
    public void addData(List<SearchCompositeData> data) {
        int size = mDatas.size();
        mDatas.addAll(data);
        notifyItemRangeInserted(size, mDatas.size() - size);
    }


    //tag标签ViewHolder
    public class TagViewHolder extends RecyclerView.ViewHolder {

        private FlowLayout tagContent;

        TagViewHolder(View itemView) {
            super(itemView);
            tagContent = itemView.findViewById(R.id.results_all_tag_content);
        }
    }

    //淘数据ViewHolder
    public class TaoViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mTitleClick;
        private TextView mTitle;
        private TextView mMore;
        private RecyclerView mList;
        private View mLine;

        TaoViewHolder(View itemView) {
            super(itemView);
            mTitleClick = itemView.findViewById(R.id.results_all_item_title_click);
            mTitle = itemView.findViewById(R.id.results_all_item_title);
            mMore = itemView.findViewById(R.id.results_all_item_more);
            mList = itemView.findViewById(R.id.results_all_item_list);
            mLine = itemView.findViewById(R.id.results_all_item_line);

            //点击查看更多
            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchCompositeData searchCompositeData = mDatas.get(getLayoutPosition());
                        SearchCompositeLinkdata linkdata = searchCompositeData.getLinkdata();
                        String url = linkdata.getUrl();
                        if (!TextUtils.isEmpty(url)) {
                            onEventClickListener.onSearchMoreClick(url, linkdata.getEvent_params());
                        }
                    }
                }
            });

        }
    }

    //百科数据ViewHolder
    public class BaikeViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTitleClick;
        private View mLine;
        private TextView mTitle;
        private TextView mMore;
        private RecyclerView mList;

        BaikeViewHolder(View itemView) {
            super(itemView);
            mTitleClick = itemView.findViewById(R.id.results_all_item_title_click);
            mLine = itemView.findViewById(R.id.results_all_item_line);
            mTitle = itemView.findViewById(R.id.results_all_item_title);
            mMore = itemView.findViewById(R.id.results_all_item_more);
            mList = itemView.findViewById(R.id.results_all_item_list);

            //点击查看更多
            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchCompositeData searchCompositeData = mDatas.get(getLayoutPosition());
                        SearchCompositeLinkdata linkdata = searchCompositeData.getLinkdata();
                        String url = linkdata.getUrl();
                        if (!TextUtils.isEmpty(url)) {
                            onEventClickListener.onSearchMoreClick(url, linkdata.getEvent_params());
                        }
                    }
                }
            });
        }
    }

    //日记数据ViewHolder
    public class DiaryViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTitleClick;
        private View mLine;
        private TextView mTitle;
        private TextView mMore;
        private RecyclerView mList;

        DiaryViewHolder(View itemView) {
            super(itemView);
            mTitleClick = itemView.findViewById(R.id.results_all_item_title_click);
            mLine = itemView.findViewById(R.id.results_all_item_line);
            mTitle = itemView.findViewById(R.id.results_all_item_title);
            mMore = itemView.findViewById(R.id.results_all_item_more);
            mList = itemView.findViewById(R.id.results_all_item_list);

            //点击查看更多
            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchCompositeData searchCompositeData = mDatas.get(getLayoutPosition());
                        SearchCompositeLinkdata linkdata = searchCompositeData.getLinkdata();
                        String url = linkdata.getUrl();
                        if (!TextUtils.isEmpty(url)) {
                            onEventClickListener.onSearchMoreClick(url, linkdata.getEvent_params());
                        }
                    }
                }
            });
        }
    }

    //医院数据ViewHolder
    public class HospitalViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTitleClick;
        private View mLine;
        private TextView mTitle;
        private TextView mMore;
        private RecyclerView mList;

        HospitalViewHolder(View itemView) {
            super(itemView);
            mTitleClick = itemView.findViewById(R.id.results_all_item_title_click);
            mLine = itemView.findViewById(R.id.results_all_item_line);
            mTitle = itemView.findViewById(R.id.results_all_item_title);
            mMore = itemView.findViewById(R.id.results_all_item_more);
            mList = itemView.findViewById(R.id.results_all_item_list);

            //点击查看更多
            mMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        SearchCompositeData searchCompositeData = mDatas.get(getLayoutPosition());
                        SearchCompositeLinkdata linkdata = searchCompositeData.getLinkdata();
                        String url = linkdata.getUrl();
                        if (!TextUtils.isEmpty(url)) {
                            onEventClickListener.onSearchMoreClick(url, linkdata.getEvent_params());
                        }
                    }
                }
            });

        }
    }

    //占位数据ViewHolder
    public class NullViewHolder extends RecyclerView.ViewHolder {
        NullViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnEventClickListener {
        void onSearchKeyClick(String key);

        void onSearchMoreClick(String type, HashMap<String, String> event_params);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
