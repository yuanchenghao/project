package com.module.home.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.other.module.bean.MakeTagListData;
import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import org.xutils.common.util.DensityUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/20
 */
public class AllProjectRightAdapter extends RecyclerView.Adapter<AllProjectRightAdapter.ViewHolder> {
    private final String TAG = "AllProjectRightAdapter";
    private String x_id;
    private int rightListWight = 0;

    private Context mContext;
    private List<MakeTagListData> mData;
    private LayoutInflater inflater;

    public AllProjectRightAdapter(Context mContext, List<MakeTagListData> mData, String id) {
        this.mContext = mContext;
        this.mData = mData;
        this.x_id = id;
        Log.e(TAG, "mData ==  " + mData.size());
        inflater = LayoutInflater.from(mContext);

        if (mContext instanceof Activity) {
            DisplayMetrics metric = new DisplayMetrics();
            ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metric);
            rightListWight = metric.widthPixels - Utils.dip2px(117);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = inflater.inflate(R.layout.item_all_project_right, parent, false);
        return new ViewHolder(itemView);        //把这个布局传到ViewHolder中
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int pos) {
        //设置Ui
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.projectRight.getLayoutParams();
        Log.e(TAG, "layoutParams === " + layoutParams);
        if (pos == 0) {
            layoutParams.topMargin = Utils.dip2px(5);
            layoutParams.bottomMargin = Utils.dip2px(7.5f);
        } else if (pos == mData.size() - 1) {
            layoutParams.topMargin = Utils.dip2px(7.5f);
            layoutParams.bottomMargin = Utils.dip2px(15);
        } else {
            layoutParams.topMargin = Utils.dip2px(7.5f);
            layoutParams.bottomMargin = Utils.dip2px(7.5f);
        }
        viewHolder.projectRight.setLayoutParams(layoutParams);

        final MakeTagListData beanX = mData.get(pos);

        //设置上边的数据
        String img = beanX.getImg();
        if (!TextUtils.isEmpty(img)) {
            viewHolder.tvRightImage.setVisibility(View.VISIBLE);
            viewHolder.tvRightTitle.setVisibility(View.GONE);

            Glide.with(mContext).load(img).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();

                    int imageHeight = (rightListWight * intrinsicHeight) / intrinsicWidth;
                    ViewGroup.LayoutParams layoutParams1 = viewHolder.tvRightImage.getLayoutParams();
                    layoutParams1.width = rightListWight;
                    layoutParams1.height = imageHeight;

                    viewHolder.tvRightImage.setImageDrawable(resource);
                }
            });
        } else {
            viewHolder.tvRightImage.setVisibility(View.GONE);
            viewHolder.tvRightTitle.setVisibility(View.VISIBLE);
            viewHolder.tvRightTitle.setText(beanX.getName());
        }

        setRightView(viewHolder.flowlayout, beanX, pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * 设置新数据
     *
     * @param datas
     */
    public void setData(List<MakeTagListData> datas, String id) {
        this.x_id = id;
        this.mData = datas;
        notifyDataSetChanged();
    }

    /**
     * 设置右边下边的标签
     *
     * @param flowlayout
     * @param data
     */
    private void setRightView(FlowLayout flowlayout, MakeTagListData data, int pos) {
        List<MakeTagListListData> list = data.getList();

        flowlayout.removeAllViews();

        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = DensityUtil.dip2px(5);
        lp.rightMargin = DensityUtil.dip2px(5);
        lp.topMargin = DensityUtil.dip2px(5);
        lp.bottomMargin = DensityUtil.dip2px(5);

        for (int i = 0; i < list.size(); i++) {
            MakeTagListListData listBean = list.get(i);

            TextView textView = new TextView(mContext);
            textView.setTextColor(Utils.getLocalColor(mContext, R.color.zz_gray_22));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            textView.setText(listBean.getName());
            textView.setBackgroundResource(R.drawable.shape_corners_f5);

            flowlayout.addView(textView, lp);

            textView.setTag(i);
            textView.setOnClickListener(new onRightView(pos, data.getId()));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout projectRight;
        private FrameLayout tvRightClick;
        private TextView tvRightTitle;
        private ImageView tvRightImage;
        private FlowLayout flowlayout;

        public ViewHolder(View itemView) {
            super(itemView);
            projectRight = itemView.findViewById(R.id.item_all_project_right);
            tvRightClick = itemView.findViewById(R.id.project_right_click);
            tvRightTitle = itemView.findViewById(R.id.project_right_title);
            tvRightImage = itemView.findViewById(R.id.project_right_image);
            flowlayout = itemView.findViewById(R.id.project_right_flowlayout);

            tvRightTitle.getPaint().setFakeBoldText(true);

            //点击事件
            tvRightClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MakeTagListData makeTagListData = mData.get(getLayoutPosition());
                    String id = makeTagListData.getId();
                    String name = makeTagListData.getName();

                    Log.e(TAG, "id == " + id);
                    if (mItemClickListener != null) {
                        if (tvRightImage.getVisibility() == View.GONE) {
                            mItemClickListener.onItemClick(makeTagListData.getLevel(), id, name, x_id, makeTagListData.getEvent_params());
                        } else {
                            mItemClickListener.onItemClick(makeTagListData.getLevel(), id, name, x_id, makeTagListData.getEvent_params());
                        }
                    }


                }
            });
        }
    }

    class onRightView implements View.OnClickListener {

        private int mPos;
        private final String mTwoId;

        public onRightView(int pos, String two_id) {
            this.mPos = pos;
            this.mTwoId = two_id;
        }

        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            List<MakeTagListListData> list = mData.get(mPos).getList();
            for (int i = 0; i < list.size(); i++) {
                if (i == selected) {
                    MakeTagListListData makeTagListListData = list.get(i);
                    String id = makeTagListListData.getId();
                    String name = makeTagListListData.getName();
                    Log.e(TAG, "id >>> " + id);
                    Log.e(TAG, "name >>> " + name);

                    Utils.tongjiApp(mContext, "recommend_part", (selected + 1) + "", id, "95");

                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(makeTagListListData.getLevel(), id, name, x_id, makeTagListListData.getEvent_params());
                    }
                }
            }
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    private ItemClickListener mItemClickListener;

    public interface ItemClickListener {
        void onItemClick(String level, String id, String name, String selected_id, HashMap<String, String> params);
    }

}
