package com.module.home.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.module.home.model.bean.BaikeSearchItemData;

import java.util.ArrayList;
import java.util.List;

public class SearchBaikeAdapter extends BaseAdapter {

	private final String TAG = "SearchBaikeAdapter";

	private List<BaikeSearchItemData> mBaikeSearchItemData = new ArrayList<BaikeSearchItemData>();
	private Context mContext;
	private LayoutInflater inflater;
	private BaikeSearchItemData BaikeSearchItemData;
	ViewHolder viewHolder;

	public SearchBaikeAdapter(Context mContext,
			List<BaikeSearchItemData> mBaikeSearchItemData) {
		this.mContext = mContext;
		this.mBaikeSearchItemData = mBaikeSearchItemData;
		inflater = LayoutInflater.from(mContext);

		// Log.e(TAG, "BaikeSearchItemData==" + mBaikeSearchItemData);
	}

	static class ViewHolder {
		public TextView mPart1NameTV;
		public RelativeLayout mRly;
	}

	@Override
	public int getCount() {
		return mBaikeSearchItemData.size();
	}

	@Override
	public Object getItem(int position) {
		return mBaikeSearchItemData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_tao_pop, null);
			viewHolder = new ViewHolder();

			viewHolder.mPart1NameTV = convertView
					.findViewById(R.id.pop_tao_item_name_tv);
			viewHolder.mRly = convertView
					.findViewById(R.id.top_city_rly);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		BaikeSearchItemData = mBaikeSearchItemData.get(position);
		// String cityid = BaikeSearchItemData.get_id();

		viewHolder.mPart1NameTV.setText(BaikeSearchItemData.getName());

		// viewHolder.mPart1NameTV.setTextSize(16);
		// viewHolder.mPart1NameTV.setTextColor(mContext.getResources().getColor(
		// R.color.gary_person));
		// viewHolder.mRly.setBackgroundResource(R.color.login_co);

		return convertView;
	}

	public void add(List<BaikeSearchItemData> infos) {
		mBaikeSearchItemData.addAll(infos);
	}
}
