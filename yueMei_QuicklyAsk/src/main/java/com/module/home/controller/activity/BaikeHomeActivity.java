/**
 * 
 */
package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 百科首页
 *
 * @author Robin
 *
 */
public class BaikeHomeActivity extends BaseActivity {

	private final String TAG = "BaikeHomeActivity";

	@BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.baike_tow_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private BaikeHomeActivity mContex;
	public JSONObject obj_http;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_zhifu_help1);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = BaikeHomeActivity.this;

		mTop.setCenterText("悦美百科");

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex,"11");

		initWebview();

		LodUrl1(FinalConstant.BAIKE_HOME);

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						BaikeHomeActivity.this.finish();
					}
				});
		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}



	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.setHorizontalScrollBarEnabled(false);//水平不显示
		docDetWeb.setVerticalScrollBarEnabled(false); //垂直不显示
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		contentWeb.addView(docDetWeb);
	}


	/**
	 * 加载web
	 */
	public void LodUrl1(String url) {
		baseWebViewClientMessage.startLoading();

		WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
		if (null != docDetWeb) {
			docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
		}

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
