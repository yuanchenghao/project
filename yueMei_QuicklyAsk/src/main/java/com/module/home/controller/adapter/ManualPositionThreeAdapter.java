package com.module.home.controller.adapter;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.home.model.bean.ManualPositionBtnBoard;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/7
 */
public class ManualPositionThreeAdapter extends RecyclerView.Adapter<ManualPositionThreeAdapter.ViewHolder> {

    private final Activity mContext;
    private final LayoutInflater mInflater;
    private final List<ManualPositionBtnBoard> boardList;
    private final FunctionManager mFunctionManager;

    public ManualPositionThreeAdapter(Activity context, List<ManualPositionBtnBoard> boardList) {
        this.mContext = context;
        this.boardList = boardList;
        mInflater = LayoutInflater.from(context);
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public ManualPositionThreeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.project_list_itme_text3, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ManualPositionThreeAdapter.ViewHolder viewHolder, int i) {
        ManualPositionBtnBoard data = boardList.get(i);
        List<String> back_color = data.getBack_color();

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.item_view.getLayoutParams();
        layoutParams.width = (Utils.getScreenSize(mContext)[0] - Utils.dip2px(50)) / 4;

        viewHolder.itemTxt.setText(data.getTitle());

        setGradientBackground(viewHolder.itemImage, back_color, data.getBack_img());
    }

    @Override
    public int getItemCount() {
        return boardList.size();
    }


    /**
     * 设置渐变背景
     *
     * @param imageView       :要设置渐变色组件
     * @param backColor：渐变色颜色
     * @param backImage：背景图
     */
    private void setGradientBackground(ImageView imageView, List<String> backColor, String backImage) {
        if (!TextUtils.isEmpty(backImage)) {
            mFunctionManager.setPlaceholderImageSrc(imageView, backImage);
        } else {
            try {
                if (backColor.size() >= 2) {
                    String colorLeft = backColor.get(0);
                    String colorRight = backColor.get(1);
                    if (colorLeft.startsWith("#") && colorRight.startsWith("#")) {
                        GradientDrawable aDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Utils.setCustomColor(colorLeft), Utils.setCustomColor(colorRight)});
                        imageView.setBackground(aDrawable);
                    } else {
                        if (colorLeft.startsWith("#")) {
                            imageView.setBackgroundColor(Utils.setCustomColor(colorLeft));
                        } else if (colorRight.startsWith("#")) {
                            imageView.setBackgroundColor(Utils.setCustomColor(colorRight));
                        } else {
                            imageView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_ball));
                        }
                    }
                } else {
                    imageView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_ball));
                }
            } catch (NumberFormatException e) {
                imageView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_ball));
            }
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout item_view;
        ImageView itemImage;
        TextView itemTxt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_view = itemView.findViewById(R.id.project_list_itme_three_view);
            itemImage = itemView.findViewById(R.id.project_list_item_view3);
            itemTxt = itemView.findViewById(R.id.project_list_item_txt3);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition(), boardList.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    private OnItemClickListener onItemClickListener;

    //点击事件
    public interface OnItemClickListener {
        void onItemClick(View view, int position, ManualPositionBtnBoard data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}