package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.module.taodetail.model.bean.TaoTagItem;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/9/9
 */
public class SearchNavtagAdapter extends RecyclerView.Adapter<SearchNavtagAdapter.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<TaoTagItem> mDatas = new ArrayList<>();
    private final LayoutInflater mInflater;
    private final int windowsWight;
    private final int size = 5;
    private final int margin = Utils.dip2px(9);

    public SearchNavtagAdapter(Activity context, ArrayList<TaoTagItem> datas) {
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);

        for (TaoTagItem data : datas) {
            String title = data.getTitle();
            if (!TextUtils.isEmpty(title) && title.length() <= 5) {
                mDatas.add(data);
            }
        }


        DisplayMetrics metric = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public SearchNavtagAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.item_hot_navtag_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchNavtagAdapter.ViewHolder viewHolder, int pos) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.navtagView.getLayoutParams();
        //左右间距
        layoutParams.leftMargin = margin;
        layoutParams.rightMargin = margin;

        //上下间距
        if (pos - size < 0) {
            layoutParams.bottomMargin = Utils.dip2px(5);
        } else {
            layoutParams.topMargin = Utils.dip2px(5);
            layoutParams.bottomMargin = Utils.dip2px(5);
        }

        int diameter = (windowsWight - (size * margin * 2) - Utils.dip2px(10)) / size;
        layoutParams.width = diameter;
        layoutParams.height = diameter;
        viewHolder.navtagView.setLayoutParams(layoutParams);

        TaoTagItem taoTagItem = mDatas.get(pos);
        String title = taoTagItem.getTitle();
        if (!TextUtils.isEmpty(title)) {
            if (title.length() <= 3) {
                viewHolder.navtagName.setMaxEms(3);
            } else if (title.length() == 4) {
                viewHolder.navtagName.setMaxEms(2);
            } else if (title.length() == 5) {
                viewHolder.navtagName.setMaxEms(3);
            } else if (title.length() == 6) {
                viewHolder.navtagName.setMaxEms(3);
            } else {
                viewHolder.navtagName.setMaxEms(4);
            }
            viewHolder.navtagName.setText(title);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout navtagView;
        private TextView navtagName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            navtagView = itemView.findViewById(R.id.item_hot_navtag_view);
            navtagName = itemView.findViewById(R.id.hot_navtag_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onHotClick(v, mDatas.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    //item点击回调
    public interface OnEventClickListener {
        void onHotClick(View v, TaoTagItem data);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}