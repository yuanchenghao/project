package com.module.home.controller.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.BbsBean;
import com.quicklyask.activity.R;

import java.util.List;

public class CommunityAdapter extends BaseQuickAdapter<BbsBean,BaseViewHolder> {

    private Context mContext;
    public CommunityAdapter(Context context,int layoutResId, @Nullable List<BbsBean> data) {
        super(layoutResId, data);
        mContext=context;

    }

    @Override
    protected void convert(BaseViewHolder helper, BbsBean item) {
        Glide.with(mContext).load(item.getImg_new()).into((ImageView) helper.getView(R.id.community_img_item));
        helper.setText(R.id.community_item_title,item.getTitle());
        helper.setText(R.id.community_item_content,item.getDesc());
    }
}
