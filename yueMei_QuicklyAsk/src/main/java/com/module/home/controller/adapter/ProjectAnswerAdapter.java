package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.view.CenterImageSpan;
import com.module.community.controller.other.VideoFlowLayoutGroup;
import com.module.home.model.bean.QuestionListData;
import com.module.home.model.bean.QuestionListReplyData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/5/14
 */
public class ProjectAnswerAdapter extends RecyclerView.Adapter<ProjectAnswerAdapter.ViewHolder> {
    private final Activity mContext;
    private final List<QuestionListData> mDatas;
    private final FunctionManager mFunctionManager;
    private String mFrom;
    private String TAG = "ProjectAnswerAdapter";

    public ProjectAnswerAdapter(Activity context, List<QuestionListData> datas,String from) {
        this.mContext = context;
        this.mDatas = datas;
        this.mFrom = from;
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_answer, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        QuestionListData data = mDatas.get(pos);
        QuestionListReplyData replyData = data.getReplyData();

        if(mFrom.equals("PostsDetails")){
            if(pos == 0){
                SpannableString spanString = new SpannableString("占位 " + data.getTitle());
                Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.project_answer_one);
                drawable.setBounds(0, 0, Utils.dip2px(13), Utils.dip2px(13));
                CenterImageSpan span = new CenterImageSpan(drawable);
                spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                viewHolder.mTitle.setText(spanString);
            }else if(pos == 1){
                SpannableString spanString = new SpannableString("占位 " + data.getTitle());
                Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.project_answer_two);
                drawable.setBounds(0, 0, Utils.dip2px(13), Utils.dip2px(13));
                CenterImageSpan span = new CenterImageSpan(drawable);
                spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                viewHolder.mTitle.setText(spanString);
            }else if(pos == 2){
                SpannableString spanString = new SpannableString("占位 " + data.getTitle());
                Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.project_answer_three);
                drawable.setBounds(0, 0, Utils.dip2px(13), Utils.dip2px(13));
                CenterImageSpan span = new CenterImageSpan(drawable);
                spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                viewHolder.mTitle.setText(spanString);
            }else {
                SpannableString spanString = new SpannableString(data.getTitle());
                viewHolder.mTitle.setText(spanString);
            }
        }else{
            SpannableString spanString = new SpannableString(data.getTitle());
            viewHolder.mTitle.setText(spanString);
        }


        //标题
        if (!TextUtils.isEmpty(data.getTitle())) {
            viewHolder.mTitle.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mTitle.setVisibility(View.GONE);
        }
        //用户头像
        mFunctionManager.setCircleImageSrc(viewHolder.mUserImg, replyData.getUserImg());
        //用户名称
        viewHolder.mUserName.setText(replyData.getUserName());
        //用户旁边的文字
        viewHolder.mUserNoticeText.setText(replyData.getNoticeText());

        //设置开始的图片
        SpannableString spanString = new SpannableString("占位 " + replyData.getReplyContent());
        Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.community_ask);
        drawable.setBounds(0, 0, Utils.dip2px(13), Utils.dip2px(13));
        CenterImageSpan span = new CenterImageSpan(drawable);
        spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        //评论内容
        viewHolder.mUserContent.setText(spanString);
        //标签
        ArrayList<DiaryTagList> diaryTagLists = new ArrayList<>();
        Log.e(TAG, "diaryTagLists == " + diaryTagLists.size());
        for (String tag : data.getTagList()) {
            diaryTagLists.add(new DiaryTagList("0", "", tag));
        }

        VideoFlowLayoutGroup videoFlowLayoutGroup = new VideoFlowLayoutGroup(mContext, viewHolder.mTag, diaryTagLists, Utils.getLocalColor(mContext, R.color.red_ff94ab), 12);
        videoFlowLayoutGroup.setClickCallBack(new VideoFlowLayoutGroup.ClickCallBack() {
            @Override
            public void onClick(View v, int pos, DiaryTagList data) {
                String url = data.getUrl();
                Log.e(TAG, "url == " + url);
                if (!TextUtils.isEmpty(url)) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                }
            }
        });

        //封面图
        if (!TextUtils.isEmpty(data.getImg())) {
            viewHolder.mImage.setVisibility(View.VISIBLE);
            mFunctionManager.setRoundImageSrc(viewHolder.mImage, data.getImg(), Utils.dip2px(4));
        } else {
            viewHolder.mImage.setVisibility(View.GONE);
        }

        //分割线是否显示
//        if(pos != mDatas.size() - 1){
            viewHolder.mLine.setVisibility(View.VISIBLE);
//        }else{
//            viewHolder.mLine.setVisibility(View.GONE);
//        }



//        //浏览数
//        viewHolder.mBrowse.setText("浏览 " + data.getViewNum());
//        //评论数
//        viewHolder.mComments.setText("评论 " + data.getAnswerNum());
//        //点赞数
//        viewHolder.mPraise.setText("赞 " + data.getAgreeNum());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    /**
     * 添加数据
     *
     * @param data
     */
    public void addData(List<QuestionListData> data) {
        int size = mDatas.size();
        mDatas.addAll(data);
        notifyItemRangeInserted(size, mDatas.size() - size);
    }

    public void clearData() {
        mDatas.clear();
    }

    public void addData2(ArrayList<QuestionListData> data) {
        mDatas.addAll(data);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitle;
        ImageView mUserImg;
        TextView mUserName;
        TextView mUserNoticeText;
        TextView mUserContent;
        FlowLayout mTag;
        ImageView mImage;
        View mLine;
//        TextView mBrowse;
//        TextView mComments;
//        TextView mPraise;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.item_project_answer_title);
            mUserImg = itemView.findViewById(R.id.item_project_answer_user_img);
            mUserName = itemView.findViewById(R.id.item_project_answer_user_name);
            mUserNoticeText = itemView.findViewById(R.id.item_project_answer_user_noticeText);
            mUserContent = itemView.findViewById(R.id.item_project_answer_user_content);
            mTag = itemView.findViewById(R.id.item_project_answer_tag);
            mImage = itemView.findViewById(R.id.item_project_answer_image);
            mLine = itemView.findViewById(R.id.item_project_answer_line);
//            mBrowse = itemView.findViewById(R.id.item_project_answer_browse);
//            mComments = itemView.findViewById(R.id.item_project_answer_comments);
//            mPraise = itemView.findViewById(R.id.item_project_answer_praise);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(getLayoutPosition(), mDatas.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    public List<QuestionListData> getDatas() {
        return mDatas;
    }

    //item点击事件
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int pos, QuestionListData data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
