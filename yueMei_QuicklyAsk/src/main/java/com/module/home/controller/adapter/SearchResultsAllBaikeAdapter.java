package com.module.home.controller.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.community.web.WebViewUrlLoading;
import com.module.home.model.bean.SearchAllBaikeData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索综合页百科列表
 * Created by 裴成浩 on 2019/7/25
 */
public class SearchResultsAllBaikeAdapter extends RecyclerView.Adapter<SearchResultsAllBaikeAdapter.ViewHolder> {

    private Context mContext;
    private List<SearchAllBaikeData> mData;

    public SearchResultsAllBaikeAdapter(Activity context, ArrayList<SearchAllBaikeData> hosLists) {
        this.mContext = context;
        this.mData = hosLists;
    }

    @NonNull
    @Override
    public SearchResultsAllBaikeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.search_results_all_baike_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultsAllBaikeAdapter.ViewHolder viewHolder, int pos) {
        SearchAllBaikeData data = mData.get(pos);

        viewHolder.mTitle.setText(data.getTitle());
        viewHolder.mContent.setText(data.getDesc());
        viewHolder.linktitle.setText(data.getLinktitle());

        Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.results_all_card_right);
        drawable.setBounds(0, 0, Utils.dip2px(10), Utils.dip2px(10));
        viewHolder.linktitle.setCompoundDrawables(null, null, drawable, null);
        viewHolder.linktitle.setCompoundDrawablePadding(Utils.dip2px(2));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mContent;
        private TextView linktitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.results_baike_item_title);
            mContent = itemView.findViewById(R.id.results_baike_item_content);
            linktitle = itemView.findViewById(R.id.results_baike_item_linktitle);

            //百科卡片点击
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = mData.get(getLayoutPosition()).getUrl();
                    if (!TextUtils.isEmpty(url)) {
                        WebViewUrlLoading.getInstance().showWebDetail(mContext, url);
                    }
                }
            });

        }
    }
}
