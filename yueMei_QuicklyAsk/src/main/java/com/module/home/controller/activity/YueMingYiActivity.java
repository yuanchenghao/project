package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 约名医
 */
public class YueMingYiActivity extends BaseActivity {

	private final String TAG = "YueMingYiActivity";

	@BindView(id = R.id.slide_pic_linearlayout, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.wan_beautiful_web_back, click = true)
	private RelativeLayout back;// 返回
	@BindView(id = R.id.wan_beautifu_docname)
	private TextView titleBarTv;

	private RelativeLayout shareBt;// 分享
	@BindView(id = R.id.tao_web_share_rly111, click = true)

	private WebView docDetWeb;

	private YueMingYiActivity mContex;

	public JSONObject obj_http;

	private String uid;

	// 分享
	private String shareUrl = "http://m.yuemei.com/mingyi/";
	private String shareContent = "约名医甄选公立三甲医生，保证服务质量。悦美作为医美行业资深品牌，将全程跟踪、把控各服务环节，为你营造一个安心、舒心的变美之旅。";
	private String shareTitle = "【悦美】约名医 专注品质医疗美容";
	private BaseWebViewClientMessage baseWebViewClientMessage;
	private String mUrl;
	private String mReferer;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_acty_slidepic);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = YueMingYiActivity.this;
		uid = Utils.getUid();
		mUrl = getIntent().getStringExtra("url");
		mReferer = Cfg.loadStr(this, "referer", "");
		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setTypeOutside(true);
		baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
			@Override
			public void typeOutside(WebView view, String url) {
				if(url.startsWith("type")){
					newShowWebde(url);
				}else{
					WebUrlTypeUtil.getInstance(mContex).urlToApp(url,"0","0");
				}
			}
		});
		initWebview();

		LodUrl1(FinalConstant.MINGYI_MORE);
		titleBarTv.setText("约名医");

	}


	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	void newShowWebde(String result) {

		if (result.length() > 0) {
			WebUrlTypeUtil.getInstance(mContex).urlToApp(result, "0", "0");
		}
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String url) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
		if (null != docDetWeb) {
			docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.wan_beautiful_web_back:
			// finish();
			onBackPressed();
			break;
		case R.id.tao_web_share_rly111:// 分享按钮
			setShare();
			break;
		}
	}

	/**
	 * 设置分享
	 */
	private void setShare() {
		BaseShareView baseShareView = new BaseShareView(mContex);
		baseShareView
				.setShareContent(shareContent)
				.ShareAction();

		baseShareView.getShareBoardlistener()
				.setSinaText(shareContent + shareUrl + "分享自@悦美整形APP")
				.setSinaThumb(new UMImage(mContex, R.drawable.share_mingyi_))
				.setSmsText(shareTitle + "，" + shareUrl)
				.setTencentUrl(shareUrl)
				.setTencentTitle(shareTitle)
				.setTencentThumb(new UMImage(mContex, R.drawable.share_mingyi_))
				.setTencentDescription(shareContent)
				.setTencentText(shareContent)
				.getUmShareListener()
				.setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
					@Override
					public void onShareResultClick(SHARE_MEDIA platform) {

						if (!platform.equals(SHARE_MEDIA.SMS)) {
							Toast.makeText(mContex, " 分享成功啦",
									Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
	}

	public void onResume() {
		super.onResume();
		uid = Utils.getUid();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

}