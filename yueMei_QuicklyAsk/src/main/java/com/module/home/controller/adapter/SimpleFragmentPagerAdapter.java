package com.module.home.controller.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.home.view.BadgeView;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 消息带下标的
 * Created by 裴成浩 on 2017/10/17.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private List<Fragment> mFragmentList;
    private List<String> mPageTitleList;
    private List<Integer> mBadgeCountList;

    public SimpleFragmentPagerAdapter(Context context,
                                      FragmentManager fm,
                                      List<Fragment> fragmentList,
                                      List<String> pageTitleList,
                                      List<Integer> badgeCountList) {
        super(fm);
        this.mContext = context;
        this.mFragmentList = fragmentList;
        this.mPageTitleList = pageTitleList;
        this.mBadgeCountList = badgeCountList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitleList.get(position);
    }

    public View getTabItemView(int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.tab_layout_item, null);
        TextView textView = view.findViewById(R.id.textview);
        textView.setText(mPageTitleList.get(position));
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();

        View target = view.findViewById(R.id.badgeview_target);
        Integer integer = mBadgeCountList.get(position);
        if(integer > 0 ){
            target.setVisibility(View.VISIBLE);
            params.setMargins(-Utils.dip2px(mContext,30),0,0,0);

            BadgeView badgeView = new BadgeView(mContext);
            badgeView.setTargetView(target);
            badgeView.setBadgeMargin(0, 6, 6, 0);
            badgeView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
            badgeView.setText(formatBadgeNumber(mBadgeCountList.get(position)));
        }else {
            target.setVisibility(View.INVISIBLE);
            params.setMargins(-Utils.dip2px(mContext,10),0,0,0);
        }

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }

    public static String formatBadgeNumber(int value) {
        if (value <= 0) {
            return null;
        }

        if (value < 100) {
            // equivalent to String#valueOf(int);
            return Integer.toString(value);
        }

        // my own policy
        return "99+";
    }
}

