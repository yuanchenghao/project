package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.cn.demo.pinyin.AssortView;
import com.cn.demo.pinyin.PinyinAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.community.model.api.PartListApi;
import com.module.doctor.controller.adapter.HotCityAdapter;
import com.module.doctor.model.api.HotCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.home.controller.adapter.TeMaiSortAdapter;
import com.module.home.view.LoadingProgress;
import com.module.other.api.HotIssueApi;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.quicklyask.view.MyGridView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 超值特卖
 * <p/>
 * Created by dwb on 16/10/21.
 */
public class TeMaiActivity extends BaseActivity {

    private final String TAG = "TeMaiActivity";

    private Context mContex;

    // List
    @BindView(id = R.id.my_doc_list_view)
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;
    private List<HomeTaoData> lvHotIssueData = new ArrayList<>();
    private List<HomeTaoData> lvHotIssueMoreData = new ArrayList<>();
    private TaoAdpter623 hotAdpter;

    @BindView(id = R.id.my_collect_post_tv_nodata)
    private LinearLayout nodataTv;// 数据为空时候的显示

    @BindView(id = R.id.project_part_pop_rly1, click = true)
    private RelativeLayout partRly;
    @BindView(id = R.id.project_sort_pop_rly, click = true)
    private RelativeLayout cityRly;
    @BindView(id = R.id.project_part_pop_tv)
    private TextView partTv;
    @BindView(id = R.id.project_sort_pop_tv)
    private TextView cityTv;
    @BindView(id = R.id.project_part_pop_iv)
    private ImageView partIv;
    @BindView(id = R.id.project_sort_pop_iv)
    private ImageView cityIv;

    @BindView(id = R.id.zhengxing_riji_top)
    private CommonTopBar mTop;

    private String cityName;
    private String cityName1;
    private String partId = "0";

    private List<TaoPopItemIvData> lvGroupData = new ArrayList<>();

    // 城市筛选
    private ExpandableListView eListView;
    private AssortView assortView;
    private PinyinAdapter pinAdapter;
    private List<String> names;
    private TextView cityLocTv;
    private RelativeLayout othserRly;
    private View headView;
    private String autoCity;

    private List<CityDocDataitem> hotcityList;
    private MyGridView hotGridlist;
    private HotCityAdapter hotcityAdapter;

    private CityPopwindows cityPop;
    private SortPopupwindows sortPop;
    private int sorPos = 0;
    private TeMaiSortAdapter mSort2Adapter;
    private LoadingProgress mDialog;
    private HotIssueApi hotIssueApi;
    private HashMap<String, Object> hotIssueMap = new HashMap<>();


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_zhengxing_riji);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = TeMaiActivity.this;

        mDialog = new LoadingProgress(TeMaiActivity.this);
        hotIssueApi = new HotIssueApi();
        cityName = "全国";
        mTop.setCenterText("超值特卖");

        cityName1 = Cfg.loadStr(getApplicationContext(), FinalConstant.DWCITY, "");
        if (cityName1.length() > 0) {

            if (cityName1.equals("失败")) {
                cityName = "全国";
                cityTv.setText(cityName);

            } else if (cityName1.equals(cityName)) {

            } else {
                cityName = cityName1;
                cityTv.setText(cityName);
            }

        } else {
            cityName = "全国";
            cityTv.setText("全国");
        }

        cityPop = new CityPopwindows(mContex, partRly);
        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        loadPartList();
        initList();

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }


    void loadPartList() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", "3");
        new PartListApi().getCallBack(mContex, maps, new BaseCallBackListener<List<TaoPopItemIvData>>() {
            @Override
            public void onSuccess(List<TaoPopItemIvData> taoPopItemIvData) {
                if (taoPopItemIvData != null) {
                    lvGroupData = taoPopItemIvData;
                    sortPop = new SortPopupwindows(TeMaiActivity.this, partRly);
                    sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

                        @Override
                        public void onDismiss() {
                            initpop();
                        }
                    });
                } else {
                    ViewInject.toast("请求错误");
                }
            }
        });
    }


    void initList() {

        mHandler = getHandler();
        mDialog.startLoading();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new DropDownListView.OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvHotIssueData = null;
                lvHotIssueMoreData = null;
                mCurPage = 1;
                mDialog.startLoading();
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {

                if (null != lvHotIssueData && lvHotIssueData.size() > 0) {
                    String id = lvHotIssueData.get(pos).get_id();
                    Intent it1 = new Intent();
                    it1.putExtra("id", id);
                    it1.putExtra("source", "4");
                    it1.putExtra("objid", "0");
                    it1.setClass(mContex, TaoDetailActivity.class);
                    startActivity(it1);
                }
            }
        });
    }

    void onreshData() {
        lvHotIssueData = null;
        lvHotIssueMoreData = null;
        mCurPage = 1;
        mDialog.startLoading();
        lodHotIssueData(true);
        mlist.setHasMore(true);
    }

    private void lodHotIssueData(final boolean isDonwn) {
        hotIssueMap.put("page", mCurPage);
        hotIssueMap.put("partId", partId);
        hotIssueMap.put("flag", "1");
        hotIssueApi.getCallBack(mContex, hotIssueMap, new BaseCallBackListener<List<HomeTaoData>>() {
            @Override
            public void onSuccess(List<HomeTaoData> homeTaoDatas) {
                Message msg = null;
                if (isDonwn) {
                    if (mCurPage == 1) {

                        lvHotIssueData = homeTaoDatas;


                        msg = mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                } else {
                    mCurPage++;
                    lvHotIssueMoreData = homeTaoDatas;
                    msg = mHandler.obtainMessage(2);
                    msg.sendToTarget();
                }
            }
        });
    }


    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:

                        if (null != lvHotIssueData && lvHotIssueData.size() > 0) {

                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);

                            mDialog.stopLoading();

                            hotAdpter = new TaoAdpter623(TeMaiActivity.this, lvHotIssueData);
                            mlist.setAdapter(hotAdpter);

                            mlist.onDropDownComplete();
                            mlist.onBottomComplete();
                        } else {
                            mDialog.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (null != lvHotIssueMoreData && lvHotIssueMoreData.size() > 0) {
                            hotAdpter.add(lvHotIssueMoreData);
                            hotAdpter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }


    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.project_sort_pop_rly:
                if (null != cityPop) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        if (Build.VERSION.SDK_INT == 24) {          // 适配 android 7.0
                            // 适配 android 7.0
                            int[] location = new int[2];
                            partRly.getLocationOnScreen(location);
                            int x = location[0];
                            int y = location[1];
                            cityPop.showAtLocation(partRly, Gravity.NO_GRAVITY, 0, y + partRly.getHeight() + cityPop.getHeight() + 1);
                        } else {
                            cityPop.showAsDropDown(partRly, 0, 0);
                        }
                    }
                    initpop();
                }
                break;
            case R.id.project_part_pop_rly1:
                if (null != sortPop) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        if (Build.VERSION.SDK_INT == 24) {          // 适配 android 7.0
                            // 适配 android 7.0
                            int[] location = new int[2];
                            partRly.getLocationOnScreen(location);
                            int x = location[0];
                            int y = location[1];
                            sortPop.showAtLocation(partRly, Gravity.NO_GRAVITY, 0, y + partRly.getHeight() + sortPop.getHeight() + 1);
                        } else {
                            sortPop.showAsDropDown(partRly, 0, 0);
                        }
                    }
                    initpop();
                }
                break;
        }
    }


    void initpop() {

        if (null != sortPop) {
            if (sortPop.isShowing()) {
                partTv.setTextColor(Color.parseColor("#E95165"));
                partIv.setBackgroundResource(R.drawable.red_tao_tab);
            } else {
                partTv.setTextColor(Color.parseColor("#414141"));
                partIv.setBackgroundResource(R.drawable.gra_tao_tab);
            }
        }
        if (null != cityPop) {
            if (cityPop.isShowing()) {
                cityTv.setTextColor(Color.parseColor("#E95165"));
                cityIv.setBackgroundResource(R.drawable.red_tao_tab);
            } else {
                cityTv.setTextColor(Color.parseColor("#414141"));
                cityIv.setBackgroundResource(R.drawable.gra_tao_tab);
            }
        }
    }


    /**
     * 部分下拉选择
     *
     * @author Rubin
     */
    public class SortPopupwindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public SortPopupwindows(Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.pop_tao_zx_project, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            if (Build.VERSION.SDK_INT >= 25) {
                WindowManager wm = TeMaiActivity.this.getWindowManager();
                int height = wm.getDefaultDisplay().getHeight();
                setHeight(height - DensityUtil.dip2px(110));
            } else {
                setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            }
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);

            update();

            final ListView prlist1 = view.findViewById(R.id.pop_list_tao_project_list);
            RelativeLayout otherRly1 = view.findViewById(R.id.ly_content_ly1);
            RelativeLayout otherRly2 = view.findViewById(R.id.ly_content_ly2);
            otherRly1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    dismiss();
                    initpop();
                }
            });

            otherRly2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    dismiss();
                    initpop();
                }
            });

            mSort2Adapter = new TeMaiSortAdapter(TeMaiActivity.this.mContex, lvGroupData, sorPos);
            prlist1.setAdapter(mSort2Adapter);

            prlist1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                    partId = lvGroupData.get(pos).get_id();
                    sorPos = pos;

                    mSort2Adapter = new TeMaiSortAdapter(TeMaiActivity.this.mContex, lvGroupData, pos);
                    prlist1.setAdapter(mSort2Adapter);

                    partTv.setText(lvGroupData.get(pos).getName());
                    dismiss();
                    initpop();
                    onreshData();
                }
            });
        }
    }


    /**
     * 地区下拉选择
     *
     * @author Rubin
     */
    public class CityPopwindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public CityPopwindows(Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.pop_city_diqu, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            eListView = view.findViewById(R.id.elist1);
            assortView = view.findViewById(R.id.assort1);
            othserRly = view.findViewById(R.id.ly_content_ly1);

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            if (Build.VERSION.SDK_INT >= 25) {
                WindowManager wm = TeMaiActivity.this.getWindowManager();
                int height = wm.getDefaultDisplay().getHeight();
                setHeight(height - DensityUtil.dip2px(110));
            } else {
                setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            }

            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            initViewData();
            autoCity = Cfg.loadStr(mContext, FinalConstant.LOCATING_CITY, "失败");
            cityLocTv.setText(autoCity);


            othserRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    dismiss();
                    initpop();
                }
            });
        }
    }


    @SuppressLint("InflateParams")
    void initViewData() {
        names = new ArrayList<String>();
        names.add("鞍山");

        names.add("保定");
        names.add("北京");

        names.add("常州");
        names.add("长春");
        names.add("长沙");
        names.add("成都");
        names.add("重庆");

        names.add("大连");
        names.add("大庆");
        names.add("丹东");
        names.add("东莞");

        names.add("佛山");
        names.add("福州");
        names.add("阜阳");

        names.add("广州");
        names.add("贵阳");

        names.add("哈尔滨");
        names.add("邯郸");
        names.add("杭州");
        names.add("合肥");
        names.add("衡水");
        names.add("呼和浩特");

        names.add("吉林");
        names.add("济南");
        names.add("佳木斯");
        names.add("金华");

        names.add("昆明");

        names.add("兰州");
        names.add("临沂");
        names.add("柳州");
        names.add("洛阳");

        names.add("绵阳");

        names.add("南昌");
        names.add("南京");
        names.add("南宁");
        names.add("宁波");

        names.add("普洱");

        names.add("齐齐哈尔");
        names.add("秦皇岛");
        names.add("青岛");


        names.add("上海");
        names.add("绍兴");
        names.add("深圳");
        names.add("沈阳");
        names.add("石家庄");
        names.add("苏州");

        names.add("台州");
        names.add("泰安");
        names.add("太原");
        names.add("唐山");
        names.add("天津");
        names.add("通化");

        names.add("威海");
        names.add("潍坊");
        names.add("温州");
        names.add("乌鲁木齐");
        names.add("无锡");
        names.add("武汉");

        names.add("西安");
        names.add("西宁");
        names.add("厦门");


        names.add("烟台");
        names.add("延吉");
        names.add("阳江");
        names.add("宜昌");

        names.add("郑州");
        names.add("珠海");
        names.add("淄博");

        LayoutInflater mInflater = getLayoutInflater();
        headView = mInflater.inflate(R.layout.main_city_select__head_560, null);
        eListView.addHeaderView(headView);
        hotGridlist = headView.findViewById(R.id.group_grid_list1);

        loadHotCity();

        RelativeLayout cityAll = headView.findViewById(R.id.city_all_doc_rly);
        cityAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                cityName = "全国";
                cityTv.setText(cityName);
                Cfg.saveStr(TeMaiActivity.this.mContex, FinalConstant.DWCITY, cityName);
                Utils.getCityOneToHttp(TeMaiActivity.this.mContex, "1");

                cityPop.dismiss();
                initpop();

                onreshData();

            }
        });

        RelativeLayout cityAntuo = headView.findViewById(R.id.city_auto_loaction_rly);
        cityLocTv = headView.findViewById(R.id.doc_city_select_tv);

        // if (dwCity.length() > 0) {
        // cityLocTv.setText(dwCity + "(自动定位)");
        // } else {
        // cityLocTv.setText("获取位置失败");
        // }

        cityAntuo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String city = autoCity;
                if (city.equals("失败")) {
                    cityName = "全国";
                } else {
                    cityName = city;
                }
                cityTv.setText(cityName);

                Cfg.saveStr(TeMaiActivity.this.mContex, FinalConstant.DWCITY, cityName);
                Utils.getCityOneToHttp(TeMaiActivity.this.mContex, "1");

                onreshData();

                cityPop.dismiss();
                initpop();
            }
        });

        pinAdapter = new PinyinAdapter(mContex, names);
        eListView.setAdapter(pinAdapter);

        eListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView arg0, View arg1, int groupPosition, int childPosition, long arg4) {
                int tolal = 0;

                for (int j = 0; j < groupPosition; j++) {
                    int chidsize1 = pinAdapter.getChildrenCount(j);
                    tolal = tolal + chidsize1;
                }
                tolal = tolal + childPosition;

                String city;
                city = names.get(tolal);
                cityName = city;
                cityTv.setText(cityName);
                Cfg.saveStr(TeMaiActivity.this.mContex, FinalConstant.DWCITY, cityName);
                Utils.getCityOneToHttp(TeMaiActivity.this.mContex, "1");

                onreshData();

                cityPop.dismiss();
                initpop();

                return true;
            }
        });

        // 展开所有
        for (int i = 0, length = pinAdapter.getGroupCount(); i < length; i++) {
            eListView.expandGroup(i);
        }

        // 字母按键回调
        assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {

            View layoutView = LayoutInflater.from(mContex).inflate(R.layout.alert_dialog_menu_layout, null);

            TextView text = (TextView) layoutView.findViewById(R.id.content);
            RelativeLayout alRly = (RelativeLayout) layoutView.findViewById(R.id.pop_city_rly);

            public void onTouchAssortListener(String str) {
                int index = pinAdapter.getAssort().getHashList().indexOfKey(str);
                if (index != -1) {
                    eListView.setSelectedGroup(index);

                }
            }

            @Override
            public void onTouchAssortUP() {

            }
        });
    }

    void loadHotCity() {
        new HotCityApi().getCallBack(mContex, new HashMap<String, Object>(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitem) {
                hotcityList = cityDocDataitem;

                hotcityAdapter = new HotCityAdapter(mContex, hotcityList);
                hotGridlist.setAdapter(hotcityAdapter);
                hotGridlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                        cityName = hotcityList.get(pos).getName();

                        cityTv.setText(cityName);
                        Cfg.saveStr(TeMaiActivity.this.mContex, FinalConstant.DWCITY, cityName);
                        Utils.getCityOneToHttp(TeMaiActivity.this.mContex, "1");

                        onreshData();

                        cityPop.dismiss();
                        initpop();
                    }
                });
            }

        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

}
