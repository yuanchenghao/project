package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.model.bean.ProjectFocuMapData;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 频道页固定三卡适配器
 * Created by 裴成浩 on 2019/4/2
 */
public class ProjectDetailsActivityAdapter extends RecyclerView.Adapter<ProjectDetailsActivityAdapter.ViewHolder> {

    private String TAG = "ProjectDetailsActivityAdapter";
    private Activity mContext;
    private List<ProjectFocuMapData> mDatas;
    private final FunctionManager mFunctionManager;
    private final int windowsWight;

    public ProjectDetailsActivityAdapter(Activity context, List<ProjectFocuMapData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        Log.e(TAG, "mDatas == " + mDatas.size());
        mFunctionManager = new FunctionManager(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public ProjectDetailsActivityAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_activity, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectDetailsActivityAdapter.ViewHolder viewHolder, int position) {
        ProjectFocuMapData data = mDatas.get(position);

        mFunctionManager.setPlaceholderImageSrc(viewHolder.mImageView, data.getImg());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.item_project_details_activity_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProjectFocuMapData data = mDatas.get(getLayoutPosition());
                    String url = data.getUrl();
                    HashMap<String, String> event_params = data.getEvent_params();
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_3K, (getLayoutPosition() + 1) + ""),event_params);
                            WebViewUrlLoading.getInstance().showWebDetail(mContext,url);
                    }
                }
            });

        }
    }
}
