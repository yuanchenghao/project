package com.module.home.view;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

/**
 * 搜索页面的Top
 * Created by 裴成浩 on 2019/4/16
 */
public class SearchTitleView extends LinearLayout {
    private String TAG = "SearchTitleView";
    private final Context mContext;
    private EditText mEdit;
    private ImageView mColse;
    private TextView mCancel;

    private final String TEXT_SEARCH = "搜索";
    private final String TEXT_CANCEL = "取消";
    private final String EDIT_HINT = "搜整形项目、问题、整形医生";

    private boolean isSetContent = false;           //判断是否是手动设置的

    public SearchTitleView(Context context) {
        this(context, null);
    }

    public SearchTitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView() {
        setOrientation(VERTICAL);

        //初始化默认布局
        View view = View.inflate(mContext, R.layout.search_title_view, this);
        mEdit = view.findViewById(R.id.search_title_edit);
        mColse = view.findViewById(R.id.search_title_colse);
        mCancel = view.findViewById(R.id.search_title_cancel);

        mEdit.clearFocus();

        //设置默认提示文字
        setHint(EDIT_HINT);
        mEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“搜索”键*/
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String contentText = getEditStr();
                    String hintText = mEdit.getHint().toString();

                    if (!TextUtils.isEmpty(contentText)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onSearchClick(v, contentText);
                        }
                    } else {
                        if (!TextUtils.isEmpty(hintText) && !EDIT_HINT.equals(hintText)) {
                            setContent(hintText);
                            if (onEventClickListener != null) {
                                onEventClickListener.onSearchClick(v, getHint());
                            }
                        } else {
                            MyToast.makeTextToast2(mContext, "请输入搜索内容", MyToast.SHOW_TIME).show();
                        }
                    }


                    //隐藏软键盘
                    if (mContext instanceof Activity) {
                        Utils.hideSoftKeyboard((Activity) mContext);
                    }
                    return true;
                }
                return false;
            }
        });

        //editView监听
        mEdit.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e(TAG, "onTextChanged == ");
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e(TAG, "beforeTextChanged == ");

            }

            public void afterTextChanged(Editable s) {
                Log.e(TAG, "afterTextChanged == ");
                if (s.length() == 0) {
                    mCancel.setText(TEXT_CANCEL);
                    mColse.setVisibility(GONE);
                } else {
                    mCancel.setText(TEXT_SEARCH);
                    mColse.setVisibility(VISIBLE);
                }

                if (onEventClickListener != null) {
                    if (isSetContent) {
                        isSetContent = false;
                    } else {
                        onEventClickListener.onKeywordsClick(getEditStr());
                    }
                }
            }
        });

        mEdit.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (onEventClickListener != null) {
                    onEventClickListener.onFocusChange(v, hasFocus, getEditStr());
                }
            }
        });

        //添加底部下划线
        View bottomLine = new View(mContext);
        bottomLine.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(1)));
        bottomLine.setBackgroundColor(Utils.getLocalColor(mContext, R.color._cc));
        addView(bottomLine);

        //搜索/取消点击
        mCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    if (TEXT_SEARCH.equals(mCancel.getText().toString())) {
                        onEventClickListener.onSearchClick(v, getEditStr());
                        mCancel.setText(TEXT_CANCEL);
                    } else {
                        onEventClickListener.onCancelClick(v);
                    }
                }
            }
        });

        //清空搜索列表
        mColse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof Activity) {
                    Utils.hideSoftKeyboard((Activity) mContext);
                }
                mEdit.setText("");
            }
        });
    }

    /**
     * 设置搜索词
     *
     * @param keys
     */
    public void setContent(String keys) {
        isSetContent = true;
        mEdit.setText(keys);
        mCancel.setText(TEXT_CANCEL);
        mEdit.clearFocus();                         //光标失去焦点
//        mEdit.setSelection(getEditStr().length()); //将光标移至文字末尾
    }

    /**
     * 设置提示文字
     *
     * @param hint
     */
    public void setHint(String hint) {
        mEdit.setHint(hint);
    }

    /**
     * 获取到搜索内容
     *
     * @return
     */
    public String getEditStr() {
        return mEdit.getText().toString();
    }

    public String getHint(){
        return mEdit.getHint().toString();
    }


    public EditText getEditText(){
        return mEdit;
    }

    public interface OnEventClickListener {
        void onFocusChange(View v, boolean hasFocus, String key);     //是否获取焦点

        void onKeywordsClick(String key);                 //搜索联想词回调

        void onSearchClick(View v, String text);          //搜索回调

        void onCancelClick(View v);                      //取消回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
