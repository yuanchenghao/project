package com.module.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.home.model.bean.HuangDeng1;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 首页大促模块
 * Created by 裴成浩 on 2019/9/18
 */
public class HomeTopPromote extends LinearLayout {
    private final Context mContext;
    private String TAG = "HomeTopPromote";
    private boolean isBottom;
    private List<HuangDeng1> mDatas;
    private int windowsWight;
    private View bottomView;

    public HomeTopPromote(Context context) {
        this(context, null);
    }

    public HomeTopPromote(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeTopPromote(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        setOrientation(VERTICAL);

    }

    public void initView(boolean isBottom, List<HuangDeng1> data, final int windowsWight) {
        this.isBottom = isBottom;
        this.mDatas = data;
        this.windowsWight = windowsWight;
        if (data.size() > 0) {
            //头部图片
            final HuangDeng1 huangDeng0 = data.get(0);
            final ImageView imageTop = new ImageView(mContext);
            imageTop.setScaleType(ImageView.ScaleType.FIT_XY);

            addView(imageTop);
            Glide.with(mContext).load(huangDeng0.getImg()).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();

                    int imgWidth = windowsWight - ((MarginLayoutParams) getLayoutParams()).leftMargin - ((MarginLayoutParams) getLayoutParams()).rightMargin;
                    int imgHeight = (imgWidth * intrinsicHeight) / intrinsicWidth;

                    ViewGroup.LayoutParams layoutParams = imageTop.getLayoutParams();
                    layoutParams.width = imgWidth;
                    layoutParams.height = imgHeight;
                    imageTop.setLayoutParams(layoutParams);
                    imageTop.setImageDrawable(resource);


                    if(mDatas.size() > 1){
                        setBottomView(imgHeight);
                    }else{
                        if (onEventClickListener != null) {
                            onEventClickListener.onImageLoadingEndClick(imgHeight);
                        }
                    }
                }

            });

            //设置image
            imageTop.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, huangDeng0);
                    }
                }
            });

        }
    }

    /**
     * 底部view设置
     *
     * @param topImgHeight ：头部图片高度
     */
    private void setBottomView(final int topImgHeight) {
        if (isBottom && mDatas.size() >= 6) {
            final HuangDeng1 huangDeng1 = mDatas.get(1);

            bottomView = View.inflate(mContext, R.layout.home_top_promote_view, this);
            final ImageView mOne = bottomView.findViewById(R.id.home_top_promote_one);

            Log.e(TAG, "huangDeng1.getImg()  == " + huangDeng1.getImg());
            Glide.with(mContext).load(huangDeng1.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {

                    mOne.setImageDrawable(resource);

                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();
                    MarginLayoutParams parentPl = (MarginLayoutParams) getLayoutParams();
                    MarginLayoutParams bottomPl = (MarginLayoutParams) bottomView.getLayoutParams();
                    int imgWidth = (windowsWight - parentPl.leftMargin - parentPl.rightMargin - bottomPl.leftMargin - bottomPl.rightMargin) / 3;
                    int bottomimgHeight = (imgWidth * intrinsicHeight) / intrinsicWidth;

                    if (onEventClickListener != null) {
                        onEventClickListener.onImageLoadingEndClick(topImgHeight + bottomimgHeight);
                    }

                    ViewGroup.LayoutParams layoutParams = getLayoutParams();
                    layoutParams.height = topImgHeight + bottomimgHeight;
                    setLayoutParams(layoutParams);

                    setBottomImage();
                }

            });

            mOne.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, huangDeng1);
                    }
                }
            });

        }
    }

    private void setBottomImage() {

        final HuangDeng1 huangDeng2 = mDatas.get(2);
        final HuangDeng1 huangDeng3 = mDatas.get(3);
        final HuangDeng1 huangDeng4 = mDatas.get(4);
        final HuangDeng1 huangDeng5 = mDatas.get(5);

        ImageView mTwoOne = bottomView.findViewById(R.id.home_top_promote_two_one);
        ImageView mTwoTwo = bottomView.findViewById(R.id.home_top_promote_two_two);
        ImageView mThreeOne = bottomView.findViewById(R.id.home_top_promote_three_one);
        ImageView mThreeTwo = bottomView.findViewById(R.id.home_top_promote_three_two);

        Glide.with(mContext).load(huangDeng2.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mTwoOne);
        mTwoOne.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng2);
                }
            }
        });

        Glide.with(mContext).load(huangDeng3.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mTwoTwo);
        mTwoTwo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng3);
                }
            }
        });

        Glide.with(mContext).load(huangDeng4.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mThreeOne);
        mThreeOne.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng4);
                }
            }
        });

        Glide.with(mContext).load(huangDeng5.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mThreeTwo);
        mThreeTwo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng5);
                }
            }
        });
    }


    public interface OnEventClickListener {
        void onItemClick(View v, HuangDeng1 data);                      //item点击回调

        void onImageLoadingEndClick(int imgHeight);                                  //图片加载回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
