package com.module.home.view.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.cpoopc.scrollablelayoutlib.ScrollableHelper;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData23;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.DropDownListView;
import com.umeng.analytics.MobclickAgent;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 搜索后的问题贴
 * 
 * @author Rubin
 * 
 */
public class QuestionListFragment extends ScrollAbleFragment implements ScrollableHelper.ScrollableContainer   {

	private final String TAG = "QuestionListFragment";

	private Context mCotext;


	private LinearLayout docText;
	// List
	private DropDownListView mlist;
	private int mCurPage = 1;
	private Handler mHandler;
	private List<BBsListData550> lvBBslistData = new ArrayList<BBsListData550>();
	private List<BBsListData550> lvBBslistMoreData = new ArrayList<BBsListData550>();
	private BBsListAdapter bbsListAdapter;

	private String id = " ";

	private String key;
	private String city;
	private LinearLayout nodataTv;
	private LoadingProgress mDialog;


	public static QuestionListFragment newInstance(String key,String city) {

		QuestionListFragment listFragment = new QuestionListFragment();
		Bundle b = new Bundle();
		b.putString("key", key);
		b.putString("city", city);
		listFragment.setArguments(b);
		return listFragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_search_list, container, false);
		mlist = v.findViewById(R.id.list_searh);
		nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata1);

		mDialog = new LoadingProgress(getActivity());
		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Log.e(TAG, "onCreate");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
		super.onActivityCreated(savedInstanceState);
		mCotext = getActivity();

		if(isAdded()) {
			key = getArguments().getString("key");
			city = getArguments().getString("city");
		}
		initList();
	}

	@Override
	public void onStart() {
		super.onStart();
		// Log.e(TAG, "onStart");
	}

	void initList() {

		mHandler = getHandler();
		mDialog.startLoading();
		lodBBsListData550(true);

		mlist.setOnBottomListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lodBBsListData550(false);
			}
		});

		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpter, View v, int pos,
					long arg3) {

				String url = lvBBslistData.get(pos).getUrl();
				if (url.length() > 0) {
					String qid = lvBBslistData.get(pos).getQ_id();
					Intent it2 = new Intent();
					it2.putExtra("url", url);
					it2.putExtra("qid", qid);
					// Log.e(TAG, "url=" + url + "/qid=" + qid);
					it2.setClass(getActivity(), DiariesAndPostsActivity.class);
					startActivity(it2);
				}
			}
		});
	}

	void lodBBsListData550(final boolean isDonwn) {

		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = null;
				HashMap<String,Object> maps=new HashMap<>();
				maps.put("key", URLEncoder.encode(key));
				maps.put("type","2");
				maps.put("page",mCurPage+"");
				if (isDonwn) {
					if (mCurPage == 1) {
						Log.e("url222", "3333");
						new LodHotIssueDataApi().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
							@Override
							public void onSuccess(ServerData serverData) {
								if ("1".equals(serverData.code)){
									try {
										SearchResultData23 searchResultData23 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData23.class);
										lvBBslistData=searchResultData23.getList();
										Message message = mHandler.obtainMessage(1);
										message.sendToTarget();
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						});
//						lvBBslistData = HttpData.loadSearchBBsListData(key,
//								mCurPage, "2", city);
					}
				} else {
					mCurPage++;
					maps.put("page",mCurPage+"");
					Log.e("url222", "44444");
					new LodHotIssueDataApi().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
						@Override
						public void onSuccess(ServerData serverData) {
							if ("1".equals(serverData.code)){
								try {
									SearchResultData23 searchResultData23 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData23.class);
									lvBBslistMoreData=searchResultData23.getList();
									Message message = mHandler.obtainMessage(2);
									message.sendToTarget();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					});
					//lvBBslistMoreData = HttpData.loadSearchBBsListData(key, mCurPage, "2", city);

				}
			}
		}).start();

	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (null != lvBBslistData && lvBBslistData.size() > 0) {
						nodataTv.setVisibility(View.GONE);

						mDialog.stopLoading();
						if (null != getActivity()) {
							bbsListAdapter = new BBsListAdapter(getActivity(),
									lvBBslistData);
							mlist.setAdapter(bbsListAdapter);
						}

						mlist.onBottomComplete();
					} else {
						mDialog.stopLoading();
						nodataTv.setVisibility(View.VISIBLE);
						mlist.setVisibility(View.GONE);
						// ViewInject.toast("您还没有收藏的专家");
					}
					break;
				case 2:
					if (null!=lvBBslistMoreData&&lvBBslistMoreData.size()>0) {
						bbsListAdapter.add(lvBBslistMoreData);
						bbsListAdapter.notifyDataSetChanged();
						mlist.onBottomComplete();
					} else {
						mlist.setHasMore(false);
						mlist.setShowFooterWhenNoMore(true);
						mlist.onBottomComplete();
					}
					break;
				}

			}
		};
	}

	@Override
	public View getScrollableView() {
		return mlist;
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MainScreen"); // 统计页面
		StatService.onResume(getActivity());
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MainScreen");
		StatService.onPause(getActivity());
	}
}
