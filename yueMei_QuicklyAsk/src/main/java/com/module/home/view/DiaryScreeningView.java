package com.module.home.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.other.module.bean.TaoPopItemData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;

/**
 * 频道页面日记筛选头
 * Created by 裴成浩 on 2019/6/25
 */
public class DiaryScreeningView extends LinearLayout {

    private final Context mContext;
    private ArrayList<TaoPopItemData> mDatas = new ArrayList<>();
    private View[] views = new LinearLayout[4];
    private LinearLayout[] screeningClick = new LinearLayout[4];
    private LinearLayout[] screeningBg = new LinearLayout[4];
    private TextView[] screeningText = new TextView[4];
    private String TAG = "DiaryScreeningView";

    public DiaryScreeningView(Context context) {
        this(context, null);
    }

    public DiaryScreeningView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DiaryScreeningView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        mDatas.add(new TaoPopItemData("1", "推荐"));
        mDatas.add(new TaoPopItemData("2", "人气"));
        mDatas.add(new TaoPopItemData("3", "最热"));
        mDatas.add(new TaoPopItemData("4", "最新"));

        initView();
    }

    private void initView() {
        setOrientation(HORIZONTAL);
        setBackgroundColor(Utils.getLocalColor(mContext, R.color.white));
        for (int i = 0; i < mDatas.size(); i++) {
            final TaoPopItemData data = mDatas.get(i);
            final int finalI = i;

            views[i] = View.inflate(mContext, R.layout.diary_screening_view, null);
            screeningClick[i] = views[i].findViewById(R.id.diary_screening_click);
            screeningBg[i] = views[i].findViewById(R.id.diary_screening_bg);
            screeningText[i] = views[i].findViewById(R.id.diary_screening_text);

            screeningText[i].setText(data.getName());

            //点击事件
            screeningClick[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    isSelectedPos(finalI);
                    if (onClickCallBackListener != null) {
                        onClickCallBackListener.onClick(data.get_id());
                    }
                }
            });
        }

        isSelectedPos(0);

        //添加view
        for (View view : views) {
            addView(view);
        }

    }

    /**
     * 选中和未选中设置
     *
     * @param selectedPos：选中下标
     */
    private void isSelectedPos(int selectedPos) {
        //设置文字颜色
        for (int i = 0; i < screeningText.length; i++) {
            if (i == selectedPos) {
                screeningText[i].setTextColor(getResources().getColor(R.color.tab_tag));
            } else {
                screeningText[i].setTextColor(getResources().getColor(R.color._33));
            }
        }
        //设置背景颜色
        for (int i = 0; i < screeningBg.length; i++) {
            if (i == selectedPos) {
                screeningBg[i].setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab));
            } else {
                screeningBg[i].setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_diary_tab2));
            }
        }
    }

    //再写一页接口回调
    private OnClickCallBackListener onClickCallBackListener;

    public interface OnClickCallBackListener {
        void onClick(String id);
    }

    public void setOnClickCallBackListener(OnClickCallBackListener onClickCallBackListener) {
        this.onClickCallBackListener = onClickCallBackListener;
    }
}
