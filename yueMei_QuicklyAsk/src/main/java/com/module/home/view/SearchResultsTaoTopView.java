package com.module.home.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 搜索淘大促筛选
 * Created by 裴成浩 on 2019/9/6
 */
public class SearchResultsTaoTopView extends LinearLayout {

    private final Context mContext;
    private boolean leftSelected = false;               //左边是否选中
    private boolean rightSelected = false;              //右边是否选中
    private TextView textLeft;
    private TextView textRight;

    private List<SearchResultBoard> screen_active;      //要用到的数据


    public SearchResultsTaoTopView(Context context) {
        this(context, null);
    }

    public SearchResultsTaoTopView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchResultsTaoTopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    /**
     * 设置UI
     */
    public void initView(final List<SearchResultBoard> screen_active) {
        removeAllViews();
        this.screen_active = screen_active;
        setOrientation(HORIZONTAL);
        setPadding(Utils.dip2px(6), 0, Utils.dip2px(6), 0);

        LayoutParams layoutParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1;
        layoutParams.leftMargin = Utils.dip2px(8);
        layoutParams.rightMargin = Utils.dip2px(8);

        int size = screen_active.size();

        //左侧布局
        if (size >= 1) {
            SearchResultBoard searchResultBoard = screen_active.get(0);
            textLeft = new TextView(mContext);
            textLeft.setGravity(Gravity.CENTER);
            textLeft.setText(searchResultBoard.getTitle());
            textLeft.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff5c77));
            textLeft.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            textLeft.setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
            addView(textLeft, layoutParams);
            //左边点击
            textLeft.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (leftSelected) {
                        leftSelected = false;
                    } else {
                        leftSelected = true;
                    }
                    clickUiSwitch(leftSelected, textLeft, screen_active);
                }
            });
        }

        //右侧布局
        if (size >= 2) {
            SearchResultBoard searchResultBoard = screen_active.get(1);
            textRight = new TextView(mContext);
            textRight.setGravity(Gravity.CENTER);
            textRight.setText(searchResultBoard.getTitle());
            textRight.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff5c77));
            textRight.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            textRight.setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
            addView(textRight, layoutParams);
            //右边点击
            textRight.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rightSelected) {
                        rightSelected = false;
                    } else {
                        rightSelected = true;
                    }
                    clickUiSwitch(rightSelected, textRight, screen_active);
                }
            });
        }
    }

    /**
     * Ui切换
     *
     * @param isSelected
     * @param text
     */
    private void clickUiSwitch(boolean isSelected, TextView text, List<SearchResultBoard> screen_active) {
        if (isSelected) {
            Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.search_tao_top_selected);
            drawable.setBounds(0, 0, Utils.dip2px(14), Utils.dip2px(14));
            text.setCompoundDrawables(drawable, null, null, null);

            text.setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ffedf2);
        } else {
            text.setCompoundDrawables(null, null, null, null);
            text.setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
        }

        SearchResultBoard activeLeft = null;
        SearchResultBoard activeRight = null;
        if (screen_active.size() >= 2) {
            activeLeft = screen_active.get(0);
            activeRight = screen_active.get(1);
        } else if (screen_active.size() >= 1) {
            activeLeft = screen_active.get(0);
        }

        if (onEventClickListener != null) {
            if (leftSelected && rightSelected) {
                onEventClickListener.onEventClick(activeLeft, activeRight);
            } else if (leftSelected) {
                onEventClickListener.onEventClick(activeLeft, null);
            } else if (rightSelected) {
                onEventClickListener.onEventClick(null, activeRight);
            } else {
                onEventClickListener.onEventClick(null, null);
            }
        }
    }

    /**
     * 是否重新加载
     *
     * @return : true:重新加载
     */
    public boolean isReLoading(List<SearchResultBoard> data) {
        if (screen_active != null) {
            if (screen_active.size() > 0) {
                if (screen_active.size() == data.size()) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onEventClick(SearchResultBoard dataLeft, SearchResultBoard dataRight);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
