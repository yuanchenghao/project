package com.module.home.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.quicklyask.activity.R;


/**
 * 项目的加载等待框
 * Created by 裴成浩 on 2018/2/27.
 */

public class LoadingProgress extends Dialog {

    private String TAG = "LoadingProgress";
    private Activity mActivity;

    public LoadingProgress(Activity activity) {
        this(activity, R.style.MyDialog);
    }

    public LoadingProgress(Activity activity, int theme) {
        super(activity, theme);
        this.mActivity = activity;
    }


    private ImageButton colseBt;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_loading);
        colseBt = findViewById(R.id.colse_bt_aaa);

        colseBt.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                dismiss();
                return false;
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        this.dismiss();
        return true;
    }


    /**
     * 加载对话框显示
     */
    public void startLoading() {
        if (!mActivity.isFinishing()) {
            show();
        }
    }

    /**
     * 加载对话框消失
     */
    public void stopLoading() {
        Log.e(TAG, "mActivity.isFinishing() === " + mActivity.isFinishing());
        if (!mActivity.isFinishing()) {
            dismiss();
        }
    }
}
