package com.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.entity.VersionJCData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/17.
 */

public class VersionApi implements BaseCallBackApi {
    private String TAG = "VersionApi";
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.MESSAGE, "versions", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData.code === " + mData.code);
                Log.e(TAG, "mData === " + mData.toString());
                if ("1".equals(mData.code)){
                    try {
                        VersionJCData versionJCData = JSONUtil.TransformSingleBean(mData.data, VersionJCData.class);
                        listener.onSuccess(versionJCData);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
