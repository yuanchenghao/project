package com.module.commonview.broadcast;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.module.MainActivity;
import com.module.MyApplication;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;

import java.util.HashMap;
import java.util.List;

public class MessageService extends Service {

    private Toast toast;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null){
            String message = intent.getStringExtra("message");
            String clientid = intent.getStringExtra("clientid");
            String hosname = intent.getStringExtra("hosname");
            String hosimg = intent.getStringExtra("hosimg");
            String hosid = intent.getStringExtra("hosid");
            String pos = intent.getStringExtra("pos");
            if (TextUtils.isEmpty(pos)){
                pos="0";
            }
//            Log.e("MessageService",MyApplication.getInstance().getCurrentActivity().toString());
//            MessageToast.getInstance().init(MyApplication.getInstance().getCurrentActivity());
//            MessageToast.getInstance().createToast(hosname,message,hosimg,clientid,hosid,pos);
//            WLToast.getInstance(MyApplication.getInstance().getCurrentActivity()).show("我是一个粉刷匠");
            TopWindowUtils.show(MyApplication.getInstance().getCurrentActivity(),  hosname,message,hosimg,clientid,hosid,pos);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("hos_id",hosid);
            hashMap.put("event_others",pos);
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PUSH,"show"),hashMap);
        }

        return super.onStartCommand(intent, flags, startId);
    }




//


}
