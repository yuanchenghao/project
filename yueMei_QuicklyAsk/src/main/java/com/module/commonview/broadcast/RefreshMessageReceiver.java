package com.module.commonview.broadcast;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.module.MainTableActivity;
import com.module.commonview.activity.ChatActivity;
import com.module.commonview.module.bean.ChatListBean;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.home.fragment.XiaoxiFragment;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * 刷新未读消息数的广播
 * Created by 裴成浩 on 2018/1/12.
 */

public class RefreshMessageReceiver extends BroadcastReceiver {

    private MessageFragmentActivity1 messageFragmentActivity1;
    private String TAG = "RefreshMessageReceiver";
    private List<ChatListBean.ListBean> chatListBeen;
    private int mPos;
    private XiaoxiFragment xiaoxiFragment;
    private NotificationManager m_notificationMgr;
    private String mChannelID;


    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra("message");
        String time = intent.getStringExtra("time");
        String clientid = intent.getStringExtra("clientid");
        Log.e(TAG, "message === " + message);
        Log.e(TAG, "time === " + time);
        Log.e(TAG, "clientid === " + clientid);

        //设置Tab消息个数

        int newsNum = Cfg.loadInt(context,FinalConstant.ZONG_ID,0);
        if (Utils.isLogin() && Utils.isBind()) {
            MainTableActivity.mainBottomBar.setMessageNum(newsNum + 1);
        }
        if (messageFragmentActivity1 != null) {
            if (xiaoxiFragment != null) {
                if (xiaoxiFragment.messageAdapter == null)return;
                chatListBeen = xiaoxiFragment.messageAdapter.getmChatList();
            }

            for (int i = 0; i < chatListBeen.size(); i++) {
                String id = chatListBeen.get(i).getId();
                if (clientid.equals(id)) {
                    mPos = i;
                    break;
                }
            }
            Log.e(TAG, "mPos === " + mPos);

            //设置消息页面私信个数
            int mySixinNum = Cfg.loadInt(context,FinalConstant.SIXIN_ID,0);        //自己的私信
            Cfg.saveInt(context,FinalConstant.SIXIN_ID,mySixinNum + 1);
            messageFragmentActivity1.mBadgeCountList.set(0, mySixinNum + 1);
            messageFragmentActivity1.setUpTabBadge(0);


            //设置医院消息
            xiaoxiFragment.messageAdapter.setNewContent(mPos, message);
            //设置消息时间
            xiaoxiFragment.messageAdapter.setNewTime(mPos, time);
            //设置医院消息个数
            int noread = Integer.parseInt(chatListBeen.get(mPos).getNoread());
            xiaoxiFragment.messageAdapter.setNoread(mPos, (noread + 1) + "");

            xiaoxiFragment.messageAdapter.notifyDataSetChanged();

//            showNotifycation(context,message,clientid);
//            showCustomNotifycation(context);

        }
    }

    public void setMessageFragmentActivity1(MessageFragmentActivity1 messageFragmentActivity1) {
        this.messageFragmentActivity1 = messageFragmentActivity1;
        this.xiaoxiFragment = messageFragmentActivity1.xiaoxiFragment;
    }


    public void showNotifycation(Context context,String message,String clientid){
        m_notificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 26){
            mChannelID = "2";
            String channelName = "channel_name";
            NotificationChannel channel = new NotificationChannel(mChannelID, channelName, NotificationManager.IMPORTANCE_HIGH);
            m_notificationMgr.createNotificationChannel(channel);
        }
        Notification.Builder builder = new Notification.Builder(context);     //创建通知栏对象
        builder.setContentTitle("悦美");                //设置标题
        builder.setContentText(message);
        builder.setSmallIcon(R.drawable.ic_launcher);       //设置图标
        builder.setTicker("新消息");
        builder.setDefaults(Notification.DEFAULT_ALL);  //设置默认的提示音，振动方式，灯光
        builder.setAutoCancel(true);                    //打开程序后图标消失
        builder.setWhen(System.currentTimeMillis());
        if (Build.VERSION.SDK_INT >= 26){
            //创建通知时指定channelID
            builder.setChannelId(mChannelID);
        }
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("directId",clientid);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        Notification notification1 = builder.build();
        m_notificationMgr.notify(8, notification1); // 通过通知管理器发送通知

    }

    private void showCustomNotifycation(Context context){
        String channelId="3";
        String channlName="悦美微整形";

       NotificationManager notificationManager= (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channlName, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("通知渠道");
            notificationChannel.enableLights(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setTicker("悬浮通知");
        builder.setDefaults(0);
        builder.setAutoCancel(true);                    //打开程序后图标消失
        Notification notification = builder.build();

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.notifycation_message);
        remoteViews.setTextViewText(R.id.notify_title,"悦美微整形通知");
        remoteViews.setImageViewResource(R.id.notify_img,R.drawable.ic_launcher);
        Intent intent = new Intent(context, ChatActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//        remoteViews.setOnClickPendingIntent(R.id.notify_click,pendingIntent);
        builder.setFullScreenIntent(pendingIntent,false);
        notification.bigContentView=remoteViews;
        notificationManager.notify(6,notification);
    }

}
