package com.module.commonview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.module.other.module.bean.DocPopItemData;
import com.quicklyask.activity.R;

import java.util.List;


public class DocPopAdapter extends BaseAdapter {
    private final String TAG = "DocPopAdapter";
    private List<DocPopItemData> mDocPopItemData;
    private Context mContext;
    private LayoutInflater inflater;
    private DocPopItemData TaoPopItemData;
    private int curpo;
    TaoPopAdapter.ViewHolder viewHolder;


    public DocPopAdapter(Context mContext,
                         List<DocPopItemData> mTaoPopItemData, int curpo) {
        this.mContext = mContext;
        this.mDocPopItemData = mTaoPopItemData;
        this.curpo = curpo;
        inflater = LayoutInflater.from(mContext);
    }

    static class ViewHolder {
        public TextView mPart1NameTV;
        public RelativeLayout mRly;
    }

    @Override
    public int getCount() {
        return mDocPopItemData.size();
    }

    @Override
    public Object getItem(int position) {
        return mDocPopItemData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_tao_pop, null);
            viewHolder = new TaoPopAdapter.ViewHolder();

            viewHolder.mPart1NameTV = convertView.findViewById(R.id.pop_tao_item_name_tv);
            viewHolder.mRly = convertView.findViewById(R.id.top_city_rly);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TaoPopAdapter.ViewHolder) convertView.getTag();
        }

        TaoPopItemData = mDocPopItemData.get(position);
        String cityid = TaoPopItemData.get_id();
        if (cityid.equals("a")) {
            viewHolder.mPart1NameTV.setText(TaoPopItemData.getName());
            viewHolder.mPart1NameTV.setTextSize(14);
            viewHolder.mPart1NameTV.setTextColor(mContext.getResources()
                    .getColor(R.color.gary_person));
            viewHolder.mRly.setBackgroundResource(R.color.login_co);
        } else if (cityid.equals("b")) {
            viewHolder.mPart1NameTV.setText(TaoPopItemData.getName());
            viewHolder.mPart1NameTV.setTextSize(14);
            viewHolder.mPart1NameTV.setTextColor(mContext.getResources()
                    .getColor(R.color.gary_person));
            viewHolder.mRly.setBackgroundResource(R.drawable.layout_bt);
        } else {
            viewHolder.mPart1NameTV.setText(TaoPopItemData.getName());
            viewHolder.mRly.setBackgroundResource(R.drawable.layout_bt);
            viewHolder.mPart1NameTV.setTextSize(15);

            if (position == curpo) {
                viewHolder.mPart1NameTV.setTextColor(mContext.getResources().getColor(R.color.red_ff4965));
            } else {
                viewHolder.mPart1NameTV.setTextColor(mContext.getResources().getColor(R.color._33));
            }

        }

        return convertView;
    }

    public void add(List<DocPopItemData> infos) {
        mDocPopItemData.addAll(infos);
    }

    public void setNotifyCurpo(int curpo){
        this.curpo = curpo;
        notifyDataSetChanged();
    }
}
