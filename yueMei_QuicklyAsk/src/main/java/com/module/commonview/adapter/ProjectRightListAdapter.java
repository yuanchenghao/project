package com.module.commonview.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.MakeTagListData;
import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;
import com.quicklyask.view.FlowLayout;

import org.xutils.common.util.DensityUtil;

import java.util.List;

/**
 * pop项目选择右边适配
 * Created by 裴成浩 on 2018/1/25.
 */

public class ProjectRightListAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final List<MakeTagListData> mData;
    private final String TAG = "ProjectRightListAdapter";
    private final List<MakeTagData> mServerData;
    private final int mPos;
    private MakeTagListListData selectedData;
    private int mSelectedPos = 0;


    public ProjectRightListAdapter(Activity mActivity, List<MakeTagData> mServerData, int pos, MakeTagListListData selectedData) {
        this.mServerData = mServerData;
        this.mPos = pos;
        this.mData = mServerData.get(pos).getList();
        this.selectedData = selectedData;
        Log.e(TAG, "mData ==  " + mData.toString());
        Log.e(TAG, "mData ==  " + mData.size());
        inflater = LayoutInflater.from(mActivity);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_make_right1, null);
            holder.rlTitleClick = convertView.findViewById(R.id.rl_title_click);
            holder.tvRightTitle = convertView.findViewById(R.id.tv_right_title);
            holder.flowlayout = convertView.findViewById(R.id.flowlayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final MakeTagListData beanX = mData.get(pos);
        holder.tvRightTitle.setText(beanX.getName());            //设置上边的数据

        holder.rlTitleClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedData = null;
                if(beanX.getList().size() > 0){                                                //不是查看全部
                    onItemSelectedClickListener.onItemSelectedClick(beanX.getId(),beanX.getName());
                }else{
                    if(mPos>=0){
                        MakeTagData dataBean = mServerData.get(mPos);
                        onItemSelectedClickListener.onItemSelectedClick(dataBean.getId(),dataBean.getName());
                    }
                }
            }
        });


        setRightView(holder.flowlayout, beanX.getList(), pos);
        return convertView;
    }

    /**
     * 设置右边下边的标签
     *
     * @param flowlayout
     * @param list
     */
    private void setRightView(FlowLayout flowlayout, List<MakeTagListListData> list, int pos) {
        flowlayout.removeAllViews();

        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = DensityUtil.dip2px(4);
        lp.rightMargin = DensityUtil.dip2px(4);
        lp.topMargin = DensityUtil.dip2px(5);
        lp.bottomMargin = DensityUtil.dip2px(5);

        boolean isHave = false;

        for (int i = 0; i < list.size(); i++) {
            MakeTagListListData listBean = list.get(i);
            Log.e(TAG, "selectedData == " + selectedData);
            if(selectedData == null) {
                isHave = false;
            }else {
                Log.e(TAG, "listBean.getId() == " + listBean.getId());
                Log.e(TAG, "selectedData.getId() == " + selectedData.getId());
                if (listBean.getId().equals(selectedData.getId())) {                              //如果是当前的
                    isHave = true;
                    mSelectedPos = pos;
                } else {
                    isHave = false;
                }
            }

            View view = inflater.inflate(R.layout.item_make_right_below1, null);

            TextView fxContextYes = view.findViewById(R.id.fx_context_yes);

            TextView fxContextNo = view.findViewById(R.id.fx_context_no);

            Log.e(TAG, "isHave == " + isHave);
            if (isHave) {
                fxContextYes.setVisibility(View.VISIBLE);
                fxContextNo.setVisibility(View.GONE);
            } else {
                fxContextYes.setVisibility(View.GONE);
                fxContextNo.setVisibility(View.VISIBLE);
            }

            fxContextYes.setText(listBean.getName());
            fxContextNo.setText(listBean.getName());

            flowlayout.addView(view, lp);
            view.setTag(i);
            view.setOnClickListener(new onRightView(pos));
        }
    }


    class onRightView implements View.OnClickListener {
        private int mPos;
        public onRightView(int pos) {
            mPos = pos;
        }

        @Override
        public void onClick(View v) {
            boolean isHave = false; //是否是选中的
            int selected = (int) v.getTag();
            List<MakeTagListListData> list = mData.get(mPos).getList();
            for (int i = 0; i < list.size(); i++) {
                if (i == selected) {
                    TextView fxContextYes = v.findViewById(R.id.fx_context_yes);
                    TextView fxContextNo = v.findViewById(R.id.fx_context_no);

                    MakeTagListListData listBean = list.get(i);

                    if(selectedData == null){
                        isHave = false;
                    }else {
                        if (listBean.getId().equals(selectedData.getId())) {                              //如果是选中的
                            isHave = true;
                            mSelectedPos = mPos;
                        } else {
                            isHave = false;
                        }
                    }
                    if (!isHave) {
                        fxContextYes.setVisibility(View.VISIBLE);
                        fxContextNo.setVisibility(View.GONE);       //改为选中状态
                        selectedData = listBean;
                        break;
                    }
                }
            }
            onItemSelectedClickListener.onItemSelectedClick(selectedData.getId(),selectedData.getName());
        }
    }


    class ViewHolder {
        public RelativeLayout rlTitleClick;
        public TextView tvRightTitle;
        public FlowLayout flowlayout;
    }

    public int getmSelectedPos() {
        return mSelectedPos;
    }

    private OnItemSelectedClickListener onItemSelectedClickListener;

    public interface OnItemSelectedClickListener {
        void onItemSelectedClick(String id,String name);
    }

    public void setOnItemSelectedClickListener(OnItemSelectedClickListener onItemSelectedClickListener) {
        Log.e("null", "onItemSelectedClickListener222 == " + onItemSelectedClickListener);
        this.onItemSelectedClickListener = onItemSelectedClickListener;
    }
}