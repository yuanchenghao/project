package com.module.commonview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.FunctionManager;
import com.module.commonview.module.bean.VoteListBean;
import com.module.commonview.view.NiceImageView;
import com.module.event.VoteMsgEvent;
import com.quicklyask.activity.R;

import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * 文 件 名: VotePostContentAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-03 16:32
 * 邮   箱: 188897876@qq.com
 * 修改备注：帖子页里多个投票适配器
 */

public class VotePostContentAdapter extends EasyAdapter<VotePostContentAdapter.viewHolder1> {
    private Context mContext;
    private LayoutInflater inflater;
    private FunctionManager mFunctionManager;
    private String isVote = "0"; //未投0 已投1
    private VoteListBean voteListBean;
    private ArrayList<String> selectList;

    public VotePostContentAdapter(Context mContext, VoteListBean voteListBean, String isVote) {
        this.mContext = mContext;
        this.voteListBean = voteListBean;
        this.isVote = isVote;
        inflater = LayoutInflater.from(mContext);
//        mFunctionManager = new FunctionManager(mContext);
        selectList = new ArrayList<>();
    }


    private void setView1(viewHolder1 holder, final int i) {
        if (isVote.equals("0")) {
            holder.ll_type1.setVisibility(View.VISIBLE);
            holder.ll_type2.setVisibility(View.GONE);
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTao().getList_cover_image())) {
//                mFunctionManager.setRoundImageSrc(holder.iv_pic1, R.drawable.sall_null_2x, Utils.dip2px(5));
                Glide.with(mContext).load(R.drawable.sall_null_2x).into(holder.iv_pic1);
            } else {
//                mFunctionManager.setRoundImageSrc(holder.iv_pic1, voteListBean.getOption().get(i).getTao().getList_cover_image(), Utils.dip2px(5));
                Glide.with(mContext).load(voteListBean.getOption().get(i).getTao().getList_cover_image()).into(holder.iv_pic1);
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTao().getTitle())) {
                holder.tv_sku_title1.setText("");
            } else {
                holder.tv_sku_title1.setText(voteListBean.getOption().get(i).getTao().getTitle());
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTao().getSale_price())) {
                holder.tv_sku_price1.setText("");
            } else {
                holder.tv_sku_price1.setText(voteListBean.getOption().get(i).getTao().getSale_price());
            }
            holder.iv_select.setTag(i);
            holder.iv_pic1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new VoteMsgEvent(3, voteListBean.getOption().get(i).getTao_id()));
                }
            });
            holder.ll_content_type1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new VoteMsgEvent(3, voteListBean.getOption().get(i).getTao_id()));
                }
            });

        } else {
            holder.ll_type1.setVisibility(View.GONE);
            holder.ll_type2.setVisibility(View.VISIBLE);

            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTao().getList_cover_image())) {
//                mFunctionManager.setRoundImageSrc(holder.iv_pic2, R.drawable.sall_null_2x, Utils.dip2px(5));
                Glide.with(mContext).load(R.drawable.sall_null_2x).into(holder.iv_pic2);
            } else {
//                mFunctionManager.setRoundImageSrc(holder.iv_pic2, voteListBean.getOption().get(i).getTao().getList_cover_image(), Utils.dip2px(5));
                Glide.with(mContext).load(voteListBean.getOption().get(i).getTao().getList_cover_image()).into(holder.iv_pic2);
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTao().getTitle())) {
                holder.tv_sku_title2.setText("");
            } else {
                holder.tv_sku_title2.setText(voteListBean.getOption().get(i).getTao().getTitle());
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTao().getSale_price())) {
                holder.tv_sku_price2.setText("");
            } else {
                holder.tv_sku_price2.setText(voteListBean.getOption().get(i).getTao().getSale_price());
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getVote_option_rate())) {
                holder.tv_progress.setText("");
                holder.progress.setProgress(0);
            } else {
                holder.tv_progress.setText(new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(i).getVote_option_rate().trim()) * 100));
                holder.progress.setProgress((int) (Double.parseDouble(voteListBean.getOption().get(i).getVote_option_rate().trim()) * 100));
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getVote_num())) {
                holder.tv_num.setText("");
            } else {
                holder.tv_num.setText(voteListBean.getOption().get(i).getVote_num() + "票");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new VoteMsgEvent(3, voteListBean.getOption().get(i).getTao_id()));
                }
            });
        }

    }


    @NonNull
    @Override
    public viewHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new viewHolder1(inflater.inflate(R.layout.item1_vote_post, parent, false));
    }

    public String getSelectId() {
        selectList.clear();
        if (VotePostContentAdapter.this.getSelectMode() == SelectMode.SINGLE_SELECT) {
            if (VotePostContentAdapter.this.getSingleSelectedPosition() != -1) {
                if (!selectList.contains(voteListBean.getOption().get(VotePostContentAdapter.this.getSingleSelectedPosition()).getId())) {
                    selectList.add(voteListBean.getOption().get(VotePostContentAdapter.this.getSingleSelectedPosition()).getId());
                }
            }
        } else {
            for (int i = 0; i < voteListBean.getOption().size(); i++) {
                if (isSelected(i)) {
                    if (!selectList.contains(voteListBean.getOption().get(i).getId())) {
                        selectList.add(voteListBean.getOption().get(i).getId());
                    }
                }
            }
        }
        return (StringUtils.strip(selectList.toString(), "[]").trim()).replace(" ", "");
    }

    @Override
    public int getItemCount() {
        return voteListBean.getOption().size();
    }

    @Override
    public void whenBindViewHolder(viewHolder1 holder, int i) {
        setView1((viewHolder1) holder, i);
    }


    class viewHolder1 extends RecyclerView.ViewHolder {
        LinearLayout ll_type1;
        NiceImageView iv_pic1;
        TextView tv_sku_title1;
        TextView tv_sku_price1;
        ImageView iv_select;
        LinearLayout ll_content_type1;

        LinearLayout ll_type2;
        NiceImageView iv_pic2;
        TextView tv_sku_title2;
        TextView tv_sku_price2;
        TextView tv_progress;
        TextView tv_num;
        ProgressBar progress;

        public viewHolder1(View itemView) {
            super(itemView);
            ll_type1 = itemView.findViewById(R.id.ll_type1);
            iv_pic1 = itemView.findViewById(R.id.iv_pic1);
            tv_sku_title1 = itemView.findViewById(R.id.tv_sku_title1);
            tv_sku_price1 = itemView.findViewById(R.id.tv_sku_price1);
            iv_select = itemView.findViewById(R.id.iv_select);
            ll_content_type1 = itemView.findViewById(R.id.ll_content_type1);

            ll_type2 = itemView.findViewById(R.id.ll_type2);
            iv_pic2 = itemView.findViewById(R.id.iv_pic2);
            tv_sku_title2 = itemView.findViewById(R.id.tv_sku_title2);
            tv_sku_price2 = itemView.findViewById(R.id.tv_sku_price2);
            tv_progress = itemView.findViewById(R.id.tv_progress);
            tv_num = itemView.findViewById(R.id.tv_num);
            progress = itemView.findViewById(R.id.progress);
        }
    }

}
