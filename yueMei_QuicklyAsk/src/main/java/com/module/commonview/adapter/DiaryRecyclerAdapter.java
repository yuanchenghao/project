package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryListListData;
import com.module.commonview.module.bean.DiaryListListPic;
import com.module.commonview.view.Expression;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;


/**
 * Created by 裴成浩 on 2018/5/28.
 */
public class DiaryRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    private List<DiaryListListData> mDatas;
    private LayoutInflater mInflater;
    private final int windowsWight;         //屏幕宽度

    private final int ITEM_TYPE_ZERO = 0;
    private final int ITEM_TYPE_ONE = 1;
    private final int ITEM_TYPE_TWO = 2;
    private final int ITEM_TYPE_THREE = 3;
    private final int ITEM_TYPE_FOUR = 4;
    private final int ITEM_TYPE_FIVE = 5;
    private final int ITEM_TYPE_SIX = 6;
    private String TAG = "DiaryRecyclerAdapter";
    private static final String LIKE = "like";


    public DiaryRecyclerAdapter(Activity context, List<DiaryListListData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDatas.get(position).getPic().size()) {
            case 0:
                return ITEM_TYPE_ZERO;
            case 1:
                return ITEM_TYPE_ONE;
            case 2:
                return ITEM_TYPE_TWO;
            case 3:
                return ITEM_TYPE_THREE;
            case 4:
                return ITEM_TYPE_FOUR;
            case 5:
                return ITEM_TYPE_FIVE;
            case 6:
            default:
                return ITEM_TYPE_SIX;
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case ITEM_TYPE_ZERO:
                return new ViewHolder0(mInflater.inflate(R.layout.item_diary_list_recycler0, parent, false));
            case ITEM_TYPE_ONE:
                return new ViewHolder1(mInflater.inflate(R.layout.item_diary_list_recycler1, parent, false));
            case ITEM_TYPE_TWO:
                return new ViewHolder2(mInflater.inflate(R.layout.item_diary_list_recycler2, parent, false));
            case ITEM_TYPE_THREE:
                return new ViewHolder3(mInflater.inflate(R.layout.item_diary_list_recycler3, parent, false));
            case ITEM_TYPE_FOUR:
                return new ViewHolder4(mInflater.inflate(R.layout.item_diary_list_recycler4, parent, false));
            case ITEM_TYPE_FIVE:
                return new ViewHolder5(mInflater.inflate(R.layout.item_diary_list_recycler5, parent, false));
            case ITEM_TYPE_SIX:
            default:
                return new ViewHolder6(mInflater.inflate(R.layout.item_diary_list_recycler6, parent, false));
        }

    }

    /**
     * 局部刷新
     *
     * @param holder
     * @param position
     * @param payloads
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {

            Log.e(TAG, "payloads == " + payloads.size());
            Log.e(TAG, "position == " + position);

            if (holder instanceof ViewHolder0) {
                ViewHolder0 viewHolder0 = (ViewHolder0) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder0.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder0.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder0.mPraiseNum.setVisibility(View.GONE);
                            }

                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder0.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder0.mPraiseImg);
                            }

                            break;
                    }

                }

            } else if (holder instanceof ViewHolder1) {
                ViewHolder1 viewHolder1 = (ViewHolder1) holder;
                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder1.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder1.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder1.mPraiseNum.setVisibility(View.GONE);
                            }

                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder1.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder1.mPraiseImg);
                            }
                            break;
                    }
                }

            } else if (holder instanceof ViewHolder2) {
                ViewHolder2 viewHolder2 = (ViewHolder2) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder2.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder2.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder2.mPraiseNum.setVisibility(View.GONE);
                            }

                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder2.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder2.mPraiseImg);
                            }

                            break;
                    }
                }

            } else if (holder instanceof ViewHolder3) {
                ViewHolder3 viewHolder3 = (ViewHolder3) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder3.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder3.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder3.mPraiseNum.setVisibility(View.GONE);
                            }
                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder3.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder3.mPraiseImg);
                            }

                            break;
                    }
                }

            } else if (holder instanceof ViewHolder4) {
                ViewHolder4 viewHolder4 = (ViewHolder4) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder4.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder4.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder4.mPraiseNum.setVisibility(View.GONE);
                            }

                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder4.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder4.mPraiseImg);
                            }

                            break;
                    }
                }

            } else if (holder instanceof ViewHolder5) {
                ViewHolder5 viewHolder5 = (ViewHolder5) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder5.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder5.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder5.mPraiseNum.setVisibility(View.GONE);
                            }

                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder5.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder5.mPraiseImg);
                            }

                            break;
                    }
                }

            } else if (holder instanceof ViewHolder6) {
                ViewHolder6 viewHolder6 = (ViewHolder6) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case LIKE:
                            int agreeNum = Integer.parseInt(mDatas.get(position).getAgree_num());
                            if (agreeNum > 0) {
                                viewHolder6.mPraiseNum.setVisibility(View.VISIBLE);
                                viewHolder6.mPraiseNum.setText(mDatas.get(position).getAgree_num());
                            } else {
                                viewHolder6.mPraiseNum.setVisibility(View.GONE);
                            }

                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(viewHolder6.mPraiseImg);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(viewHolder6.mPraiseImg);
                            }

                            break;
                    }
                }
            }

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder0) {

            setImgView0((ViewHolder0) holder, position);

        } else if (holder instanceof ViewHolder1) {
            setImgView1((ViewHolder1) holder, position);

        } else if (holder instanceof ViewHolder2) {
            setImgView2((ViewHolder2) holder, position);
        } else if (holder instanceof ViewHolder3) {
            setImgView3((ViewHolder3) holder, position);

        } else if (holder instanceof ViewHolder4) {
            setImgView4((ViewHolder4) holder, position);

        } else if (holder instanceof ViewHolder5) {
            setImgView5((ViewHolder5) holder, position);

        } else if (holder instanceof ViewHolder6) {
            setImgView6((ViewHolder6) holder, position);

        }
    }

    /**
     * 没有图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView0(ViewHolder0 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();

        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }

        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(10));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }
    }


    /**
     * 一张图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView1(ViewHolder1 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();
        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }

        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }

        List<DiaryListListPic> pic = listData.getPic();
        Log.e(TAG, "pic.get(0).getImg() === " + pic.get(0).getImg());

        if ("diary_list_video_was_not_approved".equals(pic.get(0).getImg())) {
            Glide.with(mContext).load(R.drawable.diary_list_video_was_not_approved).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto1Img1);
        } else {
            Glide.with(mContext).load(pic.get(0).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto1Img1);
        }
        if ("1".equals(pic.get(0).getIs_video())) {
            holder.phpto1Video1.setVisibility(View.VISIBLE);
        } else {
            holder.phpto1Video1.setVisibility(View.GONE);
        }
    }

    /**
     * 两张图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView2(ViewHolder2 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();

        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }

        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }

        List<DiaryListListPic> pic = listData.getPic();

        Glide.with(mContext).load(pic.get(0).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto2Img1);
        Glide.with(mContext).load(pic.get(1).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto2Img2);


        if ("1".equals(pic.get(0).getIs_video())) {
            holder.phpto2Video1.setVisibility(View.VISIBLE);
        } else {
            holder.phpto2Video1.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(1).getIs_video())) {
            holder.phpto2Video2.setVisibility(View.VISIBLE);
        } else {
            holder.phpto2Video2.setVisibility(View.GONE);
        }


        float remainingWight = windowsWight - Utils.dip2px(94);      //剩余宽度
        float ImgWight = remainingWight / 2;

        ViewGroup.LayoutParams layoutParams1 = holder.phpto2Click1.getLayoutParams();
        layoutParams1.height = (int) ImgWight;
        holder.phpto2Click1.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = holder.phpto2Click2.getLayoutParams();
        layoutParams2.height = (int) ImgWight;
        holder.phpto2Click2.setLayoutParams(layoutParams2);
    }

    /**
     * 三张图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView3(ViewHolder3 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();

        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }
        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }

        List<DiaryListListPic> pic = listData.getPic();

        Glide.with(mContext).load(pic.get(0).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto3Img1);
        Glide.with(mContext).load(pic.get(1).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto3Img2);
        Glide.with(mContext).load(pic.get(2).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto3Img3);

        if ("1".equals(pic.get(0).getIs_video())) {
            holder.phpto3Video1.setVisibility(View.VISIBLE);
        } else {
            holder.phpto3Video1.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(1).getIs_video())) {
            holder.phpto3Video2.setVisibility(View.VISIBLE);
        } else {
            holder.phpto3Video2.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(2).getIs_video())) {
            holder.phpto3Video3.setVisibility(View.VISIBLE);
        } else {
            holder.phpto3Video3.setVisibility(View.GONE);
        }

        float remainingWight = windowsWight - Utils.dip2px(94);      //剩余宽度

        float leftImgWight = remainingWight / (189 + 92) * 189;
        float rightImgWight = remainingWight / (189 + 92) * 92;

        ViewGroup.LayoutParams layoutParams1 = holder.phpto3Click1.getLayoutParams();
        layoutParams1.height = (int) leftImgWight;
        holder.phpto3Click1.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = holder.phpto3Click2.getLayoutParams();
        layoutParams2.height = (int) rightImgWight;
        holder.phpto3Click2.setLayoutParams(layoutParams2);
        ViewGroup.LayoutParams layoutParams3 = holder.phpto3Click3.getLayoutParams();
        layoutParams3.height = (int) rightImgWight;
        holder.phpto3Click3.setLayoutParams(layoutParams3);

    }

    /**
     * 四张图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView4(ViewHolder4 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();

        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }

        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }

        List<DiaryListListPic> pic = listData.getPic();

        Glide.with(mContext).load(pic.get(0).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto4Img1);
        Glide.with(mContext).load(pic.get(1).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto4Img2);
        Glide.with(mContext).load(pic.get(2).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto4Img3);
        Glide.with(mContext).load(pic.get(3).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto4Img4);

        if ("1".equals(pic.get(0).getIs_video())) {
            holder.phpto4Video1.setVisibility(View.VISIBLE);
        } else {
            holder.phpto4Video1.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(1).getIs_video())) {
            holder.phpto4Video2.setVisibility(View.VISIBLE);
        } else {
            holder.phpto4Video2.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(2).getIs_video())) {
            holder.phpto4Video3.setVisibility(View.VISIBLE);
        } else {
            holder.phpto4Video3.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(3).getIs_video())) {
            holder.phpto4Video4.setVisibility(View.VISIBLE);
        } else {
            holder.phpto4Video4.setVisibility(View.GONE);
        }

        float remainingWight = windowsWight - Utils.dip2px(94);      //剩余宽度
        float ImgWight = remainingWight / 2;

        ViewGroup.LayoutParams layoutParams1 = holder.phpto4Click1.getLayoutParams();
        layoutParams1.height = (int) ImgWight;
        holder.phpto4Click1.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = holder.phpto4Click2.getLayoutParams();
        layoutParams2.height = (int) ImgWight;
        holder.phpto4Click2.setLayoutParams(layoutParams2);
        ViewGroup.LayoutParams layoutParams3 = holder.phpto4Click3.getLayoutParams();
        layoutParams3.height = (int) ImgWight;
        holder.phpto4Click3.setLayoutParams(layoutParams3);
        ViewGroup.LayoutParams layoutParams4 = holder.phpto4Click4.getLayoutParams();
        layoutParams4.height = (int) ImgWight;
        holder.phpto4Click4.setLayoutParams(layoutParams4);
    }

    /**
     * 五张图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView5(ViewHolder5 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();

        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }

        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }

        List<DiaryListListPic> pic = listData.getPic();

        Glide.with(mContext).load(pic.get(0).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto5Img1);
        Glide.with(mContext).load(pic.get(1).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto5Img2);
        Glide.with(mContext).load(pic.get(2).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto5Img3);
        Glide.with(mContext).load(pic.get(3).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto5Img4);
        Glide.with(mContext).load(pic.get(4).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto5Img5);

        if ("1".equals(pic.get(0).getIs_video())) {
            holder.phpto5Video1.setVisibility(View.VISIBLE);
        } else {
            holder.phpto5Video1.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(1).getIs_video())) {
            holder.phpto5Video2.setVisibility(View.VISIBLE);
        } else {
            holder.phpto5Video2.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(2).getIs_video())) {
            holder.phpto5Video3.setVisibility(View.VISIBLE);
        } else {
            holder.phpto5Video3.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(3).getIs_video())) {
            holder.phpto5Video4.setVisibility(View.VISIBLE);
        } else {
            holder.phpto5Video4.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(4).getIs_video())) {
            holder.phpto5Video5.setVisibility(View.VISIBLE);
        } else {
            holder.phpto5Video5.setVisibility(View.GONE);
        }

        float remainingWight = windowsWight - Utils.dip2px(94);      //剩余宽度
        float topImgWight = remainingWight / 2;

        float remainingWight1 = windowsWight - Utils.dip2px(104);      //剩余宽度
        float bottomImgWight = remainingWight1 / 3;

        Log.e(TAG, "remainingWight == " + remainingWight);
        Log.e(TAG, "topImgWight == " + topImgWight);
        Log.e(TAG, "remainingWight1 == " + remainingWight1);
        Log.e(TAG, "bottomImgWight == " + bottomImgWight);

        ViewGroup.LayoutParams layoutParams1 = holder.phpto5Click1.getLayoutParams();
        layoutParams1.height = (int) topImgWight;
        holder.phpto5Click1.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = holder.phpto5Click2.getLayoutParams();
        layoutParams2.height = (int) topImgWight;
        holder.phpto5Click2.setLayoutParams(layoutParams2);
        ViewGroup.LayoutParams layoutParams3 = holder.phpto5Click3.getLayoutParams();
        layoutParams3.height = (int) bottomImgWight;
        holder.phpto5Click3.setLayoutParams(layoutParams3);
        ViewGroup.LayoutParams layoutParams4 = holder.phpto5Click4.getLayoutParams();
        layoutParams4.height = (int) bottomImgWight;
        holder.phpto5Click4.setLayoutParams(layoutParams4);
        ViewGroup.LayoutParams layoutParams5 = holder.phpto5Click5.getLayoutParams();
        layoutParams5.height = (int) bottomImgWight;
        holder.phpto5Click5.setLayoutParams(layoutParams5);
    }

    /**
     * 六张及以上图片
     *
     * @param holder
     * @param position
     */
    @SuppressLint("SetTextI18n")
    public void setImgView6(ViewHolder6 holder, final int position) {
        DiaryListListData listData = mDatas.get(position);
        String day = listData.getDay();
        String cycle = listData.getCycle();

        //术后天数
        if (Integer.parseInt(day) > 0) {
            String dayNumber = "术后" + day + "天";
            String titleText = dayNumber + cycle;
            SpannableString textSpanned = new SpannableString(titleText);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff4965)), 2, 2 + day.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textSpanned.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color._33)), dayNumber.length(), titleText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mTitle.setText(textSpanned);
        } else {
            holder.mTitle.setText(listData.getDay_title());
        }

        //日记顺序
        String diary_number = listData.getDiary_number();
        if (!TextUtils.isEmpty(diary_number)) {
            holder.mDiaryNumber.setText(diary_number);
        }

        try {
            Log.e(TAG, "data.getContent() == " + listData.getContent());
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(listData.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //点赞数
        if (Integer.parseInt(listData.getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(listData.getAgree_num());
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图
        if ("1".equals(mDatas.get(position).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseImg);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseImg);
        }

        //删除
        if (isOneselfDiary(position)) {
            holder.mPraiseDelete.setVisibility(View.VISIBLE);
            holder.mPraiseDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                    diariesDeleteData.setId(mDatas.get(position).getId());
                    diariesDeleteData.setReplystr("0");
                    diariesDeleteData.setPos(position);
                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });
        } else {
            holder.mPraiseDelete.setVisibility(View.GONE);
        }

        List<DiaryListListPic> pic = listData.getPic();
        if (pic.size() > 6) {
            holder.imgNum.setText(listData.getHave_pic());
            holder.imgNum.setVisibility(View.VISIBLE);
        } else {
            holder.imgNum.setVisibility(View.GONE);
        }

        Glide.with(mContext).load(pic.get(0).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto6Img1);
        Glide.with(mContext).load(pic.get(1).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto6Img2);
        Glide.with(mContext).load(pic.get(2).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto6Img3);
        Glide.with(mContext).load(pic.get(3).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto6Img4);
        Glide.with(mContext).load(pic.get(4).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto6Img5);
        Glide.with(mContext).load(pic.get(5).getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(holder.phpto6Img6);

        if ("1".equals(pic.get(0).getIs_video())) {
            holder.phpto6Video1.setVisibility(View.VISIBLE);
        } else {
            holder.phpto6Video1.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(1).getIs_video())) {
            holder.phpto6Video2.setVisibility(View.VISIBLE);
        } else {
            holder.phpto6Video2.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(2).getIs_video())) {
            holder.phpto6Video3.setVisibility(View.VISIBLE);
        } else {
            holder.phpto6Video3.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(3).getIs_video())) {
            holder.phpto6Video4.setVisibility(View.VISIBLE);
        } else {
            holder.phpto6Video4.setVisibility(View.GONE);
        }
        if ("1".equals(pic.get(4).getIs_video())) {
            holder.phpto6Video5.setVisibility(View.VISIBLE);
        } else {
            holder.phpto6Video5.setVisibility(View.GONE);
        }

        if ("1".equals(pic.get(5).getIs_video())) {
            holder.phpto6Video6.setVisibility(View.VISIBLE);
        } else {
            holder.phpto6Video6.setVisibility(View.GONE);
        }

        float remainingWight = windowsWight - Utils.dip2px(94);      //剩余宽度

        float leftAndTopImgWight = remainingWight / (189 + 92) * 189;
        float topRightImgWight = (leftAndTopImgWight - Utils.dip2px(5)) / 2;
        float rightAndBottomImgWight = remainingWight / (189 + 92) * 92;

        Log.e(TAG, "windowsWight == " + windowsWight);
        Log.e(TAG, " Utils.dip2px(94) == " + Utils.dip2px(94));
        Log.e(TAG, " leftAndTopImgWight == " + leftAndTopImgWight);
        Log.e(TAG, " topRightImgWight == " + topRightImgWight);
        Log.e(TAG, " rightAndBottomImgWight == " + rightAndBottomImgWight);

        ViewGroup.LayoutParams layoutParams1 = holder.phpto6Click1.getLayoutParams();
        layoutParams1.height = (int) leftAndTopImgWight;
        holder.phpto6Click1.setLayoutParams(layoutParams1);

        ViewGroup.LayoutParams layoutParams2 = holder.phpto6Click2.getLayoutParams();
        layoutParams2.height = (int) topRightImgWight;
        holder.phpto6Click2.setLayoutParams(layoutParams2);

        ViewGroup.LayoutParams layoutParams3 = holder.phpto6Click3.getLayoutParams();
        layoutParams3.height = (int) topRightImgWight;
        holder.phpto6Click3.setLayoutParams(layoutParams3);

        ViewGroup.LayoutParams layoutParams4 = holder.phpto6Click4.getLayoutParams();
        layoutParams4.height = (int) rightAndBottomImgWight;
        holder.phpto6Click4.setLayoutParams(layoutParams4);

        ViewGroup.LayoutParams layoutParams5 = holder.phpto6Click5.getLayoutParams();
        layoutParams5.height = (int) rightAndBottomImgWight;
        holder.phpto6Click5.setLayoutParams(layoutParams5);

        ViewGroup.LayoutParams layoutParams6 = holder.phpto6Click6.getLayoutParams();
        layoutParams6.height = (int) rightAndBottomImgWight;
        holder.phpto6Click6.setLayoutParams(layoutParams6);
    }

    /**
     * 删除某一条数据
     *
     * @param pos
     */
    public void deleteItem(int pos) {
        mDatas.remove(pos);             //删除数据源
        notifyItemRemoved(pos);         //刷新被删除的地方
        notifyItemRangeChanged(pos, getItemCount()); //刷新被删除数据，以及其后面的数据
    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数

        ViewHolder0(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });
        }
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数
        FrameLayout phpto1Click1;
        ImageView phpto1Video1;
        ImageView phpto1Img1;

        ViewHolder1(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);
            phpto1Click1 = itemView.findViewById(R.id.diary_list_phpto1_click1);
            phpto1Video1 = itemView.findViewById(R.id.diary_list_phpto1_video1);
            phpto1Img1 = itemView.findViewById(R.id.diary_list_phpto1_img1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            //图片点击
            phpto1Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 0);
                }
            });
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数
        FrameLayout phpto2Click1;
        FrameLayout phpto2Click2;
        ImageView phpto2Video1;
        ImageView phpto2Video2;
        ImageView phpto2Img1;
        ImageView phpto2Img2;

        ViewHolder2(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);
            phpto2Click1 = itemView.findViewById(R.id.diary_list_phpto2_click1);
            phpto2Click2 = itemView.findViewById(R.id.diary_list_phpto2_click2);
            phpto2Video1 = itemView.findViewById(R.id.diary_list_phpto2_video1);
            phpto2Video2 = itemView.findViewById(R.id.diary_list_phpto2_video2);
            phpto2Img1 = itemView.findViewById(R.id.diary_list_phpto2_img1);
            phpto2Img2 = itemView.findViewById(R.id.diary_list_phpto2_img2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            phpto2Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 0);
                }
            });

            phpto2Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 1);
                }
            });
        }
    }

    public class ViewHolder3 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数
        FrameLayout phpto3Click1;
        FrameLayout phpto3Click2;
        FrameLayout phpto3Click3;
        ImageView phpto3Video1;
        ImageView phpto3Video2;
        ImageView phpto3Video3;
        ImageView phpto3Img1;
        ImageView phpto3Img2;
        ImageView phpto3Img3;

        ViewHolder3(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);
            phpto3Click1 = itemView.findViewById(R.id.diary_list_phpto3_click1);
            phpto3Click2 = itemView.findViewById(R.id.diary_list_phpto3_click2);
            phpto3Click3 = itemView.findViewById(R.id.diary_list_phpto3_click3);
            phpto3Video1 = itemView.findViewById(R.id.diary_list_phpto3_video1);
            phpto3Video2 = itemView.findViewById(R.id.diary_list_phpto3_video2);
            phpto3Video3 = itemView.findViewById(R.id.diary_list_phpto3_video3);
            phpto3Img1 = itemView.findViewById(R.id.diary_list_phpto3_img1);
            phpto3Img2 = itemView.findViewById(R.id.diary_list_phpto3_img2);
            phpto3Img3 = itemView.findViewById(R.id.diary_list_phpto3_img3);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            phpto3Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 0);
                }
            });
            phpto3Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 1);
                }
            });
            phpto3Click3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 2);
                }
            });
        }
    }

    public class ViewHolder4 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数

        FrameLayout phpto4Click1;
        FrameLayout phpto4Click2;
        FrameLayout phpto4Click3;
        FrameLayout phpto4Click4;
        ImageView phpto4Video1;
        ImageView phpto4Video2;
        ImageView phpto4Video3;
        ImageView phpto4Video4;

        ImageView phpto4Img1;
        ImageView phpto4Img2;
        ImageView phpto4Img3;
        ImageView phpto4Img4;

        ViewHolder4(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);
            phpto4Click1 = itemView.findViewById(R.id.diary_list_phpto4_click1);
            phpto4Click2 = itemView.findViewById(R.id.diary_list_phpto4_click2);
            phpto4Click3 = itemView.findViewById(R.id.diary_list_phpto4_click3);
            phpto4Click4 = itemView.findViewById(R.id.diary_list_phpto4_click4);
            phpto4Video1 = itemView.findViewById(R.id.diary_list_phpto4_video1);
            phpto4Video2 = itemView.findViewById(R.id.diary_list_phpto4_video2);
            phpto4Video3 = itemView.findViewById(R.id.diary_list_phpto4_video3);
            phpto4Video4 = itemView.findViewById(R.id.diary_list_phpto4_video4);
            phpto4Img1 = itemView.findViewById(R.id.diary_list_phpto4_img1);
            phpto4Img2 = itemView.findViewById(R.id.diary_list_phpto4_img2);
            phpto4Img3 = itemView.findViewById(R.id.diary_list_phpto4_img3);
            phpto4Img4 = itemView.findViewById(R.id.diary_list_phpto4_img4);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            phpto4Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 0);
                }
            });
            phpto4Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 1);
                }
            });
            phpto4Click3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 2);
                }
            });
            phpto4Click4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 3);
                }
            });

        }
    }

    public class ViewHolder5 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数

        FrameLayout phpto5Click1;
        FrameLayout phpto5Click2;
        FrameLayout phpto5Click3;
        FrameLayout phpto5Click4;
        FrameLayout phpto5Click5;
        ImageView phpto5Video1;
        ImageView phpto5Video2;
        ImageView phpto5Video3;
        ImageView phpto5Video4;
        ImageView phpto5Video5;

        ImageView phpto5Img1;
        ImageView phpto5Img2;
        ImageView phpto5Img3;
        ImageView phpto5Img4;
        ImageView phpto5Img5;

        ViewHolder5(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);
            phpto5Click1 = itemView.findViewById(R.id.diary_list_phpto5_click1);
            phpto5Click2 = itemView.findViewById(R.id.diary_list_phpto5_click2);
            phpto5Click3 = itemView.findViewById(R.id.diary_list_phpto5_click3);
            phpto5Click4 = itemView.findViewById(R.id.diary_list_phpto5_click4);
            phpto5Click5 = itemView.findViewById(R.id.diary_list_phpto5_click5);
            phpto5Video1 = itemView.findViewById(R.id.diary_list_phpto5_video1);
            phpto5Video2 = itemView.findViewById(R.id.diary_list_phpto5_video2);
            phpto5Video3 = itemView.findViewById(R.id.diary_list_phpto5_video3);
            phpto5Video4 = itemView.findViewById(R.id.diary_list_phpto5_video4);
            phpto5Video5 = itemView.findViewById(R.id.diary_list_phpto5_video5);
            phpto5Img1 = itemView.findViewById(R.id.diary_list_phpto5_img1);
            phpto5Img2 = itemView.findViewById(R.id.diary_list_phpto5_img2);
            phpto5Img3 = itemView.findViewById(R.id.diary_list_phpto5_img3);
            phpto5Img4 = itemView.findViewById(R.id.diary_list_phpto5_img4);
            phpto5Img5 = itemView.findViewById(R.id.diary_list_phpto5_img5);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            phpto5Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 0);
                }
            });
            phpto5Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 1);
                }
            });
            phpto5Click3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 2);
                }
            });
            phpto5Click4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 3);
                }
            });
            phpto5Click5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 4);
                }
            });

        }
    }

    public class ViewHolder6 extends RecyclerView.ViewHolder {
        TextView mTitle;                    //术后天数
        TextView mDiaryNumber;              //术后日记数
        TextView mContent;                  //术后文案
        LinearLayout mPraise;               //删除和点赞容器
        TextView mPraiseDelete;             //删除
        ImageView mPraiseImg;               //点赞图
        TextView mPraiseNum;                //点赞数
        FrameLayout phpto6Click1;
        FrameLayout phpto6Click2;
        FrameLayout phpto6Click3;
        FrameLayout phpto6Click4;
        FrameLayout phpto6Click5;
        FrameLayout phpto6Click6;
        ImageView phpto6Video1;
        ImageView phpto6Video2;
        ImageView phpto6Video3;
        ImageView phpto6Video4;
        ImageView phpto6Video5;
        ImageView phpto6Video6;
        ImageView phpto6Img1;
        ImageView phpto6Img2;
        ImageView phpto6Img3;
        ImageView phpto6Img4;
        ImageView phpto6Img5;
        ImageView phpto6Img6;
        TextView imgNum;                    //图片数

        ViewHolder6(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycler_title);
            mDiaryNumber = itemView.findViewById(R.id.item_diary_list_recycler_diary);
            mContent = itemView.findViewById(R.id.item_diary_list_recycler_content);
            mPraise = itemView.findViewById(R.id.item_diary_list_recycler_praise);
            mPraiseDelete = itemView.findViewById(R.id.item_diary_list_recycler_praise_delete);
            mPraiseImg = itemView.findViewById(R.id.item_diary_list_recycler_praise_img);
            mPraiseNum = itemView.findViewById(R.id.item_diary_list_recycler_praise_num);
            phpto6Click1 = itemView.findViewById(R.id.diary_list_phpto6_click1);
            phpto6Click2 = itemView.findViewById(R.id.diary_list_phpto6_click2);
            phpto6Click3 = itemView.findViewById(R.id.diary_list_phpto6_click3);
            phpto6Click5 = itemView.findViewById(R.id.diary_list_phpto6_click5);
            phpto6Click6 = itemView.findViewById(R.id.diary_list_phpto6_click6);
            phpto6Click4 = itemView.findViewById(R.id.diary_list_phpto6_click4);
            phpto6Video1 = itemView.findViewById(R.id.diary_list_phpto6_video1);
            phpto6Video2 = itemView.findViewById(R.id.diary_list_phpto6_video2);
            phpto6Video3 = itemView.findViewById(R.id.diary_list_phpto6_video3);
            phpto6Video4 = itemView.findViewById(R.id.diary_list_phpto6_video4);
            phpto6Video5 = itemView.findViewById(R.id.diary_list_phpto6_video5);
            phpto6Video6 = itemView.findViewById(R.id.diary_list_phpto6_video6);
            phpto6Img1 = itemView.findViewById(R.id.diary_list_phpto6_img1);
            phpto6Img2 = itemView.findViewById(R.id.diary_list_phpto6_img2);
            phpto6Img3 = itemView.findViewById(R.id.diary_list_phpto6_img3);
            phpto6Img4 = itemView.findViewById(R.id.diary_list_phpto6_img4);
            phpto6Img5 = itemView.findViewById(R.id.diary_list_phpto6_img5);
            phpto6Img6 = itemView.findViewById(R.id.diary_list_phpto6_img6);

            imgNum = itemView.findViewById(R.id.diary_list_phpto6_img_num);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //点赞
            mPraise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setIs_reply("0");
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            phpto6Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 0);
                }
            });
            phpto6Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 1);
                }
            });
            phpto6Img3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 2);
                }
            });
            phpto6Click4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 3);
                }
            });
            phpto6Click5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 4);
                }
            });
            phpto6Click6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickListener.onItemImgClick(v, getLayoutPosition(), 5);
                }
            });

        }
    }

    /**
     * 判断是否是自己的日记
     *
     * @param position
     * @return
     */
    private boolean isOneselfDiary(int position) {
        return Utils.getUid().equals(mDatas.get(position).getUser_id());
    }

    public List<DiaryListListData> getmDatas() {
        return mDatas;
    }

    public void addDatas(List<DiaryListListData> mData) {
        mDatas.addAll(mData);
        notifyDataSetChanged();
    }

    /**
     * 添加一条数据
     *
     * @param data
     */
    public void addData(DiaryListListData data) {
        mDatas.add(0, data);
        notifyItemInserted(0);
    }

    public void removeDatas(List<DiaryListListData> mData) {
        mDatas.clear();
        mDatas.addAll(mData);
        notifyDataSetChanged();
    }


    public interface OnEventClickListener {
        void onItemClick(View v, int pos);                      //item点击回调

        void onItemLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemImgClick(View v, int pos, int imgPos);       //图片点击回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
