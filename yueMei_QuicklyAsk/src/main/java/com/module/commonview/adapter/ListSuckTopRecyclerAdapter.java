package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/11.
 */
public class ListSuckTopRecyclerAdapter extends RecyclerView.Adapter<ListSuckTopRecyclerAdapter.ViewHolder> {

    private final int windowsWight;         //屏幕宽度
    private Activity mContext;
    private List<DiaryTaoData> mDatas;
    private final LayoutInflater mInflater;
    private String TAG = "ListSuckTopRecyclerAdapter";

    public ListSuckTopRecyclerAdapter(Activity context, List<DiaryTaoData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_diary_list_sku_top, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DiaryTaoData taoData = mDatas.get(position);

        Log.e(TAG, "position == " + position);
        Glide.with(mContext)
                .load(taoData.getList_cover_image())
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                .into(holder.mImg);
        holder.mTitle.setText(taoData.getTitle());               //文案
        holder.mPrice.setText("¥" + taoData.getPrice_discount());      //价格

        String member_price = taoData.getMember_price();
        int i = Integer.parseInt(member_price);
        if (i >= 0) {
            holder.mPlusVisibity.setVisibility(View.VISIBLE);
            holder.mPlusPrice.setText("¥" + member_price);
        } else {
            holder.mPlusVisibity.setVisibility(View.GONE);
        }
        ViewGroup.LayoutParams layoutParams = holder.suckTop.getLayoutParams();
        if (mDatas.size() > 1) {
            layoutParams.width = windowsWight / 3 * 2;
        } else {
            layoutParams.width = windowsWight;
        }
        holder.suckTop.setLayoutParams(layoutParams);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout suckTop;
        ImageView mImg;
        TextView mTitle;
        TextView mPrice;
        LinearLayout mPlusVisibity;
        TextView mPlusPrice;
        Button mToChat;

        public ViewHolder(View itemView) {
            super(itemView);
            suckTop = itemView.findViewById(R.id.item_diary_list_suck_top);
            mImg = itemView.findViewById(R.id.diary_list_suck_top_img);
            mTitle = itemView.findViewById(R.id.diary_list_suck_top_title);
            mPrice = itemView.findViewById(R.id.diary_list_suck_top_price);
            mPlusVisibity = itemView.findViewById(R.id.tao_plus_vibility);
            mPlusPrice = itemView.findViewById(R.id.tao_plus_price);
            mToChat = itemView.findViewById(R.id.diary_list_suck_top_btn);

            //item点击
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onItemClick(v, mDatas.get(getLayoutPosition()));
                    }
                }
            });

            //咨询点击
            mToChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onChatClick(v, mDatas.get(getLayoutPosition()));
                    }
                }
            });

        }
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, DiaryTaoData data);

        void onChatClick(View v, DiaryTaoData data);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}