package com.module.commonview.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.QuickQuestion;
import com.quicklyask.activity.R;

import java.util.List;

public class ChatTipListAdapter extends BaseQuickAdapter<QuickQuestion, BaseViewHolder> {
    public ChatTipListAdapter(int layoutResId, @Nullable List<QuickQuestion> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, QuickQuestion item) {
            helper.setText(R.id.tip_text,item.getShow_content());

    }
}
