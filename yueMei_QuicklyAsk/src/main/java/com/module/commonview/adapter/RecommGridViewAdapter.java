package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.commonview.module.bean.DiaryOtherPostBean;
import com.module.commonview.module.bean.DiaryOtherPostBoardBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.List;

/**
 * 日记推荐列表
 * Created by 裴成浩 on 2018/5/28.
 */
public class RecommGridViewAdapter extends RecyclerView.Adapter<RecommGridViewAdapter.ViewHolder> {

    private Activity mContext;
    private List<DiaryOtherPostBean> mDatas;
    private final int mLeftMargin;
    private final int mRightMargin;
    private final int mItemMargin;

    private final LayoutInflater mInflater;
    private final int imgWightAndHigh;
    private String TAG = "RecommGridViewAdapter";

    public RecommGridViewAdapter(Activity context, List<DiaryOtherPostBean> datas, int leftMargin, int rightMargin, int itemMargin) {
        this.mContext = context;
        this.mDatas = datas;
        this.mLeftMargin = leftMargin;
        this.mRightMargin = rightMargin;
        this.mItemMargin = itemMargin;
        mInflater = LayoutInflater.from(context);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        int windowsWight = metric.widthPixels;
        imgWightAndHigh = (windowsWight - (mLeftMargin + mRightMargin + mItemMargin)) / 2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_diary_list_gridview, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DiaryOtherPostBean data = mDatas.get(position);

        Glide.with(mContext).load(data.getImg())
//                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .into(holder.mImg);
        holder.mDairyNum.setText(data.getShare_num());
        holder.mTitle.setText(data.getTitle());
        Glide.with(mContext).load(data.getUser_img())
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .transform(new GlideCircleTransform(mContext))
                .into(holder.mHeadPortrait);
        holder.mName.setText(data.getUser_name());
        holder.mSee.setText(data.getView_num());

        //设置标签
        List<DiaryOtherPostBoardBean> board = data.getBoard();
        if (board != null && board.size() > 1) {
            final DiaryOtherPostBoardBean diaryOtherPostBoardBean = board.get(0);
            holder.mTag.setVisibility(View.VISIBLE);
            holder.mTag.setText("#"+diaryOtherPostBoardBean.getName());
            holder.mTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String m_url = diaryOtherPostBoardBean.getM_url();
                    if (!TextUtils.isEmpty(m_url)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(m_url);
                    }
                }
            });
        } else {
            holder.mTag.setVisibility(View.GONE);
        }

        //设置图片
        ViewGroup.LayoutParams layoutParams = holder.mImgClick.getLayoutParams();
        layoutParams.width = imgWightAndHigh;
        layoutParams.height = imgWightAndHigh;
        holder.mImgClick.setLayoutParams(layoutParams);

        //设置间距
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.mItemGridview.getLayoutParams();
        params.width = imgWightAndHigh;
        if (position % 2 == 0) {
            params.leftMargin = mLeftMargin;
            params.rightMargin = mItemMargin / 2;
        } else {
            params.leftMargin = mItemMargin / 2;
            params.rightMargin = mRightMargin;
        }
        holder.mItemGridview.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void setDataAll(List<DiaryOtherPostBean> datas) {
        mDatas.addAll(datas);
        notifyItemRangeChanged(mDatas.size() - datas.size(), datas.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mItemGridview;
        RelativeLayout mImgClick;
        ImageView mImg;
        TextView mDairyNum;
        TextView mTitle;
        ImageView mHeadPortrait;
        TextView mName;
        TextView mSee;
        TextView mTag;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemGridview = itemView.findViewById(R.id.item_diary_list_gridview);
            mImgClick = itemView.findViewById(R.id.item_diary_list_gridview_click);
            mImg = itemView.findViewById(R.id.item_diary_list_gridview_img);
            mDairyNum = itemView.findViewById(R.id.item_diary_list_gridview_dairy_num);
            mTitle = itemView.findViewById(R.id.item_diary_list_gridview_title);
            mHeadPortrait = itemView.findViewById(R.id.item_diary_list_gridview_head_portrait);
            mName = itemView.findViewById(R.id.item_diary_list_gridview_name);
            mSee = itemView.findViewById(R.id.item_diary_list_gridview_see);
            mTag = itemView.findViewById(R.id.item_diary_list_gridview_tag);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onItemClick(v, mDatas.get(getLayoutPosition()), getLayoutPosition());
                    }
                }
            });
        }
    }

    public List<DiaryOtherPostBean> getmDatas() {
        return mDatas;
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, DiaryOtherPostBean data, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}