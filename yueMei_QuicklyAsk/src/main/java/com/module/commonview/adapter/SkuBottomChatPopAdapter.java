package com.module.commonview.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoQuickReply;

import java.util.List;

public class SkuBottomChatPopAdapter extends BaseQuickAdapter<TaoQuickReply, BaseViewHolder> {
    public SkuBottomChatPopAdapter(int layoutResId, @Nullable List<TaoQuickReply> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TaoQuickReply item) {
        helper.setText(R.id.bottom_chat_list_txt,item.getShow_content());
    }
}
