/**
 * 
 */
package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.commonview.module.bean.ProjectFourTree;
import com.module.other.activity.ProjectFourTreeListFragment;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 项目详情四级列表
 * 
 * @author Robin
 * 
 */
public class ProjectFourTreeAdapter extends BaseAdapter {

	private final String TAG = "ProjectFourTreeAdapter";

	private List<ProjectFourTree> mGroupData = new ArrayList<ProjectFourTree>();
	private Context mContext;
	private LayoutInflater inflater;
	private ProjectFourTree groupData;
	ViewHolder viewHolder;

	public ProjectFourTreeAdapter(Context mContext,
			List<ProjectFourTree> mGroupData) {
		this.mContext = mContext;
		this.mGroupData = mGroupData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView groupNameTV;
		public TextView groupJianJTV;
		public ImageView xcheckIv;
	}

	@Override
	public int getCount() {
		return mGroupData.size();
	}

	@Override
	public Object getItem(int position) {
		return mGroupData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "NewApi", "InlinedApi" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_project_four, null);
			viewHolder = new ViewHolder();
			viewHolder.groupNameTV = convertView
					.findViewById(R.id.project_four_item_name_tv);
			viewHolder.groupJianJTV = convertView
					.findViewById(R.id.project_four_item_jianjie_tv);
			viewHolder.xcheckIv = convertView
					.findViewById(R.id.project_four_item_xuan_iv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (null != mGroupData) {
			groupData = mGroupData.get(position);

		 	if (position == ProjectFourTreeListFragment.mmPosition) {
				viewHolder.groupNameTV.setTextColor(mContext.getResources()
						.getColor(R.color.qianlan));
				viewHolder.xcheckIv
						.setBackgroundResource(R.drawable.project_four_yes_3x);
			} else {
				viewHolder.groupNameTV.setTextColor(mContext.getResources()
						.getColor(R.color._44));
				viewHolder.xcheckIv
						.setBackgroundResource(R.drawable.project_four_no_3x);
			}

			// 填充布局
			if (null != groupData) {

				viewHolder.groupNameTV.setText(groupData.getTitle());

				if(null!=groupData.getSubtitle()&&groupData.getSubtitle().length()>0){
					viewHolder.groupJianJTV.setVisibility(View.VISIBLE);
					viewHolder.groupJianJTV.setText(groupData.getSubtitle());
				}else {
					viewHolder.groupJianJTV.setVisibility(View.GONE);
				}
				// viewHolder.groupNameTV.setTextColor(mContext.getResources()
				// .getColor(R.color.tabfontbackgroud));
			}
		}

		return convertView;
	}

	public void add(List<ProjectFourTree> infos) {
		mGroupData.addAll(infos);
	}
}
