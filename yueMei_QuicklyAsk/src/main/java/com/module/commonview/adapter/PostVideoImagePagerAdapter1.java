package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.commonview.module.bean.PostOtherpic;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiVideoView;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/8/31.
 */
public class PostVideoImagePagerAdapter1 extends PagerAdapter {

    private final Context mContext;
    private final int windowsWight;
    private final int maxHeight;
    private List<PostOtherpic> mDatas;
    private int[] imgheights;
    private String TAG = "PostVideoImagePagerAdapter1";

    private final int PLAY_VIDEO = 10;
    private YueMeiVideoView yueMeiVideoView;

    @SuppressLint("HandlerLeak")

    public PostVideoImagePagerAdapter1(Context context, List<PostOtherpic> datas, DisplayMetrics metric) {
        this.mContext = context;
        this.mDatas = datas;

        windowsWight = metric.widthPixels;
        maxHeight = (metric.heightPixels / 3) * 2;
        imgheights = new int[mDatas.size()];
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        PostOtherpic postOtherpic = mDatas.get(position);

        int mWidth = Integer.parseInt(postOtherpic.getWidth());
        int mHeight = Integer.parseInt(postOtherpic.getHeight());

        String isVideo = postOtherpic.getIs_video();
        String img = postOtherpic.getImg();
        if ("1".equals(isVideo)) {

            yueMeiVideoView = setVideoData(postOtherpic);
            yueMeiVideoView.setNetworkChanges(false);

            yueMeiVideoView.setOnProgressBarStateClick(new YueMeiVideoView.OnProgressBarStateClick() {
                @Override
                public void onProgressBarStateClick(int visibility) {

                    if (mDatas.size() > 1) {               //如果两个以上

                        if (onAdapterClickListener != null) {
                            onAdapterClickListener.onProgressBarStateClick(visibility);
                        }
                    }
                }
            });
            if (mWidth != 0 && mHeight != 0) {
                int height = (windowsWight * mHeight) / mWidth;
                imgheights[position] = height > maxHeight ? maxHeight : height;
            } else {
                Glide.with(mContext).load(postOtherpic.getImg()).into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        int intrinsicWidth = resource.getIntrinsicWidth();
                        int intrinsicHeight = resource.getIntrinsicHeight();

                        int mImageHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                        imgheights[position] = mImageHeight;
                    }
                });
            }

            container.addView(yueMeiVideoView);
            return yueMeiVideoView;

        } else {

            final ImageView mImageView = new ImageView(mContext);
            mImageView.setBackgroundColor(Utils.setCustomColor("#f5f8fa"));
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Log.e(TAG, "postOtherpic.getImg() == " + postOtherpic.getImg());
            Log.e(TAG, "mWidth == " + mWidth);
            Log.e(TAG, "mHeight " + mHeight);

            if (position == 0) {
                if (mWidth != 0 && mHeight != 0) {
                    int height = (windowsWight * mHeight) / mWidth;
                    imgheights[position] = height > maxHeight ? maxHeight : height;

                    Glide.with(mContext).load(img).into(mImageView);
                } else {

                    Glide.with(mContext).load(postOtherpic.getImg()).into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                            int intrinsicWidth = resource.getIntrinsicWidth();
                            int intrinsicHeight = resource.getIntrinsicHeight();

                            int mImageHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                            imgheights[position] = mImageHeight > maxHeight ? maxHeight : mImageHeight;

                            mImageView.setImageDrawable(resource);
                        }
                    });
                }
            } else {
                if (mWidth != 0 && mHeight != 0) {
                    int height = (windowsWight * mHeight) / mWidth;
                    imgheights[position] = height > maxHeight ? maxHeight : height;
                }

                Glide.with(mContext).load(postOtherpic.getImg()).into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        int intrinsicWidth = resource.getIntrinsicWidth();
                        int intrinsicHeight = resource.getIntrinsicHeight();

                        int mImageHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                        imgheights[position] = mImageHeight > maxHeight ? maxHeight : mImageHeight;

                        mImageView.setImageDrawable(resource);
                    }
                });
            }

            container.addView(mImageView);
            return mImageView;
        }

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public int[] getImgheights() {
        return imgheights;
    }

    public List<PostOtherpic> getData() {
        return mDatas;
    }


    /**
     * 设置播放器
     */
    private YueMeiVideoView setVideoData(PostOtherpic postOtherpic) {
        final YueMeiVideoView mVideoView = new YueMeiVideoView(mContext);
        String imgUrl = postOtherpic.getImg();
        final String videoUrl = postOtherpic.getVideo_url();

        mVideoView.setVideoParameter(imgUrl, videoUrl, windowsWight);

        //视频最大化点击
        mVideoView.setOnMaxVideoClickListener(new YueMeiVideoView.OnMaxVideoClickListener() {
            @Override
            public void onMaxVideoClick(View v) {
                if (onAdapterClickListener != null) {
                    onAdapterClickListener.onMaxVideoClick(videoUrl, mVideoView.getCurrentPosition());
                }
            }
        });


        @SuppressLint("HandlerLeak") Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case PLAY_VIDEO:
                        mVideoView.videoStartPlayer();
                        break;
                }
            }
        };

        mHandler.sendEmptyMessageDelayed(PLAY_VIDEO, 3000);
        return mVideoView;
    }


    //放大视频的监听
    private OnAdapterClickListener onAdapterClickListener;

    public YueMeiVideoView getVideoView() {
        return yueMeiVideoView;
    }

    public interface OnAdapterClickListener {
        void onMaxVideoClick(String videoUrl, int currentPosition);

        void onProgressBarStateClick(int visibility);
    }

    public void setOnAdapterClickListener(OnAdapterClickListener onAdapterClickListener) {
        this.onAdapterClickListener = onAdapterClickListener;
    }
}
