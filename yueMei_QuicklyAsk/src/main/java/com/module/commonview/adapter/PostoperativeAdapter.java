package com.module.commonview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.commonview.activity.DiaryPhotoBrowsingActivity;
import com.module.commonview.module.bean.PostoperativeRecoverBean;
import com.quicklyask.activity.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostoperativeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<PostoperativeRecoverBean.DataBean> mData;
    private boolean mIsMyself;
    private int mParentPosition = -1;
    private int mChildPosition = -1;
    private boolean isFirst = false;

    public PostoperativeAdapter(Context context, List<PostoperativeRecoverBean.DataBean> data, Boolean isMyself) {
        mContext = context;
        mData = data;
        mIsMyself = isMyself;
        Log.d("PostoperativeAdapter", "size:===>" + mData.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.postoperative_recoviery_item, null, false);
        PrViewHolder prViewHolder = new PrViewHolder(view);
        return prViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int parentPosition) {
        PostoperativeRecoverBean.DataBean dataBean = mData.get(parentPosition);
        PrViewHolder viewHolder = (PrViewHolder) holder;
        viewHolder.mItemTitle2.setText(dataBean.getTitle());
        final List<PostoperativeRecoverBean.DataBean.PicBean> pic = dataBean.getPic();
        PrGridAdapter prGridAdapter = new PrGridAdapter(mContext, pic, mIsMyself);
        viewHolder.mMyselfPostoperativeRecyclerview.setAdapter(prGridAdapter);
        prGridAdapter.setItemClickListener(new PrGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int childPosition) {
                if (mIsMyself) {
                    //作者自己点击添加术后封面
                    if (mParentPosition != parentPosition) {
                        if (mChildPosition != childPosition) {
                            if (isFirst) {
                                mData.get(mParentPosition).getPic().get(mChildPosition).setIs_cover(false);
                            }
                            mData.get(parentPosition).getPic().get(childPosition).setIs_cover(true);
                            mParentPosition = parentPosition;
                            mChildPosition = childPosition;
                            isFirst = true;
                            notifyDataSetChanged();
                        } else {
                            mData.get(mParentPosition).getPic().get(mChildPosition).setIs_cover(false);
                            mData.get(parentPosition).getPic().get(childPosition).setIs_cover(true);
                            mParentPosition = parentPosition;
                            mChildPosition = childPosition;
                            notifyDataSetChanged();
                        }
                    } else {
                        if (mChildPosition != childPosition) {
                            mData.get(mParentPosition).getPic().get(mChildPosition).setIs_cover(false);
                            mData.get(parentPosition).getPic().get(childPosition).setIs_cover(true);
                            mParentPosition = parentPosition;
                            mChildPosition = childPosition;
                            notifyDataSetChanged();
                        }
                    }
                    String surgeryafterdays = mData.get(parentPosition).getSurgeryafterdays();
                    String images = mData.get(parentPosition).getPic().get(childPosition).getImages();
                    String width = mData.get(parentPosition).getPic().get(childPosition).getWidth();
                    String height = mData.get(parentPosition).getPic().get(childPosition).getHeight();
                    String url = mData.get(parentPosition).getPic().get(childPosition).getImg();
                    mItemCallBack.onItemCallBack(surgeryafterdays, images, url, width, height);
                } else {
                    Intent intent = new Intent(mContext, DiaryPhotoBrowsingActivity.class);
                    intent.putExtra("post_id", mData.get(parentPosition).getId());
                    intent.putExtra("position", childPosition);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    static class PrViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title2)
        TextView mItemTitle2;
        @BindView(R.id.myself_postoperative_recyclerview)
        RecyclerView mMyselfPostoperativeRecyclerview;

        public PrViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            RecyclerView.LayoutManager manager = new GridLayoutManager(itemView.getContext(), 3);
            manager.setAutoMeasureEnabled(true);
            mMyselfPostoperativeRecyclerview.setLayoutManager(manager);

        }
    }

    public void setItemCallBack(ItemCallBack itemCallBack) {
        mItemCallBack = itemCallBack;
    }

    private ItemCallBack mItemCallBack;

    public interface ItemCallBack {
        void onItemCallBack(String surgeryafterdays, String serverAdreess, String url, String width, String heigth);
    }
}
