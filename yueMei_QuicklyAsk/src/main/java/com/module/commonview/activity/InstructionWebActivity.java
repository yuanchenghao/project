package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyElasticScrollView1;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 颜值币经验说明(颜值币经验明细)
 *
 * @author Rubin
 *
 */
public class InstructionWebActivity extends BaseActivity {

	private final String TAG = "InstructionWebActivity";

	@BindView(id = R.id.instruction_web_det_scrollview3, click = true)
	private MyElasticScrollView1 scollwebView;
	@BindView(id = R.id.instruction_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.instruction_web_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private InstructionWebActivity mContex;

	public JSONObject obj_http;

	private String uid;
	private String type;
	private BaseWebViewClientMessage baseWebViewClientMessage;
	private String mTyUrl;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_acty_instruction);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = InstructionWebActivity.this;
		uid = Utils.getUid();

		Intent it = getIntent();
		type = it.getStringExtra("type");

		scollwebView.GetLinearLayout(contentWeb);

		setWebType();
		initWebview();

		if (type.equals("1")) {
			LodUrl1(FinalConstant.POINTS_DETAIL);
			mTop.setCenterText("颜值币明细");
		} else if (type.equals("2")) {
			LodUrl1(FinalConstant.POINTS_SHENGIMG);
			mTop.setCenterText("颜值币说明");
		} else if (type.equals("3")) {
			LodUrl1(FinalConstant.EXP_DETAIL);
			mTop.setCenterText("经验值明细");
		} else if (type.equals("4")) {
			LodUrl1(FinalConstant.JINGYAN_EXP);
			mTop.setCenterText("经验值说明");
		} else if (type.equals("5")) {
			LodUrl1(FinalConstant.YUYUE_LIUCHENG);
			mTop.setCenterText("预订流程");
		} else if (type.equals("6")) {
			LodUrl1(FinalConstant.TAO_SERVICE_DETAIL);
			mTop.setCenterText("悦美服务");
		} else if (type.equals("7")) {
			LodUrl1(FinalConstant.GAITUI);
			mTop.setCenterText("改退规则");
		} else if (type.equals("8")) {
			LodUrl1(FinalConstant.BANK_ZHIFU_LIMIT);
			mTop.setCenterText("银行卡限额");
		} else if (type.equals("9")) {
			LodUrl1(FinalConstant.MINGYI_MORE);
			mTop.setCenterText("约名医");
		} else {
			LodUrl1(FinalConstant.baseUrl + FinalConstant.VER + type);
			mTop.setCenterText("费用说明");
		}

	}

	/**
	 * type值设置
	 */
	private void setWebType() {
		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setTypeOutside(true);
		baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
			@Override
			public void typeOutside(WebView view, String url) {
				if(url.startsWith("type")){
					if (type.equals("9")) {
						newShowWebde(url);
					} else {
						baseWebViewClientMessage.showWebDetail(url);
					}
				}else{
					WebUrlTypeUtil.getInstance(mContex).urlToApp(url,"0","0",mTyUrl);
				}

			}
		});
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});

	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 *
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		// Log.e(TAG, urlStr);

		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			newShowWebde(urlStr);

			if (obj.getString("type").equals("535")) {// 打电话
				showDialog();
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	void newShowWebde(String result) {
		String sid = "";

		if (result.length() > 0) {

			if (result.contains("http://m.yuemei.com/tao/")) {// 淘整形详情
				sid = getStringNum(result);

				Intent it1 = new Intent();
				it1.putExtra("id", sid);
				it1.putExtra("source", "0");
				it1.putExtra("objid","0" );
				it1.setClass(mContex, TaoDetailActivity.class);
				startActivity(it1);

			} else if (result.contains("http://m.yuemei.com/c/")) {// 日记贴详情
				sid = getStringNum(result);

				String link = FinalConstant.baseUrl + FinalConstant.VER
						+ "/forum/shareinfo/id/" + sid + "/";
				Intent it = new Intent();
				it.setClass(mContex, DiariesAndPostsActivity.class);
				it.putExtra("url", link);
				it.putExtra("qid", sid);
				startActivity(it);

			} else if (result.contains("http://m.yuemei.com/p/")) {// 达人帖详情
				sid = getStringNum(result);
				String link = FinalConstant.baseUrl + FinalConstant.VER
						+ "/forum/postinfo/id/" + sid + "/";
				Intent it = new Intent();
				it.setClass(mContex, DiariesAndPostsActivity.class);
				it.putExtra("url", link);
				it.putExtra("qid", sid);
				startActivity(it);

			} else if (result.contains("http://m.yuemei.com/ask/")) {// 问题贴详情
				sid = getStringNum(result);
				String link = FinalConstant.baseUrl + FinalConstant.VER
						+ "/forum/askinfo/id/" + sid + "/";
				Intent it = new Intent();
				it.setClass(mContex, DiariesAndPostsActivity.class);
				it.putExtra("url", link);
				it.putExtra("qid", sid);
				startActivity(it);

			} else if (result.contains("http://m.yuemei.com/tao_zt/")) {// 专题详情
				sid = getStringNum(result);
				String url = "http://user.yuemei.com/home/taozt/id/" + sid
						+ "/";
				Intent it1 = new Intent();
				it1.setClass(mContex, ZhuanTiWebActivity.class);
				it1.putExtra("url", url);
				it1.putExtra("title", "悦美");
				it1.putExtra("ztid", sid);
				startActivity(it1);

			} else if (result.contains("http://m.yuemei.com/hospital/")) {// 医院详情
				sid = getStringNum(result);
				Intent it = new Intent();
				it.setClass(mContex, HosDetailActivity.class);
				it.putExtra("hosid", sid);
				startActivity(it);

			} else if (result.contains("http://m.yuemei.com/dr/")) {// 医生详情
				sid = getStringNum(result);
				String docname = "";
				Intent it = new Intent();
				it.setClass(mContex, DoctorDetailsActivity592.class);
				it.putExtra("docId", sid);
				it.putExtra("docName", docname);
				it.putExtra("partId", "");
				startActivity(it);

			} else if (result.contains("http://m.yuemei.com/app/ym.")) {

				Intent intent = new Intent();
				intent.setAction("android.intent.action.VIEW");
				Uri content_url = Uri.parse(result);
				intent.setData(content_url);
				startActivity(intent);

			} else {
				String url = result;
				Intent it1 = new Intent();
				it1.setClass(mContex, SlidePicTitieWebActivity.class);
				it1.putExtra("url", url);
				it1.putExtra("shareTitle", "0");
				it1.putExtra("sharePic", "0");
				startActivity(it1);
			}
		}
	}

	public String getStringNum(String zifuchuang) {
		String a = zifuchuang;
		String regEx = "[^0-9]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(a);
		// System.out.println(m.replaceAll("").trim());
		String sss = m.replaceAll("").trim();

		return sss;
	}

	void showDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(mContex);
		builder.setMessage("     拨打400-056-7118？");
		builder.setPositiveButton("拨打", new DialogInterface.OnClickListener() {

			@SuppressLint("MissingPermission")
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				ViewInject.toast("正在拨打中·····");
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:4000567118"));
				try {
					startActivity(intent);
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
		builder.create().show();
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	public void onResume() {
		super.onResume();
		uid = Utils.getUid();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

}