package com.module.commonview.activity;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.SharePictorial;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.model.bean.ShareDetailPictorial;
import com.module.doctor.view.MzRatingBar;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.util.ExternalStorage;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.io.File;
import java.util.List;

import static com.quicklyask.util.ExternalStorage.saveImageToGallery;

/**
 * 画报页面
 */
public class IllustratedActivity extends BaseActivity {

    @BindView(id = R.id.illustrated_back, click = true)
    private RelativeLayout back;        // 返回
    @BindView(id = R.id.illustrated_share_wechat, click = true)
    private RelativeLayout shareWechat;        //分享微信
    @BindView(id = R.id.illustrated_share_circle_friends, click = true)
    private RelativeLayout shareCircleFriends;        //分享朋友圈
    @BindView(id = R.id.illustrated_save_album, click = true)
    private RelativeLayout saveAlbum;        //保存到本地

    //SKU生成的画报
    @BindView(id = R.id.activity_illustrated1)
    private ScrollView mIllustrated1;
    @BindView(id = R.id.card_illustrated1)
    private LinearLayout mCard1;

    @BindView(id = R.id.illustrated1_picture)
    private ImageView skuPicture;               //SKU生成的画报
    @BindView(id = R.id.illustrated1_title)
    private TextView skuTitle;               //SKU标题
    @BindView(id = R.id.illustrated1_discount_price)
    private TextView skuDiscountPrice;               //SKU价格
    @BindView(id = R.id.illustrated1_fee_cale)
    private TextView skuFeeCale;               //SKU单位
    @BindView(id = R.id.illustrated1_price)
    private TextView skuPrice;                      //SKU实际价格
    @BindView(id = R.id.illustrated1_reservation_number)
    private TextView skuReservationNumber;   //SKU预定数
    @BindView(id = R.id.illustrated1_sun_code)
    private ImageView skuSunCode;           //SKU小程序码
    @BindView(id = R.id.illustrated1_hospital)
    private TextView skuHospital;           //医院名称
    @BindView(id = R.id.illustrated_room_ratingbar)
    private MzRatingBar skuRatingbar;           //医院评分星表示
    @BindView(id = R.id.illustrated1_score)
    private TextView skuScore;              //医院评分数字表示


    //日记贴生成的画报
    @BindView(id = R.id.activity_illustrated2)
    private ScrollView mIllustrated2;
    @BindView(id = R.id.card_illustrated2)
    private LinearLayout mCard2;

    @BindView(id = R.id.illustrated2_photo1)
    private ImageView diaryPhoto1;              //一张日记照片

    @BindView(id = R.id.illustrated2_photo2)
    private LinearLayout diaryPhoto2;              //两张张日记照片
    @BindView(id = R.id.illustrated2_photo2_figure1)
    private ImageView diaryPhoto2Figure1;           //两张日记照片的第一张
    @BindView(id = R.id.illustrated2_photo2_figure2)
    private ImageView diaryPhoto2Figure2;           //两张日记照片的第二张


    @BindView(id = R.id.illustrated2_photo3)
    private LinearLayout diaryPhoto3;              //三张日记照片的
    @BindView(id = R.id.illustrated2_photo3_figure1)
    private ImageView diaryPhoto3Figure1;           //三张日记照片的第一张
    @BindView(id = R.id.illustrated2_photo3_figure2)
    private ImageView diaryPhoto3Figure2;           //三张日记照片的第二张
    @BindView(id = R.id.illustrated2_photo3_figure3)
    private ImageView diaryPhoto3Figure3;           //三张日记照片的第三张

    @BindView(id = R.id.illustrated2_title)
    private TextView diaryTitle;               //日记标题
    @BindView(id = R.id.illustrated2_portrait)
    private ImageView diaryPortrait;               //日记头像
    @BindView(id = R.id.illustrated2_name)
    private TextView diaryName;               //日记昵称
    @BindView(id = R.id.illustrated2_browse_num)
    private TextView diaryBrowseNum;               //日记浏览数
    @BindView(id = R.id.illustrated2_comments_num)
    private TextView diaryCommentsNum;               //日记评论数
    @BindView(id = R.id.illustrated2_content)
    private TextView diaryContent;               //日记内容
    @BindView(id = R.id.illustrated2_sun_code)
    private ImageView diarySunCode;               //日记小程序码

    //传过来需要显示的数据
    private String falg;        //标识，0：SKU传过来的数据，1：日记传过来的数据
    private SharePictorial mSharePictorialData; //SKU生成画报显示的数据
    private ShareDetailPictorial mShareDetailPictorialData; //日记生成画报显示的数据

    //其他数据
    private IllustratedActivity mContext;
    private String TAG = "IllustratedActivity";
    private MyUMShareListener myUMShareListener;


    @Override
    public void setRootView() {
        setContentView(R.layout.activity_illustrated);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = IllustratedActivity.this;

        QMUIStatusBarHelper.translucent(mContext);
        QMUIStatusBarHelper.setStatusBarLightMode(mContext);

        initView();

    }

    private void initView() {
        myUMShareListener = new MyUMShareListener(mContext);

        //SKU生成的画报
        falg = getIntent().getStringExtra("falg");
        Log.e(TAG, "falg == " + falg);
        if ("0".equals(falg)) {      //SKU
            mIllustrated1.setVisibility(View.VISIBLE);
            mIllustrated2.setVisibility(View.GONE);
            mSharePictorialData = getIntent().getParcelableExtra("data");
            initData1();
        } else { //日记
            mIllustrated1.setVisibility(View.GONE);
            mIllustrated2.setVisibility(View.VISIBLE);
            mShareDetailPictorialData = getIntent().getParcelableExtra("data1");
            initData2();
        }

        //分享回调
        myUMShareListener.setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {
                Toast.makeText(mContext, "分享成功", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                Toast.makeText(mContext, "分享失败,图片已保存到相册", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * SKU数据加载
     */
    private void initData1() {
        String feeScale = mSharePictorialData.getFeeScale();
        String hos_name = mSharePictorialData.getHos_name();
        String hos_rate = mSharePictorialData.getHos_rate();
        String hos_rateScale = mSharePictorialData.getHos_rateScale();
        String img = mSharePictorialData.getImg();
        String order_num = mSharePictorialData.getOrder_num();
        String price = mSharePictorialData.getPrice();
        String price_discount = mSharePictorialData.getPrice_discount();
        String sun_img = mSharePictorialData.getSun_img();
        String title = mSharePictorialData.getTitle();
        Log.e(TAG, "feeScale == " + feeScale);
        Log.e(TAG, "hos_name == " + hos_name);
        Log.e(TAG, "hos_rate == " + hos_rate);
        Log.e(TAG, "hos_rateScale == " + hos_rateScale);
        Log.e(TAG, "img == " + img);
        Log.e(TAG, "order_num == " + order_num);
        Log.e(TAG, "price == " + price);
        Log.e(TAG, "price_discount == " + price_discount);
        Log.e(TAG, "sun_img == " + sun_img);
        Log.e(TAG, "title == " + title);


        Log.e(TAG, "img == " + img);
        Glide.with(mContext)
                .load(img)
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into(skuPicture);

        //小程序码
        Glide.with(mContext)
                .load(sun_img)
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into(skuSunCode);

        skuTitle.setText(title);
        skuDiscountPrice.setText(price_discount);
        skuFeeCale.setText(feeScale);
        skuPrice.setText("￥" + price);
        skuPrice.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG); //中划线
        skuReservationNumber.setText(order_num);
        skuHospital.setText(hos_name);
        Log.e(TAG, "hos_rateScale === " + hos_rateScale);
        skuRatingbar.setMax(100);
        skuRatingbar.setProgress(Integer.parseInt(hos_rateScale));
        skuScore.setText(hos_rate);
    }

    /**
     * 日记数据加载
     */
    private void initData2() {

        Log.e(TAG, "mShareDetailPictorialData == " + mShareDetailPictorialData);
        List<String> img = mShareDetailPictorialData.getImg();
        String content = mShareDetailPictorialData.getContent();
        String liulan = mShareDetailPictorialData.getLiulan();
        String sun_img = mShareDetailPictorialData.getSun_img();
        String title = mShareDetailPictorialData.getTitle();
        String user_img = mShareDetailPictorialData.getUser_img();
        String user_name = mShareDetailPictorialData.getUser_name();

        Log.e(TAG, "img == " + img);
        Log.e(TAG, "content == " + content);
        Log.e(TAG, "liulan == " + liulan);
        Log.e(TAG, "sun_img == " + sun_img);
        Log.e(TAG, "title == " + title);
        Log.e(TAG, "user_img == " + user_img);
        Log.e(TAG, "user_name == " + user_name);

        switch (img.size()) {
            case 1:
                diaryPhoto1.setVisibility(View.VISIBLE);

                Glide.with(mContext)
                        .load(img.get(0))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(diaryPhoto1);

                break;
            case 2:
                diaryPhoto2.setVisibility(View.VISIBLE);

                Glide.with(mContext)
                        .load(img.get(0))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(diaryPhoto2Figure1);
                Glide.with(mContext)
                        .load(img.get(1))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(diaryPhoto2Figure2);

                break;
            case 3:
                diaryPhoto3.setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .load(img.get(0))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(diaryPhoto3Figure1);
                Glide.with(mContext)
                        .load(img.get(1))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(diaryPhoto3Figure2);
                Glide.with(mContext)
                        .load(img.get(2))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(diaryPhoto3Figure3);


                break;
        }

        Log.e(TAG, "user_img == " + user_img);
        Glide.with(mContext)
                .load(user_img)
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .transform(new GlideCircleTransform(mContext))
                .into(diaryPortrait);

        Glide.with(mContext)
                .load(sun_img)
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .into(diarySunCode);

        diaryTitle.setText(title);
        diaryName.setText(user_name);
        diaryBrowseNum.setText(liulan);
        diaryContent.setText(content);
    }

    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.illustrated_back:     //退出
                finish();
                break;
            case R.id.illustrated_share_wechat: //分享到微信
                Bitmap bitmapFromView1 = null;
                if ("0".equals(falg)) {      //SKU
                    bitmapFromView1 = ExternalStorage.getBitmapByView(mIllustrated1);
                } else {
                    bitmapFromView1 = ExternalStorage.getBitmapByView(mIllustrated2);
                }
                File file1 = saveImageToGallery(mContext, bitmapFromView1);

                UMImage image1 = new UMImage(mContext, bitmapFromView1);//本地文件
                new ShareAction(mContext)
                        .withMedia(image1)
                        .setPlatform(SHARE_MEDIA.WEIXIN)
                        .setCallback(myUMShareListener).share();

                break;
            case R.id.illustrated_share_circle_friends: //分享到朋友圈
                Bitmap bitmapByView2 = null;
                if ("0".equals(falg)) {
                    bitmapByView2 = ExternalStorage.getBitmapByView(mIllustrated1);
                } else {
                    bitmapByView2 = ExternalStorage.getBitmapByView(mIllustrated2);
                }
                File file2 = ExternalStorage.saveImageToGallery(mContext, bitmapByView2);

                UMImage image2 = new UMImage(mContext, bitmapByView2);//本地文件
                new ShareAction(mContext)
                        .withMedia(image2)
                        .setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
                        .setCallback(myUMShareListener).share();

                break;
            case R.id.illustrated_save_album: //保存到本地
                Bitmap bitmapByView3 = null;
                if ("0".equals(falg)) {
                    bitmapByView3 = ExternalStorage.getBitmapByView(mIllustrated1);
                } else {
                    bitmapByView3 = ExternalStorage.getBitmapByView(mIllustrated2);
                }
                ExternalStorage.saveImageToGallery(mContext, bitmapByView3);

                Toast.makeText(IllustratedActivity.this, "画报已保存到相册", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
