package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.view.ExpressionKeyboardPopupwindows;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

public class QuestionsActivity extends BaseActivity {

    private String TAG = "QuestionsActivity";

    @BindView(id = R.id.ll_lord_components)
    private LinearLayout lordComponents;

    @BindView(id = R.id.question_back, click = true)
    private RelativeLayout colseRly;
    @BindView(id = R.id.question_head_sumbit_rly, click = true)
    private RelativeLayout saveRly;
    @BindView(id = R.id.question_head_sumbit_tv)
    private TextView saveTv;
    @BindView(id = R.id.tv_before_price)
    private TextView beforePrice;

    @BindView(id = R.id.tv_price_discount)
    private TextView priceDiscount;
    @BindView(id = R.id.tv_xiang_mu)
    private TextView xiangMu;

    @BindView(id = R.id.qiestions_sku_img)
    private ImageView qiestionsSkuImg;

    @BindView(id = R.id.question_que_content_et)
    private EditText mContent;

    //选择表情按钮
    @BindView(id = R.id.questions_biaoqing_input_rly, click = true)
    private RelativeLayout biaoqingRly;

    private QuestionsActivity mActivity;
    private ExpressionKeyboardPopupwindows instance;
    private String taoid;
    private String img;
    private String mPriceDiscount;
    private String price;
    private String title;


    @Override
    public void setRootView() {
        setContentView(R.layout.activity_questions);
        mActivity = QuestionsActivity.this;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {

        mContent.setFocusable(true);
        mContent.setFocusableInTouchMode(true);
        mContent.requestFocus();

        instance = new ExpressionKeyboardPopupwindows(mActivity, mContent);

        taoid = getIntent().getStringExtra("taoid");
        img = getIntent().getStringExtra("img");
        mPriceDiscount = getIntent().getStringExtra("priceDiscount");
        price = getIntent().getStringExtra("price");
        title = getIntent().getStringExtra("title");

        Glide.with(mActivity).load(img)
                .transform(new GlideRoundTransform(mActivity, Utils.dip2px(5))).into(qiestionsSkuImg);

        //发布初始化不可点
        saveRly.setEnabled(false);

        xiangMu.setText(title);
        priceDiscount.setText("￥" + mPriceDiscount);
        beforePrice.setText("￥" + price);
        beforePrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        //表情键盘关闭的回调
        instance.setOnColseBiaoqingClickListener(new ExpressionKeyboardPopupwindows.onColseBiaoqingClickListener() {
            @Override
            public void onColseBiaoqingClick() {
                biaoqingRly.setVisibility(View.VISIBLE);
            }
        });

        //输入框点击事件
        mContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instance.dismiss();
                biaoqingRly.setVisibility(View.VISIBLE);
            }
        });

        //输入框输入监听
        mContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                int length = mContent.getText().toString().length();
                if (length >= 4) {
                    saveRly.setEnabled(true);
                } else {
                    saveRly.setEnabled(false);
                }

            }
        });

    }


    /**
     * 点击事件的处理
     *
     * @param v
     */
    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.question_back:        //返回按钮

                finish();

                break;
            case R.id.question_head_sumbit_rly: //发布按钮

                saveRly.setEnabled(false);
                saveTv.setText("发布中");

                uploadProblem(mContent.getText().toString());
                break;
            case R.id.questions_biaoqing_input_rly: //表情输入按钮

                instance.showAtLocation(lordComponents, Gravity.BOTTOM, 0, 0);
                biaoqingRly.setVisibility(View.GONE);

                break;
        }
    }


    private void uploadProblem(String content) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("tao_id", taoid);
        maps.put("content", content);


        HttpParams httpParams = SignUtils.buildHttpParam5(maps);

        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);
        CookieConfig.getInstance().setCookie("https","sjapp.yuemei.com","sjapp.yuemei.com");
        OkGo.post(FinalConstant.QUESTION_MESSAGE)
                .cacheMode(CacheMode.DEFAULT)
                .params(httpParams)
                .headers(headers)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String result, Call call, Response response) {
                        Log.e(TAG, "result === " + result);
                        String code = JSONUtil.resolveJson(result, FinalConstant.CODE);
                        String message = JSONUtil.resolveJson(result, FinalConstant.MESSAGE);
                        ViewInject.toast(message);

                        saveRly.setEnabled(true);
                        saveTv.setText("发布");
                        finish();

                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        Log.e(TAG, "call === " + call);
                        Log.e(TAG, "response === " + response);
                        Log.e(TAG, "e === " + e);
                    }
                });
    }
}
