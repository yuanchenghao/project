package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.api.PhotoBrowsApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.PageJumpManager;
import com.module.commonview.adapter.PhotoBrowsingAdapter2;
import com.module.commonview.module.api.ZanOrJuBaoApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.PhotoBrowsListData;
import com.module.commonview.module.bean.PhotoBrowsListPic;
import com.module.commonview.module.bean.PhotoBrowsListTaoData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 日记本照片浏览页面
 */
public class DiaryPhotoBrowsingActivity extends YMBaseActivity implements View.OnTouchListener {

    @BindView(R.id.photo_browsing_order_goods)
    LinearLayout mTopToBottomFinish;                //整体布局
    @BindView(R.id.photo_browsing_order_goods_price)
    TextView mGoodsPrice;                //价格
    @BindView(R.id.diary_list_plus_vibiable)
    LinearLayout mPlusVisibity;                //plus价格是否显示
    @BindView(R.id.plus_price)
    TextView mPlusPrice;                //plus价格
    @BindView(R.id.photo_browsing_order_goods_title)
    TextView mOrderShop;                //下单商品
    @BindView(R.id.photo_browsing_order_goods_content)
    TextView mGoodsContent;                //文案
    @BindView(R.id.photo_browsing_order_goods_img)
    RelativeLayout mGoodsImg;                //按钮
    @BindView(R.id.photo_browsing_order_goods_click)
    RelativeLayout mGoodsClick;                //sku点击
    @BindView(R.id.photo_browsing_photo_page)
    ViewPager mViewPage;                //viewPage
    @BindView(R.id.photo_browsing_after_number_days)
    TextView mNumberDays;                //术后天数
    @BindView(R.id.photo_browsing_after_number_photo)
    TextView mNumberPhoto;                //照片数量
    @BindView(R.id.photo_browsing_after_like)
    ImageView mAfterLike;                     //点赞
    @BindView(R.id.photo_browsing_after_content)
    TextView mAfterContent;                     //文案
    @BindView(R.id.content_click)
    LinearLayout mContentClick;                     //文案
    @BindView(R.id.left_right_tip)
    FrameLayout mLeftRightTip;                     //左右横滑提示
    @BindView(R.id.photo_browsing_order_btn)
    Button btnZixun;                     //咨询
    private boolean flag = true;

    private PhotoBrowsApi photoBrowsApi;

    private String TAG = "DiaryPhotoBrowsingActivity";
    private String mPostId;
    private int selectedPos;
    private PhotoBrowsingAdapter2 photoBrowsingAdapter;
    private String mUid;
    private View.OnClickListener clickListener;
    private float mPosX;
    private float mPosY;
    private float mCurPosX;
    private float mCurPosY;
    private float mLineSpacingMultiplier = 1.0f;
    private float mLineAdditionalVerticalPadding = 0.0f;
    private int mFlag;
    private String mLeft_right_tip;
    private PhotoBrowsListData mPhotoBrowsListData;
    private PhotoBrowsListTaoData mTaoData;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_diary_photo_browsing;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initView() {
        Intent intent = getIntent();
        mPostId = intent.getStringExtra("post_id");
        selectedPos = intent.getIntExtra("position", -1);
        mFlag = intent.getIntExtra("flag", -1);
        mUid = Utils.getUid();
        Log.e(TAG, "mDiaryId === " + mPostId);
        Log.e(TAG, "selectedPos === " + selectedPos);
        mLeft_right_tip = Cfg.loadStr(mContext, "left_right_tip", "");

        //设置白色字体
        QMUIStatusBarHelper.setStatusBarDarkMode(mContext);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTopToBottomFinish.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;
        mTopToBottomFinish.setLayoutParams(layoutParams);

        mContentClick.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mPosX = event.getX();
                        mPosY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mCurPosX = event.getX();
                        mCurPosY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mCurPosY - mPosY > 0 && (Math.abs(mCurPosY - mPosY) > 25)) {
                            //向下滑動
                            mAfterContent.setEllipsize(null);
                            mAfterContent.setSingleLine(true);
                            mAfterContent.setMaxLines(1);

                        } else if (mCurPosY - mPosY < 0 && (Math.abs(mCurPosY - mPosY) > 25)) {
                            //向上滑动
                            mAfterContent.setEllipsize(null);
                            mAfterContent.setSingleLine(false);
                            mAfterContent.setMaxLines(5);
                        }
                        break;
                }
                return false;
            }

        });
        mContentClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DiaryPhotoBrowsingActivity.this.flag) {
                    mAfterContent.setEllipsize(null);
                    mAfterContent.setSingleLine(false);
                    mAfterContent.setMaxLines(5);
                    DiaryPhotoBrowsingActivity.this.flag = false;
                } else {
                    mAfterContent.setEllipsize(null);
                    mAfterContent.setSingleLine(true);
                    mAfterContent.setMaxLines(1);
                    DiaryPhotoBrowsingActivity.this.flag = true;
                }
            }
        });
        mLeftRightTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftRightTip.setVisibility(View.GONE);
            }
        });
        btnZixun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    String is_rongyun = mTaoData.getIs_rongyun();
                    String hos_userid = mTaoData.getHos_userid();
                    String taoid = mTaoData.getId();
                    String title = mTaoData.getTitle();
                    String price_discount = mTaoData.getPrice_discount();
                    String member_price = mTaoData.getMember_price();
                    String img = mTaoData.getImg();
                    if ("3".equals(is_rongyun)) {
                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                .setDirectId(hos_userid)
                                .setObjId(taoid)
                                .setObjType("2")
                                .setTitle(title)
                                .setPrice(price_discount)
                                .setImg(img)
                                .setMemberPrice(member_price)
                                .setYmClass("106")
                                .setYmId(mPostId)
                                .build();
                        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                                .setEvent_name("chat_hospital")
                                .setEvent_pos("bigimg")
                                .setHos_id(mTaoData.getHospital_id())
                                .setDoc_id(mTaoData.getDoc_id())
                                .setTao_id(taoid)
                                .setEvent_others(mTaoData.getHospital_id())
                                .setId(mPostId)
                                .setReferrer("17")
                                .setType("2")
                                .build();
                        Utils.chatTongJi(mContext, tongJiParams);

                    } else {
                        Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    @Override
    protected void initData() {

        photoBrowsApi = new PhotoBrowsApi();

        downloadData(LoadingStatus.CURRENT_PAGE, mPostId);
        if (TextUtils.isEmpty(mLeft_right_tip)) {
            mLeftRightTip.setVisibility(View.VISIBLE);
            Cfg.saveStr(mContext, "left_right_tip", "1");
        }
    }

    /**
     * 联网请求数据
     */
    private void downloadData(final LoadingStatus loadingStatus, String id) {
        photoBrowsApi.addData("id", id);
        photoBrowsApi.getCallBack(mContext, photoBrowsApi.getHashMap(), new BaseCallBackListener<PhotoBrowsListData>() {
            @Override
            public void onSuccess(PhotoBrowsListData photoBrowsListDatas) {
                mPhotoBrowsListData = photoBrowsListDatas;
                mTaoData = mPhotoBrowsListData.getTaoData();
                if ("3".equals(mTaoData.getIs_rongyun())) {
                    btnZixun.setVisibility(View.VISIBLE);
                } else {
                    btnZixun.setVisibility(View.GONE);
                }
                setDataList(loadingStatus, photoBrowsListDatas);
                Log.e(TAG, photoBrowsListDatas.toString());
            }
        });
    }

    /**
     * 设置数据
     */
    @SuppressLint("SetTextI18n")
    private void setDataList(LoadingStatus loadingStatus, final PhotoBrowsListData datas) {
        if (photoBrowsingAdapter != null) {
            Log.e(TAG, "个数 == " + photoBrowsingAdapter.getImageUrls().size());
        }
        clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(datas.getUrl(), "0", "0");
            }
        };


        //文案设置
        String s = resetText(datas.getContent());
        SpannableString spanableInfo = new SpannableString(s + "全文>>");
        int start = s.length();
        int end = start + "全文>>".length();
        spanableInfo.setSpan(new Clickable(clickListener), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mAfterContent.setText(spanableInfo);
        mAfterContent.setMovementMethod(LinkMovementMethod.getInstance());

        switch (loadingStatus) {
            case PREVIOUS_PAGE:            //上一页
                //setPreviousPage(datas);
                break;
            case CURRENT_PAGE:             //当前页面
                setCurrentPage(datas);
                break;
            case NEXT_PAGE:                //下一页
                //setNextPage(datas);
                break;
        }
    }

    /**
     * 设置上一页数据
     */
    @SuppressLint("SetTextI18n")
    private void setPreviousPage(PhotoBrowsListData datas) {
        List<PhotoBrowsListPic> pic = datas.getPic();
//        mNumberPhoto.setText("(" + pic.size() + "/" + pic.size() + ")");
        photoBrowsingAdapter.setImageUrls(0, pic);
    }

    /**
     * 设置当前页
     *
     * @param datas
     */
    @SuppressLint("SetTextI18n")
    private void setCurrentPage(final PhotoBrowsListData datas) {
        final PhotoBrowsListTaoData taoData = datas.getTaoData();
        List<PhotoBrowsListPic> pic = datas.getPic();
        if (!"".equals(taoData.getTitle())) {
            if (mFlag == 1) {
                mAfterContent.setVisibility(View.GONE);
            }
            String member_price = taoData.getMember_price();
            int i = Integer.parseInt(member_price);
            if (i >= 0) {
                mPlusVisibity.setVisibility(View.VISIBLE);
                mPlusPrice.setText("￥" + member_price);
                mGoodsPrice.setText("￥" + taoData.getPrice_discount());
            } else {
                mPlusVisibity.setVisibility(View.GONE);
                mGoodsPrice.setText("￥" + taoData.getPrice_discount());
            }

            //标题设置
            mGoodsContent.setText(taoData.getTitle());

            //默认天数
            mNumberDays.setText(datas.getTitle());
        } else {
            //价格设置
            if (mFlag == 1) {
                mAfterContent.setVisibility(View.GONE);
            }
            mGoodsPrice.setVisibility(View.GONE);
//            mGoodsImg.setVisibility(View.GONE);
            mOrderShop.setVisibility(View.GONE);
            //标题设置
            mGoodsContent.setText(datas.getDiary_title());
            mGoodsClick.setEnabled(false);
            //默认天数
            mNumberDays.setText(datas.getTitle());
        }

        if ("1".equals(datas.getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(mAfterLike);
        } else {
            Glide.with(mContext).load(R.drawable.diary_zan).into(mAfterLike);
        }

        //当前天数/总天数 默认设置
        mNumberPhoto.setText("(" + (selectedPos + 1) + "/" + pic.size() + ")");
        photoBrowsingAdapter = new PhotoBrowsingAdapter2(mPostId, getSupportFragmentManager(), pic);
        mViewPage.setAdapter(photoBrowsingAdapter);
        mViewPage.setCurrentItem(selectedPos);                          //默认选中的页

        //viewPage滑动回调
        mViewPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mNumberPhoto.setText("(" + (position + 1) + "/" + photoBrowsingAdapter.getImageUrls().size() + ")");
                loadingPreviousAndNext(position, datas);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mGoodsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //跳转SKU
        mGoodsClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tao_id = taoData.get_id();
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", mPostId);
                hashMap.put("to_page_type", "2");
                hashMap.put("to_page_id", tao_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BIGIMG_TAO), hashMap);

                taoData.setUrl(FinalConstant1.BASE_API_M_URL + FinalConstant1.SYMBOL2 + FinalConstant1.TAO + FinalConstant1.SYMBOL2 + tao_id);
                WebUrlTypeUtil.getInstance(mContext).urlToApp(taoData.getUrl(), "0", "0");
            }
        });
        //点赞
        mAfterLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> maps = new HashMap<>();
                maps.put("id", mPostId);
                maps.put("flag", "1");
                maps.put("puid", mUid);
                new ZanOrJuBaoApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData data) {
                        if ("1".equals(data.code)) {
                            if (!data.isOtherCode){
                                mFunctionManager.showShort(data.message);
                            }
                            String is_agree = JSONUtil.resolveJson(data.data, "is_agree");
                            if ("1".equals(is_agree)) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(mAfterLike);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_zan).into(mAfterLike);
                            }
                        }

                    }
                });
            }
        });

    }

    /**
     * 设置下一页数据
     */
    @SuppressLint("SetTextI18n")
    private void setNextPage(PhotoBrowsListData datas) {
        List<PhotoBrowsListPic> pic = datas.getPic();
//        mNumberPhoto.setText("(1/" + pic.size() + ")");
        photoBrowsingAdapter.setImageUrls(photoBrowsingAdapter.getImageUrls().size() - 1, pic);
    }

    /**
     * 重TextView内部文字渲染逻辑
     */
    private String resetText(String origText) {

        if (!TextUtils.isEmpty(origText)) {
            String firstLineText;
            String secondLineText;
            String thirdLineText;
            String fourdLineText;
            String fivedLineText = "";
            String resultText = origText;
            Log.e(TAG, "origText=" + origText);
            Layout layout = createRenderLayout(origText, mAfterContent.getWidth() - mAfterContent.getPaddingLeft() - mAfterContent.getPaddingRight());
            if (layout.getLineCount() > 4) {
                //取出第一到五行文字
                Log.e(TAG, "origText == " + origText);

                firstLineText = origText.substring(0, layout.getLineEnd(0));
                Log.e(TAG, "firstLineText=" + firstLineText);
                secondLineText = origText.substring(layout.getLineEnd(0), layout.getLineEnd(1));
                Log.e(TAG, "secondLineText=" + secondLineText);
                thirdLineText = origText.substring(layout.getLineEnd(1), layout.getLineEnd(2));
                Log.e(TAG, "thirdLineText=" + thirdLineText);
                fourdLineText = origText.substring(layout.getLineEnd(2), layout.getLineEnd(3));
                Log.e(TAG, "fourdLineText=" + fourdLineText);
                if (layout.getLineCount() > 5) {
                    fivedLineText = origText.substring(layout.getLineEnd(3), layout.getLineEnd(4) - 9);
                    Log.e(TAG, "fivedLineText=" + fivedLineText);
                    resultText = firstLineText + secondLineText + thirdLineText + fourdLineText + fivedLineText + "...";
                    return resultText;
                } else {
                    fivedLineText = origText.substring(layout.getLineEnd(3), layout.getLineEnd(4));
                    resultText = firstLineText + secondLineText + thirdLineText + fourdLineText + fivedLineText;
                    return resultText;
                }
            }
        }
        return origText;
    }

    /**
     * @param workingText
     * @param width
     * @return StaticLayout @See https://developer.android.com/reference/android/text/StaticLayout.html
     */
    private Layout createRenderLayout(CharSequence workingText, int width) {
        return new StaticLayout(workingText, mAfterContent.getPaint(), width, Layout.Alignment.ALIGN_NORMAL, mLineSpacingMultiplier, mLineAdditionalVerticalPadding, false);
    }

    /**
     * 加载上一条和下一条数据
     *
     * @param pos
     * @param datas
     */
    private void loadingPreviousAndNext(int pos, PhotoBrowsListData datas) {
        if (pos == 0) {
            String prevId = datas.getPrev_id();
            if (!TextUtils.isEmpty(prevId)) {
                downloadData(LoadingStatus.PREVIOUS_PAGE, prevId);
            }
        } else if (pos == photoBrowsingAdapter.getImageUrls().size() - 1) {
            String nextId = datas.getNext_id();
            if (!TextUtils.isEmpty(nextId)) {
                downloadData(LoadingStatus.NEXT_PAGE, nextId);
            }
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.activity_close);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int Action = event.getAction();
        float X = event.getX();
        float Y = event.getY();

        Log.d(TAG, "x:" + X + "y:" + Y);
        return false;
    }

    /**
     * 数据加载状态
     */
    enum LoadingStatus {
        PREVIOUS_PAGE, CURRENT_PAGE, NEXT_PAGE
    }

    class Clickable extends ClickableSpan {

        private final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            mListener = l;
        }

        /**
         * 重写父类点击事件
         */
        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }

        /**
         * 重写父类updateDrawState方法  我们可以给TextView设置字体颜色,背景颜色等等...
         */
        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(getResources().getColor(R.color.red_ff4965));
        }
    }
}
