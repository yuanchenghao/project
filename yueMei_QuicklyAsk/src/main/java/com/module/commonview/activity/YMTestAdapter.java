package com.module.commonview.activity;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiVideoView;

import java.util.List;

public class YMTestAdapter extends RecyclerView.Adapter<YMTestAdapter.PostViewHolder>  {
    private List<TestBean> datas;
    private Activity mContext;
    private LayoutInflater mInflater;

    public YMTestAdapter(List<TestBean> datas, Activity mContext) {
        this.datas = datas;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public YMTestAdapter.PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return new PostViewHolder(mInflater.inflate(R.layout.layout_video, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull YMTestAdapter.PostViewHolder viewHolder, int i) {
        TestBean testBean = datas.get(i);
        viewHolder.videoView.removeAllViews();
        YueMeiVideoView videoView = new YueMeiVideoView(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,Utils.dip2px(187));
        videoView.setLayoutParams(layoutParams);
        viewHolder.videoView.addView(videoView);
        videoView.setVideoParameter(testBean.getImg_url(),testBean.getUrl());
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }


    public class PostViewHolder extends RecyclerView.ViewHolder{

        public LinearLayout videoView;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.video_container);
        }
    }
}
