package com.module.commonview.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.ProjectRightListAdapter;
import com.module.commonview.module.api.LoadProjectDataApi;
import com.module.commonview.module.api.LoadTwoTreeListApi;
import com.module.commonview.module.bean.ProjectDetailBoard;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseProjectPopupwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.commonview.view.SortScreenPopwin;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.model.bean.TaoListDoctors;
import com.module.community.model.bean.TaoListDoctorsCompared;
import com.module.community.model.bean.TaoListDoctorsEnum;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.WantBeautifulActivity548;
import com.module.doctor.model.api.FilterDataApi;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.model.api.SearchShowDataApi;
import com.module.home.model.bean.ComparedBean;
import com.module.home.model.bean.SearchResultDoctor;
import com.module.home.model.bean.SearchXSData;
import com.module.home.view.LoadingProgress;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.my.model.bean.TypeProblemData;
import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.HomeTaoDataS;
import com.module.taodetail.view.adapter.SearhBoardRecyclerAdapter;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Location;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 重构的项目详细页
 * Created by 裴成浩 on 2018/1/31.
 */

public class ProjectDetailActivity638 extends YMBaseActivity {
    @BindView(R.id.project_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.order_time_back)
    RelativeLayout back;
    @BindView(R.id.priject_input_edit1)
    TextView inputEdit1;
    @BindView(R.id.ll_sousuo_click)
    LinearLayout sousuoClick;
    @BindView(R.id.title_bar_rly)
    LinearLayout titleRly;
    @BindView(R.id.my_doc_collect_list_view)
    LoadMoreRecyclerView mRecycler;
    @BindView(R.id.my_doc_collect_refresh)
    SmartRefreshLayout mSkuRefresh;
    @BindView(R.id.btn_help_find)
    Button helpFindBt;
    @BindView(R.id.my_collect_doc_tv_nodata)
    LinearLayout mNotData;

    private String TAG = "ProjectDetailA";

    //上一个页面传过来的
    private String oneid;
    private String twoid;
    private String threeid;
    //popwindow
    private BaseProjectPopupwindows projectPop;
    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private SortScreenPopwin sortShaiPop;

    //popwindow数据的集合
    private List<MakeTagData> mData = new ArrayList<>();   //项目详情数据
    private ArrayList<TaoPopItemData> lvSortData = new ArrayList<>();//智能排序数据

    //进行筛选的参数
    private String partId = "0'";              //项目选择
    private String sortStr1 = "1";            //智能排序
    private ArrayList<ProjcetList> kindStr = new ArrayList<>();  //筛选1

    //其他参数
    private String curPid = "0";
    private SearchXSData searchShow;
    private LoadingProgress progress2;
    private int mDataPage = 1;  //页数
    private int sortPos = 0;
    private LocationManager lm;
    private int GPS_REQUEST_CODE = 100;

    private String provinceLocation;
    private LocationClient locationClient;
    private TaoListAdapter mProjectSkuAdapter;
    private LoadProjectDataApi loadProjectDataApi;
    private BaseNetWorkCallBackApi getScreenBoard;
    private SearchResultBoard searchActivity;                       //活动
    private int lastVisibleItemPosition;                            //可显示的最后一个数据
    private LinearLayoutManager linearLayoutManager;
    private View headerView;
    private SearhBoardRecyclerAdapter mBoardRecyclerAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_project_detail638;
    }

    @Override
    protected void initView() {
        progress2 = new LoadingProgress(mContext);
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        //获取传过来的数据
        Intent it = getIntent();

        String action = it.getAction();
        Log.e(TAG, "action == " + action);
        //判断是自己app跳转的，还是其他app跳转的
        if (!TextUtils.isEmpty(action) && Intent.ACTION_VIEW.equals(action)) {
            String scheme = it.getScheme();
            Uri uri = it.getData();
            Log.e(TAG, "scheme === " + scheme);

            //获取指定参数值
            if (uri != null) {
                String id = uri.getQueryParameter("id");
                oneid = id;
                twoid = id;
                threeid = id;
            }
        } else {
            oneid = it.getStringExtra("oneid");
            twoid = it.getStringExtra("twoid");
            threeid = it.getStringExtra("threeid");
        }

        Log.e(TAG, "oneid  = " + oneid);
        Log.e(TAG, "twoid  = " + twoid);
        Log.e(TAG, "threeid  = " + threeid);
        Log.e(TAG, "kindStr  = " + kindStr);

        if (!"0".equals(threeid)) {
            partId = threeid;
            curPid = threeid;
        } else if (!"0".equals(twoid)) {
            partId = twoid;
            curPid = twoid;
        } else if (!"0".equals(oneid)) {
            partId = oneid;
            curPid = oneid;
        } else {
            partId = "0";
            curPid = "0";
        }

        //初始化筛选
        mScreen.initView(true, true);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener2() {
            @Override
            public void onProjectClick() {
                if (projectPop != null) {
                    if (projectPop.isShowing()) {
                        projectPop.dismiss();
                    } else {
                        projectPop.showPop();
                    }
                    mScreen.initProjectView(projectPop.isShowing());
                }
            }

            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                    }
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }

            @Override
            public void onKindClick() {
                if (sortShaiPop != null) {
                    if (sortShaiPop.isShowing()) {
                        sortShaiPop.dismiss();
                    } else {
                        sortShaiPop.showPop();
                    }
                    mScreen.initKindView(sortShaiPop.isShowing(), isScerPopSelected());
                }
            }
        });
    }

    @Override
    protected void initData() {
        //初始化pop筛选
        loadTwoTreeList();      //获取项目列表数据
        cityPop = new BaseCityPopwindows(mContext, mScreen);        //城市筛选
        setPopData();           //智能排序
        loadSXData();           //获取筛选列表数据

        loadProjectDataApi = new LoadProjectDataApi();
        getScreenBoard = new BaseNetWorkCallBackApi(FinalConstant1.BOARD, "getScreenBoard");

        listeningCallback();    //设置监听回调
        initSearhShowData();    //获取搜索框数据
        lodBoardData();         //获取活动
        lodHotIssueData();      //获取淘列表数据
    }

    @Override
    public void onPause() {
        saveData();
        super.onPause();
    }

    /**
     * 获取活动数据
     */
    private void lodBoardData() {
        if (mBoardRecyclerAdapter == null) {
            getScreenBoard.addData("flag", "20");
            getScreenBoard.startCallBack(new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData data) {
                    if ("1".equals(data.code)) {
                        try {
                            ProjectDetailBoard projectDetailBoard = JSONUtil.TransformSingleBean(data.data, ProjectDetailBoard.class);
                            ArrayList<SearchResultBoard> resultBoards = projectDetailBoard.getScreen_board();
                            if (resultBoards.size() > 0) {
                                setHeadView(resultBoards);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        }
    }

    /**
     * 设置头布局
     */
    private void setHeadView(ArrayList<SearchResultBoard> resultBoards) {
        if (headerView != null) {
            mRecycler.removeHeaderView(headerView);
        }

        headerView = View.inflate(mContext, R.layout.project_tao_top_view, null);
        RecyclerView mActivityRecycler = headerView.findViewById(R.id.project_activity_recycler);

        headerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mRecycler.addHeaderView(headerView);

        if (mProjectSkuAdapter != null) {
            mProjectSkuAdapter.isHeadView(headerView != null);
        }

        //活动列表设置
        if (mBoardRecyclerAdapter == null) {
            LinearLayoutManager activityLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            ((DefaultItemAnimator) mActivityRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果(是因为局部刷新的是每一个item,而不是item的部分)
            mBoardRecyclerAdapter = new SearhBoardRecyclerAdapter(mContext, resultBoards);
            mActivityRecycler.setLayoutManager(activityLinearLayoutManager);
            mActivityRecycler.setAdapter(mBoardRecyclerAdapter);
            mBoardRecyclerAdapter.setOnEventClickListener(new SearhBoardRecyclerAdapter.OnEventClickListener() {
                @Override
                public void onItemClick(SearchResultBoard data) {
                    searchActivity = data;
                    progress2.startLoading();
                    mProjectSkuAdapter = null;
                    mDataPage = 1;
                    lodHotIssueData();
                }
            });
        }
    }

    /**
     * 监听的回调
     */
    private void listeningCallback() {
        //返回的监听
        back.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("NewApi")
            @Override
            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        //搜索框点击监听
        sousuoClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                Intent it = new Intent(mContext, SearchAllActivity668.class);
                String searchKey = searchShow.getSearch_key();
                if (!TextUtils.isEmpty(searchKey)) {
                    it.putExtra("keys", searchKey);
                } else {
                    it.putExtra("keys", "");
                }
                startActivity(it);
            }
        });

        //我们帮您找一下按钮
        helpFindBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent it = new Intent();
                it.putExtra("po", "2340");
                it.putExtra("hosid", "0");
                it.putExtra("docid", "0");
                it.putExtra("shareid", "0");
                it.putExtra("cityId", "0");
                it.putExtra("partId", oneid);
                it.putExtra("partId_two", twoid);
                it.setClass(mContext, WantBeautifulActivity548.class);
                startActivity(it);
            }
        });

        //关闭上拉加载更多
        mRecycler.setLoadingMoreEnabled(false);

        //加载更多和刷新
        mSkuRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mSkuRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                lodHotIssueData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                reshData();
            }
        });


        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color.subscribe_item_drag_bg)));
        mRecycler.setLayoutManager(linearLayoutManager);

        //检测recylerview的滚动事件
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int lastItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                    if (lastItemPosition > lastVisibleItemPosition) {
                        lastVisibleItemPosition = lastItemPosition;
                    }
                }
            }
        });

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }

                reshData();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });
    }

    private void intelligentSorting(int pos) {
        sortPop.setAdapter(pos);
        sortStr1 = lvSortData.get(pos).get_id();
        mScreen.setSortTitle(lvSortData.get(pos).getName());
        sortPop.dismiss();
        reshData();

    }

    /**
     * dialog提示
     */
    void showDialogExitEdit3() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce1);
        editDialog.setCanceledOnTouchOutside(false);
        Log.e(TAG, "editDialog == " + editDialog);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setVisibility(View.VISIBLE);
        titleTv77.setText("您未打开定位权限");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("请允许悦美获取您当前位置，以帮您查询附近医院");
        titleTv88.setHeight(Utils.dip2px(mContext, 35));

        LinearLayout llFengexian = editDialog.findViewById(R.id.ll_fengexian);
        llFengexian.setVisibility(View.VISIBLE);


        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("去设置");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // 转到手机设置界面，用户设置GPS
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, GPS_REQUEST_CODE); // 设置完成后返回到原来的界面
                editDialog.dismiss();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setVisibility(View.VISIBLE);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 项目详情数据获取
     */
    private void loadTwoTreeList() {
        Map<String, Object> maps = new HashMap<>();
        new LoadTwoTreeListApi().getCallBack(mContext, maps, new BaseCallBackListener<List<MakeTagData>>() {
            @Override
            public void onSuccess(List<MakeTagData> serverData) {
                Log.e(TAG, "serverData === " + serverData.toString());
                mData = serverData;

                projectPop = new BaseProjectPopupwindows(mContext, mScreen, mData, oneid);

                projectPop.setmServerData(mData);
                projectPop.setOneid(oneid);
                projectPop.setTwoid(twoid);
                projectPop.setThreeid(threeid);

                projectPop.setLeftView();
                projectPop.setRightView(projectPop.getmPos());

                setProPopTitle();

                //滚动到相应位置
                projectPop.getmLeftRecy().scrollToPosition(projectPop.getmPos());
                ProjectRightListAdapter rightListAdapter = projectPop.getRightListAdapter();
                int selectedPos = 0;
                if (rightListAdapter != null) {
                    selectedPos = rightListAdapter.getmSelectedPos();
                }
                projectPop.getRightList().setSelection(selectedPos);

                projectPop.setOnItemSelectedClickListener(new BaseProjectPopupwindows.OnItemSelectedClickListener() {
                    @Override
                    public void onItemSelectedClick(String id, String name) {
                        partId = id;
                        if (!TextUtils.isEmpty(name)) {
                            mScreen.setProjectTitle(name);
                        }
                        reshData();
                    }
                });

                //筛选关闭
                projectPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mScreen.initProjectView(false);
                    }
                });
            }
        });
    }

    /**
     * 筛选数据请求
     */
    private void loadSXData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("partId", curPid);
        new FilterDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ArrayList<ProjcetData>>() {
            @Override
            public void onSuccess(ArrayList<ProjcetData> lvkindData) {

                sortShaiPop = new SortScreenPopwin(mContext, mScreen, lvkindData);

                sortShaiPop.setOnButtonClickListener(new SortScreenPopwin.OnButtonClickListener() {
                    @Override
                    public void onResetListener(View view) {
                        if (sortShaiPop != null) {
                            kindStr.clear();
                            sortShaiPop.resetData();
                        }
                    }

                    @Override
                    public void onSureListener(View view, ArrayList data) {
                        if (sortShaiPop != null) {
                            kindStr = sortShaiPop.getSelectedData();
                            reshData();
                            sortShaiPop.dismiss();
                        }
                    }
                });

                //筛选关闭
                sortShaiPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mScreen.initKindView(false, isScerPopSelected());
                    }
                });
            }

        });
    }

    /**
     * 搜索框显示加载
     */
    void initSearhShowData() {
        Log.e(TAG, "FinalConstant.SEARCH_SHOW_DATA === " + FinalConstant.SEARCH_SHOW_DATA);
        new SearchShowDataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<SearchXSData>() {
            @Override
            public void onSuccess(SearchXSData searchXSData) {
                searchShow = searchXSData;
                inputEdit1.setText(searchShow.getShow_key());
            }
        });
    }

    private void lodHotIssueData() {
        loadProjectDataApi.getHashMap().clear();

        //把数组中的数据拼接成字符串
        loadProjectDataApi.addData("partId", partId);
        loadProjectDataApi.addData("sort", sortStr1);

        Log.e(TAG, "partId = " + partId);
        Log.e(TAG, "sortStr1 = " + sortStr1);
        Log.e(TAG, "kindStr = " + kindStr.size());
        for (ProjcetList data : kindStr) {
            Log.e(TAG, "data.getPostName() = " + data.getPostName());
            loadProjectDataApi.addData(data.getPostName(), data.getPostVal());
        }

        //活动
        Log.e(TAG, "searchActivity == " + searchActivity);
        if (searchActivity != null) {
            Log.e(TAG, "searchActivity.getPostName() == " + searchActivity.getPostName());
            Log.e(TAG, "searchActivity.getPostVal() == " + searchActivity.getPostVal());
            loadProjectDataApi.addData(searchActivity.getPostName(), searchActivity.getPostVal());
        }

        loadProjectDataApi.addData("page", mDataPage + "");
        loadProjectDataApi.getCallBack(mContext, loadProjectDataApi.getHashMap(), new BaseCallBackListener<HomeTaoDataS>() {
            @Override
            public void onSuccess(HomeTaoDataS data) {
                progress2.stopLoading();
                mDataPage++;

                List<HomeTaoData> mFragmentData = data.getData();

                mSkuRefresh.finishRefresh();
                if (mFragmentData.size() == 0) {
                    Log.e(TAG, "111");
                    mSkuRefresh.finishLoadMoreWithNoMoreData();
                } else {
                    mSkuRefresh.finishLoadMore();
                    Log.e(TAG, "2222");
                }

                if (mProjectSkuAdapter == null) {
                    if (mFragmentData.size() != 0) {
                        mSkuRefresh.setVisibility(View.VISIBLE);
                        mNotData.setVisibility(View.GONE);

                        setRecyclerData(mFragmentData, data.getComparedConsultative());

                    } else {
                        mSkuRefresh.setVisibility(View.GONE);
                        mNotData.setVisibility(View.VISIBLE);
                    }

                } else {
                    mProjectSkuAdapter.addData(mFragmentData);
                }
            }
        });
    }


    /**
     * 设置列表数据
     */
    private void setRecyclerData(List<HomeTaoData> mFragmentData, SearchResultDoctor mDoctors) {
        mProjectSkuAdapter = new TaoListAdapter(mContext, mFragmentData, mDoctors);
        mProjectSkuAdapter.isHeadView(headerView != null);
        mRecycler.setAdapter(mProjectSkuAdapter);

        mProjectSkuAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TaoListDataType taoData, int pos) {
                HomeTaoData data = taoData.getTao();
                if (TextUtils.isEmpty(data.getTuijianTitle())) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "board|taolist_" + Utils.getCity() + "_" + partId + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, data.getBmsid(), "1"), data.getEvent_params());
                    String id = data.get_id();
                    Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                    it1.putExtra("id", id);
                    it1.putExtra("source", "12");
                    it1.putExtra("objid", "0");
                    startActivity(it1);
                }
            }
        });

        //咨询统计
        mProjectSkuAdapter.setOnDoctorChatlickListener(new TaoListAdapter.OnDoctorChatlickListener() {
            @Override
            public void onDoctorChatClick(HashMap<String, String> event_params) {

                YmStatistics.getInstance().tongjiApp(event_params);

            }

            @Override
            public void onChangeClick(TaoListDoctorsCompared compared, int changePos) {
                TaoListDoctorsEnum doctorsEnum = compared.getDoctorsEnum();
                HashMap<String, String> changeOneEventParams = compared.getChangeOneEventParams();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARED_CHANGE_ONE_CLICK, (changePos + 1) + "", (doctorsEnum == TaoListDoctorsEnum.TOP ? "0" : "1")), changeOneEventParams);
            }
        });
    }

    /**
     * 刷新数据
     */
    private void reshData() {
        saveData();
        progress2.startLoading();
        mProjectSkuAdapter = null;
        mDataPage = 1;
        lodHotIssueData();
        lodBoardData();
    }

    /**
     * 设置popwindoc的数据
     */
    private void setPopData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("2");
        a2.setName("销量最高");
        TaoPopItemData a3 = new TaoPopItemData();
        a3.set_id("3");
        a3.setName("日记和案例最多");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("4");
        a4.setName("最新上架");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("5");
        a5.setName("价格从低到高");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("7");
        a6.setName("离我最近");

        lvSortData.add(a1);
        lvSortData.add(a2);
        lvSortData.add(a3);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a6);

        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        //智能排序item的回调
        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                sortPos = pos;
                if (sortPos == 5) {
                    boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    Log.e(TAG, "ok == " + ok);
                    if (ok) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Acp.getInstance(ProjectDetailActivity638.this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                                @Override
                                public void onGranted() {
                                    initLocation();
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                    sortPop.dismiss();
                                }
                            });
                        } else {
                            initLocation();
                        }
                    } else {
                        sortPop.dismiss();
                        showDialogExitEdit3();
                    }
                } else {
                    intelligentSorting(sortPos);
                }
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    /**
     * 设置项目pop的title
     */
    private void setProPopTitle() {

        //设置所选项目id和和title1
        if (projectPop.getmTwoPos() > 0) {
            if (projectPop.getmThreePos() > 0) {
                String name = mData.get(projectPop.getmPos()).getList().get(projectPop.getmTwoPos()).getList().get(projectPop.getmThreePos()).getName();
                mScreen.setProjectTitle(name);
                partId = threeid;
            } else {
                String name = mData.get(projectPop.getmPos()).getList().get(projectPop.getmTwoPos()).getName();
                mScreen.setProjectTitle(name);
                partId = twoid;
            }

        } else {
            if ("0".equals(oneid)) {
                mScreen.setProjectTitle("全部项目");
            } else {
                String name = mData.get(projectPop.getmPos()).getName();
                mScreen.setProjectTitle(name);
            }
            partId = oneid;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPS_REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                Acp.getInstance(ProjectDetailActivity638.this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                    @Override
                    public void onGranted() {
                        initLocation();
                    }

                    @Override
                    public void onDenied(List<String> permissions) {
                        sortPop.dismiss();
                    }
                });
            } else {
                initLocation();
            }

        }
    }

    private void initLocation() {
        try {
            locationClient = new LocationClient(getApplicationContext());
            // 设置监听函数（定位结果）
            locationClient.registerLocationListener(new MyBDLocationListener());
            LocationClientOption option = new LocationClientOption();
            option.setAddrType("all");// 返回的定位结果包含地址信息
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
            locationClient.setLocOption(option);
            locationClient.start();
            // 请求定位

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 定位结果的响应函数
     *
     * @author
     */
    public class MyBDLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            /**
             * 此处需联网
             */
            // 设置editTextLocation为返回的百度地图获取到的街道信息
            try {
                // 自定义的位置类保存街道信息和经纬度（地图中使用）
                Location location = new Location();
                location.setAddress(bdLocation.getAddrStr());

                GeoPoint geoPoint = new GeoPoint((int) (bdLocation.getLatitude() * 1E6), (int) (bdLocation.getLongitude() * 1E6));
                location.setGeoPoint(geoPoint);

                String latitude = bdLocation.getLatitude() + "";
                String longitude = bdLocation.getLongitude() + "";
                //判断是否定位失败
                if (!"4.9E-324".equals(latitude) && !"4.9E-324".equals(longitude)) {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, latitude);
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, longitude);
                } else {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, "0");
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, "0");
                    MyToast.makeTextToast2(mContext, "定位失败，请查看手机是否开启了定位权限", MyToast.SHOW_TIME).show();
                }

                intelligentSorting(sortPos);
                String ss = bdLocation.getCity();

                if (ss != null && ss.length() > 1) {

                    if (ss.contains("省")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("市")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("自治区")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 3);
                    }

                    Cfg.saveStr(ProjectDetailActivity638.this, "city_dingwei", provinceLocation);

                    Cfg.saveStr(ProjectDetailActivity638.this, FinalConstant.TAOCITY, provinceLocation);

                    Cfg.saveStr(ProjectDetailActivity638.this, FinalConstant.DWCITY, provinceLocation);

                    locationClient.unRegisterLocationListener(new MyBDLocationListener());

                } else {
                    provinceLocation = "失败";
                    Cfg.saveStr(ProjectDetailActivity638.this, "city_dingwei", "全国");

                    Cfg.saveStr(ProjectDetailActivity638.this, FinalConstant.TAOCITY, "全国");

                    Cfg.saveStr(ProjectDetailActivity638.this, FinalConstant.DWCITY, "全国");

                    locationClient.unRegisterLocationListener(new MyBDLocationListener());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 筛选中的数据是否有选中项
     *
     * @return ：true:有 false:无
     */
    private boolean isScerPopSelected() {
        return kindStr.size() != 0;
    }

    @Override
    protected void onDestroy() {

        if (projectPop != null) {
            projectPop.dismiss();
        }
        if (cityPop != null) {
            cityPop.dismiss();
        }
        if (sortPop != null) {
            sortPop.dismiss();
        }
        if (sortShaiPop != null) {
            sortShaiPop.dismiss();
        }

        super.onDestroy();
    }

    /**
     * 保存曝光数据
     */
    private void saveData() {
        try {
            List<HashMap<String, String>> doctors = new ArrayList<>();
            if (mProjectSkuAdapter != null) {
                List<HomeTaoData> datas = mProjectSkuAdapter.getData();
                int size = lastVisibleItemPosition < datas.size() ? lastVisibleItemPosition : datas.size();
                for (int i = 0; i < size; i++) {
                    List<TaoListDoctors> comparChatDatas = mProjectSkuAdapter.getComparChatData();
                    for (int j = 0; j < comparChatDatas.size(); j++) {
                        TaoListDoctorsCompared compared = comparChatDatas.get(j).getCompared();
                        int doctorSelectPos = Integer.parseInt(compared.getShowSkuListPosition());
                        if (doctorSelectPos >= 0 && i == doctorSelectPos) {
                            doctors.add(compared.getExposureEventParams());
                        }
                    }
                }
            }
            for (HashMap<String, String> doc : doctors) {
                if (doc != null) {
                    Log.e(TAG, "doc == " + doc.toString());
                    YmStatistics.getInstance().tongjiApp(doc);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
