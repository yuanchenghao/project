package com.module.commonview.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.commonview.module.bean.PhotoBrowsListPic;
import com.quicklyask.activity.R;

import uk.co.senab.photoview.PhotoView;

public class DiariesPhotoFragment extends Fragment {
    public static final String TAG="DiariesPhotoFragment";
    PhotoView mDiariesPhotoview;
    private PhotoBrowsListPic mData;
    private boolean flag=true;
    private TextView mTextContent;
    private int windowsHeight;//屏幕高
    private int windowsWight;//屏幕宽



    public static DiariesPhotoFragment newInstance(PhotoBrowsListPic imageUrls) {
        DiariesPhotoFragment fragment = new DiariesPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data",imageUrls);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = getArguments().getParcelable("data");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View view = inflater.inflate(R.layout.diaries_photo_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTextContent = getActivity().findViewById(R.id.photo_browsing_after_content);
        mDiariesPhotoview = getView().findViewById(R.id.diaries_photoview);
        DisplayMetrics metric = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsHeight = metric.heightPixels;
        windowsWight = metric.widthPixels;

            Glide.with(getActivity())
                    .load(mData.getImg())
                    .into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();
                    int imageHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                    ViewGroup.LayoutParams layoutParams = mDiariesPhotoview.getLayoutParams();
                    layoutParams.height = imageHeight;
                    layoutParams.width = windowsWight;
                    mDiariesPhotoview.setLayoutParams(layoutParams);
                    mDiariesPhotoview.setImageDrawable(resource);
                }
            });



        mDiariesPhotoview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag){
                    mTextContent.setEllipsize(null);
                    mTextContent.setSingleLine(false);
                    mTextContent.setMaxLines(5);
                    flag=false;
                }else {
                    mTextContent.setEllipsize(null);
                    mTextContent.setSingleLine(true);
                    mTextContent.setMaxLines(1);
                    flag=true;
                }
            }
        });
    }
}
