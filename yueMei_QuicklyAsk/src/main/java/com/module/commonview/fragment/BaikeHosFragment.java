package com.module.commonview.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.OtherListApi;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.bean.HosListData;
import com.module.home.controller.adapter.ProjectHosAdapter;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 百科四级页面相关医院
 * Created by 裴成浩 on 2019/5/5
 */
public class BaikeHosFragment extends YMBaseFragment {

    @BindView(R.id.baike_hos_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.baike_hos_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.baike_hos_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.baike_hos_not_view)
    LinearLayout mNotView;

    private OtherListApi otherListApi;
    private String TAG = "BaikeHosFragment";

    private String mKey;
    private String mId;
    private String mParentId;
    private int mPage = 1;
    private String latitude;
    private String longitude;
    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private String mSort = "1";                                 //排序的选中id
    private ProjectHosAdapter searchResultsTaoAdapter;

    public static BaikeHosFragment newInstance(String key, String id, String parentId) {
        BaikeHosFragment fragment = new BaikeHosFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("id", id);
        bundle.putString("parentId", parentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_baike_hos;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mId = getArguments().getString("id");
        mParentId = getArguments().getString("parentId");

        latitude = mFunctionManager.loadStr(FinalConstant.DW_LATITUDE, "0");
        longitude = mFunctionManager.loadStr(FinalConstant.DW_LONGITUDE, "0");

        mScreen.initView(false);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener1() {
            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                    }
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }
        });

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
    }

    @Override
    protected void initData(View view) {
        otherListApi = new OtherListApi();
        cityPop = new BaseCityPopwindows(mContext, mScreen);
        setSortData();
        loadingData();

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }
                refresh();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        otherListApi.getHashMap().clear();
        otherListApi.addData("key", mKey);               //筛选词
        otherListApi.addData("page", mPage + "");        //页码
        otherListApi.addData("id", mId);                 //四级id
        otherListApi.addData("parentId", mParentId);     //二级id
        otherListApi.addData("sort", mSort);             //排序
        otherListApi.addData("kind", "0");               //筛选
        otherListApi.addData("flag", "5");               //解析模型
        otherListApi.addData("lon", longitude);
        otherListApi.addData("lat", latitude);

        otherListApi.getCallBack(mContext, otherListApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                List<HosListData> lvHotIssueData = JSONUtil.jsonToArrayList(serverData.data, HosListData.class);
                setNotDataView(lvHotIssueData);
                mPage++;
                if (mRefresh != null) {
                    //刷新隐藏
                    mRefresh.finishRefresh();
                    if (lvHotIssueData.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }

                    if (searchResultsTaoAdapter == null) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        mRecycler.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(10), Utils.getLocalColor(mContext, R.color._f6)));
                        mRecycler.setLayoutManager(linearLayoutManager);

                        searchResultsTaoAdapter = new ProjectHosAdapter(mContext, lvHotIssueData);
                        mRecycler.setAdapter(searchResultsTaoAdapter);

                        searchResultsTaoAdapter.setOnEventClickListener(new ProjectHosAdapter.OnEventClickListener() {
                            @Override
                            public void onItemClick(View v,HosListData data, int pos) {

                                String hosid = data.getHos_id();
                                HashMap<String, String> event_params = data.getEvent_params();
                                event_params.put("id", mId);
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BAIKE_HOSPITALLIST_CLICK, (pos + 1) + ""), event_params, new ActivityTypeData("126"));
                                Intent it = new Intent(mContext, HosDetailActivity.class);
                                it.putExtra("hosid", hosid);
                                mContext.startActivity(it);
                            }
                        });

                    } else {
                        searchResultsTaoAdapter.addData(lvHotIssueData);
                    }
                }
            }
        });
    }

    /**
     * 排序数据
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("3");
        a2.setName("价格从低到高");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("4");
        a4.setName("销量最高");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("5");
        a5.setName("日记最多");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("7");
        a6.setName("离我最近");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a2);
        lvSortData.add(a6);
        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (sortPop != null) {
                    sortPop.dismiss();
                    mSort = sortId;
                    mScreen.initSortView(sortPop.isShowing());
                    mScreen.setSortTitle(sortName);
                }
                refresh();
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    /**
     * 设置没有数据的view
     * @param lvBBslistData
     */
    private void setNotDataView(List<HosListData> lvBBslistData) {
        if(mPage == 1 && lvBBslistData.size()==0){
            mNotView.setVisibility(View.VISIBLE);
            mRefresh.setVisibility(View.GONE);
        }else{
            mNotView.setVisibility(View.GONE);
            mRefresh.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 刷新
     */
    private void refresh() {
        searchResultsTaoAdapter = null;
        mPage = 1;
        loadingData();
    }
}
