package com.module.commonview.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.view.FocusButton2;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;

import java.util.HashMap;

import butterknife.BindView;

public class PostTopTitleFragment extends YMBaseFragment {

    @BindView(R.id.adiary_user_title)
    TextView userTitle;                 //文案
    @BindView(R.id.adiary_user_diary_numer)
    TextView userDiaryNumer;           //日记数
    @BindView(R.id.adiary_user_click)
    LinearLayout userClick;           //头像点击
    @BindView(R.id.adiary_user_image)
    ImageView userImage;                //头像
    @BindView(R.id.diary_list_user_name)
    TextView userName;                  //昵称
    @BindView(R.id.diary_list_user_flag)
    ImageView userFlag;                 //标识
    @BindView(R.id.diary_list_user_date)
    TextView writeDate;                  //日期
    @BindView(R.id.iv_back)
    ImageView iv_back;                   //返回键

    @BindView(R.id.adiary_list_focus)
    FocusButton2 mFocus;           //关注按钮

    @BindView(R.id.adiary_list_positioning_title)
    TextView mPositioningTitle;           //删除按钮

    private String mDiaryId;
    private FocusAndCancelApi mFocusAndCancelApi;       //关注取消关注请求类
    private DiaryUserdataBean mUserdata;
    private DiaryListData mDiaryListData;
    private PostListData mPostListData;
    private HashMap<String, String> followClickData;
    private String title;
    private String shareTotalNumber;

    /**
     * 日记本初始化
     *
     * @param data
     * @param diaryId
     * @return
     */
    public static PostTopTitleFragment newInstance(DiaryListData data, String diaryId) {
        PostTopTitleFragment fragment = new PostTopTitleFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("diaryId", diaryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    /**
     * 帖子初始化
     *
     * @param data
     * @param diaryId
     * @return
     */
    public static PostTopTitleFragment newInstance(PostListData data, String diaryId) {
        PostTopTitleFragment fragment = new PostTopTitleFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("diaryId", diaryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDiaryId = getArguments().getString("diaryId");

        if (isDiaries()) {
            mDiaryListData = getArguments().getParcelable("data");
            mUserdata = mDiaryListData.getUserdata();
            followClickData = mDiaryListData.getFollowClickData();
            title = mDiaryListData.getTitle();
            shareTotalNumber = mDiaryListData.getShareTotalNumber();
        } else {
            mPostListData = getArguments().getParcelable("data");
            mUserdata = mPostListData.getUserdata();
           followClickData = mPostListData.getFollowClickData();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_adiary_top_title;
    }

    @Override
    protected void initView(View view) {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostTopTitleFragment.this.getActivity().finish();
            }
        });
    }

    @Override
    protected void initData(View view) {
        mFocusAndCancelApi = new FocusAndCancelApi();

        initUserData();
    }

    private void initUserData() {
        mFunctionManager.setCircleImageSrc(userImage, mUserdata.getAvatar());  //设置头像
        userName.setText(mUserdata.getName());          //设置昵称
        writeDate.setText(mUserdata.getLable());        //设置日期时间

        //如果是日记
        if(!TextUtils.isEmpty(title)){
            userTitle.setVisibility(View.VISIBLE);
            userTitle.setText(title);
        }else{
            userTitle.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(shareTotalNumber)){
            userDiaryNumer.setVisibility(View.VISIBLE);
            userDiaryNumer.setText(shareTotalNumber);
        }else{
            userDiaryNumer.setVisibility(View.GONE);
        }

        //浏览自己的
        if (isSeeOneself()) {
            //关注隐藏，定位显示
            mFocus.setVisibility(View.GONE);

            mPositioningTitle.setVisibility(View.VISIBLE);

            if (isDiaries()) {
                mPositioningTitle.setText("删除日记");
            } else {
                mPositioningTitle.setText("删除帖子");
            }

        } else {                //浏览他人的

            //关注显示，定位隐藏
            mFocus.setVisibility(View.VISIBLE);
            mPositioningTitle.setVisibility(View.GONE);

            //设置标签
            switch (mUserdata.getTalent()) {
                case "0":
                    userFlag.setVisibility(View.GONE);
                    break;
                case "10":                                      //悦美官方
                    userFlag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(userFlag, R.drawable.yuemei_officia_listl);
                    break;
                case "11":                                      //悦美达人
                    userFlag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(userFlag, R.drawable.talent_big);
                    break;
                case "12":                                      //认证医生
                    userFlag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(userFlag, R.drawable.yuemei_renzheng_2x);
                    break;
                case "13":                                      //认证医院
                    userFlag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(userFlag, R.drawable.user_tag_7_2x);
                    break;
            }

            //设置关注按钮
            setFocusButton();

            //关注按钮点击
            mFocus.setFocusClickListener(mUserdata.getIs_following(), new FocusButton2.ClickCallBack() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(followClickData);
                    FocusAndCancel();
                }
            });
        }

        //其他点击事件
        setMultiOnClickListener(userClick, mPositioningTitle);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.adiary_user_click:                    //查看用户点击
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_USER_AVATAR_CLICK),mUserdata.getEvent_params(),new ActivityTypeData("45"));
                WebUrlTypeUtil.getInstance(mContext).urlToApp(mUserdata.getUrl(), "0", "0");
                break;
            case R.id.adiary_list_positioning_title:        //删除按钮点击
                if (onEventClickListener != null && isSeeOneself()) {
                    DiariesDeleteData deleteData = new DiariesDeleteData();
                    deleteData.setId(mDiaryId);
                    deleteData.setZhuTie(true);
                    deleteData.setDiaries(false);
                    deleteData.setReplystr("0");
                    onEventClickListener.onDeleteClick(deleteData);
                }
                break;
        }
    }

    /**
     * 设置关注按钮
     */
    public void setFocusButton() {

        switch (mUserdata.getIs_following()) {
            case "0":               //未关注
                mFocus.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                break;
            case "1":               //已关注
                mFocus.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                break;
            case "2":               //互相关注
                mFocus.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                break;
        }
    }

    /**
     * 关注和取消关注
     */
    private void FocusAndCancel() {
        mFocusAndCancelApi.addData("objid", mUserdata.getObj_id());
        mFocusAndCancelApi.addData("type", mUserdata.getObj_type());
        mFocusAndCancelApi.getCallBack(mContext, mFocusAndCancelApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData mFocusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (mFocusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mUserdata.setIs_following("0");
                                mFocus.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                                MyToast.makeTextToast1(mContext, "取关成功", MyToast.SHOW_TIME).show();
                                break;
                            case "1":          //已关注
                                mUserdata.setIs_following("1");
                                mFocus.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                                MyToast.makeTextToast1(mContext, serverData.message, MyToast.SHOW_TIME).show();
                                break;
                            case "2":          //互相关注
                                mUserdata.setIs_following("2");
                                mFocus.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                                MyToast.makeTextToast1(mContext, serverData.message, MyToast.SHOW_TIME).show();
                                break;
                        }

                        //关注按钮点击
                        mFocus.setFocusClickListener(mUserdata.getIs_following(), new FocusButton2.ClickCallBack() {
                            @Override
                            public void onClick(View v) {
                                YmStatistics.getInstance().tongjiApp(followClickData);
                                FocusAndCancel();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 判断是日记还是帖子
     *
     * @return true 日记
     */
    private boolean isDiaries() {
        return getArguments().getParcelable("data") instanceof DiaryListData;
    }


    /**
     * 判断是查看的自己日记还是别人的日记
     *
     * @return ：ture:查看自己的，false:查看别人的
     */
    private boolean isSeeOneself() {
        return mUserdata.getId().equals(Utils.getUid());
    }

    public interface OnEventClickListener {

        void onDeleteClick(DiariesDeleteData deleteData);   //删除点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
