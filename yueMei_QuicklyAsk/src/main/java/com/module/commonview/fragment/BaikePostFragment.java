package com.module.commonview.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.OtherListApi;
import com.module.community.controller.adapter.CommunityPostAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 百科四级页面相关科普
 * Created by 裴成浩 on 2019/5/5
 */
public class BaikePostFragment extends YMBaseFragment {

    @BindView(R.id.baike_post_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.baike_post_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.baike_post_not_view)
    LinearLayout mNotView;
    private OtherListApi otherListApi;
    private String TAG = "BaikePostFragment";

    private int mPage = 1;
    private String mKey;
    private String mId;
    private String mParentId;
    private CommunityPostAdapter communityPostAdapter;

    public static BaikePostFragment newInstance(String key, String id, String parentId) {
        BaikePostFragment fragment = new BaikePostFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("id", id);
        bundle.putString("parentId", parentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_baike_post;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mId = getArguments().getString("id");
        mParentId = getArguments().getString("parentId");

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                communityPostAdapter = null;
                mPage = 1;
                loadingData();
            }
        });
    }

    @Override
    protected void initData(View view) {
        otherListApi = new OtherListApi();
        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        otherListApi.addData("key", mKey);               //筛选词`
        otherListApi.addData("page", mPage + "");        //页码
        otherListApi.addData("id", mId);                 //四级id
        otherListApi.addData("parentId", mParentId);     //二级id
        otherListApi.addData("sort", "0");              //排序
        otherListApi.addData("kind", "0");              //筛选
        otherListApi.addData("flag", "6");              //解析模型

        otherListApi.getCallBack(mContext, otherListApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                List<BBsListData550> lvBBslistData = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);
                setNotDataView(lvBBslistData);
                mPage++;

                if (mRefresh != null) {
                    //刷新隐藏
                    mRefresh.finishRefresh();
                    if (lvBBslistData.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }

                    if (communityPostAdapter == null) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        mRecycler.setLayoutManager(linearLayoutManager);

                        communityPostAdapter = new CommunityPostAdapter(mContext, lvBBslistData);
                        mRecycler.setAdapter(communityPostAdapter);
                        communityPostAdapter.setOnItemCallBackListener(new CommunityPostAdapter.ItemCallBackListener() {
                            @Override
                            public void onItemClick(View v, int pos) {
                                if (pos != communityPostAdapter.getItemCount()) {
                                    List<BBsListData550> bBsListData550s = communityPostAdapter.getmData();
                                    String url = bBsListData550s.get(pos).getAppmurl();
                                    HashMap<String, String> event_params = bBsListData550s.get(pos).getEvent_params();
                                    event_params.put("id", mId);
                                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BAIKE_KEPULIST_CLICK, (pos + 1) + ""), event_params, new ActivityTypeData("127"));
                                    if (url.length() > 0) {
                                        if ("404".equals(communityPostAdapter.getmData().get(pos).getAskorshare())) {
                                            mFunctionManager.showShort("该帖子已被删除");
                                        } else {
                                            WebUrlTypeUtil.getInstance(getActivity()).urlToApp(url, "0", "0");
                                        }
                                    }
                                }
                            }
                        });
                    } else {
                        communityPostAdapter.addData(lvBBslistData);
                    }
                }
            }
        });
    }

    /**
     * 设置没有数据的view
     * @param lvBBslistData
     */
    private void setNotDataView(List<BBsListData550> lvBBslistData) {
        if(mPage == 1 && lvBBslistData.size()==0){
            mNotView.setVisibility(View.VISIBLE);
            mRefresh.setVisibility(View.GONE);
        }else{
            mNotView.setVisibility(View.GONE);
            mRefresh.setVisibility(View.VISIBLE);
        }
    }

}
