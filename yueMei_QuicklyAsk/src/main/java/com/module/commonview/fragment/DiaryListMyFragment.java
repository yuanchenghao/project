package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.PostoperativeRecoveryActivity;
import com.module.commonview.module.bean.DiaryCompareBean;
import com.module.commonview.module.bean.DiaryHosDocBean;
import com.module.commonview.module.bean.DiaryHosDocFanXian;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryRateBean;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.view.PostFlowLayoutGroup;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.view.MzRatingBar;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.my.controller.activity.AddMoreNoteMessageActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 浏览自己时的Fragment
 * Created by 裴成浩 on 2018/6/14.
 */
public class DiaryListMyFragment extends YMBaseFragment {

    @BindView(R.id.diary_list_before_photo_container)
    RelativeLayout mBeforeContainer;
    @BindView(R.id.diary_list_before_photo_backgrod)
    LinearLayout mBeforeBackgrod;
    @BindView(R.id.diary_list_before_photo)
    ImageView mBeforePhoto;
    @BindView(R.id.diary_list_before_atlas_click)
    LinearLayout mBeforeAtlasClick;
    @BindView(R.id.diary_list_before_atlas)
    LinearLayout mBeforeAtlas;
    @BindView(R.id.diary_list_before_atlas_num)
    TextView mBeforeAtlasNum;
    @BindView(R.id.diary_list_before_editor)
    TextView mBeforeEditor;

    @BindView(R.id.diary_list_after_photo_container)
    RelativeLayout mAfterContainer;
    @BindView(R.id.diary_list_after_photo_backgrod)
    LinearLayout mAfterBackgrod;
    @BindView(R.id.diary_list_after_photo)
    ImageView mAfterPhoto;
    @BindView(R.id.diary_list_after_atlas_click)
    LinearLayout mAfterAtlasClick;
    @BindView(R.id.diary_list_after_atlas)
    LinearLayout mAfterAtlas;
    @BindView(R.id.diary_list_after_atlas_num)
    TextView mAfterAtlasNum;
    @BindView(R.id.diary_list_after_editor)
    TextView mAfterEditor;

    @BindView(R.id.adiary_list_cash_back_container)
    FrameLayout myCashBackContainer;

    @BindView(R.id.diary_list_service_click)
    LinearLayout myserviceClick;
    @BindView(R.id.diary_list_service_hos)
    TextView myserviceHos;
    @BindView(R.id.diary_list_service_doc)
    TextView myserviceDoc;
    @BindView(R.id.diary_list_service_score)
    MzRatingBar myserviceScore;
    @BindView(R.id.diary_list_service_editor)
    ImageView myserviceEditor;

    //服务信息
    @BindView(R.id.adiary_my_user_service_click)
    LinearLayout userServiceClick;

    //标签
    @BindView(R.id.diary_list_other_tag_container)
    FlowLayout userFlowLayout;

    private DiaryListData mData;
    private String mDiaryId;
    private final String TAG = "DiaryListMyFragment";
    private final int SUPPLEMENTARY_INFORMATION = 1;        //补充信息
    private final int BEFORE_OR_AFTER_PHOTO = 2;            //术前术后照片选择
    private BaseWebViewClientMessage webViewClientManager;
    private DiaryCompareBean mCompareData;
    private DiaryCompareBean.BeforeBean before;
    private DiaryCompareBean.AfterBean after;

    public static DiaryListMyFragment newInstance(DiaryListData data, String id) {
        DiaryListMyFragment fragment = new DiaryListMyFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = getArguments().getParcelable("data");
        mDiaryId = getArguments().getString("id");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_adiary_list_my_user;
    }

    @Override
    protected void initView(View view) {

        //术前术后图集隐藏
        mBeforeAtlas.setVisibility(View.GONE);
        mAfterAtlas.setVisibility(View.GONE);

        //术前术后编辑按钮显示
        mBeforeEditor.setVisibility(View.VISIBLE);
        mAfterEditor.setVisibility(View.VISIBLE);

        //设置照片的高度
        int photoHeigth = (windowsWight - Utils.dip2px(50)) / 2;

        ViewGroup.LayoutParams layoutParams1 = mBeforeContainer.getLayoutParams();
        layoutParams1.height = photoHeigth;
        mBeforeContainer.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams2 = mAfterContainer.getLayoutParams();
        layoutParams2.height = photoHeigth;
        mAfterContainer.setLayoutParams(layoutParams2);
    }

    @Override
    protected void initData(View view) {
        webViewClientManager = new BaseWebViewClientMessage(mContext);
        initDiaryData();
    }

    /**
     * 设置数据
     */
    @SuppressLint("SetTextI18n")
    private void initDiaryData() {
        DiaryHosDocBean hosDoc = mData.getHosDoc();     //服务的医生医院数据
        DiaryRateBean mRateData = mData.getRate();      //服务评分数据

        //术前照片和术后照片
        mCompareData = mData.getCompare();

        if (mCompareData != null) {
            before = mCompareData.getBefore();
            after = mCompareData.getAfter();
            Log.e(TAG, "before.getImg() === " + before.getImg());
            Log.e(TAG, "after.getImg() === " + after.getImg());
            if (!TextUtils.isEmpty(before.getImg())) {
                mBeforeAtlasClick.setVisibility(View.VISIBLE);
                mBeforeBackgrod.setVisibility(View.GONE);
                mFunctionManager.setRoundImageSrc(mBeforePhoto, before.getImg(), Utils.dip2px(4));
            } else {
                mBeforeAtlasClick.setVisibility(View.GONE);
                mBeforeBackgrod.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(after.getImg())) {
                mAfterAtlasClick.setVisibility(View.VISIBLE);
                mAfterBackgrod.setVisibility(View.GONE);
                mFunctionManager.setRoundImageSrc(mAfterPhoto, after.getImg(), Utils.dip2px(4));
            } else {
                mAfterAtlasClick.setVisibility(View.GONE);
                mAfterBackgrod.setVisibility(View.VISIBLE);
            }

            //初始化返现类型数据
            CashBackContainer();

            //初始化服务医生医院
            Log.e(TAG, "hosDoc == " + hosDoc);
            initDocAndHos(hosDoc, mRateData);

            //加载日记标签
            setTagData(hosDoc);

            //评分编辑点击事件
            myserviceEditor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, AddMoreNoteMessageActivity.class);
                    intent.putExtra("mainQid", mDiaryId);
                    startActivityForResult(intent, SUPPLEMENTARY_INFORMATION);
                }
            });

            //上传术前照片点击
            mBeforeBackgrod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_COMPARED_IMG_CLICK,"0"),before.getEvent_params(),new ActivityTypeData("45"));
                    jumpDiariesAdnPostsActivity();
                }
            });

            //上传术后照片点击
            mAfterBackgrod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_COMPARED_IMG_CLICK,"1"),after.getEvent_params(),new ActivityTypeData("45"));
                    jumpDiariesAdnPostsActivity();
                }
            });

            //术前照片编辑
            mBeforeEditor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jumpDiariesAdnPostsActivity();
                }
            });

            //术后照片编辑
            mAfterEditor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jumpDiariesAdnPostsActivity();
                }
            });
            //术前照片
            mBeforePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jumpDiariesAdnPostsActivity();
                }
            });
            //术后照片
            mAfterPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jumpDiariesAdnPostsActivity();
                }
            });
        }

    }

    /**
     * 初始化医生医院
     *
     * @param hosDoc
     * @param mRateData
     */
    @SuppressLint("SetTextI18n")
    private void initDocAndHos(DiaryHosDocBean hosDoc, DiaryRateBean mRateData) {
        if (hosDoc != null) {
            Log.e(TAG, "hosDoc.getHospital_name() == " + hosDoc.getHospital_name());
            Log.e(TAG, "hosDoc.getDoctor_name() == " + hosDoc.getDoctor_name());
            myserviceHos.setText("项目服务医院：" + hosDoc.getHospital_name());
            myserviceDoc.setText("项目服务医生：" + hosDoc.getDoctor_name());
            myserviceScore.setMax(100);
            myserviceScore.setProgress(Integer.parseInt(mRateData.getRateSale()));    //评分

            userServiceClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, AddMoreNoteMessageActivity.class);
                    intent.putExtra("mainQid", mDiaryId);
                    startActivityForResult(intent, SUPPLEMENTARY_INFORMATION);
                }
            });

            //判断显示哪个
            if (!TextUtils.isEmpty(hosDoc.getHospital_name()) && !TextUtils.isEmpty(hosDoc.getDoctor_name())) {
                userServiceClick.setVisibility(View.GONE);
                myserviceClick.setVisibility(View.VISIBLE);
            } else {
                userServiceClick.setVisibility(View.VISIBLE);
                myserviceClick.setVisibility(View.GONE);
            }
        } else {
            userServiceClick.setVisibility(View.GONE);
            myserviceClick.setVisibility(View.VISIBLE);
        }

    }


    /**
     * 设置标签数据
     *
     * @param hosDoc
     */
    private void setTagData(DiaryHosDocBean hosDoc) {
        List<DiaryTagList> tags = mData.getTag();
        //添加医生标签
        if (hosDoc != null) {
            String doctor_id = hosDoc.getDoctor_id();
            String doctor_name = hosDoc.getDoctor_name();
            String title = hosDoc.getTitle();
            Log.e(TAG, "doctor_id == " + doctor_id);
            Log.e(TAG, "doctor_name == " + doctor_name);
            Log.e(TAG, "title == " + title);

            if (!TextUtils.isEmpty(doctor_name) && !TextUtils.isEmpty(doctor_id)) {
                String docName;
                if (!TextUtils.isEmpty(title)) {
                    docName = doctor_name + " " + title;
                } else {
                    docName = doctor_name;
                }
                DiaryTagList diaryTagList = new DiaryTagList(doctor_id, hosDoc.getDoctor_url(), docName);
                tags.add(diaryTagList);
            }
        }
        //适配数据
        PostFlowLayoutGroup mPostFlowLayoutGroup = new PostFlowLayoutGroup(mContext, userFlowLayout, tags);
        mPostFlowLayoutGroup.setClickCallBack(new PostFlowLayoutGroup.ClickCallBack() {
            @Override
            public void onClick(View v, int pos, DiaryTagList data) {
                if (!TextUtils.isEmpty(data.getUrl())) {
                    HashMap<String, String> event_params = data.getEvent_params();
                    if (event_params != null) {
                        event_params.put("id",mDiaryId);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TAG_CLICK, (pos + 1) + ""), event_params);
                        Intent it = new Intent(mContext, SearchAllActivity668.class);
                        it.putExtra("keys", data.getName());
                        it.putExtra("results", true);
                        it.putExtra("position", 1);
                        startActivity(it);
                    } else {
                        Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                        intent.putExtra("docId", data.getId());
                        intent.putExtra("docName", data.getName());
                        intent.putExtra("partId", "");
                        startActivity(intent);
                    }
                }

            }
        });
    }

    /**
     * 浏览自己数据时返现类型设置
     */
    private void CashBackContainer() {
        final DiaryHosDocFanXian fanxian = mData.getFanxian();
        if (fanxian.getCashback_type() != null) {
            switch (fanxian.getCashback_type()) {
                case "1":   //返现进度
                    View view1 = mInflater.inflate(R.layout.adiary_list_my_user_cash_back1, myCashBackContainer);

                    ProgressBar back1Progess = view1.findViewById(R.id.adiary_list_my_user_cash_back1_progess);
                    TextView back1Require = view1.findViewById(R.id.adiary_list_my_user_cash_back1_require);

                    mFunctionManager.setTextValue(view1, R.id.adiary_list_my_user_cash_back1_price, "￥" + fanxian.getCashback_money());
                    mFunctionManager.setTextValue(view1, R.id.adiary_list_my_user_cash_back1_centen, fanxian.getCashback_desc());

                    back1Progess.setProgress(Integer.parseInt(fanxian.getCashback_degree()));

                    back1Require.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
                    back1Require.getPaint().setAntiAlias(true);                  //抗锯齿
                    back1Require.setText(fanxian.getHref_title());
                    //返现要求
                    back1Require.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = fanxian.getUrl();
                            if (url.startsWith("http")) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                            } else {
                                webViewClientManager.setView(userServiceClick);
                                webViewClientManager.showWebDetail(url);
                            }
                        }
                    });

                    break;
                case "2":       //人工审核
                    View view2 = mInflater.inflate(R.layout.adiary_list_my_user_cash_back2, myCashBackContainer);
                    mFunctionManager.setTextValue(view2, R.id.adiary_list_my_user_cash_back2_title, fanxian.getCashback_title());
                    TextView back2Infor = view2.findViewById(R.id.adiary_list_my_user_cash_back2_infor);
                    back2Infor.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
                    back2Infor.getPaint().setAntiAlias(true);                  //抗锯齿
                    back2Infor.setText(fanxian.getHref_title());
                    //填写收款信息
                    back2Infor.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = fanxian.getUrl();
                            if (url.startsWith("http")) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                            } else {
                                webViewClientManager.setView(userServiceClick);
                                webViewClientManager.showWebDetail(url);
                            }
                        }
                    });

                    break;
                case "3":       //审核通过
                    View view3 = mInflater.inflate(R.layout.adiary_list_my_user_cash_back3, myCashBackContainer);
                    mFunctionManager.setTextValue(view3, R.id.adiary_list_my_user_cash_back3_title, fanxian.getCashback_title());
                    mFunctionManager.setTextValue(view3, R.id.adiary_list_my_user_cash_back3_centent, fanxian.getCashback_desc());

                    TextView back3Consul = view3.findViewById(R.id.adiary_list_my_user_cash_back3_consul);
                    back3Consul.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
                    back3Consul.getPaint().setAntiAlias(true);                  //抗锯齿
                    back3Consul.setText(fanxian.getHref_title());
                    //咨询小悦悦
                    back3Consul.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = fanxian.getUrl();
                            if (url.startsWith("http")) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                            } else {
                                webViewClientManager.setView(userServiceClick);
                                webViewClientManager.showWebDetail(url);
                            }
                        }
                    });

                    break;
                case "4":       //无法返现
                    View view4 = mInflater.inflate(R.layout.adiary_list_my_user_cash_back4, myCashBackContainer);
                    mFunctionManager.setTextValue(view4, R.id.adiary_list_my_user_cash_back4_title, fanxian.getCashback_title());

                    TextView back4Consul = view4.findViewById(R.id.adiary_list_my_user_cash_back4_consul);
                    back4Consul.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
                    back4Consul.getPaint().setAntiAlias(true);                  //抗锯齿
                    back4Consul.setText(fanxian.getHref_title());
                    //有问题
                    back4Consul.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = fanxian.getUrl();
                            if (url.startsWith("http")) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                            } else {
                                webViewClientManager.setView(userServiceClick);
                                webViewClientManager.showWebDetail(url);
                            }
                        }
                    });

                    break;
                case "5":       //代金券
                    View view5 = mInflater.inflate(R.layout.adiary_list_my_user_cash_back5, myCashBackContainer);
                    mFunctionManager.setTextValue(view5, R.id.adiary_list_my_user_cash_back4_title, fanxian.getCashback_title());
                    mFunctionManager.setTextValue(view5, R.id.adiary_list_my_user_cash_back4_content, fanxian.getCashback_desc());

                    TextView back5Consul = view5.findViewById(R.id.adiary_list_my_user_cash_back5_consul);
                    back5Consul.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
                    back5Consul.getPaint().setAntiAlias(true);                  //抗锯齿
                    back5Consul.setText(fanxian.getHref_title());


                    //咨询小悦悦
                    back5Consul.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = fanxian.getUrl();
                            if (url.startsWith("http")) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                            } else {
                                webViewClientManager.setView(userServiceClick);
                                webViewClientManager.showWebDetail(url);
                            }
                        }
                    });

                    break;
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SUPPLEMENTARY_INFORMATION:             //补充信息返回
                if (resultCode == 100) {
                    String hos = data.getStringExtra("hos");
                    String doc = data.getStringExtra("doc");
                    int progress = data.getIntExtra("progress", 0);
                    if (!TextUtils.isEmpty(hos) && !TextUtils.isEmpty(doc)) {

                        myserviceClick.setVisibility(View.VISIBLE);
                        userServiceClick.setVisibility(View.GONE);
                        myserviceHos.setText("项目服务医院：" + hos);
                        myserviceDoc.setText("项目服务医生：" + doc);

                    } else if (!TextUtils.isEmpty(hos)) {
                        myserviceClick.setVisibility(View.VISIBLE);
                        userServiceClick.setVisibility(View.GONE);
                        myserviceHos.setText("项目服务医院：" + hos);
                        myserviceDoc.setText("项目服务医生：暂未添加");

                    } else if (!TextUtils.isEmpty(doc)) {
                        myserviceClick.setVisibility(View.VISIBLE);
                        userServiceClick.setVisibility(View.GONE);
                        myserviceHos.setText("项目服务医院：暂未添加");
                        myserviceDoc.setText("项目服务医生：" + doc);
                    } else {
                        myserviceClick.setVisibility(View.GONE);
                        userServiceClick.setVisibility(View.VISIBLE);
                    }

                    myserviceScore.setProgress(progress);    //评分
                }
                break;
            case BEFORE_OR_AFTER_PHOTO:                     //术前术后照片选择
                String beforePhoto = data.getStringExtra("before");
                String afterPhoto = data.getStringExtra("after");

                Log.e(TAG, "beforePhoto == " + beforePhoto);
                Log.e(TAG, "afterPhoto == " + afterPhoto);

                if (!TextUtils.isEmpty(beforePhoto)) {
                    mBeforeAtlasClick.setVisibility(View.VISIBLE);
                    mBeforeBackgrod.setVisibility(View.GONE);
                    mCompareData.getBefore().setImg(beforePhoto);
                    mFunctionManager.setImageSrc(mBeforePhoto, mCompareData.getBefore().getImg());
                } else if (TextUtils.isEmpty(before.getImg())) {
                    mBeforeAtlasClick.setVisibility(View.GONE);
                    mBeforeBackgrod.setVisibility(View.VISIBLE);
                }
                if (!TextUtils.isEmpty(afterPhoto)) {
                    mAfterAtlasClick.setVisibility(View.VISIBLE);
                    mBeforeBackgrod.setVisibility(View.GONE);
                    mCompareData.getAfter().setImg(afterPhoto);
                    mFunctionManager.setImageSrc(mAfterPhoto, mCompareData.getAfter().getImg());
                } else if (TextUtils.isEmpty(after.getImg())) {
                    mAfterAtlasClick.setVisibility(View.GONE);
                    mAfterBackgrod.setVisibility(View.VISIBLE);
                }

                break;
        }
    }


    /**
     * 跳转到术前术后照片页面
     */
    private void jumpDiariesAdnPostsActivity() {
        Intent intent = new Intent(mContext, PostoperativeRecoveryActivity.class);
        intent.putExtra("diary_id", mDiaryId);
        startActivityForResult(intent, BEFORE_OR_AFTER_PHOTO);
    }

    //再写一页接口回调
    private OnWritePageCallBackListener onWritePageCallBackListener;

    public interface OnWritePageCallBackListener {
        void onWritePageClick(View v);
    }

    public void setOnWritePageCallBackListener(OnWritePageCallBackListener onWritePageCallBackListener) {
        this.onWritePageCallBackListener = onWritePageCallBackListener;
    }
}
