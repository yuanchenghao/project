package com.module.commonview.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.api.OtherListApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.model.bean.BBsListData550;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.ProjectDiaryAdapter;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 百科四级页面相关日记
 * Created by 裴成浩 on 2019/5/5
 */
public class BaikeDailyFragment extends YMBaseFragment {

    @BindView(R.id.baike_daily_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.baike_daily_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.baike_daily_not_view)
    LinearLayout mNotView;
    private OtherListApi otherListApi;
    private String TAG = "BaikeDailyFragment";

    private int mPage = 1;
    private String mKey;
    private String mId;
    private String mParentId;
    private ProjectDiaryAdapter searchResultsTaoAdapter;
    private int mTempPos = -1;

    public static BaikeDailyFragment newInstance(String key, String id, String parentId) {
        BaikeDailyFragment fragment = new BaikeDailyFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("id", id);
        bundle.putString("parentId", parentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_baike_daily;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mId = getArguments().getString("id");
        mParentId = getArguments().getString("parentId");

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                searchResultsTaoAdapter = null;
                mPage = 1;
                loadingData();
            }
        });
    }

    @Override
    protected void initData(View view) {
        otherListApi = new OtherListApi();
        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        otherListApi.addData("key", mKey);               //筛选词
        otherListApi.addData("page", mPage + "");        //页码
        otherListApi.addData("id", mId);                 //四级id
        otherListApi.addData("parentId", mParentId);     //二级id
        otherListApi.addData("sort", "0");              //排序
        otherListApi.addData("kind", "0");              //筛选
        otherListApi.addData("flag", "1");              //解析模型

        otherListApi.getCallBack(mContext, otherListApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                List<BBsListData550> lvBBslistData = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);
                if (mRefresh != null) {
                    setNotDataView(lvBBslistData);

                    mPage++;
                    //刷新隐藏
                    mRefresh.finishRefresh();
                    if (lvBBslistData.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }

                    if (searchResultsTaoAdapter == null) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        mRecycler.setLayoutManager(linearLayoutManager);

                        searchResultsTaoAdapter = new ProjectDiaryAdapter(mContext, lvBBslistData);
                        mRecycler.setAdapter(searchResultsTaoAdapter);

                        searchResultsTaoAdapter.setOnItemPersonClickListener(new ProjectDiaryAdapter.OnItemPersonClickListener() {
                            @Override
                            public void onItemClick(int pos) {
                                mTempPos = pos;
                                String appmurl = searchResultsTaoAdapter.getDatas().get(pos).getAppmurl();
                                HashMap<String, String> event_params = searchResultsTaoAdapter.getDatas().get(pos).getEvent_params();
                                if (!TextUtils.isEmpty(appmurl)) {
                                    event_params.put("id", mId);
                                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BAIKE_SHARELIST_CLICK, (pos + 1) + ""), event_params, new ActivityTypeData("122"));
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");

                                }
                            }

                            @Override
                            public void onItemPersonClick(String id, int pos) {
                                mTempPos = pos;
                                Intent intent = new Intent(mContext, PersonCenterActivity641.class);
                                intent.putExtra("id", id);
                                startActivityForResult(intent, 18);
                            }
                        });
                    } else {
                        searchResultsTaoAdapter.addData(lvBBslistData);
                    }
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (mTempPos >= 0) {
                isFocu(searchResultsTaoAdapter.getmHotIssues().get(mTempPos).getUser_id());
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                searchResultsTaoAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }

    /**
     * 设置没有数据的view
     * @param lvBBslistData
     */
    private void setNotDataView(List<BBsListData550> lvBBslistData) {
        if(mPage == 1 && lvBBslistData.size()==0){
            mNotView.setVisibility(View.VISIBLE);
            mRefresh.setVisibility(View.GONE);
        }else{
            mNotView.setVisibility(View.GONE);
            mRefresh.setVisibility(View.VISIBLE);
        }
    }

}
