package com.module.commonview.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.OtherListApi;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.ProjectAnswerAdapter;
import com.module.home.model.bean.QuestionListData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 百科四级常见问题
 * Created by 裴成浩 on 2019/4/29
 */
public class BaikeAnswerFragment extends YMBaseFragment {
    @BindView(R.id.community_answer_refresh)
    SmartRefreshLayout mPullRefresh;
    @BindView(R.id.community_answer_list_recycler)
    RecyclerView mAnswerList;
    @BindView(R.id.baike_answer_not_view)
    LinearLayout mNotView;

    private OtherListApi otherListApi;
    private String TAG = "BaikeAnswerFragment";
    private ArrayList<QuestionListData> mAnswerData;
    private ProjectAnswerAdapter projectAnswerAdapter;
    private int mPage = 1;
    private String mKey;
    private String mId;
    private String mParentId;

    public static BaikeAnswerFragment newInstance(String key, String id, String parentId) {
        BaikeAnswerFragment fragment = new BaikeAnswerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("id", id);
        bundle.putString("parentId", parentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_baike_answer;
    }

    @Override
    protected void initView(View view) {

        mKey = getArguments().getString("key");
        mId = getArguments().getString("id");
        mParentId = getArguments().getString("parentId");

        mPullRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mPullRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                loadingData();
            }
        });

    }

    @Override
    protected void initData(View view) {
        otherListApi = new OtherListApi();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mAnswerList.setLayoutManager(linearLayoutManager);

        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        otherListApi.addData("key", mKey);               //筛选词
        otherListApi.addData("page", mPage + "");        //页码
        otherListApi.addData("id", mId);                 //四级id
        otherListApi.addData("parentId", mParentId);     //二级id
        otherListApi.addData("sort", "0");              //排序
        otherListApi.addData("kind", "0");              //筛选
        otherListApi.addData("flag", "2");              //解析模型

        otherListApi.getCallBack(mContext, otherListApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                Log.e(TAG, "json === " + data.data);
                mAnswerData = JSONUtil.jsonToArrayList(data.data, QuestionListData.class);
                setNotDataView(mAnswerData);

                mPage++;
                Log.e(TAG, "mAnswerData == " + mAnswerData.size());
                if (mPullRefresh != null) {
                    mPullRefresh.finishRefresh();
                    if (mAnswerData.size() == 0) {
                        mPullRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mPullRefresh.finishLoadMore();
                    }

                    initViewData();
                }
            }
        });
    }

    /**
     * 设置没有数据的view
     * @param lvBBslistData
     */
    private void setNotDataView(List<QuestionListData> lvBBslistData) {
        if(mPage == 1 && lvBBslistData.size()==0){
            mNotView.setVisibility(View.VISIBLE);
            mPullRefresh.setVisibility(View.GONE);
        }else{
            mNotView.setVisibility(View.GONE);
            mPullRefresh.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 加载完成后的数据设置
     */
    private void initViewData() {
        if (projectAnswerAdapter == null) {
            projectAnswerAdapter = new ProjectAnswerAdapter(mContext, mAnswerData,"");
            mAnswerList.setAdapter(projectAnswerAdapter);
            projectAnswerAdapter.setOnItemClickListener(new ProjectAnswerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int pos, QuestionListData data) {
                    String jumpUrl = data.getJumpUrl();
                    if (!TextUtils.isEmpty(jumpUrl)) {
                        HashMap<String, String> event_params = data.getEvent_params();
                        event_params.put("id", mId);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BAIKE_ASKLIST_CLICK, (pos + 1) + ""), event_params, new ActivityTypeData("123"));
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(jumpUrl);


                    }
                }
            });
        } else {
            projectAnswerAdapter.addData(mAnswerData);
        }

    }
}
