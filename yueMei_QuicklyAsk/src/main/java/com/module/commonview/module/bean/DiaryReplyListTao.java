package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
//          "url":"https://m.yuemei.com/tao/20299/",
//                  "title":"【玻尿酸】瑞蓝铂金玻尿酸    明星脸冠军打造医师 无注射费隐形消费",
//                  "price_discount":"6800",
//                  "is_tao":"1",
//                  "id":"20299",
//                  "_id":"20299",
//                  "price":"9800",
//                  "showPrice":"1",
//                  "fee_scale":"/次",
//                  "list_cover_image":"https://p24.yuemei.com/tao/2019/1129/200_200/jt191129161617_145c2d.jpg",
//                  "img":"https://p24.yuemei.com/tao/2019/1129/200_200/jt191129161617_145c2d.jpg",
//                  "fanxian":"体验项目写日记返现200元",
//                  "member_price":"-1",
//                  "dacu66_id":"0",
//                  "dacu66_img":"",
//                  "dacu66_url":"",
//                  "is_rongyun":"3",
//                  "hos_userid":"84945559",
//                  "hospital_id":"3689",
//                  "doctor_id":"88072363"
public class DiaryReplyListTao implements Parcelable {

    private String title;
    private String price_discount;
    private String url;
    private String is_tao;
    private String id;
    private String _id;
    private String price;
    private String showPrice;
    private String fee_scale;
    private String list_cover_image;
    private String img;
    private String fanxian;
    private String member_price;
    private String dacu66_id;
    private String dacu66_img;
    private String dacu66_url;
    private String is_rongyun;
    private String hos_userid;
    private String hospital_id;
    private String doctor_id;


    public DiaryReplyListTao() {
    }

    protected DiaryReplyListTao(Parcel in) {
        title = in.readString();
        price_discount = in.readString();
        url = in.readString();
        is_tao = in.readString();
        id = in.readString();
        _id = in.readString();
        price = in.readString();
        showPrice = in.readString();
        fee_scale = in.readString();
        list_cover_image = in.readString();
        img = in.readString();
        fanxian = in.readString();
        member_price = in.readString();
        dacu66_id = in.readString();
        dacu66_img = in.readString();
        dacu66_url = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
        hospital_id = in.readString();
        doctor_id = in.readString();
    }

    public static final Creator<DiaryReplyListTao> CREATOR = new Creator<DiaryReplyListTao>() {
        @Override
        public DiaryReplyListTao createFromParcel(Parcel in) {
            return new DiaryReplyListTao(in);
        }

        @Override
        public DiaryReplyListTao[] newArray(int size) {
            return new DiaryReplyListTao[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(String showPrice) {
        this.showPrice = showPrice;
    }

    public String getFee_scale() {
        return fee_scale;
    }

    public void setFee_scale(String fee_scale) {
        this.fee_scale = fee_scale;
    }

    public String getList_cover_image() {
        return list_cover_image;
    }

    public void setList_cover_image(String list_cover_image) {
        this.list_cover_image = list_cover_image;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getFanxian() {
        return fanxian;
    }

    public void setFanxian(String fanxian) {
        this.fanxian = fanxian;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public String getDacu66_id() {
        return dacu66_id;
    }

    public void setDacu66_id(String dacu66_id) {
        this.dacu66_id = dacu66_id;
    }

    public String getDacu66_img() {
        return dacu66_img;
    }

    public void setDacu66_img(String dacu66_img) {
        this.dacu66_img = dacu66_img;
    }

    public String getDacu66_url() {
        return dacu66_url;
    }

    public void setDacu66_url(String dacu66_url) {
        this.dacu66_url = dacu66_url;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIs_tao() {
        return is_tao;
    }

    public void setIs_tao(String is_tao) {
        this.is_tao = is_tao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(price_discount);
        dest.writeString(url);
        dest.writeString(is_tao);
        dest.writeString(id);
        dest.writeString(_id);
        dest.writeString(price);
        dest.writeString(showPrice);
        dest.writeString(fee_scale);
        dest.writeString(list_cover_image);
        dest.writeString(img);
        dest.writeString(fanxian);
        dest.writeString(fanxian);
        dest.writeString(member_price);
        dest.writeString(dacu66_id);
        dest.writeString(dacu66_img);
        dest.writeString(dacu66_url);
        dest.writeString(hos_userid);
        dest.writeString(hospital_id);
        dest.writeString(doctor_id);
    }
}
