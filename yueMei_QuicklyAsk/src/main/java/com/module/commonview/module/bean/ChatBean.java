package com.module.commonview.module.bean;

/**
 * Created by Administrator on 2017/12/7.
 */

public class ChatBean {

    /**
     * type : init
     * client_id : 7f00000108fc000001dc
     */

    private String type;
    private String client_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    @Override
    public String toString() {
        return "ChatBean{" +
                "type='" + type + '\'' +
                ", client_id='" + client_id + '\'' +
                '}';
    }
}
