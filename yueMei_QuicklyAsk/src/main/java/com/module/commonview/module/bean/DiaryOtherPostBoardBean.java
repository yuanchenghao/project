package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/8/1
 */
public class DiaryOtherPostBoardBean implements Parcelable {
    private String m_url;
    private String url_name;
    private String level;
    private String top_id;
    private String id;
    private String rel_id;
    private String pc_url;
    private String p_id;
    private String status;
    private String name;

    protected DiaryOtherPostBoardBean(Parcel in) {
        m_url = in.readString();
        url_name = in.readString();
        level = in.readString();
        top_id = in.readString();
        id = in.readString();
        rel_id = in.readString();
        pc_url = in.readString();
        p_id = in.readString();
        status = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(m_url);
        dest.writeString(url_name);
        dest.writeString(level);
        dest.writeString(top_id);
        dest.writeString(id);
        dest.writeString(rel_id);
        dest.writeString(pc_url);
        dest.writeString(p_id);
        dest.writeString(status);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryOtherPostBoardBean> CREATOR = new Creator<DiaryOtherPostBoardBean>() {
        @Override
        public DiaryOtherPostBoardBean createFromParcel(Parcel in) {
            return new DiaryOtherPostBoardBean(in);
        }

        @Override
        public DiaryOtherPostBoardBean[] newArray(int size) {
            return new DiaryOtherPostBoardBean[size];
        }
    };

    public String getM_url() {
        return m_url;
    }

    public void setM_url(String m_url) {
        this.m_url = m_url;
    }

    public String getUrl_name() {
        return url_name;
    }

    public void setUrl_name(String url_name) {
        this.url_name = url_name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTop_id() {
        return top_id;
    }

    public void setTop_id(String top_id) {
        this.top_id = top_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRel_id() {
        return rel_id;
    }

    public void setRel_id(String rel_id) {
        this.rel_id = rel_id;
    }

    public String getPc_url() {
        return pc_url;
    }

    public void setPc_url(String pc_url) {
        this.pc_url = pc_url;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
