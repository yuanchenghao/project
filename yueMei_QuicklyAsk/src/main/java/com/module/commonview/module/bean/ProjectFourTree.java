/**
 * 
 */
package com.module.commonview.module.bean;

/**
 * @author lenovo17
 * 
 */
public class ProjectFourTree {
	private String _id;
	private String title;
	private String subtitle;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
}
