package com.module.commonview.module.bean;

public class ChatParmarsData  {
    private String directId;
    private String objId;
    private String objType;
    private String title;
    private String price;
    private String img;
    private String memberPrice;
    private String flag;
    private String ymClass;
    private String ymId;
    private String url;
    private String skuId;
    private String zt_title;
    private String zt_id;
    private String group_id;

    public String getDirectId() {
        return directId;
    }

    public String getObjId() {
        return objId;
    }

    public String getObjType() {
        return objType;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getImg() {
        return img;
    }

    public String getMemberPrice() {
        return memberPrice;
    }

    public String getFlag() {
        return flag;
    }

    public String getYmClass() {
        return ymClass;
    }

    public String getYmId() {
        return ymId;
    }

    public String getUrl() {
        return url;
    }

    public String getSkuId() {
        return skuId;
    }

    public String getZt_title() {
        return zt_title;
    }

    public String getZt_id() {
        return zt_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public static class ChatParmarsBuilder {
        private String directId;
        private String objId;
        private String objType;
        private String title;
        private String price;
        private String img;
        private String memberPrice;
        private String flag;
        private String ymClass;
        private String ymId;
        private String url;
        private String skuId;
        private String zt_title;
        private String zt_id;
        private String group_id;


        public ChatParmarsBuilder setDirectId(String directId) {
            this.directId = directId;
            return this;
        }

        public ChatParmarsBuilder setObjId(String objId) {
            this.objId = objId;
            return this;
        }

        public ChatParmarsBuilder setObjType(String objType) {
            this.objType = objType;
            return this;
        }

        public ChatParmarsBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public ChatParmarsBuilder setPrice(String price) {
            this.price = price;
            return this;
        }

        public ChatParmarsBuilder setImg(String img) {
            this.img = img;
            return this;
        }

        public ChatParmarsBuilder setMemberPrice(String memberPrice) {
            this.memberPrice = memberPrice;
            return this;
        }

        public ChatParmarsBuilder setFlag(String flag) {
            this.flag = flag;
            return this;
        }

        public ChatParmarsBuilder setYmClass(String ymClass) {
            this.ymClass = ymClass;
            return this;
        }

        public ChatParmarsBuilder setYmId(String ymId) {
            this.ymId = ymId;
            return this;
        }

        public ChatParmarsBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        public ChatParmarsBuilder setSkuId(String skuId) {
            this.skuId = skuId;
            return this;
        }

        public ChatParmarsBuilder setZt_title(String zt_title) {
            this.zt_title = zt_title;
            return this;
        }

        public ChatParmarsBuilder setZt_id(String zt_id) {
            this.zt_id = zt_id;
            return this;
        }

        public ChatParmarsBuilder setGroup_id(String group_id) {
            this.group_id = group_id;
            return this;
        }

        public ChatParmarsData build() {
            return new ChatParmarsData(this);
        }
    }

    private ChatParmarsData(ChatParmarsBuilder builder){
        this.directId=builder.directId;
        this.flag=builder.flag;
        this.objId=builder.objId;
        this.objType=builder.objType;
        this.title=builder.title;
        this.img=builder.img;
        this.price=builder.price;
        this.memberPrice=builder.memberPrice;
        this.url=builder.url;
        this.ymClass=builder.ymClass;
        this.ymId=builder.ymId;
        this.skuId=builder.skuId;
        this.zt_id=builder.zt_id;
        this.zt_title=builder.zt_title;
        this.group_id=builder.group_id;

    }
}
