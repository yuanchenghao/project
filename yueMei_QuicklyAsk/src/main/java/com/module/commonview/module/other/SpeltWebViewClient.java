package com.module.commonview.module.other;

import android.content.Intent;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.module.commonview.activity.SpeltActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;

/**
 * Created by 裴成浩 on 2018/1/22.
 */

public class SpeltWebViewClient implements BaseWebViewClientCallback {

    private final SpeltActivity mActivity;
    private final Intent intent;
    private String uid;
    private int mNum;
    private TaoDetailBean mTaoDetailBean;
    private String mSource;
    private String mObjid;

    public SpeltWebViewClient(SpeltActivity mActivity,int num,TaoDetailBean taoDetailBean, final String source, final String objid) {
        this.mActivity = mActivity;
        intent = new Intent();
        this.mNum=num;
        this.mTaoDetailBean=taoDetailBean;
        this.mSource=source;
        this.mObjid=objid;


    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "6353":// 我要参团

                if(uid != null && !"".equals(uid)){

                    mActivity.mSkuOrdingPopwindow.setOnDismissListener(new OnPopupDismissListener());
                    WindowManager.LayoutParams lp=mActivity.getWindow().getAttributes();
                    lp.alpha=0.7f;
                    mActivity.getWindow().setAttributes(lp);
                    mActivity.mSkuOrdingPopwindow.setNumber(mNum);
                    mActivity.mSkuOrdingPopwindow.showAtLocation(mActivity.findViewById(R.id.llytRootParentGroup),Gravity.BOTTOM,0,0);

                    //        type 1 正常下单 2，拼团
                    //         flag 0 参团 1发起拼团
//                    Intent intent = new Intent(mActivity, MakeSureOrderActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("tao_id", mTaoDetailBean.getBasedata().getId());
//                    String start_number = mTaoDetailBean.getBasedata().getStart_number();
//                    bundle.putString("number",  start_number);
//                    bundle.putString("source", mSource);
//                    bundle.putString("objid", mObjid);
//                    bundle.putString("buy_for_cart", "0");
//                    bundle.putString("u", "0");
//                    TaoDetailBean.GroupInfoBean groupInfo = mTaoDetailBean.getGroupInfo();
//                    if (groupInfo.getGroup().size() != 0) {
//                        bundle.putString("group_id", groupInfo.getGroup().get(mNum).getGroup_id());
//                        Log.e("SkuOrdingPopwindow",mNum+"mnumber========>"+groupInfo.getGroup().get(mNum).getGroup_id());
//                    }
//                        bundle.putString("is_group", "1");
//
//                    intent.putExtra("data", bundle);
//                    mActivity.startActivity(intent);

                }else{
                    Utils.jumpLogin(mActivity);
                }

                break;
        }
    }


    /**
     * popupwindow消失的回调
     */
    private class OnPopupDismissListener implements
            PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            // 标题和主页开始播放动画
            WindowManager.LayoutParams lp=mActivity.getWindow().getAttributes();
            lp.alpha=1f;
            mActivity.getWindow().setAttributes(lp);
        }
    }
}