package com.module.commonview.module.bean;

import java.util.List;

public class DiaryPhotoBrowsingBean {

    /**
     * url : https://m.yuemei.com/c/2966983.html
     * taoData : {"title":"【网红脸套餐】广州面部私人订制：眼综合+鼻综合  瞬变女神  美到爆  院长亲诊","price_discount":"11680","url":"https://m.yuemei.com/tao/34175/"}
     * title : 术后106天
     * content : 哇偶，时间过的真的还是挺快的，从我在医美做鼻子眼睛到现在已经快百天了，感觉还是上个星期的事情，眼睛鼻子恢复的效果想必大家也是看了照片了，其实心动不如行动，鼻子现在是越来越自然，现在一上线就会看到很多小伙伴的留言，这里要谢谢大家关注咯~我看到都会一一恢复宝宝的。
     * pic : [{"img":"https://p31.yuemei.com/postimg/20180615/500_500/15290292291171.jpg","pic_id":"6900553","images":"upload/forum/image/20180615/15290292291171.jpg","width":"500","height":"500","weight":"0","is_video":"0","video_url":""},{"img":"https://p31.yuemei.com/postimg/20180615/500_500/15290292294418.jpg","pic_id":"6900556","images":"upload/forum/image/20180615/15290292294418.jpg","width":"500","height":"500","weight":"0","is_video":"0","video_url":""},{"img":"https://p31.yuemei.com/postimg/20180615/500_500/15290292293917.jpg","pic_id":"6900559","images":"upload/forum/image/20180615/15290292293917.jpg","width":"500","height":"500","weight":"0","is_video":"0","video_url":""}]
     * prev_id : 2910424
     * next_id : 0
     */

    private String url;
    private TaoDataBean taoData;
    private String title;
    private String content;
    private String prev_id;
    private String next_id;
    private List<PicBean> pic;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public TaoDataBean getTaoData() {
        return taoData;
    }

    public void setTaoData(TaoDataBean taoData) {
        this.taoData = taoData;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPrev_id() {
        return prev_id;
    }

    public void setPrev_id(String prev_id) {
        this.prev_id = prev_id;
    }

    public String getNext_id() {
        return next_id;
    }

    public void setNext_id(String next_id) {
        this.next_id = next_id;
    }

    public List<PicBean> getPic() {
        return pic;
    }

    public void setPic(List<PicBean> pic) {
        this.pic = pic;
    }

    public static class TaoDataBean {
        /**
         * title : 【网红脸套餐】广州面部私人订制：眼综合+鼻综合  瞬变女神  美到爆  院长亲诊
         * price_discount : 11680
         * url : https://m.yuemei.com/tao/34175/
         */

        private String title;
        private String price_discount;
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class PicBean {
        /**
         * img : https://p31.yuemei.com/postimg/20180615/500_500/15290292291171.jpg
         * pic_id : 6900553
         * images : upload/forum/image/20180615/15290292291171.jpg
         * width : 500
         * height : 500
         * weight : 0
         * is_video : 0
         * video_url :
         */

        private String img;
        private String pic_id;
        private String images;
        private String width;
        private String height;
        private String weight;
        private String is_video;
        private String video_url;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getPic_id() {
            return pic_id;
        }

        public void setPic_id(String pic_id) {
            this.pic_id = pic_id;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getIs_video() {
            return is_video;
        }

        public void setIs_video(String is_video) {
            this.is_video = is_video;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }
    }
}
