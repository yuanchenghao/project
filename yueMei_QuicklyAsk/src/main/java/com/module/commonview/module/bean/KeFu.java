/**
 * 
 */
package com.module.commonview.module.bean;

import com.module.my.model.bean.KeFuData;

/**
 * 客服数据
 * 
 * @author Robin
 * 
 */
public class KeFu {

	private String code;

	private String message;

	private KeFuData data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public KeFuData getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(KeFuData data) {
		this.data = data;
	}

}
