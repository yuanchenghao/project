package com.module.commonview.module.bean;

/**
 * Created by dwb on 17/4/27.
 */

public class HXkfu {

    private String code;
    private String message;
    private HXkfuData data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HXkfuData getData() {
        return data;
    }

    public void setData(HXkfuData data) {
        this.data = data;
    }
}
