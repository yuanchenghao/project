package com.module.commonview.module.other;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.module.commonview.activity.TaoDetailActivity;
import com.quicklyask.entity.TaoDetailData639;
import com.quicklyask.util.Utils;

import org.kymjs.kjframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by 裴成浩 on 2018/1/10.
 */

public class OnHorizontalScrollView implements View.OnClickListener {

    private List<TaoDetailData639.Hos_Red_Packet> listRedPacket = new ArrayList<>();
    private TaoDetailActivity mActivity;
    private String TAG = "OnHorizontalScrollView";

    public OnHorizontalScrollView(TaoDetailActivity activity, List<TaoDetailData639.Hos_Red_Packet> listRedPacket) {
        this.mActivity = activity;
        this.listRedPacket = listRedPacket;
    }

    @Override
    public void onClick(View v) {
        if (Utils.isLogin()) {
            if (Utils.isBind()){
                int selected = (int) v.getTag();
                Log.e(TAG, "selected === " + selected);
                for (int i = 0; i < listRedPacket.size(); i++) {
                    if (i == selected) {
                        TaoDetailData639.Hos_Red_Packet hos_red_packet = listRedPacket.get(i);
                        Log.e(TAG, "hos_red_packet.getCoupons_status() === " + hos_red_packet.getCoupons_status());
                        if ("0".equals(hos_red_packet.getCoupons_status())) {
                            ViewInject.toast("代金券太抢手已经被抢光了~");
                        } else if ("1".equals(hos_red_packet.getCoupons_status())) {
//                            getDaijinjuanData(hos_red_packet.getId(), i);  //联网领取代金券
                        }
                    }
                }
            }else {
                Utils.jumpBindingPhone(mActivity);

            }

        } else {
//            mActivity.jumpLogin();
        }
    }

    /**
     * 红包领取请求
     *
     * @param id
     * @param pos
     */
//    private void getDaijinjuanData(String id, final int pos) {
//        DaiJinJuanApi daiJinJuanApi = new DaiJinJuanApi();
//        Map<String, Object> maps = new HashMap<>();
//        maps.put("id", id);
//        Log.e("hongbao", "id == " + id);
//        daiJinJuanApi.getCallBack(mActivity, maps, new BaseCallBackListener<ServerData>() {
//
//            @Override
//            public void onSuccess(ServerData data) {
//                Log.e(TAG, "执行到这里.....data =-== " + data.toString());
//                if ("1".equals(data.code)) {
//                    if (data != null) {
//                        View view = mActivity.llRedPacket.getChildAt(pos);
//                        LinearLayout llRedPacket = view.findViewById(R.id.ll_red_packet1);
//                        ImageView hongbaoTag = view.findViewById(R.id.hongbao_tag);
//                        if (data.code.equals("0")) {
//
//                            DaiJinJuan redBao = JSONUtil.TransformDaiJinJuan(data.data);
//
//                            String couponsStatus = redBao.getCoupons_status();
//                            String isGet = redBao.getIs_get();
//                            if ("0".equals(couponsStatus)) {            //已抢光
//                                llRedPacket.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.sp_redpacket_background2));
//                                hongbaoTag.setVisibility(View.VISIBLE);
//                                hongbaoTag.setImageResource(R.drawable.red_envelope_gone);
//
//                            } else if ("1".equals(couponsStatus)) {        //未抢光
//
//                                if ("1".equals(isGet)) {                  //已领取
//
//                                    llRedPacket.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.sp_redpacket_background2));
//                                    hongbaoTag.setVisibility(View.VISIBLE);
//                                    hongbaoTag.setImageResource(R.drawable.red_envelope_receive);
//
//                                } else {                                 //未领取
//
//                                    llRedPacket.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.sp_redpacket_background1));
//                                    hongbaoTag.setVisibility(View.GONE);
//
//                                }
//                            }
//
//                            Toast.makeText(mActivity, data.message, Toast.LENGTH_SHORT).show();
//                        } else {
//
//                            llRedPacket.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.sp_redpacket_background2));
//                            hongbaoTag.setVisibility(View.VISIBLE);
//
//                            hongbaoTag.setImageResource(R.drawable.red_envelope_receive);
//
//                            showDialogExitEdit1(data.message);
//                        }
//                    } else {
//                        ViewInject.toast("领取失败，领取次数已用完~");
//                    }
//                } else {
//                    ViewInject.toast(data.message);
//                }
//            }
//        });
//
//    }


//    private void showDialogExitEdit1(String message) {
//        final EditExitDialog editDialog = new EditExitDialog(mActivity, R.style.mystyle, R.layout.dialog_edit_exit2);
//        editDialog.setCanceledOnTouchOutside(false);
//        editDialog.show();
//        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
//        titleTv77.setVisibility(View.VISIBLE);
//        titleTv77.setText("领取成功！");
//        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
//        titleTv88.setText(message);
//        titleTv88.setHeight(50);
//        Button cancelBt88 = (Button) editDialog.findViewById(R.id.cancel_btn1_edit);
//        cancelBt88.setText("查看代金券");
//        cancelBt88.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                Intent intent = new Intent(mActivity, MyDaijinjuanActivity.class);
//                intent.putExtra("link", "");
//                mActivity.startActivity(intent);
//                editDialog.dismiss();
//            }
//        });
//
//        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
//        trueBt1188.setText("确认");
//        trueBt1188.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                editDialog.dismiss();
//            }
//        });
//
//    }

}
