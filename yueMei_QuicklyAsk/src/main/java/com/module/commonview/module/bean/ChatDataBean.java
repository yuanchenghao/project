package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class ChatDataBean implements Parcelable {

    /**
     * ymaq_class : 5
     * ymaq_id : 6521353
     * is_rongyun : 3
     * hos_userid : 85059031
     * event_name : chat_hospital
     * event_pos : diary_bottom_1
     * show_message : 入线上面诊快速通
     */

    private String ymaq_class;
    private String ymaq_id;
    private String is_rongyun;
    private String hos_userid;
    private String event_name;
    private String event_pos;
    private String show_message;
    private ChatDataGuiJi guiji;
    private WorkTime work_time;
    private String obj_type;
    private String obj_id;
    private String doctor_id;
    private HashMap<String,String> event_params;


    protected ChatDataBean(Parcel in) {
        ymaq_class = in.readString();
        ymaq_id = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
        event_name = in.readString();
        event_pos = in.readString();
        show_message = in.readString();
        guiji = in.readParcelable(ChatDataGuiJi.class.getClassLoader());
        work_time = in.readParcelable(WorkTime.class.getClassLoader());
        obj_type = in.readString();
        obj_id = in.readString();
        doctor_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ymaq_class);
        dest.writeString(ymaq_id);
        dest.writeString(is_rongyun);
        dest.writeString(hos_userid);
        dest.writeString(event_name);
        dest.writeString(event_pos);
        dest.writeString(show_message);
        dest.writeParcelable(guiji, flags);
        dest.writeParcelable(work_time, flags);
        dest.writeString(obj_type);
        dest.writeString(obj_id);
        dest.writeString(doctor_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatDataBean> CREATOR = new Creator<ChatDataBean>() {
        @Override
        public ChatDataBean createFromParcel(Parcel in) {
            return new ChatDataBean(in);
        }

        @Override
        public ChatDataBean[] newArray(int size) {
            return new ChatDataBean[size];
        }
    };

    public String getYmaq_class() {
        return ymaq_class;
    }

    public void setYmaq_class(String ymaq_class) {
        this.ymaq_class = ymaq_class;
    }

    public String getYmaq_id() {
        return ymaq_id;
    }

    public void setYmaq_id(String ymaq_id) {
        this.ymaq_id = ymaq_id;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(String event_pos) {
        this.event_pos = event_pos;
    }

    public String getShow_message() {
        return show_message;
    }

    public void setShow_message(String show_message) {
        this.show_message = show_message;
    }

    public ChatDataGuiJi getGuiji() {
        return guiji;
    }

    public void setGuiji(ChatDataGuiJi guiji) {
        this.guiji = guiji;
    }

    public WorkTime getWork_time() {
        return work_time;
    }

    public void setWork_time(WorkTime work_time) {
        this.work_time = work_time;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
