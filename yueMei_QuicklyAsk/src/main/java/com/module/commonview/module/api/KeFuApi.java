package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.KeFuData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by Administrator on 2017/10/23.
 */

public class KeFuApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.SERVICE, "getkfuinfo", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)){
                    try {
                        KeFuData keFuData = JSONUtil.TransformSingleBean(mData.data, KeFuData.class);
                        listener.onSuccess(keFuData);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
