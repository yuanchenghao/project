package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/8.
 */
public class DiaryHosDocFanXian implements Parcelable {
    private String cashback_type;
    private String cashback_money;
    private String cashback_degree;
    private String cashback_desc;
    private String cashback_title;
    private String url;
    private String href_title;


    protected DiaryHosDocFanXian(Parcel in) {
        cashback_type = in.readString();
        cashback_money = in.readString();
        cashback_degree = in.readString();
        cashback_desc = in.readString();
        cashback_title = in.readString();
        url = in.readString();
        href_title = in.readString();
    }

    public static final Creator<DiaryHosDocFanXian> CREATOR = new Creator<DiaryHosDocFanXian>() {
        @Override
        public DiaryHosDocFanXian createFromParcel(Parcel in) {
            return new DiaryHosDocFanXian(in);
        }

        @Override
        public DiaryHosDocFanXian[] newArray(int size) {
            return new DiaryHosDocFanXian[size];
        }
    };

    public String getCashback_type() {
        return cashback_type;
    }

    public void setCashback_type(String cashback_type) {
        this.cashback_type = cashback_type;
    }

    public String getCashback_money() {
        return cashback_money;
    }

    public void setCashback_money(String cashback_money) {
        this.cashback_money = cashback_money;
    }

    public String getCashback_degree() {
        return cashback_degree;
    }

    public void setCashback_degree(String cashback_degree) {
        this.cashback_degree = cashback_degree;
    }

    public String getCashback_desc() {
        return cashback_desc;
    }

    public void setCashback_desc(String cashback_desc) {
        this.cashback_desc = cashback_desc;
    }

    public String getCashback_title() {
        return cashback_title;
    }

    public void setCashback_title(String cashback_title) {
        this.cashback_title = cashback_title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHref_title() {
        return href_title;
    }

    public void setHref_title(String href_title) {
        this.href_title = href_title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cashback_type);
        dest.writeString(cashback_money);
        dest.writeString(cashback_degree);
        dest.writeString(cashback_desc);
        dest.writeString(cashback_title);
        dest.writeString(url);
        dest.writeString(href_title);
    }
}
