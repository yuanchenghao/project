package com.module.commonview.module.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * 关注取消关注
 * Created by 裴成浩 on 2018/4/18.
 */

public class FocusAndCancelApi implements BaseCallBackApi {

    private String TAG = "FocusAndCancelApi";
    private HashMap<String, Object> hashMap;  //传值容器

    public FocusAndCancelApi() {
        hashMap = new HashMap<>();
    }
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "following", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }
}
