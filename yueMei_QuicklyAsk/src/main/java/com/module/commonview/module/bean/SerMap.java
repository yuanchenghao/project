package com.module.commonview.module.bean;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2017/12/18.
 */

public class SerMap implements Serializable {

    public HashMap<String,String> map;

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }
}