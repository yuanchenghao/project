/**
 * 
 */
package com.module.commonview.module.bean;

/**
 * @author lenovo17
 * 
 */
public class Pic {
	private String img;

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

}
