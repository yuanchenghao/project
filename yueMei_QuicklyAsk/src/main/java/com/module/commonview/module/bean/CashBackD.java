package com.module.commonview.module.bean;

import java.util.List;

/**
 * Created by dwb on 16/3/30.
 */
public class CashBackD {

    private String desc;
    private List<CashBackData> data;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<CashBackData> getData() {
        return data;
    }

    public void setData(List<CashBackData> data) {
        this.data = data;
    }
}
