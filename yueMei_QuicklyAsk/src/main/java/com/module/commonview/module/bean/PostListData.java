package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.home.model.bean.HomeAskEntry;
import com.module.home.model.bean.QuestionListData;
import com.module.taodetail.model.bean.HomeTaoData;

import java.util.HashMap;
import java.util.List;

/**
 * 帖子列表数据
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostListData implements Parcelable {

    private String title;                           //标题
    private String id;                              //id
    private List<PostContentList> content;         //
    private DiaryUserdataBean userdata;             //用户信息
    private List<DiaryOtherPostBean> other_post;    //日记推荐
    private List<QuestionListData> other_question; //问答列表
    private DiaryHosDocBean hosDoc;
    private List<PostOtherpic> pic;                //头图
    private String p_id;
    private String askorshare;
    private String agree_num;
    private String answer_num;
    private String collect_num;
    private String is_collect;                      //判断是否收藏
    private List<DiaryReplyList> replyList;         //回复列表
    private List<DiaryTagList> tag;                 //标签
    private String moban;                           //模版
    private String vote;                            //是否有投票
    private PostPeopleList people;
    private String baoming_is_end;                  //	报名结束 1
    private String vote_is_end;                     //	报名结束 1
    private List<HomeTaoData> taolist;               //推荐列表
    private HomeAskEntry ask_entry;                 //	提问入口
    private String loadHtmlUrl;                     //是否加载 Html不为空加载
    private List<DiaryTaoData> taoDataList;               //头像下的淘整型
    private List<DiaryTaoData> contentSkuList;            //文字底部淘整型
    private HashMap<String, String> shareClickData;      //分享按钮点击事件统计用
    private HashMap<String, String> followClickData;     //关注按钮点击事件统计用
    private HashMap<String, String> baomingClickData;    //报名按钮点击事件统计用
    private String selfPageType;                        //当前页面类型
    private ChatDataBean chatData;                      //私信入口
    private BaomingData baoming;                        //报名数据
    private String is_agree;                            //是否点赞
    private String loadZtUrl;                          //问答吸底跳转链接
    private List<PostRecommendDoctor> recommend_doctor_list;              //推荐医生列表
    private VoteListBean vote_list;



    protected PostListData(Parcel in) {
        title = in.readString();
        id = in.readString();
        userdata = in.readParcelable(DiaryUserdataBean.class.getClassLoader());
        other_post = in.createTypedArrayList(DiaryOtherPostBean.CREATOR);
        other_question = in.createTypedArrayList(QuestionListData.CREATOR);
        hosDoc = in.readParcelable(DiaryHosDocBean.class.getClassLoader());
        pic = in.createTypedArrayList(PostOtherpic.CREATOR);
        p_id = in.readString();
        askorshare = in.readString();
        agree_num = in.readString();
        answer_num = in.readString();
        collect_num = in.readString();
        is_collect = in.readString();
        replyList = in.createTypedArrayList(DiaryReplyList.CREATOR);
        tag = in.createTypedArrayList(DiaryTagList.CREATOR);
        moban = in.readString();
        vote = in.readString();
        people = in.readParcelable(PostPeopleList.class.getClassLoader());
        baoming_is_end = in.readString();
        vote_is_end = in.readString();
        taolist = in.createTypedArrayList(HomeTaoData.CREATOR);
        loadHtmlUrl = in.readString();
        taoDataList = in.createTypedArrayList(DiaryTaoData.CREATOR);
        contentSkuList = in.createTypedArrayList(DiaryTaoData.CREATOR);
        selfPageType = in.readString();
        chatData = in.readParcelable(ChatDataBean.class.getClassLoader());
        baoming = in.readParcelable(BaomingData.class.getClassLoader());
        is_agree = in.readString();
        loadZtUrl = in.readString();
        recommend_doctor_list = in.createTypedArrayList(PostRecommendDoctor.CREATOR);
        vote_list = in.readParcelable(VoteListBean.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(id);
        dest.writeParcelable(userdata, flags);
        dest.writeTypedList(other_post);
        dest.writeTypedList(other_question);
        dest.writeParcelable(hosDoc, flags);
        dest.writeTypedList(pic);
        dest.writeString(p_id);
        dest.writeString(askorshare);
        dest.writeString(agree_num);
        dest.writeString(answer_num);
        dest.writeString(collect_num);
        dest.writeString(is_collect);
        dest.writeTypedList(replyList);
        dest.writeTypedList(tag);
        dest.writeString(moban);
        dest.writeString(vote);
        dest.writeParcelable(people, flags);
        dest.writeString(baoming_is_end);
        dest.writeString(vote_is_end);
        dest.writeTypedList(taolist);
        dest.writeString(loadHtmlUrl);
        dest.writeTypedList(taoDataList);
        dest.writeTypedList(contentSkuList);
        dest.writeString(selfPageType);
        dest.writeParcelable(chatData, flags);
        dest.writeParcelable(baoming, flags);
        dest.writeString(is_agree);
        dest.writeString(loadZtUrl);
        dest.writeTypedList(recommend_doctor_list);
        dest.writeParcelable(vote_list, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PostListData> CREATOR = new Creator<PostListData>() {
        @Override
        public PostListData createFromParcel(Parcel in) {
            return new PostListData(in);
        }

        @Override
        public PostListData[] newArray(int size) {
            return new PostListData[size];
        }
    };


    public VoteListBean getVote_list() {
        return vote_list;
    }

    public void setVote_list(VoteListBean vote_list) {
        this.vote_list = vote_list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<PostContentList> getContent() {
        return content;
    }

    public void setContent(List<PostContentList> content) {
        this.content = content;
    }

    public DiaryUserdataBean getUserdata() {
        return userdata;
    }

    public void setUserdata(DiaryUserdataBean userdata) {
        this.userdata = userdata;
    }

    public List<DiaryOtherPostBean> getOther_post() {
        return other_post;
    }

    public void setOther_post(List<DiaryOtherPostBean> other_post) {
        this.other_post = other_post;
    }

    public List<QuestionListData> getOther_question() {
        return other_question;
    }

    public void setOther_question(List<QuestionListData> other_question) {
        this.other_question = other_question;
    }

    public DiaryHosDocBean getHosDoc() {
        return hosDoc;
    }

    public void setHosDoc(DiaryHosDocBean hosDoc) {
        this.hosDoc = hosDoc;
    }

    public List<PostOtherpic> getPic() {
        return pic;
    }

    public void setPic(List<PostOtherpic> pic) {
        this.pic = pic;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public String getCollect_num() {
        return collect_num;
    }

    public void setCollect_num(String collect_num) {
        this.collect_num = collect_num;
    }

    public String getIs_collect() {
        return is_collect;
    }

    public void setIs_collect(String is_collect) {
        this.is_collect = is_collect;
    }

    public List<DiaryReplyList> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<DiaryReplyList> replyList) {
        this.replyList = replyList;
    }

    public List<DiaryTagList> getTag() {
        return tag;
    }

    public void setTag(List<DiaryTagList> tag) {
        this.tag = tag;
    }

    public String getMoban() {
        return moban;
    }

    public void setMoban(String moban) {
        this.moban = moban;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public PostPeopleList getPeople() {
        return people;
    }

    public void setPeople(PostPeopleList people) {
        this.people = people;
    }

    public String getBaoming_is_end() {
        return baoming_is_end;
    }

    public void setBaoming_is_end(String baoming_is_end) {
        this.baoming_is_end = baoming_is_end;
    }

    public String getVote_is_end() {
        return vote_is_end;
    }

    public void setVote_is_end(String vote_is_end) {
        this.vote_is_end = vote_is_end;
    }

    public List<HomeTaoData> getTaolist() {
        return taolist;
    }

    public void setTaolist(List<HomeTaoData> taolist) {
        this.taolist = taolist;
    }

    public HomeAskEntry getAsk_entry() {
        return ask_entry;
    }

    public void setAsk_entry(HomeAskEntry ask_entry) {
        this.ask_entry = ask_entry;
    }

    public String getLoadHtmlUrl() {
        return loadHtmlUrl;
    }

    public void setLoadHtmlUrl(String loadHtmlUrl) {
        this.loadHtmlUrl = loadHtmlUrl;
    }

    public List<DiaryTaoData> getTaoDataList() {
        return taoDataList;
    }

    public void setTaoDataList(List<DiaryTaoData> taoDataList) {
        this.taoDataList = taoDataList;
    }

    public List<DiaryTaoData> getContentSkuList() {
        return contentSkuList;
    }

    public void setContentSkuList(List<DiaryTaoData> contentSkuList) {
        this.contentSkuList = contentSkuList;
    }

    public HashMap<String, String> getShareClickData() {
        return shareClickData;
    }

    public void setShareClickData(HashMap<String, String> shareClickData) {
        this.shareClickData = shareClickData;
    }

    public HashMap<String, String> getFollowClickData() {
        return followClickData;
    }

    public void setFollowClickData(HashMap<String, String> followClickData) {
        this.followClickData = followClickData;
    }

    public HashMap<String, String> getBaomingClickData() {
        return baomingClickData;
    }

    public void setBaomingClickData(HashMap<String, String> baomingClickData) {
        this.baomingClickData = baomingClickData;
    }

    public String getSelfPageType() {
        return selfPageType;
    }

    public void setSelfPageType(String selfPageType) {
        this.selfPageType = selfPageType;
    }

    public ChatDataBean getChatData() {
        return chatData;
    }

    public void setChatData(ChatDataBean chatData) {
        this.chatData = chatData;
    }

    public BaomingData getBaoming() {
        return baoming;
    }

    public void setBaoming(BaomingData baoming) {
        this.baoming = baoming;
    }

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }

    public String getLoadZtUrl() {
        return loadZtUrl;
    }

    public void setLoadZtUrl(String loadZtUrl) {
        this.loadZtUrl = loadZtUrl;
    }

    public List<PostRecommendDoctor> getRecommend_doctor_list() {
        return recommend_doctor_list;
    }

    public void setRecommend_doctor_list(List<PostRecommendDoctor> recommend_doctor_list) {
        this.recommend_doctor_list = recommend_doctor_list;
    }
}
