package com.module.commonview.module.api;

import android.content.Context;
import android.content.Intent;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/23.
 */

public class TaoDataApi implements BaseCallBackApi {
    private String TAG = "TaoDataApi";
    private Intent mIntent;

    @Override
    public void getCallBack(final Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.TAO, "tao", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);
            }
        });
    }

    public void setIntent(Intent intent) {
        this.mIntent = intent;
    }

}
