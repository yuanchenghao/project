package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/8.
 */
public class DiaryLocationData implements Parcelable {
    private String title;
    private String appmurl;

    protected DiaryLocationData(Parcel in) {
        title = in.readString();
        appmurl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(appmurl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryLocationData> CREATOR = new Creator<DiaryLocationData>() {
        @Override
        public DiaryLocationData createFromParcel(Parcel in) {
            return new DiaryLocationData(in);
        }

        @Override
        public DiaryLocationData[] newArray(int size) {
            return new DiaryLocationData[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAppmurl() {
        return appmurl;
    }

    public void setAppmurl(String appmurl) {
        this.appmurl = appmurl;
    }
}
