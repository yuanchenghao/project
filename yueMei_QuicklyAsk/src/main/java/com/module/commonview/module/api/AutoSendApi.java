package com.module.commonview.module.api;

import android.app.ActivityManager;
import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

public class AutoSendApi implements BaseCallBackApi {
    public static final int DOCTOR_INSTANCE=0;
    public static final int HOSPITAL_INSTANCE=1;
    public static final int SKU_INSTANCE=2;

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        if (getInstanceType(context) == DOCTOR_INSTANCE){

        }
        if (getInstanceType(context) == HOSPITAL_INSTANCE){

        }
        if (getInstanceType(context) == SKU_INSTANCE){

        }

        NetWork.getInstance().call(FinalConstant1.CHAT, "autosend", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                listener.onSuccess(mData);
            }
        });
    }

    private int getInstanceType(Context context){
        if (context instanceof HosDetailActivity){
            return HOSPITAL_INSTANCE;
        }
        if (context instanceof DoctorDetailsActivity592){
            return DOCTOR_INSTANCE;
        }
        if (context instanceof TaoDetailActivity){
            return SKU_INSTANCE;
        }

        return 0;
    }

    private int getLastInstanceType(Context context){

        ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.RunningTaskInfo info = manager.getRunningTasks(1).get(0);
        String shortClassName = info.topActivity.getShortClassName();    //类名
        String className = info.topActivity.getClassName();              //完整类名
        String packageName = info.topActivity.getPackageName();          //包名

        if (context instanceof HosDetailActivity){
            return HOSPITAL_INSTANCE;
        }
        if (context instanceof DoctorDetailsActivity592){
            return DOCTOR_INSTANCE;
        }
        if (context instanceof TaoDetailActivity){
            return SKU_INSTANCE;
        }

        return 0;
    }


}
