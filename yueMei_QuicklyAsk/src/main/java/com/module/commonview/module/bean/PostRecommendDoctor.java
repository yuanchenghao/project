package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 文 件 名: PostRecommendDoctor
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-27 10:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class PostRecommendDoctor implements Parcelable {


    /**
     * name : 王宇琨
     * id : 86077150
     * avatar : https://p21.yuemei.com/avatar/086/07/71/50_avatar_120_120.jpg
     * talent : 12
     * group_id : 2
     * lable : 11-14
     * url : https://m.yuemei.com/dr/86077150/
     * doctor_good_board : [134]
     * hospital_id : 11239
     * doctor_id : 86077150
     * is_rongyun : 3
     * hos_userid : 85116209
     * obj_id : 86077150
     * obj_type : 1
     * title :
     * cityName : 成都
     * hospitalName : 成都铜雀台医学美容医院
     * is_following : 0
     * speciality_board : ["鼻部整形"]
     */

    private String name;
    private String id;
    private String avatar;
    private String talent;
    private String group_id;
    private String lable;
    private String url;
    private String hospital_id;
    private String doctor_id;
    private String is_rongyun;
    private String hos_userid;
    private String obj_id;
    private String obj_type;
    private String title;
    private String cityName;
    private String hospitalName;
    private String is_following;
    private List<Integer> doctor_good_board;
    private List<String> speciality_board;

    protected PostRecommendDoctor(Parcel in) {
        name = in.readString();
        id = in.readString();
        avatar = in.readString();
        talent = in.readString();
        group_id = in.readString();
        lable = in.readString();
        url = in.readString();
        hospital_id = in.readString();
        doctor_id = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
        obj_id = in.readString();
        obj_type = in.readString();
        title = in.readString();
        cityName = in.readString();
        hospitalName = in.readString();
        is_following = in.readString();
        speciality_board = in.createStringArrayList();
    }

    public static final Creator<PostRecommendDoctor> CREATOR = new Creator<PostRecommendDoctor>() {
        @Override
        public PostRecommendDoctor createFromParcel(Parcel in) {
            return new PostRecommendDoctor(in);
        }

        @Override
        public PostRecommendDoctor[] newArray(int size) {
            return new PostRecommendDoctor[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(avatar);
        dest.writeString(talent);
        dest.writeString(group_id);
        dest.writeString(lable);
        dest.writeString(url);
        dest.writeString(hospital_id);
        dest.writeString(doctor_id);
        dest.writeString(is_rongyun);
        dest.writeString(hos_userid);
        dest.writeString(obj_id);
        dest.writeString(obj_type);
        dest.writeString(title);
        dest.writeString(cityName);
        dest.writeString(hospitalName);
        dest.writeString(is_following);
        dest.writeStringList(speciality_board);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getIs_following() {
        return is_following;
    }

    public void setIs_following(String is_following) {
        this.is_following = is_following;
    }

    public List<Integer> getDoctor_good_board() {
        return doctor_good_board;
    }

    public void setDoctor_good_board(List<Integer> doctor_good_board) {
        this.doctor_good_board = doctor_good_board;
    }

    public List<String> getSpeciality_board() {
        return speciality_board;
    }

    public void setSpeciality_board(List<String> speciality_board) {
        this.speciality_board = speciality_board;
    }
}
