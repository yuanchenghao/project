package com.module.commonview.module.bean;

public class AmountRefundBean {
    private String pay_total_fee;
    private String extract_balance_pay_money;
    private String unextract_balance_pay_money;

    public String getPay_total_fee() {
        return pay_total_fee;
    }

    public void setPay_total_fee(String pay_total_fee) {
        this.pay_total_fee = pay_total_fee;
    }

    public String getExtract_balance_pay_money() {
        return extract_balance_pay_money;
    }

    public void setExtract_balance_pay_money(String extract_balance_pay_money) {
        this.extract_balance_pay_money = extract_balance_pay_money;
    }

    public String getUnextract_balance_pay_money() {
        return unextract_balance_pay_money;
    }

    public void setUnextract_balance_pay_money(String unextract_balance_pay_money) {
        this.unextract_balance_pay_money = unextract_balance_pay_money;
    }
}
