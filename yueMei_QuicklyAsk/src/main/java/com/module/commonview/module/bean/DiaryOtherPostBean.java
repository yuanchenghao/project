package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryOtherPostBean implements Parcelable {

    private String user_img;
    private String user_name;
    private List<DiaryOtherPostBoardBean> board;
    private String id;
    private String url;
    private String title;
    private String img;
    private String view_num;
    private String share_num;
    private String is_video = "0";
    private HashMap<String, String> event_params;


    protected DiaryOtherPostBean(Parcel in) {
        user_img = in.readString();
        user_name = in.readString();
        board = in.createTypedArrayList(DiaryOtherPostBoardBean.CREATOR);
        id = in.readString();
        url = in.readString();
        title = in.readString();
        img = in.readString();
        view_num = in.readString();
        share_num = in.readString();
        is_video = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_img);
        dest.writeString(user_name);
        dest.writeTypedList(board);
        dest.writeString(id);
        dest.writeString(url);
        dest.writeString(title);
        dest.writeString(img);
        dest.writeString(view_num);
        dest.writeString(share_num);
        dest.writeString(is_video);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryOtherPostBean> CREATOR = new Creator<DiaryOtherPostBean>() {
        @Override
        public DiaryOtherPostBean createFromParcel(Parcel in) {
            return new DiaryOtherPostBean(in);
        }

        @Override
        public DiaryOtherPostBean[] newArray(int size) {
            return new DiaryOtherPostBean[size];
        }
    };

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public List<DiaryOtherPostBoardBean> getBoard() {
        return board;
    }

    public void setBoard(List<DiaryOtherPostBoardBean> board) {
        this.board = board;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getView_num() {
        return view_num;
    }

    public void setView_num(String view_num) {
        this.view_num = view_num;
    }

    public String getShare_num() {
        return share_num;
    }

    public void setShare_num(String share_num) {
        this.share_num = share_num;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
