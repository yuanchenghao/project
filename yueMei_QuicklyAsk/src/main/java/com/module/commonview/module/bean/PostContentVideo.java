package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/8/17.
 */
public class PostContentVideo implements Parcelable {
    private String img;
    private String video_url;
    private String img_width;
    private String img_height;

    public PostContentVideo(String video_url) {
        this.video_url = video_url;
    }

    protected PostContentVideo(Parcel in) {
        img = in.readString();
        video_url = in.readString();
        img_width = in.readString();
        img_height = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(video_url);
        dest.writeString(img_width);
        dest.writeString(img_height);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PostContentVideo> CREATOR = new Creator<PostContentVideo>() {
        @Override
        public PostContentVideo createFromParcel(Parcel in) {
            return new PostContentVideo(in);
        }

        @Override
        public PostContentVideo[] newArray(int size) {
            return new PostContentVideo[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getImg_width() {
        return img_width;
    }

    public void setImg_width(String img_width) {
        this.img_width = img_width;
    }

    public String getImg_height() {
        return img_height;
    }

    public void setImg_height(String img_height) {
        this.img_height = img_height;
    }
}
