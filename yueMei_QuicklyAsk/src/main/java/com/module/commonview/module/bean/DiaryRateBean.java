package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryRateBean implements Parcelable {
    /**
     * rateSale : 20
     * effect : 手术效果：1.0
     * service : 服务：1.0
     * pf_doctor : 医生评价：1.0
     */

    private String rateSale;
    private String effect;
    private String service;
    private String pf_doctor;

    protected DiaryRateBean(Parcel in) {
        rateSale = in.readString();
        effect = in.readString();
        service = in.readString();
        pf_doctor = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rateSale);
        dest.writeString(effect);
        dest.writeString(service);
        dest.writeString(pf_doctor);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryRateBean> CREATOR = new Creator<DiaryRateBean>() {
        @Override
        public DiaryRateBean createFromParcel(Parcel in) {
            return new DiaryRateBean(in);
        }

        @Override
        public DiaryRateBean[] newArray(int size) {
            return new DiaryRateBean[size];
        }
    };

    public String getRateSale() {
        return rateSale;
    }

    public void setRateSale(String rateSale) {
        this.rateSale = rateSale;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPf_doctor() {
        return pf_doctor;
    }

    public void setPf_doctor(String pf_doctor) {
        this.pf_doctor = pf_doctor;
    }
}
