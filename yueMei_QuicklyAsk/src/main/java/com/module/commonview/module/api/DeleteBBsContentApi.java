package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/16.
 */

public class DeleteBBsContentApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.FORUM, "delpostcontent", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.d("onServerCallback",mData.toString());
                if ("1".equals(mData.code)){

                        listener.onSuccess(mData.code);

                }
            }
        });
    }
}
