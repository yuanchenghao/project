package com.module.commonview.module.bean;

public class ChatParmsBean {


    /**
     * ymaq_class : 16
     * ymaq_id : 9226
     * guiji : {"title":"腰腹吸脂塑形","image":"https://p34.yuemei.com/tao/2019/0704/200_200/jt190704104252_c75100.jpg","url":"https://m.yuemei.com/tao/40307/","price":"10800","member_price":"-1"}
     * obj_type : 2
     * obj_id : 40307
     * zt_title : 美体塑形
     * zt_id : 9226
     */

    private int ymaq_class;
    private String ymaq_id;
    private GuijiBean guiji;
    private int obj_type;
    private String obj_id;
    private String zt_title;
    private String zt_id;

    public int getYmaq_class() {
        return ymaq_class;
    }

    public void setYmaq_class(int ymaq_class) {
        this.ymaq_class = ymaq_class;
    }

    public String getYmaq_id() {
        return ymaq_id;
    }

    public void setYmaq_id(String ymaq_id) {
        this.ymaq_id = ymaq_id;
    }

    public GuijiBean getGuiji() {
        return guiji;
    }

    public void setGuiji(GuijiBean guiji) {
        this.guiji = guiji;
    }

    public int getObj_type() {
        return obj_type;
    }

    public void setObj_type(int obj_type) {
        this.obj_type = obj_type;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }

    public String getZt_title() {
        return zt_title;
    }

    public void setZt_title(String zt_title) {
        this.zt_title = zt_title;
    }

    public String getZt_id() {
        return zt_id;
    }

    public void setZt_id(String zt_id) {
        this.zt_id = zt_id;
    }

    public static class GuijiBean {
        /**
         * title : 腰腹吸脂塑形
         * image : https://p34.yuemei.com/tao/2019/0704/200_200/jt190704104252_c75100.jpg
         * url : https://m.yuemei.com/tao/40307/
         * price : 10800
         * member_price : -1
         */

        private String title;
        private String image;
        private String url;
        private String price;
        private String member_price;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }
    }
}
