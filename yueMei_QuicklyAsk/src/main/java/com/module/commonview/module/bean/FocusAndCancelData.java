package com.module.commonview.module.bean;

/**
 * 关注取消关注数据
 * Created by 裴成浩 on 2018/4/23.
 */

public class FocusAndCancelData {
    private String is_following;

    public String getIs_following() {
        return is_following;
    }

    public void setIs_following(String is_following) {
        this.is_following = is_following;
    }
}
