package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/5/15
 */
public class ChatDataGuiJi implements Parcelable {
    private String title;
    private String image;
    private String url;
    private String price;
    private String member_price;

    protected ChatDataGuiJi(Parcel in) {
        title = in.readString();
        image = in.readString();
        url = in.readString();
        price = in.readString();
        member_price = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(image);
        dest.writeString(url);
        dest.writeString(price);
        dest.writeString(member_price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatDataGuiJi> CREATOR = new Creator<ChatDataGuiJi>() {
        @Override
        public ChatDataGuiJi createFromParcel(Parcel in) {
            return new ChatDataGuiJi(in);
        }

        @Override
        public ChatDataGuiJi[] newArray(int size) {
            return new ChatDataGuiJi[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }
}
