package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostPeopleList implements Parcelable {
    private String title;
    private List<PostPeoplePeopleList> people;

    protected PostPeopleList(Parcel in) {
        title = in.readString();
        people = in.createTypedArrayList(PostPeoplePeopleList.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeTypedList(people);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PostPeopleList> CREATOR = new Creator<PostPeopleList>() {
        @Override
        public PostPeopleList createFromParcel(Parcel in) {
            return new PostPeopleList(in);
        }

        @Override
        public PostPeopleList[] newArray(int size) {
            return new PostPeopleList[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<PostPeoplePeopleList> getPeople() {
        return people;
    }

    public void setPeople(List<PostPeoplePeopleList> people) {
        this.people = people;
    }
}
