package com.module.commonview.module.bean;

/**
 * 自定义分享控制面板的数据
 * Created by 裴成浩 on 2018/5/16.
 */
public class ButtomDialogBean {
    private ButtomDialogEnum buttomDialogEnum;
    private int image;
    private String title;

    public ButtomDialogBean(ButtomDialogEnum buttomDialogEnum, int image, String title) {
        this.buttomDialogEnum = buttomDialogEnum;
        this.image = image;
        this.title = title;
    }

    public ButtomDialogEnum getButtomDialogEnum() {
        return buttomDialogEnum;
    }

    public void setButtomDialogEnum(ButtomDialogEnum buttomDialogEnum) {
        this.buttomDialogEnum = buttomDialogEnum;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
