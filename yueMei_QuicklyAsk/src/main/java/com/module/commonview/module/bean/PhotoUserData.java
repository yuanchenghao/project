package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotoUserData implements Parcelable {

    /**
     * talent : 0
     * lable :
     * name : 蔷薇薇薇
     * id : 85175869
     * avatar : https://p21.yuemei.com/avatar/085/17/58/69_avatar_50_50.jpg
     * url : https://m.yuemei.com/u/home/85175869/
     */

    private String talent;
    private String lable;
    private String name;
    private String id;
    private String avatar;
    private String url;

    protected PhotoUserData(Parcel in) {
        talent = in.readString();
        lable = in.readString();
        name = in.readString();
        id = in.readString();
        avatar = in.readString();
        url = in.readString();
    }

    public static final Creator<PhotoUserData> CREATOR = new Creator<PhotoUserData>() {
        @Override
        public PhotoUserData createFromParcel(Parcel in) {
            return new PhotoUserData(in);
        }

        @Override
        public PhotoUserData[] newArray(int size) {
            return new PhotoUserData[size];
        }
    };

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(talent);
        dest.writeString(lable);
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(avatar);
        dest.writeString(url);
    }
}
