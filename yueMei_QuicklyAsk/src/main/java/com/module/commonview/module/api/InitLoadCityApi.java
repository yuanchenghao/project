package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * Created by Administrator on 2018/2/22.
 */

public class InitLoadCityApi implements BaseCallBackApi{

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.M_CITY, "city", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.d("InitLoadCityApi","======>"+mData.data);
                listener.onSuccess(mData);
            }
        });
    }
}
