package com.module.commonview.module.bean;

/**
 * Created by 裴成浩 on 2017/11/22.
 */

public class DaiJinJuan {
    private String coupons_status;
    private String is_get;

    public String getCoupons_status() {
        return coupons_status;
    }

    public void setCoupons_status(String coupons_status) {
        this.coupons_status = coupons_status;
    }

    public String getIs_get() {
        return is_get;
    }

    public void setIs_get(String is_get) {
        this.is_get = is_get;
    }
}
