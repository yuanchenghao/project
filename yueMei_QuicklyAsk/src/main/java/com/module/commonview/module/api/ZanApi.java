package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.entity.Status1Data;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by Administrator on 2017/10/13.
 */

public class ZanApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "getagreeport", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.d("ZanApi","-----------"+mData.toString());
                if ("1".equals(mData.code)) {
                    try {
                        Log.d("ZanApi","-----------");
                        Status1Data status1 = JSONUtil.TransformSingleBean(mData.data, Status1Data.class);
                        listener.onSuccess(status1);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
