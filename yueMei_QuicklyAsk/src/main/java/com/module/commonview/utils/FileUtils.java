package com.module.commonview.utils;

import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;

/**
 * 文 件 名: FileUtils
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-07 13:27
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class FileUtils {
    public static boolean isFileExist(File file) {
        if (file != null) {
            return file.exists();
        }

        return false;
    }

    public static boolean createNewFile(String filename, boolean overrideExist) {
        if (TextUtils.isEmpty(filename))
            return false;

        File file = new File(filename);

        boolean blnFileExist = isFileExist(file);
        if (blnFileExist && file.isDirectory())
            return false;

        if (blnFileExist == true) {
            if (overrideExist == false)
                return false;
            else
                file.delete();
        }

        file.getParentFile().mkdirs();

        try {
            return file.createNewFile();
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean fileIsExists(String path) {
        try {
            File f = new File(path);
            if (!f.exists()) {
                return false;
            }
        } catch (Exception e) {

            return false;
        }
        return true;
    }

//    public static String getFileMD5(File file) {
//        if (!file.isFile()) {
//            return null;
//        }
//        MessageDigest digest = null;
//        FileInputStream in = null;
//        byte buffer[] = new byte[1024];
//        int len;
//        try {
//            digest = MessageDigest.getInstance("MD5");
//            in = new FileInputStream(file);
//            while ((len = in.read(buffer, 0, 1024)) != -1) {
//                digest.update(buffer, 0, len);
//            }
//            in.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//        return bytesToHexString(digest.digest());
//    }
//
//    public static String bytesToHexString(byte[] src) {
//        StringBuilder stringBuilder = new StringBuilder("");
//        if (src == null || src.length <= 0) {
//            return null;
//        }
//        for (int i = 0; i < src.length; i++) {
//            int v = src[i] & 0xFF;
//            String hv = Integer.toHexString(v);
//            if (hv.length() < 2) {
//                stringBuilder.append(0);
//            }
//            stringBuilder.append(hv);
//        }
//        return stringBuilder.toString();
//    }

    //删除文件
    public static void delFile(String fileName) {
        File file = new File(fileName);
        if (file.isFile()) {
            file.delete();
        }
        file.exists();
    }

    //删除文件夹和文件夹里面的文件
//    public static boolean deleteFile(String path) {
//        if (TextUtils.isEmpty(path)) {
//            return true;
//        }
//
//        File file = new File(path);
//        if (!file.exists()) {
//            return true;
//        }
//        if (file.isFile()) {
//            return file.delete();
//        }
//        if (!file.isDirectory()) {
//            return false;
//        }
//        for (File f : file.listFiles()) {
//            if (f.isFile()) {
//                f.delete();
//            } else if (f.isDirectory()) {
//                deleteFile(f.getAbsolutePath());
//            }
//        }
//        return file.delete();
//    }
}
