package com.module.commonview.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.module.commonview.adapter.DiscountExpiredAdapter;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.xinlan.imageeditlibrary.editimage.view.imagezoom.easing.Linear;

import org.xutils.common.util.DensityUtil;

/**
 * 文 件 名: DialogUtils
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-16 16:18
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class DialogUtils {
    private static Dialog dialog;
    private static DiscountExpiredAdapter discountExpiredAdapter;

    //优惠过期dialog
    public static void showDiscountExpiredDialog(Context context, String hint, final CallBack callBack) {
        if (context == null)
            return;
        closeDialog();
        dialog = new Dialog(context, R.style.DiscountExpiredDialog);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View inflate = View.inflate(context, R.layout.dialog_discount_expired, null);
        dialog.setContentView(inflate);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    closeDialog();
                    return true;
                }
                return false;
            }
        });
        LinearLayout ll_root = inflate.findViewById(R.id.ll_root);
        LinearLayout ll1 = inflate.findViewById(R.id.ll1);
        LinearLayout ll2 = inflate.findViewById(R.id.ll2);
        TextView tv_title = inflate.findViewById(R.id.tv_title);
        TextView tv_hint = inflate.findViewById(R.id.tv_hint);
        ListView lv = inflate.findViewById(R.id.lv);
        TextView tv_see = inflate.findViewById(R.id.tv_see);
        ImageView iv_close = inflate.findViewById(R.id.iv_close);

        tv_hint.setText(hint);

        discountExpiredAdapter = new DiscountExpiredAdapter(context);
        lv.setAdapter(discountExpiredAdapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onDismiss();
                dialog.dismiss();
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                callBack.onItemClick(position);
                dialog.dismiss();
            }
        });
        tv_see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onSee();
                dialog.dismiss();
            }
        });
        ll_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ll1.setOnClickListener(null);
        ll2.setOnClickListener(null);
        dialog.show();

    }


    public interface CallBack {
        void onItemClick(int pos);

        void onDismiss();

        void onSee();

    }


    public static void closeDialog() {
        try {
            if (dialog != null) {
                dialog.cancel();
                dialog.dismiss();
            }
            if (discountExpiredAdapter != null) {
                discountExpiredAdapter.cancelAllTimers();
            }

        } catch (Exception e) {

        }
    }

}

