package com.module.commonview.view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cn.demo.pinyin.AssortView;
import com.cn.demo.pinyin.PinyinAdapter591;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.InitLoadCityApi;
import com.module.doctor.controller.adapter.HotCityAdapter;
import com.module.doctor.model.api.HotCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.MainCityData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyGridView;

import org.kymjs.aframe.database.KJDB;

import java.util.HashMap;
import java.util.List;

/**
 * 城市选择Popwindows
 * Created by 裴成浩 on 2017/12/13.
 */

public class BaseCityPopwindows extends PopupWindow {

    private final View mView;
    private boolean isShowNear = false;
    private String TAG = "BaseCityPopwindows";
    private final Activity mActivity;
    private final ExpandableListView eListView;

    private final AssortView assortView;
    private final RelativeLayout othserRly;
    private final KJDB kjdbs;
    private View headView;
    private MyGridView hotGridlist;

    private List<MainCityData> citylistCache;
    private PinyinAdapter591 pinAdapter;
    private TextView cityLocTv;
    private List<CityDocDataitem> hotcityList;
    private HotCityAdapter hotcityAdapter;
    private List<MainCityData> citylist;
    private String autoCity;

    public BaseCityPopwindows(Activity mActivity, View v) {
        this(mActivity,v,false);
    }
    public BaseCityPopwindows(Activity mActivity, View v,boolean isShowNear) {
        this.mActivity = mActivity;
        this.mView = v;
        this.isShowNear = isShowNear;

        kjdbs = KJDB.create(mActivity, "yuemeicity");

        final View view = View.inflate(mActivity, R.layout.pop_city_diqu, null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_ins));

        eListView = view.findViewById(R.id.elist1);
        assortView = view.findViewById(R.id.assort1);
        othserRly = view.findViewById(R.id.ly_content_ly1);

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        setBackgroundDrawable(new BitmapDrawable());
        mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//多加这一句，问题就解决了！这句的官方文档解释是：让窗口背景后面的任何东西变暗
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable(mActivity.getResources(), (Bitmap) null));
        setContentView(view);
        update();

        initViewData();
        autoCity = Cfg.loadStr(mActivity, FinalConstant.LOCATING_CITY, "失败");
        cityLocTv.setText(autoCity);

        othserRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
    }

    void initViewData() {

        LayoutInflater mInflater = mActivity.getLayoutInflater();
        headView = mInflater.inflate(R.layout.main_city_select__head_560, null);
        eListView.addHeaderView(headView);

        //附近是否显示隐藏
        LinearLayout nearCity = headView.findViewById(R.id.city_all_near_rly);
        if (isShowNear) {
            nearCity.setVisibility(View.VISIBLE);
            nearCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onAllClickListener != null) {
                        onAllClickListener.onAllClick("附近");
                    }
                }
            });
        } else {
            nearCity.setVisibility(View.GONE);
        }


        hotGridlist = headView.findViewById(R.id.group_grid_list1);

        citylistCache = kjdbs.findAll(MainCityData.class);
        Log.e(TAG, "citylistCache === " + citylistCache.toString());
        Log.e(TAG, "citylistCache === " + citylistCache.size());
        if (null != citylistCache && citylistCache.size() > 0) {
            pinAdapter = new PinyinAdapter591(mActivity, citylistCache);
            eListView.setAdapter(pinAdapter);

            // 展开所有
            for (int i = 0, length = pinAdapter.getGroupCount(); i < length; i++) {
                eListView.expandGroup(i);
            }

            // 字母按键回调
            assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {

                View layoutView = LayoutInflater.from(mActivity).inflate(
                        R.layout.alert_dialog_menu_layout, null);

                TextView text = (TextView) layoutView
                        .findViewById(R.id.content);
                RelativeLayout alRly = (RelativeLayout) layoutView
                        .findViewById(R.id.pop_city_rly);

                PopupWindow popupWindow;

                public void onTouchAssortListener(String str) {
                    int index = pinAdapter.getAssort().getHashList()
                            .indexOfKey(str);
                    if (index != -1) {
                        eListView.setSelectedGroup(index);
                    }
                    if (popupWindow == null) {

                        popupWindow = new PopupWindow(layoutView, 80, 80, false);

                        popupWindow.showAtLocation(alRly, Gravity.CENTER, 0, 0);
                    }
                }

                public void onTouchAssortUP() {
                    if (popupWindow != null)
                        popupWindow.dismiss();
                    popupWindow = null;
                }
            });

            eListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView arg0, View arg1, int groupPosition, int childPosition, long arg4) {
                    if (onAllClickListener != null) {
                        onAllClickListener.onAllClick(PinyinAdapter591.assortAA.getHashList().getValueIndex(groupPosition, childPosition));
                    }
                    return true;
                }
            });

        } else {
            initloadCity();
        }

        loadHotCity();

        RelativeLayout cityAll = headView
                .findViewById(R.id.city_all_doc_rly);
        cityAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (onAllClickListener != null) {
                    onAllClickListener.onAllClick("全国");
                }
            }
        });

        RelativeLayout cityAntuo = headView.findViewById(R.id.city_auto_loaction_rly);
        cityLocTv = headView.findViewById(R.id.doc_city_select_tv);


        cityAntuo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (onAllClickListener != null) {
                    String city = autoCity;
                    String cityName;
                    if ("失败".equals(city)) {
                        cityName = "全国";
                    } else {
                        cityName = city;
                    }
                    Log.e(TAG, "cityName2222 == " + cityName);
                    onAllClickListener.onAllClick(cityName);
                }
            }
        });

    }

    void loadHotCity() {
        new HotCityApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitem) {
                hotcityList = cityDocDataitem;
                hotcityAdapter = new HotCityAdapter(mActivity, hotcityList);
                hotGridlist.setAdapter(hotcityAdapter);
                hotGridlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                        if (onAllClickListener != null) {
                            String cityName = hotcityList.get(pos).getName();
                            Log.e(TAG, "cityName333 == " + cityName);
                            onAllClickListener.onAllClick(cityName);
                        }
                    }
                });
            }

        });

    }

    void initloadCity() {
        new InitLoadCityApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "serverData.data === " + serverData.data);
                    citylist = JSONUtil.jsonToArrayList(serverData.data, MainCityData.class);

                    if (null != citylist && citylist.size() > 0) {
                        pinAdapter = new PinyinAdapter591(mActivity,
                                citylist);
                        eListView.setAdapter(pinAdapter);
                    }

                    // 展开所有
                    for (int i = 0, length = pinAdapter
                            .getGroupCount(); i < length; i++) {
                        eListView.expandGroup(i);
                    }

                    // 字母按键回调
                    assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {

                        View layoutView = LayoutInflater
                                .from(mActivity)
                                .inflate(
                                        R.layout.alert_dialog_menu_layout,
                                        null);

                        TextView text = (TextView) layoutView
                                .findViewById(R.id.content);
                        RelativeLayout alRly = (RelativeLayout) layoutView
                                .findViewById(R.id.pop_city_rly);

                        PopupWindow popupWindow;

                        public void onTouchAssortListener(
                                String str) {
                            int index = pinAdapter
                                    .getAssort()
                                    .getHashList()
                                    .indexOfKey(str);
                            if (index != -1) {
                                eListView
                                        .setSelectedGroup(index);
                            }
                            if (popupWindow != null) {
                                // text.setText(str);
                            } else {
                                popupWindow = new PopupWindow(
                                        layoutView, 80, 80,
                                        false);
                                popupWindow.showAtLocation(
                                        alRly,
                                        Gravity.CENTER, 0,
                                        0);
                            }
                        }

                        public void onTouchAssortUP() {
                            if (popupWindow != null)
                                popupWindow.dismiss();
                            popupWindow = null;
                        }
                    });

                    eListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                        @Override
                        public boolean onChildClick(ExpandableListView arg0, View arg1, int groupPosition, int childPosition, long arg4) {
                            if (onAllClickListener != null) {
                                onAllClickListener.onAllClick(PinyinAdapter591.assortAA.getHashList().getValueIndex(groupPosition, childPosition));
                            }
                            return true;
                        }
                    });
                }
            }
        });
    }

    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }
    }

    public void showPop(int rawY) {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
//        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
//            showAsDropDown(mView);
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        }

    }

    //城市选择回调
    private OnAllClickListener onAllClickListener;

    public interface OnAllClickListener {
        void onAllClick(String city);
    }

    public void setOnAllClickListener(OnAllClickListener onAllClickListener) {
        this.onAllClickListener = onAllClickListener;
    }
}
