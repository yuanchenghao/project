package com.module.commonview.view;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.module.commonview.adapter.TaoPopAdapter;
import com.module.other.module.bean.TaoPopItemData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 智能排序PopupWindow
 * Created by 裴成浩 on 2017/12/13.
 */
public class BaseSortPopupwindows extends PopupWindow {

    private final View mView;
    private final String TAG = "BaseSortPopupwindows";
    private final Activity mActivity;
    private TaoPopAdapter mSort2Adapter;
    private List<TaoPopItemData> mLvSortData = new ArrayList<>();
    private final ListView prlist1;

    @SuppressWarnings("deprecation")
    public BaseSortPopupwindows(Activity activity, View v, List<TaoPopItemData> lvSortData) {
        this.mLvSortData = lvSortData;
        this.mView = v;
        this.mActivity = activity;
        Log.e("BaseSort", mLvSortData.toString() + "=====" + mLvSortData.size());
        final View view = View.inflate(mActivity, R.layout.pop_tao_zx_project, null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();

        prlist1 = view.findViewById(R.id.pop_list_tao_project_list);
        RelativeLayout otherRly1 = view.findViewById(R.id.ly_content_ly1);
        RelativeLayout otherRly2 = view.findViewById(R.id.ly_content_ly2);
        otherRly1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                dismiss();
            }
        });
        otherRly2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                dismiss();
            }
        });

        mSort2Adapter = new TaoPopAdapter(mActivity, mLvSortData, 0);
        prlist1.setAdapter(mSort2Adapter);

        prlist1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (onSequencingClickListener != null) {
                    TaoPopItemData data = setSelected(pos);
                    if (data != null) {
                        String sortId = data.get_id();
                        String sortName = data.getName();
                        onSequencingClickListener.onSequencingClick(pos, sortId, sortName);
                    }
                }
            }
        });
    }

    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }
    }

    public void showPop(int rawY) {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
//        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
//            showAsDropDown(mView);
        }
    }

    public void setAdapter(int curpo) {
        mSort2Adapter.setNotifyCurpo(curpo);
    }

    public List<TaoPopItemData> getLvSortData() {
        return mLvSortData;
    }


    /**
     * 设置选中项
     *
     * @param pos
     */
    public TaoPopItemData setSelected(int pos) {
        mSort2Adapter.setNotifyCurpo(pos);

        List<TaoPopItemData> taoPopItemData = mSort2Adapter.getTaoPopItemData();
        if (pos < taoPopItemData.size()) {
            return taoPopItemData.get(pos);
        } else {
            return null;
        }
    }

    //智能排序选择回调
    private OnSequencingClickListener onSequencingClickListener;

    public interface OnSequencingClickListener {
        void onSequencingClick(int pos, String sortId, String sortName);
    }

    public void setOnSequencingClickListener(OnSequencingClickListener onSequencingClickListener) {
        this.onSequencingClickListener = onSequencingClickListener;
    }
}
