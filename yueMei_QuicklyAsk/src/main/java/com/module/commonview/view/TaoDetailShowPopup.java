package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.TaoDetailShowPopupData;
import com.module.commonview.module.bean.YuDingData;
import com.module.my.view.orderpay.OrderWriteMessageActivity639;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailData639;
import com.quicklyask.util.Utils;

import java.util.HashMap;

/**
 * 预定谈层
 * Created by 裴成浩 on 2017/12/15.
 */

public class TaoDetailShowPopup extends PopupWindow {

    private TaoDetailData639 mTaoDetailData;
    private String is_fanxian;
    private String groupId;
    private String hos_userid;
    private PageJumpManager pageJumpManager;
    private String mSource;
    private String mObjid;
    private String taoid;
    private String payType = "";
    private String refund;
    private String mType;
    private Activity mActivity;
    private YuDingData ydData;
    private String isGroup;
    private String fenqi;
    private String relTitle;
    private int groupPriceInt;
    private int lijianInt;

    String depositOrPayment;    //判断订金还是全款
    private String TAG = "TaoDetailShowPopup";
    private String mPostStr;
    private String mU;

    /**
     * @param activity
     * @param mData
     */
    public TaoDetailShowPopup(Activity activity, TaoDetailShowPopupData mData, TaoDetailData639 taoDetailData591,String postStr,String u) {
        this.mActivity = activity;
        this.ydData = mData.getYdData();
        this.mSource = mData.getSource();
        this.mObjid = mData.getObjid();
        this.mType = mData.getType();
        this.groupId = mData.getGroupId();
        this.is_fanxian = mData.getIs_fanxian();
        this.mU=u;
        HashMap<String, String> mMap = mData.getmMap();

        this.mTaoDetailData = taoDetailData591;
        this.mPostStr=postStr;
        pageJumpManager = new PageJumpManager(mActivity);

        fenqi = mMap.get("fenqi");
        refund = mMap.get("refund");
        taoid = mMap.get("taoid");

        hos_userid = mMap.get("hos_userid");

        relTitle = mMap.get("relTitle");

        // 拼团价
        if (TextUtils.isEmpty(ydData.getGroup_price())) {
            groupPriceInt = 0;
        } else {
            groupPriceInt = Integer.parseInt(ydData.getGroup_price());
        }
        // 立减
        if (TextUtils.isEmpty(ydData.getLijian())) {
            lijianInt = 0;
        } else {
            lijianInt = Integer.parseInt(ydData.getLijian());
        }

        //是否是拼团
        isGroup = ydData.getIs_group();

        initView();

    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        final View view = View.inflate(mActivity, R.layout.view_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setContentView(view);
        update();

        ImageView mBtnClose = view.findViewById(R.id.btnClose);
        LinearLayout otherLy = view.findViewById(R.id.goods_param_head);
        TextView docname = view.findViewById(R.id.yd_doc_name_tv);// 医生姓名
        TextView hosname = view.findViewById(R.id.yd_hos_name_tv);// 医院姓名
        final LinearLayout selectLy = view.findViewById(R.id.yd_select_method_ly);// 选择支付方式框
        final TextView select1 = view.findViewById(R.id.yd_select1_dingjin);// 选择定金
        final TextView select2 = view.findViewById(R.id.yd_select2_zhiding);// 选择直订
        TextView lijianTopTv = view.findViewById(R.id.yd_lijian_top_tv);// 立减tips
        TextView yuemeiJiaTv = view.findViewById(R.id.yd_yuemei_jia_tv);// 悦美价/拼团价
        TextView zhidingLijianTv = view.findViewById(R.id.yd_zhiding_lijian_tv);// 直订立减
        final RelativeLayout zhidingRly = view.findViewById(R.id.yd_zhiding_lijian_rly);// 直订立减框
        TextView onlineDingjinTv = view.findViewById(R.id.yd_online_dingjin_tv);// 在线订金
        final RelativeLayout onlineDingjinRly = view.findViewById(R.id.yd_online_dingjin_rly);// 在线订金框
        TextView onlineQuankuanTv = view.findViewById(R.id.yd_online_quankuan_tv);// 在线金额
        final RelativeLayout onlineQuankuanRly = view.findViewById(R.id.yd_online_quankuan_rly);// 在线金额框
        TextView daoYuanTv = view.findViewById(R.id.yd_daoyuan_zhifu_tv);// 到院支付
        final RelativeLayout daoyuanRly = view.findViewById(R.id.yd_daoyuan_zhifu_rly);// 到院支付框
        final RelativeLayout daoYuanTipsTv = view.findViewById(R.id.yd_daoyuan_tips_rly);// 到院支付提示
        TextView jifeiTv = view.findViewById(R.id.tao_detail_jifei_tv);

        final ImageView selectIv1 = view.findViewById(R.id.yd_select_img_iv1);// 选择变化图
        final ImageView selectIv2 = view.findViewById(R.id.yd_select_img_iv2);// 选择变化图

        final TextView priceTitle = view.findViewById(R.id.tv_price_title);     // 价格文案

        //分期
        final RelativeLayout fenqiLy = view.findViewById(R.id.taodetail_fenqi_rly);
        final TextView fenqiTv = view.findViewById(R.id.taolist_isfenqi_tv);
        final RelativeLayout fenqiNoLy = view.findViewById(R.id.taodetail_nofenqi_rly);

        Button tellBt = view.findViewById(R.id.yd_tao_tel_bt);
        Button orderBt = view.findViewById(R.id.yd_tao_order_bt);

        LinearLayout refundLys = view.findViewById(R.id.tao_pop_refund_tips_ly);
        TextView refundTitle = view.findViewById(R.id.tao_pop_refund_title);

        LinearLayout speeds = view.findViewById(R.id.tao_pop_speed_tips_ly);
        TextView speedTitle = view.findViewById(R.id.tao_pop_speed_title);

        TextView relTitleTv = view.findViewById(R.id.taode_tan_rel_title_tv);


        //初始化数据
        docname.setText(ydData.getDoc_name());
        hosname.setText(ydData.getHos_name());
        zhidingLijianTv.setText("-" + ydData.getLijian());
        select2.setText("全款");

        if (null != fenqi && fenqi.length() > 0) {
            fenqiTv.setText(fenqi);
        }

        if (null != relTitle && relTitle.length() > 1) {
            relTitleTv.setText("(" + relTitle + ")");
        }

        if ("1".equals(refund)) {
            refundLys.setVisibility(View.VISIBLE);
            speeds.setVisibility(View.VISIBLE);
        } else {
            refundLys.setVisibility(View.GONE);
            speeds.setVisibility(View.GONE);
        }

        if ("1".equals(isGroup)) {
            if ("0".equals(mType)) {
                priceTitle.setText("悦美价：");
                refundTitle.setText("未消费可随时退款");
                speedTitle.setText("极速退款");
                yuemeiJiaTv.setText("￥" + mTaoDetailData.getPay_price().getPayPrice());       //会员价
                onlineDingjinTv.setText("￥" + mTaoDetailData.getPay_price().getDingjin()); //在线支付会员定金
                onlineQuankuanTv.setText("￥" + mTaoDetailData.getPay_price().getPayPrice());// 全款金额
                daoYuanTv.setText("￥" + mTaoDetailData.getPay_price().getHos_price());
            } else {
                priceTitle.setText("拼团价：");
                refundTitle.setText("48小时未成团自动极速退");
                speedTitle.setText("未消费可退款（成团后申请，7日后自动退款）");
                yuemeiJiaTv.setText("￥" + ydData.getGroup_price()); //拼团价
                onlineDingjinTv.setText("￥" + ydData.getGroup_dingjin()); //在线拼团支付定金
                onlineQuankuanTv.setText("￥" + groupPriceInt);      //拼团在线支付金额
                int surplusPrice = Integer.parseInt(ydData.getGroup_price()) - Integer.parseInt(ydData.getGroup_dingjin());
                daoYuanTv.setText("￥" + surplusPrice);         //到医院决定消费后付款金额
            }
        } else {
            if ("1".equals(mTaoDetailData.getPay_price().getIs_member())){
                priceTitle.setText("会员价：");
            }else {
                priceTitle.setText("悦美价：");

            }
            refundTitle.setText("未消费可随时退款");
            speedTitle.setText("极速退款");
            yuemeiJiaTv.setText("￥" + mTaoDetailData.getPay_price().getPayPrice());       //会员价
            onlineDingjinTv.setText("￥" + mTaoDetailData.getPay_price().getDingjin()); //在线支付会员定金
            onlineQuankuanTv.setText("￥" + mTaoDetailData.getPay_price().getPayPrice());// 全款金额
            daoYuanTv.setText("￥" + mTaoDetailData.getPay_price().getHos_price());         //到医院决定消费后付款金额

        }
        depositOrPayment = ydData.getGroup_is_order();

        if (null != ydData.getFeeScale() && ydData.getFeeScale().length() > 0 && !"0".equals(ydData.getFeeScale())) {
            jifeiTv.setVisibility(View.VISIBLE);
            jifeiTv.setText(ydData.getFeeScale());
        } else {
            jifeiTv.setVisibility(View.GONE);
        }


        if (lijianInt == 0) {
            lijianTopTv.setVisibility(View.GONE);
            zhidingLijianTv.setVisibility(View.GONE);

        } else {
            // lijianTopTv.setText("立减" + "1000");
            lijianTopTv.setText("手机专享补贴" + lijianInt);
        }

        /**
         * (2全款、订金；3订金;4全款)
         */
        if ("3".equals(depositOrPayment)) {
            payType = "3";
            selectLy.setVisibility(View.GONE);
            onlineQuankuanRly.setVisibility(View.GONE);
            daoYuanTipsTv.setVisibility(View.GONE);
            zhidingRly.setVisibility(View.GONE);
        } else if ("4".equals(depositOrPayment)) {
            payType = "4";
            selectLy.setVisibility(View.GONE);
            onlineDingjinRly.setVisibility(View.GONE);
            daoyuanRly.setVisibility(View.INVISIBLE);

            if (null != ydData.getLijian() && !"0".equals(ydData.getLijian())) {
                zhidingRly.setVisibility(View.VISIBLE);
            } else {
                zhidingRly.setVisibility(View.GONE);
            }
        } else if ("2".equals(depositOrPayment)) {

            payType = "3";
            onlineQuankuanRly.setVisibility(View.GONE);
            daoYuanTipsTv.setVisibility(View.GONE);
            zhidingRly.setVisibility(View.GONE);

            onlineDingjinRly.setVisibility(View.VISIBLE);
            daoyuanRly.setVisibility(View.VISIBLE);

            if (null != fenqi && fenqi.length() > 0) {
                fenqiLy.setVisibility(View.GONE);
                fenqiNoLy.setVisibility(View.VISIBLE);
            }


            select1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {//订金
                    payType = "3";
                    select1.setBackgroundResource(R.drawable.biankuang_juse);
                    select1.setTextColor(Color.parseColor("#ff4400"));
                    select2.setBackgroundResource(R.drawable.biankuang_hui);
                    select2.setTextColor(Color.parseColor("#606670"));
                    // selectIv.setBackgroundResource(R.drawable.one_jiantou1);
                    selectIv1.setVisibility(View.VISIBLE);
                    selectIv2.setVisibility(View.GONE);

                    onlineQuankuanRly.setVisibility(View.GONE);
                    daoYuanTipsTv.setVisibility(View.GONE);
                    zhidingRly.setVisibility(View.GONE);

                    onlineDingjinRly.setVisibility(View.VISIBLE);
                    daoyuanRly.setVisibility(View.VISIBLE);

                    if (null != fenqi && fenqi.length() > 0) {
                        fenqiLy.setVisibility(View.GONE);
                        fenqiNoLy.setVisibility(View.VISIBLE);
                    }

                    Utils.tongjiApp(mActivity, "tao_alert_dingjin", "tao", taoid, mSource);

                }
            });

            select2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {//全款
                    payType = "4";
                    select1.setBackgroundResource(R.drawable.biankuang_hui);
                    select1.setTextColor(Color.parseColor("#606670"));
                    select2.setBackgroundResource(R.drawable.biankuang_juse);
                    select2.setTextColor(Color.parseColor("#ff4400"));
                    // selectIv.setBackgroundResource(R.drawable.two_jiantou1);
                    selectIv1.setVisibility(View.GONE);
                    selectIv2.setVisibility(View.VISIBLE);

                    onlineQuankuanRly.setVisibility(View.VISIBLE);
                    daoYuanTipsTv.setVisibility(View.GONE);
//                        zhidingRly.setVisibility(View.VISIBLE);

                    onlineDingjinRly.setVisibility(View.GONE);
                    daoyuanRly.setVisibility(View.INVISIBLE);


                    if (null != fenqi && fenqi.length() > 0) {
                        fenqiLy.setVisibility(View.VISIBLE);
                        fenqiNoLy.setVisibility(View.GONE);
                    }

                    Utils.tongjiApp(mActivity, "tao_alert_quankuan", "tao", taoid, mSource);

                }
            });
        }


        mBtnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });


        otherLy.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        tellBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Utils.tongjiApp(mActivity, "tao_kefu", "bottom", taoid, mSource);
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(taoid)
                        .setObjType("2")
                        .setTitle( mTaoDetailData.getTitle())
                        .setPrice(mTaoDetailData.getPrice_discount())
                        .setImg(mTaoDetailData.getPic().get(0).getImg())
                        .setYmClass("2")
                        .setYmId(taoid)
                        .build();
                pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
            }
        });

        orderBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.e("MMMMMM", "taoid === " + taoid);
                Log.e("MMMMMM", "payType === " + payType);
                Log.e("MMMMMM", "mSource === " + mSource);
                Log.e("MMMMMM", "mObjid === " + mObjid);
                Log.e("MMMMMM", "groupId === " + groupId);
                Log.e("MMMMMM", "mType === " + mType);
                Log.e("MMMMMM", "is_fanxian === " + is_fanxian);
                Intent it = new Intent(mActivity, OrderWriteMessageActivity639.class);
                it.putExtra("id", taoid);
                it.putExtra("payType", payType);
                it.putExtra("source", mSource);
                it.putExtra("objid", mObjid);
                it.putExtra("type", mType);
                it.putExtra("group_id", groupId);
                it.putExtra("data", mTaoDetailData);
                it.putExtra("postStr",mPostStr);
                it.putExtra("u",mU);

                mActivity.startActivity(it);

                Utils.tongjiApp(mActivity, "tao_alert_order", "tao", taoid, mSource);

                dismiss();
                mActivity.finish();
            }
        });
    }
}
