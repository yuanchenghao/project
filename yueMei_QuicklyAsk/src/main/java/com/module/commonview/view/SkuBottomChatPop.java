package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.commonview.adapter.SkuBottomChatPopAdapter;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoQuickReply;

import java.util.List;

public class SkuBottomChatPop extends PopupWindow {
    private PopItemClickListener mPopItemClickListener;
    public SkuBottomChatPop(final Context context, List<TaoQuickReply> taoQuickReply){
        final View view = View.inflate(context, R.layout.sku_bottom_chatpop, null);

        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(view);
        update();

        setTouchInterceptor(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_OUTSIDE){
                    return true;
                }
                return false;
            }
        });



        RecyclerView chatList = view.findViewById(R.id.sku_bottom_list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        chatList.setLayoutManager(linearLayoutManager);
        SkuBottomChatPopAdapter chatPopAdapter = new SkuBottomChatPopAdapter(R.layout.bottom_chat_list_item, taoQuickReply);
        chatList.setAdapter(chatPopAdapter);
        chatPopAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mPopItemClickListener.onItemClick(adapter,view,position);
            }
        });
    }


    public void setPopItemClickListener(PopItemClickListener popItemClickListener) {
        mPopItemClickListener = popItemClickListener;
    }


    public interface PopItemClickListener{
        void onItemClick(BaseQuickAdapter adapter, View view, int position);
    }
}
