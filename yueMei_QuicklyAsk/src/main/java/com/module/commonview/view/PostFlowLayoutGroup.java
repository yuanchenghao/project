package com.module.commonview.view;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.commonview.module.bean.DiaryTagList;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.List;

/**
 * 帖子页面标签
 * Created by 裴成浩 on 2018/8/21.
 */
public class PostFlowLayoutGroup {

    private final String TAG = "PostFlowLayoutGroup";
    private Activity mContext;
    private FlowLayout mPostFlowLayout;
    private List<DiaryTagList> mTags;


    public PostFlowLayoutGroup(Activity context, FlowLayout postFlowLayout, List<DiaryTagList> tags) {
        this.mContext = context;
        this.mPostFlowLayout = postFlowLayout;
        this.mTags = tags;
        Log.e(TAG, "mTags == " + mTags.size());
        Log.e(TAG, "mTags == " + mTags);
        Log.e(TAG, "mTags == " + mTags.toString());
        initView();
    }

    /**
     * 初始化样式
     */
    private void initView() {
        mPostFlowLayout.removeAllViews();
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(Utils.dip2px(10), 0, Utils.dip2px(10), 0);
        for (int i = 0; i < mTags.size(); i++) {
            DiaryTagList data = mTags.get(i);

            TextView view = new TextView(mContext);
            view.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            view.setTextSize(12);
            view.setText(data.getName());
            view.setBackgroundResource(R.drawable.shape_bian_yuanjiao_f6f6f6);

            //设置图片
            Drawable drawable;
            if ("1".equals(data.getJump_type())) {
                drawable = Utils.getLocalDrawable(mContext, R.drawable.adiary_list_top_sku);
            } else {
                drawable = Utils.getLocalDrawable(mContext, R.drawable.adiary_list_top_others);
            }
            drawable.setBounds(0, 0, Utils.dip2px(11), Utils.dip2px(11));
            view.setCompoundDrawables(drawable, null, null, null);
            view.setCompoundDrawablePadding(Utils.dip2px(2));

            mPostFlowLayout.addView(view, i, lp);

            view.setTag(i);
            view.setOnClickListener(new onClickView());
        }
    }

    /**
     * 点击按钮回调
     */
    class onClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            for (int i = 0; i < mTags.size(); i++) {
                if (i == selected) {
                    if (clickCallBack != null) {
                        clickCallBack.onClick(v, i, mTags.get(i));
                    }
                }
            }
        }
    }

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onClick(View v, int pos, DiaryTagList data);
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

}
