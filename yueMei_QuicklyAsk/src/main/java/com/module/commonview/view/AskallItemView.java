package com.module.commonview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class AskallItemView extends RelativeLayout {
    private Context mContext;
    private TextView mQuestion;
    private TextView mAnswer;
    public AskallItemView(Context context) {
        this(context,null);
    }

    public AskallItemView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public AskallItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext=context;
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.askall_itemview, this, true);
        mQuestion=view.findViewById(R.id.sku_comment_aks_question);
        mAnswer=view.findViewById(R.id.sku_comment_aks_answer);
    }

    public void setConent(String question,String answer){
        mQuestion.setText(question);
        mAnswer.setText(answer);
    }
}
