package com.module.commonview.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.PostListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.List;

import static android.view.Gravity.CENTER_VERTICAL;

/**
 * 医生dialog流式布局
 */
public class DoctorFlowLayoutGroup {

    private final String TAG = "DoctorFlowLayoutGroup";
    private Context mContext;
    private FlowLayout mFlowLayout;
    private List<String> mSpeciality_board;


    public DoctorFlowLayoutGroup(Context context, FlowLayout postFlowLayout, List<String> speciality_board) {
        this.mContext = context;
        this.mFlowLayout = postFlowLayout;
        this.mSpeciality_board = speciality_board;
        initView();
    }

    /**
     * 初始化样式
     */
    private void initView() {
        mFlowLayout.removeAllViews();
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(Utils.dip2px(10), 0, Utils.dip2px(10), 0);
        for (int i = 0; i < mSpeciality_board.size(); i++) {
//            DiaryTagList data = mTags.get(i);

            TextView view = new TextView(mContext);
            view.setGravity(CENTER_VERTICAL);
            view.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            view.setTextSize(12);
            view.setText(mSpeciality_board.get(i));
            view.setBackgroundResource(R.drawable.shape_doctor);

//            //设置图片
//            Drawable drawable;
//            if ("1".equals(data.getJump_type())) {
//                drawable = Utils.getLocalDrawable(mContext, R.drawable.adiary_list_top_sku);
//            } else {
//                drawable = Utils.getLocalDrawable(mContext, R.drawable.adiary_list_top_others);
//            }
//            drawable.setBounds(0, 0, Utils.dip2px(11), Utils.dip2px(11));
//            view.setCompoundDrawables(drawable, null, null, null);
//            view.setCompoundDrawablePadding(Utils.dip2px(2));

            mFlowLayout.addView(view, i, lp);

            view.setTag(i);
            view.setOnClickListener(new onClickView());
        }
    }

    /**
     * 点击按钮回调
     */
    class onClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            for (int i = 0; i < mSpeciality_board.size(); i++) {
                if (i == selected) {
                    if (clickCallBack != null) {
                        clickCallBack.onClick(v, i, mSpeciality_board.get(i));
                    }
                }
            }
        }
    }

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onClick(View v, int pos, String board);
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

}
