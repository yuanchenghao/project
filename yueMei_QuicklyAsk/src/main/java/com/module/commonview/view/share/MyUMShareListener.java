package com.module.commonview.view.share;

import android.app.Activity;
import android.widget.Toast;

import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * 分享的回调
 * Created by 裴成浩 on 2018/1/4.
 */

public class MyUMShareListener implements UMShareListener {

    private Activity mActivity;

    public MyUMShareListener(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onStart(SHARE_MEDIA share_media) {

    }

    @Override
    public void onResult(SHARE_MEDIA platform) {
        onShareResultClickListener.onShareResultClick(platform);
    }

    @Override
    public void onError(SHARE_MEDIA platform, Throwable t) {
        onShareResultClickListener.onShareErrorClick(platform,t);
    }

    @Override
    public void onCancel(SHARE_MEDIA platform) {
        Toast.makeText(mActivity, platform + " 分享取消了", Toast.LENGTH_SHORT).show();
    }


    //分享成功回调
    public interface OnShareResultClickListener {
        void onShareResultClick(SHARE_MEDIA platform);
        void onShareErrorClick(SHARE_MEDIA platform,Throwable t);
    }

    private OnShareResultClickListener onShareResultClickListener;

    public void setOnShareResultClickListener(OnShareResultClickListener onShareResultClickListener) {
        this.onShareResultClickListener = onShareResultClickListener;
    }

}
