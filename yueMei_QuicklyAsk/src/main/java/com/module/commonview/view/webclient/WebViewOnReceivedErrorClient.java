package com.module.commonview.view.webclient;

import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;

/**
 * Created by 裴成浩 on 2018/1/23.
 */

public interface WebViewOnReceivedErrorClient {
    void onReceivedErrorClient(WebView view, WebResourceRequest request, WebResourceError error);
}
