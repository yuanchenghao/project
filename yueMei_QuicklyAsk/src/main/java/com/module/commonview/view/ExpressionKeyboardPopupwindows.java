package com.module.commonview.view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;

import com.quicklyack.emoji.Expressions;
import com.quicklyask.activity.R;

import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表情键盘选择
 * Created by 裴成浩 on 2017/11/3.
 */

public class ExpressionKeyboardPopupwindows extends PopupWindow {

    private String TAG = "ExpressionKeyboard";

    private EditText mPostContent;
    private ImageButton colseBiaoqing;
    private ViewPager viewPager;
    private ImageView page0;
    private ImageView page1;
    private ImageView page2;
    private Activity mActivity;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private int[] expressionImages2;
    private String[] expressionImageNames2;
    private final LayoutInflater inflater;
    private GridView gView2;
    private GridView gView3;
    private final View view;


    /**
     * 初始化表情键盘
     *
     * @param activity
     * @param postContent：输入表情的输入框
     */
    public ExpressionKeyboardPopupwindows(Activity activity, EditText postContent) {

        this.mActivity = activity;
        this.mPostContent = postContent;
        inflater = LayoutInflater.from(mActivity);

        view = View.inflate(mActivity, R.layout.pop_expression_keyboard, null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity,
                R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        setHeight(DensityUtil.dip2px(250));

        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(false);
        setOutsideTouchable(false);
        setContentView(view);
        update();

        initView();
        initViewPager();
    }

    /**
     * 初始化组件
     */
    private void initView() {

        colseBiaoqing = view.findViewById(R.id.colse_biaoqingjian_bt);
        viewPager = view.findViewById(R.id.viewpager);
        page0 = view.findViewById(R.id.page0_select);
        page1 = view.findViewById(R.id.page1_select);
        page2 = view.findViewById(R.id.page2_select);


        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;
        expressionImages2 = Expressions.expressionImgs2;
        expressionImageNames2 = Expressions.expressionImgNames2;



        //关闭表情键盘
        colseBiaoqing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                onColseBiaoqingClickListener.onColseBiaoqingClick();
            }
        });

    }

    /**
     * 初始化表情
     */
    private void initViewPager() {

        final ArrayList<GridView> grids = new ArrayList<>();
        GridView gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mActivity, listItems,
                R.layout.singleexpression, new String[]{"image"},
                new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    mPostContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Log.e(TAG, "第一页");
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(mActivity.getResources(),
                            expressionImages[arg2 % expressionImages.length]);

//                    bitmap = Utils.zoomImage(bitmap, 47, 47);
//                    ImageSpan imageSpan = new ImageSpan(mActivity, bitmap,ImageSpan.ALIGN_BOTTOM);

                    //居中对齐imageSpan
                    CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mActivity,bitmap);

                    SpannableString spannableString = new SpannableString(
                            expressionImageNames[arg2].substring(1,
                                    expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0,
                            expressionImageNames[arg2].length() - 2,
                            ImageSpan.ALIGN_BASELINE);

                    // 编辑框设置数据
                    mPostContent.append(spannableString);

                }
            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);
        gView3 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView3);

        // grids.add(gView3);
        // System.out.println("GridView的长度 = " + grids.size());

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_focused));
                    page1.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_unfocused));
                    page2.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_focused));
                    page0.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_unfocused));
                    page2.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mActivity,
                            listItems, R.layout.singleexpression,
                            new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                mPostContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {
                                Log.e(TAG, "第二页");
                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(
                                        mActivity.getResources(), expressionImages1[pos % expressionImages1.length]);
//                                bitmap = Utils.zoomImage(bitmap, 47, 47);

//                                ImageSpan imageSpan = new ImageSpan(mActivity, bitmap,ImageSpan.ALIGN_BOTTOM);
                                CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mActivity,bitmap);

                                SpannableString spannableString = new SpannableString(
                                        expressionImageNames1[pos]
                                                .substring(1,
                                                        expressionImageNames1[pos]
                                                                .length() - 1));

                                spannableString.setSpan(imageSpan, 0,
                                        expressionImageNames1[pos].length() - 2,
                                        ImageSpan.ALIGN_BASELINE);
                                // 编辑框设置数据
                                mPostContent.append(spannableString);

                            }
                        }
                    });
                    break;
                case 2:
                    page0.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_unfocused));
                    page1.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_unfocused));
                    page2.setImageDrawable(mActivity.getResources().getDrawable(
                            R.drawable.page_focused));
                    List<Map<String, Object>> listItems2 = new ArrayList<Map<String, Object>>();
                    // 生成28个表情
                    for (int i = 0; i < 15; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages2[i]);
                        listItems2.add(listItem);
                    }

                    SimpleAdapter simpleAdapter2 = new SimpleAdapter(mActivity,
                            listItems2, R.layout.singleexpression,
                            new String[]{"image"}, new int[]{R.id.image});

                    gView3.setAdapter(simpleAdapter2);
                    // 表情点击
                    gView3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                mPostContent.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {
                                Log.e(TAG, "第三页");
                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(
                                        mActivity.getResources(), expressionImages2[pos % expressionImages2.length]);
//                                bitmap = Utils.zoomImage(bitmap, 47, 47);

//                                ImageSpan imageSpan = new ImageSpan(mActivity, bitmap,ImageSpan.ALIGN_BOTTOM);
                                CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mActivity,bitmap);

                                SpannableString spannableString = new SpannableString(
                                        expressionImageNames2[pos]
                                                .substring(1,
                                                        expressionImageNames2[pos]
                                                                .length() - 1));

                                spannableString.setSpan(imageSpan, 0,
                                        expressionImageNames2[pos].length() - 2,
                                        ImageSpan.ALIGN_BASELINE);
                                // 编辑框设置数据
                                mPostContent.append(spannableString);
                            }
                        }
                    });
                    break;
            }
        }
    }


    private onColseBiaoqingClickListener onColseBiaoqingClickListener;

    //关闭表情键盘回调
    public interface onColseBiaoqingClickListener {
        void onColseBiaoqingClick();
    }

    public void setOnColseBiaoqingClickListener(onColseBiaoqingClickListener onColseBiaoqingClickListener) {
        this.onColseBiaoqingClickListener = onColseBiaoqingClickListener;
    }
}
