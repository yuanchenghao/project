package com.module.commonview.view.webclient;

import android.webkit.WebView;

/**
 * Created by 裴成浩 on 2018/1/23.
 */

public interface WebViewOnLoadResourceClient {
    void onLoadResource(WebView view, String url);
}
