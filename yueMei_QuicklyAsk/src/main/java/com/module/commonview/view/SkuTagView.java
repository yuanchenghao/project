package com.module.commonview.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class SkuTagView extends LinearLayout {
    private TextView mSkuTagView;
    private Context mContext;
    public SkuTagView(Context context) {
        this(context,null);
    }

    public SkuTagView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public SkuTagView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext=context;
        initView();
    }

    private void initView() {

        View view = LayoutInflater.from(mContext).inflate(R.layout.sku_tagview, this, true);
        mSkuTagView=view.findViewById(R.id.sku_tag_view);
        mSkuTagView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextViewListener.textClickListener(v);
            }
        });
    }


    public void setIsSelect(String checked, String status, String name){
        if ("1".equals(checked)){
            mSkuTagView.setBackground(ContextCompat.getDrawable(mContext,R.drawable.sku_tagview_select));
            mSkuTagView.setTextColor(ContextCompat.getColor(mContext,R.color.red_ff4965));
            mSkuTagView.setEnabled(false);
        }else {
            if ("4".equals(status)){
                mSkuTagView.setTextColor(ContextCompat.getColor(mContext,R.color._3));
                mSkuTagView.setEnabled(true);
            }else {
                mSkuTagView.setTextColor(ContextCompat.getColor(mContext,R.color._cc));
                mSkuTagView.setEnabled(false);
            }
            mSkuTagView.setBackground(ContextCompat.getDrawable(mContext,R.drawable.sku_tagview_unselect));
        }
        mSkuTagView.setText(name);
    }


    private TextViewListener mTextViewListener;

    public void setTextViewListener(TextViewListener textViewListener) {
        mTextViewListener = textViewListener;
    }

    public interface TextViewListener{
        void textClickListener(View view);
    }


}
