package com.module.commonview.view;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 指南引导页面
 * Created by 裴成浩 on 2018/5/7.
 */
public class TipGuidePowindow extends PopupWindow {

    private Activity mActivity;

    private LinearLayout popBackground;
    private TextView popGuideContent;
    private ImageView popGuideImage;

    public static final int HOME_BUBBLE_LEFT = R.drawable.home_bubble_left;
    public static final int HOME_BUBBLE_RIGHT = R.drawable.home_bubble_right;
    public static final int HOME_BUBBLE_CONTENT = R.drawable.home_bubble_center;

    public TipGuidePowindow(Activity activity) {
        this(activity, "");
    }

    public TipGuidePowindow(Activity activity, String content) {
        super(activity);
        this.mActivity = activity;
        initView(content);
    }

    private void initView(String content) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.pop_function_guide, null, false);
        popBackground = view.findViewById(R.id.pop_background);
        popGuideContent = view.findViewById(R.id.pop_guide_content);
        popGuideImage = view.findViewById(R.id.pop_guide_image);
        popGuideContent.setText(content);

        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(null);
        setContentView(view);
    }

    /**
     * 设置背景
     *
     * @param resource
     * @return
     */
    public TipGuidePowindow setBackground(int resource) {
        popBackground.setBackground(ContextCompat.getDrawable(mActivity, resource));
        return this;
    }

    /**
     * 设置文字内容，颜色，大小
     *
     * @return
     */
    public TipGuidePowindow setContent(String content) {
        return setContent(content, 14, Utils.getLocalColor(mActivity, R.color.white));
    }

    public TipGuidePowindow setContent(String content, int size, int color) {
        return setContent(content, size, color, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public TipGuidePowindow setContent(String content, int size, int color, int width) {
        popGuideContent.setText(content);
        popGuideContent.setTextSize(size);
        popGuideContent.setWidth(width);
        popGuideContent.setTextColor(color);
        return this;
    }

    public TipGuidePowindow setImageBackground(int offsetX) {
        return setImageBackground(offsetX, HOME_BUBBLE_CONTENT);
    }

    /**
     * 设置图片的样式和位置
     *
     * @param resource :左右箭头
     * @param offsetX  ：横向偏移量
     * @return
     */
    public TipGuidePowindow setImageBackground(int offsetX, int resource) {
        popGuideImage.setBackground(ContextCompat.getDrawable(mActivity, resource));
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) popGuideImage.getLayoutParams();
        params.setMargins(offsetX, 0, 0, 0);
        popGuideImage.setLayoutParams(params);
        return this;
    }

    /**
     * 相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
     *
     * @param parent
     * @param x
     * @param y
     */
    public void showPopupWindow(View parent, int x, int y) {
        if (isShowing()) {
            dismiss();
        } else {
            showAsDropDown(parent, x, y);
        }
    }
}
