package com.module.commonview.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.nfc.Tag;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;


/**
 * Created by YaphetZhao
 * on 2016/12/12.
 */
public class MultiViewPager extends ViewPager {

    private int mMaxWidth = -1;
    private int mMaxHeight = -1;
    private int mMatchWidthChildResId;
    private boolean mNeedsMeasurePage;
    private final Point size;
    private final Point maxSize;

    private float downX;
    private float downY;
    public static final float MAX_SCALE = 1.2f;
    public static final float MIN_SCALE = 0.7f;
    //默认距离
    private final static float DISTANCE = 10;

    private static void constrainTo(Point size, Point maxSize) {
        if (maxSize.x >= 0) {
            if (size.x > maxSize.x) {
                size.x = maxSize.x;
            }
        }
        if (maxSize.y >= 0) {
            if (size.y > maxSize.y) {
                size.y = maxSize.y;
            }
        }
    }

    public MultiViewPager(Context context) {
        super(context);
        size = new Point();
        maxSize = new Point();
    }

    public MultiViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        size = new Point();
        maxSize = new Point();
    }

    private void init(Context context, AttributeSet attrs) {
        setClipChildren(false);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MultiViewPager);
        setMaxWidth(ta.getDimensionPixelSize(R.styleable.MultiViewPager_android_maxWidth, -1));
        setMaxHeight(ta.getDimensionPixelSize(R.styleable.MultiViewPager_android_maxHeight, -1));
        setMatchChildWidth(ta.getResourceId(R.styleable.MultiViewPager_matchChildWidth, 0));
        ta.recycle();
    }



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        size.set(MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
        if (mMaxWidth >= 0 || mMaxHeight >= 0) {
            maxSize.set(mMaxWidth, mMaxHeight);
            constrainTo(size, maxSize);
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(
                    size.x,
                    MeasureSpec.EXACTLY);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(
                    size.y,
                    MeasureSpec.EXACTLY);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        onMeasurePage(widthMeasureSpec, heightMeasureSpec);
    }

    protected void onMeasurePage(int widthMeasureSpec, int heightMeasureSpec) {
        if (!mNeedsMeasurePage) {
            return;
        }
        if (mMatchWidthChildResId == 0) {
            mNeedsMeasurePage = false;
        } else if (getChildCount() > 0) {
            View child = getChildAt(0);
            child.measure(widthMeasureSpec, heightMeasureSpec);
            int pageWidth = child.getMeasuredWidth();
            View match = child.findViewById(mMatchWidthChildResId);
            if (match == null) {
                throw new NullPointerException(
                        "NullPointerException");
            }
            int childWidth = match.getMeasuredWidth();
            if (childWidth > 0) {
                mNeedsMeasurePage = false;
                int difference = pageWidth - childWidth;
                setPageMargin(-(difference-Utils.dip2px(15)));
                Log.e("MultiViewPager","pageWidth == "+(-(difference-Utils.dip2px(15))));
                int offscreen = (int) Math.ceil((float) pageWidth / (float) childWidth) + 1;
                setOffscreenPageLimit(offscreen);
                requestLayout();
            }
        }
    }



    /**
     * @param ev
     * @return
     */
    private View viewOfClickOnScreen(MotionEvent ev) {
        int childCount = getChildCount();
        Log.e("MultiViewPager","childCount =="+childCount);
        int[] location = new int[2];
        for (int i = 0; i < childCount; i++) {
            Log.e("MultiViewPager","i =="+i);
            View v = getChildAt(i);
            v.getLocationOnScreen(location);


            int minX = location[0];
            int minY = getTop();
            Log.e("MultiViewPager","minX =="+minX+" ,minY =="+minY);
            int maxX = location[0] + v.getWidth();
            int maxY = getBottom();
            Log.e("MultiViewPager","maxX =="+maxX+" ,maxY =="+maxY);
            float x = ev.getX();
            float y = ev.getY();
            Log.e("MultiViewPager","getX =="+x+" ,getY =="+y);
            Log.e("MultiViewPager","getTag =="+v.getTag());
            Log.e("MultiViewPager","===========================================");

            if ((x > minX && x < maxX) && (y > minY && y < maxY) ) {
                Log.e("MultiViewPager","----------------------------------------");
                Log.e("MultiViewPager","| getTag =="+v.getTag());
                Log.e("MultiViewPager","----------------------------------------");
//                return v;
            }
        }
        return null;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mNeedsMeasurePage = true;
    }

    public void setMatchChildWidth(int matchChildWidthResId) {
        if (mMatchWidthChildResId != matchChildWidthResId) {
            mMatchWidthChildResId = matchChildWidthResId;
            mNeedsMeasurePage = true;
        }
    }

    public void setMaxWidth(int width) {
        mMaxWidth = width;
    }

    public void setMaxHeight(int height) {
        mMaxHeight = height;
    }

}