package com.module.commonview.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;

import java.util.Map;

public class SkuFenqiPop extends PopupWindow {
    Context mContext;
    public SkuFenqiPop(Context context,final String url, Map<String, Object> urlMap) {
        mContext=context;
        final View view = View.inflate(mContext, R.layout.fenqi_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setContentView(view);
        update();
        WebView webView= view.findViewById(R.id.fenqi_webview);
        Button btn= view.findViewById(R.id.fenqi_btn);
        LinearLayout fenQiHead= view.findViewById(R.id.fenqi_head);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, urlMap);
        webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        fenQiHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
