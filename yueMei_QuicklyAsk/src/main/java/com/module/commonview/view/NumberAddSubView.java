package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;


/**
 * 自定义加减控件
 * Created by 裴成浩 on 2018/11/22.
 */
public class NumberAddSubView extends LinearLayout implements View.OnClickListener {
    private String TAG = "NumberAddSubView";
    private Button btn_sub;
    private TextView tv_num;
    private Button btn_add;
    private Context mContext;

    /**
     * 设置默认值
     */
    private int value = 1;                  //当前要购买的数量
    private int minValue = 1;               //最小限购数
    private int maxValue = 10;              //最大限购数

    private NumberViewDialog numberViewDialog;
    private boolean isAddSubText = true;

    public NumberAddSubView(Context context) {
        this(context, null);
    }

    public NumberAddSubView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberAddSubView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
        initattribute(attrs);
    }

    /**
     * 设置布局
     */
    private void initView() {
        //第三个参数：把当前 View 加载到 NumberAddSubView 控件上
        View.inflate(mContext, R.layout.number_add_sub_view, this);
        btn_sub = findViewById(R.id.jian_num_bt);
        tv_num = findViewById(R.id.shuliang_num_tv);
        btn_add = findViewById(R.id.jia_num_bt);

        btn_sub.setOnClickListener(this);
        tv_num.setOnClickListener(this);
        btn_add.setOnClickListener(this);

        numberViewDialog = new NumberViewDialog(mContext, value, minValue, maxValue);

        numberViewDialog.setBtnClickListener(new NumberViewDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                numberViewDialog.dismiss();
            }

            @Override
            public void rightBtnClick(int num) {
                if (num > maxValue) {
                    if (onButtonClickListenter != null) {
                        onButtonClickListenter.onButtonAddClick(num, false);
                    }
                } else if (num < minValue) {
                    if (onButtonClickListenter != null) {
                        onButtonClickListenter.onButtonSubClick(num, false);
                    }
                } else {
                    setValue(num);
                    if (onButtonClickListenter != null) {
                        onButtonClickListenter.onTextViewClick(value);
                    }
                }
                numberViewDialog.dismiss();
            }
        });
    }

    /**
     * 自定义属性
     *
     * @param attrs
     */
    private void initattribute(AttributeSet attrs) {
        //得到属性
        if (attrs != null) {
            TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.NumberAddSubView);
            int maxValue = a.getInt(R.styleable.NumberAddSubView_maxValue, 0);
            int minValue = a.getInt(R.styleable.NumberAddSubView_minValue, 0);

            String subText = a.getString(R.styleable.NumberAddSubView_sub_text);
            value = a.getInt(R.styleable.NumberAddSubView_value_text, 0);
            String addText = a.getString(R.styleable.NumberAddSubView_add_text);

            int btnSubBackground = a.getResourceId(R.styleable.NumberAddSubView_btnSubBackground, R.color.white);
            int btnAddBackground = a.getResourceId(R.styleable.NumberAddSubView_btnAddBackground, R.color.white);
            int textViewBackground = a.getResourceId(R.styleable.NumberAddSubView_textViewBackground, R.color.white);

            int subTextColor = a.getResourceId(R.styleable.NumberAddSubView_sub_text_color, R.color._99);
            int valueTextColor = a.getResourceId(R.styleable.NumberAddSubView_value_text_color, R.color._33);
            int addTextColor = a.getResourceId(R.styleable.NumberAddSubView_add_text_color, R.color._99);

            int subTextSize = a.getInt(R.styleable.NumberAddSubView_sub_text_size, 13);
            int valueTextSize = a.getInt(R.styleable.NumberAddSubView_value_text_size, 12);
            int addTextSize = a.getInt(R.styleable.NumberAddSubView_add_text_size, 13);

            isAddSubText = a.getBoolean(R.styleable.NumberAddSubView_is_add_sub_text, true);

            //最小值、最大值
            setMaxValue(maxValue);
            setMinValue(minValue);

            //减、默认值、加 的值
            btn_sub.setText(TextUtils.isEmpty(subText) ? "-" : subText);
            setValue(value);
            btn_add.setText(TextUtils.isEmpty(addText) ? "+" : addText);

            //设置背景颜色
            btn_sub.setBackgroundResource(btnSubBackground);
            btn_sub.setBackgroundResource(btnAddBackground);
            tv_num.setBackgroundResource(textViewBackground);

            //设置文字颜色
            btn_sub.setTextColor(Utils.getLocalColor(mContext, subTextColor));
            tv_num.setTextColor(Utils.getLocalColor(mContext, valueTextColor));
            btn_add.setTextColor(Utils.getLocalColor(mContext, addTextColor));

            //设置文字大小
            btn_sub.setTextSize(TypedValue.COMPLEX_UNIT_SP, subTextSize);
            tv_num.setTextSize(TypedValue.COMPLEX_UNIT_SP, valueTextSize);
            btn_add.setTextSize(TypedValue.COMPLEX_UNIT_SP, addTextSize);

            a.recycle();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.jian_num_bt:
                if (onButtonClickListenter != null) {
                    if (value != minValue) {
                        subNum();
                        onButtonClickListenter.onButtonSubClick(value, true);
                    } else {
                        onButtonClickListenter.onButtonSubClick(value, false);
                    }
                }
                break;
            case R.id.shuliang_num_tv:
                numberViewDialog.show();
                break;
            case R.id.jia_num_bt:
                if (onButtonClickListenter != null) {
                    if (value != maxValue) {
                        addNum();
                        onButtonClickListenter.onButtonAddClick(value, true);
                    } else {
                        onButtonClickListenter.onButtonAddClick(value, false);
                    }
                }
                break;
        }
    }

    /**
     * 获取个数
     *
     * @return
     */
    public int getValue() {
        String val = tv_num.getText().toString();
        if (!TextUtils.isEmpty(val)) {
            value = Integer.parseInt(val);
        }
        return value;
    }

    /**
     * 设置个数
     *
     * @param value
     */
    public void setValue(int value) {
        this.value = value;
        tv_num.setText(value + "");
    }

    /**
     * 获取最小个数
     *
     * @return
     */
    public int getMinValue() {
        return minValue;
    }

    /**
     * 设置最小个数
     *
     * @param minValue
     */
    public void setMinValue(int minValue) {
        this.minValue = minValue;


    }

    /**
     * 获取最大个数
     *
     * @return
     */
    public int getMaxValue() {
        return maxValue;
    }

    /**
     * 设置最大个数
     *
     * @param maxValue
     */
    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * 减少数据
     */
    @SuppressLint("SetTextI18n")
    private void subNum() {
        if (value > minValue) {
            value = value - 1;
            if (isAddSubText) {
                tv_num.setText((value < minValue ? minValue : value) + "");
            }
        }
    }

    /**
     * 添加数据
     */
    @SuppressLint("SetTextI18n")
    private void addNum() {
        if (value < maxValue) {
            value = value + 1;
            if (isAddSubText) {
                tv_num.setText((value > maxValue ? maxValue : value) + "");
            }
        }
    }

    public interface OnButtonClickListenter {
        /**
         * 当增加按钮被点击的时候回调该方法
         * *
         *
         * @param value
         */
        void onButtonAddClick(int value, boolean b);

        /**
         * 数字点击按钮
         *
         * @param value
         */
        void onTextViewClick(int value);

        /**
         * 当减少按钮被点击的时候回调这个方法
         * *
         *
         * @param value
         */
        void onButtonSubClick(int value, boolean b);

    }

    private OnButtonClickListenter onButtonClickListenter;

    public void setOnButtonClickListenter(OnButtonClickListenter
                                                  onButtonClickListenter) {
        this.onButtonClickListenter = onButtonClickListenter;
    }
}