package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.amap.api.maps2d.model.LatLng;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyToast;

import java.util.List;

/**
 * 医院位置页面底部导航跳转
 * Created by 裴成浩 on 2019/6/4
 */
public class MapButtomDialogView extends Dialog {


    private Context mContext;
    private String TAG = "MapButtomDialogView";
    private double mLon;                  //经度
    private double mLat;                  //纬度
    private String mAddressStr;


    public MapButtomDialogView(Context context) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //设置布局
        View view = LayoutInflater.from(mContext).inflate(R.layout.map_hospital_buttom_view, null, false);
        setContentView(view);

        setCancelable(false);                    //点击外部不可dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);

        TextView mapBaidu = view.findViewById(R.id.map_hospital_buttom_baidu);
        TextView mapGaode = view.findViewById(R.id.map_hospital_buttom_gaode);
        TextView mapTencent = view.findViewById(R.id.map_hospital_buttom_tencent);
        TextView mapCancel = view.findViewById(R.id.map_hospital_buttom_cancel);

        /**
         * 百度地图
         */
        mapBaidu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToBaiduMap();
                dismiss();
            }
        });

        /**
         * 高德地图
         */
        mapGaode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGaodeMap();
                dismiss();
            }
        });

        /**
         * 腾讯地图
         */
        mapTencent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTencentMap();
                dismiss();
            }
        });


        mapCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    /**
     * 跳转百度地图
     */
    private void goToBaiduMap() {
        if (!isInstalled("com.baidu.BaiduMap")) {
            MyToast.makeTextToast2(mContext, "请先安装百度地图客户端", MyToast.SHOW_TIME).show();
            return;
        }
        Intent intent = new Intent();
        intent.setData(Uri.parse("baidumap://map/direction?destination=latlng:"
                + mLat + ","
                + mLon + "|name:" + mAddressStr + // 终点
                "&mode=driving" + // 导航路线方式
                "&src=" + mContext.getPackageName()));
        mContext.startActivity(intent); // 启动调用
    }


    /**
     * 跳转高德地图
     */
    private void goToGaodeMap() {
        if (!isInstalled("com.autonavi.minimap")) {
            MyToast.makeTextToast2(mContext, "请先安装高德地图客户端", MyToast.SHOW_TIME).show();
            return;
        }
        LatLng endPoint = BD2GCJ(new LatLng(mLat, mLon));//坐标转换
        StringBuffer stringBuffer = new StringBuffer("androidamap://navi?sourceApplication=").append("amap");
        stringBuffer.append("&lat=").append(endPoint.latitude)
                .append("&lon=").append(endPoint.longitude).append("&keywords=" + mAddressStr)
                .append("&dev=").append(0)
                .append("&style=").append(2);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(stringBuffer.toString()));
        intent.setPackage("com.autonavi.minimap");
        mContext.startActivity(intent);
    }

    /**
     * 跳转腾讯地图
     */
    private void goToTencentMap() {
        if (!isInstalled("com.tencent.map")) {
            MyToast.makeTextToast2(mContext, "请先安装腾讯地图客户端", MyToast.SHOW_TIME).show();
            return;
        }
        LatLng endPoint = BD2GCJ(new LatLng(mLat, mLon));//坐标转换
        StringBuffer stringBuffer = new StringBuffer("qqmap://map/routeplan?type=drive")
                .append("&tocoord=").append(endPoint.latitude).append(",").append(endPoint.longitude).append("&to=" + mAddressStr);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(stringBuffer.toString()));
        mContext.startActivity(intent);
    }


    /**
     * 检测程序是否安装
     *
     * @param packageName
     * @return
     */
    private boolean isInstalled(String packageName) {
        PackageManager manager = mContext.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> installedPackages = manager.getInstalledPackages(0);
        if (installedPackages != null) {
            for (PackageInfo info : installedPackages) {
                if (info.packageName.equals(packageName))
                    return true;
            }
        }
        return false;
    }

    /**
     * BD-09 坐标转换成 GCJ-02 坐标
     */
    public static LatLng BD2GCJ(LatLng bd) {
        double x = bd.longitude - 0.0065, y = bd.latitude - 0.006;
        double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * Math.PI);
        double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * Math.PI);

        double lng = z * Math.cos(theta);//lng
        double lat = z * Math.sin(theta);//lat
        return new LatLng(lat, lng);
    }

    /**
     * GCJ-02 坐标转换成 BD-09 坐标
     */
    public static LatLng GCJ2BD(LatLng bd) {
        double x = bd.longitude, y = bd.latitude;
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * Math.PI);
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * Math.PI);
        double tempLon = z * Math.cos(theta) + 0.0065;
        double tempLat = z * Math.sin(theta) + 0.006;
        return new LatLng(tempLat, tempLon);
    }

    /**
     * 显示地图
     *
     * @param addressStr : 医院地址
     * @param lon        ：经度
     * @param lat        ：纬度
     */
    public void showView(String addressStr, String lon, String lat) {
        mLon = Double.parseDouble(lon);
        mLat = Double.parseDouble(lat);
        mAddressStr = addressStr;
        show();
    }
}


