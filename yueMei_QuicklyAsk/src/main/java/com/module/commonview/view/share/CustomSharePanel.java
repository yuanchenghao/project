package com.module.commonview.view.share;

import android.app.Activity;
import android.support.v7.content.res.AppCompatResources;
import android.view.View;

import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheetItemView;
import com.quicklyask.activity.R;
import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * 自定义分享控制面板(包括位置)
 * Created by 裴成浩 on 2018/5/4.
 */
public class CustomSharePanel extends QMUIBottomSheet.BottomGridSheetBuilder {

    public final int TAG_SHARE_ILLUSTRATED = 0;         //画报
    public final int TAG_SHARE_SINA = 1;                //新浪
    public final int TAG_SHARE_QQ = 2;                  //QQ好友
    public final int TAG_SHARE_QZONE= 3;                //QQ空间
    public final int TAG_SHARE_WEIXIN_CIRCLE = 4;       //朋友圈
    public final int TAG_SHARE_WEIXIN = 5;              //微信好友
    public final int TAG_SHARE_SMS = 6;              //短信
    private final Activity mActivity;

    private MyShareBoardlistener myShareBoardlistener;
    private QMUIBottomSheet qmuiBottomSheet;
    private QMUIBottomSheetItemView huabaoView;

    public CustomSharePanel(Activity activity, MyShareBoardlistener myShareBoardlistener) {
        super(activity);
        this.mActivity = activity;
        this.myShareBoardlistener = myShareBoardlistener;
    }

    /**
     * 初始化控制面板
     */
    public void startShow(){
        setButtonText("取消");
        huabaoView = createItemView(AppCompatResources.getDrawable(mActivity, R.drawable.umeng_socialize_huabao), "画报", TAG_SHARE_ILLUSTRATED, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
        addItem(huabaoView,QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
        addItem(R.drawable.umeng_socialize_sina, "新浪", TAG_SHARE_SINA, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
        addItem(R.drawable.umeng_socialize_qq, "QQ", TAG_SHARE_QQ, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
        addItem(R.drawable.umeng_socialize_qzone, "QQ空间", TAG_SHARE_QZONE, QMUIBottomSheet.BottomGridSheetBuilder.FIRST_LINE);
        addItem(R.drawable.umeng_socialize_wechat, "微信", TAG_SHARE_WEIXIN_CIRCLE, QMUIBottomSheet.BottomGridSheetBuilder.SECOND_LINE);
        addItem(R.drawable.umeng_socialize_wxcircle, "微信朋友圈", TAG_SHARE_WEIXIN, QMUIBottomSheet.BottomGridSheetBuilder.SECOND_LINE);
        addItem(R.drawable.umeng_socialize_sms, "短信", TAG_SHARE_SMS, QMUIBottomSheet.BottomGridSheetBuilder.SECOND_LINE);

        setOnSheetItemClickListener(new OnSheetItemClickListener() {
            @Override
            public void onClick(QMUIBottomSheet dialog, View itemView) {
                int tag = (int) itemView.getTag();
                switch (tag) {
                    case TAG_SHARE_ILLUSTRATED:
//                        myShareBoardlistener.illustratedShare();
                        break;
                    case TAG_SHARE_SINA:
                        myShareBoardlistener.sinaShare(SHARE_MEDIA.SINA);
                        break;
                    case TAG_SHARE_QQ:
                        myShareBoardlistener.tencentShare(SHARE_MEDIA.QQ,MyShareBoardlistener.QQ_FLAG);
                        break;
                    case TAG_SHARE_QZONE:
                        myShareBoardlistener.tencentShare(SHARE_MEDIA.QZONE,MyShareBoardlistener.QQ_FLAG);
                        break;
                    case TAG_SHARE_WEIXIN_CIRCLE:
                        myShareBoardlistener.tencentShare(SHARE_MEDIA.WEIXIN_CIRCLE,MyShareBoardlistener.WEI_XIN_FLAG);
                        break;
                    case TAG_SHARE_WEIXIN:
                        myShareBoardlistener.weixinShare(SHARE_MEDIA.WEIXIN);
                        break;
                    case TAG_SHARE_SMS:
                        myShareBoardlistener.smsShare(SHARE_MEDIA.SMS);
                        break;
                }
                dialog.dismiss();
            }
        });
        qmuiBottomSheet = build();
        qmuiBottomSheet.show();
    }

    public QMUIBottomSheet getQmuiBottomSheet() {
        return qmuiBottomSheet;
    }

    public QMUIBottomSheetItemView getHuabaoView() {
        return huabaoView;
    }
}
