package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.commonview.module.bean.DiaryRadioBean;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import org.xutils.common.util.DensityUtil;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/6.
 */
public class DiaryFlowLayout {

    private final Context mContext;
    private FlowLayout mFlowLayout;
    private List<DiaryRadioBean> mDatas;
    private String TAG = "DiaryFlowLayout";

    public DiaryFlowLayout(Context context, FlowLayout flowlayout, List<DiaryRadioBean> data) {
        this.mContext = context;
        this.mFlowLayout = flowlayout;
        this.mDatas = data;
        setDiaryLabelView();
    }

    /**
     * 设置相关日记列表标签
     */
    @SuppressLint("SetTextI18n")
    private void setDiaryLabelView() {
        mFlowLayout.removeAllViews();

        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = DensityUtil.dip2px(10);
        layoutParams.bottomMargin = DensityUtil.dip2px(10);
        for (int i = 0; i < mDatas.size(); i++) {

            View view = LayoutInflater.from(mContext).inflate(R.layout.item_diary_flow_label, null);
            TextView title = view.findViewById(R.id.item_diary_flow_label_title);
            TextView num = view.findViewById(R.id.item_diary_flow_label_num);

            title.setText(mDatas.get(i).getRadio_name());
            num.setText(mDatas.get(i).getShare_num());

            if (i == 0) {                       //第一个
                layoutParams.rightMargin = DensityUtil.dip2px(10);
                title.setTextColor(Utils.getLocalColor(mContext, R.color.white));
                num.setTextColor(Utils.getLocalColor(mContext, R.color.white));
                view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_rounded_corners_ff6fb7_ffbeae));       //默认选中状态
                mDatas.get(i).setSelected(true);
            } else if (i == mDatas.size() - 1) {       //最后一个
                layoutParams.leftMargin = DensityUtil.dip2px(10);
                title.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                num.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_tuoyuan_fdf1f3));
                mDatas.get(i).setSelected(false);
            } else {
                layoutParams.leftMargin = DensityUtil.dip2px(10);
                layoutParams.rightMargin = DensityUtil.dip2px(10);
                title.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                num.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_tuoyuan_fdf1f3));
                mDatas.get(i).setSelected(false);
            }

            mFlowLayout.addView(view, layoutParams);
            view.setTag(i);
            view.setOnClickListener(new onClickView());
        }
    }

    /**
     * 设置点击事件
     */
    private class onClickView implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            int selected = (int) view.getTag();
            for (int i = 0; i < mDatas.size(); i++) {
                View childAt = mFlowLayout.getChildAt(i);
                TextView title = childAt.findViewById(R.id.item_diary_flow_label_title);
                TextView num = childAt.findViewById(R.id.item_diary_flow_label_num);
                if (i == selected) {                        //点击的改为选中状态
                    mDatas.get(i).setSelected(true);
                    childAt.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_rounded_corners_ff6fb7_ffbeae));
                    title.setTextColor(Utils.getLocalColor(mContext, R.color.white));
                    num.setTextColor(Utils.getLocalColor(mContext, R.color.white));

                    if (diaryFlowCallBackListener != null) {
                        diaryFlowCallBackListener.onSelectedClickView(i);
                    }

                } else {                                    //未点击的改为未选中状态
                    mDatas.get(i).setSelected(false);
                    childAt.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_bian_tuoyuan_fdf1f3));
                    title.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                    num.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                    if (diaryFlowCallBackListener != null) {
                        diaryFlowCallBackListener.onUncheckClickView(i);
                    }
                }
            }
        }
    }

    /**
     * 获取选中的id
     *
     * @return
     */
    public String getSelected() {
        for (DiaryRadioBean data : mDatas) {
            if (data.isSelected()) {
                return data.getRadio_id();
            }
        }
        return "0";
    }

    /**
     * 删除数据接口
     *
     * @param isVideo ：删除的是否是带视频的
     */
    public void deleteData(String isVideo) {
        if ("1".equals(isVideo)) {
            //删除全部中的一条
            for (int i = 0; i < mDatas.size(); i++) {
                DiaryRadioBean data = mDatas.get(i);
                if ("全部".equals(data.getRadio_name()) || "视频".equals(data.getRadio_name())) {
                    int num = Integer.parseInt(data.getShare_num()) - 1;
                    mDatas.get(i).setShare_num((num < 0 ? 0 : num) + "");
                }
            }
        } else {
            //删除全部中的一条
            for (int i = 0; i < mDatas.size(); i++) {
                DiaryRadioBean data = mDatas.get(i);
                if ("全部".equals(data.getRadio_name())) {
                    int num = Integer.parseInt(data.getShare_num()) - 1;
                    mDatas.get(i).setShare_num((num < 0 ? 0 : num) + "");
                }
            }
        }

        setDiaryLabelView();
    }

    /**
     * 添加一条数据
     *
     * @param isVideo ： 添加的是否是带视频的
     */
    public void addData(String isVideo) {
        if ("1".equals(isVideo)) {
            for (int i = 0; i < mDatas.size(); i++) {
                DiaryRadioBean data = mDatas.get(i);
                if ("全部".equals(data.getRadio_name()) || "视频".equals(data.getRadio_name())) {
                    int num = Integer.parseInt(data.getShare_num()) + 1;
                    mDatas.get(i).setShare_num(num + "");
                }
            }
        } else {
            for (int i = 0; i < mDatas.size(); i++) {
                DiaryRadioBean data = mDatas.get(i);
                if ("全部".equals(data.getRadio_name())) {
                    int num = Integer.parseInt(data.getShare_num()) + 1;
                    mDatas.get(i).setShare_num(num + "");
                }
            }
        }

        setDiaryLabelView();
    }

    /**
     * 获取数据
     *
     * @return
     */
    public List<DiaryRadioBean> getDatas() {
        return mDatas;
    }

    private DiaryFlowCallBackListener diaryFlowCallBackListener;

    public interface DiaryFlowCallBackListener {
        void onSelectedClickView(int pos);

        void onUncheckClickView(int pos);
    }

    public void setOnDiaryFlowCallBackListener(DiaryFlowCallBackListener diaryFlowCallBackListener) {
        this.diaryFlowCallBackListener = diaryFlowCallBackListener;
    }
}
