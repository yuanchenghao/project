package com.module.commonview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 筛选的View
 * Created by 裴成浩 on 2019/4/29
 */
public class ScreenTitleView extends LinearLayout {
    private final Context mContext;
    LinearLayout mTitleProjectClick;
    TextView mTitleProject;
    ImageView mImageProject;
    View mViewLine1;
    LinearLayout mTitleCityClick;
    TextView mTitleCity;
    ImageView mImageCity;
    LinearLayout mTitleSortClick;
    TextView mTitleSort;
    ImageView mImageSort;
    View mViewLine4;
    LinearLayout mTitleKindClick;
    TextView mTitleKind;
    ImageView mImageKind;
    ImageView mKindSelected;

    public ScreenTitleView(Context context) {
        this(context, null);
    }

    public ScreenTitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScreenTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    /**
     * 判断是否有筛选这一项
     *
     * @param isHaveScreen true:有，flase:无
     */
    public void initView(boolean isHaveScreen) {
        initView(isHaveScreen,false);
    }

    /**
     * 判断是否有全部项目和筛选项
     * @param isHaveScreen
     * @param isHaveProject
     */
    public void initView(boolean isHaveScreen, boolean isHaveProject) {

        setOrientation(VERTICAL);

        View view = View.inflate(mContext, R.layout.screen_title_view, this);

        mTitleProjectClick = view.findViewById(R.id.screen_title_project_click);
        mTitleProject = view.findViewById(R.id.screen_title_project);
        mImageProject = view.findViewById(R.id.screen_image_project);
        mViewLine1 = view.findViewById(R.id.screen_view_line1);
        mTitleCityClick = view.findViewById(R.id.screen_title_city_click);
        mTitleCity = view.findViewById(R.id.screen_title_city);
        mImageCity = view.findViewById(R.id.screen_image_city);
        mTitleSortClick = view.findViewById(R.id.screen_title_sort_click);
        mTitleSort = view.findViewById(R.id.screen_title_sort);
        mImageSort = view.findViewById(R.id.screen_image_sort);
        mViewLine4 = view.findViewById(R.id.screen_view_line4);                   //排序和筛选的分割线
        mTitleKindClick = view.findViewById(R.id.screen_title_kind_click);
        mTitleKind = view.findViewById(R.id.screen_title_kind);
        mImageKind = view.findViewById(R.id.screen_image_kind);
        mKindSelected = view.findViewById(R.id.screen_image_kind_selected);


        //项目是否隐藏
        if (isHaveProject) {
            mViewLine1.setVisibility(VISIBLE);
            mTitleProjectClick.setVisibility(VISIBLE);
        } else {
            mViewLine1.setVisibility(GONE);
            mTitleProjectClick.setVisibility(GONE);
        }

        //筛选项是否隐藏
        if (isHaveScreen) {
            mViewLine4.setVisibility(VISIBLE);
            mTitleKindClick.setVisibility(VISIBLE);
        } else {
            mViewLine4.setVisibility(GONE);
            mTitleKindClick.setVisibility(GONE);
        }
        View line = new View(mContext);
        line.setBackgroundColor(Utils.getLocalColor(mContext, R.color._e5));
        line.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(0.5f)));
        addView(line);

        //判断项目筛选是否存在
        mTitleProjectClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener2 != null) {
                    onEventClickListener2.onProjectClick();
                }
            }
        });

        //城市点击
        mTitleCityClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCityClick();
                }
                if (onEventClickListener1 != null) {
                    onEventClickListener1.onCityClick();
                }
                if (onEventClickListener2 != null) {
                    onEventClickListener2.onCityClick();
                }
            }
        });

        //排序点击
        mTitleSortClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onSortClick();
                }

                if (onEventClickListener1 != null) {
                    onEventClickListener1.onSortClick();
                }
                if (onEventClickListener2 != null) {
                    onEventClickListener2.onSortClick();
                }
            }
        });

        //筛选点击
        mTitleKindClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onKindClick();
                }

                if (onEventClickListener2 != null) {
                    onEventClickListener2.onKindClick();
                }
            }
        });
    }

    /**
     * 项目的展开和收缩
     *
     * @param isShow
     */
    public void initProjectView(boolean isShow) {
        if (isShow) {
            mTitleProject.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageProject.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleProject.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageProject.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    /**
     * 城市的展开和收缩
     *
     * @param isShow
     */
    public void initCityView(boolean isShow) {
        if (isShow) {
            mTitleCity.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageCity.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleCity.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageCity.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    /**
     * 排序的展开和收缩
     *
     * @param isShow
     */
    public void initSortView(boolean isShow) {
        if (isShow) {
            mTitleSort.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageSort.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleSort.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageSort.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    /**
     * 筛选的展开和收缩
     *
     * @param isShow     :
     * @param isSelected ：是否有选中项
     */
    public void initKindView(boolean isShow, boolean isSelected) {
        if (isShow) {
            mTitleKind.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageKind.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleKind.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageKind.setBackgroundResource(R.drawable.gra_tao_tab);
        }

        if (isSelected) {
            mKindSelected.setVisibility(VISIBLE);
        } else {
            mKindSelected.setVisibility(GONE);
        }

    }

    /**
     * 设置城市名称
     *
     * @param title
     */
    public void setProjectTitle(String title) {
        mTitleProject.setText(title);
    }

    /**
     * 设置城市名称
     *
     * @param title
     */
    public void setCityTitle(String title) {
        mTitleCity.setText(title);
    }

    /**
     * 设置排序的名称
     *
     * @param title
     */
    public void setSortTitle(String title) {
        mTitleSort.setText(title);
    }

    /**
     * 设置筛选名称
     *
     * @param title
     */
    public void setKindTitle(String title) {
        mTitleKind.setText(title);
    }

    //三个的回调
    public interface OnEventClickListener {
        void onCityClick();                 //城市回调

        void onSortClick();                 //排序回调

        void onKindClick();                 //筛选回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }


    //两个的回调
    public interface OnEventClickListener1 {
        void onCityClick();                 //城市回调

        void onSortClick();                 //排序回调
    }

    private OnEventClickListener1 onEventClickListener1;

    public void setOnEventClickListener(OnEventClickListener1 onEventClickListener) {
        this.onEventClickListener1 = onEventClickListener;
    }

    //四个的回调
    public interface OnEventClickListener2 {
        void onProjectClick();                 //项目回调

        void onCityClick();                 //城市回调

        void onSortClick();                 //排序回调

        void onKindClick();                 //筛选回调
    }

    private OnEventClickListener2 onEventClickListener2;

    public void setOnEventClickListener(OnEventClickListener2 onEventClickListener2) {
        this.onEventClickListener2 = onEventClickListener2;
    }

}
