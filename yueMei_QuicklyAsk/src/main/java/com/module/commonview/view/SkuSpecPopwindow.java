package com.module.commonview.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.CouponPriceInfo;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.util.Cfg;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import org.apache.commons.collections.CollectionUtils;

import java.util.HashMap;
import java.util.List;

public class SkuSpecPopwindow extends PopupWindow {

    int pos=0;
    private String tao_id;
    private HashMap<String, String> event_params;
    private final ImageView mSkuSpecImg;
    private final TextView mSkuSpecTitle;
    private final TextView mSkuSpecOrding;
    private final TextView mSkuSpecPrice;
    private final TextView mSkuSpecSpec;
    private final TextView mSkuSpecSpecDesc;
    private final RelativeLayout mSkuSpecActVisorgone;
    private final TextView mSkuSpecActType1;
    private final TextView mSkuSpecActPrice;
    private final TextView mSkuSpecSpec2;

    public SkuSpecPopwindow(final Context context, final TaoDetailBean.RelTaoBeanX rel_tao){
        final View view = View.inflate(context, R.layout.sku_spec_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        setHeight((int) (windowH * 0.7));
        setFocusable(true);
        setContentView(view);
        update();


        mSkuSpecImg = view.findViewById(R.id.sku_spec_img);
        mSkuSpecTitle = view.findViewById(R.id.sku_spec_title);
        mSkuSpecOrding = view.findViewById(R.id.sku_spec_ording);
        mSkuSpecPrice = view.findViewById(R.id.sku_spec_price);
        mSkuSpecSpec = view.findViewById(R.id.sku_spec_spec);
        mSkuSpecSpecDesc = view.findViewById(R.id.sku_spec_price_desc);
        mSkuSpecActVisorgone = view.findViewById(R.id.sku_spec_act_visorgone);
        mSkuSpecActType1 = view.findViewById(R.id.sku_spec_act_type1);
        mSkuSpecActPrice = view.findViewById(R.id.sku_spec_act_price);
        mSkuSpecSpec2 = view.findViewById(R.id.sku_spec_spec2);
        final TagFlowLayout skuSpecTag=view.findViewById(R.id.sku_spec_tag);
        LinearLayout skuSpecClose=view.findViewById(R.id.sku_spec_close);
        Button skuSpecBtn=view.findViewById(R.id.sku_spec_btn);
        TextView skuSpec=view.findViewById(R.id.sku_spec);

        skuSpec.setText(rel_tao.getRel_title());

        final List<TaoDetailBean.RelTaoBeanX.RelTaoBean> rel_tao1 = rel_tao.getRel_tao();
        TagAdapter<TaoDetailBean.RelTaoBeanX.RelTaoBean> tagAdapter = new TagAdapter<TaoDetailBean.RelTaoBeanX.RelTaoBean>(rel_tao1) {
            @Override
            public View getView(FlowLayout parent, int position, TaoDetailBean.RelTaoBeanX.RelTaoBean item) {
                Log.e("skuspecpop","getView");
                View view = LayoutInflater.from(context).inflate(R.layout.sku_tagview, skuSpecTag, false);
                TextView tagView = view.findViewById(R.id.sku_tag_view);
                String checked = item.getChecked();
                String status = item.getStatus();
                String spe_name = item.getSpe_name();

                if ("1".equals(checked)) {
                    setData(context,rel_tao,position);
                    tagView.setBackground(ContextCompat.getDrawable(context, R.drawable.sku_tagview_select));
                    view.setClickable(false);
                    tagView.setClickable(false);
                } else {
                    if ("4".equals(status)) {
                        tagView.setTextColor(ContextCompat.getColor(context, R.color._3));
                    } else {
                        tagView.setTextColor(ContextCompat.getColor(context, R.color._cc));
                        view.setClickable(false);
                        tagView.setClickable(false);
                    }
                    tagView.setBackground(ContextCompat.getDrawable(context, R.drawable.sku_tagview_unselect));
                }
                tagView.setText(spe_name);
                return view;
            }

            @Override
            public void onSelected(int position, View view) {
                    TextView tagView = view.findViewById(R.id.sku_tag_view);
                    tagView.setBackground(ContextCompat.getDrawable(context, R.drawable.sku_tagview_select));
                    tagView.setTextColor(ContextCompat.getColor(context, R.color.red_ff4965));

            }

            @Override
            public void unSelected(int position, View view) {
                    TextView tagView = view.findViewById(R.id.sku_tag_view);
                    tagView.setTextColor(ContextCompat.getColor(context, R.color._3));
                    tagView.setBackground(ContextCompat.getDrawable(context, R.drawable.sku_tagview_unselect));
            }
        };
        skuSpecTag.setAdapter(tagAdapter);
        for (int i = 0; i <rel_tao1.size() ; i++) {
            String checked = rel_tao1.get(i).getChecked();
            if ("1".equals(checked)){
                pos=i;
            }
        }
        tagAdapter.setSelectedList(pos);

        skuSpecTag.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {

            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                Log.e("skuspecpop","onTagClick");
                setData(context,rel_tao,position);
                tao_id = rel_tao1.get(position).getTao_id();
                event_params = rel_tao1.get(position).getEvent_params();
                return true;
            }
        });

        skuSpecClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        skuSpecBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                event_params.put("id",tao_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAOREL_TO_TAO,"0"),event_params,new ActivityTypeData("2"));
                onTagClickListener.onTagClickListener(tao_id);

            }
        });
    }


    OnTagClickListener onTagClickListener;

    public void setOnTagClickListener(OnTagClickListener onTagClickListener) {
        this.onTagClickListener = onTagClickListener;
    }

    public interface OnTagClickListener{
      void   onTagClickListener(String tao_id);
    }


    private void setData(Context context,TaoDetailBean.RelTaoBeanX rel_tao,int pos){
        Log.e("skuspecpop","setData======"+pos);
        List<TaoDetailBean.RelTaoBeanX.RelTaoBean> relTao = rel_tao.getRel_tao();
        if (CollectionUtils.isNotEmpty(relTao)){
            TaoDetailBean.RelTaoBeanX.RelTaoBean relTaoBean = relTao.get(pos);
            if (relTaoBean != null){
                TaoDetailBean.BasedataBean basedata = relTaoBean.getBasedata();
                TaoDetailBean.GroupInfoBean groupInfo = relTaoBean.getGroupInfo();
                CouponPriceInfo couponPriceInfo = relTaoBean.getCouponPriceInfo();//券后价数据
                if (groupInfo == null){
                    return;
                }
                String is_group = groupInfo.getIs_group(); //是否是拼团
                int coupon_type = couponPriceInfo.getCoupon_type();//	>0 显示券后价
                String order_num = basedata.getOrder_num();

                String img = relTaoBean.getImg();
                String title = basedata.getTitle();
                Glide.with(context).load(img).into(mSkuSpecImg);
                mSkuSpecTitle.setText(title);

                if ("1".equals(is_group)){//拼团
                    mSkuSpecOrding.setVisibility(View.GONE);

                    mSkuSpecActVisorgone.setVisibility(View.VISIBLE);

                    mSkuSpecActType1.setText("拼团价");
                    mSkuSpecActPrice.setText("¥"+groupInfo.getGroup_price());
                    mSkuSpecSpec2.setText(order_num);
                }else {
                    if (!TextUtils.isEmpty(order_num)){
                        mSkuSpecOrding.setVisibility(View.VISIBLE);
                        mSkuSpecOrding.setText(order_num);
                    }else {
                        mSkuSpecOrding.setVisibility(View.GONE);
                    }

                    mSkuSpecActVisorgone.setVisibility(View.GONE);
                }



                if (coupon_type > 0){
                    mSkuSpecSpecDesc.setVisibility(View.VISIBLE);
                    mSkuSpecPrice.setText(couponPriceInfo.getCouponPrice());
                    Log.e("skuspecpop","couponPriceInfo======"+couponPriceInfo.getCouponPrice());
                }else {
                    mSkuSpecSpecDesc.setVisibility(View.GONE);
                    mSkuSpecPrice.setText(basedata.getPrice_discount());
                    Log.e("skuspecpop","price======"+basedata.getPrice_discount());
                }
                mSkuSpecSpec.setText(basedata.getFeeScale());
            }
        }

    }

}
