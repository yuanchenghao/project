package com.module.commonview.view;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.module.commonview.adapter.ProjectRightListAdapter;
import com.module.other.adapter.MakeLeftAdapter;
import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.MakeTagListData;
import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 项目选择popupwindows
 * Created by 裴成浩 on 2018/1/26.
 */

public class BaseProjectPopupwindows extends PopupWindow {
    private final String mOneid;
    private String TAG = "BaseProjectPopupwindows";
    private final Activity mActivity;
    private final View mView;
    private List<MakeTagData> mServerData;
    private final int windowsHeight;
    private RecyclerView mLeftRecy;
    private ListView rightList;
    private RelativeLayout otherRly;
    private MakeLeftAdapter leftListAdapter;
    private ProjectRightListAdapter rightListAdapter;
    private int mOnePos = 0;
    private int mTwoPos = -1;
    private int mThreePos = -1;
    private String threeid = "";

    @SuppressWarnings("deprecation")
    public BaseProjectPopupwindows(Activity mActivity, View view, List<MakeTagData> serverData, String oneid) {
        this.mActivity = mActivity;
        this.mView = view;
        this.mOneid = oneid;
        this.mServerData = serverData;

        DisplayMetrics metric = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsHeight = metric.heightPixels;

        initView();

        if (mServerData != null && mServerData.size() > 0) {
            setLeftView();
            if (!"0".equals(mOneid)) {
                setRightView(mOnePos);
            }
        }
    }

    private void initView() {
        final View view = View.inflate(mActivity, R.layout.pop_show_project, null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_ins));

        mLeftRecy = view.findViewById(R.id.pop_project_listview);
        rightList = view.findViewById(R.id.pop_project_listview2);
        otherRly = view.findViewById(R.id.ly_content_ly1);

        otherRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();
    }

    /**
     * 设置左边listView的数据
     */
    public void setLeftView() {
        //设置布局管理器
        mLeftRecy.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        ((DefaultItemAnimator) mLeftRecy.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果

        //设置适配器
        Log.e(TAG, "mPos == " + mOnePos);
        leftListAdapter = new MakeLeftAdapter(mActivity, mServerData, mOnePos, !"0".equals(mOneid), Utils.dip2px(mActivity, 41));
        mLeftRecy.setAdapter(leftListAdapter);

        // 设置点击某条的监听
        leftListAdapter.setOnItemClickListener(new MakeLeftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (mOnePos != pos) {
                    int typePos = mOnePos;         //之前选中的

                    mOnePos = pos;

                    leftListAdapter.setmPos(mOnePos);
                    leftListAdapter.notifyItemChanged(mOnePos);

                    leftListAdapter.notifyItemChanged(typePos);

                    setRightView(pos);
                }
            }
        });
    }

    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }
    }

    /**
     * 设置右边listView的数据
     *
     * @param pos
     */
    public void setRightView(int pos) {

        //设置适配器
        rightListAdapter = new ProjectRightListAdapter(mActivity, mServerData, pos, new MakeTagListListData(threeid));
        rightList.setAdapter(rightListAdapter);
        rightListAdapter.setOnItemSelectedClickListener(new ProjectRightListAdapter.OnItemSelectedClickListener() {

            @Override
            public void onItemSelectedClick(String id, String name) {
                onItemSelectedClickListener.onItemSelectedClick(id, name);
                dismiss();
            }
        });
    }

    public ProjectRightListAdapter getRightListAdapter() {
        return rightListAdapter;
    }

    public void setmServerData(List<MakeTagData> mServerData) {
        this.mServerData = mServerData;
    }

    public void setOneid(String oneid) {
        Log.e(TAG, "oneid == " + oneid);
        if (!TextUtils.isEmpty(oneid)) {
            for (int i = 0; i < mServerData.size(); i++) {
                Log.e(TAG, "mServerData.get(i).getId() == " + mServerData.get(i).getId());
                if (oneid.equals(mServerData.get(i).getId())) {
                    mOnePos = i;
                    break;
                }
            }
        }
    }

    public void setTwoid(String twoid) {
        Log.e(TAG, "mOnePos == " + mOnePos);
        Log.e(TAG, "mServerData == " + mServerData.size());
        List<MakeTagListData> mList = mServerData.get(mOnePos).getList();
        Log.e(TAG, "twoid == " + twoid);
        if (!TextUtils.isEmpty(twoid)) {
            for (int i = 0; i < mList.size(); i++) {
                if (twoid.equals(mList.get(i).getId())) {
                    mTwoPos = i;
                    break;
                }
            }
        }
    }

    public void setThreeid(String threeid) {
        if (mTwoPos >= 0) {
            this.threeid = threeid;
            Log.e(TAG, "threeid == " + threeid);
            List<MakeTagListListData> mList = mServerData.get(mOnePos).getList().get(mTwoPos).getList();
            if (!TextUtils.isEmpty(threeid)) {
                for (int i = 0; i < mList.size(); i++) {
                    if (threeid.equals(mList.get(i).getId())) {
                        mThreePos = i;
                        break;
                    }
                }
            }
        }
    }

    public int getmPos() {
        return mOnePos;
    }

    public void setmPos(int mPos) {
        this.mOnePos = mPos;
    }

    public int getmTwoPos() {
        return mTwoPos;
    }

    public void setmTwoPos(int mTwoPos) {
        this.mTwoPos = mTwoPos;
    }

    public int getmThreePos() {
        return mThreePos;
    }

    public void setmThreePos(int mThreePos) {
        this.mThreePos = mThreePos;
    }

    public RecyclerView getmLeftRecy() {
        return mLeftRecy;
    }

    public ListView getRightList() {
        return rightList;
    }

    private OnItemSelectedClickListener onItemSelectedClickListener;

    public interface OnItemSelectedClickListener {
        void onItemSelectedClick(String id, String name);
    }

    public void setOnItemSelectedClickListener(OnItemSelectedClickListener onItemSelectedClickListener) {
        Log.e("null", "onItemSelectedClickListener222 == " + onItemSelectedClickListener);
        this.onItemSelectedClickListener = onItemSelectedClickListener;
    }
}
