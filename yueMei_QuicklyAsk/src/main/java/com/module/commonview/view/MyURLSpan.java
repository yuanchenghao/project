package com.module.commonview.view;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.quicklyask.util.Utils;

/**
 * 这里是textView超链接点击
 * Created by 裴成浩 on 2017/12/29.
 */

public class MyURLSpan extends ClickableSpan {

    private String url;
    private int color;

    public MyURLSpan(String url) {
        this(url, Utils.setCustomColor("#ff5c77"));         //默认值颜色
    }

    public MyURLSpan(String url, int color) {
        this.url = url;
        this.color = color;
    }

    @Override
    public void onClick(View arg0) {
        if (onClickListener != null) {
            onClickListener.onClick(arg0, url);
        }
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(color);
    }

    //查看更多的点击事件
    public interface OnClickListener {
        void onClick(View arg0, String url);
    }

    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}