package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quicklyask.activity.R;
import com.quicklyask.entity.UserBrowseTao;

import java.util.List;

/**
 * Created by 李鹏 on 2017/03/01 0001.
 */
@SuppressLint("ViewConstructor")
public class DropDownMultiPagerItem extends LinearLayout {

    private TextView mTextNum;

    public DropDownMultiPagerItem(Context context, final int num, final List<UserBrowseTao> beanList) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.item_dropdownfootprint, this);
        //当前位置
        mTextNum = (TextView) findViewById(R.id.item_num);
//        mTextNum.setText("我的足迹（" + (num + 1) + "/" + beanList.size() + ")");

        //描述
        TextView textDes = (TextView) findViewById(R.id.item_des);
        textDes.setText(beanList.get(num).getTitle());

        //价格
        TextView textPrice = (TextView) findViewById(R.id.item_price);
        textPrice.setText("¥"+beanList.get(num).getPrice_discount());

        //图文
        ImageView img = (ImageView) findViewById(R.id.item_img);
        Glide.with(context).load(beanList.get(num).getImg()).into(img);
        //plus
        LinearLayout plusVisorgone = (LinearLayout) findViewById(R.id.item_plus_visorgone);
        TextView plusPrice = (TextView) findViewById(R.id.item_plus_price);

        String memberPrice = beanList.get(num).getMember_price();
        if (!TextUtils.isEmpty(memberPrice) && Integer.parseInt(memberPrice) > 0){
            plusVisorgone.setVisibility(VISIBLE);
            plusPrice.setText("¥"+memberPrice);
        }else {
            plusVisorgone.setVisibility(GONE);
        }

    }

    public void setTextNum(String string) {
        mTextNum.setText(string);
    }
}
