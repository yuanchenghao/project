package com.module.commonview.chatnet;

import android.content.Context;

import okhttp3.WebSocket;

/**
 * Created by Administrator on 2018/1/12.
 */

public interface ReceiveMessageCallBack {
    void webSocketCallBack(Context context,WebSocket webSocket,String text);
}
