package com.module.commonview.chatnet;

/**
 * websocket连接状态
 * @author lucher
 *
 */
public enum Status {

	/**
	 * 连接中
	 */
	CONNECTING,
	/**
	 * 已连接
	 */
	CONNECTED,
	/**
	 * 已断开
	 */
	DISCONNECTED,
	/**
	 * 连接错误
	 */
	ERROR,

}
