package com.module.taodetail.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseWebViewActivity;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.activity.HomeTopTitle;
import com.module.home.controller.activity.MainCitySelectActivity560;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.activity.SearchTopView;
import com.module.home.model.api.SearchShowDataApi;
import com.module.home.model.bean.SearchEntry;
import com.module.home.model.bean.SearchXSData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.CustomDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class TaoActivity extends YMBaseWebViewActivity {

    public static final String TAG = "TaoActivity";
    @BindView(R.id.tao_top_title)
    public SearchTopView mTopTitle;                                              //滑动头部的搜索栏
    @BindView(R.id.acty_tao_container)
    LinearLayout actyTaoContainer;
    @BindView(R.id.community_web_view)
    SmartRefreshLayout taoWebView;



    private BaseNetWorkCallBackApi searchEntryApi;

    //搜索框显示
    private List<SearchEntry> mSearchEntry;
    private String mCity;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_tao;
    }

    @Override
    protected void initView() {
        super.initView();
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTopTitle.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;
        BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(this);
        mWebView.setWebViewClient(baseWebViewClientMessage);
        mWebView.setLayerType(View.LAYER_TYPE_HARDWARE,null);
        actyTaoContainer.addView(mWebView);
        mCity = Utils.getCity();
        taoWebView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadUrl(FinalConstant.TAO_TWO);
            }
        });
        mTopTitle.setOnEventClickListener(new SearchTopView.OnEventClickListener() {
            @Override
            public void onCityClick(View v, String city) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent addIntent = new Intent();
                addIntent.setClass(mContext, MainCitySelectActivity560.class);
                addIntent.putExtra("curcity", city);
                addIntent.putExtra("type", "5");
                if (null != MainTableActivity.mContext){
                    MainTableActivity.mContext.startActivityForResult(addIntent, 1004);
                }else {
                    startActivityForResult(addIntent, 5);
                }

                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_CITY, "tao", "tao"));
            }

            @Override
            public void onTitleClick(View v, String key, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_SEARCH, "tao", "tao"));


                Intent it = new Intent(mContext, SearchAllActivity668.class);
                if (!TextUtils.isEmpty(key)) {
                    it.putExtra(SearchAllActivity668.KEYS, key);
                    SearchEntry searchEntry = mSearchEntry.get(pos);
                    it.putExtra(SearchAllActivity668.TARGET,searchEntry);
                } else {
                    it.putExtra(SearchAllActivity668.KEYS, "");
                }
                it.putExtra(SearchAllActivity668.TYPE, "2");
                startActivity(it);
            }

            @Override
            public void onCartClick(View v) {
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });
        //设置定位城市
        mTopTitle.setCity();
        loadUrl(FinalConstant.TAO_TWO);
    }

    @Override
    protected void initData() {

        initSearhShowData();     //搜索框按钮点击回调初始化
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    

    @Override
    protected void onYmProgressChanged(WebView view, int newProgress) {
        if (newProgress == 100) {
            taoWebView.finishRefresh();
        }

        super.onYmProgressChanged(view, newProgress);
    }
    /**
     * 搜索框显示加载
     * InitSearhShowDataApi
     */
    void initSearhShowData() {

        searchEntryApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "searchEntry");
        searchEntryApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    mSearchEntry = JSONUtil.jsonToArrayList(serverData.data, SearchEntry.class);
                    //设置头部标题
                    mTopTitle.setTitleText(mSearchEntry);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mCity.equals(Utils.getCity())){
            mCity = Utils.getCity();
            loadUrl(FinalConstant.TAO_TWO);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 5) {
            Log.e(TAG,"onActivityResult ===");
            loadUrl(FinalConstant.TAO_TWO);
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mTopTitle.setCity() ;
        initSearhShowData();
        //设置购物车数量
        mTopTitle.setCartNum();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog = new CustomDialog(getParent(),
                    R.style.mystyle, R.layout.customdialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
