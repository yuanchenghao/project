package com.module.taodetail.controller.activity.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.taodetail.model.bean.TaoTagItem;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/20
 */
public class TagNavigationAdapter692 extends RecyclerView.Adapter<TagNavigationAdapter692.ViewHolder> {

    private final Activity mContext;
    private final List<TaoTagItem> mDatas;
    private final LayoutInflater mInflater;
    private final int[] screenSize;
    private final int mImgWH;

    public TagNavigationAdapter692(Activity context, List<TaoTagItem> datas, int imgWH) {
        this.mContext = context;
        this.mDatas = datas;
        this.mImgWH = imgWH;

        mInflater = LayoutInflater.from(mContext);
        screenSize = Utils.getScreenSize(mContext);
    }

    @NonNull
    @Override
    public TagNavigationAdapter692.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.item_tao_tag, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TagNavigationAdapter692.ViewHolder viewHolder, int position) {
        TaoTagItem tagItem = mDatas.get(position);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.tagView.getLayoutParams();
        layoutParams.width = screenSize[0] / 5;
        if (position < 5) {
            layoutParams.bottomMargin = Utils.dip2px(6);
        } else if (position >= (mDatas.size() - 5)) {
            layoutParams.topMargin = Utils.dip2px(6);
        } else {
            layoutParams.topMargin = Utils.dip2px(6);
            layoutParams.bottomMargin = Utils.dip2px(6);
        }

        viewHolder.tagView.setLayoutParams(layoutParams);


        ViewGroup.LayoutParams imgLayout = viewHolder.tagImg.getLayoutParams();
        imgLayout.width = mImgWH;
        imgLayout.height = mImgWH;
        viewHolder.tagImg.setLayoutParams(imgLayout);


        Glide.with(mContext)
                .load(tagItem.getImg())
                .into(viewHolder.tagImg);

        viewHolder.tagTitle.setText(tagItem.getTitle());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout tagView;
        ImageView tagImg;
        TextView tagTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tagView = itemView.findViewById(R.id.tag_view);
            tagImg = itemView.findViewById(R.id.tag_img);
            tagTitle = itemView.findViewById(R.id.tag_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(getLayoutPosition(), mDatas.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    public interface OnEventClickListener {

        void onItemClick(int pos, TaoTagItem data);   //item点击
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}