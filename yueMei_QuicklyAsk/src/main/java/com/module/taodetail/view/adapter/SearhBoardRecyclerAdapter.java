package com.module.taodetail.view.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 活动适配器
 * Created by 裴成浩 on 2019/4/18
 */
public class SearhBoardRecyclerAdapter extends RecyclerView.Adapter<SearhBoardRecyclerAdapter.ViewHolder> {

    private final Activity mContext;
    private final List<SearchResultBoard> mDatas;
    private final LayoutInflater mInflater;
    private final FunctionManager mFunctionManager;

    private final String TEXT_NOTIFY = "text_notify";
    private final String IMAGE_NOTIFY = "image_notify";
    private String TAG = "SearhBoardRecyclerAdapter";

    public SearhBoardRecyclerAdapter(Activity context, List<SearchResultBoard> boards) {
        this.mContext = context;
        this.mDatas = boards;
        mInflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public SearhBoardRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.item_searh_board_view, parent, false));
    }

    /**
     * 局部刷新
     *
     * @param holder
     * @param position
     * @param payloads
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            for (Object payload : payloads) {
                switch ((String) payload) {
                    case TEXT_NOTIFY:
                        setTextView(holder, mDatas.get(position).isSelected());
                        break;
                    case IMAGE_NOTIFY:
                        setImageView(holder, mDatas.get(position).isSelected(), position);
                        break;
                }

            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SearhBoardRecyclerAdapter.ViewHolder viewHolder, int pos) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.board.getLayoutParams();
        if (pos == 0) {
            layoutParams.leftMargin = Utils.dip2px(0);
            layoutParams.rightMargin = Utils.dip2px(5);
        } else if (pos == mDatas.size()) {
            layoutParams.leftMargin = Utils.dip2px(5);
            layoutParams.rightMargin = Utils.dip2px(0);
        } else {
            layoutParams.leftMargin = Utils.dip2px(5);
            layoutParams.rightMargin = Utils.dip2px(5);
        }
        viewHolder.board.setLayoutParams(layoutParams);

        SearchResultBoard data = mDatas.get(pos);
        String before = data.getImage().getBefore();
        String after = data.getImage().getAfter();

        if (!TextUtils.isEmpty(before) && !TextUtils.isEmpty(after)) {
            viewHolder.boardText.setVisibility(View.GONE);
            viewHolder.boardImage.setVisibility(View.VISIBLE);
            setImageView(viewHolder, data.isSelected(), pos);
        } else {
            viewHolder.boardText.setVisibility(View.VISIBLE);
            viewHolder.boardImage.setVisibility(View.GONE);
            viewHolder.boardText.setText(data.getTitle());
            setTextView(viewHolder, data.isSelected());
        }
    }

    /***
     * 设置文字点击
     * @param viewHolder
     * @param isSelected
     */
    private void setTextView(SearhBoardRecyclerAdapter.ViewHolder viewHolder, boolean isSelected) {
        if (isSelected) {
            viewHolder.boardText.setTextColor(Utils.getLocalColor(mContext, R.color.tab_tag));
            viewHolder.boardText.setBackgroundResource(R.drawable.home_diary_tab);
        } else {
            viewHolder.boardText.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            viewHolder.boardText.setBackgroundResource(R.drawable.home_diary_tab2);
        }
    }


    /**
     * 设置ImageView
     *
     * @param viewHolder
     * @param isSelected
     */
    private void setImageView(SearhBoardRecyclerAdapter.ViewHolder viewHolder, boolean isSelected, int pos) {
        SearchResultBoard data = mDatas.get(pos);
        String before = data.getImage().getBefore();
        String after = data.getImage().getAfter();
        if (isSelected) {
            mFunctionManager.setRoundImageSrc(viewHolder.boardImage, after, Utils.dip2px(10));
        } else {
            mFunctionManager.setRoundImageSrc(viewHolder.boardImage, before, Utils.dip2px(10));
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout board;
        private TextView boardText;
        private ImageView boardImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            board = itemView.findViewById(R.id.item_searh_board_view);
            boardText = itemView.findViewById(R.id.item_searh_board_text);
            boardImage = itemView.findViewById(R.id.item_searh_board_image);

            //普通样式
            boardText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG, "普通样式点击");
                    setSelected(getLayoutPosition());
                    if (onEventClickListener != null) {
                        YmStatistics.getInstance().tongjiApp(mDatas.get(getLayoutPosition()).getEvent_params());
                        onEventClickListener.onItemClick(getSelectedIdString());
                    }
                }
            });

            //图片样式
            boardImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelected(getLayoutPosition());
                    if (onEventClickListener != null) {
                        YmStatistics.getInstance().tongjiApp(mDatas.get(getLayoutPosition()).getEvent_params());
                        onEventClickListener.onItemClick(getSelectedIdString());
                    }
                }
            });
        }
    }

    /**
     * 设置选中
     *
     * @param position
     */
    private void setSelected(int position) {
        int typePos = -1;                  //之前选中的
        for (int i = 0; i < mDatas.size(); i++) {
            SearchResultBoard data = mDatas.get(i);
            if (data.isSelected()) {
                typePos = i;
                break;
            }
        }

        if (typePos == position) {
            //之前选中的和现在选中的是同一个
            mDatas.get(position).setSelected(!mDatas.get(position).isSelected());

        } else {
            //之前选中的和现在选中的不是同一个
            if (typePos >= 0) {
                //之前有选中
                mDatas.get(typePos).setSelected(!mDatas.get(typePos).isSelected());
                mDatas.get(position).setSelected(!mDatas.get(position).isSelected());

                notifyType(typePos);

            } else {
                //之前没有选中
                mDatas.get(position).setSelected(!mDatas.get(position).isSelected());
            }
        }

        notifyType(position);
    }

    /**
     * 选中刷新
     *
     * @param position
     */
    private void notifyType(int position) {
        SearchResultBoard searchResultBoard = mDatas.get(position);
        String before = searchResultBoard.getImage().getBefore();
        String after = searchResultBoard.getImage().getAfter();
        if (!TextUtils.isEmpty(before) && !TextUtils.isEmpty(after)) {
            notifyItemChanged(position, IMAGE_NOTIFY);
        } else {
            notifyItemChanged(position, TEXT_NOTIFY);
        }
    }

    /**
     * 获取选中id
     *
     * @return
     */
    private SearchResultBoard getSelectedIdString() {
        for (SearchResultBoard data : mDatas) {
            if (data.isSelected()) {
                return data;
            }
        }
        return null;
    }

    /**
     * 刷新数据
     *
     * @param data
     */
    public void notifyData(List<SearchResultBoard> data) {
        mDatas.clear();
        mDatas.addAll(data);
        notifyDataSetChanged();
    }

    public interface OnEventClickListener {
        void onItemClick(SearchResultBoard data);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
