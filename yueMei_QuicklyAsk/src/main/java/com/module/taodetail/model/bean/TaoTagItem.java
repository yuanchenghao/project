package com.module.taodetail.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2017/8/3.
 */

public class TaoTagItem {

    private String medthod;
    private String one_id;
    private String two_id;
    private String three_id;
    private String level;
    private String channel_title;
    private String title;
    private String img;
    private String homeSource;
    private String isAll;
    private HashMap<String,String> event_params;

    public String getMedthod() {
        return medthod;
    }

    public void setMedthod(String medthod) {
        this.medthod = medthod;
    }

    public String getOne_id() {
        return one_id;
    }

    public void setOne_id(String one_id) {
        this.one_id = one_id;
    }

    public String getTwo_id() {
        return two_id;
    }

    public void setTwo_id(String two_id) {
        this.two_id = two_id;
    }

    public String getThree_id() {
        return three_id;
    }

    public void setThree_id(String three_id) {
        this.three_id = three_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getChannel_title() {
        return channel_title;
    }

    public void setChannel_title(String channel_title) {
        this.channel_title = channel_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getHomeSource() {
        return homeSource;
    }

    public void setHomeSource(String homeSource) {
        this.homeSource = homeSource;
    }

    public String getIsAll() {
        return isAll;
    }

    public void setIsAll(String isAll) {
        this.isAll = isAll;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
