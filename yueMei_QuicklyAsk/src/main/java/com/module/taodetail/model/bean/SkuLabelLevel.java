package com.module.taodetail.model.bean;

/**
 * Created by 裴成浩 on 2019/6/21
 */
public class SkuLabelLevel {
    private String level;
    private String desc;
    private String bilateral_desc;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getBilateral_desc() {
        return bilateral_desc;
    }

    public void setBilateral_desc(String bilateral_desc) {
        this.bilateral_desc = bilateral_desc;
    }
}
