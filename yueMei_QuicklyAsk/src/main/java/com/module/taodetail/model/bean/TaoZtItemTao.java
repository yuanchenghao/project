package com.module.taodetail.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2017/8/3.
 */

public class TaoZtItemTao {
    private String seckilling;
    private String img;
    private String title;
    private String subtitle;
    private String hos_name;
    private String doc_name;
    private String price;
    private String price_discount;
    private String price_range_max;
    private String _id;
    private String showprice;
    private String specialPrice;
    private String show_hospital;
    private String lijian;
    private String invitation;
    private String baoxian;
    private String img66;
    private String repayment;
    private String hos_red_packet;
    private String shixiao;
    private String newp;
    private String hot;
    private String mingyi;
    private String rate;
    private String member_price;
    private HashMap event_params;

    public String getSeckilling() {
        return seckilling;
    }

    public void setSeckilling(String seckilling) {
        this.seckilling = seckilling;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice_range_max() {
        return price_range_max;
    }

    public void setPrice_range_max(String price_range_max) {
        this.price_range_max = price_range_max;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getShowprice() {
        return showprice;
    }

    public void setShowprice(String showprice) {
        this.showprice = showprice;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getShow_hospital() {
        return show_hospital;
    }

    public void setShow_hospital(String show_hospital) {
        this.show_hospital = show_hospital;
    }

    public String getLijian() {
        return lijian;
    }

    public void setLijian(String lijian) {
        this.lijian = lijian;
    }

    public String getInvitation() {
        return invitation;
    }

    public void setInvitation(String invitation) {
        this.invitation = invitation;
    }

    public String getBaoxian() {
        return baoxian;
    }

    public void setBaoxian(String baoxian) {
        this.baoxian = baoxian;
    }

    public String getImg66() {
        return img66;
    }

    public void setImg66(String img66) {
        this.img66 = img66;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getHos_red_packet() {
        return hos_red_packet;
    }

    public void setHos_red_packet(String hos_red_packet) {
        this.hos_red_packet = hos_red_packet;
    }

    public String getShixiao() {
        return shixiao;
    }

    public void setShixiao(String shixiao) {
        this.shixiao = shixiao;
    }

    public String getNewp() {
        return newp;
    }

    public void setNewp(String newp) {
        this.newp = newp;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getMingyi() {
        return mingyi;
    }

    public void setMingyi(String mingyi) {
        this.mingyi = mingyi;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public HashMap getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap event_params) {
        this.event_params = event_params;
    }
}
