package com.module.taodetail.model.bean;

/**
 * Created by 裴成浩 on 2019/6/27
 */
public class PromotionNoticeTextData {
    private String promotionType;
    private String promotionNotice;

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getPromotionNotice() {
        return promotionNotice;
    }

    public void setPromotionNotice(String promotionNotice) {
        this.promotionNotice = promotionNotice;
    }
}
