package com.module;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.view.MyPagerGalleryView3;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dwb on 16/7/18.
 */
public class SurfaceActivity extends Activity {

    private MediaPlayer mediaPlayer; // 播放器的内部实现是通过MediaPlayer
    private SurfaceView surfaceView;// 装在视频的容器
    private SurfaceHolder surfaceHolder;// 控制surfaceView的属性（尺寸、格式等）对象
    private Button jinruBt;

    //幻灯片
    private RelativeLayout huandengRly;// 幻灯片的容器
    private MyPagerGalleryView3 gallery;// 广告控件
    private LinearLayout ovalLayout;//圆点容器
    private TextView adgallerytxt;// 图片上面的文字
    private String[] urlImageList;
    private String[] txtViewpager;
    /**
     * 图 id的数组,本地 试用
     */
    private int[] imageId = new int[]{R.drawable.img03, R.drawable.img05, R.drawable.img03, R.drawable.img05};
    private ImageView ivSurfaceView;
    private RelativeLayout rlVideo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_surface_start);

        surfaceView = findViewById(R.id.surface);
        ivSurfaceView = findViewById(R.id.iv_surface);
        rlVideo = findViewById(R.id.rl_video);
        ivSurfaceView.setImageResource(R.drawable.guiding_img);

        //根据日期判断使用视频还是图片
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");//设置日期格式
        long curtime = Integer.parseInt(df.format(new Date()));

        if (curtime >= 20171103 && curtime <= 20171117) {
            rlVideo.setVisibility(View.GONE);
            ivSurfaceView.setVisibility(View.VISIBLE);
        } else {
            rlVideo.setVisibility(View.VISIBLE);
            ivSurfaceView.setVisibility(View.GONE);
        }

        jinruBt = findViewById(R.id.bt_jinru);
        //头部幻灯片
        huandengRly = findViewById(R.id.homepage_huandeng_content_rly);
        gallery = findViewById(R.id.adgallery);
        ovalLayout = findViewById(R.id.ovalLayout1);// 获取圆点容器
        adgallerytxt = findViewById(R.id.adgallerytxt);

        txtViewpager = new String[4];
        txtViewpager[0] = "百万网友真实整形日记";
        txtViewpager[1] = "严选优质合作医生";
        txtViewpager[2] = "来整形社区获得帮助";
        txtViewpager[3] = "分享你的每一个改变";

        gallery.start(SurfaceActivity.this, null, imageId,
                4000, ovalLayout,
                R.drawable.dot_normal_ff5c77,
                R.drawable.dot_unnormal_ffffff_30,
                adgallerytxt, txtViewpager, txtViewpager);

        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDisplay(surfaceHolder);
                try {
                    AssetManager assetManager = SurfaceActivity.this.getAssets();
                    AssetFileDescriptor afd = assetManager.openFd("yuemei_start.mp4");
                    AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                    audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mediaPlayer.setLooping(true);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mediaPlayer != null)
                    mediaPlayer.release();
            }
        });

        /**
         *  这里必须设置为SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS哦，意思
         *  是创建一个push的'surface'，主要的特点就是不进行缓冲
         */
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


        jinruBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JumpMasterProcess();
            }
        });

        ivSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JumpMasterProcess();
            }
        });

        skip3Miao();

    }


    void skip3Miao(){
        new CountDownTimer(3500, 1000) {
            // 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                JumpMasterProcess();
            }
        }.start();
    }

    private void JumpMasterProcess() {
        if (null != mediaPlayer) {

            if (mediaPlayer != null) {
                try {
                    mediaPlayer.stop();
                } catch (IllegalStateException e) {
                    mediaPlayer = null;
                    mediaPlayer = new MediaPlayer();
                }
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }

        Intent intent3 = new Intent(SurfaceActivity.this,
                MainTableActivity.class);
        startActivity(intent3);
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mediaPlayer) {
//            if (mediaPlayer.isPlaying()) {
//                mediaPlayer.stop();
//            }
            mediaPlayer.release();
        }
        //Activity销毁时停止播放，释放资源。不做这个操作，即使退出还是能听到视频播放的声音
    }
}
