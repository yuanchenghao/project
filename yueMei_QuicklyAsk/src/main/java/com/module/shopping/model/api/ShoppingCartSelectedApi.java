package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * 购物车选中/不选中
 * Created by 裴成浩 on 2018/12/3
 */
public class ShoppingCartSelectedApi implements BaseCallBackApi {
    private String TAG = "ShoppingCartNumberApi";
    private HashMap<String, Object> mShoppingCartSelectedHashMap;  //传值容器

    public ShoppingCartSelectedApi() {
        mShoppingCartSelectedHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "selectedcart", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData = " + mData.toString());
                listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getmShoppingCartSelectedHashMap() {
        return mShoppingCartSelectedHashMap;
    }

    public void addData(String key, String value) {
        mShoppingCartSelectedHashMap.put(key, value);
    }
}