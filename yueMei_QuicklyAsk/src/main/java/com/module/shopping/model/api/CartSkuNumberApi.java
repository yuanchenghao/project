package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.CartSkuNumberData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 获取购物车中SKU数量
 * Created by 裴成浩 on 2019/2/15
 */
public class CartSkuNumberApi implements BaseCallBackApi {
    private String TAG = "ShoppingCartNumberApi";
    private HashMap<String, Object> mCartSkuNumberHashMap;  //传值容器

    public CartSkuNumberApi() {
        mCartSkuNumberHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "getcartnumber", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData = " + mData.toString());
                if ("1".equals(mData.code)) {
                    try {
                        CartSkuNumberData shoppingCartSkuData = JSONUtil.TransformSingleBean(mData.data, CartSkuNumberData.class);
                        listener.onSuccess(shoppingCartSkuData);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public HashMap<String, Object> getmCartSkuNumberHashMap() {
        return mCartSkuNumberHashMap;
    }

    public void addData(String key, String value) {
        mCartSkuNumberHashMap.put(key, value);
    }
}
