package com.module.shopping.model.bean;

import java.util.Comparator;

/**
 * 购物车医院红包降序排序
 * Created by 裴成浩 on 2018/12/5
 */
public class CouponsRank implements Comparator<CartSkuCouponsData> {

    @Override
    public int compare(CartSkuCouponsData lhs, CartSkuCouponsData rhs) {
        return (int)(Float.parseFloat(rhs.getMoney()) - Float.parseFloat(lhs.getMoney()));
    }
}