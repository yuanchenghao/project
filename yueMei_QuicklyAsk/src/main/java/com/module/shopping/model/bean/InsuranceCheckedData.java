package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/14
 */
public class InsuranceCheckedData {
    private int hosPos;
    private int skuPos;
    private boolean isChecked;
    private String taoid;       //SKU的id
    private float price;        //保险价格

    public int getHosPos() {
        return hosPos;
    }

    public void setHosPos(int hosPos) {
        this.hosPos = hosPos;
    }

    public int getSkuPos() {
        return skuPos;
    }

    public void setSkuPos(int skuPos) {
        this.skuPos = skuPos;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getTaoid() {
        return taoid;
    }

    public void setTaoid(String taoid) {
        this.taoid = taoid;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}

