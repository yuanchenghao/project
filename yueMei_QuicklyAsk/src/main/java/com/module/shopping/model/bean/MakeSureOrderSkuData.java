package com.module.shopping.model.bean;

import com.module.other.netWork.netWork.FinalConstant1;

/**
 * Created by 裴成浩 on 2018/11/27.
 */
public class MakeSureOrderSkuData {
    private MakeSureOrderSkuNumberData nousemember;
    private MakeSureOrderSkuNumberData usemember;
    private String editTextStr = FinalConstant1.YUEMEI_DEBUG ? "悦美测试" : "";

    public MakeSureOrderSkuNumberData getNousemember() {
        return nousemember;
    }

    public void setNousemember(MakeSureOrderSkuNumberData nousemember) {
        this.nousemember = nousemember;
    }

    public MakeSureOrderSkuNumberData getUsemember() {
        return usemember;
    }

    public void setUsemember(MakeSureOrderSkuNumberData usemember) {
        this.usemember = usemember;
    }

    public String getEditTextStr() {
        return editTextStr;
    }

    public void setEditTextStr(String editTextStr) {
        this.editTextStr = editTextStr;
    }
}
