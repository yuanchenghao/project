package com.module.shopping.model.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/12/12
 */
public class MakeSureOrderSkuNumberData {
    private String hos_id;
    private String hos_name;
    private String pay_dingjin;
    private String pay_weikaun;
    private List<MakeSureOrderSkuNumberTao> tao_data;
    private ArrayList<YouHuiCoupons> wk_coupons;                //尾款红包
    private YouHuiCoupons coupons;                         //选中的尾款红包，默认是null的

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getPay_dingjin() {
        return pay_dingjin;
    }

    public void setPay_dingjin(String pay_dingjin) {
        this.pay_dingjin = pay_dingjin;
    }

    public String getPay_weikaun() {
        return pay_weikaun;
    }

    public void setPay_weikaun(String pay_weikaun) {
        this.pay_weikaun = pay_weikaun;
    }

    public List<MakeSureOrderSkuNumberTao> getTao_data() {
        return tao_data;
    }

    public void setTao_data(List<MakeSureOrderSkuNumberTao> tao_data) {
        this.tao_data = tao_data;
    }

    public ArrayList<YouHuiCoupons> getWk_coupons() {
        return wk_coupons;
    }

    public void setWk_coupons(ArrayList<YouHuiCoupons> wk_coupons) {
        this.wk_coupons = wk_coupons;
    }

    public YouHuiCoupons getCoupons() {
        return coupons;
    }

    public void setCoupons(YouHuiCoupons coupons) {
        this.coupons = coupons;
    }
}
