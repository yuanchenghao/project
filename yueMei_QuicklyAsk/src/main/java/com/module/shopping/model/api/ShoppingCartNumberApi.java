package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.ShoppingCartSkuData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 购物车操作加减
 * Created by 裴成浩 on 2018/12/3
 */
public class ShoppingCartNumberApi implements BaseCallBackApi {
    private String TAG = "ShoppingCartNumberApi";
    private HashMap<String, Object> mShoppingCartNumberHashMap;  //传值容器

    public ShoppingCartNumberApi() {
        mShoppingCartNumberHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "upcartnumber", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData = " + mData.toString());
                if ("1".equals(mData.code)) {
                    ShoppingCartSkuData shoppingCartSkuData = null;
                    try {
                        shoppingCartSkuData = JSONUtil.TransformSingleBean(mData.data, ShoppingCartSkuData.class);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                    listener.onSuccess(shoppingCartSkuData);
                }
            }
        });
    }

    public HashMap<String, Object> getmShoppingCartNumberHashMap() {
        return mShoppingCartNumberHashMap;
    }

    public void addData(String key, String value) {
        mShoppingCartNumberHashMap.put(key, value);
    }
}
