package com.module.shopping.model.bean;

import java.util.Comparator;

/**
 * Created by 裴成浩 on 2018/12/13
 */
public class WkCouponsRank implements Comparator<YouHuiCoupons> {

    @Override
    public int compare(YouHuiCoupons lhs, YouHuiCoupons rhs) {
        return (int)(Float.parseFloat(rhs.getMoney()) - Float.parseFloat(lhs.getMoney()));
    }
}