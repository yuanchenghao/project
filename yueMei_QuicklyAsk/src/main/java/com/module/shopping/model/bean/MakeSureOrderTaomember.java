package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/12
 */
public class MakeSureOrderTaomember {
    private String user_is_member;
    private String memberlijian;
    private String frist_lijian_price;
    private String buy_plus_price;

    public String getUser_is_member() {
        return user_is_member;
    }

    public void setUser_is_member(String user_is_member) {
        this.user_is_member = user_is_member;
    }

    public String getMemberlijian() {
        return memberlijian;
    }

    public void setMemberlijian(String memberlijian) {
        this.memberlijian = memberlijian;
    }

    public String getFrist_lijian_price() {
        return frist_lijian_price;
    }

    public void setFrist_lijian_price(String frist_lijian_price) {
        this.frist_lijian_price = frist_lijian_price;
    }

    public String getBuy_plus_price() {
        return buy_plus_price;
    }

    public void setBuy_plus_price(String buy_plus_price) {
        this.buy_plus_price = buy_plus_price;
    }
}
