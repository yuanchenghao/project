package com.module.shopping.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.web.WebViewUrlLoading;
import com.module.shopping.model.bean.CartDeleteData;
import com.module.shopping.model.bean.CartNumberData;
import com.module.shopping.model.bean.CartSelectedData;
import com.module.shopping.model.bean.CartSkuCouponsData;
import com.module.shopping.model.bean.CouponsRank;
import com.module.shopping.model.bean.ShoppingCartData;
import com.module.shopping.model.bean.ShoppingCartSkuCoupons;
import com.module.shopping.model.bean.ShoppingCartSkuData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/11/21.
 */
public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder> {

    private String TAG = "ShoppingCartAdapter";
    private Activity mContext;
    private List<ShoppingCartData> mDatas;
    private LayoutInflater mInflater;
    private ShoppingOnStateChangeListener stateChangeListener;                  //删除操作监听
    private HashMap<String, ShoppingCartSkuAdapter> skuAdapters = new LinkedHashMap<>();
//    private BaseWebViewClientMessage webViewClientManager;                          //跳转

    public static final String RED_PACKAGE = "red_package";                   //医院红包优惠局部刷新
    public static final String SELECTED_BUTTON = "selected_button";           //是否选中局部刷新

    private HashMap<String, HashMap<String, String>> selecteSkuList = new HashMap<>();        //选中SKU列表

    public ShoppingCartAdapter(Activity context, List<ShoppingCartData> shoppingCartData) {
        this.mContext = context;
        this.mDatas = dataSort(shoppingCartData);
        mInflater = LayoutInflater.from(mContext);
        stateChangeListener = new ShoppingOnStateChangeListener();
//        webViewClientManager = new BaseWebViewClientMessage(mContext);

        Log.e(TAG, "mDatas == " + mDatas.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View inflate = mInflater.inflate(R.layout.shopping_cart_view, parent, false);
        return new ViewHolder(inflate);
    }

    /**
     * 局部刷新
     *
     * @param viewHolder
     * @param position
     * @param payloads
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(viewHolder, position);
        } else {
            for (Object payload : payloads) {
                ShoppingCartData shoppingCartData = mDatas.get(position);
                ShoppingCartSkuCoupons couponsData = shoppingCartData.getCoupons();
                switch ((String) payload) {
                    case RED_PACKAGE:
                        //医院红包最佳优惠
                        ShoppingCartSkuAdapter shoppingCartSkuAdapter1 = skuAdapters.get(getItemHosId(position));
                        if (shoppingCartSkuAdapter1 != null) {
                            setCouponsData(viewHolder, getMaxCoupons(shoppingCartSkuAdapter1.getHosSelectedPrice(), couponsData.getData()));
                        }
                        break;
                    case SELECTED_BUTTON:
                        //选中未选中局部刷新
                        if (!"0".equals(shoppingCartData.getHos_id())) {
                            //不是失效SKU
                            ShoppingCartSkuAdapter shoppingCartSkuAdapter = skuAdapters.get(getItemHosId(position));
                            if (shoppingCartSkuAdapter != null) {
                                viewHolder.hosRadio.setChecked(shoppingCartSkuAdapter.isAllCheck());        //是否是选中状态
                                //医院红包最佳优惠
                                Log.e(TAG, "22222");
                                setCouponsData(viewHolder, getMaxCoupons(shoppingCartSkuAdapter.getHosSelectedPrice(), couponsData.getData()));
                            }
                        }
                        break;
                }
            }
        }
    }


    @SuppressLint({"RecyclerView", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.cartView.getLayoutParams();
        if (pos == 0) {
            layoutParams.topMargin = Utils.dip2px(14);
            layoutParams.bottomMargin = Utils.dip2px(5);
        } else if (pos == mDatas.size() - 1) {
            layoutParams.topMargin = Utils.dip2px(5);
            layoutParams.bottomMargin = Utils.dip2px(29);

        } else {
            layoutParams.topMargin = Utils.dip2px(5);
            layoutParams.bottomMargin = Utils.dip2px(5);
        }

        viewHolder.cartView.setLayoutParams(layoutParams);

        ShoppingCartData shoppingCartData = mDatas.get(pos);
        ShoppingCartSkuCoupons couponsData = shoppingCartData.getCoupons();

        //设置SKU适配器
        ShoppingCartSkuAdapter shoppingCartSkuAdapter;
        if (skuAdapters.size() < mDatas.size()) {
            Log.e(TAG, "1111");
            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            shoppingCartSkuAdapter = new ShoppingCartSkuAdapter(mContext, shoppingCartData.getHos_id(), mDatas.get(pos).getData(), stateChangeListener);
            ((DefaultItemAnimator) viewHolder.mSkuRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果(是因为局部刷新的是每一个item,而不是item的部分)
            viewHolder.mSkuRecycler.setLayoutManager(scrollLinearLayoutManager);
            viewHolder.mSkuRecycler.setAdapter(shoppingCartSkuAdapter);
            skuAdapters.put(shoppingCartData.getHos_id(), shoppingCartSkuAdapter);
        } else {
            Log.e(TAG, "2222");
            shoppingCartSkuAdapter = skuAdapters.get(shoppingCartData.getHos_id());
            viewHolder.mSkuRecycler.setAdapter(shoppingCartSkuAdapter);
        }

        //监听
        viewHolder.initListener();

        //判断是否是失效的sku
        if (!"0".equals(shoppingCartData.getHos_id())) {
            viewHolder.llHosSku.setVisibility(View.VISIBLE);
            viewHolder.llFailureSku.setVisibility(View.GONE);
            //是否是选中状态
            Log.e(TAG, "shoppingCartSkuAdapter.isAllCheck()111 === " + shoppingCartSkuAdapter.isAllCheck());
            viewHolder.hosRadio.setChecked(shoppingCartSkuAdapter.isAllCheck());
            //医院名称
            viewHolder.hosName.setText(shoppingCartData.getHos_name());
            //领劵
            viewHolder.mSecurities.setText(couponsData.getTitle());
            //获取优惠力度最大的优惠劵
            setCouponsData(viewHolder, getMaxCoupons(skuAdapters.get(getItemHosId(pos)).getHosSelectedPrice(), couponsData.getData()));

        } else {
            viewHolder.llHosSku.setVisibility(View.GONE);
            viewHolder.llFailureSku.setVisibility(View.VISIBLE);
            viewHolder.llFailureSkuNumber.setText("失效商品" + shoppingCartData.getData().size() + "件");
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout cartView;                  //卡片效果
        private LinearLayout llHosSku;                  //医院sku容器
        private CheckBox hosRadio;               //是否选择该SKu

        private TextView hosName;                  //医院名称
        private TextView mSecurities;               //领劵
        private LinearLayout mRedPackageClick;             //满减
        private TextView mPreferential;             //满减
        private TextView mAdditem;                  //凑单

        private LinearLayout llFailureSku;               //失效SKU容器
        private TextView llFailureSkuNumber;         //失效SKU数量
        private TextView llDeleteSkuNumber;         //失效SKU清空
        private TextView llSeeSkuNumber;            //失效SKU查看

        private RecyclerView mSkuRecycler;          //SKU列表

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cartView = itemView.findViewById(R.id.shopping_cart_view);
            llHosSku = itemView.findViewById(R.id.ll_hos_sku);
            hosRadio = itemView.findViewById(R.id.shopping_cart_hos_radio);

            hosName = itemView.findViewById(R.id.shopping_cart_hos_name);
            mSecurities = itemView.findViewById(R.id.shopping_cart_securities);
            mRedPackageClick = itemView.findViewById(R.id.shopping_cart_red_package_click);
            mPreferential = itemView.findViewById(R.id.shopping_cart_view_preferential);
            mAdditem = itemView.findViewById(R.id.shopping_cart_additem);

            llFailureSku = itemView.findViewById(R.id.ll_failure_sku);
            llFailureSkuNumber = itemView.findViewById(R.id.ll_failure_sku_number);
            llDeleteSkuNumber = itemView.findViewById(R.id.ll_failure_delete_sku_number);
            llSeeSkuNumber = itemView.findViewById(R.id.ll_failure_see_sku_number);

            mSkuRecycler = itemView.findViewById(R.id.shopping_cart_sku_recycler);
        }

        /**
         * 监听
         */
        private void initListener() {
            //医院名称
            hosName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(mDatas.get(getLayoutPosition()).getUrl())) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(mDatas.get(getLayoutPosition()).getUrl(), "0", "0");
                    }
                }
            });

            //领劵点击
            mSecurities.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(mDatas.get(getLayoutPosition()).getUrl())) {
                        WebViewUrlLoading.getInstance().showWebDetail(mContext, mDatas.get(getLayoutPosition()).getCoupons().getUrl());
                    }
                }
            });

            //凑单
            mAdditem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShoppingCartSkuAdapter shoppingCartSkuAdapter = skuAdapters.get(getItemHosId(getLayoutPosition()));
                    if (shoppingCartSkuAdapter != null) {
                        CartSkuCouponsData[] maxCoupons = getMaxCoupons(shoppingCartSkuAdapter.getHosSelectedPrice(), mDatas.get(getLayoutPosition()).getCoupons().getData());
                        if (maxCoupons.length == 2) {
                            //下个更大满减存在
                            if (!TextUtils.isEmpty(maxCoupons[1].getSearch_url())) {
                                WebViewUrlLoading.getInstance().showWebDetail(mContext, maxCoupons[1].getSearch_url());
                            }
                        }
                    }
                }
            });

            //选中监听
            hosRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.isPressed()) {
                        skuAdapters.get(getItemHosId(getLayoutPosition())).allSelected(isChecked);

                        CartSelectedData cartSelectedData = new CartSelectedData();
                        if (isChecked) {
                            cartSelectedData.setSelect("1");
                        } else {
                            cartSelectedData.setSelect("0");
                        }

                        itemSelected(getLayoutPosition(), cartSelectedData);
                    }
                }
            });

            //回调监听
            skuAdapters.get(getItemHosId(getLayoutPosition())).setOnEventClickListener(new ShoppingCartSkuAdapter.OnEventClickListener() {

                @Override
                public void onSelectedClick(boolean allCheck, CartSelectedData data) {
                    hosRadio.setChecked(allCheck);
                    Log.e(TAG, "item === " + getLayoutPosition());
                    itemSelected(getLayoutPosition(), data);
                }

                @Override
                public void onDeleteClick(List<CartDeleteData> datas) {
                    if (onEventClickListener != null) {
                        Log.e(TAG, "pos = " + getLayoutPosition());
                        for (CartDeleteData data : datas) {
                            data.setHos_id(mDatas.get(getLayoutPosition()).getHos_id());

                        }
                        onEventClickListener.onDeleteClick(datas);
                    }
                }

                @Override
                public void onNumberClick(CartNumberData data) {
                    if (onEventClickListener != null) {
                        data.setHosPos(getLayoutPosition());
                        onEventClickListener.onNumberClick(data);
                    }
                }
            });

            //失效SKU清空
            llDeleteSkuNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onEventClickListener != null) {
                        List<CartDeleteData> cartDeleteDatas = new ArrayList<>();
                        for (ShoppingCartSkuData data : mDatas.get(getLayoutPosition()).getData()) {
                            CartDeleteData cartDeleteData = new CartDeleteData();
                            cartDeleteData.setHos_id("0");
                            cartDeleteData.setSku_id(data.getTao_id());
                            cartDeleteData.setCart_id(data.getCart_id());
                            cartDeleteDatas.add(cartDeleteData);
                        }
                        onEventClickListener.onDeleteClick(cartDeleteDatas);
                    }
                }
            });

//            //失效SKU查看
//            llSeeSkuNumber.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
//                    intent.putExtra("id", mDatas.get(getLayoutPosition()).getData().get());
//                    intent.putExtra("source", "0");
//                    intent.putExtra("objid", "0");
//                    mContext.startActivity(intent);
//                }
//            });
        }
    }

    /**
     * 得到最大优惠的医院红包
     *
     * @param hosAllPrice ：获取当前医院订单金额
     * @param datas       :医院红包数据
     * @return ：CartSkuCouponsData[0]：最大优惠，CartSkuCouponsData[1]：下一个更大的满减优惠
     */
    private CartSkuCouponsData[] getMaxCoupons(float hosAllPrice, List<CartSkuCouponsData> datas) {

        Log.e(TAG, "hosAllPrice == " + hosAllPrice);
        //降序排序
        for (CartSkuCouponsData data : datas) {
            Log.e(TAG, "getLowest_consumption 111== " + data.getLowest_consumption());
            Log.e(TAG, "getMoney 111== " + data.getMoney());
        }

        Collections.sort(datas, new CouponsRank());
        for (CartSkuCouponsData data : datas) {
            Log.e(TAG, "getLowest_consumption 222== " + data.getLowest_consumption());
            Log.e(TAG, "getMoney 222== " + data.getMoney());
        }

        //获取到最大优惠下标
        int maxCouponsPos = -1;          //最大优惠下标
        int nextCouponsPos = -1;         //下一个更大的满减优惠下标
        for (int i = 0; i < datas.size(); i++) {
            CartSkuCouponsData data = datas.get(i);
            //是领取状态
            float lowestConsumption = Float.parseFloat(data.getLowest_consumption());
            Log.e(TAG, "lowestConsumption == " + lowestConsumption);
            if (lowestConsumption <= hosAllPrice) {
                maxCouponsPos = i;
                if (i != 0) {
                    nextCouponsPos = i - 1;
                }
                break;
            }
        }

        //获取最大优惠数据，并且获取下个更大优惠的数据
        Log.e(TAG, "maxCouponsPos == " + maxCouponsPos);
        Log.e(TAG, "nextCouponsPos == " + nextCouponsPos);
        if (maxCouponsPos >= 0 && nextCouponsPos < 0) {
            //当前优惠存在，下个满减优惠不存在
            CartSkuCouponsData maxCouponsData = datas.get(maxCouponsPos);
            return new CartSkuCouponsData[]{maxCouponsData, null};
        } else if (nextCouponsPos >= 0) {
            //下个满减优惠存在，当前优惠存在
            CartSkuCouponsData maxCouponsData = datas.get(maxCouponsPos);
            CartSkuCouponsData nextCouponsData = datas.get(nextCouponsPos);
            return new CartSkuCouponsData[]{maxCouponsData, nextCouponsData};
        } else {
            if (datas.size() != 0) {
                //未达到使用红包的要求
                CartSkuCouponsData nextCartSkuCoupons = datas.get(datas.size() - 1);
                for (CartSkuCouponsData data : datas) {
                    if (Float.parseFloat(data.getMoney()) == Float.parseFloat(nextCartSkuCoupons.getMoney())) {         //如果满减金额相等
                        if (Float.parseFloat(data.getLowest_consumption()) < Float.parseFloat(nextCartSkuCoupons.getLowest_consumption())) {        //按照最低门槛找
                            nextCartSkuCoupons = data;
                        }
                    }
                }
                return new CartSkuCouponsData[]{null, nextCartSkuCoupons};
            } else {
                //没有可用医院红包
                return new CartSkuCouponsData[]{null, null};
            }
        }
    }

    /**
     * 设置最大医院红包文案
     *
     * @param viewHolder
     * @param cartSkuCouponsData
     */
    @SuppressLint("SetTextI18n")
    private void setCouponsData(ViewHolder viewHolder, CartSkuCouponsData[] cartSkuCouponsData) {
        CartSkuCouponsData maxCoupons = cartSkuCouponsData[0];
        CartSkuCouponsData nextCoupons = cartSkuCouponsData[1];
        if (maxCoupons != null && nextCoupons != null) {
            Log.e(TAG, "cartSkuCouponsData[0].getLowest_consumption() == " + cartSkuCouponsData[0].getLowest_consumption());
            Log.e(TAG, "cartSkuCouponsData[0].getMoney() == " + cartSkuCouponsData[0].getMoney());
            Log.e(TAG, "cartSkuCouponsData[1].getLowest_consumption() == " + cartSkuCouponsData[1].getLowest_consumption());
            Log.e(TAG, "cartSkuCouponsData[1].getMoney() == " + cartSkuCouponsData[1].getMoney());

            //下个满减优惠存在，当前优惠存在
            viewHolder.mRedPackageClick.setVisibility(View.VISIBLE);
            viewHolder.mPreferential.setText("已减" + cartSkuCouponsData[0].getMoney() + "元，满" + cartSkuCouponsData[1].getLowest_consumption() + "减" + cartSkuCouponsData[1].getMoney());

            //凑单
            viewHolder.mAdditem.setVisibility(View.VISIBLE);
            viewHolder.mAdditem.setText(nextCoupons.getSearch_title());
        } else if (maxCoupons != null) {
            Log.e(TAG, "cartSkuCouponsData[0].getLowest_consumption() == " + cartSkuCouponsData[0].getLowest_consumption());
            Log.e(TAG, "cartSkuCouponsData[0].getMoney() == " + cartSkuCouponsData[0].getMoney());

            //当前优惠存在，下个满减优惠不存在
            viewHolder.mRedPackageClick.setVisibility(View.VISIBLE);
            viewHolder.mPreferential.setText("已减" + cartSkuCouponsData[0].getMoney() + "元");

            //凑单
            viewHolder.mAdditem.setVisibility(View.GONE);
        } else if (nextCoupons != null) {
            Log.e(TAG, "cartSkuCouponsData[1].getLowest_consumption() == " + cartSkuCouponsData[1].getLowest_consumption());
            Log.e(TAG, "cartSkuCouponsData[1].getMoney() == " + cartSkuCouponsData[1].getMoney());

            //有医院红包单未达到使用红包的要求
            viewHolder.mRedPackageClick.setVisibility(View.VISIBLE);
            viewHolder.mPreferential.setText("满" + cartSkuCouponsData[1].getLowest_consumption() + "减" + cartSkuCouponsData[1].getMoney());

            //凑单
            viewHolder.mAdditem.setVisibility(View.VISIBLE);
            viewHolder.mAdditem.setText(nextCoupons.getSearch_title());
        } else {
            //没有可用医院红包
            viewHolder.mRedPackageClick.setVisibility(View.GONE);

            //凑单
            viewHolder.mAdditem.setVisibility(View.GONE);
        }
    }


    /**
     * 购物车下的所有SKU全部选中 or 全部未选中
     *
     * @param isChecked :
     */
    public void allSelected(boolean isChecked) {
        for (int i = 0; i < mDatas.size(); i++) {
            ShoppingCartSkuAdapter skuAdapter = skuAdapters.get(getItemHosId(i));
            //如果当前适配器存在
            if (skuAdapter != null) {
                skuAdapter.allSelected(isChecked);
            } else {
                //当前适配器不存在
                List<ShoppingCartSkuData> skuData = mDatas.get(i).getData();
                for (int j = 0; j < skuData.size(); j++) {
                    if (isChecked) {
                        mDatas.get(i).getData().get(j).setSelected("1");
                    } else {
                        mDatas.get(i).getData().get(j).setSelected("0");
                    }
                }
            }
            //局部刷新
            notifyItemChanged(i, SELECTED_BUTTON);
        }
    }

    /**
     * 某条item选中刷新
     *
     * @param pos
     */
    private void itemSelected(int pos, CartSelectedData cartSelectedData) {
        Log.e(TAG, "pos === " + pos);
        boolean allSelected = true;
        if (skuAdapters.size() < mDatas.size()) {
            for (int i = skuAdapters.size(); i < mDatas.size(); i++) {
                List<ShoppingCartSkuData> data = mDatas.get(i).getData();
                for (ShoppingCartSkuData skuData : data) {
                    if ("0".equals(skuData.getSelected())) {
                        allSelected = false;
                    }
                }
            }
        }

        for (ShoppingCartSkuAdapter adapter : skuAdapters.values()) {
            if (!adapter.isAllCheck()) {
                allSelected = false;
                break;
            }
        }

        if (TextUtils.isEmpty(cartSelectedData.getFlag())) {
            if (allSelected) {
                cartSelectedData.setFlag("1");
            } else {
                cartSelectedData.setFlag("2");
                cartSelectedData.setHos_id(getItemHosId(pos));
            }
        }

        if (onEventClickListener != null) {
            cartSelectedData.setHosPos(pos);
            onEventClickListener.onSelectedClick(allSelected, cartSelectedData);
        }
    }

    /**
     * 获取某一条的医院id
     *
     * @param pos
     * @return
     */
    private String getItemHosId(int pos) {
        Log.e(TAG, "mDatas == " + mDatas);
        return mDatas.get(pos).getHos_id();
    }


    /**
     * 删除一条数据
     *
     * @param hos_id
     */
    public void deleteData(String hos_id) {
        for (int i = 0; i < mDatas.size(); i++) {
            ShoppingCartData data = mDatas.get(i);
            if (data.getHos_id().equals(hos_id)) {
                Log.e(TAG, "删除第 == " + i + "个");
                skuAdapters.remove(hos_id);                            //删除sku适配器
                mDatas.remove(i);                                     //删除数据源
                notifyItemRemoved(i);                                 //刷新被删除的地方
                break;
            }
        }
    }

    /**
     * 加减，选中，删除操作时局部刷新
     *
     * @param pos
     * @param payload :标识
     */
    public void notifyItemLocal(int pos, Object payload) {
        notifyItemChanged(pos, payload);
    }

    /**
     * 根据hos_id，加减，选中，删除操作时局部刷新
     *
     * @param hos_id
     * @param payload
     */
    public void notifyItemLocal(String hos_id, Object payload) {
        for (int i = 0; i < mDatas.size(); i++) {
            ShoppingCartData data = mDatas.get(i);
            if (data.getHos_id().equals(hos_id)) {
                notifyItemChanged(i, payload);
                break;
            }
        }
    }

    /**
     * 获取侧滑操作类
     *
     * @return
     */
    public ShoppingOnStateChangeListener getStateChangeListener() {
        return stateChangeListener;
    }

    /**
     * 通过下标获取当前的SKU列表适配器
     *
     * @param pos :下标
     * @return
     */
    public ShoppingCartSkuAdapter getSkuAdapters(int pos) {
        return skuAdapters.get(getItemHosId(pos));
    }

    /**
     * 通过医院id获取当前的SKU列表适配器
     *
     * @param hos_id :医院id
     * @return
     */
    public ShoppingCartSkuAdapter getSkuIdAdapters(String hos_id) {
        return skuAdapters.get(hos_id);
    }


    /**
     * 计算订单选中价格
     *
     * @return :当前订单选中的总价格
     */
    public float getSelectedPrice() {
        float hosAllPrice = 0;
        //循环计算
        for (ShoppingCartSkuAdapter adapter : skuAdapters.values()) {
            for (ShoppingCartSkuData skuData : adapter.getDatas()) {
                if ("1".equals(skuData.getSelected())) {
                    float price = Float.parseFloat(skuData.getTao().getPay_dingjin());
                    hosAllPrice += price;
                }
            }
        }

        //item没有全部加载出来
        if (skuAdapters.size() < mDatas.size()) {
            //还没有加载出来的价格计算
            for (int i = skuAdapters.size(); i < mDatas.size(); i++) {
                for (ShoppingCartSkuData skuData : mDatas.get(i).getData()) {
                    if ("1".equals(skuData.getSelected())) {
                        float price = Float.parseFloat(skuData.getTao().getPay_dingjin());
                        hosAllPrice += price;
                    }
                }
            }
        }

        //减去所有医院红包的优惠
//        for (ShoppingCartData data : mDatas) {
//            ShoppingCartSkuAdapter skuIdAdapters = getSkuIdAdapters(data.getHos_id());
//            float hosSelectedPrice = 0;
//            if (skuIdAdapters != null) {
//                //加载出来的计算医院总金额
//                hosSelectedPrice = skuIdAdapters.getHosSelectedPrice();
//            } else {
//                //未加载出来的计算医院总金额
//                for (ShoppingCartSkuData skuData : data.getData()) {
//                    if ("1".equals(skuData.getSelected())) {
//                        float price = Float.parseFloat(skuData.getTao().getPay_dingjin());
//                        hosSelectedPrice += price;
//                    }
//                }
//            }
//
//            //获取最大力度的优惠劵
//            Log.e(TAG, "444 == " + hosSelectedPrice);
//            CartSkuCouponsData[] maxCoupons = getMaxCoupons(hosSelectedPrice, data.getCoupons().getData());
//            if (maxCoupons[0] != null) {
//                hosAllPrice -= Float.parseFloat(maxCoupons[0].getMoney());
//            }
//        }

        return hosAllPrice < 0 ? 0 : hosAllPrice;
    }

    /**
     * 获取所有数据
     *
     * @return
     */
    public List<ShoppingCartData> getmDatas() {
        return mDatas;
    }

    /**
     * 判断购物车中是否有商品的价格是下降的
     *
     * @return true: 有，false：无
     */
    public boolean isPriceFalling() {
        for (ShoppingCartData data : mDatas) {
            List<ShoppingCartSkuData> datas1 = data.getData();
            for (ShoppingCartSkuData data1 : datas1) {
                if (!TextUtils.isEmpty(data1.getTao().getDepreciate())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断购物车中是否有商品库存少于10件
     *
     * @return true: 有，false：无
     */
    public boolean isInventoryNumber() {
        for (ShoppingCartData data : mDatas) {
            List<ShoppingCartSkuData> datas1 = data.getData();
            for (ShoppingCartSkuData data1 : datas1) {
                if (Integer.parseInt(data1.getTao().getKucun_number()) < 10) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断所有SKU是否全部选中
     *
     * @return ：
     */
    public boolean allSelected() {
        boolean allCheck = true;        //默认全部都是选中状态
        //循环判断
        for (ShoppingCartSkuAdapter adapter : skuAdapters.values()) {
            if (!adapter.isAllCheck()) {
                allCheck = false;
                break;
            }
        }

        //item没有全部加载出来
        if (skuAdapters.size() < mDatas.size()) {
            for (int i = skuAdapters.size(); i < mDatas.size(); i++) {
                for (ShoppingCartSkuData data : mDatas.get(i).getData()) {
                    if ("0".equals(data.getSelected())) {
                        allCheck = false;
                        break;
                    }
                }
            }
        }
        return allCheck;
    }

    /**
     * 判断是否有选中的
     *
     * @return ：true:有选中的，false:全部未选中
     */
    public boolean haveSelected() {
        boolean allCheck = false;        //默认全部都是不选中状态
        //循环判断
        for (ShoppingCartSkuAdapter adapter : skuAdapters.values()) {
            if (adapter.isAllNotCheck()) {
                allCheck = true;
                break;
            }
        }

        //item没有全部加载出来
        if (skuAdapters.size() < mDatas.size()) {
            for (int i = skuAdapters.size(); i < mDatas.size(); i++) {
                for (ShoppingCartSkuData data : mDatas.get(i).getData()) {
                    if ("1".equals(data.getSelected())) {
                        allCheck = true;
                        break;
                    }
                }
            }
        }
        return allCheck;
    }

    /**
     * 数据排序,如果有失效商品，放在最后
     *
     * @return ：排序后的数据
     */
    private List<ShoppingCartData> dataSort(List<ShoppingCartData> datas) {
        for (ShoppingCartData data : datas) {
            if ("0".equals(data.getHos_id())) {
                ShoppingCartData typeData = data;
                datas.remove(data);
                datas.add(typeData);
                break;
            }
        }
        return datas;
    }


    /**
     * 是否是选中状态
     *
     * @param isDelete true:删除状态，false:结算状态
     */
    public void cartState(boolean isDelete) {
        if (isDelete) {
            //删除状态
            for (ShoppingCartData cartData : mDatas) {
                HashMap<String, String> list = new HashMap<>();
                for (ShoppingCartSkuData skuData : cartData.getData()) {
                    list.put(skuData.getTao_id(), skuData.getSelected());
                    skuData.setSelected("0");
                }
                selecteSkuList.put(cartData.getHos_id(), list);
            }

        } else {
            //结算状态
            for (ShoppingCartData cartData : mDatas) {
                HashMap<String, String> listData = selecteSkuList.get(cartData.getHos_id());
                for (ShoppingCartSkuData skuData : cartData.getData()) {
                    if (listData != null) {
                        skuData.setSelected(listData.get(skuData.getTao_id()));
                    }
                }
            }
            selecteSkuList.clear();
        }

        notifyDataSetChanged();
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onSelectedClick(boolean allCheck, CartSelectedData data);

        void onDeleteClick(List<CartDeleteData> datas);

        void onNumberClick(CartNumberData data);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
