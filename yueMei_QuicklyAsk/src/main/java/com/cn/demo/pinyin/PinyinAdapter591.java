/**
 * 
 */
package com.cn.demo.pinyin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.module.doctor.model.bean.MainCityData;
import com.quicklyask.activity.R;

import org.pingyin.LanguageComparator_CN;
import org.pingyin.LanguageComparator_EN;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class PinyinAdapter591 extends BaseExpandableListAdapter {

	// 字符串
	private List<MainCityData> strList;

	private AssortPinyinList assort = new AssortPinyinList();
	public static AssortPinyinList assortAA = new AssortPinyinList();

	private Context context;

	private LayoutInflater inflater;
	// 中文排序
	private LanguageComparator_CN cnSort = new LanguageComparator_CN();
	// 英文排序
	private LanguageComparator_EN enSort = new LanguageComparator_EN();

	public PinyinAdapter591(Context context, List<MainCityData> strList) {
		super();
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.strList = strList;
		if (strList == null) {
			strList = new ArrayList<MainCityData>();
		}

		long time = System.currentTimeMillis();
		// 排序
		sort();
		// Toast.makeText(context,
		// String.valueOf(System.currentTimeMillis() - time), 1).show();

	}

	private void sort() {
		// 分类
		for (MainCityData str : strList) {
			assort.getHashList().add(str.getName());
		}

		assort.getHashList().sortKeyComparator(cnSort);
		// assort.getHashList().sortKeyComparator(enSort);

		assortAA = assort;

		for (int i = 0, length = assort.getHashList().size(); i < length; i++) {

			Collections.sort((assort.getHashList().getValueListIndex(i)),
					cnSort);
			// Collections.sort((assort.getHashList().getValueListIndex(i)),
			// enSort);
		}

	}

	public Object getChild(int group, int child) {
		return assort.getHashList().getValueIndex(group, child);
	}

	public long getChildId(int group, int child) {
		return child;
	}

	public View getChildView(int group, int child, boolean arg2,
			View contentView, ViewGroup arg4) {
		if (contentView == null) {
			contentView = inflater.inflate(R.layout.adapter_chat, null);
		}
		TextView textView = contentView.findViewById(R.id.name);

		textView.setText(assort.getHashList().getValueIndex(group, child));
		return contentView;
	}

	public int getChildrenCount(int group) {
		return assort.getHashList().getValueListIndex(group).size();
	}

	public Object getGroup(int group) {
		return assort.getHashList().getValueListIndex(group);
	}

	public int getGroupCount() {
		return assort.getHashList().size();
	}

	public long getGroupId(int group) {
		return group;
	}

	public View getGroupView(int group, boolean arg1, View contentView,
			ViewGroup arg3) {
		if (contentView == null) {
			contentView = inflater.inflate(R.layout.list_group_item, null);
			contentView.setClickable(true);
		}
		TextView textView = contentView.findViewById(R.id.name);

		textView.setText(assort.getFirstChar(assort.getHashList()
				.getValueIndex(group, 0)));
		// 禁止伸展
		return contentView;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

	public AssortPinyinList getAssort() {
		return assort;
	}

}
