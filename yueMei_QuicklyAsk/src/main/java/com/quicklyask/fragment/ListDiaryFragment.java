package com.quicklyask.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.controller.adapter.DiaryListAdapter;
import com.module.home.view.LoadingProgress;
import com.module.other.api.HomeDiarySXApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.DropDownListView2;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import simplecache.ACache;

/**
 * Created by dwb on 16/10/10.
 */
public class ListDiaryFragment extends ListFragment {

    private Activity mCotext;

    private int position;
    private String[] part2Id;
    private String cityId = "0";
    private String cateid = "0";
    private String uid;
    private ACache mCache;

    // List
    private DropDownListView2 mlist;
    private int mCurPage = 1;
    private Handler mHandler;

    private List<BBsListData550> lvBBslistData = new ArrayList<BBsListData550>();
    private List<BBsListData550> lvBBslistMoreData = new ArrayList<BBsListData550>();
    private DiaryListAdapter bbsListAdapter;

    private LinearLayout nodataTv;
    private LoadingProgress mDialog;
    private HomeDiarySXApi homeDiarySXApi;
    private HashMap<String, Object> HomeDiarySXMap = new HashMap<>();

    public static ListDiaryFragment newInstance(int position, String[] partId,
                                               String cityId) {
        ListDiaryFragment f = new ListDiaryFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        b.putStringArray("part2Id", partId);
        b.putString("cityId", cityId);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_diary, container,
                false);
        nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata);
        uid= Utils.getUid();
        if (isAdded()) {
            position = getArguments().getInt("position");
            part2Id = getArguments().getStringArray("part2Id");
            cityId = getArguments().getString("cityId");
            cateid = part2Id[position];
        }

        return v;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);
        mCotext = getActivity();
        mlist = (DropDownListView2) getListView();

        mDialog = new LoadingProgress(getActivity());
        homeDiarySXApi = new HomeDiarySXApi();

        mCache = ACache.get(mCotext);

        initList();
    }

    void initList() {

        mHandler = getHandler();
        mDialog.startLoading();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new DropDownListView2.OnDropDownListener2() {

            @Override
            public void onDropDown() {
                lvBBslistData = null;
                lvBBslistMoreData = null;
                mCurPage = 1;
                mDialog.startLoading();
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {

                if((pos-1)>=0){
                    String url = lvBBslistData.get(pos - 1).getUrl();
                    String qid = lvBBslistData.get(pos - 1).getQ_id();

                    String appmurl = lvBBslistData.get(pos).getAppmurl();
                    WebUrlTypeUtil.getInstance(mCotext).urlToApp(appmurl, "0", "0");
                }
            }
        });
    }

    void lodHotIssueData(final boolean isDonwn) {
        List<BBsListData550> replylistDatas = getCacheData();
        if (replylistDatas != null && replylistDatas.size() > 0) {
            Message msg = null;
            if (isDonwn) {
                if (mCurPage == 1) {
                    lvBBslistData = replylistDatas;
                    msg = mHandler.obtainMessage(1);
                    msg.sendToTarget();
                }
            } else {
                mCurPage++;
                lvBBslistMoreData = replylistDatas;

                msg = mHandler.obtainMessage(2);
                msg.sendToTarget();
            }
        } else {

            HomeDiarySXMap.put("page", mCurPage + "");
            HomeDiarySXMap.put("cateid", cateid);
            HomeDiarySXMap.put("cityId", cityId);
            homeDiarySXApi.getCallBack(mCotext, HomeDiarySXMap, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {
                    mCache.put("diary_json" + cateid + "_" + mCurPage, serverData.data, 60 * 60 * 24);
                    if ("1".equals(serverData.code)) {
                        List<BBsListData550> bBsListData550s = JSONUtil.jsonToArrayList(serverData.data, BBsListData550.class);

                        Message msg = null;
                        if (isDonwn) {
                            if (mCurPage == 1) {

                                lvBBslistData = bBsListData550s;

                                msg = mHandler.obtainMessage(1);
                                msg.sendToTarget();
                            }
                        } else {
                            mCurPage++;
                            lvBBslistMoreData = bBsListData550s;

                            msg = mHandler.obtainMessage(2);
                            msg.sendToTarget();
                        }

                    }
                }
            });
        }
    }

    /**
     * 获取到缓存的数据
     *
     * @return
     */
    private List<BBsListData550> getCacheData() {
        ACache mCache;
        mCache = ACache.get(mCotext);

        if (!SystemTool.checkNet(mCotext)) {
            String homejson = mCache.getAsString("diary_json" + cateid + "_" + mCurPage);
            if (homejson != null) {
                List<BBsListData550> hotData = JSONUtil.jsonToArrayList(homejson, BBsListData550.class);
                return hotData;
            }
        }

        return null;
    }
    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (null!=lvBBslistData&&lvBBslistData.size()>0) {
                            nodataTv.setVisibility(View.GONE);

                            mDialog.stopLoading();
                            mlist.onBottomComplete();

                            if (null != getActivity()) {
                                bbsListAdapter = new DiaryListAdapter(getActivity(), lvBBslistData,"");
                                mlist.setAdapter(bbsListAdapter);
                            }

                            mlist.onDropDownComplete();

                        } else {
                            mDialog.stopLoading();
                            mlist.onBottomComplete();
                            mlist.onDropDownComplete();
                            nodataTv.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        if (null!=lvBBslistMoreData&&lvBBslistMoreData.size()>0) {

                            bbsListAdapter.add(lvBBslistMoreData);
                            bbsListAdapter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {

                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
    }
}
