package com.quicklyask.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by 裴成浩 on 2017/7/20.
 */

public class NetWorkChangeBroadcastReceiver extends BroadcastReceiver {

    public static final String NET_CHANGE = "net_change";
    //标记当前网络状态，0为无可用网络状态，1表示有，2表示有网络是移动网络不是wifi网络
    public static final String NET_TYPE = "net_type";

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager connectivityManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //移动数据
        NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //wifi网络
        NetworkInfo  wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
            //网络状态全部不可用
            Intent netIntent = new Intent(NET_CHANGE);
            netIntent.putExtra(NET_TYPE,0);
            context.sendBroadcast(netIntent);
            return;
        }else {
            if(mobNetInfo.isConnected()&&!wifiNetInfo.isConnected()){
                //手机没有处于wifi网络而是处于移动网络
                Intent netIntent = new Intent(NET_CHANGE);
                netIntent.putExtra(NET_TYPE,2);
                context.sendBroadcast(netIntent);
                return;
            }else {

                if(mobNetInfo.isConnected() || wifiNetInfo.isConnected()){
                    //有网络 wifi网络或者移动网络
                    Intent netIntent = new Intent(NET_CHANGE);
                    netIntent.putExtra(NET_TYPE,1);
                    context.sendBroadcast(netIntent);
                    return;
                }

            }
        }
    }
}