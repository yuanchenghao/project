package com.quicklyask.entity;

/**
 * 搜索结果
 * 
 * @author Rubin
 * 
 */
public class SearchResult {

	private String code;
	private String message;
	private SearchResultData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SearchResultData getData() {
		return data;
	}

	public void setData(SearchResultData data) {
		this.data = data;
	}

}
