package com.quicklyask.entity;

import com.module.community.model.bean.BBsListData550;

import java.util.HashMap;
import java.util.List;

public class DiaryListBean {
    private String title;
    private String jumpUrl;
    private String num;
    private List<BBsListData550> diaryList;
    private HashMap<String,String> event_params;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public List<BBsListData550> getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(List<BBsListData550> diaryList) {
        this.diaryList = diaryList;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
