/**
 * 
 */
package com.quicklyask.entity;

/**
 * 项目首页头部的热门微创
 * 
 * @author Robin
 * 
 */
public class ProjectHot {

	private String oneid;
	private String onetitle;
	private String twoid;
	private String twotitle;
	private String _id;
	private String title;
	private String img;

	/**
	 * @return the oneid
	 */
	public String getOneid() {
		return oneid;
	}

	/**
	 * @param oneid
	 *            the oneid to set
	 */
	public void setOneid(String oneid) {
		this.oneid = oneid;
	}

	/**
	 * @return the onetitle
	 */
	public String getOnetitle() {
		return onetitle;
	}

	/**
	 * @param onetitle
	 *            the onetitle to set
	 */
	public void setOnetitle(String onetitle) {
		this.onetitle = onetitle;
	}

	/**
	 * @return the twoid
	 */
	public String getTwoid() {
		return twoid;
	}

	/**
	 * @param twoid
	 *            the twoid to set
	 */
	public void setTwoid(String twoid) {
		this.twoid = twoid;
	}

	/**
	 * @return the twotitle
	 */
	public String getTwotitle() {
		return twotitle;
	}

	/**
	 * @param twotitle
	 *            the twotitle to set
	 */
	public void setTwotitle(String twotitle) {
		this.twotitle = twotitle;
	}

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img
	 *            the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

}
