package com.quicklyask.entity;


import com.module.doctor.model.bean.GroupDiscData;

import java.util.List;

/**
 * 讨论组一级列表数据
 * 
 * @author Rubin
 * 
 */
public class GroupDisc {
	private String code;
	private String message;
	private List<GroupDiscData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<GroupDiscData> getData() {
		return data;
	}

	public void setData(List<GroupDiscData> data) {
		this.data = data;
	}

}
