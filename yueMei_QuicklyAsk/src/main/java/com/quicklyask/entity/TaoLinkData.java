/**
 * 
 */
package com.quicklyask.entity;

/**
 * 写日记关联淘整形日记数据
 * 
 * @author Robin
 * 
 */
public class TaoLinkData {

	private String price;
	private String title;
	private String docname;
	private String hosname;
	private String _id;
	private String doc_id;
	private String hos_id;
	private String server_id;
	private String sharetime;

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the docname
	 */
	public String getDocname() {
		return docname;
	}

	/**
	 * @param docname
	 *            the docname to set
	 */
	public void setDocname(String docname) {
		this.docname = docname;
	}

	/**
	 * @return the hosname
	 */
	public String getHosname() {
		return hosname;
	}

	/**
	 * @param hosname
	 *            the hosname to set
	 */
	public void setHosname(String hosname) {
		this.hosname = hosname;
	}

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the doc_id
	 */
	public String getDoc_id() {
		return doc_id;
	}

	/**
	 * @param doc_id
	 *            the doc_id to set
	 */
	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}

	/**
	 * @return the hos_id
	 */
	public String getHos_id() {
		return hos_id;
	}

	/**
	 * @param hos_id
	 *            the hos_id to set
	 */
	public void setHos_id(String hos_id) {
		this.hos_id = hos_id;
	}

	/**
	 * @return the server_id
	 */
	public String getServer_id() {
		return server_id;
	}

	/**
	 * @param server_id
	 *            the server_id to set
	 */
	public void setServer_id(String server_id) {
		this.server_id = server_id;
	}

	/**
	 * @return the sharetime
	 */
	public String getSharetime() {
		return sharetime;
	}

	/**
	 * @param sharetime
	 *            the sharetime to set
	 */
	public void setSharetime(String sharetime) {
		this.sharetime = sharetime;
	}


}
