package com.quicklyask.entity;


import com.module.taodetail.model.bean.TaoBoardItem;
import com.module.taodetail.model.bean.TaoTagItem;
import com.module.taodetail.model.bean.TaoZtItem;

import java.util.List;

/**
 * Created by 裴成浩 on 17/8/3.
 */
public class TaoData623 {

    private List<TaoTagItem> tag;
    private List<TaoZtItem> zt;
    private List<TaoBoardItem> board;

    public List<TaoTagItem> getTag() {
        return tag;
    }

    public void setTag(List<TaoTagItem> tag) {
        this.tag = tag;
    }

    public List<TaoZtItem> getZt() {
        return zt;
    }

    public void setZt(List<TaoZtItem> zt) {
        this.zt = zt;
    }

    public List<TaoBoardItem> getBoard() {
        return board;
    }

    public void setBoard(List<TaoBoardItem> board) {
        this.board = board;
    }
}
