package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/9/11.
 */
public class TaIsMember implements Parcelable {
    private String dingjin;
    private String is_order;
    private String hos_price;
    private String payPrice;
    private String discountPrice;
    private String is_member;

    protected TaIsMember(Parcel in) {
        dingjin = in.readString();
        is_order = in.readString();
        hos_price = in.readString();
        payPrice = in.readString();
        discountPrice = in.readString();
        is_member = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dingjin);
        dest.writeString(is_order);
        dest.writeString(hos_price);
        dest.writeString(payPrice);
        dest.writeString(discountPrice);
        dest.writeString(is_member);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaIsMember> CREATOR = new Creator<TaIsMember>() {
        @Override
        public TaIsMember createFromParcel(Parcel in) {
            return new TaIsMember(in);
        }

        @Override
        public TaIsMember[] newArray(int size) {
            return new TaIsMember[size];
        }
    };

    public String getDingjin() {
        return dingjin;
    }

    public void setDingjin(String dingjin) {
        this.dingjin = dingjin;
    }

    public String getIs_order() {
        return is_order;
    }

    public void setIs_order(String is_order) {
        this.is_order = is_order;
    }

    public String getHos_price() {
        return hos_price;
    }

    public void setHos_price(String hos_price) {
        this.hos_price = hos_price;
    }

    public String getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(String payPrice) {
        this.payPrice = payPrice;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }
}
