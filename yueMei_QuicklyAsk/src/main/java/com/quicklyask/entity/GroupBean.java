package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2017/12/15.
 */

public class GroupBean implements Parcelable {
    private String group_id;        //拼团的id
    private String group_user_name; //拼团人昵称
    private String group_user_img;  //拼团人头像连接
    private String group_complete_people;//还差几人成团
    private String group_start_time;    //开始时间
    private String group_end_time;      //结束时间

    protected GroupBean(Parcel in) {
        group_id = in.readString();
        group_user_name = in.readString();
        group_user_img = in.readString();
        group_complete_people = in.readString();
        group_start_time = in.readString();
        group_end_time = in.readString();
    }

    public static final Creator<GroupBean> CREATOR = new Creator<GroupBean>() {
        @Override
        public GroupBean createFromParcel(Parcel in) {
            return new GroupBean(in);
        }

        @Override
        public GroupBean[] newArray(int size) {
            return new GroupBean[size];
        }
    };

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_user_name() {
        return group_user_name;
    }

    public void setGroup_user_name(String group_user_name) {
        this.group_user_name = group_user_name;
    }

    public String getGroup_user_img() {
        return group_user_img;
    }

    public void setGroup_user_img(String group_user_img) {
        this.group_user_img = group_user_img;
    }

    public String getGroup_complete_people() {
        return group_complete_people;
    }

    public void setGroup_complete_people(String group_complete_people) {
        this.group_complete_people = group_complete_people;
    }

    public String getGroup_start_time() {
        return group_start_time;
    }

    public void setGroup_start_time(String group_start_time) {
        this.group_start_time = group_start_time;
    }

    public String getGroup_end_time() {
        return group_end_time;
    }

    public void setGroup_end_time(String group_end_time) {
        this.group_end_time = group_end_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(group_id);
        parcel.writeString(group_user_name);
        parcel.writeString(group_user_img);
        parcel.writeString(group_complete_people);
        parcel.writeString(group_start_time);
        parcel.writeString(group_end_time);
    }
}
