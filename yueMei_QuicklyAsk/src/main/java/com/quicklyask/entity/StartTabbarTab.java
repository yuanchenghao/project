package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/9/27.
 */
public class StartTabbarTab implements Parcelable{
    private String selected_img_2x;
    private String selected_img_3x;
    private String unselected_img_2x;
    private String unselected_img_3x;
    private String title;

    protected StartTabbarTab(Parcel in) {
        selected_img_2x = in.readString();
        selected_img_3x = in.readString();
        unselected_img_2x = in.readString();
        unselected_img_3x = in.readString();
        title = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(selected_img_2x);
        dest.writeString(selected_img_3x);
        dest.writeString(unselected_img_2x);
        dest.writeString(unselected_img_3x);
        dest.writeString(title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StartTabbarTab> CREATOR = new Creator<StartTabbarTab>() {
        @Override
        public StartTabbarTab createFromParcel(Parcel in) {
            return new StartTabbarTab(in);
        }

        @Override
        public StartTabbarTab[] newArray(int size) {
            return new StartTabbarTab[size];
        }
    };

    public String getSelected_img_2x() {
        return selected_img_2x;
    }

    public void setSelected_img_2x(String selected_img_2x) {
        this.selected_img_2x = selected_img_2x;
    }

    public String getSelected_img_3x() {
        return selected_img_3x;
    }

    public void setSelected_img_3x(String selected_img_3x) {
        this.selected_img_3x = selected_img_3x;
    }

    public String getUnselected_img_2x() {
        return unselected_img_2x;
    }

    public void setUnselected_img_2x(String unselected_img_2x) {
        this.unselected_img_2x = unselected_img_2x;
    }

    public String getUnselected_img_3x() {
        return unselected_img_3x;
    }

    public void setUnselected_img_3x(String unselected_img_3x) {
        this.unselected_img_3x = unselected_img_3x;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
