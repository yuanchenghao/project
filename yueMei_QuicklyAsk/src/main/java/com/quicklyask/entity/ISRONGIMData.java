package com.quicklyask.entity;

public class ISRONGIMData {

	private String is_rongyun;
	private String hos_userid;

	public String getIs_rongyun() {
		return is_rongyun;
	}

	public void setIs_rongyun(String is_rongyun) {
		this.is_rongyun = is_rongyun;
	}

	public String getHos_userid() {
		return hos_userid;
	}

	public void setHos_userid(String hos_userid) {
		this.hos_userid = hos_userid;
	}

}
