package com.quicklyask.entity;

public class ISRONGIM {

	private String code;
	private String message;
	private ISRONGIMData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ISRONGIMData getData() {
		return data;
	}

	public void setData(ISRONGIMData data) {
		this.data = data;
	}

}
