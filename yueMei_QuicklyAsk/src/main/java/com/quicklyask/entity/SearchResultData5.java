/**
 * 
 */
package com.quicklyask.entity;


import com.module.doctor.model.bean.HosListData;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class SearchResultData5 {

	private List<HosListData> list;
	private String search_hit_board_id;

	/**
	 * @return the list
	 */
	public List<HosListData> getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<HosListData> list) {
		this.list = list;
	}

	public String getSearch_hit_board_id() {
		return search_hit_board_id;
	}

	public void setSearch_hit_board_id(String search_hit_board_id) {
		this.search_hit_board_id = search_hit_board_id;
	}
}
