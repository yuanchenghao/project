package com.quicklyask.entity;

public class GuiJiBean {

    /**
     * title : 私信送小气泡/脱毛+皮肤检测 线雕面部提升提拉紧致 单部位埋线提升 抗衰除皱
     * image : https://p34.yuemei.com/tao/2019/0430/200_200/jt190430151833_9cbc27.jpg
     * url : https://m.yuemei.com/tao/191278/
     * price : 1980
     * member_price : -1
     */

    private String title;
    private String image;
    private String url;
    private String price;
    private String member_price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }
}
