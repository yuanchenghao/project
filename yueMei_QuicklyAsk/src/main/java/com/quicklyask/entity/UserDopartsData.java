package com.quicklyask.entity;

/**
 * Created by 裴成浩 on 2018/4/23.
 */

public class UserDopartsData {
    private String id;
    private String name;
    private String checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
