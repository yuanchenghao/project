package com.quicklyask.entity;

import java.util.HashMap;

public class AlertCouponsBean {

    /**
     * coupons_id : 0
     * money : 0
     * limit_money : 0
     */

    private int coupons_id;
    private int money;
    private int limit_money;
    private HashMap<String,String> event_params;

    public int getCoupons_id() {
        return coupons_id;
    }

    public void setCoupons_id(int coupons_id) {
        this.coupons_id = coupons_id;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getLimit_money() {
        return limit_money;
    }

    public void setLimit_money(int limit_money) {
        this.limit_money = limit_money;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
