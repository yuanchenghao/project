package com.quicklyask.entity;

/**
 * Created by 裴成浩 on 2017/7/6.
 */

public class WriteVideoResult {


    /**
     * code : 200
     * file_size : 5688157
     * url : /201707/4f534bc53fadf7823f00d865161256ef.mp4
     * time : 1499337903
     * message : ok
     * mimetype : video/mp4
     */

    private int code;
    private int file_size;
    private String url;
    private int time;
    private String message;
    private String mimetype;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getFile_size() {
        return file_size;
    }

    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    @Override
    public String toString() {
        return "WriteVideoResult{" +
                "code=" + code +
                ", file_size=" + file_size +
                ", url='" + url + '\'' +
                ", time=" + time +
                ", message='" + message + '\'' +
                ", mimetype='" + mimetype + '\'' +
                '}';
    }
}
