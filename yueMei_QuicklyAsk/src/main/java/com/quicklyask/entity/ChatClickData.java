package com.quicklyask.entity;

import java.util.HashMap;

public class ChatClickData {

    private String ymaq_class;
    private String ymaq_id;
    private String is_rongyun;
    private String hos_userid;
    private String event_name;
    private String event_pos;
    private HashMap<String,String> event_params;
    private String show_message;
    private GuiJiBean guiji;
    private String obj_type;
    private String obj_id;



    public String getYmaq_class() {
        return ymaq_class;
    }

    public void setYmaq_class(String ymaq_class) {
        this.ymaq_class = ymaq_class;
    }

    public String getYmaq_id() {
        return ymaq_id;
    }

    public void setYmaq_id(String ymaq_id) {
        this.ymaq_id = ymaq_id;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(String event_pos) {
        this.event_pos = event_pos;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getShow_message() {
        return show_message;
    }

    public void setShow_message(String show_message) {
        this.show_message = show_message;
    }

    public GuiJiBean getGuiji() {
        return guiji;
    }

    public void setGuiji(GuiJiBean guiji) {
        this.guiji = guiji;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }
}
