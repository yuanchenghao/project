package com.quicklyask.entity;

import com.module.community.model.bean.ZhuanTi;
import com.module.my.model.bean.RecoveryReminder;

/**
 * Created by dwb on 16/6/30.
 */
public class AdertAdv {

    private ZhuanTi alert;
    private ZhuanTi adv;
    private StartTabbar tabbar;
    private SneakGuest sneakGuest;
    private String foramSetType;
    private String loadDoctorHtml;
    private RecoveryReminder recoveryReminder;


    public ZhuanTi getAlert() {
        return alert;
    }

    public void setAlert(ZhuanTi alert) {
        this.alert = alert;
    }

    public ZhuanTi getAdv() {
        return adv;
    }

    public void setAdv(ZhuanTi adv) {
        this.adv = adv;
    }

    public StartTabbar getTabbar() {
        return tabbar;
    }

    public void setTabbar(StartTabbar tabbar) {
        this.tabbar = tabbar;
    }

    public SneakGuest getSneakGuest() {
        return sneakGuest;
    }

    public void setSneakGuest(SneakGuest sneakGuest) {
        this.sneakGuest = sneakGuest;
    }

    public String getForamSetType() {
        return foramSetType;
    }

    public void setForamSetType(String foramSetType) {
        this.foramSetType = foramSetType;
    }

    public String getLoadDoctorHtml() {
        return loadDoctorHtml;
    }

    public void setLoadDoctorHtml(String loadDoctorHtml) {
        this.loadDoctorHtml = loadDoctorHtml;
    }

    public RecoveryReminder getRecoveryReminder() {
        return recoveryReminder;
    }

    public void setRecoveryReminder(RecoveryReminder recoveryReminder) {
        this.recoveryReminder = recoveryReminder;
    }
}
