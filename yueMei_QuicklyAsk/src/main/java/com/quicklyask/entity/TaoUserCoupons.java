package com.quicklyask.entity;

public class TaoUserCoupons {

    /**
     * limit_money : 5999
     * money : 5999
     * endtime : null
     * coupons_type : 1
     */

    private String limit_money;
    private String money;
    private String endtime;
    private int coupons_type;

    public String getLimit_money() {
        return limit_money;
    }

    public void setLimit_money(String limit_money) {
        this.limit_money = limit_money;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getCoupons_type() {
        return coupons_type;
    }

    public void setCoupons_type(int coupons_type) {
        this.coupons_type = coupons_type;
    }
}
