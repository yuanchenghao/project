/**
 * 
 */
package com.quicklyask.entity;


import com.module.my.model.bean.BaikeItemData;

import java.util.List;

/**
 * 收藏的百科列表
 * 
 * @author Robin
 * 
 */
public class BaikeItem {

	private String code;
	private String message;
	private List<BaikeItemData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<BaikeItemData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<BaikeItemData> data) {
		this.data = data;
	}

}
