package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/9/27.
 */
public class StartTabbar implements Parcelable {
    private StartTabbarBgimage bgimage;
    private String selected_color;
    private String unselected_color;
    private List<StartTabbarTab> tab;

    protected StartTabbar(Parcel in) {
        bgimage = in.readParcelable(StartTabbarBgimage.class.getClassLoader());
        selected_color = in.readString();
        unselected_color = in.readString();
        tab = in.createTypedArrayList(StartTabbarTab.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(bgimage, flags);
        dest.writeString(selected_color);
        dest.writeString(unselected_color);
        dest.writeTypedList(tab);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StartTabbar> CREATOR = new Creator<StartTabbar>() {
        @Override
        public StartTabbar createFromParcel(Parcel in) {
            return new StartTabbar(in);
        }

        @Override
        public StartTabbar[] newArray(int size) {
            return new StartTabbar[size];
        }
    };

    public StartTabbarBgimage getBgimage() {
        return bgimage;
    }

    public void setBgimage(StartTabbarBgimage bgimage) {
        this.bgimage = bgimage;
    }

    public String getSelected_color() {
        return selected_color;
    }

    public void setSelected_color(String selected_color) {
        this.selected_color = selected_color;
    }

    public String getUnselected_color() {
        return unselected_color;
    }

    public void setUnselected_color(String unselected_color) {
        this.unselected_color = unselected_color;
    }

    public List<StartTabbarTab> getTab() {
        return tab;
    }

    public void setTab(List<StartTabbarTab> tab) {
        this.tab = tab;
    }
}
