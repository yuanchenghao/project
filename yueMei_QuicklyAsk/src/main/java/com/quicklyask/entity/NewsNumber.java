package com.quicklyask.entity;

public class NewsNumber {

	private String code;
	private String message;
	private NewsNumberData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public NewsNumberData getData() {
		return data;
	}

	public void setData(NewsNumberData data) {
		this.data = data;
	}


}
