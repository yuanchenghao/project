package com.quicklyask.entity;

public class SearchResult4 {
	private String code;
	private String message;
	private SearchResultData4 data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SearchResultData4 getData() {
		return data;
	}

	public void setData(SearchResultData4 data) {
		this.data = data;
	}

}
