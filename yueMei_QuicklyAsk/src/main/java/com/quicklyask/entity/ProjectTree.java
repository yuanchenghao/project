/**
 * 
 */
package com.quicklyask.entity;

import java.util.List;

/**
 * 项目详情级联菜单
 * 
 * @author Robin
 * 
 */
public class ProjectTree {

	private String code;
	private String message;
	private List<ProjectTwoTree> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<ProjectTwoTree> getData() {
		return data;
	}

	public void setData(List<ProjectTwoTree> data) {
		this.data = data;
	}
}
