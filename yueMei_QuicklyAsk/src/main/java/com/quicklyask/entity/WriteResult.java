package com.quicklyask.entity;

/**
 * 发帖 体验 返回的数据
 * 
 * @author Rubin
 * 
 */
public class WriteResult {
	private String code;
	private String message;
	private WriteResultData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public WriteResultData getData() {
		return data;
	}

	public void setData(WriteResultData data) {
		this.data = data;
	}


}
