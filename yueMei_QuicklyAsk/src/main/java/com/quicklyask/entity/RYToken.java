/**
 * 
 */
package com.quicklyask.entity;

/**
 * @author lenovo17
 * 
 */
public class RYToken {
	private String code;
	private String message;
	private RYTokenData data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public RYTokenData getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(RYTokenData data) {
		this.data = data;
	}


}
