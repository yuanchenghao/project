package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/9/11.
 */
public class TaoMemberData implements Parcelable {
    private String member_price;
    private String user_is_member;
    private String desc;
    private String frist_lijian_price;
    private String is_show_member;
    private String member_dingjin;
    private String member_hos_price;

    protected TaoMemberData(Parcel in) {
        member_price = in.readString();
        user_is_member = in.readString();
        desc = in.readString();
        frist_lijian_price = in.readString();
        is_show_member = in.readString();
        member_dingjin = in.readString();
        member_hos_price = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(member_price);
        dest.writeString(user_is_member);
        dest.writeString(desc);
        dest.writeString(frist_lijian_price);
        dest.writeString(is_show_member);
        dest.writeString(member_dingjin);
        dest.writeString(member_hos_price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaoMemberData> CREATOR = new Creator<TaoMemberData>() {
        @Override
        public TaoMemberData createFromParcel(Parcel in) {
            return new TaoMemberData(in);
        }

        @Override
        public TaoMemberData[] newArray(int size) {
            return new TaoMemberData[size];
        }
    };

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public String getUser_is_member() {
        return user_is_member;
    }

    public void setUser_is_member(String user_is_member) {
        this.user_is_member = user_is_member;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFrist_lijian_price() {
        return frist_lijian_price;
    }

    public void setFrist_lijian_price(String frist_lijian_price) {
        this.frist_lijian_price = frist_lijian_price;
    }

    public String getIs_show_member() {
        return is_show_member;
    }

    public void setIs_show_member(String is_show_member) {
        this.is_show_member = is_show_member;
    }

    public String getMember_dingjin() {
        return member_dingjin;
    }

    public void setMember_dingjin(String member_dingjin) {
        this.member_dingjin = member_dingjin;
    }

    public String getMember_hos_price() {
        return member_hos_price;
    }

    public void setMember_hos_price(String member_hos_price) {
        this.member_hos_price = member_hos_price;
    }
}
