/**
 * 
 */
package com.quicklyask.entity;


import com.module.commonview.module.bean.ProjectFourTree;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class ProjectTwoTree {
	private String _id;
	private String title;
	private List<ProjectFourTree> list;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ProjectFourTree> getList() {
		return list;
	}

	public void setList(List<ProjectFourTree> list) {
		this.list = list;
	}
}
