package com.quicklyask.entity;

/**
 * 淘整形收藏
 *
 * @author Rubin
 */
public class TaoCollectData {

    private String seckilling;  // 全民疯抢 1
    private String img;         //	图片
    private String title;       //	标题
    private String subtitle;    //	副标题
    private String hos_name;    //	医院名
    private String doc_name;    //	医生名
    private String price;       //	医院价
    private String price_discount;  //	悦美价
    private String price_range_max; //	最大悦美价
    private String _id;             //	淘Id

    private String showprice;       //	是否显示医院价 1显示
    private String specialPrice;    //是否是超值特卖 1是
    private String show_hospital;   //	是否显示医院
    private String lijian;          //	优惠立减
    private String invitation;      // 	是否是特邀 1是
    private String baoxian;         //是否有保险 1有

    private String img66;           //	是否有大促logo 1有

    private String repayment;       //	是否 有分期 1有
    private String hos_red_packet;  //医院红包


    private String shixiao;

    private String newp;

    private String hot;

    private String mingyi;

    private String rate;            //评分

    private String feeScale;        //	计费方式
    private String member_price;
    private String depreciate;      //比收藏时降价N元

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getShixiao() {
        return shixiao;
    }

    public void setShixiao(String shixiao) {
        this.shixiao = shixiao;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getNewp() {
        return newp;
    }

    public void setNewp(String newp) {
        this.newp = newp;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getMingyi() {
        return mingyi;
    }

    public void setMingyi(String mingyi) {
        this.mingyi = mingyi;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getImg66() {
        return img66;
    }

    public void setImg66(String img66) {
        this.img66 = img66;
    }

    public String getBaoxian() {
        return baoxian;
    }

    public void setBaoxian(String baoxian) {
        this.baoxian = baoxian;
    }

    public String getDepreciate() {
        return depreciate;
    }

    public void setDepreciate(String depreciate) {
        this.depreciate = depreciate;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getSeckilling() {
        return seckilling;
    }

    public void setSeckilling(String seckilling) {
        this.seckilling = seckilling;
    }

    public String getPrice_range_max() {
        return price_range_max;
    }

    public void setPrice_range_max(String price_range_max) {
        this.price_range_max = price_range_max;
    }

    public String getShowprice() {
        return showprice;
    }

    public void setShowprice(String showprice) {
        this.showprice = showprice;
    }

    public String getShow_hospital() {
        return show_hospital;
    }

    public void setShow_hospital(String show_hospital) {
        this.show_hospital = show_hospital;
    }

    public String getLijian() {
        return lijian;
    }

    public void setLijian(String lijian) {
        this.lijian = lijian;
    }

    public String getInvitation() {
        return invitation;
    }

    public void setInvitation(String invitation) {
        this.invitation = invitation;
    }

    public String getHos_red_packet() {
        return hos_red_packet;
    }

    public void setHos_red_packet(String hos_red_packet) {
        this.hos_red_packet = hos_red_packet;
    }

    public String getFeeScale() {
        return feeScale;
    }

    public void setFeeScale(String feeScale) {
        this.feeScale = feeScale;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }
}
