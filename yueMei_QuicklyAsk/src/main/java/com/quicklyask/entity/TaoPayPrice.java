package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/9/11.
 */
public class TaoPayPrice implements Parcelable {
    private String dingjin;                 //订金价格
    private String is_order;                //通过悦美预定 (2全款、订金；3订金;4全款)
    private String hos_price;               //到院再支付金额
    private String payPrice;                //全款价
    private String discountPrice;           //	专享，在线支付优惠的价格
    private String is_member;               //是否是会员下单

    protected TaoPayPrice(Parcel in) {
        dingjin = in.readString();
        is_order = in.readString();
        hos_price = in.readString();
        payPrice = in.readString();
        discountPrice = in.readString();
        is_member = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dingjin);
        dest.writeString(is_order);
        dest.writeString(hos_price);
        dest.writeString(payPrice);
        dest.writeString(discountPrice);
        dest.writeString(is_member);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaoPayPrice> CREATOR = new Creator<TaoPayPrice>() {
        @Override
        public TaoPayPrice createFromParcel(Parcel in) {
            return new TaoPayPrice(in);
        }

        @Override
        public TaoPayPrice[] newArray(int size) {
            return new TaoPayPrice[size];
        }
    };

    public String getDingjin() {
        return dingjin;
    }

    public void setDingjin(String dingjin) {
        this.dingjin = dingjin;
    }

    public String getIs_order() {
        return is_order;
    }

    public void setIs_order(String is_order) {
        this.is_order = is_order;
    }

    public String getHos_price() {
        return hos_price;
    }

    public void setHos_price(String hos_price) {
        this.hos_price = hos_price;
    }

    public String getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(String payPrice) {
        this.payPrice = payPrice;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }
}
