package com.quicklyask.entity;

public class SearchResult23 {

	private String code;
	private String message;
	private SearchResultData23 data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SearchResultData23 getData() {
		return data;
	}

	public void setData(SearchResultData23 data) {
		this.data = data;
	}


}
