package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class BudgetBean implements Parcelable {


    /**
     * id : 1
     * name : 1千以下
     * event_params : {"event_name":"cold_start_budget","type":0,"event_pos":1}
     */

    private int id;
    private String name;
    private HashMap<String,String> event_params;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String,String> event_params) {
        this.event_params = event_params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeSerializable(this.event_params);
    }

    public BudgetBean() {
    }

    protected BudgetBean(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.event_params = (HashMap) in.readSerializable();
    }

    public static final Parcelable.Creator<BudgetBean> CREATOR = new Parcelable.Creator<BudgetBean>() {
        @Override
        public BudgetBean createFromParcel(Parcel source) {
            return new BudgetBean(source);
        }

        @Override
        public BudgetBean[] newArray(int size) {
            return new BudgetBean[size];
        }
    };
}
