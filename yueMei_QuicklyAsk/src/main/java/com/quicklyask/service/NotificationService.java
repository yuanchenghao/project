package com.quicklyask.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import com.module.commonview.broadcast.SignInformReceiver;

import java.util.List;

/**
 * 本地推送
 * Created by 裴成浩 on 2017/6/5.
 */

public class NotificationService extends Service {

    private static final int CHECK_TICK = 1 * 60 * 1000;

    private NotifyThread m_notifyThread = null;

    private static String TAG = "NotificationService";
    private SignInformReceiver mSignInformReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //创建新的线程
        m_notifyThread = new NotifyThread();
        m_notifyThread.start();

        Log.e(TAG, " onCreate...");
        IntentFilter intentFilter = new IntentFilter("com.example.mybroadcast.MY_SIGNINFORM");
        mSignInformReceiver = new SignInformReceiver();
        registerReceiver(mSignInformReceiver,intentFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, Service.START_REDELIVER_INTENT);
    }

    public static boolean isRunning(Context context) {
        ActivityManager activityMgr = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityMgr != null) {
            List<ActivityManager.RunningServiceInfo> serviceList = activityMgr.getRunningServices(50);

            if (serviceList.isEmpty()) {
                return false;
            }
            for (int i = 0; i <serviceList.size() ; i++) {
                String serviceName = serviceList.get(i).service.getClassName();
                if ("com.quicklyask.service.NotificationService".equals(serviceName)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onDestroy() {

        if (m_notifyThread != null) {
            m_notifyThread.stopThread();
        }
        if (mSignInformReceiver != null){
            unregisterReceiver(mSignInformReceiver);
        }
        super.onDestroy();
    }

    //自定义线程
    private class NotifyThread extends Thread {
        private boolean m_bStop = false;

        public synchronized void stopThread() {
            m_bStop = true;
        }

        @Override
        public void run() {
            while (!m_bStop) {
                Intent intent = new Intent();
                intent.setAction("com.example.mybroadcast.MY_SIGNINFORM");
                sendBroadcast(intent);
                Log.e(TAG, " sendBroadcast............");
                try {
                    sleep(CHECK_TICK);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}