package com.quicklyask.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.ScrollViewScrolCallBack;

/**
 * Created by 裴成浩 on 2017/10/16.
 */

public class ElasticLinearLayout extends RelativeLayout {
    private final static int RELEASE_To_REFRESH = 0;
    private final static int PULL_To_REFRESH = 1;
    private final static int REFRESHING = 2;
    private final static int DONE = 3;
    private final static int LOADING = 4;

    private final static int RATIO = 3;

    private int headContentWidth;
    private int headContentHeight;

    private LinearLayout innerLayout;
    private LinearLayout headView;
    private ImageView arrowImageView;
    private ProgressBar progressBar;
    private TextView tipsTextview;
    private TextView lastUpdatedTextView;
    private ElasticScrollView.OnRefreshListener refreshListener;
    private boolean isRefreshable;
    private int state;
    private boolean isBack;

    private RotateAnimation animation;
    private RotateAnimation reverseAnimation;

    private boolean canReturn;
    private boolean isRecored;
    private int startY;
    private LayoutInflater inflater;

    public static int loac_scrol;

    private ScrollViewScrolCallBack mOnScrollChangedCallback;

    private ImageView sxIv;
    private TextView sxTv;
    private AnimationDrawable animationDrawable;

    public ElasticLinearLayout(Context context) {
        super(context);
        inflater = LayoutInflater.from(context);
    }

    public ElasticLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = LayoutInflater.from(context);
    }

    public void GetLinearLayout(LinearLayout f_LinearLayout) {

        init(f_LinearLayout);
    }



    private void init(LinearLayout f_LinearLayout) {
        innerLayout = f_LinearLayout;

        headView = (LinearLayout) inflater.inflate(R.layout.mylistview_head2, null);

        arrowImageView = headView
                .findViewById(R.id.head_arrowImageView);
        progressBar = headView
                .findViewById(R.id.head_progressBar);
        tipsTextview = headView.findViewById(R.id.head_tipsTextView);
        lastUpdatedTextView = headView
                .findViewById(R.id.head_lastUpdatedTextView);

        sxIv = headView.findViewById(R.id.sx_iv);
        sxTv = headView.findViewById(R.id.sx_tv);

        // Log.d("888888888", lastUpdatedTextView.toString());

        measureView(headView);

        headContentHeight = headView.getMeasuredHeight();

        headContentWidth = headView.getMeasuredWidth();
        headView.setPadding(0, -1 * headContentHeight, 0, 0);
        headView.invalidate();

        innerLayout.addView(headView);

        animation = new RotateAnimation(0, -180,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(250);
        animation.setFillAfter(true);

        reverseAnimation = new RotateAnimation(-180, 0,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        reverseAnimation.setInterpolator(new LinearInterpolator());
        reverseAnimation.setDuration(200);
        reverseAnimation.setFillAfter(true);

        state = DONE;
        isRefreshable = false;
        canReturn = false;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isRefreshable) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (getScrollY() == 0 && !isRecored) {
                        isRecored = true;
                        startY = (int) event.getY();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (state != REFRESHING && state != LOADING) {
                        if (state == DONE) {

                        }
                        if (state == PULL_To_REFRESH) {
                            state = DONE;
                            changeHeaderViewByState();
                        }
                        if (state == RELEASE_To_REFRESH) {
                            state = REFRESHING;
                            changeHeaderViewByState();
                            onRefresh();
                        }
                    }
                    isRecored = false;
                    isBack = false;

                    break;
                case MotionEvent.ACTION_MOVE:
                    int tempY = (int) event.getY();
                    if (!isRecored && getScrollY() == 0) {

                        isRecored = true;
                        startY = tempY;
                    }

                    if (state != REFRESHING && isRecored && state != LOADING) {

                        if (state == RELEASE_To_REFRESH) {
                            canReturn = true;

                            if (((tempY - startY) / RATIO < headContentHeight)
                                    && (tempY - startY) > 0) {
                                state = PULL_To_REFRESH;
                                changeHeaderViewByState();
                            } else if (tempY - startY <= 0) {
                                state = DONE;
                                changeHeaderViewByState();
                            } else {

                            }
                        }
                        if (state == PULL_To_REFRESH) {
                            canReturn = true;

                            if ((tempY - startY) / RATIO >= headContentHeight) {
                                state = RELEASE_To_REFRESH;
                                isBack = true;
                                changeHeaderViewByState();
                            } else if (tempY - startY <= 0) {
                                state = DONE;
                                changeHeaderViewByState();
                            }
                        }

                        if (state == DONE) {
                            if (tempY - startY > 0) {
                                state = PULL_To_REFRESH;
                                changeHeaderViewByState();
                            }
                        }

                        if (state == PULL_To_REFRESH) {
                            headView.setPadding(0, -1 * headContentHeight
                                    + (tempY - startY) / RATIO, 0, 0);

                        }

                        if (state == RELEASE_To_REFRESH) {
                            headView.setPadding(0, (tempY - startY) / RATIO
                                    - headContentHeight, 0, 0);
                        }
                        if (canReturn) {
                            canReturn = false;
                            return true;
                        }
                    }
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    //
    private void changeHeaderViewByState() {
        switch (state) {
            case RELEASE_To_REFRESH:
                arrowImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                tipsTextview.setVisibility(View.VISIBLE);
                lastUpdatedTextView.setVisibility(View.VISIBLE);

                arrowImageView.clearAnimation();
                arrowImageView.startAnimation(animation);

                tipsTextview.setText("松开可以更新");

                sxTv.setText("释放刷新");
                break;
            case PULL_To_REFRESH:
                progressBar.setVisibility(View.GONE);
                tipsTextview.setVisibility(View.VISIBLE);
                lastUpdatedTextView.setVisibility(View.VISIBLE);
                arrowImageView.clearAnimation();
                arrowImageView.setVisibility(View.VISIBLE);

                if (isBack) {
                    isBack = false;
                    arrowImageView.clearAnimation();
                    arrowImageView.startAnimation(reverseAnimation);

                    tipsTextview.setText("下拉可以更新");
                } else {
                    tipsTextview.setText("下拉可以更新");
                }

                sxTv.setText("下拉刷新");
                sxIv.clearAnimation();
                sxIv.setImageResource(R.drawable.sx_three_mao_start);
                animationDrawable = (AnimationDrawable) sxIv.getDrawable();
                animationDrawable.start();

                break;

            case REFRESHING:

                headView.setPadding(0, 0, 0, 0);

                progressBar.setVisibility(View.VISIBLE);
                arrowImageView.clearAnimation();
                arrowImageView.setVisibility(View.GONE);
                tipsTextview.setText("正在更新...");
                lastUpdatedTextView.setVisibility(View.VISIBLE);

                sxTv.setText("正在刷新");
                sxIv.clearAnimation();
                sxIv.setImageResource(R.drawable.sx_three_mao);
                animationDrawable = (AnimationDrawable) sxIv.getDrawable();
                animationDrawable.start();
                startAction();

                break;
            case DONE:
                headView.setPadding(0, -1 * headContentHeight, 0, 0);

                progressBar.setVisibility(View.GONE);
                arrowImageView.clearAnimation();
                arrowImageView.setImageResource(R.drawable.goicon);
                tipsTextview.setText("下拉可以更新");
                lastUpdatedTextView.setVisibility(View.VISIBLE);

                break;
        }
    }

    @SuppressWarnings("deprecation")
    private void measureView(View child) {
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
        int lpHeight = p.height;
        int childHeightSpec;
        if (lpHeight > 0) {
            childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight,
                    MeasureSpec.EXACTLY);
        } else {
            childHeightSpec = MeasureSpec.makeMeasureSpec(0,
                    MeasureSpec.UNSPECIFIED);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public void setonRefreshListener(ElasticScrollView.OnRefreshListener refreshListener) {
        this.refreshListener = refreshListener;
        isRefreshable = true;
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    public void onRefreshComplete() {
        state = DONE;
        // Log.d("999999999999999999", lastUpdatedTextView.toString());
        // lastUpdatedTextView.setText("更新时间：" + new Date().toLocaleString());
        changeHeaderViewByState();
        invalidate();
        scrollTo(0, 0);
    }

    private void onRefresh() {
        if (refreshListener != null) {
            new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    refreshListener.onRefresh();
                }
            }.start();
        }
    }

    public void addChild(View child) {
        innerLayout.addView(child);
    }

    public void addChild(View child, int position) {
        innerLayout.addView(child, position);
    }

    // du_add
    public void startAction() {

    }

    public void setOnScrollChangedCallback(
            final ScrollViewScrolCallBack onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        loac_scrol = t;
        // Log.e("onScrollChanged", "onScrollChanged==||t==" + t);
        if (mOnScrollChangedCallback != null)
            mOnScrollChangedCallback.scrolChanged(l, t);
    }
}
