package com.quicklyask.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.PopupWindow;

import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.ViewInject;

/**
 * Created by dwb on 17/2/20.
 */

public class GetPhoneCodePopWindow extends PopupWindow {

    private Context mContext;
    private Button cancleBt;
    private Button callPhoneBt;

    public GetPhoneCodePopWindow(final Context mContext) {
        final View view = View.inflate(mContext, R.layout.pop_getphonecode,
                null);
        view.startAnimation(AnimationUtils.loadAnimation(mContext,
                R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();
        this.mContext=mContext;

        cancleBt= view.findViewById(R.id.cancel_bt);
        callPhoneBt= view.findViewById(R.id.zixun_bt);

        cancleBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();

            }
        });

        callPhoneBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewInject.toast("正在拨打中·····");
                Intent it = new Intent(Intent.ACTION_CALL, Uri
                        .parse("tel:" + "4000567118"));
                try {
                    mContext.startActivity(it);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dismiss();
            }
        });

    }
}
