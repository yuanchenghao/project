package com.quicklyask.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.my.controller.activity.PostingMessageActivity;
import com.module.my.controller.activity.SelectNoteActivity;
import com.module.my.model.bean.NoteBookListData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/8/28
 */
public class NoteTipsDialog extends Dialog {

    private final Context mContext;

    private RelativeLayout colsely;
    private Button popGoonBt;
    private NoteBookListData noteData;
    private NoteBookListData nnn;
    private String type;
    private PageJumpManager pageJumpManager;

    private String full_name = "";
    private String mobile = "";

    public NoteTipsDialog(Context context, NoteBookListData noteData, String type, NoteBookListData nnn) {
        super(context, R.style.mystyle);
        this.mContext = context;
        this.noteData = noteData;
        this.type = type;
        this.nnn = nnn;

        pageJumpManager = new PageJumpManager((Activity) mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pop_note_tips);
        setCanceledOnTouchOutside(false);

        colsely = findViewById(R.id.pop_close_rly);
        popGoonBt = findViewById(R.id.zixun_bt);

        popGoonBt.setText("写日记");

        initMthod();
    }

    private void initMthod() {

        colsely.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        popGoonBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("1".equals(type)) {//继续

                    if (null != nnn) {

                        Log.e("SelectNoteActivity", "111111");
                        Intent it = new Intent();
                        it.setClass(mContext, SelectNoteActivity.class);
                        it.putExtra("cateid", "1090");
                        it.putExtra("userid", nnn.getDoc_id());
                        it.putExtra("hosid", nnn.getHos_id());
                        it.putExtra("hosname", nnn.getHosname());
                        it.putExtra("docname", nnn.getDocname());
                        it.putExtra("fee", nnn.getPrice());
                        it.putExtra("taoid", nnn.get_id());
                        it.putExtra("server_id", nnn.getServer_id());
                        mContext.startActivity(it);

                        dismiss();

                    } else {
                        Log.e("SelectNoteActivity", "2222222");
                        Intent it = new Intent();
                        it.setClass(mContext, SelectNoteActivity.class);
                        it.putExtra("cateid", "1090");
                        it.putExtra("userid", "0");
                        it.putExtra("hosid", "0");
                        it.putExtra("hosname", "");
                        it.putExtra("docname", "");
                        it.putExtra("fee", "");
                        it.putExtra("taoid", "0");
                        it.putExtra("server_id", "0");
                        mContext.startActivity(it);

                        dismiss();
                    }
                } else {//写日记
                    if (null != noteData) {

                        getUserData();

                    } else {
                        Log.e("SelectNoteActivity", "33333");
                        Intent it = new Intent();
                        it.setClass(mContext, SelectNoteActivity.class);
                        it.putExtra("cateid", "1090");
                        it.putExtra("userid", "0");
                        it.putExtra("hosid", "0");
                        it.putExtra("hosname", "");
                        it.putExtra("docname", "");
                        it.putExtra("fee", "");
                        it.putExtra("taoid", "0");
                        it.putExtra("server_id", "0");
                        mContext.startActivity(it);

                        dismiss();
                    }

                }
            }
        });
    }

    /**
     * 获取发帖人信息页
     */
    private void getUserData() {
        Map<String, Object> keyValues = new HashMap<>();

        keyValues.put("id", Utils.getUid());
        new BBsDetailUserInfoApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    full_name = userData.getFull_name();
                    mobile = userData.getMobile();
                    if (full_name.length() > 0 && mobile.length() > 0) {

                        HashMap<String, String> mMap = new HashMap<>();
                        mMap.put("cateid", "1090");
                        mMap.put("userid", noteData.getDoc_id());
                        mMap.put("hosid", noteData.getHos_id());
                        mMap.put("hosname", noteData.getHosname());
                        mMap.put("docname", noteData.getDocname());
                        mMap.put("fee", noteData.getPrice());
                        mMap.put("taoid", noteData.get_id());
                        mMap.put("server_id", noteData.getServer_id());
                        mMap.put("sharetime", noteData.getSharetime());
                        mMap.put("type", "2");
                        mMap.put("noteid", noteData.getT_id());
                        mMap.put("notetitle", noteData.getTitle());
                        mMap.put("consumer_certificate", "0");
                        pageJumpManager.jumpToWriteNoteActivity(PageJumpManager.DEFAULT_LOGO, mMap);

                        dismiss();
                    } else {
                        Intent intent = new Intent(mContext, PostingMessageActivity.class);

                        intent.putExtra("cateid", "1090");
                        intent.putExtra("userid", noteData.getDoc_id());
                        intent.putExtra("hosid", noteData.getHos_id());
                        intent.putExtra("hosname", noteData.getHosname());
                        intent.putExtra("docname", noteData.getDocname());
                        intent.putExtra("fee", noteData.getPrice());
                        intent.putExtra("taoid", noteData.get_id());
                        intent.putExtra("server_id", noteData.getServer_id());
                        intent.putExtra("sharetime", noteData.getSharetime());
                        intent.putExtra("type", "2");
                        intent.putExtra("noteid", noteData.getT_id());
                        intent.putExtra("notetitle", noteData.getTitle());

                        intent.putExtra("tiaozhuan", "8");

                        mContext.startActivity(intent);
                        dismiss();
                    }
                } else {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }

            }

        });
    }
}
