package com.quicklyask.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyask.activity.R;

import java.util.Map;

/**
 * 保险谈层
 * Created by dwb on 16/9/5.
 */
public class BaoxianPopWindow extends PopupWindow {

    private RelativeLayout alerCloseRly;
    private WebView webView;

    public BaoxianPopWindow(final Context mContext, final String url, Map<String, Object> urlMap) {
        final View view = View.inflate(mContext, R.layout.pop_baoxian,
                null);
        view.startAnimation(AnimationUtils.loadAnimation(mContext,
                R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();

        alerCloseRly = view.findViewById(R.id.pop_aler_close_rly);
        webView = view.findViewById(R.id.baoxian_web);

        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, urlMap);
        webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

        alerCloseRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();

            }
        });
    }
}

