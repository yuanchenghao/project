package com.quicklyask.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.quicklyask.activity.R;

/**
 * Created by dwb on 16/6/20.
 */
public class WritePicPopWindow extends PopupWindow {

    private Context mContext;
    private LinearLayout allLy;

    public WritePicPopWindow(Context mContext) {
        this(mContext,R.drawable.tiezhi_tips_2x);
    }

    public WritePicPopWindow(Context mContext,int tips){
        final View view = View.inflate(mContext, R.layout.pop_write_tuzhi_tips,
                null);

        ImageView pictureAndVideo = view.findViewById(R.id.iv_picture_and_video);
        pictureAndVideo.setImageResource(tips);

        view.startAnimation(AnimationUtils.loadAnimation(mContext,
                R.anim.fade_ins));

        allLy= view.findViewById(R.id.pop_cccc);


        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(false);
        setOutsideTouchable(true);
        setContentView(view);
        update();


        this.mContext=mContext;


        allLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
