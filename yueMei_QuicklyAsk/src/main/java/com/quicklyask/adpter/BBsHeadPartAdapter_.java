package com.quicklyask.adpter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.community.model.bean.HomeCommunityPartsData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by dwb on 16/8/11.
 */
public class BBsHeadPartAdapter_ extends BaseAdapter {

    private final String TAG = "BBsHeadPartAdapter";

    private List<HomeCommunityPartsData> mGroupData;
    private Context mContext;
    private LayoutInflater inflater;
    private HomeCommunityPartsData groupData;
    ViewHolder viewHolder;


    public BBsHeadPartAdapter_(Context mContext, List<HomeCommunityPartsData> mGroupData) {
        this.mContext = mContext;
        this.mGroupData = mGroupData;
        inflater = LayoutInflater.from(mContext);
    }

    static class ViewHolder {

        public TextView groupNameTV;
        public TextView groupSubNameTV;
        public ImageView groupIV;
        public TextView groupJiaTv;

    }

    @Override
    public int getCount() {
        return mGroupData.size();
    }

    @Override
    public Object getItem(int position) {
        return mGroupData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"NewApi", "InlinedApi", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_bbs_head_part_, null);

            viewHolder = new ViewHolder();
            viewHolder.groupNameTV = convertView.findViewById(R.id.item_bbs_head_name_iv);
            viewHolder.groupIV = convertView.findViewById(R.id.item_bbs_head_part_iv);
            viewHolder.groupSubNameTV = convertView.findViewById(R.id.item_bbs_head_subname_iv);
            viewHolder.groupJiaTv = convertView.findViewById(R.id.item_bbs_jia_tv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        groupData = mGroupData.get(position);

        // 填充布局
        if (groupData != null) {
            viewHolder.groupNameTV.setText(groupData.getName());
            viewHolder.groupSubNameTV.setText(groupData.getDesc());

            Glide.with(mContext).load(groupData.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).into(viewHolder.groupIV);

            viewHolder.groupJiaTv.setText("今日+" + groupData.getJia());
        }
        convertView.setBackgroundResource(R.color.white);

        return convertView;
    }

    public void add(List<HomeCommunityPartsData> infos) {
        mGroupData.addAll(infos);
    }
}
