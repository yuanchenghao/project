package com.quicklyask.util;

import android.util.Log;

import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.community.model.bean.AccessToken;
import com.module.community.model.bean.OcpaActionsData;
import com.module.community.model.bean.OcpaActionsUserIdData;
import com.module.community.model.bean.OcpaUserActionsDatas;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by 裴成浩 on 2019/7/18
 */
public class YybOcpa {
    private static String TAG = "YybOcpa";

//    /**
//     * 应用宝上报行为数据
//     */
//    private static void yybAdvertising() {
//
//        //获取access_token
//        new BaseNetWorkCallBackApi(FinalConstant1.MESSAGE, "ocpa").startCallBack(new BaseCallBackListener<ServerData>() {
//            @Override
//            public void onSuccess(ServerData data) {
//                Log.e(TAG, "data === " + data.toString());
//                AccessToken accessToken = JSONUtil.TransformSingleBean(data.data, AccessToken.class);
//                Log.e(TAG, "accessToken === " + accessToken.getAccess_token());
//
//                long nowTime = System.currentTimeMillis() / 1000;
//
//                String url = "https://api.e.qq.com/v1.1/user_actions/add?access_token=" + accessToken.getAccess_token() + "&timestamp=" + nowTime + "&nonce=" + nowTime + "nonce";
//
//                String jsoNparameter = JSONparameter(nowTime);
//                Log.e(TAG, "jsoNparameter === " + jsoNparameter);
//
//                HttpParams httpParams = new HttpParams();
//
//                httpParams.put("data",jsoNparameter);
//
//                HttpHeaders httpHeaders = new HttpHeaders();
//                httpHeaders.put("Content-Type", "application/json");
//
//                OkGo.post(url).params(httpParams)
//                        .headers(httpHeaders).execute(new StringCallback() {
//                    @Override
//                    public void onSuccess(String result, Call call, Response response) {
//                        Log.e(TAG, "result222 == " + result);
//                    }
//
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                    }
//                });
//
//            }
//        });
//    }

//    /**
//     * 应用宝上报行为数据
//     */
//    public static void yybAdvertising() {
//
//        //获取access_token
//        new BaseNetWorkCallBackApi(FinalConstant1.MESSAGE, "ocpa").startCallBack(new BaseCallBackListener<ServerData>() {
//            @Override
//            public void onSuccess(ServerData data) {
//                Log.e(TAG, "data === " + data.toString());
//                AccessToken accessToken = JSONUtil.TransformSingleBean(data.data, AccessToken.class);
//                Log.e(TAG, "accessToken === " + accessToken.getAccess_token());
//
//                String url = "https://api.e.qq.com/v1.1/user_actions/add?access_token=" + accessToken.getAccess_token() + "&timestamp=" + (System.currentTimeMillis() / 1000) + "&nonce=" + (System.currentTimeMillis() / 1000) + "nonce";
//
//                ArrayList<OcpaActionsData> aadsa = new ArrayList<>();
//
//                OcpaActionsData ocpaActionsData = new OcpaActionsData();
//                ocpaActionsData.setAction_time((int) (System.currentTimeMillis() / 1000));
//                ocpaActionsData.setAction_type("ACTIVATE_APP");
//
//                OcpaActionsUserIdData userIdData = new OcpaActionsUserIdData();
//                String s1 = MD5(Utils.getImei());
//                Log.e(TAG, "getImei() == " + Utils.getImei());
//                Log.e(TAG, "s1 == " + s1);
//                userIdData.setHash_imei(s1);
//                ocpaActionsData.setUser_id(userIdData);
//
//                aadsa.add(ocpaActionsData);
//
//                String s = new Gson().toJson(aadsa);
//                Log.e(TAG, "s == " + s);
//
//                Log.e(TAG, "url == " + url);
//
//                OkHttpClient client = new OkHttpClient();
//                //创建表单请求参数
//                FormBody.Builder builder = new FormBody.Builder();
//                builder.add("account_id", 293945 + "");                   //app的唯一id
//                builder.add("user_action_set_id", 1109546103 + "");           //数据源上报id
//                builder.add("actions", s);
//                FormBody formBody = builder.build();
//
//                Log.e(TAG, "asdsadaad" + new Gson().toJson(formBody));
//                Request request = new Request.Builder()
//                        .header("Content-Type", "application/json")
//                        .url(url)
//                        .post(formBody)
//                        .build();
//                client.newCall(request).enqueue(new Callback() {
//                    @Override
//                    public void onFailure(@NonNull Call call, IOException e) {
//                    }
//
//                    @Override
//                    public void onResponse(@NonNull Call call, @NonNull Response response) {
//                        try {
//                            Log.e(TAG, "response == " + response.toString());
//                            Log.e(TAG, "response.body() == " + response.body().string());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            Log.e(TAG, "e == " + e.toString());
//                        }
//
//                    }
//                });
//            }
//        });
//    }
//    /**
//     * 应用宝上报行为数据
//     */
//    private static void yybAdvertising() {
//
//        //获取access_token
//        new BaseNetWorkCallBackApi(FinalConstant1.MESSAGE, "ocpa").startCallBack(new BaseCallBackListener<ServerData>() {
//            @Override
//            public void onSuccess(ServerData data) {
//                Log.e(TAG, "data === " + data.toString());
//                AccessToken accessToken = JSONUtil.TransformSingleBean(data.data, AccessToken.class);
//                Log.e(TAG, "accessToken === " + accessToken.getAccess_token());
//
//                String url = "https://api.e.qq.com/v1.1/user_actions/add?access_token=" + accessToken.getAccess_token() + "&timestamp=" + (System.currentTimeMillis() / 1000) + "&nonce=" + (System.currentTimeMillis() / 1000) + "nonce";
//
//                ArrayList<OcpaActionsData> aadsa = new ArrayList<>();
//
//                OcpaActionsData ocpaActionsData = new OcpaActionsData();
//                ocpaActionsData.setAction_time((int) (System.currentTimeMillis() / 1000));
//                ocpaActionsData.setAction_type("ACTIVATE_APP");
//
//                OcpaActionsUserIdData userIdData = new OcpaActionsUserIdData();
//                String s1 = MD5(getImei());
//                Log.e(TAG, "getImei() == " + getImei());
//                Log.e(TAG, "s1 == " + s1);
//                userIdData.setHash_imei(s1);
//                ocpaActionsData.setUser_id(userIdData);
//
//                aadsa.add(ocpaActionsData);
//
//                String s = new Gson().toJson(aadsa);
//                Log.e(TAG, "s == " + s);
//
//                Log.e(TAG, "url == " + url);
//
//                OkHttpClient client = new OkHttpClient();
//                //创建表单请求参数
//                FormBody.Builder builder = new FormBody.Builder();
//                builder.add("account_id", 293945 + "");                   //app的唯一id
//                builder.add("user_action_set_id", 1109546103 + "");           //数据源上报id
//                builder.add("actions", s);
//                FormBody formBody = builder.build();
//
//                Log.e(TAG, "asdsadaad" + new Gson().toJson(formBody));
//                Request request = new Request.Builder()
//                        .header("Content-Type", "application/json")
//                        .url(url)
//                        .post()
//                        .build();
//                client.newCall(request).enqueue(new Callback() {
//                    @Override
//                    public void onFailure(@NonNull Call call, IOException e) {
//                    }
//
//                    @Override
//                    public void onResponse(@NonNull Call call, @NonNull Response response) {
//                        try {
//                            Log.e(TAG, "response == " + response.toString());
//                            Log.e(TAG, "response.body() == " + response.body().string());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            Log.e(TAG, "e == " + e.toString());
//                        }
//
//                    }
//                });
//            }
//        });
//    }
    /**
     * 应用宝上报行为数据
     */
    public static void yybAdvertising() {

        //获取access_token
        new BaseNetWorkCallBackApi(FinalConstant1.MESSAGE, "ocpa").startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                Log.e(TAG, "data === " + data.toString());
                AccessToken accessToken = JSONUtil.TransformSingleBean(data.data, AccessToken.class);
                Log.e(TAG, "accessToken === " + accessToken.getAccess_token());

                HttpParams httpParams = new HttpParams();
                String url = "https://api.e.qq.com/v1.1/user_actions/add?access_token=" + accessToken.getAccess_token() + "&timestamp=" + (System.currentTimeMillis() / 1000) + "&nonce=" + (System.currentTimeMillis() / 1000) + "nonce";

                httpParams.put("account_id", 293945 + "");                   //app的唯一id
                httpParams.put("user_action_set_id", 1109546103 + "");           //数据源上报id

                ArrayList<OcpaActionsData> aadsa = new ArrayList<>();

                OcpaActionsData ocpaActionsData = new OcpaActionsData();
                ocpaActionsData.setAction_time((int) (System.currentTimeMillis() / 1000));
                ocpaActionsData.setAction_type("ACTIVATE_APP");

                OcpaActionsUserIdData userIdData = new OcpaActionsUserIdData();
                String s1 = Utils.StringInMd5(Utils.getImei());
                Log.e(TAG, "getImei() == " + Utils.getImei());
                Log.e(TAG, "s1 == " + s1);
                userIdData.setHash_imei(s1);
                ocpaActionsData.setUser_id(userIdData);

                aadsa.add(ocpaActionsData);

                String s = new Gson().toJson(aadsa);
                Log.e(TAG, "s == " + s);
                httpParams.put("actions", s);
                Log.e(TAG, "url == " + url);

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.put("Content-Type", "application/json");

                String toJson = new Gson().toJson(httpParams);
                Log.e(TAG, "toJson == " + toJson);

                OkGo.post(url).params(httpParams).headers(httpHeaders).execute(new StringCallback() {
                    @Override
                    public void onSuccess(String result, Call call, Response response) {
                        Log.e(TAG, "result222 == " + result);
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });


            }
        });
    }

    /**
     * 获取请求参数串
     *
     * @return
     */
    private static String JSONparameter(long nowTime) {

        OcpaActionsUserIdData userIdData = new OcpaActionsUserIdData();
        String s1 = Utils.StringInMd5(Utils.getImei());
        Log.e(TAG, "getImei() == " + Utils.getImei());
        Log.e(TAG, "s1 == " + s1);
        userIdData.setHash_imei(s1);

        ArrayList<OcpaActionsData> ocpaActionsDatas = new ArrayList<>();
        OcpaActionsData ocpaActionsData = new OcpaActionsData();
        ocpaActionsData.setAction_time((int) nowTime);
        ocpaActionsData.setAction_type("ACTIVATE_APP");
        ocpaActionsData.setUser_id(userIdData);

        ocpaActionsDatas.add(ocpaActionsData);

        OcpaUserActionsDatas ocpaUserActionsDatas = new OcpaUserActionsDatas();
        ocpaUserActionsDatas.setAccount_id(293945);
        ocpaUserActionsDatas.setUser_action_set_id(1109546103);
        ocpaUserActionsDatas.setActions(ocpaActionsDatas);

        return new Gson().toJson(ocpaUserActionsDatas);
    }
}
