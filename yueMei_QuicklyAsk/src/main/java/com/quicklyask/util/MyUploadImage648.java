package com.quicklyask.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.util.Log;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.my.model.bean.UploadImageSuccessData;
import com.module.my.view.view.PostingAndNoteHandler;
import com.module.other.netWork.netWork.QiNiuConfigration;
import com.module.other.netWork.netWork.ServerData;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/8/8.
 */
public class MyUploadImage648 {

    private String TAG = "MyUploadImage648";
    private Context mContext;
    private PostingAndNoteHandler mHandler;
    private File uploadFile;
    private UploadImageSuccessData imageData;

    /**
     * 连续上传
     *
     * @param context
     * @param handler
     * @param imgPath
     * @return
     */
    public static MyUploadImage648 getMyUploadImage(Context context, PostingAndNoteHandler handler, String imgPath) {
        return getMyUploadImage(context, handler, imgPath, true);
    }

    /**
     * 不连续上传
     *
     * @param context
     * @param handler
     * @param imgPath
     * @return
     */
    public static MyUploadImage648 getMyUploadImage(Context context, PostingAndNoteHandler handler, String imgPath, boolean isContinuous) {
        return new MyUploadImage648(context, handler, imgPath, isContinuous);
    }

    private MyUploadImage648(Context context, PostingAndNoteHandler handler, String imgPath, boolean isContinuous) {
        this.mContext = context;
        this.mHandler = handler;

//        uploadFile = FileCompressionUtils.compressionFile(imgPath);
        uploadFile = new File(imgPath);

        imageData = new UploadImageSuccessData(isContinuous);
        int[] imageSize = getImageSize(imgPath);
        imageData.setWidth(imageSize[0]);
        imageData.setHeight(imageSize[1]);
    }

    public void uploadImage(final String qiNiuKey) {
        String qiniutoken = Cfg.loadStr(mContext, "qiniutoken", "0");
        UploadManager uploadManager = QiNiuConfigration.getInstance().init();
        Log.e(TAG, "uploadFile === " + uploadFile);
        Log.e(TAG, "qiniutoken == " + qiniutoken);
        uploadManager.put(uploadFile, qiNiuKey, qiniutoken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {
                if (info.isOK()) {
                    Log.e(TAG, "Upload Success");
                    imageData.setImageUrl(qiNiuKey);
                    Message msg = Message.obtain();
                    msg.obj = imageData;
                    msg.what = 3;
                    mHandler.sendMessage(msg);
                } else {
                    try {
                        Log.e(TAG, "response === " + response);
                        if (response != null) {
                            String error = response.getString("error");
                            if ("expired token".equals(error)) {
                                new QiNiuTokenApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        if ("1".equals(serverData.code)) {
                                            try {
                                                QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                Cfg.saveStr(mContext, "qiniutoken", qiNiuBean.getQiniu_token());
                                                uploadImage(qiNiuKey);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else {
                                Message msg = Message.obtain();
                                msg.what = 4;
                                mHandler.sendMessage(msg);
                            }
                        } else {
                            Message msg = Message.obtain();
                            msg.what = 4;
                            mHandler.sendMessage(msg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                Log.e(TAG, key + ",\r\n " + info + ",\r\n " + response);
            }
        }, new UploadOptions(null, null, false, new UpProgressHandler() {
            @Override
            public void progress(String key, double percent) {
                Log.e(TAG, "progress===" + (int) (percent * 100));
                Message obtain = Message.obtain();
                obtain.what = 2;
                obtain.arg1 = (int) (percent * 100);
                mHandler.sendMessage(obtain);
            }
        }, null));
    }

    /**
     * 获取图片宽高
     *
     * @param path
     * @return
     */
    private int[] getImageSize(String path) {
        int[] imageSize = new int[2];
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path); // 此时返回的bitmap为null

            imageSize[0] = bitmap.getWidth();
            imageSize[1] = bitmap.getHeight();

            // bitmap回收防止内存溢出
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            System.gc();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e(TAG, "e === " + e.toString());
            imageSize[0] = 0;
            imageSize[1] = 0;
        }
        return imageSize;
    }
}
