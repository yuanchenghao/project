package com.quicklyask.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.module.base.api.BaseCallBackListener;
import com.module.home.model.api.NoteBookDataApi;
import com.module.my.controller.activity.SelectNoteActivity;
import com.module.my.model.bean.NoteBookListData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.view.NoteTipsDialog;

import java.util.HashMap;

/**
 * 写日记管理器
 * <p>
 * Created by dwb on 16/3/28.
 */
public class WriteNoteManager {

    private Context mContext;

    private NoteTipsDialog notePop;

    private static WriteNoteManager writeNoteManager = null;

    private WriteNoteManager(Context mContext) {
        this.mContext = mContext;
    }

    public static WriteNoteManager getInstance(Context mContext) {

        writeNoteManager = new WriteNoteManager(mContext);

        return writeNoteManager;
    }

    public void ifAlert(final NoteBookListData nnn) {
        NoteBookDataApi noteBookDataApi = new NoteBookDataApi();
        HashMap<String, Object> NoteBookDataMap = new HashMap<>();
        noteBookDataApi.getCallBack(mContext, NoteBookDataMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                try {
                    if (!"1".equals(serverData.code)) {
                        NoteBookListData noteD = JSONUtil.TransformSingleBean(serverData.data, NoteBookListData.class);
                        if (null != noteD) {
                            notePop = new NoteTipsDialog(mContext, noteD, "1", nnn);
                            notePop.show();
                        } else {
                            if (null != nnn) {
                                Log.e("SelectNoteActivity", "444444");
                                Intent it = new Intent();
                                it.setClass(mContext, SelectNoteActivity.class);
                                it.putExtra(    "cateid", "1090");
                                it.putExtra("userid", nnn.getDoc_id());
                                it.putExtra("hosid", nnn.getHos_id());
                                it.putExtra("hosname", nnn.getHosname());
                                it.putExtra("docname", nnn.getDocname());
                                it.putExtra("fee", nnn.getPrice());
                                it.putExtra("taoid", nnn.get_id());
                                it.putExtra("server_id", nnn.getServer_id());
                                mContext.startActivity(it);
                            } else {
                                Log.e("SelectNoteActivity", "5555555");
                                Intent it = new Intent();
                                it.setClass(mContext, SelectNoteActivity.class);
                                it.putExtra("cateid", "1090");
                                it.putExtra("userid", "");
                                it.putExtra("hosid", "");
                                it.putExtra("hosname", "");
                                it.putExtra("docname", "");
                                it.putExtra("fee", "");
                                it.putExtra("taoid", "");
                                it.putExtra("server_id", "");
                                mContext.startActivity(it);
                            }
                        }

                    } else {        //直接跳日记本列表
                        if (null != nnn) {
                            Log.e("SelectNoteActivity", "666666");
                            Intent it = new Intent();
                            it.setClass(mContext, SelectNoteActivity.class);
                            it.putExtra("cateid", "1090");
                            it.putExtra("userid", nnn.getDoc_id());
                            it.putExtra("hosid", nnn.getHos_id());
                            it.putExtra("hosname", nnn.getHosname());
                            it.putExtra("docname", nnn.getDocname());
                            it.putExtra("fee", nnn.getPrice());
                            it.putExtra("taoid", nnn.get_id());
                            it.putExtra("server_id", nnn.getServer_id());
                            mContext.startActivity(it);
                        } else {
                            Log.e("SelectNoteActivity", "777777");
                            Intent it = new Intent();
                            it.setClass(mContext, SelectNoteActivity.class);
                            it.putExtra("cateid", "1090");
                            it.putExtra("userid", "");
                            it.putExtra("hosid", "");
                            it.putExtra("hosname", "");
                            it.putExtra("docname", "");
                            it.putExtra("fee", "");
                            it.putExtra("taoid", "");
                            it.putExtra("server_id", "");
                            mContext.startActivity(it);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }


}
