package com.quicklyask.util.upyun;

public class UpException extends Exception {
    public UpException(String msg) {
        super(msg);
    }
}
