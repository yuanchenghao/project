package com.xinlan.imageeditlibrary.editimage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.xinlan.imageeditlibrary.BaseActivity;
import com.xinlan.imageeditlibrary.R;
import com.xinlan.imageeditlibrary.editimage.fragment.CropFragment;
import com.xinlan.imageeditlibrary.editimage.fragment.FliterListFragment;
import com.xinlan.imageeditlibrary.editimage.fragment.MainMenuFragment;
import com.xinlan.imageeditlibrary.editimage.fragment.RotateFragment;
import com.xinlan.imageeditlibrary.editimage.fragment.StirckerFragment;
import com.xinlan.imageeditlibrary.editimage.utils.BitmapUtils;
import com.xinlan.imageeditlibrary.editimage.view.CropImageView;
import com.xinlan.imageeditlibrary.editimage.view.CustomViewPager;
import com.xinlan.imageeditlibrary.editimage.view.RotateImageView;
import com.xinlan.imageeditlibrary.editimage.view.StickerView;
import com.xinlan.imageeditlibrary.editimage.view.imagezoom.ImageViewTouch;
import com.xinlan.imageeditlibrary.editimage.view.imagezoom.ImageViewTouchBase;

import java.io.File;

/**
 * 图片编辑 主页面
 *
 * @author panyi
 *         <p>
 *         包含 1.贴图 2.滤镜 3.剪裁 4.底图旋转 功能
 */
public class EditImageActivity extends BaseActivity {
    public static final String FILE_PATH = "file_path";
    public static final String EXTRA_OUTPUT = "extra_output";
    public static final String EXTRA_COVER = "extra_cover";

    public static final int MODE_NONE = 0;// 主菜单模式
    public static final int MODE_STICKERS = 1;// 贴图模式
    public static final int MODE_FILTER = 2;// 滤镜模式
    public static final int MODE_CROP = 3;// 剪裁模式
    public static final int MODE_ROTATE = 4;// 旋转模式
    public static final int MODE_TEXT = 5;// 文字模式

    public String filePath;// 需要编辑图片路径
    public String saveFilePath;// 生成的新图片路径
    private int imageWidth, imageHeight;// 展示图片控件 宽 高
    private LoadImageTask mLoadImageTask;

    public int mode = MODE_NONE;// 当前操作模式
    private EditImageActivity mContext;
    public Bitmap mainBitmap;// 底层显示Bitmap
    public ImageViewTouch mainImage;
    private View backBtn;

    public ViewFlipper bannerFlipper;
    private View applyBtn;// 应用按钮
    private View saveBtn;// 保存按钮

    public StickerView mStickerView;// 贴图层View
    public CropImageView mCropPanel;// 剪切操作控件
    public RotateImageView mRotatePanel;// 旋转操作控件

    public CustomViewPager bottomGallery;// 底部gallery
    private BottomGalleryAdapter mBottomGalleryAdapter;// 底部gallery
    private MainMenuFragment mMainMenuFragment;// Menu 菜单
    public StirckerFragment mStirckerFragment;// 贴图Fragment
    public FliterListFragment mFliterListFragment;// 滤镜FliterListFragment
    private CropFragment mCropFragment;// 图片剪裁Fragment
    public RotateFragment mRotateFragment;// 图片旋转Fragment

    public RelativeLayout mDeleteBt;
    private String pos = "0";
    public RelativeLayout mCover;
    public String extraCover;
    private String TAG = "EditImageActivity";
    private ImageView mDiagram;
    private boolean isCover = false;        //是否设置为封面。默认不是封面图
    private boolean addSickers = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        checkInitImageLoader();
        setContentView(R.layout.activity_image_edit);
        initView();
        getData();
    }

    private void getData() {
        filePath = getIntent().getStringExtra(FILE_PATH);        //传过来图片的路径
        saveFilePath = getIntent().getStringExtra(EXTRA_OUTPUT);// 保存图片路径

        extraCover = getIntent().getStringExtra(EXTRA_COVER);// 是否有设置封面的功能

        if(null != extraCover && "1".equals(extraCover)){     //有封面功能
            mCover.setVisibility(View.VISIBLE);
        }else {
            mCover.setVisibility(View.GONE);
        }

        pos = getIntent().getStringExtra("pos");
        isCover = getIntent().getBooleanExtra("isCover", false);
        if(isCover){
            mDiagram.setImageResource(R.drawable.select_cover_diagram);
        }else {
            mDiagram.setImageResource(R.drawable.uncheck_cover_diagram);
        }

        loadImage(filePath);
    }

    private void initView() {
        mContext = this;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        imageWidth = (int) ((float) metrics.widthPixels / 1.5);
        imageHeight = (int) ((float) metrics.heightPixels / 1.5);

        bannerFlipper = findViewById(R.id.banner_flipper);
//		bannerFlipper.setInAnimation(this, R.anim.in_bottom_to_top);
//		bannerFlipper.setOutAnimation(this, R.anim.out_bottom_to_top);
        applyBtn = findViewById(R.id.apply);
        applyBtn.setOnClickListener(new ApplyBtnClick());
        saveBtn = findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new SaveBtnClick());
        mDeleteBt = findViewById(R.id.delete_tutututu);
        mCover = findViewById(R.id.delete_txtxtxtx);
        mDiagram = findViewById(R.id.cover_diagram);
        mDeleteBt.setOnClickListener(new DeleBtnClick());
        mDiagram.setOnClickListener(new DiagramBtnClick());

        mainImage = findViewById(R.id.main_image);//主图片
        backBtn = findViewById(R.id.back_btn);// 退出按钮
        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                forceReturnBack();
            }
        });

        mStickerView = findViewById(R.id.sticker_panel);
        mCropPanel = findViewById(R.id.crop_panel);
        mRotatePanel = findViewById(R.id.rotate_panel);

        // 底部gallery
        bottomGallery = findViewById(R.id.bottom_gallery);
        //bottomGallery.setOffscreenPageLimit(5);
        mBottomGalleryAdapter = new BottomGalleryAdapter(
                this.getSupportFragmentManager());
        mMainMenuFragment = MainMenuFragment.newInstance(this);
        mStirckerFragment = StirckerFragment.newInstance(this);
        mFliterListFragment = FliterListFragment.newInstance(this);
        mCropFragment = CropFragment.newInstance(this);
        mRotateFragment = RotateFragment.newInstance(this);
        bottomGallery.setAdapter(mBottomGalleryAdapter);
    }

    /**
     * @author panyi
     */
    private final class BottomGalleryAdapter extends FragmentPagerAdapter {
        public BottomGalleryAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int index) {
            Log.e(TAG,"index11 === " + index);
            switch (index) {
                case MainMenuFragment.INDEX:// 主菜单      0
                    return mMainMenuFragment;
                case StirckerFragment.INDEX:// 贴图         1
                    return mStirckerFragment;
                case FliterListFragment.INDEX:// 滤镜         2
                    return mFliterListFragment;
                case CropFragment.INDEX://剪裁            3
                    return mCropFragment;
                case RotateFragment.INDEX://旋转          4
                    return mRotateFragment;
            }//end switch
            return MainMenuFragment.newInstance(mContext);
        }

        @Override
        public int getCount() {
            return 5;
        }
    }// end inner class

    /**
     * 异步载入编辑图片
     *
     * @param filepath
     */
    public void loadImage(String filepath) {
        if (mLoadImageTask != null) {
            mLoadImageTask.cancel(true);
        }
        mLoadImageTask = new LoadImageTask();
        mLoadImageTask.execute(filepath);
    }

    private final class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            return BitmapUtils.loadImageByPath(params[0], imageWidth,
                    imageHeight);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (mainBitmap != null) {
                mainBitmap.recycle();
                mainBitmap = null;
                System.gc();
            }
            mainBitmap = result;
            mainImage.setImageBitmap(result);
            mainImage.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);
            // mainImage.setDisplayType(DisplayType.FIT_TO_SCREEN);
        }
    }// end inner class

    /**
     * 按下返回键
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e(TAG,"mode222 === " + mode);
            switch (mode) {
                case MODE_STICKERS:
                    mStirckerFragment.backToMain();
                    return true;
                case MODE_FILTER:// 滤镜编辑状态
                    mFliterListFragment.backToMain();// 保存滤镜贴图
                    return true;
                case MODE_CROP:// 剪切图片保存
                    mCropFragment.backToMain();
                    return true;
                case MODE_ROTATE:// 旋转图片保存
                    mRotateFragment.backToMain();
                    return true;
            }// end switch

            forceReturnBack();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 强制推出
     */
    private void forceReturnBack() {
        setResult(RESULT_CANCELED);
        this.finish();
    }

    /**
     * 保存按钮点击(如果没有添加贴纸则不走这个方法)
     *
     * @author panyi
     */
    private final class ApplyBtnClick implements OnClickListener {
        @Override
        public void onClick(View v) {
            addSickers = true;
            Log.e(TAG, "mode333 == " + mode);
            switch (mode) {
                case MODE_STICKERS:   //1
                    mStirckerFragment.saveStickers();// 保存贴图
                    break;
                case MODE_FILTER:// 滤镜编辑状态  2
                    mFliterListFragment.saveFilterImage();// 保存滤镜贴图
                    break;
                case MODE_CROP:// 剪切图片保存
                    mCropFragment.saveCropImage();
                    break;
                case MODE_ROTATE:// 旋转图片保存
                    mRotateFragment.saveRotateImage();
                    break;
                default:
                    break;
            }// end switch
        }
    }// end inner class

    /**
     * 保存按钮 点击退出
     *
     * @author panyi
     */
    private final class SaveBtnClick implements OnClickListener {
        @Override
        public void onClick(View v) {

            mStirckerFragment.saveStickers();// 保存贴图

//			if(ifhaveFile(saveFilePath)) {
//
//				Intent returnIntent = new Intent();
//				returnIntent.putExtra("save_file_path", saveFilePath);
//				returnIntent.putExtra("pos", pos);
//				returnIntent.putExtra("dele", "0");
//				mContext.setResult(RESULT_OK, returnIntent);
//				mContext.finish();
//			}else {
//				forceReturnBack();
//			}
        }
    }// end inner class


    private final class DeleBtnClick implements OnClickListener {
        @Override
        public void onClick(View v) {

            Intent returnIntent = new Intent();
            returnIntent.putExtra("save_file_path", saveFilePath);
            returnIntent.putExtra("pos", pos);
            returnIntent.putExtra("dele", "1");
            returnIntent.putExtra("isCover",isCover);
            mContext.setResult(RESULT_OK, returnIntent);
            mContext.finish();

        }
    }// end inner class DeleBtnClick()

    private final class DiagramBtnClick implements OnClickListener {
        @Override
        public void onClick(View v) {
           if(isCover){
               mDiagram.setImageResource(R.drawable.uncheck_cover_diagram);
               isCover = false;
           }else {
               mDiagram.setImageResource(R.drawable.select_cover_diagram);
               isCover = true;
           }
        }
    }// end inner class DiagramBtnClick()

    public boolean ifhaveFile(String paths) {
        try {
            File f = new File(paths);
            if (!f.exists()) {
                return false;
            }

        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
        return true;
    }

    /**
     * 切换底图Bitmap
     *
     * @param newBit
     */
    public void changeMainBitmap(Bitmap newBit) {
        if (mainBitmap != null) {
            if (!mainBitmap.isRecycled()) {// 回收
                mainBitmap.recycle();
            }
            mainBitmap = newBit;
        } else {
            mainBitmap = newBit;
        }// end if
        mainImage.setImageBitmap(mainBitmap);

        if (ifhaveFile(saveFilePath)) {

            Intent returnIntent = new Intent();
            if(addSickers){                                 //添加了贴纸
                returnIntent.putExtra("save_file_path", saveFilePath);
            }else {                                 //没有添加贴纸，但是可以添加封面
                returnIntent.putExtra("save_file_path", filePath);
            }

            returnIntent.putExtra("pos", pos);
            returnIntent.putExtra("dele", "0");
            returnIntent.putExtra("isCover",isCover);
            mContext.setResult(RESULT_OK, returnIntent);
            mContext.finish();
        } else {
            forceReturnBack();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLoadImageTask != null) {
            mLoadImageTask.cancel(true);
        }
    }

}// end class
